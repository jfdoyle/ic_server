<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defined("INCLUDE_DIR") || define("INCLUDE_DIR", dirname(__FILE__) . DIRECTORY_SEPARATOR . "include" . DIRECTORY_SEPARATOR);

require INCLUDE_DIR . "config.default.php";
include INCLUDE_DIR . "config.inc.php";

/**
 * Get git version and commit date
 * @return type
 */
function git_version() {
	global $cfg;

	$package = json_decode(file_get_contents("package.json"), true);
	$commitHash = trim(exec("$cfg[github_git] log --pretty=%h -n1 HEAD"));
	$commitDate = new DateTime(trim(exec("$cfg[github_git] log -n1 --pretty=%ci HEAD")));
	$commitDate->setTimezone(new DateTimeZone("UTC"));
	return sprintf("%s v%s-%s (%s)", $package["name"], $package["version"], $commitHash, $commitDate->format("Y-m-d H:m:s"));
}

function get_versions() {
	$output = "╥ ╔═╕ " . git_version() . "\\n";
	chdir("editor");
	$output .= "║ ║   " . git_version() . "\\n";
	chdir("activity");
	$output .= "╨ ╚═╛ " . git_version();
	return $output;
}

echo "try{console.log('" . get_versions() . "')}catch(e){}";
