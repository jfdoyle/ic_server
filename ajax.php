<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

if (!filter_has_var(INPUT_SERVER, "HTTP_X_REQUESTED_WITH") || strtolower(filter_input(INPUT_SERVER, "HTTP_X_REQUESTED_WITH", FILTER_SANITIZE_STRING)) !== "xmlhttprequest") {
	// Only respond to Ajax
	die("{\"error\":\"Invalid Request\"}");
}

require_once "include/common.php";

$editor = true;

// NOTE: Do not call do_login() - we"re only interested in data, not logging in
if (!login_check()) {
	if (filter_has_var(INPUT_POST, "username") && filter_has_var(INPUT_POST, "password")) {
		if (login(filter_input(INPUT_POST, "username", FILTER_UNSAFE_RAW), filter_input(INPUT_POST, "password", FILTER_UNSAFE_RAW))) {
			die("{\"success\":\"Logged in\"}");
		}
	}
	die("{\"error\":\"Not logged in\"}");
}

// ----------------------------------------------------------------------------

/**
 * Gets the _POST data with correct handling of nested brackets:
 * "path[to][data[nested]]=value"
 * "path"
 *    -> "to"
 *       -> "data[nested]" = value
 * @return array
 */
function get_real_post() {

	function set_nested_value(&$arr, &$keys, &$value) {
		$key = array_shift($keys);
		if (count($keys)) {
			// Got deeper to go
			if (!array_key_exists($key, $arr)) {
				// Make sure we can get deeper if we've not hit this key before
				$arr[$key] = array();
			} elseif (!is_array($arr[$key])) {
				// This should never be relevant for well formed input data
				throw new Exception("Setting a value and an array with the same key: $key");
			}
			set_nested_value($arr[$key], $keys, $value);
		} elseif (array_key_exists($key, $arr)) {
			// Already set, so must be an array
			if (!is_array($arr[$key])) {
				$arr[$key] = array($arr[$key]);
			}
			$arr[$key][] = $value;
		} elseif (empty($key)) {
			// Setting an Array
			$arr[] = $value;
		} else {
			// Setting an Object
			$arr[$key] = $value;
		}
	}

	$input = array();
	$parts = array();
	$pairs = explode("&", file_get_contents("php://input"));
	foreach ($pairs as $pair) {
		$key_value = explode("=", $pair, 2);
		preg_match_all("/([a-zA-Z0-9]*)(?:\[([^\[\]]*(?:(?R)[^\[\]]*)*)\])?/", urldecode($key_value[0]), $parts);
		$keys = array($parts[1][0]);
		if (!empty($parts[2][0])) {
			array_pop($parts[2]); // Remove the blank one on the end
			$keys = array_merge($keys, $parts[2]);
		}
		$value = urldecode($key_value[1]);
		if ($value == "true") {
			$value = true;
		} else if ($value == "false") {
			$value = false;
		} else if (is_numeric($value)) {
			if (strpos($value, ".") !== false) {
				$num = floatval($value);
			} else {
				$num = intval($value);
			}
			if (strval($num) === $value) {
				$value = $num;
			}
		}
		set_nested_value($input, $keys, $value);
	}
	return $input;
}

function filter_post($key, $filter = FILTER_DEFAULT, $options = null) {
	global $post;

	if ($filter === FILTER_VALIDATE_INT && is_null($options)) {
		$options = array("options" => array("default" => 0));
	}
	if (array_key_exists($key, $post)) {
//		error_log("filter_post, $key = " . print_r(filter_var($post[$key], $filter, $options), true));
		return filter_var($post[$key], $filter, $options);
	}
	return filter_var("", $filter, $options);
}

$result = array("error" => "No permission");
$post = get_real_post();

if (array_key_exists("action", $post) && preg_match("/^[a-z_]+$/", $post["action"])) {
	$ajax_call = INCLUDE_DIR . "ajax/$post[action].php";

	if (is_file($ajax_call)) {
//		error_log($ajax_call);
//		error_log(print_r($post, true));
		$result = (include($ajax_call));
	}
}

json_die($result);

// No closing delimiter as this is AJAX only
