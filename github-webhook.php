<?php

/**
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * GitHub webhook handler template.
 * 
 * @see  https://developer.github.com/webhooks/
 */
define("INCLUDE_DIR", dirname(__FILE__) . DIRECTORY_SEPARATOR . "include" . DIRECTORY_SEPARATOR);

require_once(INCLUDE_DIR . "config.default.php");
include_once(INCLUDE_DIR . "config.inc.php");

$debug = true;

if (!$cfg["github"]) {
	die;
}

function quit($msg) {
	global $debug;

	if ($debug) {
		error_log($msg);
	}
	die;
}

set_exception_handler(function($e) {
	header("HTTP/1.1 500 Internal Server Error");
	quit("Error on line {$e->getLine()}: " . htmlSpecialChars($e->getMessage()));
});

$raw = null;

if (!empty($cfg["github_secret"])) {
	$signature = filter_input(INPUT_SERVER, "HTTP_X_HUB_SIGNATURE");

	if (!isset($signature)) {
		quit("HTTP header \"X-Hub-Signature\" is missing.");
	} elseif (!extension_loaded("hash")) {
		quit("Missing \"hash\" extension to check the secret code validity.");
	}
	list($algo, $hash) = explode("=", $signature, 2) + array("", "");
	if (!in_array($algo, hash_algos(), TRUE)) {
		quit("Hash algorithm \"$algo\" is not supported.");
	}
	$raw = file_get_contents("php://input");
	if (!hash_equals($hash, hash_hmac($algo, $raw, $cfg["github_secret"]))) {
		quit("Hook secret does not match.");
	}
}

$type = filter_input(INPUT_SERVER, "CONTENT_TYPE");
$event = filter_input(INPUT_SERVER, "HTTP_X_GITHUB_EVENT");

if (empty($type)) {
	quit("Missing HTTP \"Content-Type\" header.");
} elseif (empty($event)) {
	quit("Missing HTTP \"X-Github-Event\" header.");
}

switch ($type) {
	case "application/json":
		$json = $raw ?: file_get_contents("php://input");
		break;

	case "application/x-www-form-urlencoded":
		$json = filter_input(INPUT_POST, "payload", FILTER_UNSAFE_RAW);
		break;

	default:
		quit("Unsupported content type: $type");
}

# Payload structure depends on triggered event
# https://developer.github.com/v3/activity/events/types/
$payload = json_decode($json);

require "include/twitter.php";

switch (strtolower($event)) {
	case "ping":
		echo "pong";
		break;

	case "push":
		if ($payload->repository->url == "https://github.com/$cfg[github_repo]") {
			// Valid repo, but don't do anything extra if it's the wrong branch
			if ($payload->ref == "refs/heads/$cfg[github_branch]") {
				$return_var = null;
				passthru("$cfg[github_git] $cfg[github_pull] $cfg[github_branch]", $return_var);
				if ($return_var) {
					tweet("Pull FAILED Server ($return_var): " . $payload->compare);
				} else {
					chdir("editor");
					passthru("$cfg[github_git] $cfg[github_pull2] $cfg[github_branch]", $return_var);
					if ($return_var) {
						tweet("Pull FAILED Editor ($return_var): " . $payload->compare);
					} else {
						chdir("activity");
						passthru("$cfg[github_git] $cfg[github_pull3] $cfg[github_branch]", $return_var);
						if ($return_var) {
							tweet("Pull FAILED Activities ($return_var): " . $payload->compare);
						} else {
							tweet("Pull success: " . $payload->compare);
						}
					}
				}
			}
			break;
		} else {
			tweet("Invalid pull from GitHub");
			if ($debug) {
				error_log("Unmatched Event: $event\nRepository: " . $payload->repository->url . "\nBranch: " . $payload->ref . "\nPayload:\n");
			}
		}

	default:
		header("HTTP/1.0 404 Not Found");
		if ($debug) {
			echo "Event: $event\nPayload:\n";
			print_r($payload); # For debug only. Can be found in GitHub hook log.
		} else {
			echo "Failure";
		}
		break;
}
