<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

if (!isset($cfg)) {
	$cfg = array();
}
(include_once "include/config.inc.php") or require_once "include/config.default.php";

if (strlen(session_id()) > 0) {
	session_write_close();
}

set_error_handler(function($errno, $message) {
	if ($errno === E_WARNING && (strpos($message, "filemtime") === 0 || strpos($message, "fopen") === 0)) {
		return true; // using filemtime for file_exists too
	}
	return false; // default handler
});

$request_uri = filter_input(INPUT_SERVER, "REQUEST_URI", FILTER_SANITIZE_URL);
$referer = filter_input(INPUT_SERVER, "HTTP_REFERER", FILTER_SANITIZE_URL);
$file = filter_input(INPUT_SERVER, "QUERY_STRING", FILTER_UNSAFE_RAW);

//header("Connection: close");
// Not logging in or anything - don"t need shared code beyond db...

$mime_types = array(
	"txt" => "text/plain",
	"htm" => "text/html",
	"html" => "text/html",
	"php" => "text/html",
	"css" => "text/css",
	"js" => "text/javascript",
	"json" => "application/json",
	"xml" => "application/xml",
	"swf" => "application/x-shockwave-flash",
	"flv" => "video/x-flv",
	// images
	"png" => "image/png",
	"jpe" => "image/jpeg",
	"jpeg" => "image/jpeg",
	"jpg" => "image/jpeg",
	"gif" => "image/gif",
	"bmp" => "image/bmp",
	"ico" => "image/vnd.microsoft.icon",
	"tiff" => "image/tiff",
	"tif" => "image/tiff",
	"svg" => "image/svg+xml",
	"svgz" => "image/svg+xml",
	// archives
	"zip" => "application/zip",
	"rar" => "application/x-rar-compressed",
	"exe" => "application/x-msdownload",
	"msi" => "application/x-msdownload",
	"cab" => "application/vnd.ms-cab-compressed",
	// audio/video
	"mp3" => "audio/mpeg",
	"m4a" => "audio/aac",
	"aac" => "audio/aac",
	"ogg" => "audio/ogg",
	"qt" => "video/quicktime",
	"mov" => "video/quicktime",
	// adobe
	"pdf" => "application/pdf",
	"psd" => "image/vnd.adobe.photoshop",
	"ai" => "application/postscript",
	"eps" => "application/postscript",
	"ps" => "application/postscript",
	// ms office
	"doc" => "application/msword",
	"rtf" => "application/rtf",
	"xls" => "application/vnd.ms-excel",
	"ppt" => "application/vnd.ms-powerpoint",
	// open office
	"odt" => "application/vnd.oasis.opendocument.text",
	"ods" => "application/vnd.oasis.opendocument.spreadsheet",
	// fonts
	"woff" => "application/x-font-woff",
	"woff2" => "application/font-woff2",
	"eot" => "application/vnd.ms-fontobject",
	"otf" => "font/opentype",
	"ttf" => "font/truetype"
);

//if (!function_exists("mime_content_type2")) {

function mime_content_type2($filename) {
	global $mime_types;

	$ext = strtolower(array_pop(explode(".", $filename)));
	if (array_key_exists($ext, $mime_types)) {
		return $mime_types[$ext];
	} elseif (function_exists("finfo_open")) {
		$finfo = finfo_open(FILEINFO_MIME);
		$mimetype = finfo_file($finfo, $filename);
		finfo_close($finfo);
		return $mimetype;
	} else {
		return "application/octet-stream";
	}
}

//}
// ----------------------------------------------------------------------------

/* * *****************************************************************************
 * MATHS
 * **************************************************************************** */
if (!preg_match("/^\/assets(?:\/|\.php\?)(\d+\/)?.*\.(" . (implode("|", array_keys($mime_types))) . ")(?:\?_=\d+)?$/i", $request_uri)) {

	require_once "include/latex.php";
	$filename = latex(rawurldecode(preg_replace("/^(\/assets\.php\?)/", "", $GLOBALS["request_uri"])));
	header("Location: " . $filename, true, 302);
	exit;

//// Old phantomjs svg generation
//	function get_formula($formula) {
//		global $cfg;
//
//		// From assets.php
//		$basepath = getcwd();
//		$folder = "cache";
//
//		$bbcode = array(
////			"/([\&\%\$\#\_\{\}])/s" => "\\$1",
//			"/\n/" => '\n\\newline\n',
//			"/([\&\%\$\#\_])/s" => '\\\\\\1',
//			"/\[b\](.*?)\[\/b\]/is" => '\textbf{$1}',
//			"/\[i\](.*?)\[\/i\]/is" => '\textit{$1}',
//			"/\[u\](.*?)\[\/u\]/is" => '\underline{$1}',
//			"/\[sup\](.*?)\[\/sup\]/is" => '\textsuperscript{$1}',
//			"/\[sub\](.*?)\[\/sub\]/is" => '\textsubscript{$1}',
//			"/\(([^)]*?)\)\/\/\(([^)]*?)\)/is" => '\frac{$1}{$2}',
//			"/\(([^)]*?)\)\/\/(\d+)/is" => '\frac{$1}{$2}',
//			"/(\d+)\/\/\(([^)]*?)\)/is" => '\frac{$1}{$2}',
//			"/\(([^(]*?)\/\/([^)]*?)\)/is" => '\frac{$1}{$2}',
//			"/(\d+)\/\/(\d+)/is" => '\frac{$1}{$2}',
//			"/(\*)/is" => '\\times ',
//			"/(\/)/is" => '\\div ',
//			"/(!=)/is" => '\\neq ',
//			"/(\.\.\.)/is" => '\\dots '
//		);
//
////		$formula = rawurldecode(preg_replace("/^(\/assets(\/|\.php\?))/", "", $_SERVER["REQUEST_URI"]));
//		$string = preg_replace(array_keys($bbcode), array_values($bbcode), $formula);
//		$filename = "/" . $folder . "/" . sha1($formula) . ".svg"; # Cache the formula
//
//		if (!file_exists($basepath . $filename)) {
////			error_log("Create SVG Formula: " . $formula);
//			if (empty($cfg["remote"])) {
//				file_put_contents($basepath . $filename, file_get_contents("http://localhost:16000/?" . str_replace(" ", "%20", rawurlencode($string))));
//			} else {
//				// TODO: grab from a working server...
//				file_put_contents($basepath . $filename, file_get_contents("http://infuze-pc:16000/?" . str_replace(" ", "%20", rawurlencode($string))));
//			}
//		}
//		return $filename;
//	}
//
//	header("Location: " . get_formula(rawurldecode(preg_replace("/^(\/assets\.php\?)/", "", $GLOBALS["request_uri"]))), true, 302);
//	exit;
}

/* * *****************************************************************************
 * ASSET FILE
 * **************************************************************************** */

$path = array("", "../defaults/");
if (preg_match("/^\/assets(?:\/|\.php\?)(\d+)\/(?:.*\/)?(.*?)(?:\?_=\d+)?$/", $request_uri, $tmp)) {
	$node_id = $tmp[1];
	$file = $tmp[2];
} else if ($referer) {
	parse_str(parse_url($referer, PHP_URL_QUERY), $query);
	if (isset($query["id"])) {
		$node_id = $query["id"];
	}
}

//header("Content-Type: text/plain", true);
//print basename($_SERVER["QUERY_STRING"]) . "\n" . $node_id . "\n" . $file . "\n";
//print_r($_SERVER);
//print_r($_GET);
//die;

if (isset($node_id)) {
	array_unshift($path, $node_id . "/");
	require_once "include/db.php";
	$result = array();
	$stmt = $mysqli->prepare("SELECT parent.`node_id` FROM (SELECT @r AS _id,@level := @level + 1 AS level,(SELECT @r := NULLIF(`node_parent`, 0) FROM `ic_node` hn WHERE `node_id` = _id) FROM (SELECT @r := ?, @level := 0) vars, `ic_node` child WHERE @r IS NOT NULL) child JOIN `ic_node` parent ON parent.`node_id` = child._id ORDER BY level DESC");
	$stmt->bind_param("i", $node_id);
	$stmt->execute();
	$stmt->store_result();
	$stmt->bind_result($id);
	while ($stmt->fetch()) {
		array_unshift($path, $id . "/");
	}
	$stmt->close();
}

function sendAssetPath($filename) {
	if (file_exists($filename)) {
		header("Location: /" . $filename, true, 302);
		exit;
	}
}

define("TYPE_UNKNOWN", 0);
define("TYPE_IMAGE", 1);
define("TYPE_SOUND", 2);
define("TYPE_CSS", 3);
define("TYPE_WOFF", 4);
define("TYPE_JS", 5);

$ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
$dotext = ".$ext";
switch ($ext) {
	case "png":
	case "svg":
	case "jpg":
	case "gif":
		$type = TYPE_IMAGE;
		break;
	case "mp3":
	case "ogg":
		$type = TYPE_SOUND;
		break;
	case "css":
		$type = TYPE_CSS;
		break;
	case "js":
		$file = str_replace(".min.js", ".js", $file);
		$type = TYPE_JS;
		break;
	case "woff":
	case "woff2":
		$type = TYPE_WOFF;
		break;
	default:
		$type = TYPE_UNKNOWN;
		break;
}

if ($type === TYPE_CSS) {
	require_once "include/scss.php";
}

foreach ($path as $pathname) {
	$assetfile = "assets/$pathname" . $file;
	if ($type === TYPE_IMAGE) {
		foreach (array(".png", ".svg", ".jpg", ".gif") as $extension) {
			sendAssetPath(str_replace($dotext, $extension, $assetfile));
		}
	} elseif ($type === TYPE_CSS) {
		if (update_scss("assets/$pathname" . str_replace($dotext, ".scss", $file))) {
			sendAssetPath($assetfile);
		}
	} elseif ($type === TYPE_WOFF) {
		foreach (array(".woff", ".woff2") as $extension) {
			sendAssetPath(str_replace($dotext, $extension, $assetfile));
		}
	} else {
		sendAssetPath($assetfile);
	}
}

if ($type === TYPE_SOUND) {
	// If we get here then the sound hasn't been found - supply a default one
	sendAssetPath("defaults/default.$ext");
}

//error_log("error: " . $file);
header("HTTP/1.0 404 Not Found");

// No closing delimiter as this is AJAX only