<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

$allow = array(
	"192.168.11.150"
);

if (!in_array(filter_input(INPUT_SERVER, "REMOTE_ADDR", FILTER_SANITIZE_STRING), $allow, true)) {
	die;
}

ignore_user_abort(1);

require_once "include/latex.php";

latex("\\\\QNAP-NAS\\Web\\ActivityBuilder\\" . filter_input(INPUT_POST, "filename", FILTER_SANITIZE_STRING), true);
