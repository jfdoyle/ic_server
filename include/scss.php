<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

require_once "css-crush/CssCrush.php";
require_once "scssphp/scss.inc.php";

use Leafo\ScssPhp\Compiler;

/**
 * Build a css file from an scss file if needed
 * @param string $scss_name
 */
function update_scss($scss_name) {
	if (strtolower(pathinfo($scss_name, PATHINFO_EXTENSION)) === "scss") {
		$scss_time = filemtime($scss_name);
		if ($scss_time !== false) {
			$css_name = str_replace(".scss", ".css", $scss_name);
			$css_time = filemtime($css_name);
			if ($css_time === false || $css_time < $scss_time) {
				@unlink($css_name); // Force it to be re-created
				$scssc = new Compiler();
				$scssc->setFormatter("scss_formatter_compressed");
				file_put_contents($css_name, csscrush_string($scssc->compile(file_get_contents($scss_name))));
			}
			return true;
		}
	}
	return false;
}

/**
 * Compile a string of scss into a common css file
 * @global array $cache_md5
 * @param string $basename
 * @param string $scss
 */
function compile_scss($basename, $scss) {
	global $cache_md5;

	$scssc = new Compiler();
	$css = $scssc->compile($scss);
	$files = $scssc->getParsedFiles();
	$hashes = array();
	foreach ($files as $file) {
		$file = preg_replace("/\/[^\/]+\/\.\.\//", "/", str_replace("activity/", "", $file));
		cache_file($file);
		$hashes[$file] = $cache_md5[$file];
	}
	file_put_contents("../_common/css/$basename.md5", json_encode($hashes));
	$output = csscrush_string($css);
	$cache_md5["$basename.css"] = md5($output);
	file_put_contents("../_common/css/$basename.css", $output);
}
