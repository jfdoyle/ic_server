<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

/**
 * Get all organisations
 * @global mysqli $mysqli
 * @return array
 */
function get_group($group_id = null, $users = false) {
	global $mysqli, $user_group;

	$id = $name = $node = null;
	$result = array();
	if (!is_root()) {
		$group_id = $user_group;
	}
	if (empty($group_id) || $group_id === -1) {
		$stmt = $mysqli->prepare("SELECT `group_id`, `group_name`, `group_node` FROM `ic_group`");
	} else {
		$stmt = $mysqli->prepare("SELECT `group_id`, `group_name`, `group_node` FROM `ic_group` WHERE `group_id` IN (?)");
		$stmt->bind_param("s", $user_group);
	}
	$stmt->execute();
	$stmt->store_result();
	$stmt->bind_result($id, $name, $node);
	while ($stmt->fetch()) {
		$organisation = array();
		$organisation["id"] = $id;
		$organisation["name"] = $name;
		$organisation["node"] = $node;
		if ($users) {
			$organisation["users"] = get_users($id);
		}
		$result[] = $organisation;
	}
	$stmt->close();
	return $result;
}

/**
 * Get all users for an organisation
 * @global mysqli $mysqli
 * @param int $group_id
 * @return array
 */
function get_users($group_id) {
	global $mysqli;

	$id = $email = $perms = $group = $seen = $update = null;
	$result = array();
	if (empty($group_id) || $group_id === -1) {
		$stmt = $mysqli->prepare("SELECT `user_id`, `user_email`, `user_permission`, `user_group`, DATE_FORMAT(`user_seen`,'%Y-%m-%dT%TZ'), DATE_FORMAT(`user_update`,'%Y-%m-%dT%TZ') FROM `ic_user`");
	} else {
		$stmt = $mysqli->prepare("SELECT `user_id`, `user_email`, `user_permission`, `user_group`, DATE_FORMAT(`user_seen`,'%Y-%m-%dT%TZ'), DATE_FORMAT(`user_update`,'%Y-%m-%dT%TZ') FROM `ic_user` WHERE `user_group` = ?");
		$stmt->bind_param("i", $group_id);
	}
	$stmt->execute();
	$stmt->store_result();
	$stmt->bind_result($id, $email, $perms, $group, $seen, $update);
	while ($stmt->fetch()) {
		$result[] = array(
			"id" => $id,
			"email" => $email,
			"permission" => $perms,
			"group" => $group,
			"seen" => $seen,
			"update" => $update
		);
	}
	$stmt->close();
	return $result;
}
