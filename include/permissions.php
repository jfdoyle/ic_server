<?php

/**
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */
define('PERM_ROOT', 0x0001); // can edit and view every organisation

define('PERM_OWNER', 0x0002); // can edit and view everything in organisation
define('PERM_MANAGE', 0x0004); // can edit projects but not users in organisation

define('PERM_VIEW', 0x0008); // can view activities

define('PERM_NODE_CREATE', 0x0010); // can create activities
define('PERM_NODE_DELETE', 0x0020); // can delete activities
define('PERM_NODE_CONTENT', 0x0040); // can edit content
define('PERM_NODE_THEME', 0x0080); // can edit theme

define('PERM_EDIT', 0xFFFF); // Can do more than just view things

function is_root() {
	global $user_perms;
	return ($user_perms & PERM_ROOT);
}

function is_admin() {
	global $user_perms;
	return ($user_perms & PERM_ROOT) || ($user_perms & PERM_OWNER);
}

function has_perm($perm) {
	global $user_perms;
	return is_root() || ($user_perms & $perm);
}

function in_group($id) {
	global $user_group;
	return is_root() || preg_match("/[^,]" . $id . "[$,]/", $user_group);
}

// No closing delimiter as this is included only