<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Delete a node
 * Toggles the delete flag
 */
if ($cfg["read_only"]) {
	return array("error" => "Server is read-only");
}
$node_id = filter_post("id", FILTER_VALIDATE_INT);
if (has_node($node_id, $user_id) && has_perm(PERM_NODE_DELETE)) {
	require_once("include/node/node_flags.php");
	// error_log("Delete " . ACTIVITY_FLAG_DELETED);
	$stmt = $mysqli->prepare("UPDATE `ic_node` SET `node_flags` = `node_flags` ^ " . ACTIVITY_FLAG_DELETED . ", `node_time` = UTC_TIMESTAMP() WHERE `node_id` = ? LIMIT 1");
	$stmt->bind_param("i", $node_id);
	$stmt->execute();
	if ($stmt->affected_rows) {
		$result = get_node($node_id);
		log_action($node_id, "delete");
	} else {
		$result = array("error" => "Unable to delete node");
	}
	$stmt->close();
	return $result;
}
return array("error" => "No permission");
