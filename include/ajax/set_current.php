<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Set watched node
 */
$node_id = filter_post("id", FILTER_VALIDATE_INT);
if (has_node($node_id, $user_id)) {
	$result = array();
	$email = null;

	$stmt = $mysqli->prepare("UPDATE `ic_user` SET `user_current` = ? WHERE `user_id` = ? LIMIT 1");
	$stmt->bind_param("ii", $node_id, $user_id);
	$stmt->execute();
	$stmt->prepare("SELECT `user_email` "
			. "FROM `ic_user` "
			. "WHERE `user_current` = ? "
			. "AND `user_seen` > UTC_TIMESTAMP() - INTERVAL 5 MINUTE");
	$stmt->bind_param("i", $node_id);
	$stmt->execute();
	$stmt->store_result();
	$stmt->bind_result($email);
	while ($stmt->fetch()) {
		$result[] = $email;
	}
	$stmt->close();
	return $result;
}
return array("error" => "No permission");
