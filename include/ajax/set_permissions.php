<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Set another user's permissions.
 * Cannot set own permissions.
 */

if (is_admin()) {
	if ($cfg["read_only"]) {
		return array("error" => "Server is read-only");
	}
	$user_id = filter_post("id", FILTER_VALIDATE_INT);
	if ($user_id === $user_id) {
		return array("error" => "Cannot change own permissions");
	}
	$permissions = filter_post("permissions", FILTER_VALIDATE_INT);
	if (!is_root()) {
		$permissions &= ~PERM_ROOT;
	}
	$stmt = $mysqli->prepare("UPDATE `ic_user` SET `user_permission` = ? WHERE `user_id` = ? LIMIT 1");
	$stmt->bind_param("ii", $permissions, $user_id);
	$stmt->execute();
	$changed = $stmt->affected_rows;
	$stmt->close();
	return $changed > 0 ? array("success" => "Permissions changed") : array("error" => "Cannot change permissions");
}
return array("error" => "No permission");
