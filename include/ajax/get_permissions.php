<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Get (logged in) user's permissions
 */
return array("permission" => $user_perms);
