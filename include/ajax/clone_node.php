<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Clone a node
 */

/**
 * Clone a node
 * @param int $node_id
 * @return boolean
 */
function clone_node($node_id, $parent_id = -1, $index = -1, $rename = true, $skip_result = false) {
	global $mysqli, $cfg, $user_id;

	if ($cfg["read_only"]) {
		return array("error" => "Server is read-only");
	}

	$result = $children = $type = null;
	if ($parent_id == -1) {
		$stmt = $mysqli->prepare("SELECT parent.`node_id`, parent.`node_children`, child.`node_type` FROM `ic_node` AS parent LEFT JOIN `ic_node` AS child ON parent.`node_id` = child.`node_parent` WHERE child.`node_id` = ? LIMIT 1;");
		$stmt->bind_param("i", $node_id);
	} else {
		$rename = false;
		$stmt = $mysqli->prepare("SELECT parent.`node_id`, parent.`node_children`, child.`node_type` FROM `ic_node` AS parent, `ic_node` AS child WHERE parent.`node_id` = ? AND child.`node_id` = ? LIMIT 1;");
		$stmt->bind_param("ii", $parent_id, $node_id);
	}
	$stmt->execute();
	$stmt->store_result();
	$stmt->bind_result($parent_id, $children, $type);
	$stmt->fetch();
	// TODO: Better copy naming - need to make sure we don't have duplicate names
	$stmt->prepare("INSERT INTO `ic_node` (`node_parent`, `node_creator`, `node_created`, `node_time`, `node_flags`, `node_type`, `node_name`, `node_html`, `node_data`, `node_tags`, `node_build`, `node_children`) "
			. "SELECT ?, ?, UTC_TIMESTAMP(), UTC_TIMESTAMP(), `node_flags`, `node_type`, " . ($rename ? "CONCAT(`node_name`, ' - copy')" : "`node_name`") . ", `node_html`, `node_data`, `node_tags`, `node_build`, `node_children` FROM `ic_node` "
			. "WHERE `node_id` = ? LIMIT 1;");
	$stmt->bind_param("iii", $parent_id, $user_id, $node_id);
	$stmt->execute();
	$new_id = $stmt->insert_id;
	$stmt->close();
	if ($new_id) {
		log_action($new_id, "clone", $node_id);
		$mysqli->query("UPDATE `ic_node` SET `node_html` = REPLACE(`node_html`, '../assets/" . intval($node_id) . "/', '../assets/$new_id/') WHERE `node_id` = '$new_id' LIMIT 1");
		if (($children_list = explode(",", $children)) === FALSE) {
			$children_list = array();
		}
		if ($index == -1) {
			$index = array_search($node_id, $children_list);
			if ($index != false) {
				$index += 1;
			}
		}
		if ($index == false || $index > count($children_list)) {
			$children_list[] = $new_id;
		} else {
			array_splice($children_list, $index, 0, $new_id);
		}
		$children = implode(",", $children_list);
		$mysqli->query("UPDATE `ic_node` SET `node_children` = '$children', `node_time` = UTC_TIMESTAMP() WHERE `node_id` = '$parent_id' LIMIT 1");
		if (!is_dir("assets")) {
			mkdir("assets");
		}
		$source = "assets/$node_id";
		$dest = "assets/$new_id";
		if (is_dir($source)) {
			mkdir($dest); // Should never exist already, so letting it report the error
			$dir = dir($source);
			while (false !== ($entry = $dir->read())) {
				if ($entry != "." && $entry != ".." && is_file("$source/$entry")) {
					copy("$source/$entry", "$dest/$entry");
				}
			}
			$dir->close();
		}
		if ($type < 10) {
			$children_result = $mysqli->query("SELECT `node_children` FROM `ic_node` WHERE `node_id` = '$new_id' LIMIT 1;");
			if ($children_result) {
				$children_list = explode(",", $children_result->fetch_array()[0]);
				$mysqli->query("UPDATE `ic_node` SET `node_children` = '' WHERE `node_id` = '$new_id' LIMIT 1");
				foreach ($children_list as $child_id) {
					clone_node($child_id, $new_id, -1, false, true);
				}
			}
		}
		if ($skip_result) {
			$result = true;
		} else {
			$result = get_node(array($new_id, $parent_id));
		}
	} elseif ($mysqli->errno) {
		$result = array("error" => "Unable to clone activity");
	} else {
		$result = array("error" => "Nothing changed");
	}
	return $result;
}

$node_id = filter_post("id", FILTER_VALIDATE_INT);
if (has_node($node_id, $user_id) && has_perm(PERM_NODE_CREATE)) {
	$parent = filter_post("parent", FILTER_VALIDATE_INT);
	$index = filter_post("pos", FILTER_VALIDATE_INT);
	return clone_node($node_id, is_numeric($parent) ? $parent : -1, is_numeric($index) ? $index : -1);
}
return array("error" => "No permission");
