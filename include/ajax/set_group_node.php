<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Set a group's root node
 */
if ($cfg["read_only"]) {
	return array("error" => "Server is read-only");
}
if (is_root()) {
	$group_id = filter_post("id", FILTER_VALIDATE_INT);
	$node_id = filter_post("node", FILTER_VALIDATE_INT);
	if ($node_id) {
		$stmt = $mysqli->prepare("UPDATE `ic_group` SET `group_node` = ? WHERE `group_id` = ? LIMIT 1");
		$stmt->bind_param("ii", $node_id, $group_id);
		$stmt->execute();
		$changed = $stmt->affected_rows;
		$stmt->close();
	}
	return !empty($changed) ? array("success" => "Node changed") : array("error" => "Cannot change node");
}
return array("error" => "No permission");
