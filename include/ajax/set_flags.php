<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Rename a node
 */
if ($cfg["read_only"]) {
	return array("error" => "Server is read-only");
}
$node_id = filter_post("id", FILTER_VALIDATE_INT);
if (has_node($node_id, $user_id)) {
	$flags = filter_post("flags", FILTER_VALIDATE_INT);
	// TODO: Check permission for setting flags such as DISABLED
	$result = false;
	$stmt = $mysqli->prepare("UPDATE `ic_node` SET `node_flags` = ?, `node_time` = UTC_TIMESTAMP() WHERE `node_id` = ? AND `node_flags` != ? LIMIT 1");
	$stmt->bind_param("iii", $flags, $node_id, $flags);
	$stmt->execute();
	if ($stmt->affected_rows) {
		log_action($node_id, "flag");
		$stmt->prepare("SELECT `node_parent`, BIT_OR(`node_flags`) FROM `ic_node` WHERE `node_parent` = (SELECT `node_parent` FROM `ic_node` WHERE `node_id` = ?)");
		while ($node_id) {
			$stmt->bind_param("i", $node_id);
			$stmt->execute();
			$stmt->store_result();
			$stmt->bind_result($node_id, $flags);
			$stmt->fetch();
			if ($node_id) {
				$mysqli->query("UPDATE `ic_node` SET `node_flags` = $flags, `node_time` = UTC_TIMESTAMP() WHERE `node_id` = $node_id AND `node_flags` != $flags");
			}
		}
		$result = array("success" => "Activity updated");
	} elseif ($stmt->errno) {
		json_die(array("error" => "Unable to update activity"));
	} else {
		$result = array("success" => "Nothing changed");
	}
	$stmt->close();
	return $result;
}
return array("error" => "No permission");
