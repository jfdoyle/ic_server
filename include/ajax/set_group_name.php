<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Rename a group
 */
if ($cfg["read_only"]) {
	return array("error" => "Server is read-only");
}
if (is_root()) {
	$group_id = filter_post("id", FILTER_VALIDATE_INT);
	$name = filter_post("name", FILTER_UNSAFE_RAW);
	if (is_root() && $name) {
		$stmt = $mysqli->prepare("UPDATE `ic_group` SET `group_name` = ? WHERE `group_id` = ? LIMIT 1");
		$stmt->bind_param("si", $name, $group_id);
		$stmt->execute();
		$changed = $stmt->affected_rows;
		$stmt->close();
	}
	return !empty($changed) ? array("success" => "Node changed") : array("error" => "Cannot change node");
}
return array("error" => "No permission");
