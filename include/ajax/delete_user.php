<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Delete a user
 * Changes the email address to something impossible to match (prefixes "DELETED: ") to prevent login
 */
if ($cfg["read_only"]) {
	return array("error" => "Server is read-only");
}
if (is_admin()) {
	$id = filter_post("id", FILTER_VALIDATE_INT);
	if ($id === $user_id) {
		return array("error" => "Cannot delete yourself");
	}
	$result = array();
	if (is_root()) {
		$stmt = $mysqli->prepare("UPDATE `ic_user` SET `user_email` = CONCAT('DELETED: ', `user_email`), `user_update` = UTC_TIMESTAMP() WHERE `user_id` = ? LIMIT 1");
		$stmt->bind_param("i", $id);
	} else {
		$stmt = $mysqli->prepare("UPDATE `ic_user` SET `user_email` = CONCAT('DELETED: ', `user_email`), `user_update` = UTC_TIMESTAMP() WHERE `user_id` = ? AND `user_group` = ? LIMIT 1");
		$stmt->bind_param("ii", $id, $user_group);
	}
	$stmt->execute();
	if ($stmt->affected_rows) {
		$result["success"] = true;
	} else {
		$result["error"] = "Unable to delete";
	}
	$stmt->close();
	return $result;
}
return array("error" => "No permission");
