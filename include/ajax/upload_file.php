<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Upload a file.
 */
if ($cfg["read_only"]) {
	return array("error" => "Server is read-only");
}
$node_id = filter_post("id", FILTER_VALIDATE_INT);
if (has_node($node_id, $user_id)) {
	$name = filter_post("name", FILTER_SANITIZE_STRING);
	$content = filter_post("content", FILTER_UNSAFE_RAW);
	return upload_file($node_id, $name, $content);
}
return array("error" => "No permission");
