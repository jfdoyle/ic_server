<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Move a node
 */
if ($cfg["read_only"]) {
	return array("error" => "Server is read-only");
}
$node_id = filter_post("id", FILTER_VALIDATE_INT);
$parent_id = filter_post("parent", FILTER_VALIDATE_INT);
if (has_node($parent_id, $user_id) && has_node($node_id, $user_id) && has_perm(PERM_NODE_CREATE)) {
	$pos = filter_post("pos", FILTER_VALIDATE_INT);
	// TODO: Handle moving to a target without a position - append to end
	$old_parent_id = $old_children = $new_parent_id = $new_children = null;
	$result = false;

//	error_log("move_node(" . $node_id . ", " . $parent . ", " . $pos . ")");
//	$query = "SET @id := $node_id, @new_parent := $parent, @new_pos := $pos;" .
//			"SELECT `node_parent`, `node_num` INTO @old_parent, @old_pos FROM `ic_node` WHERE `node_id` = @id LIMIT 1;" .
//			"SELECT COUNT(`node_num`) + 1 INTO @node_count FROM `ic_node` WHERE `node_parent` = @old_parent;" .
//			"SET @new_pos := IF(@new_pos > 0, @new_pos, @node_count);" .
//			"UPDATE `ic_node` SET `node_num` = `node_num` - 1 WHERE `node_parent` = @old_parent AND `node_num` > @old_pos;" .
//			"UPDATE `ic_node` SET `node_num` = `node_num` + 1 WHERE `node_parent` = @new_parent AND `node_num` >= @new_pos;" .
//			"UPDATE `ic_node` SET `node_parent` = @new_parent, `node_num` = @new_pos WHERE `node_id` = @id LIMIT 1;";
//	$mysqli->query($query);
//	error_log($mysqli->errno . "; " . $mysqli->error);

	$stmt = $mysqli->prepare("SELECT `node_id`, `node_children` FROM `ic_node` WHERE `node_id` = (SELECT `node_parent` FROM `ic_node` WHERE `node_id` = ?) LIMIT 1");
	$stmt->bind_param("i", $node_id);
	$stmt->execute();
	$stmt->store_result();
	$stmt->bind_result($old_parent_id, $old_children);
	$stmt->fetch();
	$stmt->prepare("SELECT `node_id`, `node_children` FROM `ic_node` WHERE `node_id` = ? LIMIT 1");
	$stmt->bind_param("i", $parent_id);
	$stmt->execute();
	$stmt->store_result();
	$stmt->bind_result($new_parent_id, $new_children);
	$stmt->fetch();
	$stmt->close();
	// remove then insert into array
	if (empty($old_children) || ($old_children_list = explode(",", $old_children)) === FALSE) {
		$old_children_list = array();
	}
	while (($key = array_search($node_id, $old_children_list)) !== false) {
		array_splice($old_children_list, $key, 1);
	}
	if ($old_parent_id === $new_parent_id) {
		if (!empty($old_parent_id)) {
			array_splice($old_children_list, $pos, null, $node_id);
			$old_children = preg_replace("/(^0$|^0,|,0$|(?<=,)0,)/", "", implode(",", $old_children_list));
			$mysqli->query("UPDATE `ic_node` SET `node_children` = '$old_children', `node_time` = UTC_TIMESTAMP() WHERE `node_id` = '$old_parent_id' LIMIT 1");
		}
	} else {
		if (!empty($old_parent_id)) {
			$old_children = preg_replace("/(^0$|^0,|,0$|(?<=,)0,)/", "", implode(",", $old_children_list));
			$mysqli->query("UPDATE `ic_node` SET `node_children` = '$old_children', `node_time` = UTC_TIMESTAMP() WHERE `node_id` = '$old_parent_id' LIMIT 1");
		}
		if (empty($old_parent_id) || !$mysqli->errno) {
			if (empty($new_parent_id)) {
				$new_parent_id = 0;
			} else {
				if (empty($new_children) || ($new_children_list = explode(",", $new_children)) === FALSE) {
					$new_children_list = array();
				}
				array_splice($new_children_list, $pos, null, $node_id);
				$new_children = preg_replace("/(^0$|^0,|,0$|(?<=,)0,)/", "", implode(",", $new_children_list));
				$mysqli->query("UPDATE `ic_node` SET `node_children` = '$new_children', `node_time` = UTC_TIMESTAMP() WHERE `node_id` = '$new_parent_id' LIMIT 1");
			}
			if (empty($new_parent_id) || !$mysqli->errno) {
				$mysqli->query("UPDATE `ic_node` SET `node_parent` = '$new_parent_id', `node_time` = UTC_TIMESTAMP() WHERE `node_id` = '$node_id' LIMIT 1");
			}
		}
	}
	if ($mysqli->errno) {
		$result = array("error" => "Unable to move activity");
	} else {
		if ($old_parent_id === $new_parent_id) {
			log_action($node_id, "move", "$node_id -> $pos");
			$result = get_node(array($node_id, $old_parent_id));
		} else {
			log_action($node_id, "move", "$node_id -> $new_parent_id.$pos from $old_parent_id");
			$result = get_node(array($node_id, $old_parent_id, $new_parent_id));
		}
	}
	return $result;
}
return array("error" => "No permission");
