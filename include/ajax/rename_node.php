<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Rename a node
 */
if ($cfg["read_only"]) {
	return array("error" => "Server is read-only");
}
$node_id = filter_post("id", FILTER_VALIDATE_INT);
if (has_node($node_id, $user_id)) {
	$result = false;
	$name = filter_post("name", FILTER_SANITIZE_STRING);
	$stmt = $mysqli->prepare("UPDATE `ic_node` SET `node_name` = ?, `node_time` = UTC_TIMESTAMP() WHERE `node_id` = ? LIMIT 1");
	$stmt->bind_param("si", $name, $node_id);
	$stmt->execute();
	if ($stmt->affected_rows) {
		log_action($node_id, "rename");
		$result = array("success" => "Node updated");
	} elseif ($stmt->errno) {
		$result = array("error" => "Unable to update activity");
	} else {
		$result = array("success" => "Nothing changed");
	}
	$stmt->close();
	return $result;
}
return array("error" => "No permission");
