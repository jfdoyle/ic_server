<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Find a node
 */
$node_path = filter_post("path", FILTER_SANITIZE_STRING);
if (!empty($node_path)) {
	require_once(INCLUDE_DIR . "node/node_flags.php");

	$node_id = $node_name = null;
	$result = array();
	$stmt = $mysqli->prepare("SELECT `node_id`, `node_name` FROM `ic_node` WHERE (`node_name` = ? OR `node_html` LIKE ?) AND  `node_flags` & " . ACTIVITY_FLAG_DELETED . " = 0");
	$content = "%$node_path%";
	$stmt->bind_param("ss", $node_path, $content);
	$stmt->execute();
	$stmt->store_result();
	$stmt->bind_result($node_id, $node_name);
	while ($stmt->fetch()) {
		if (has_node($node_id, $user_id)) {
			$result[$node_id] = $node_name;
		}
	}
	$stmt->close();
	return $result;
}
return array("error" => "No permission");
