<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Backup the server (admin only).
 */
if (is_root()) {
	require_once "backup.php";
	return backup();
}
return array("error" => "No permission");
