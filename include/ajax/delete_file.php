<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Delete a file.
 */
if ($cfg["read_only"]) {
	return array("error" => "Server is read-only");
}
$node_id = filter_post("id", FILTER_VALIDATE_INT);
if (has_node($node_id, $user_id) && has_perm(PERM_NODE_THEME | PERM_NODE_CONTENT)) {
	$name = filter_post("name", FILTER_SANITIZE_STRING);
	if (preg_match("/[\/\\\?\*:]/", $name)) {
		return array("error" => "Bad filename");
	}
	if (is_dir("assets/$node_id") && is_file("assets/$node_id/$name")) {
		log_action($node_id, "delete", $name);
		unlink("assets/$node_id/$name");
		update_node_timestamp($node_id);
		return array("success" => "File deleted");
	}
	return array("error" => "File not found");
}
return array("error" => "No permission");
