<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Get all parent node ids of a node
 */
$node_id = filter_post("id", FILTER_VALIDATE_INT);
if (has_node($node_id, $user_id)) {
	return get_node_parent_list($node_id);
}
return array();
