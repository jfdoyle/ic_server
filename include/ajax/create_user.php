<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Create a user
 * Generates a password if one is not supplied
 * Emails the new user with their password
 */
if ($cfg["read_only"]) {
	return array("error" => "Server is read-only");
}
if (is_admin()) {
	$organisation = filter_post("group", FILTER_UNSAFE_RAW);
	$email = filter_post("email", FILTER_SANITIZE_EMAIL);
	$permissions = filter_post("permissions", FILTER_VALIDATE_INT);
	$password = filter_post("password", FILTER_UNSAFE_RAW);
	$result = array();
	$salt = hash("sha512", uniqid(mt_rand(1, mt_getrandmax()), true));
	if (empty($password)) {
		$chars = str_shuffle("abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789"); // Missing O0lI1 due to easily mixing them up
		for ($i = rand(0, $length); $i; $i--) {
			$chars = str_shuffle($chars); // Make sure it's well mixed
		}
		$password = substr($chars, 0, $length);
	}
	if (!is_root()) {
		$permissions &= ~PERM_ROOT;
		$organisation = $user_group;
	}
	$hash = hash("sha512", $password . $salt);
	$stmt = $mysqli->prepare("INSERT INTO `ic_user` (`user_group`, `user_email`, `user_password`, `user_salt`, `user_permission`, `user_update`) VALUES (?, ?, ?, ?, ?, UTC_TIMESTAMP())");
	$stmt->bind_param("ssssi", $organisation, $email, $hash, $salt, $permissions);
	$stmt->execute();
	$user_id = $stmt->insert_id;
	if ($user_id) {
		// Send email containing password
		$subject = "Welcome to Infuze Creator";
		$message = "Hello,\r\nSomebody signed you up to Infuze Creator.\r\nPlease login using your email address and this password: " . $password;
		$headers = "From: no-reply@infuze.uk\r\n" .
				"Reply-To: no-reply@infuze.uk\r\n" .
				"X-Mailer: PHP/" . phpversion();
		try {
			mail($email, $subject, $message, $headers);
		} catch (Exception $e) {
			error_log($e->getMessage());
		}
		$result = array("success" => "User created", "id" => $user_id);
	} else {
		$result["error"] = $stmt->error;
	}
	$stmt->close();
	return $result;
}
return array("error" => "No permission");
