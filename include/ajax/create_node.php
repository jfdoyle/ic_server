<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Create a node
 */
if ($cfg["read_only"]) {
	return array("error" => "Server is read-only");
}
$parent_id = filter_post("parent", FILTER_VALIDATE_INT);
if (has_node($parent_id, $user_id) && has_perm(PERM_NODE_CREATE) && array_key_exists("data", $post) && is_array($post["data"])) {
	$data = $post["data"];
	$files = array_key_exists("files", $post) ? $post["files"] : null;
	$result = false;
	if (array_key_exists("_type", $data)) {
		$flags = array_key_exists("_flags", $data) ? $data["_flags"] : 0;
		$type = $data["_type"];
		$name = array_key_exists("_name", $data) ? $data["_name"] : "";
		$html = array_key_exists("_html", $data) ? $data["_html"] : "";
		$children = array_key_exists("_children", $data) ? implode(",", $data["_children"]) : "";
		$tags = array_key_exists("_tags", $data) ? implode(",", $data["_tags"]) : "";
		$build = array_key_exists("_build", $data) ? $data["_build"] : "";
		foreach (array_keys($data) as $key) {
			if (substr($key, 0, 1) === "_") {
				unset($data[$key]);
			}
		}
		// make sure we've removed the extra stuff before encoding
		$json = json_encode(array_unique_recursive_distinct($data, get_node_parent_data($parent_id, true)));
		$stmt = $mysqli->prepare("INSERT INTO `ic_node` (`node_parent`, `node_creator`, `node_created`, `node_time`, `node_flags`, `node_type`, `node_name`, `node_html`, `node_children`, `node_data`, `node_tags`, `node_build`) "
				. "VALUES (?, ?, UTC_TIMESTAMP(), UTC_TIMESTAMP(), ?, ?, ?, ?, ?, ?, ?, ?)");
		$stmt->bind_param("iiiissssss", $parent_id, $user_id, $flags, $type, $name, $html, $children, $json, $tags, $build);
		$stmt->execute();
		$node_id = $stmt->insert_id;
		$stmt->close();
		if ($node_id) {
			$mysqli->query("UPDATE `ic_node` SET `node_children` = IF(`node_children`='', '$node_id', CONCAT_WS(',', `node_children`, '$node_id')), `node_time` = UTC_TIMESTAMP() WHERE `node_id` = '$parent_id' LIMIT 1");
			// update_node_timestamp($parent);

			log_action($node_id, "add");
			if (!empty($files)) {
				foreach ($files as $name => $content) {
					upload_file($node_id, $name, $content);
				}
			}
			$result = array(
				get_node($node_id)[0],
				get_node($parent_id)[0]
			);
		} else {
			$result = array("error" => "Unable to create node");
		}
	}
	return $result;
}
return array("error" => "No permission");
