<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Get information about a group
 */
$group_id = filter_post("id", FILTER_VALIDATE_INT);
if (!is_root() || !$group_id) {
	return array_merge(array(
		"permission" => $user_perms,
		"email" => $user_email,
		"backup" => (bool) (is_root() && $cfg["backup"]),
		"replicate" => (bool) (is_root() && $cfg["replicate"] && $cfg["replicate_host"])
			), get_group(is_root() ? -1 : $user_group));
}
return get_group($group_id);
