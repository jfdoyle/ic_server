<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Get a specific node.
 * If no specific node is requested then get the root node.
 * Admin users get the true root, otherwise it will depend on their group membership.
 */
require_once INCLUDE_DIR . "node/get_node.php";

if (array_key_exists("id", $post)) {
	if (is_int($post["id"])) {
		$node_id = filter_post("id", FILTER_VALIDATE_INT);
	} else {
		$node_id = filter_post("id", FILTER_VALIDATE_INT, array("flags" => FILTER_REQUIRE_ARRAY));
	}
	if (!empty($node_id)) {// && has_node($id, $user_id)) {
		// TODO: check access on group id
		$children = !empty($post["children"]);
		return get_node($node_id, $children);
	}
}
if (is_admin()) {
	return get_node(0, false, 0);
}

/**
 * Get an organisation's root node and optionally all of its children
 * @global mysqli $mysqli
 * @param int $group_id to get
 * @param boolean $get_children if we want children data
 * @return array
 */
function get_root_node($group_id, $get_children = false) {
	global $mysqli;

	$id = null;
	$result = array();
	$stmt = $mysqli->prepare("SELECT `group_node` FROM `ic_group` WHERE `group_id` = ?");
	$stmt->bind_param("i", $group_id);
	$stmt->execute();
	$stmt->store_result();
	$stmt->bind_result($id);
	while ($stmt->fetch()) {
		$result = get_node($id, $get_children);
		unset($result["_parent"], $result["_num"]);
	}
	$stmt->close();
	return $result;
}

//$group = filter_post("group", FILTER_VALIDATE_INT);
//if (!is_admin() || !$group) {
$group_id = $user_group;
//}
//$group = get_root_node($group, false);
//return array("_id" => 0, "_children" => array($group["_id"]));

return get_root_node($group, true);
