<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Get all child node ids of a node as an associative array
 */
if (is_admin()) {
	$id = $type = $path = $name = $data = null;
	$result = array();
	$groups = array(filter_post("id", FILTER_VALIDATE_INT));

	do {
		$stmt = $mysqli->prepare("SELECT `node_id`,`node_type`,`node_path`,`node_name`,`node_data` FROM `ic_node` WHERE `node_id` = ? OR `node_parent` = ? ORDER BY `node_id` != ?, `node_num`");
		$group_id = intval(array_shift($groups));
		$stmt->bind_param("iii", $group_id, $group_id, $group_id);
		$stmt->execute();
		$stmt->store_result();
		$stmt->bind_result($id, $type, $path, $name, $data);
		while ($stmt->fetch()) {
			if (!strcmp($type, "group") && $id !== $group_id) {
				$groups[] = $id;
			}
			if ($group_id !== $id && (!empty($path) || !empty($name))) {
				$flag_img = $flag_sound = $flag_video = $flag_css = $flag_js = false;
				foreach (glob("assets/" . $id . "/*.*") as $filename) {
					if (!$flag_img && preg_match("/\.(jpg|jpeg|png|gif|svg)$/i", $filename)) {
						$flag_img = true;
					} elseif (!$flag_sound && preg_match("/\.(mp3|ogg)$/i", $filename)) {
						$flag_sound = true;
					} elseif (!$flag_video && preg_match("/\.(mp4|m4v)$/i", $filename)) {
						$flag_video = true;
					} elseif (!$flag_css && preg_match("/\.(scss|css)$/i", $filename)) {
						$flag_css = true;
					} elseif (!$flag_js && preg_match("/\.(js)$/i", $filename)) {
						$flag_js = true;
					}
				}
				$flags = ($flag_img ? "g" : "") . ($flag_sound ? "s" : "") . ($flag_video ? "v" : "") . ($flag_css ? "c" : "") . ($flag_js ? "j" : "");
				$node = json_decode($data, true);
				$result[" $id"] = "$flags\t$type\t" . (empty($path) ? "" : $path) . "\t" . (empty($name) ? "\"\"" : "\"" . str_replace("\"", "\"\"", $name) . "\"") . "\t" . (array_key_exists("intro", $node) ? "\"" . str_replace("\"", "\"\"", $node["intro"]) . "\"" : "\"\"");
			}
		}
		$stmt->close();
	} while (!empty($groups));
	return $result;
}
return array("error" => "No permission");
