<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Get a list of all builds starting with specific hashes.
 */
if (array_key_exists("id", $post)) {
	if (is_int($post["id"])) {
		$node_id = Array(filter_post("id", FILTER_VALIDATE_INT));
	} else {
		$node_id = filter_post("id", FILTER_VALIDATE_INT, array("flags" => FILTER_REQUIRE_ARRAY));
	}
	// TODO: Check permission
}

$output = array();
if (!empty($node_id) && is_dir("publish")) {
	foreach ($node_id as $id) {
		$crc = dechex(crc32("$id"));
		if (file_exists("publish/$crc.zip")) {
			$output[$id] = array();
			foreach (glob("publish/$crc.*.zip") as $file) {
				$output[$id][] = preg_replace("/publish\/$crc\.(\d+-\d+-\d+)_(\d+)-(\d+)-(\d+).*/", "$1T$2:$3:$4", $file);
			}
		}
	}
}
return $output;
