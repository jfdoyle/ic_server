<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Create a group
 */
if ($cfg["read_only"]) {
	return array("error" => "Server is read-only");
}
if (is_admin()) {
	$node = filter_post("node", FILTER_VALIDATE_INT);
	$name = filter_post("name", FILTER_UNSAFE_RAW);
	$result = array();
	$stmt = $mysqli->prepare("INSERT INTO `ic_group` (`group_node`, `group_name`) VALUES (?, ?)");
	$stmt->bind_param("is", $node, $name);
	$stmt->execute();
	$group_id = $stmt->insert_id;
	if ($group_id) {
		$result = array("success" => "Organisation created", "id" => $group_id);
	} else {
		$result["error"] = $stmt->error;
	}
	$stmt->close();
	return $result;
}
return array("error" => "No permission");
