<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

/**
 * Change your own password
 * @global mysqli $mysqli
 * @param string $old_pass
 * @param string $new_pass
 * @param number $id
 * @return boolean
 */
function change_password(&$old_pass, &$new_pass, $id = null) {
	global $mysqli, $cfg, $user_id, $user_email;

	if ($cfg["read_only"]) {
		return array("error" => "Server is read-only");
	}

	$password = $salt = null;
	$result = array();

	if (is_null($id) || !is_root()) {
		$id = $user_id;
	}

	$stmt = $mysqli->prepare("SELECT `user_password`, `user_salt` FROM `ic_user` WHERE `user_id` = ? LIMIT 1");
	$stmt->bind_param('i', $id);
	$stmt->execute();
	$stmt->store_result();
	$stmt->bind_result($password, $salt);
	$stmt->fetch();
	error_log($id . ": " . $password . ", " . $salt . " = " . hash('sha512', $old_pass . $salt));
	if ((empty($old_pass) && is_root()) || $password == hash('sha512', $old_pass . $salt)) {
		$password = hash("sha512", $new_pass . $salt);
		$stmt->prepare("UPDATE `ic_user` SET `user_password` = ?, `user_update` = UTC_TIMESTAMP() WHERE `user_id` = ? LIMIT 1");
		$stmt->bind_param("si", $password, $id);
		$stmt->execute();
		if ($stmt->affected_rows) {
//			$user_browser = $_SERVER['HTTP_USER_AGENT']; // Get the user-agent string of the user.
			$user_browser = filter_input(INPUT_SERVER, "HTTP_USER_AGENT", FILTER_SANITIZE_STRING); // Get the user-agent string of the user.
			$_SESSION['login_string'] = hash('sha512', $password . $user_browser); // Update the session so we stay logged in
			// Send email containing updated password
			$subject = "Infuze Creator: Password Changed";
			$message = "Hello,\r\nYour password was changed on Infuze Creator.\r\nPlease login using your email address and your new password: " . $new_pass;
			$headers = "From: do_not_reply@infuze.uk\r\n" .
					"Reply-To: do_not_reply@infuze.uk\r\n" .
					"X-Mailer: PHP/" . phpversion();
			try {
				mail($user_email, $subject, $message, $headers);
			} catch (Exception $e) {
				error_log($e->getMessage());
			}
			$result = array("success" => "Password changed");
		} else {
			$result["error"] = $stmt->error;
		}
	} else {
		$result["error"] = "Old password incorrect";
	}
	$stmt->close();
	return $result;
}

$id = filter_post("id", FILTER_VALIDATE_INT);
$old_password = filter_post("oldPassword", FILTER_UNSAFE_RAW);
$new_password = filter_post("newPassword", FILTER_UNSAFE_RAW);

return change_password($old_password, $new_password, $id);
