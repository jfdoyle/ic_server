<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Rename a file.
 */
if ($cfg["read_only"]) {
	return array("error" => "Server is read-only");
}
$node_id = filter_post("id", FILTER_VALIDATE_INT);
if (has_node($node_id, $user_id) && has_perm(PERM_NODE_THEME | PERM_NODE_CONTENT)) {
	$name = filter_post("name", FILTER_SANITIZE_STRING);
	$newname = filter_post("newname", FILTER_SANITIZE_STRING);
	if (preg_match("/[\/\\\?\*:]/", $name) || preg_match("/[\/\\\?\*:]/", $newname)) {
		return array("error" => "Bad filename");
	}
	if (file_exists($newname)) {
		return array("error" => "File exists");
	}
	if (is_dir("assets/$node_id")) {
		rename("assets/$node_id/$name", "assets/$node_id/$newname");
		update_node_timestamp($node_id);
	}
	log_action($node_id, "rename", $newname);
	return array("success" => "File renamed");
}
return array("error" => "No permission");
