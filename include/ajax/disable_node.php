<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Disable a node
 */
if ($cfg["read_only"]) {
	return array("error" => "Server is read-only");
}
$node_id = filter_post("id", FILTER_VALIDATE_INT);
if (has_node($node_id, $user_id) && is_root()) {
	// error_log("Delete " . ACTIVITY_FLAG_IGNORE);
	$stmt = $mysqli->prepare("UPDATE `ic_node` SET `node_flags` = `node_flags` ^ " . ACTIVITY_FLAG_IGNORE . ", `node_time` = UTC_TIMESTAMP() WHERE `node_id` = ? LIMIT 1");
	$stmt->bind_param("i", $node_id);
	$stmt->execute();
	if ($stmt->affected_rows) {
		$result = get_node($node_id);
		log_action($node_id, "disable");
	} else {
		$result = array("error" => "Unable to disable node");
	}
	$stmt->close();
	return $result;
}
return array("error" => "No permission");
