<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Set node data
 * If the node doesn't exist then create it
 */
if ($cfg["read_only"]) {
	return array("error" => "Server is read-only");
}
$node_id = filter_post("id", FILTER_VALIDATE_INT);
if (has_node($node_id, $user_id)) {
	$data = $post["data"];
	$files = array_key_exists("files", $post) && is_array($post["files"]) ? $post["files"] : null;
	$result = false;
	$flags = array_key_exists("_flags", $data) ? $data["_flags"] : 0;
	$type = $data["_type"];
	$name = array_key_exists("_name", $data) ? $data["_name"] : "";
	$html = array_key_exists("_html", $data) ? $data["_html"] : "";
	$children = preg_replace("/(^0$|^0,|,0$|(?<=,)0,)/", "", empty($data["_children"]) ? "" : implode(",", $data["_children"]));
	$tags = empty($data["_tags"]) ? "" : implode(",", $data["_tags"]);
	$build = !empty($data["_build"]) && is_array($data["_build"]) ? json_encode($data["_build"]) : "";
	foreach (array_keys($data) as $key) {
		if (substr($key, 0, 1) === "_") {
			unset($data[$key]);
		}
	}
	// make sure we've removed the extra stuff before encoding
	$json = json_encode($data);
	$stmt = $mysqli->prepare("UPDATE `ic_node` SET `node_flags` = ?, `node_type` = ?, `node_name` = ?, `node_html` = ?, `node_children` = ?, "
			. "`node_data` = ?, `node_tags` = ?, `node_build` = ? WHERE node_id = ? LIMIT 1");
	$stmt->bind_param("iissssssi", $flags, $type, $name, $html, $children, $json, $tags, $build, $node_id);

	$stmt->execute();
	if ($stmt->errno) {
		$error = array("error" => "Unable to update activity");
		json_die($error);
	} else {
		$affected_rows = $stmt->affected_rows;
		if ($affected_rows || !empty($files)) {
			if (empty($files)) {
				update_node_timestamp($node_id, $user_id);
			} else {
				foreach ($files as $name => $content) {
					upload_file($node_id, $name, $content);
				}
			}
			log_action($node_id, "edit");
			$result = get_node($node_id);
			if ($affected_rows) {
				$json = json_encode($result);
				file_put_contents("$root/assets/$node_id.json", $json);
				if ($cfg["backup"]) {
					if (!is_dir("$cfg[backup_dir]$node_id")) {
						mkdir("$cfg[backup_dir]$node_id");
					}
					$date = date("Y-m-d_H-i-s");
					if ($cfg["backup_zip"]) {
						$zip = new ZipArchive;
						$res = $zip->open("$cfg[backup_dir]$node_id/#.json@$date.zip", ZIPARCHIVE::CREATE | ZipArchive::OVERWRITE);
					}
					if ($cfg["backup_zip"] && $res === TRUE) {
						$zip->addFromString("#.json", $json);
						$zip->close();
					} else {
						file_put_contents("$cfg[backup_dir]$node_id/#.json@$date", $json);
					}
				}
			}
		} else {
			$result = array("success" => "Nothing changed");
		}
	}
	$stmt->close();
	return $result;
}
return (include(INCLUDE_DIR . "ajax/create_node.php"));
