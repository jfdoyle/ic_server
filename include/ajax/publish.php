<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Publish a node
 */
$node_id = filter_post("id", FILTER_VALIDATE_INT);
if ($node_id > 0 && is_admin() && has_node($node_id, $user_id)) {
	require_once INCLUDE_DIR . "build.php";

	spl_autoload_register(function($classId) {
		$classIdParts = explode("\\", $classId);
		$classIdLength = count($classIdParts);
		$className = ($classIdParts[$classIdLength - 1]);
		$namespace = ($classIdParts[0]);

		for ($i = 1; $i < $classIdLength - 1; $i++) {
			$namespace .= '/' . $classIdParts[$i];
		}

		if (file_exists(INCLUDE_DIR . "lz-string-php/src/$namespace/$className.php")) {
			include INCLUDE_DIR . "lz-string-php/src/$namespace/$className.php";
		}
	});

	//require_once "include/lz-string-php/src/LZCompressor/LZString.php";
	$html = filter_post("html", FILTER_SANITIZE_STRING);
	return publish($node_id, \LZCompressor\LZString::decompressFromEncodedURIComponent($html));
}
return array("error" => "No permission");
