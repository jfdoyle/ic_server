<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Poll for changes in a list of nodes
 */
$id = array_key_exists("id", $post) ? $post["id"] : 0;
if ($id && count($id)) {
	date_default_timezone_set("Europe/London"); // TODO: Make option?
	$since = gmdate(DATE_ATOM, strtotime(filter_input(INPUT_POST, "since", FILTER_UNSAFE_RAW)));
	$nodes_list = preg_replace("/[^0-9,]/", "", is_array($id) ? implode(",", $id) : $id);
	$query = "SELECT DISTINCT(`node_id`) "
			. "FROM `ic_node` "
			. "WHERE (`node_id` IN ($nodes_list) OR `node_parent` IN ($nodes_list)) "
			. "AND `node_time` > '$since'";
	$nodes = array();
	$result = $mysqli->query($query);
	while ($row = $result->fetch_assoc()) {
		$nodes[] = $row["node_id"];
	}
	$result->free();
	if (count($nodes)) {
		return get_node($nodes);
	}
}
return array("success" => "No changes");
