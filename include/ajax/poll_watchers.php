<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Poll for a list of people watching a specific node
 */
$id = array_key_exists("id", $post) ? $post["id"] : 0;
if ($id && count($id)) {
	$nodes_list = preg_replace("/[^0-9,]/", "", is_array($id) ? implode(",", $id) : $id);
	$query = "SELECT `node_id`, `user_email` "
			. "FROM `ic_node` "
			. "RIGHT JOIN `ic_user` ON `user_current` = `node_id` "
			. "WHERE (`node_id` IN ($nodes_list) OR `node_parent` IN ($nodes_list)) "
			. "AND `user_seen` > UTC_TIMESTAMP() - INTERVAL 5 MINUTE";
	global $mysqli;

	$nodes = array();
	$result = $mysqli->query($query);
	while ($row = $result->fetch_assoc()) {
		$id = $row["node_id"];
		if (empty($nodes[$id])) {
			$nodes[$id] = array();
		}
		$nodes[$id][] = $row["user_email"];
	}
	$result->free();
	if (count($nodes)) {
		return $nodes;
	}
}
return array("success" => "No changes");
