<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Get possible templates for a node
 */
$node_id = filter_post("id", FILTER_VALIDATE_INT);
if ($node_id === 0 || has_node($node_id, $user_id)) {
	return get_node($node_id === 0 ? 1 : $node_id, true, null, true);
}
return array("error" => "No permission");
