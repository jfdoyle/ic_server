<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Gets the user list for a group
 */
if (is_admin()) {
	if (!is_root()) {
		$id = $user_group;
	}
	require_once INCLUDE_DIR . "db.organisation.php";

	return get_users($id);
}
return array("error" => "No permission");
