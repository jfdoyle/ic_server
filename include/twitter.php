<?php

/**
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */
defined("INCLUDE_DIR") || define("INCLUDE_DIR", dirname(__FILE__) . DIRECTORY_SEPARATOR);

require INCLUDE_DIR . "config.default.php";
include INCLUDE_DIR . "config.inc.php";
require INCLUDE_DIR . "twitteroauth/autoload.php";

use Abraham\TwitterOAuth\TwitterOAuth;

function tweet($message) {
	global $connection, $cfg;

	if ($cfg["twitter"]) {
		if (empty($connection)) {
			$connection = new TwitterOAuth($cfg["twitter_ck"], $cfg["twitter_cs"], $cfg["twitter_at"], $cfg["twitter_ats"]);
			// $content = $connection->get("account/verify_credentials");		
		}
		$connection->post("statuses/update", ["status" => "$cfg[twitter_prefix]$message"]);
	}
}
