<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 *
 * Connect to the database, don't expose anything!
 */

if (!isset($cfg)) {
	$cfg = array();
}
require_once "config.default.php";
include_once "config.inc.php";

$mysqli = new mysqli($cfg["mysql_host"], $cfg["mysql_user"], $cfg["mysql_password"], $cfg["mysql_database"], array_key_exists("mysql_port", $cfg) ? $cfg["mysql_port"] : 3306);
if (mysqli_connect_error()) {
	die('Connect Error (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
}
$mysqli->set_charset("utf8");

// No closing delimiter as this is included only