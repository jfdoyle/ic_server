<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

require_once "db.php";

/**
 * Backup the database
 */
function backup_tables($tables = "*") {
	global $mysqli, $cfg;

	//get all of the tables
	if ($tables == "*") {
		$tables = array();
		$result = $mysqli->query("SHOW TABLES");
		while ($row = $result->fetch_row()) {
			$tables[] = $row[0];
		}
	} else {
		$tables = is_array($tables) ? $tables : explode(",", $tables);
	}

	$output = array();

	$output[] = "-- Activity Builder SQL Dump\n-- version 1.0\n-- http://ic.infuze.uk.uk/\n--\n-- Host: $cfg[mysql_host]\n-- Generation Time: " . date(DATE_RFC2822) . "\n-- Server version: $mysqli->server_info\n-- PHP Version: " . phpversion() . "\n";
	$output[] = "SET SQL_MODE = \"NO_AUTO_VALUE_ON_ZERO\";\nSET time_zone = \"+00:00\";\n";
	$output[] = "--\n-- Database: `$cfg[mysql_database]`\n--\n";

	//cycle through
	foreach ($tables as $table) {
//		$return.= 'DROP TABLE ' . $table . ';';
//		$row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE ' . $table));
//		$return.= "\n\n" . $row2[1] . ";\n\n";
		$first = true;
		$length = 0;
		$key_list = array();
		$row_list = array();
		$result = $mysqli->query("SELECT * FROM `$table`");
		while ($row = $result->fetch_assoc()) {
			$cell_list = array();
			foreach ($row as $key => $value) {
				if ($first) {
					$key_list[] = addslashes($key);
				}
				if (is_numeric($value)) {
					$cell_list[] = $value;
				} else {
					$cell_list[] = "'" . str_replace(array("\\", "'", "\n", "\r"), array("\\\\", "\'", "\\n", ""), $value) . "'";
				}
			}
			if ($first) {
				$first = false;
				$output[] = "--\n-- Dumping data for table `$table`\n--\n";
				$output[] = "TRUNCATE TABLE `$table`;\n";
			}
			$line = "(" . implode(", ", $cell_list) . ")";
			$line_length = strlen($line);
			if ($length && $length + $line_length >= 50000) {
				$output[] = implode(",\n", $row_list) . ";\n";
				$row_list = array();
				$length = 0;
			}
			if (empty($row_list)) {
				$output[] = "REPLACE INTO `$table` (`" . implode("`, `", $key_list) . "`) VALUES";
			}
			$length += $line_length;
			$row_list[] = $line;
		}
		$output[] = implode(",\n", $row_list) . ";\n";
	}

	//save file
	return implode("\n", $output) . "\n";
}
