<?php

/**
 * Copyright © 2013-2014 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */
function get_version() {
	return "v0.14.9";
}

function get_buildtime() {
	return "Tue, 15 Aug 2017 09:15:25 GMT";
}
