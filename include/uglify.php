<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

function fnPrefix($name) {
	global $fnCount;
	return "_fn" . ucfirst($name) . "='_" . dechex($fnCount++) . "'";
}

function uglify($filename, $sourceFiles, $version = false) {
	global $cfg, $build;

	$debug = false;

	if (!$version) {
		require_once "version.php";
		$version = get_version();
	}

	set_time_limit(30);

//	if (false) {
//		$opts = array('http' =>
//			array(
//				'method' => 'POST',
//				'header' => 'Content-type: application/x-www-form-urlencoded',
//				'content' => http_build_query(
//						array(
//							"code_url" => "http://ams.infuzehosting.co.uk/_common/js/$sourceFiles",
//							"compilation_level" => "ADVANCED_OPTIMIZATIONS",
//							"output_format" => "text",
////							"output_info" => "warnings",
//							"formatting" => "pretty_print",
//							"externs_url" => array(
//								"http://ams.infuzehosting.co.uk/include/externs/jquery.extern.js"
//							)
//						)
//				)
//			)
//		);
//		$code = file_get_contents("http://closure-compiler.appspot.com/compile", false, stream_context_create($opts));
//		file_put_contents(str_replace(".js", ".cc.js", $filename), $code);
//		return;
//	}

	if (empty($cfg["remote"])) {

		ini_set("memory_limit", "512M");

//		$fnCount = 0;
//		$fnNames = array_map("fnPrefix", Array(
//			"addAnswer",
//			"addLoading",
//			"asset",
//			"callType",
//			"checkAnswer",
//			"checkAnswers",
//			"clearAnswers",
//			"createActivityType",
//			"createDrag",
//			"createDrop",
//			"doFeedback",
//			"doLayout",
//			"dragAccept",
//			"dragClick",
//			"dragDrop",
//			"dragHelper",
//			"dragMove",
//			"dragOver",
//			"dragStart",
//			"dragStop",
//			"finishLoading",
//			"get",
//			"getAnswer",
//			"getEM",
//			"getGlobal",
//			"getQuestion",
//			"getText",
//			"goFullscreen",
//			"hideoFeedback",
//			"init",
//			"inputHandlers",
//			"loadState",
//			"persist",
//			"preventNavigation",
//			"randomOrder",
//			"refreshLayout",
//			"saveState",
//			"set",
//			"setup",
//			"setAnswer",
//			"showAnswers",
//			"startup",
//			"updateFooter",
//		));

		$cmd_js = (empty($cfg["uglify"]) ? "uglifyjs" : $cfg["uglify"]) . " " . $sourceFiles
				. " --compress warnings=false,pure_getters,unsafe" . ($build ? ",drop_console" : "") // Jump-out or build version
				. " --mangle sort,toplevel,eval"
				. " --screw-ie8"
				. " --comments"
//				. " --mangle-props"
//				. " --reserve-domprops"
//				. " --reserved-file uglify.json"
				. " --output " . $filename
				. " --define \""
				. "EDITOR=" . (strpos($filename, "abc_admin") !== false ? "true" : "false")
				. ",UGLIFY=true"
				. ",BUILD=" . ($build ? "true" : "false")
				. ",VERSION='" . $version . "'"
				. ",BUILDTIME=" . date("'l dS M Y H:i:s'")
//				. "," . implode(",", $fnNames)
				. "\"";
		if ($debug) {
			$cmd_output = "";
			error_log($cmd_js);
			exec($cmd_js, $cmd_output);
			error_log("output: " . print_r($cmd_output, true));
		} else {
			exec($cmd_js);
		}
	} else {
		$opts = array("http" =>
			array(
				"method" => "POST",
				"header" => "Content-type: application/x-www-form-urlencoded",
				"content" => http_build_query(
						array(
							"build" => $build,
							"filename" => $filename,
							"version" => $version,
							"sourceFiles" => $sourceFiles
						)
				)
			)
		);
		// This automatically builds it in our own filesystem - so no need to save it
		file_get_contents("$cfg[remote]uglify.php", false, stream_context_create($opts));
	}
}
