#! /usr/bin/env node

var latex,
		fs = require("fs"),
		mjAPI = require("mathjax-node/lib/mj-single.js"),
		filename = process.argv.pop();

if (/\.tex$/.test(filename)) {
	latex = fs.readFileSync(filename);

	mjAPI.config({
		MathJax: {
			SVG: {
				font: "STIX-Web"
			}
		}
	});
	mjAPI.start();

	mjAPI.typeset({
		math: "$\\mathstrut{}"
//				+ ("\\vphantom{|}" + latex)
				+ ("" + latex)
				.trim()
				.replace(/[^\x00-\x80]/g, "") // Remove unicode
				.replace(/([&#\$])/g, "\\$1") // Fix quoting special chars
				.replace(/~/g, "\\textasciitilde")
				.replace(/\//g, "\\textbackslash")
				+ "$",
		format: "inline-TeX",
		svg: true,
//		speakText: true,
		ex: 6,
		width: 100
	}, function(data) {
		if (!data.errors) {
			fs.writeFileSync(filename.replace(".tex", ".svg"), data.svg);
		}
	});
}
