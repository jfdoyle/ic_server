<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

// === Root web filesystem path
$root = preg_replace("@/include$@i", "", str_replace("\\", "/", __DIR__));

// === MySQL details - requires valid credentials
$cfg["mysql_host"]		= "127.0.0.1";		// Server
$cfg["mysql_port"]		= 3306;				// Port (if not default)
$cfg["mysql_user"]		= "ic_user";		// MySQL user
$cfg["mysql_password"]	= "ic_password";	// MySQL password
$cfg["mysql_database"]	= "ic";			// Database

// === Server configuration
$cfg["closurecompiler"]	= false;			// Use Google Closure Compiler service, otherwise use UglifyJS
$cfg["nodejs"]			= "node";			// Full path of NodeJS required to run MathJax
$cfg["uglify"]			= "uglifyjs";		// Full path required to run UglifyJS
$cfg["read_only"]		= false;			// Read only mode (ie, can't save changes, but behaves identically)
$cfg["verbose"]			= true;				// Verbose logging
$cfg["cache_dir"]		= "$root/cache/";	// Folder to store temporary files (currently svg and zip)
$cfg["remote"]			= "";				// Remote host used for NodeJS calls - needs to have a builder installation
$cfg["publish"]			= empty($_SERVER["HTTP_HOST"]) ? "http://localhost/publish/" : "http://$_SERVER[HTTP_HOST]/publish/"; // Root path used for publishing activities

$cfg["lazy_load"]		= false;			// Allow lazy load of images on build

// === Backup
$cfg["backup"]			= true;				// Save an assets/<id>.json every time an activity is successfully changed - these are deleted when the main assets/database.sql file is updated
$cfg["backup_dir"]		= "$root/backup/";	// Folder to save daily database backup files into, otherwise only saves assets/database.sql
$cfg["backup_zip"]		= true;				// Zip database and json backups
$cfg["replicate"]		= false;			// Allow replication to or from other servers
$cfg["replicate_host"]	= "";				// Replication source host - leave blank if this is a master only - if set then this also implies read_only

// === Twitter
$cfg["twitter"]			= true;				// Report notifications on Twitter
$cfg["twitter_prefix"]	= "";				// Prefix for tweets
$cfg["twitter_ck"]		= "Mlx30n9b3hYx6D0PNpBHhgFfZ"; // CONSUMER_KEY
$cfg["twitter_cs"]		= "gy6r8i2q5IgweZiojIsAVn6qAbUaSwlnh4fODvkuqa5vU2Uxog"; // CONSUMER_SECRET
$cfg["twitter_at"]		= "822068499052953600-RzqdwjurlgiTdeyO3CVHC8FxXbDvsB2"; // ACCESS_TOKEN
$cfg["twitter_ats"]		= "mnv85v3Wx0kzyl1AxBq9jvnE9slIsGS7JKlX6ymbPjDsV"; // ACCESS_TOKEN_SECRET

// === GitHub
$cfg["github"]			= true;				// Watch for GitHub webhook changes
$cfg["github_secret"]	= "";				// Secret passphrase - https://developer.github.com/webhooks/securing/
$cfg["github_repo"]		= "Infuze/IC-Server"; // What repo are we looking for
$cfg["github_branch"]	= "master";			// Which branch
$cfg["github_git"]		= "git";			// How to call git
$cfg["github_pull"]		= "pull https://60d0e1a4f9fdcd99b1124a917dc2e57bf1579ed8@github.com/Infuze/IC-Server.git";		// Command to pull Server (gets appended to github_path)
$cfg["github_pull2"]		= "pull https://60d0e1a4f9fdcd99b1124a917dc2e57bf1579ed8@github.com/Infuze/IC-Editor.git";		// Command to pull Editor (gets appended to github_path)
$cfg["github_pull3"]		= "pull https://60d0e1a4f9fdcd99b1124a917dc2e57bf1579ed8@github.com/Infuze/IC-Activities.git";		// Command to pull Activities (gets appended to github_path)

// === Required for OSX and possibly some Linux boxes
//putenv('PATH=' . getenv('PATH') . ':/usr/local/bin');
