<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

function exec_log($cmd) {
	$cmd_output = shell_exec($cmd . " 2>&1");
	error_log("exec(" . $cmd . "): " . print_r($cmd_output, true));
}

function latex($formula, $is_file = false) {
	global $cfg;

	if ($is_file) {
		$texname = $formula;
	} else {
		$bbcode = array(
//			"/([\&\%\$\#\_\{\}])/s" => "\\$1",
			"/\n/" => '\n\\newline\n',
			"/([\&\%\$\#\_])/s" => '\\\\\\1',
			"/\[b\](.*?)\[\/b\]/is" => '\textbf{$1}',
			"/\[i\](.*?)\[\/i\]/is" => '\textit{$1}',
			"/\[u\](.*?)\[\/u\]/is" => '\underline{$1}',
			"/\[sup\](.*?)\[\/sup\]/is" => '\textsuperscript{$1}',
			"/\[sub\](.*?)\[\/sub\]/is" => '\textsubscript{$1}',
			"/\(([^)]*?)\)\/\/\(([^)]*?)\)/is" => '\frac{$1}{$2}',
			"/\(([^)]*?)\)\/\/(\d+)/is" => '\frac{$1}{$2}',
			"/(\d+)\/\/\(([^)]*?)\)/is" => '\frac{$1}{$2}',
			"/\(([^(]*?)\/\/([^)]*?)\)/is" => '\frac{$1}{$2}',
			"/(\d+)\/\/(\d+)/is" => '\frac{$1}{$2}',
			"/(\*)/is" => '\\times ',
			"/(\/)/is" => '\\div ',
			"/(!=)/is" => '\\neq ',
			"/(\.\.\.)/is" => '\\dots '
		);

		$latex = preg_replace(array_keys($bbcode), array_values($bbcode), $formula);
		$texname = "cache/" . sha1($formula) . ".tex"; # Cache the formula

		if (!file_exists($texname)) {
			file_put_contents($texname, $latex);
		}
	}

	$svgname = str_replace(".tex", ".svg", $texname);
	set_time_limit(30);

	if (!file_exists($svgname)) {
		if (empty($cfg["remote"])) {
			$cmd = (empty($cfg["nodejs"]) ? "node" : $cfg["nodejs"]) . " \"include/latex.js\" \"$texname\"";
			exec($cmd);
//			exec_log($cmd);
		} else {
			// This automatically builds it in our own filesystem - so no need to save it
			file_get_contents("$cfg[remote]latex.php", false, stream_context_create(
							array('http' =>
								array(
									'method' => 'POST',
									'header' => 'Content-type: application/x-www-form-urlencoded',
									'content' => http_build_query(
											array(
												"filename" => $texname
											)
									)
								)
			)));
		}
	}
	return $svgname;
}
