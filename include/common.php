<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

$user_id = -1; // This is set on login, otherwise is this invalid number
$user_group = ""; // This is set on login, otherwise is this invalid number
$user_perms = 0;
$user_email = "";
$build = false; // Set when exporting
$assets = array(); // List of assets for building, ignored otherwise

if (!ini_get("date.timezone")) {
	date_default_timezone_set("Europe/London");
}

/* Find all parent nodes without touching cache
  SELECT hp.*
  FROM (
  SELECT @r AS _id,
  @level := @level + 1 AS level,
  (
  SELECT @r := NULLIF(node_parent, 0)
  FROM ic_node hn
  WHERE node_id = _id
  )
  FROM (
  SELECT @r := 100, @level := 0
  ) vars,
  ic_node hc
  WHERE @r IS NOT NULL
  ) hc
  JOIN ic_node hp
  ON hp.node_id = hc._id
  ORDER BY level DESC
 */

defined("INCLUDE_DIR") || define("INCLUDE_DIR", dirname(__FILE__) . DIRECTORY_SEPARATOR);

if (!isset($cfg)) {
	$cfg = array();
}
require_once(INCLUDE_DIR . "config.default.php");
include_once(INCLUDE_DIR . "config.inc.php");

if (!empty($cfg["replicate_host"])) {
	$cfg["read_only"] = true;
}

require_once(INCLUDE_DIR . "db.php");
require_once(INCLUDE_DIR . "db.user.php");
require_once(INCLUDE_DIR . "db.organisation.php");
require_once(INCLUDE_DIR . "login.php");
require_once(INCLUDE_DIR . "permissions.php");

set_error_handler(function($errno, $message) {
	if ($errno === E_WARNING && (strpos($message, "filemtime") === 0 || strpos($message, "fopen") === 0)) {
		return true; // using filemtime for file_exists too
	}
	return false; // default handler
});

// Quick cheat to create a new user
//register("kirk@infuze.co.uk", "collins", "palace7");

session_name("SESSION");
session_start(array(// Make sure we're only storing per-session data about someone
	"cookie_httponly" => "1",
	"cookie_lifetime" => "0" // 60 * 60 * 24 = 86400 / 24 hours
));

/**
 * Login the user, display a login page if that fails
 * @param boolean $demo If a "demo" user can be used - for index.php only
 */
function do_login($demo = false) {
	if ($demo && !strcmp(filter_input(INPUT_POST, "username", FILTER_UNSAFE_RAW), "demo@demo")) {
		$_SESSION["demo"] = true;
		session_write_close();
		header("Refresh: 0"); // Prevent asking to refresh form info
		die;
	} elseif (!login_check()) {
		if (!login(filter_input(INPUT_POST, "username", FILTER_UNSAFE_RAW), filter_input(INPUT_POST, "password", FILTER_UNSAFE_RAW))) {
// <script type="text/javascript">".readfile("include/login.min.js")."</script>
			die("<html><head>"
					. "<title>Login to Activity Builder</title>"
//					. "<style type=\"text/css\"></style>"
					. "</head><body>"
					. "<form action=\"\" method=\"post\" name=\"login_form\"><table style=\"margin: 0 auto;\"><tbody>"
					. "<tr><td colspan=\"2\" style=\"text-align: center;font-weight: bold;\">Login to Activity Builder<hr></td></tr>"
					. ($demo ? "<tr><td colspan=\"2\" style=\"text-align: center;\">Use \"demo@demo\" to test spreadsheets</td></tr>" : "")
					. "<tr><td style=\"text-align: right;\">Email:</td><td><input type=\"email\" name=\"username\"></td></tr>"
					. "<tr><td style=\"text-align: right;\">Password:</td><td><input type=\"password\" name=\"password\" id=\"password\"></td></tr>"
					. "<tr><td colspan=\"2\" style=\"text-align: right;\"><input type=\"submit\" value=\"Login\"></td></tr>"
					. "</tbody></table></form></body></html>");
		} else {
			header("Refresh: 0"); // Prevent asking to refresh form info
			die;
		}
	}
}

/**
 * Generate v5 UUID
 *
 * Version 5 UUIDs are named based. They require a namespace (another
 * valid UUID) and a value (the name). Given the same namespace and
 * name, the output is always the same.
 *
 * param	uuid	$namespace
 * @param	string	$name
 */
//function uuid_v5($namespace, $name) {
function uuid_v5($name) {
	// TODO: Put this into the $cfg
	$namespace = "35d52301-56ef-47e2-a6b5-389e78e251f2";
	if (!is_valid_uuid($namespace)) {
		return false;
	}
	// Get hexadecimal components of namespace
	$nhex = str_replace(array('-', '{', '}'), '', $namespace);
	// Binary Value
	$nstr = '';
	// Convert Namespace UUID to bits
	for ($i = 0; $i < strlen($nhex); $i += 2) {
		$nstr .= chr(hexdec($nhex[$i] . $nhex[$i + 1]));
	}
	// Calculate hash value
	$hash = sha1($nstr . $name);
	return sprintf('%08s-%04s-%04x-%04x-%12s',
			// 32 bits for "time_low"
			substr($hash, 0, 8),
			// 16 bits for "time_mid"
			substr($hash, 8, 4),
			// 16 bits for "time_hi_and_version",
			// four most significant bits holds version number 5
			(hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x5000,
			// 16 bits, 8 bits for "clk_seq_hi_res",
			// 8 bits for "clk_seq_low",
			// two most significant bits holds zero and one for variant DCE1.1
			(hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,
			// 48 bits for "node"
			substr($hash, 20, 12)
	);
}

function is_valid_uuid($uuid) {
	return preg_match('/^\{?[0-9a-f]{8}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?[0-9a-f]{12}\}?$/i', $uuid) === 1;
}

require_once "node/get_node.php";

$node_parent_data_cache = array();
$node_parent_list_cache = array();

/**
 * Get and merge all parent node data for a single node
 * @global mysqli $mysqli
 * @param int node_id Activity ID
 * @param boolean $and_self To get our data too
 * @return array
 */
function get_node_parent_data($node_id, $and_self = false) {
	global $mysqli, $node_parent_data_cache;
	// Check cache first
	if (!array_key_exists($node_id, $node_parent_data_cache)) {
		$parent = $name = $data = null;
		$node = $node_id;
		$list = array();
		$node_parent_data_cache[$node] = array();
		$stmt = $mysqli->prepare("SELECT `node_parent`,`node_name`,`node_data` FROM `ic_node` WHERE `node_id` = ?");
		$stmt->bind_param("i", $node);
//		error_log("fetching data $node");
		while ($node && ($node === $node_id || !array_key_exists($node, $node_parent_data_cache))) {
			$stmt->execute();
			$stmt->bind_result($parent, $name, $data);
			if ($stmt->fetch()) {
				if ($and_self) {
					$json = json_decode($data, true);
					if (!empty($name)) {
						$json["_name"] = $name;
					}
					$node_parent_data_cache[$node] = $json;
				} else {
					$and_self = true;
				}
				$list[] = $node;
				$node = $parent;
			} else {
				$node = NULL;
			}
		}
		$stmt->close();
		$json = $node ? $node_parent_data_cache[$node] : array();
		while ($node = array_pop($list)) {
			$node_parent_data_cache[$node] = $json = array_merge_recursive_distinct($json, $node_parent_data_cache[$node]);
		}
	}
	return $node_parent_data_cache[$node_id];
}

/**
 * Get all parent node ids for a single node
 * @global mysqli $mysqli
 * @param int node_id Activity ID
 * @param boolean $and_self To get our data too
 * @return array
 */
function get_node_parent_list($node_id, $and_self = true) {
	global $mysqli, $node_parent_list_cache;

	// Check cache first
	if (!array_key_exists($node_id, $node_parent_list_cache)) {
		$node_parent_list_cache[$node_id] = array();
		$list = array($node_id);

		$stmt = $mysqli->prepare("SELECT parent.`node_id` FROM (SELECT @r AS _id,@level := @level + 1 AS level,(SELECT @r := NULLIF(`node_parent`, 0) FROM `ic_node` hn WHERE `node_id` = _id) FROM (SELECT @r := ?, @level := 0) vars, `ic_node` child WHERE @r IS NOT NULL) child JOIN `ic_node` parent ON parent.`node_id` = child._id WHERE level > " . ($and_self ? "0" : "1") . " ORDER BY level DESC");
		$stmt->bind_param("i", $node_id);
		$stmt->execute();
		$stmt->store_result();
		$id = null;
		$stmt->bind_result($id);
		while ($stmt->fetch()) {
			foreach ($list as $node) { // Add this node id to the cache
				array_push($node_parent_list_cache[$node], $id);
			}
			if ($id == $node_id) {
				continue;
			}
			if (array_key_exists($id, $node_parent_list_cache)) { // If we already have this id in the cache then save the db fetching
				foreach ($node_parent_list_cache[$id] as $node_parent) {
					foreach ($list as $node) {
						array_push($node_parent_list_cache[$node], $node_parent);
					}
				}
				break;
			} else { // Add this id to the list instead
				$node_parent_list_cache[$id] = array();
				$list[] = $id;
			}
		}
		$stmt->close();
	}
	return $node_parent_list_cache[$node_id];
}

require_once "node/update_node_timestamp.php";
require_once "node/get_node_timestamp.php";
require_once "node/log_action.php";

/**
 * Merge multiple arrays into one, keeping unique answers
 * @param array $original_array Values in this one get overwritten
 * @param array $new_array By values in this one
 * @return array
 */
function array_merge_recursive_distinct(array &$original_array, array &$new_array) {
	$base = $original_array;

	foreach ($new_array as $key => &$value) {
		if (is_array($value) && isset($base[$key]) && is_array($base[$key])) {
			$base[$key] = array_merge_recursive_distinct($base[$key], $value);
		} else {
			$base[$key] = $value;
		}
	}
	return $base;
}

/**
 * Remove all keys from the first array that appear in subsequent arrays
 * @return array
 */
function array_unique_recursive_distinct() {
	$arrays = func_get_args();
	$base = array_shift($arrays);

	foreach ($arrays as $array) {
		if (is_array($array)) {
			foreach ($array as $key => &$value) {
				if (isset($base[$key])) {
					if (is_array($value) && is_array($base[$key])) {
						$base[$key] = array_unique_recursive_distinct($base[$key], $value);
					}
					if (!isset($base[$key]) || (is_array($base[$key]) && !count($base[$key])) || (!is_array($value) && $base[$key] === $value)) {
						unset($base[$key]);
					}
				}
			}
		}
	}
	return $base;
}

/**
 * Upload a file for an activity
 * @global type $cfg
 * @global int $user_id
 * @param int $node_id
 * @param string $name
 * @param string $content
 * @return type
 */
function upload_file($node_id, $name, $content) {
	global $cfg, $user_id;

	if ($cfg["read_only"]) {
		return array("error" => "Server is read-only");
	}
	if (isset($node_id) && has_node($node_id, $user_id) && has_perm(PERM_NODE_THEME | PERM_NODE_CONTENT)) {
		if (preg_match("/[\/\\\?\*:]/", $name)) {
			return array("error" => "Bad filename");
		}
		if (!$content || !count($content)) {
			return array("error" => "Bad data");
		}
		$decoded = base64_decode($content);
		if (!count($decoded)) {
			return array("error" => "Bad data");
		}
		if (!is_dir("assets/$node_id")) {
			mkdir("assets/$node_id");
		}
		$filename = "assets/$node_id/$name";
		$edit = file_exists($filename);
		$changed = $edit ? file_has_changed($filename, $decoded) : false;
		if (!$edit || $changed) {
			file_put_contents($filename, $decoded);
			if ($cfg["backup"]) {
				if (!is_dir("$cfg[backup_dir]$node_id")) {
					mkdir("$cfg[backup_dir]$node_id");
				}
				$date = date("Y-m-d_H-i-s");
				if ($cfg["backup_zip"]) {
					$zip = new ZipArchive;
					$res = $zip->open("$cfg[backup_dir]$node_id/$name@$date.zip", ZIPARCHIVE::CREATE | ZipArchive::OVERWRITE);
				}
				if ($cfg["backup_zip"] && $res === TRUE) {
					$zip->addFromString($name, $decoded);
					$zip->close();
				} else {
					copy($filename, "$cfg[backup_dir]$node_id/$name@$date");
				}
			}
			update_node_timestamp($node_id);
			log_action($node_id, $edit ? "edit" : "add", $name);
			return array("success" => "File uploaded");
		}
		return array("success" => "No changes");
	}
}

/**
 * Compare two files and report if they are different or not.
 * @param string $filename
 * @param string $data
 * @return boolean
 */
function file_has_changed($filename, $data) {
	if (filesize($filename) !== strlen($data) || file_get_contents($filename) !== $data) {
		return true;
	}
	return false;
}

/**
 * Like realpath() but actually fixes urls etc too
 * @param string $path
 * @return string
 */
function fixpath($path) {
	$n = 1;
	while ($n > 0) {
		$path = preg_replace("#/(?!\.\.)[^/]+/\.\./#", "/", $path, -1, $n);
	}
	return $path;
}

/**
 * Build a .min.js file from an .js file if needed
 * @param type $js_name
 */
function update_js(&$js_name) {
	if (preg_match("/\.js$/i", $js_name) && !preg_match("/\.min\.js$/i", $js_name)) {
		$js_time = filemtime($js_name);
		if ($js_time !== false) {
			$min_name = str_replace(".js", ".min.js", $js_name);
			$min_time = filemtime($min_name);
			if ($min_time === false || $min_time < $js_time) {
				@unlink($min_name); // Force it to be re-created
				require_once "include/uglify.php";
				uglify($min_name, $js_name);
			}
			return true;
		}
	}
	return false;
}

$cache_files = array();
$cache_md5 = array();

/**
 * Cache a file and return it
 * @global array $cache_files
 * @param string $filename
 * @return string
 */
function cache_file($filename) {
	global $cache_files, $cache_md5;

	if (!array_key_exists($filename, $cache_files)) {
		if (file_exists("activity/$filename")) {
			$cache_files[$filename] = file_get_contents("activity/$filename");
			if (empty($cache_files[$filename]) || substr($cache_files[$filename], -1) !== "\n") {
				$cache_files[$filename] .= "\n";
			}
		} else {
			$cache_files[$filename] = "";
		}
		$cache_md5[$filename] = md5($cache_files[$filename]);
	}
	return $cache_files[$filename];
}

/**
 * Die, outputting a json object as a string
 * @param array $arr
 */
function json_die(&$arr) {
	die(json_encode($arr));
}

// No closing delimiter as this is included only