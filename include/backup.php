<?php

require_once "common.php";

function backup() {
	global $cfg, $root;

	if (php_sapi_name() === "cli" || is_root()) {
		require_once "db.backup.php";

		if ($cfg["backup"]) {
			ini_set("memory_limit", "1024M");
			ini_set("max_execution_time", "3000");
			set_time_limit(300);

			$backup = backup_tables(array("ic_node", "ic_group", "ic_user", "ic_log"));
			file_put_contents("$root/assets/database.sql", $backup);
			if (!empty($cfg["backup_dir"])) {
				if (!is_dir($cfg["backup_dir"])) {
					mkdir($cfg["backup_dir"], 0777, true);
				}
				$date = date("Y-m-d_H-i-s");
				if ($cfg["backup_zip"]) {
					$zip = new ZipArchive;
					$res = $zip->open("$cfg[backup_dir]database.sql@$date.zip", ZIPARCHIVE::CREATE | ZipArchive::OVERWRITE);
				}
				if ($cfg["backup_zip"] && $res === TRUE) {
					$zip->addFromString("database.sql", $backup);
					$zip->close();
				} else {
					file_put_contents("$cfg[backup_dir]database.sql@$date", $backup);
				}
			}
			return array("success" => "COMPLETED");
		} else {
			return array("error" => "DISABLED");
		}
	}
}
