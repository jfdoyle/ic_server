<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

require_once "db.php";

/**
 * Actively login a user, setting the session variables as needed
 * @global mysqli $mysqli
 * @global string $user_group
 * @param type $email
 * @param type $password
 * @return boolean
 */
function login($email, $password) {
	global $mysqli;
	// Using prepared Statements means that SQL injection is not possible.
	$result = false;
	$user_id = $user_email = $user_group = $user_perms = $user_password = $user_salt = null;
	$stmt = $mysqli->prepare("SELECT `user_id`, `user_email`, `user_group`, `user_permission`, `user_password`, `user_salt` FROM `ic_user` WHERE `user_email` = ? LIMIT 1");
	$stmt->bind_param("s", $email); // Bind "$email" to parameter.
	$stmt->execute(); // Execute the prepared query.
	$stmt->store_result();
	$stmt->bind_result($user_id, $user_email, $user_group, $user_perms, $user_password, $user_salt); // get variables from result.
	$stmt->fetch();
	if ($stmt->num_rows == 1) { // If the user exists
		$password = hash("sha512", $password . $user_salt); // hash the password with the unique salt.
		// We check if the account is locked from too many login attempts
		if ($user_password == $password) { // Check if the password in the database matches the password the user submitted.
			// Password is correct!
			$user_id = preg_replace("/[^0-9]+/", "", $user_id); // XSS protection as we might print this value
			$_SESSION["IPaddress"] = filter_input(INPUT_SERVER, "REMOTE_ADDR", FILTER_UNSAFE_RAW);
			$_SESSION["userAgent"] = filter_input(INPUT_SERVER, "HTTP_USER_AGENT", FILTER_UNSAFE_RAW);
			$_SESSION["user_id"] = $GLOBALS["user_id"] = $user_id;
			$_SESSION["user_email"] = $GLOBALS["user_email"] = $user_email;
			$_SESSION["user_group"] = $GLOBALS["user_group"] = $user_group;
			$_SESSION["user_perms"] = $GLOBALS["user_perms"] = $user_perms;
			$_SESSION["login_string"] = hash("sha512", $password . $_SESSION["userAgent"]);
			// Login successful.
			$result = true;
		}
	}
	session_write_close();
	return $result;
}

/**
 * Check if someone is logged in via session variables
 * @global mysqli $mysqli
 * @global string $user_group
 * @return boolean
 */
function login_check() {
	global $mysqli;

	$result = false;
	// Check if all session variables are set
	if ((array_key_exists("OBSOLETE", $_SESSION) && $_SESSION["OBSOLETE"] && $_SESSION["EXPIRES"] > time())) {
		return false;
	}
	if (isset($_SESSION["user_id"], $_SESSION["user_email"], $_SESSION["login_string"])) {
		$user_id = $_SESSION["user_id"];
		$login_string = $_SESSION["login_string"];
		$user_browser = filter_input(INPUT_SERVER, "HTTP_USER_AGENT", FILTER_UNSAFE_RAW); // Get the user-agent string of the user.

		$stmt = $mysqli->prepare("SELECT `user_password`, `user_email`, `user_group`, `user_permission` FROM `ic_user` WHERE `user_id` = ? LIMIT 1");
		$stmt->bind_param("i", $user_id);
		$stmt->execute();
		$stmt->store_result();

		if ($stmt->num_rows == 1) { // If the user exists
			$user_password = $user_email = $user_group = $user_perms = null;
			$stmt->bind_result($user_password, $user_email, $user_group, $user_perms); // get variables from result.
			$stmt->fetch();
			$login_check = hash("sha512", $user_password . $user_browser);
			if ($login_check == $login_string) {
				// Logged In!!!!
				$GLOBALS["user_id"] = $user_id;
				$GLOBALS["user_email"] = $user_email;
				$GLOBALS["user_group"] = $user_group;
				$GLOBALS["user_perms"] = $user_perms;

				$stmt->close();
				$stmt = $mysqli->prepare("UPDATE `ic_user` SET `user_seen` = UTC_TIMESTAMP() WHERE `user_id` = ? LIMIT 1");
				$stmt->bind_param("i", $user_id);
				$stmt->execute();

				$result = true;
				session_write_close(); // If we fail then leave the session open in case we're about to login
			}
			$stmt->close();
		}
	}
	return $result;
}

/**
 * Create a new user login
 * @global mysqli $mysqli
 * @param type $user_email
 * @param type $user_group
 * @param type $password
 */
function register($user_email, $user_group, $password) {
	global $mysqli;
	// Create a random salt
	$user_salt = hash("sha512", uniqid(mt_rand(1, mt_getrandmax()), true));
	// Create salted password (Careful not to over season)
	$user_password = hash("sha512", $password . $user_salt);

	// Add your insert to database script here.
	// Make sure you use prepared statements!
	if (($stmt = $mysqli->prepare("INSERT INTO ic_user (user_email, user_group, user_password, user_salt) VALUES (?, ?, ?, ?)"))) {
		$stmt->bind_param("ssss", $user_email, $user_group, $user_password, $user_salt);
		// Execute the prepared query.
		$stmt->execute();
		$stmt->close();
	}
}
