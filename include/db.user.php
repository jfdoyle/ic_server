<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

/**
 * Check if a user has access to a node
 * @global mysqli $mysqli
 * @param int $node_id to check
 * @param int $user_id Normally use $GLOBALS["user_id"]
 * @return boolean
 */
function has_node($node_id, $user_id) {
	global $mysqli;

	if (is_root()) {
		return true;
	}
	$result = false;
	if (!empty($node_id)) {
		// TODO: Need to optimise this, got a feeling it's heavy on the MySQL server
		$stmt = $mysqli->prepare("SELECT TRUE FROM `ic_group` WHERE `group_id` IN (SELECT `user_group` FROM `ic_user` WHERE `user_id` = ?) AND `group_node` IN (SELECT parent.`node_id` FROM (SELECT @r AS _id,@level := @level + 1 AS level,(SELECT @r := NULLIF(`node_parent`, 0) FROM `ic_node` hn WHERE `node_id` = _id) FROM (SELECT @r := ?, @level := 0) vars, `ic_node` child WHERE @r IS NOT NULL) child JOIN `ic_node` parent ON parent.`node_id` = child._id ORDER BY level DESC) LIMIT 1");
		$stmt->bind_param("ii", $user_id, $node_id);
		$stmt->execute();
		$stmt->store_result();
		if ($stmt->num_rows) {
			$result = true;
		}
		$stmt->close();
	}
	return $result;
}
