-- Common SQL functions

-- Select all parent nodes
SELECT parent.* FROM (
	SELECT @r AS _id,
	@level := @level + 1 AS level,
	(
		SELECT @r := NULLIF(`node_parent`, 0)
		FROM `ams_node` node
		WHERE `node_id` = _id
	)
	FROM (SELECT @r := ?, @level := 0) vars,
	`ams_node` child
	WHERE @r IS NOT NULL
) child
JOIN `ams_node` parent
ON parent.`node_id` = child._id
ORDER BY level DESC

-- Traversal / children functions
DELIMITER $$
CREATE FUNCTION hierarchy_connect_by_parent_eq_prior_id_with_level(value INT, maxlevel INT) RETURNS INT
NOT DETERMINISTIC
READS SQL DATA
BEGIN
	DECLARE _id INT;
	DECLARE _parent INT;
	DECLARE _next INT;
	DECLARE _i INT;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET @id = NULL;

	SET _parent = @id;
	SET _id = -1;
	SET _i = 0;

	IF @id IS NULL THEN
		RETURN NULL;
	END IF;

	LOOP
		SELECT  MIN(node_id)
		INTO    @id
		FROM    `ams_node`
		WHERE   node_parent = _parent
			AND node_id > _id
			AND COALESCE(@level < maxlevel, TRUE);
		IF @id IS NOT NULL OR _parent = @start_with THEN
			SET @level = @level + 1;
			RETURN @id;
		END IF;
		SET @level := @level - 1;
		SELECT  node_id, node_parent
		INTO    _id, _parent
		FROM    `ams_node`
		WHERE   node_id = _parent;
		SET _i = _i + 1;
	END LOOP;
	RETURN NULL;
END$$
CREATE FUNCTION hierarchy_connect_by_parent_eq_prior_id(value INT) RETURNS INT
NOT DETERMINISTIC
READS SQL DATA
BEGIN
	DECLARE _id INT;
	DECLARE _parent INT;
	DECLARE _next INT;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET @id = NULL;

	SET _parent = @id;
	SET _id = -1;

	IF @id IS NULL THEN
		RETURN NULL;
	END IF;

	LOOP
		SELECT  MIN(node_id)
		INTO    @id
		FROM    `ams_node`
		WHERE   node_parent = _parent
			AND node_id > _id;
		IF @id IS NOT NULL OR _parent = @start_with THEN
			SET @level = @level + 1;
			RETURN @id;
		END IF;
		SET @level := @level - 1;
		SELECT  node_id, node_parent
		INTO    _id, _parent
		FROM    `ams_node`
		WHERE   node_id = _parent;
	END LOOP;       
END$$
DELIMITER ;

-- Old triggers
-- CREATE TRIGGER `nodeDelete` AFTER DELETE ON `ams_node`
--  FOR EACH ROW DELETE FROM ams_tree WHERE tree_node = OLD.node_id OR tree_parent = OLD.node_id
-- 
-- CREATE TRIGGER `nodeInsert` AFTER INSERT ON `ams_node`
--  FOR EACH ROW BEGIN
-- 	INSERT INTO ams_tree(tree_node, tree_parent, tree_level)
-- 		SELECT NEW.node_id, NEW.node_id, 0;
-- 	INSERT INTO ams_tree(tree_node, tree_parent, tree_level)
-- 		SELECT NEW.node_id, t.tree_parent, t.tree_level + 1
-- 		FROM ams_tree t
-- 		WHERE NEW.node_parent = t.tree_node;
-- END
-- 
-- CREATE TRIGGER `nodeUpdate` AFTER UPDATE ON `ams_node`
--  FOR EACH ROW IF OLD.node_parent != NEW.node_parent THEN
-- 	CREATE TEMPORARY TABLE IF NOT EXISTS children (NodeId BIGINT(10), Level int);
-- 	DELETE FROM children;
-- 
-- 	INSERT INTO children(NodeId, Level)
-- 		SELECT t.tree_node, t.tree_level
-- 		FROM ams_tree t
-- 		WHERE NEW.node_id = t.tree_parent AND t.tree_level > 0;
-- 
-- 	DELETE FROM ams_tree
-- 	WHERE
-- 		ams_tree.tree_node IN (SELECT NodeId FROM children)
-- 		AND ams_tree.tree_parent IN ( 
-- 		SELECT 
-- 			tmp.tree_parent 
-- 			FROM (
-- 				SELECT t.tree_parent
-- 				FROM ams_tree t
-- 				WHERE NEW.node_id = t.tree_parent AND t.tree_level > 0
-- 			) as tmp
-- 		);
-- 
-- 	DELETE FROM ams_tree
-- 		WHERE ams_tree.tree_node = NEW.node_id AND ams_tree.tree_level > 0;
-- 
-- 	INSERT INTO ams_tree(tree_node, tree_parent, tree_level)
-- 		SELECT NEW.node_id, t.tree_parent, t.tree_level + 1
-- 		FROM ams_tree t
-- 		WHERE NEW.node_parent = t.tree_node;
-- 
-- 	INSERT INTO ams_tree(tree_node, tree_parent, tree_level)
-- 		SELECT c.NodeId, t.tree_parent, t.tree_level + c.Level
-- 		FROM ams_tree t, children c
-- 		WHERE NEW.node_id = t.tree_node AND t.tree_level > 0;
-- END IF

