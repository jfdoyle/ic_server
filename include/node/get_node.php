<?php

/*
 * Copyright &copy; 2013 Infuze Ltd, All Rights Reserved.
 */

$node_cache = array(); // $editor and $build are global and don't change

/**
 * Get a node and optionally all it's children
 * @global mysqli $mysqli
 * @param int $node_id to get
 * @param boolean $get_children if we want children data
 * @param boolean $editor if we want the extra editor data
 * @param int $parent_id (private) when getting children
 * @return array
 */
function get_node($node_id, $get_children = false, $parent_id = null, $get_grandchildren = false) {
	global $mysqli, $build, $editor, $node_cache;

	$result = array();

	if (is_null($parent_id) && !is_array($node_id) && array_key_exists($node_id, $node_cache)) { // TODO: Catch these other types for cache reading
		// Access the cache
		$result[] = $node = $node_cache[$node_id];
		if ($get_children && $node["_type"] < 10) {
			ini_set("memory_limit", "512M");
			$result = array_merge($result, get_node($node_id, $get_grandchildren, $node_id, $get_grandchildren));
		}
	} else {
		// Access the database
		$id = $parent = $creator_id = $created = $editor_id = $date = $flags = $type = $name = $html = $children = $data = $tags = $build_data = $group = null;
		$where = $order_by = $limit = "";
		if (!is_null($parent_id)) {
			$where = " WHERE `node_parent` = ?";
			$bind = $parent_id;
		} else if (is_array($node_id)) {
			$list_of_nodes = preg_replace("/(^,|[^0-9,]|,,+|,$)/", "", implode(",", $node_id));
			$where = "WHERE `node_id` IN ($list_of_nodes)";
			if (array_search("0", $node_id) !== false && is_root()) {
				$where .= " OR `node_parent` = 0";
			}
			$order_by = " ORDER BY FIELD(`node_id`, $list_of_nodes)";
		} else {
			$where = "WHERE `node_id` = ?";
			$limit = " LIMIT 1";
			$bind = $node_id;
		}
		$sql = "SELECT "
				. "`node_id`,"
				. "`node_parent`" . ","
				. ($editor ? "(SELECT `user_email` FROM `ic_user` WHERE `user_id` = `node_creator`)" : "0") . ","
				. ($editor ? "DATE_FORMAT(`node_created`,'%Y-%m-%dT%TZ')" : "0") . ","
				. ($editor ? "(SELECT `user_email` FROM `ic_user` WHERE `user_id` = `node_editor`)" : "0") . ","
				. "DATE_FORMAT(`node_time`,'%Y-%m-%dT%TZ'),"
				. "`node_flags`,"
				. "`node_type`,"
				. "`node_name`,"
				. "`node_html`,"
				. "`node_children`,"
				. "`node_data`,"
				. "`node_tags`,"
				. ($editor || $build ? "`node_build`" : "FALSE") . ","
				. ($editor ? "(SELECT GROUP_CONCAT(`group_name`) FROM `ic_group` WHERE `group_node` = `node_id` LIMIT 1)" : "FALSE")
				. " FROM `ic_node` "
				. $where
				. $order_by
				. $limit;
		$stmt = $mysqli->prepare($sql);
		if (isset($bind)) {
			$stmt->bind_param("i", $bind);
		}
		$stmt->execute();
		$stmt->store_result();
		$stmt->bind_result($id, $parent, $creator_id, $created, $editor_id, $date, $flags, $type, $name, $html, $children, $data, $tags, $build_data, $group);
		while ($stmt->fetch()) {
			$node = json_decode($data, true);
			$safe_children = preg_replace("/(^0$|^0,|,0$|(?<=,)0,)/", "", $children);
			if (strcmp($children, $safe_children)) {
				// if they're different then we need to fix it...
				$mysqli->query("UPDATE `ic_node` SET `node_children` = '$safe_children' WHERE `node_id` = '$id'");
			}
			$node["_children"] = empty($children) ? array() : array_map("intval", explode(",", $safe_children));
			$node["_id"] = $id;
			$node["_uuid"] = uuid_v5($id);
			$node["_name"] = strval($name);
			$node["_type"] = $type;
			$node["_html"] = $html;
			$node["_tags"] = $tags;
			$node["_flags"] = $flags; // Not build?
			$node["_date"] = $date;
			$node["_parent"] = $parent;
			if (($editor || $build) && !empty($build_data)) {
				$node["_build"] = json_decode($build_data, true);
			}
			if ($creator_id) {
				$node["_creator"] = $creator_id;
				$node["_created"] = $created;
			}
			if ($editor_id) {
				$node["_editor"] = $editor_id;
			}
			if (!empty($group)) {
				$node["_group"] = $group;
			}
			if ($editor) {
				$node["_files"] = array();
				if (is_dir("assets/$id")) {
					foreach (glob("assets/$id/*") as $file) {
						$node["_files"][] = "assets/$id/" . basename($file);
					}
				}
			}
			$result[] = $node_cache[$id] = $node;
			if ($get_children && $type < 10) {
				ini_set("memory_limit", "512M");
				$result = array_merge($result, get_node($id, $get_grandchildren, $id, $get_grandchildren));
			}
		}
		$stmt->close();
	}
	return $result;
}
