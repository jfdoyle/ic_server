<?php

/**
 * Update the timestamp on a node
 * @param int $node_id id
 */
function update_node_timestamp($node_id, $editor = null) {
	global $mysqli, $cfg;

	if (!$cfg["read_only"]) {
		$mysqli->query("UPDATE `ic_node` SET `node_time` = UTC_TIMESTAMP()" . (is_null($editor) ? "" : ", `node_editor` = '" . intval($editor) . "'") . " WHERE `node_id` = '" . intval($node_id) . "' LIMIT 1");
	}
}
