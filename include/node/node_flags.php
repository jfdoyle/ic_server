<?php

define("ACTIVITY_FLAG_LOCK", 1 << 0); // Locked - No changes permitted
define("ACTIVITY_FLAG_DELETED", 1 << 1); // Needs Deletion (will not be in build)
define("ACTIVITY_FLAG_CHECK", 1 << 2); // Unsure - Check Comments
define("ACTIVITY_FLAG_IMPORTANT", 1 << 3); // Important
define("ACTIVITY_FLAG_URGENT", 1 << 4); // Very Important
define("ACTIVITY_FLAG_IGNORE", 1 << 5); // Don't Use (will not be in build)
define("ACTIVITY_FLAG_STATE_INCOMPLETE", 1 << 6); // Not Complete
define("ACTIVITY_FLAG_STATE_CHANGE", 1 << 7); // Changes Required
define("ACTIVITY_FLAG_STATE_CHECK", 1 << 8); // Needs Checking
define("ACTIVITY_FLAG_STATE_COMPLETE", 1 << 9); // Signed Off
define("ACTIVITY_FLAG_QA_BAD", 1 << 10); // QA Bad
define("ACTIVITY_FLAG_QA_UNSURE", 1 << 11); // QA Unsure
define("ACTIVITY_FLAG_QA_GOOD", 1 << 12); // QA Good
define("ACTIVITY_FLAG_STAR_NONE", 1 << 13); // Below Average
define("ACTIVITY_FLAG_STAR_HALF", 1 << 14); // Average
define("ACTIVITY_FLAG_STAR_FULL", 1 << 15); // Above Average
define("ACTIVITY_FLAG_ACCESSIBLE", 1 << 16); // Accessible
define("ACTIVITY_FLAG_IMAGE", 1 << 17); // Image
define("ACTIVITY_FLAG_SOUND", 1 << 18); // Sound
define("ACTIVITY_FLAG_TEXT", 1 << 19); // Text
define("ACTIVITY_FLAG_VIDEO", 1 << 20); // Video
