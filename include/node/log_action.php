<?php

/**
 * Log an action to the db
 * @global mysqli $mysqli
 * @global int $user_id
 * @param int $node_id
 * @param string $action
 * @param string $file
 */
function log_action($node_id, $action, $file = "") {
	global $mysqli, $user_id;

	if (!function_exists("get_owner_group")) {

		/**
		 * Get the owner organisation of a node (closest parent)
		 * @param int $node_id
		 * @return int
		 */
		function get_owner_group($node_id) {
			$parents = get_node_parent_list($node_id);
			$organisations = get_group();
			foreach ($parents as $id) {
				foreach ($organisations as $organisation) {
					if ($organisation["node"] === $id) {
						return $organisation["id"];
					}
				}
			}
			return 0;
		}

	}

	$organisation_id = get_owner_group($node_id);
//	if ($organisation) {
	$stmt = $mysqli->prepare("INSERT INTO `ic_log` (`log_group`, `log_user`, `log_node`, `log_action`, `log_file`, `log_time`) VALUES (?, ?, ?, ?, ?, UTC_TIMESTAMP())");
	$stmt->bind_param("iiiss", $organisation_id, $user_id, $node_id, $action, $file);
	$stmt->execute();
	$stmt->close();
//	} else {
//		error_log("Logging Error: Unable to find organisation for user $user_id, '$action'ing node $node_id");
//	}
}
