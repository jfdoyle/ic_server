<?php

/**
 * Get the timestamp on a node
 * @param int $id id
 */
function get_node_timestamp($id) {
	global $mysqli;

	$timestamp = null;
	$stmt = $mysqli->prepare("SELECT DATE_FORMAT(`node_time`,'%Y-%m-%dT%TZ') FROM `ic_node` WHERE `node_id` = ? LIMIT 1");
	$stmt->bind_param("i", $id);
	$stmt->execute();
	$stmt->store_result();
	$stmt->bind_result($timestamp);
	$stmt->fetch();
	$stmt->close();
	return $timestamp;
}
