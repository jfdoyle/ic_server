<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

$time_limit = 300;
// Anything smaller than these limits won't use lazy-load or trickle-load
$min_image_width = 64;
$min_image_height = 64;
$min_image_bytes = 1024;

ini_set("memory_limit", "1024M");
ini_set("max_execution_time", "3000");
ini_set("pcre.backtrack_limit", "10000000");
ini_set("pcre.recursion_limit", "1000000");
set_time_limit($time_limit);

require_once "include/version.php";
require_once "include/node/node_flags.php";

//function create_zip($path) {
//	$zipFile = "$path.zip";
//	$zipArchive = new ZipArchive();
//
//	if (!$zipArchive->open($zipFile, ZIPARCHIVE::OVERWRITE)) {
//		error_log("Failed to create archive\n");
//	} else {
//		$options = array("add_path" => "$path/", "remove_path" => $path);
//		$zipArchive->addGlob("**", 0, $options);
//		if (!$zipArchive->status == ZIPARCHIVE::ER_OK) {
//			error_log("Failed to write files to zip\n");
//		}
//		$zipArchive->close();
//	}
//}

$build = true;
if (!is_dir("publish")) {
	mkdir("publish");
}

/**
 * Add a single file to the zip archive, cache if it's a file, write if it's text
 * @global ZipArchive $zip
 * @global array $zip_files
 * @param string $zip_path
 * @param string $file
 * @param boolean $text
 */
function add_zip($zip_path, $file = "", $text = false) {
	global $zip, $zip_files;

	if ($text) {
		if (!array_key_exists($zip_path, $zip_files)) {
			$zip_files[$zip_path] = true;
			if (!$zip->addFromString($zip_path, $file)) {
				error_log("Unable to create binary file $zip_path");
//			} else {
//				error_log("Try to write zip file $zip_path (...), status: " . $zip->getStatusString() . ", numFiles: " . $zip->numFiles);
			}
		}
	} else {
		$zip_files[$zip_path] = $file;
	}
}

$current_asset_path;
$image_size_cache = Array();

function add_zip_html($zip_path, $html) {
	//add_asset("include", "editor/activity/build/activity.min.js"); // TODO: Get the correct version
	//add_asset("include", "editor/activity/build/activity.css"); // TODO: Get the correct version

	$rx_assets = "/([\"'\(])\.\.\/(assets\/(\d+)\/(.*?))(\g{1}|\))/";
	$replace_assets = function ($matches) {
		global $zip_files, $relative_path, $current_asset_path;

		$path = $matches[2];
		$current_asset_path = $matches[3];
		$crcid = "assets/" . substr("00000000" . dechex(crc32($current_asset_path)), -8);
		$filename = $matches[4];
		$newpath = (empty($relative_path) ? "" : "$relative_path/") . "$crcid/$filename";

		if (!array_key_exists($crcid, $zip_files)) {
			add_asset($crcid, $path, $relative_path);
		}
		return "$matches[1]$newpath$matches[5]";
	};

	$rx_activity = "/([\"\'])activity\/(version\/)?(build|latest|v[\d\.]+)\/activity\.(?:min\.)?(css|js)([\"\'])/";
	$replace_activity = function ($matches) {
		global $zip_files, $relative_path;

		if ($matches[4] === "css") {
			$filename = "activity.css";
		} else {
			$filename = "activity.min.js";
		}
		$newpath = (empty($relative_path) ? "" : "$relative_path/") . "include/$filename";
		if (!array_key_exists($filename, $zip_files)) {
			add_asset("include", "editor/activity/$matches[2]$matches[3]/$filename");
		}
		return "$matches[1]$newpath$matches[1]";
	};

	$rx_img = "#(<(img)\s)([^>]*)(src)=([\"'])\.\.\/assets\/(.*?)\g{5}([^>]*>)#i";
	$replace_imgs = function ($matches) {
		global $cfg, $image_size_cache, $min_image_width, $min_image_height, $min_image_bytes;

		if (!empty($cfg["lazy_load"])) {
			$filename = $matches[6];
			if (!array_key_exists($filename, $image_size_cache)) {
				$filepath = "assets/$filename";
				if (file_exists($filepath)) {
					$size = $image_size_cache[$filename] = getimagesize($filepath);
					if (filesize($filepath) < $min_image_bytes || $size[0] < $min_image_width || $size[1] < $min_image_height) {
						$image_size_cache[$filename] = false;
					}
				} else {
					$image_size_cache[$filename] = false;
				}
			}
			$size = $image_size_cache[$filename];
			if (!empty($size)) {
				$width = $size[0];
				$height = $size[1];

//				$svg = "data:image/svg+xml;charset=utf8," . "<svg xmlns='http://www.w3.org/2000/svg' width='$width' height='$height'></svg>";
//				$svg = "data:image/svg+xml;base64," . base64_encode("<svg xmlns='http://www.w3.org/2000/svg' width='$width' height='$height'/>");
				// src=$matches[5]$svg$matches[5]
				return str_replace("srcset=", "data-srcset=", //
						str_replace("sizes=", "data-sizes=", //
								"$matches[1]$matches[3]width=$matches[5]$width$matches[5] height=$matches[5]$height$matches[5] data-src=$matches[5]../assets/$filename$matches[5]$matches[7]"));
			}
		}
		return $matches[0];
	};

	$data = preg_replace_callback($rx_assets, $replace_assets, //
			preg_replace_callback($rx_img, $replace_imgs, //
					preg_replace($rx_activity, "activity.js", //
							preg_replace_callback($rx_activity, $replace_activity, //
									$html))));

	add_zip($zip_path, $data, true);
}

/**
 * Check if a file already exists in the zip
 * @global array $zip_files
 * @param string $zip_path
 * @return boolean
 */
function has_zip($zip_path) {
	global $zip_files;

	return array_key_exists($zip_path, $zip_files);
}

/**
 * Get a list of all files in the folder and any subfolders
 * @param string $pattern
 * @param number $flags
 * @return array
 */
function rglob($pattern, $flags = 0) {
	$files = glob($pattern, $flags);
	foreach (glob(dirname($pattern) . "/*", GLOB_ONLYDIR | GLOB_NOSORT) as $dir) {
		$files = array_merge($files, rglob($dir . "/" . basename($pattern), $flags));
	}
	return $files;
}

function uuid($name) {
	$hash = md5("uk.co.infuze.abc." . $name);
	return sprintf('%08s-%04s-%04x-%04x-%12s',
			// 32 bits for "time_low"
			substr($hash, 0, 8),
			// 16 bits for "time_mid"
			substr($hash, 8, 4),
			// 16 bits for "time_hi_and_version",
			// four most significant bits holds version number 3
			(hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x3000,
			// 16 bits, 8 bits for "clk_seq_hi_res",
			// 8 bits for "clk_seq_low",
			// two most significant bits holds zero and one for variant DCE1.1
			(hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,
			// 48 bits for "node"
			substr($hash, 20, 12)
	);
}

/**
 * Add all cached files to the zip - ensure last gets in
 * @global ZipArchive $zip
 * @global array $zip_files
 */
function add_zip_files() {
	global $zip, $zip_files;

	foreach ($zip_files as $file => $zip_path) {
		if ($zip_path !== true) {
			if (!$zip->addFile($file, $zip_path)) {
				error_log("Unable to add $zip_path");
//			} else {
//				error_log("Try to write zip file $zip_path ($file), status: " . $zip->getStatusString() . ", numFiles: " . $zip->numFiles);
			}
		}
	}
}

/**
 * Add an asset to the output folder and the assets array
 * @param string $path in zip
 * @param string $file to add
 * @param string $relative_path to add to $assets array
 * @param array $assets
 * @return array
 */
function add_asset($path, $file, $relative_path = "", array &$assets = null) {
	global $zip_files;

//	error_log("add_asset($path -> $file)");
	$basename = basename($file);
	$ext = strtolower(pathinfo($file, PATHINFO_EXTENSION));
	if ($ext === "js") { // preg_match("/activity\.js$/i", $basename)
		if (is_array($assets)) { // only minify asset files
			update_js($file);
			$file = preg_replace("/(?<!\.min)\.js$/i", ".min.js", $file);
			$basename = basename($file);
		}
	}
	if (!array_key_exists($path, $zip_files) && file_exists($file)) {
		$pathname = "$path/$relative_path";
		$filename = "$pathname$basename";
		$fullpath = "$relative_path$basename";

		if (is_array($assets)) {
			if (!in_array($fullpath, $assets)) {
				$assets[] = $fullpath;
			}
		}
		if ($ext === "js" || $ext === "css") {
			$source = preg_replace_callback("/([\"'\(])\.\.\/(assets\/(\d+)\/(.*?))(\g{1}|\))/", function ($matches) use ($zip_files, $relative_path) {
				$path = $matches[2];
				$crcid = "assets/" . substr("00000000" . dechex(crc32($matches[3])), -8);
				$filename = $matches[4];
				$newpath = (empty($relative_path) ? "" : "$relative_path/") . "$crcid/$filename";

				if (!array_key_exists($crcid, $zip_files)) {
					add_asset($crcid, $path, $relative_path);
				}
				return $matches[1] . $newpath . $matches[5];
			}, file_get_contents($file));
			if ($ext === "css") {
				$dirname = dirname($file);
				$source = preg_replace_callback("#(url\(([\"']?))(?![\"']?data:)(.*?)(\g{2}\))#i", function ($matches) use ($dirname, $pathname) {
					$url = $local = $matches[3];
					if (strpos($local, "http") !== 0 && strpos($local, "//") === false) {
						$url = fixpath($dirname . "/" . $local);
					}
					if (file_exists($url) && filesize($url) < 10240) {
						$contents = file_get_contents($url);
						if (pathinfo($url, PATHINFO_EXTENSION) === "svg") {
							$mime = "image/svg+xml";
						} else {
							$mime = mime_content_type($url);
						}
						$local = "data:" . $mime . ";base64," . base64_encode($contents);
					} else {
						add_asset($pathname, $url);
					}
					//error_log("Add asset file $matches[0], $local");
					return $matches[1] . $local . $matches[4];
				}, $source);
			}
			add_zip($filename, $source, true);
		} else {
			add_zip($file, $filename);
		}
	}
	return $assets;
}

/**
 * Add a url to the zip, fixing any relative paths and grabbing any assets
 * @global array $extern_error
 * @param string $url
 */
function add_extern($url) {
	global $zip_files;
//	error_log("add_extern(" . $url . ")");
	$filename = preg_replace("/[#\?].*$/", "", preg_replace("#^(.*/_html/)#", "/_html/", $url));

	if (!array_key_exists($filename, $zip_files)) {
		if (file_exists($url)) {
//			$pathname = dirname($filename);
			$base = preg_replace("#^(.*/)(_html/.*)$#", "$1", $url);
			$path = dirname($url) . "/";
			// Fix HTML
			$data = preg_replace_callback("#(<(?:img|script|link|a|source)\s)([^>]*(?:src|href)=([\"']))(?!data)(.*?)(\g{3}[^>]*>)#i", function ($matches) use ($base, $path) {
				global $extern_assets;
				$url = $local = $matches[4];
				if ($local[0] === "." || ($local[0] === "/" && $local[1] !== "/") || strpos($local, "http") !== 0) {
					$url = fixpath($path . $local);
				}
				if (strpos($url, $base . "_html/") === 0) {
					$extern_assets[] = $url;
				}
				return $matches[1] . $matches[2] . $local . $matches[5];
			},
					// Fix CSS
					preg_replace_callback("#(url\(([\"']?))(?![\"']?data:)(.*?)(\g{2}\))#i", function ($matches) use ($base, $path) {
						global $extern_assets;
						$url = $local = $matches[3];
						if (strpos($local, "http") !== 0 && strpos($local, "//") !== 0 && ($local[0] === "." || $local[0] === "/")) {
							$url = fixpath($path . $local);
						}
						if (strpos($url, $base . "_html/") === 0) {
							$extern_assets[] = $url;
						}
						return $matches[1] . $local . $matches[4];
					}, file_get_contents($url))
			);
//			error_log("add_extern($filename)");
			add_zip($filename, $data, true);
		} else {
			global $extern_error;
//			error_log("ERROR: add_extern() - unable to open " . $url);
			$extern_error[] = "Missing file: $filename ($url)";
		}
	}
}

function publish($id, $html) {
	global $zip, $zip_files, $assets, $extern_assets, $extern_relative, $extern_error, $time_limit;

	$zip = new ZipArchive();
	$crc = dechex(crc32("$id"));
	$zip_name = "publish/$crc.zip";
	$date = date("Y-m-d_H-i-s");

	$res = $zip->open($zip_name, ZIPARCHIVE::CREATE | ZIPARCHIVE::OVERWRITE);
	if ($res !== true) {
		error_log("Can't create zip: $res");
		header("Content-Type: text/plain");
		die('{"error":"Can\'t create zip"}');
	}

	$assets = array();
	$extern_assets = array();
	$extern_relative = "";
	$extern_error = array();

	$zip_files = array();

	add_zip_html("index.html", "<!DOCTYPE html>\n<html style=\"visibility:hidden;\" data-uid=\"$crc\">$html</html>");

	while (list($key, $value) = each($extern_assets)) { // To let us add to the list as we're running
		add_extern($value);
	}

	if (count($extern_error)) {
		add_zip("/errors.txt", implode("\n", $extern_error), true);
	}

	add_zip_files();
	set_time_limit($time_limit);
	if (!$zip->close()) {
		error_log("Unable to write zip file $zip_name, " . $zip->getStatusString());
	} else {
		copy($zip_name, "publish/$crc.$date.zip");
	}

	if ($zip->open($zip_name)) {

		function delTree($dir) {
			$files = array_diff(scandir($dir), array(".", ".."));
			foreach ($files as $file) {
				(is_dir("$dir/$file")) ? delTree("$dir/$file") : unlink("$dir/$file");
			}
			return rmdir($dir);
		}

		if (!is_dir("publish/$crc")) {
			mkdir("publish/$crc");
		}
		delTree("publish/$crc/");
		$zip->extractTo("publish/$crc");
		$zip->extractTo("publish/$crc.$date");
		$zip->close();
		$node = get_node($id)[0];
		file_put_contents("publish/$crc/.htaccess", //
				"Options Indexes FollowSymLinks\n"
				. "<filesMatch \"\.(html|htm|js|css)$\">\n"
				. "FileETag None\n"
				. "<ifModule mod_headers.c>\n"
				. "Header unset ETag\n"
				. "Header set Cache-Control \"max-age=0, no-cache, no-store, must-revalidate\"\n"
				. "Header set Pragma \"no-cache\"\n"
				. "Header set Expires \"Wed, 11 Jan 1984 05:00:00 GMT\"\n"
				. "</ifModule>\n"
				. "</filesMatch>\n"
				. "<IfModule mod_autoindex.c>\n"
				. "IndexOptions IgnoreClient\n"
				. "IndexIgnore favicon.ico header.html footer.html .* _common assets\n"
				. "HeaderName header.html\n"
				. "ReadmeName footer.html\n"
				. "</IfModule>\n");
		file_put_contents("publish/$crc/header.html", "<h1>" . (empty($node["_name"]) ? $node["_id"] : $node["_name"]) . "</h1><style>ul{line-height:1.3;list-style:none;columns:300px;-webkit-columns:300px;-moz-columns:300px;}</style>\n");
		file_put_contents("publish/$crc/footer.html", //
				"<address>Generated by Infuze Creator " . get_version() . " (" . date("l dS M Y H:i:s") . ")</address>\n"
				. "<script>var i,items=[].slice.call(document.getElementsByTagName('li'))"
				. ".sort(function(a,b){"
				. "a=a.innerHTML;"
				. "b=b.innerHTML;"
				. "return a===b?0:(a>b?1:-1);"
				. "}),"
				. "list=items[0].parentNode;"
				. "for(i=0;i<items.length;i++){"
				. "list.appendChild(items[i]);"
				. "}</script>");
		copy("publish/$crc/.htaccess", "publish/$crc.$date/.htaccess");
		copy("publish/$crc/header.html", "publish/$crc.$date/header.html");
		copy("publish/$crc/footer.html", "publish/$crc.$date/footer.html");
		log_action($id, "publish", get_version());
	} else {
		error_log("Unable to publish activities, " . $zip->getStatusString());
	}

	return array("success" => "Published");
}
