# Infuze Creator

An activity creation system using HTML5.

## Server

This is the Server section, responsible for setting and getting data from the database, and building activities when needed.

Uses "Access-Control-Allow-Origin" to limit where can access this data.

Allows the editor as a submodule, but doesn't require it.

--

*See roadmap document for details.*

Copyright &copy; 2016 Infuze, All Rights Reserved.
