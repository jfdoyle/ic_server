///<reference path="../editor/activity/src/api.d.ts" />
///<reference path="../editor/activity/src/prototypes/number/toRoman.d.ts" />
///<reference path="../editor/activity/src/prototypes/string/compress.d.ts" />
///<reference path="../editor/activity/src/prototypes/string/decompress.d.ts" />
///<reference path="../editor/activity/src/prototypes/string/regex.d.ts" />
/*
 * Copyright &copy; 2016-2017 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 *
 * Rising Stars Assessment (Hodder) wrapper for Infuze Creator
 * Accesses IC API to get score and state data
 * Sends and gets data from AP web service
 */
(function (document, window) {
    /**
     * Disabled on these domains.
     * To add a domain simply base64 encode it - <code>btoa("ic.infuze.uk")</code>
     */
    var DISABLED = [
        "aWMuaW5mdXplLnVr" // ic.infuze.uk
    ];
    /**
     * Save all answers to current question, otherwise only save the ones that
     * have changed.
     */
    var SAVE_ALL_ANSWERS = false;
    /**
     * DEV: http://development.assessmentplus-risingstars.mmtdigital.co.uk/api
     * LIVE: http://my.risingstarsassessment.co.uk/api
     */
    var SERVER = "/api";
    /**
     * Prefix selector for popup boxes
     */
    var BOXPREFIX = ".ict-box-";
    /**
     * Number of seconds between state saves
     */
    var DEBOUNCE = 30;
    /**
     * Number of seconds to timeout Ajax
     */
    var TIMEOUT = 30;
    /**
     *  How often to save instead of PING
     */
    var SAVECOUNT = 1;
    var Mode;
    (function (Mode) {
        Mode[Mode["NORMAL"] = 0] = "NORMAL";
        Mode[Mode["REVIEW"] = 1] = "REVIEW";
        Mode[Mode["VIEW"] = 2] = "VIEW"; // Normal timer, no saving
    })(Mode || (Mode = {}));
    ;
    var Response;
    (function (Response) {
        Response[Response["OK"] = 0] = "OK";
        Response[Response["ERROR"] = 1] = "ERROR";
        Response[Response["EXPIRED"] = 2] = "EXPIRED"; // Token expired - TODO: pupil needs to re-launch test?
    })(Response || (Response = {}));
    ;
    var ServerAction;
    (function (ServerAction) {
        ServerAction[ServerAction["PING"] = 0] = "PING";
        ServerAction[ServerAction["STATE"] = 1] = "STATE";
        ServerAction[ServerAction["SCORE"] = 2] = "SCORE";
        ServerAction[ServerAction["END"] = 3] = "END";
    })(ServerAction || (ServerAction = {}));
    var ErrorState;
    (function (ErrorState) {
        ErrorState[ErrorState["NONE"] = 0] = "NONE";
        ErrorState[ErrorState["LOADING"] = 1] = "LOADING";
        ErrorState[ErrorState["CONNECTING"] = 2] = "CONNECTING";
        ErrorState[ErrorState["DEAD"] = 3] = "DEAD";
    })(ErrorState || (ErrorState = {}));
    function rsa() {
        var countIC = 0;
        if (!window.IC) {
            if (countIC++ < 10) {
                setTimeout(rsa, 500);
            }
            else {
                console.error("RSA Error: No Infuze Creator API available!");
            }
            return;
        }
        var IC = window.IC, connected = false, count = 0, body = document.body, 
        // Data for the current session
        location = window.location, token = ((location && location.search || "").match(/t=([a-z0-9\-]+)/) || [])[1], mode = 2 /* VIEW */, // Mode of the activities
        sessionId = 0, // Used to save any data for this test including state and answers
        paper = "", // ?
        name = "", // Pupil name
        premium = false, // Do they have premium?
        time = 0, // Time for the test
        extended = 0, // Added time as a premium pupil
        totalTime = 0, // Total time including premium if allowed
        completeUrl, pendingReply, currentState, lastState, currentScore, lastScore, serverScore = {}, sendingScore = {}, pendingScore = {}, attempted = {}, needScore, needEnd, currentXHR, timeoutXHR, action = 0 /* PING */, error = 0 /* NONE */, answerSequence = 1, debounceTimer, currentIndex, nextIndex;
        function addEventListeners(selector, listenerType, listener) {
            Array.from(document.querySelectorAll(selector)).forEach(function (el) {
                el.addEventListener(listenerType, listener);
            });
        }
        function removeEventListeners(selector, listenerType, listener) {
            Array.from(document.querySelectorAll(selector)).forEach(function (el) {
                el.removeEventListener(listenerType, listener);
            });
        }
        function displayNoTokenWarning() {
            console.warn("RSA: Not loading via website, aborting.");
        }
        function ajax(query, data) {
            if (token) {
                console.info("RSA: AJAX request", SERVER + (query || ""), data);
                try {
                    var xhr_1 = currentXHR = new XMLHttpRequest();
                    xhr_1.open(data ? "POST" : "GET", SERVER + (query || ""), true);
                    xhr_1.setRequestHeader("Content-type", "application/json; charset=utf-8");
                    xhr_1.setRequestHeader("X-Requested-With", "XMLHttpRequest");
                    xhr_1.onreadystatechange = function () {
                        if (this.readyState !== XMLHttpRequest.DONE) {
                            // We don't care until it's finished
                            return;
                        }
                        pendingReply = false;
                        clearTimeout(timeoutXHR);
                        timeoutXHR = undefined;
                        if (this.status && this.status !== 200) {
                            setState(2 /* CONNECTING */);
                        }
                        else if (currentXHR !== xhr_1) {
                            console.error("RSA: Received out-of-order response!");
                            return;
                        }
                        else if (this.response) {
                            var response = typeof this.response === "object" && this.response.responseCode ? this.response : JSON.parse(this.responseText);
                            console.info("RSA: AJAX response", response);
                            setState(0 /* NONE */);
                            switch (response.responseCode) {
                                case 0 /* OK */:
                                    if (response.markSession) {
                                        connected = true;
                                        sessionId = response.markSession;
                                        paper = response.paper || "";
                                        name = response.name || "---";
                                        premium = response.pupilPremium;
                                        time = response.time || 0;
                                        extended = response.extendedTime || 0;
                                        totalTime = time + (premium ? extended : 0);
                                        completeUrl = response.completeUrl;
                                        mode = response.mode;
                                        lastState = response.blobData;
                                        body.normalize();
                                        for (var walker = document.createNodeIterator(body, NodeFilter.SHOW_TEXT), node = walker.nextNode(); node; node = walker.nextNode()) {
                                            var text = node.textContent, newText = text.replace(/%%(name|time)%%/g, function ($0, $1) {
                                                switch ($1) {
                                                    case "name":
                                                        return name;
                                                    case "time":
                                                        return String(Math.floor(totalTime));
                                                }
                                                return ""; // Cannot get here, but things might change
                                            });
                                            if (text !== newText) {
                                                node.textContent = newText;
                                            }
                                        }
                                        if (lastState && mode !== 2 /* VIEW */) {
                                            IC.state(lastState);
                                            setTimeout(function () {
                                                // Scoring is asynchronous, so need to wait for it to finish
                                                serverScore = getAnswers();
                                            }, 500);
                                            if (mode === 0 /* NORMAL */) {
                                                showBox("continue");
                                            }
                                        }
                                        if (totalTime) {
                                            IC.timer(totalTime);
                                        }
                                        switch (mode) {
                                            case 0 /* NORMAL */:
                                                console.info("RSA: Starting in NORMAL mode.");
                                                body.addEventListener("ic-timeout", endTest);
                                                body.addEventListener("ic-screen", saveScore);
                                                body.addEventListener("click", clickEndTest);
                                                addEventListeners(".ict-screen-once .ict-button-next", "click", startTest);
                                                window.addEventListener("beforeunload", forceSaveState);
                                                window.addEventListener("unload", forceSaveState);
                                                setState(0 /* NONE */);
                                                break;
                                            case 2 /* VIEW */:
                                                connected = false;
                                                console.info("RSA: Starting in VIEW mode.");
                                                addEventListeners(".ict-screen-once .ict-button-next", "click", startTest);
                                                setState(0 /* NONE */);
                                                break;
                                            case 1 /* REVIEW */:
                                                var screens = body.querySelectorAll("ic-screens>ic-screen"), lastScreen = screens[screens.length - 1];
                                                connected = false;
                                                console.info("RSA: Starting in REVIEW mode.");
                                                if (lastScreen.icWidget) {
                                                    lastScreen.icWidget.addState("disabled");
                                                }
                                                IC.pause();
                                                IC.resume(true);
                                                IC.resume = function () { return 0; }; // Force this to be un-resumable
                                                setState(0 /* NONE */);
                                                IC.goto(1); // Go to the first screen, screen 0 is the Start screen
                                                break;
                                        }
                                    }
                                    else {
                                        switch (action) {
                                            case 3 /* END */:
                                                needEnd = connected = false;
                                                if (completeUrl) {
                                                    location.replace(completeUrl);
                                                }
                                            case 2 /* SCORE */:
                                                needScore = false;
                                                currentIndex = nextIndex;
                                                lastScore = currentScore;
                                                if (sendingScore) {
                                                    Object.keys(sendingScore).forEach(function (question) {
                                                        serverScore[question] = sendingScore[question];
                                                    });
                                                }
                                                sendingScore = undefined;
                                            case 1 /* STATE */:
                                                count = 0;
                                                lastState = currentState;
                                            case 0 /* PING */:
                                                if (pendingScore || needScore || needEnd) {
                                                    keepAlive();
                                                }
                                                break;
                                        }
                                    }
                                    return;
                                case 1 /* ERROR */:
                                    setState(3 /* DEAD */);
                                    console.error("RSA Error: Cannot connect!", response);
                                    return;
                                case 2 /* EXPIRED */:
                                    setState(3 /* DEAD */);
                                    console.error("RSA Token Expired: Cannot connect!", response);
                                    return;
                            }
                        }
                    };
                    pendingReply = true;
                    xhr_1.send(data ? JSON.stringify(data) : undefined);
                    timeoutXHR = setTimeout(function () {
                        setState(2 /* CONNECTING */);
                        console.error("RSA: AJAX request timeout!");
                        pendingReply = false;
                        keepAlive();
                    }, TIMEOUT * 1000);
                    clearTimeout(debounceTimer);
                    debounceTimer = setTimeout(debounce, DEBOUNCE * 1000);
                    return xhr_1;
                }
                catch (e) {
                    console.error("RSA Error: Can't create XMLHttpRequest", e);
                }
            }
        }
        function showBox(selector) {
            var el = document.querySelector(BOXPREFIX + selector);
            if (el) {
                el.style.display = "block";
            }
        }
        function hideBox(selector) {
            var el = document.querySelector(BOXPREFIX + selector);
            if (el) {
                el.style.display = "";
            }
        }
        function setState(state) {
            if (error === state || error === 3 /* DEAD */) {
                return;
            }
            // Open new dialogs
            switch (state) {
                case 0 /* NONE */:
                    if (error === 2 /* CONNECTING */) {
                        showBox("ok");
                    }
                    IC.resume(true);
                    break;
                case 1 /* LOADING */:
                    showBox("loading");
                    IC.pause();
                    break;
                case 2 /* CONNECTING */:
                    if (connected) {
                        showBox("warning");
                        IC.pause();
                        break;
                    }
                case 3 /* DEAD */:
                    showBox("error");
                    IC.pause();
                    break;
            }
            // Close old dialogs
            switch (error) {
                case 0 /* NONE */:
                    hideBox("ok");
                    break;
                case 1 /* LOADING */:
                    hideBox("loading");
                    break;
                case 2 /* CONNECTING */:
                    hideBox("warning");
                    break;
            }
            error = state;
        }
        /**
         * Get the current list of answers as a {question:value} object. Also
         * sets the global "attempted" key for that answer.
         */
        function getAnswers() {
            var tempScore = {};
            IC.score().forEach(function (mark) {
                for (var i = 0, points = mark.points; i < points; i++) {
                    var question = mark["id"].replace(/^(\d+)(.*)$/, function ($0, $1, $2) {
                        return (parseInt($1, 10) - 1) + ($2 || "");
                    }) + (points > 1 ? (i + 1).toRoman().toLowerCase() : ""), value = mark.score > i ? 1 : 0, realValue = mark.attempted ? value : "N";
                    if ((!sendingScore || sendingScore[question] !== realValue) && (SAVE_ALL_ANSWERS || serverScore[question] !== realValue)) {
                        var screen_1 = question.replace(/[^\d]/g, "");
                        if (tempScore[question]) {
                            console.warn("RSA Error: Duplicate question ID!", question);
                        }
                        attempted[screen_1] = attempted[screen_1] || mark.attempted;
                        tempScore[question] = serverScore[question] = realValue;
                    }
                }
            });
            return tempScore;
        }
        /**
         * Prevent the API from spamming the server with data, make sure it can
         * only send at most every 30 seconds.
         * If we're still waiting to hear back from the API then we skip this as
         * the timeout and/or response will trigger it again anyway.
         */
        function debounce() {
            if (!timeoutXHR) {
                if (sendingScore) {
                    Object.keys(sendingScore).forEach(function (question) {
                        serverScore[question] = sendingScore[question];
                    });
                }
                debounceTimer = sendingScore = undefined;
                if (pendingScore || needScore || needEnd) {
                    keepAlive();
                }
            }
        }
        function keepAlive(forceState) {
            if (connected && !pendingReply) {
                var scoreData_1 = [], time_1 = IC.timer() / 60;
                if (typeof forceState === "number") {
                    action = forceState;
                }
                else if (needEnd) {
                    action = 3 /* END */;
                }
                else if (needScore || sendingScore || pendingScore) {
                    action = 2 /* SCORE */;
                }
                else if (count++ < SAVECOUNT) {
                    action = 0 /* PING */;
                }
                else {
                    action = 1 /* STATE */;
                }
                if (action === 0 /* PING */) {
                    currentState = undefined;
                }
                else {
                    currentState = IC.state();
                }
                if (action === 0 /* PING */ || action === 1 /* STATE */) {
                    currentScore = undefined;
                }
                else {
                    /*
                     * Multiple states for the score, all are {question:value}
                     *
                     * Locally here we work out "tempScore".
                     * We also store "serverScore" for what we believe the server knows.
                     * When sending to the server "tempScore" gets copied to "sendingScore".
                     * If currently sending (within the debounce timer) "tempScore" gets copied to "pendingScore".
                     * When the debounceTimer finishes,
                     *  1. we check if "sendingScore" still exists, and use that instead for "tempScore" if so - we're retrying the last one.
                     *  2. or we check if "pendingScore" exists, and use that instead for "tempScore" if so.
                     */
                    var tempScore_1;
                    if (action === 3 /* END */) {
                        sendingScore = pendingScore = undefined;
                    }
                    if (sendingScore && timeoutXHR) {
                        // Can only hit this if we had a timeout
                        tempScore_1 = sendingScore;
                    }
                    else if (pendingScore && !debounceTimer) {
                        tempScore_1 = pendingScore;
                        pendingScore = undefined;
                    }
                    else {
                        tempScore_1 = getAnswers();
                    }
                    Object.keys(tempScore_1).forEach(function (question) {
                        if (attempted[question.replace(/[^\d]/g, "")]) {
                            scoreData_1.push({
                                question: question,
                                mark: tempScore_1[question]
                            });
                        }
                    });
                    if (action !== 3 /* END */ && !scoreData_1.length && debounceTimer) {
                        pendingScore = tempScore_1;
                        return;
                    }
                    sendingScore = tempScore_1;
                    currentScore = JSON.stringify(scoreData_1);
                    scoreData_1.length && console.info("RSA: Sending score", scoreData_1.map(function (data) { return data.question + ":" + data.mark; }).join(", "));
                }
                switch (action) {
                    case 3 /* END */:
                        ajax("/infuze/testcomplete", {
                            markSessionId: sessionId,
                            blobData: currentState,
                            timeTaken: time_1 >= totalTime ? -1 : time_1,
                            answers: scoreData_1
                        });
                        break;
                    case 2 /* SCORE */:
                    case 1 /* STATE */:
                        if (lastState !== currentState) {
                            ajax("/infuze/questiondata", {
                                markSessionId: sessionId,
                                blobData: currentState,
                                timeTaken: time_1,
                                answerSequence: answerSequence++,
                                answers: scoreData_1
                            });
                            break;
                        }
                    case 0 /* PING */:
                        break;
                }
            }
        }
        function saveScore(event) {
            if (event && event.icData) {
                currentIndex = event.icData.oldIndex;
                nextIndex = event.icData.index;
            }
            needScore = true;
            keepAlive(2 /* SCORE */);
        }
        function forceSaveState() {
            clearTimeout(debounceTimer);
            sendingScore = pendingScore = debounceTimer = undefined;
            pendingReply = false;
            keepAlive(2 /* SCORE */);
            window.removeEventListener("beforeunload", forceSaveState);
            window.removeEventListener("unload", forceSaveState);
        }
        function startTest(event) {
            IC.resume();
        }
        function clickEndTest(event) {
            var target = event.target;
            if (target && target.closest(".ict-button-end")) {
                endTest();
            }
        }
        function endTest() {
            body.removeEventListener("ic-timeout", endTest);
            body.removeEventListener("ic-screen", saveScore);
            body.removeEventListener("click", clickEndTest);
            removeEventListeners(".ict-screen-once .ict-button-next", "click", startTest);
            window.removeEventListener("beforeunload", forceSaveState);
            window.removeEventListener("unload", forceSaveState);
            IC.pause();
            needEnd = true;
            keepAlive(3 /* END */);
            // Navigation occurs after the ajax call has completed
        }
        if (token) {
            setState(1 /* LOADING */);
            ajax("/infuze/marksessionparams?token=" + token);
        }
        else if (location.pathname !== "srcdoc" && DISABLED.indexOf(btoa(location.host)) < 0) {
            IC.pause(true);
            showBox("error");
            setTimeout(displayNoTokenWarning, 1);
        }
        else if (!window.ic_edit) {
            var lastScore_1 = "";
            console.info("RSA: No server token supplied, starting in VIEW mode.");
            setInterval(function () {
                var questions = [];
                IC && IC.score().forEach(function (mark) {
                    var i = 0, points = mark.points, question = mark["id"].replace(/^(\d+)(.*)$/, function ($0, $1, $2) {
                        return (parseInt($1, 10) - 1) + ($2 || "");
                    });
                    for (; i < points; i++) {
                        questions.push(question + (points > 1 ? (i + 1).toRoman().toLowerCase() : "") + ":" + (mark.score > i ? 1 : 0));
                    }
                });
                var newScore = questions.join(", ");
                if (lastScore_1 !== newScore) {
                    lastScore_1 = newScore;
                    console.info("RSA:", lastScore_1);
                }
            }, 1000);
        }
        IC.rsaComplete = function () {
            if (connected) {
                needEnd = true;
            }
            else {
                console.warn("RSA: Not connected to API!");
            }
        };
    }
    if (/complete|loaded|interactive/.test(document.readyState) && document.body) {
        rsa();
    }
    else {
        document.addEventListener("DOMContentLoaded", rsa, false);
    }
})(document, window);
//# sourceMappingURL=rsa.js.map