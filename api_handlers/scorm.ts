///<reference path="../editor/activity/src/api.d.ts" />
///<reference path="scorm_2004.d.ts" />
/** @license
 * Copyright (C) 2013-2015 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Date {
	format(format?: string): string;
}

(function() {
	var DEBUG = true && console && console.log,
		version = "SCORM_2004 v0.0.1";

	DEBUG && console.log(version);

	/*
	 * SCORM API handle, quickly check this if we want scorm stuff
	 */
	var scormAPI: SCORM_2004_API, // The scorm API being used
		scormCommitTimer: any = 0, // Set when we're going to commit
		loadActivity, // Can be set through cmi.location - automatically if not a journal and no location.hash
		wrapper = false, // Are we within a scorm wrapper that handles init/exit
		journal = false, // Are we journalling the cmi.objectives
		isReadOnly = false, // Do we have to go read-only?
		commit = 30, // How often (at most) should we be committing our data (seconds)
		_scormID: {[id: string]: string} = {}, // Per activity scorm ID
		_interactionIndex: {[id: string]: number} = {}, // Per activity cmi.interactions.<id>
		_objectiveIndex: {[id: string]: number} = {}, // Per activity cmi.objectives.<id>
		_journalIndex: {[id: string]: number} = {}, // Per activity cmi.interactions.*.objectives.<id>
		data: {[key: string]: string | number} = {}, // Per activity persist data cache
		_global = {// Global data cache
			"state": {}
		};

	/**
	 * Initialize communication with LMS by calling the Initialize function which will be implemented by the LMS.
	 */
	function scormInitialize() {
		try {
			var win = window,
				apiName = "API_1484_11";

			while (!win[apiName] && win.parent && win.parent !== win) {
				win = win.parent;
			}
			if (!win[apiName] && win.opener) {
				win = win.opener;
				while (!win[apiName] && win.parent && win.parent !== win) {
					win = win.parent;
				}
			}
			scormAPI = win[apiName];
		} catch (e) {
			// Probably a security error
		}
		if (scormAPI) {
			scormAPI.GetValue("cmi.suspend_data");
			if (parseFloat(scormAPI.GetLastError()) !== 122) { // _RetrieveDataBeforeInitialization
				DEBUG && console.log("SCORM Already Initialised!");
			} else if (wrapper) {
				DEBUG && console.log("SCORM Wrapper Should Initialise Now");
			} else if (scormAPI.Initialize("") === "true") {// Yes, it uses strings rather than booleans...
				DEBUG && console.log("SCORM Initialised");
			} else {
				scormErrorHandler();
				scormAPI = null;
			}
		} else {
			console.log("Unable to find SCORM!");
		}
		if (scormAPI) { // Can be cleared in previous if() so can't use else
			document.addEventListener("unload", scormTerminate);
			document.addEventListener("beforeunload", scormTerminate);
			if (scormGetValue("cmi.mode") === "normal") {
				scormSetValue("cmi.exit", "suspend");
			}
			return true;
		}
		DEBUG && console.log("SCORM Disabled");
		return false;
	}

	/**
	 * Call the Commit function
	 */
	function scormCommit() {
		if (scormAPI) {
			DEBUG && console.log("scormCommit")
			clearTimeout(scormCommitTimer);
			scormCommitTimer = 0;
			if (scormAPI.Commit("") !== "true") {
				scormErrorHandler();
			}
		}
	}

	/**
	 * Close communication with LMS by calling the Terminate function which will be implemented by the LMS
	 */
	function scormTerminate() {
		if (scormAPI) {
			if (scormGetValue("cmi.mode") === "normal") {
				scormCommit();
			}
			if (wrapper) {
				DEBUG && console.log("SCORM Wrapper Should Terminate Now");
			} else {
				// call the Terminate function that should be implemented by the API
				if (scormAPI.Terminate("") === "true") {// Yes, it uses strings rather than booleans...
					DEBUG && console.log("SCORM Terminated");
					scormAPI = null;
				}
			}
		}
	}

	/**
	 * Create a time interval string from a number of seconds, or convert string to seconds
	 * @param {(String|Number)} seconds
	 * @returns {(Number|String)}
	 */
	function scormTimeInterval(seconds: string | number): string | number {
		if (typeof seconds === "string") {
			return parseInt(seconds.replace(/^PT(?:(\d+)D)?(?:(\d+)H)?(?:(\d+)M)?(?:(\d+)S)?$/, function($0: string, $1: string, $2: string, $3: string, $4: string) {
				return String((parseInt($1 || "0", 10) * 24 * 60 * 60) + (parseInt($2 || "0", 10) * 60 * 60) + (parseInt($3 || "0", 10) * 60) + parseInt($4 || "0", 10));
			}), 10);
		} else {
			var minutes = Math.floor(seconds / 60),
				hours = Math.floor(minutes / 60),
				days = Math.floor(hours / 24),
				pad = function(num: number) {
					return num <= 9 ? "0" + num : "" + num;
				},
				pad4 = function(num: number) {
					return num > 9999 ? "9999" : num > 999 ? "" + num : num > 99 ? "0" + num : num > 9 ? "00" + num : "000" + num;
				};

			hours = hours % 24;
			minutes = minutes % 60;
			seconds = Math.floor(seconds % 60);
			return "PT" + (days ? pad4(days) + "D" : "") + (hours ? pad(hours) + "H" : "") + (minutes ? pad(minutes) + "M" : "") + pad(seconds) + "S";
		}
	}

	/**
	 * Wraps the call to the GetValue method
	 * @param {string} name - string representing the cmi data model defined category or element (e.g. cmi.core.student_id)
	 * Return:  The value presently assigned by the LMS to the cmi data model
	 *       element defined by the element or category identified by the name
	 *       input value.
	 */
	function scormGetValue(name: string): string | number | Date {
		if (scormAPI) {
			var value = scormAPI.GetValue(name),
				errCode = parseFloat(scormAPI.GetLastError());

			if (!errCode) {
				return /\.timestamp$/.test(name) ? new Date(value || 0) :
					/\.(latency|max_time_allowed|total_time)$/.test(name) ? scormTimeInterval(value) :
						value;
			}
			DEBUG && console.warn("GetValue(" + name + ") failed with code " + errCode + ". \n" + scormAPI.GetErrorString(String(errCode)));
		}
	}

	/**
	 * Wraps the call to the SetValue function
	 * @param {string} name -string representing the data model defined category or element
	 * @param {(number|string)} value -the value that the named element or category will be assigned
	 */
	function scormSetValue(name: string, value: number | string): void;
	function scormSetValue(name: string[]): void;
	function scormSetValue(name: {}): void;
	function scormSetValue(name: any, value?: number | string): void {
		if (scormAPI) {
			try {
				if (Array.isArray(name)) {
					for (var i = 0; i < name.length; i += 2) {
						scormSetValue(name[i], name[i + 1]);
					}
				} else if (typeof name === "object") {
					for (var j in name) {
						if (name.hasOwnProperty(j)) {
							scormSetValue(j, name[j]);
						}
					}
				} else if (name && value === value) { // Specific test for NaN
					var result: string,
						val = /\.timestamp$/.test(name) ? new Date(value).format("c") :
							/\.(session_time|latency)$/.test(name) ? scormTimeInterval(value) :
								value;

					result = scormAPI.SetValue(name, val as string);
					if (result === "true") {
						DEBUG && console.log("SetValue(\"" + name + "\", \"" + val + "\")");
						data[name] = val;
						if (!scormCommitTimer) {
							scormCommitTimer = setTimeout(scormCommit, (commit || 30) * 1000);
						}
					} else {
						var errCode = parseFloat(scormAPI.GetLastError());

						if (errCode) {
							DEBUG && console.warn("SetValue(\"" + name + "\", \"" + val + "\") failed with code " + errCode + ". \n" + scormAPI.GetErrorString(String(errCode)));
						}
					}
				}
			} catch (e) {
				DEBUG && console.error("Exception in scormSetValue(" + name + ", " + value + ")", e.stack);
			}
		}
	}

	/*
	 * Determines if an error was encountered by the previous API call
	 * and if so, displays a message to the user.  If the error code
	 * has associated text it is also displayed.
	 */
	function scormErrorHandler() {
		if (scormAPI) {
			// check for errors caused by or from the LMS
			var errCode = scormAPI.GetLastError().toString();
			if (errCode) {
				// an error was encountered so display the error description
				var errDescription = scormAPI.GetErrorString(errCode);
				if ((window as any).DEBUG) {
					errDescription += "\n" + scormAPI.GetDiagnostic(null); // by passing null to GetDiagnostic, we get any available diagnostics on the previous error.
				}
				DEBUG && console.warn(errDescription);
			}
		}
	}

	function updateSCO(persistData: APIScore[], dontSet?: boolean) {
		var IC = window.IC,
			newData: {[key: string]: string | number} = {};

		persistData.forEach(function(score) {
			var id = score.id,
				interactionIndex = _interactionIndex[id],
				objectiveIndex = _objectiveIndex[id];
			//			var keys = {
			//				// Interactions
			//				"result": "cmi.interactions." + interactionIndex + ".result",
			//				"learner_response": "cmi.interactions." + interactionIndex + ".learner_response",
			//				"question": "cmi.interactions." + interactionIndex + ".description",
			//				"weighting": "cmi.interactions." + interactionIndex + ".weighting",
			//				"timestamp": "cmi.interactions." + interactionIndex + ".timestamp",
			//				"correct_responses": "cmi.interactions." + interactionIndex + ".correct_responses.0.pattern",
			//				"type": "cmi.interactions." + interactionIndex + ".type",
			//				"latency": "cmi.interactions." + interactionIndex + ".latency",
			//				// Objectives
			//				"success_status": "cmi.objectives." + objectiveIndex + ".success_status",
			//				"completion_status": "cmi.objectives." + objectiveIndex + ".completion_status",
			//				"scaled": "cmi.objectives." + objectiveIndex + ".score.scaled",
			//				"raw": "cmi.objectives." + objectiveIndex + ".score.raw",
			//				"min": "cmi.objectives." + objectiveIndex + ".score.min",
			//				"max": "cmi.objectives." + objectiveIndex + ".score.max",
			//				"progress_measure": "cmi.objectives." + objectiveIndex + ".progress_measure"
			//			};

			if (score["attempted"]) {
				newData["cmi.interactions." + interactionIndex + ".learner_response"] = (Array.isArray(score["answers"]) ? JSON.stringify(score["answers"] as string[]) : score["answers"]) as string;
				newData["cmi.objectives." + objectiveIndex + ".completion_status"] = score["completed"] ? "completed" : "incomplete";
				newData["cmi.interactions." + interactionIndex + ".result"] = String(score["score"]);
				if (score["min"] && score["max"]) {
					newData["cmi.objectives." + objectiveIndex + ".score.min"] = score["min"];
					newData["cmi.objectives." + objectiveIndex + ".score.max"] = score["max"];
					newData["cmi.objectives." + objectiveIndex + ".score.scaled"] = newData["cmi.objectives." + objectiveIndex + ".score.raw"] = score["scaled"];
					newData["cmi.interactions." + interactionIndex + ".weighting"] = score["points"];
				}
			} else if (score["viewed"]) {
				newData["cmi.objectives." + objectiveIndex + ".completion_status"] = "incomplete";
				newData["cmi.interactions." + interactionIndex + ".result"] = "neutral";
			} else {
				newData["cmi.objectives." + objectiveIndex + ".completion_status"] = "not attempted";
			}
		});
		//newData["_type"] = newData["marked"] && newData["_result"] === "unanticipated" ? "long-fill-in" : "other";
		if (!dontSet) {
			var values: (string | number)[] = [],
				min = 0,
				max = 0,
				raw = 0;

			newData["cmi.suspend_data"] = IC.state();
			for (var key in newData) {
				if (newData.hasOwnProperty(key) && newData[key] !== data[key] && newData[key] === newData[key]) {
					values.push(key, newData[key]);
				}
			}
			for (var key in data) {
				if (key.length > 20) {
					switch (key.substr(key.lastIndexOf(".") + 1)) {
						case "max":
							max += parseFloat(data[key] as string);
							break;
						case "min":
							min += parseFloat(data[key] as string);
							break;
						case "raw":
							raw += parseFloat(data[key] as string);
							break;
					}
				}
			}
			if (min && max) {
				if (data["cmi.score.min"] !== String(min)) {
					values.push("cmi.score.min", String(min));
				}
				if (data["cmi.score.max"] !== String(max)) {
					values.push("cmi.score.max", String(max));
				}
				if (data["cmi.score.raw"] !== String(raw)) {
					values.push("cmi.score.raw", String(raw));
				}
				var scaled = String((raw - min) / (max - min));

				if (data["cmi.score.scaled"] !== scaled) {
					values.push("cmi.score.scaled", scaled);
				}
			}
			scormSetValue(values);
		}
		//DEBUG && console.log("local", newData, values)
		//		updateGlobal(newData.setpass, dontSet);
	}

	function updateGlobal(pass, dontSet) {
		//		if (scormAPI) {
		//			var i,
		//				completed = true,
		//				progress = 0,
		//				count = 0,
		//				values = [],
		//				newGlobal = {
		//					"completed": "true",
		//					"pass": "unknown",
		//					"progress": 0,
		//					"min": 0,
		//					"max": 0,
		//					"raw": 0,
		//					"score": 0,
		//					"start": _global["start"] || Date.now(),
		//					"state": _global["state"] || {}
		//				},
		//				global_keys = {
		//					"completed": "cmi.completion_status",
		//					"_pass": "cmi.success_status",
		//					"progress": "cmi.progress_measure",
		//					"min": "cmi.score.min",
		//					"max": "cmi.score.max",
		//					"raw": "cmi.score.raw",
		//					"score": "cmi.score.scaled",
		//					"session": "cmi.session_time",
		//					"_state": "cmi.suspend_data"
		//				};
		//
		//			newGlobal["session"] = Math.floor((Date.now() - newGlobal["start"]) / 1000);
		//			for (i in _data) {
		//				if (_data.hasOwnProperty(i)) {
		//					if (completed && _data[i]["_complete"] !== "completed") {
		//						completed = false;
		//					}
		//					progress += parseFloat(_data[i]["progress"] || 0);
		//					newGlobal["raw"] += isNaN(_data[i]["_result"]) ? 0 : _data[i]["_result"];
		//					newGlobal["max"] += isNaN(_data[i]["_points"]) ? 0 : _data[i]["_points"];
		//					newGlobal["state"][_data[i]["id"]] = {
		//						"attempts": _data[i]["attempts"],
		//						"state": _data[i]["state"]
		//					};
		//					count++;
		//				}
		//			}
		//			newGlobal["completed"] = completed ? "completed" : "incomplete";
		//			newGlobal["progress"] = progress / count;
		//			newGlobal["score"] = newGlobal["max"] ? (newGlobal["raw"] / newGlobal["max"]) : 0;
		//			if (pass >= 0) {
		//				newGlobal["_pass"] = completed ? (newGlobal["score"] * 100 >= pass ? "passed" : "failed") : "unknown";
		//			}
		//			newGlobal["_state"] = JSON.stringify(newGlobal["state"]);
		//			if (!dontSet) {
		//				for (i in newGlobal) {
		//					if (newGlobal.hasOwnProperty(i) && global_keys[i] && newGlobal[i] !== _global[i] && newGlobal[i] === newGlobal[i]) {
		//						values.push(global_keys[i], newGlobal[i]);
		//					}
		//				}
		//				scormSetValue(values);
		//			}
		//			//			DEBUG && console.log("global", newGlobal, values)
		//			_global = newGlobal; // Update cache
		//		}
	}

	function startup() {
		document.body.removeEventListener("ic-startup", startup);
		if (!window.IC) {
			console.error("Error: No Infuze Creator API available!");
			return;
		}
		if (scormInitialize()) {
			var IC = window.IC,
				suspend_data = scormGetValue("cmi.suspend_data") as string;

			if (suspend_data) {
				IC.state(suspend_data);
				DEBUG && console.info("State loaded:", suspend_data);
			}
			if (!location.hash && !journal) {
				loadActivity = scormGetValue("cmi.location");
			}
			switch (scormGetValue("cmi.mode")) {
				case "browse":
					// Don't read any data but allow people to play
					scormTerminate();
					return;
				case "normal":
					//$.stylesheet("#abc .abc-footer>span a.abc-score{display:none;}");
					break;
				case "review":
					// Disable all input and read data - closes the connection when all activities are loaded
					isReadOnly = true;
					break;
			}
			var uid = document.documentElement.getAttribute("data-uid"),
				list = IC.score();

			list.forEach(function(score) {
				var id = score.id,
					scormID = _scormID[id] = "urn:X-IC:" + uid + ":" + id;

				// *** Interaction ***
				for (var interactionIndex = 0, length = parseInt(scormGetValue("cmi.interactions._count") as string || "0", 10); interactionIndex < length; interactionIndex++) {
					if (scormGetValue("cmi.interactions." + interactionIndex + ".id") === scormID) {
						break;
					}
				}
				_interactionIndex[id] = interactionIndex;
				if (interactionIndex === length && !isReadOnly) {
					scormSetValue("cmi.interactions." + interactionIndex + ".id", scormID);
				}
				// *** Objective ***
				var journalIndex = _journalIndex[id] = journal ? parseInt(scormGetValue("cmi.interactions." + interactionIndex + ".objectives._count") as string, 10) : 0,
					objectiveID = scormID + (journal ? ":" + journalIndex : "");

				for (var objectiveIndex = 0, length = parseInt(scormGetValue("cmi.objectives._count") as string || "0", 10); objectiveIndex < length; objectiveIndex++) {
					if (scormGetValue("cmi.objectives." + objectiveIndex + ".id") === objectiveID) {
						break;
					}
				}
				_objectiveIndex[id] = objectiveIndex;
				if (objectiveIndex === length && !isReadOnly) {
					scormSetValue([
						"cmi.objectives." + objectiveIndex + ".id", objectiveID,
						"cmi.objectives." + objectiveIndex + ".completion_status", "not attempted",
						"cmi.interactions." + interactionIndex + ".objectives." + journalIndex + ".id", objectiveID
					]);
				}
			});
			updateSCO(list);
			if (isReadOnly) {
				scormTerminate();
				//if (location.hash) { // && this.data.single
				//	$(".abc:abc(id=" + location.hash.replace("#", "") + ")").siblings(".abc").remove();
				//	$("#abc").children(".abc-footer").find(".abc-left,.abc-right,.abc-nav,.abc-first").addClass("disabled");
				//}
				//$("#abc").children(".abc-footer").children(".left").append("<img src=\"" + this.asset("nav_disabled") + "\" title=\"READ ONLY MODE, answers will not be saved to platform!\">");
			} else {
				//this.goto(loadActivity);
				document.body.addEventListener("ic-score", function(e) {
					var data: {[screen: number]: {[screen: number]: true}} = (e as any).icData,
						list = IC.score();

					// TODO: Use data and limit which screens / activities are updating
					updateSCO(list);
				}, false);
			}
		}
	}

	if (document.body.classList.contains("ict-started")) {
		startup();
	} else {
		document.body.addEventListener("ic-startup", startup);
	}
})();
