///<reference path="../editor/activity/src/api.d.ts" />
/** @license
 * Copyright (C) 2013-2017 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */
(function () {
    var colours = [
        "#dddddd",
        "#cccccc"
    ], chartQuestions, dataQuestions = {
        canvas: undefined,
        colours: colours,
        hole: 0.6,
        data: {
            complete: 0,
            incomplete: 0
        }
    }, chartScore, dataScore = {
        canvas: undefined,
        colours: colours,
        hole: 0.6,
        data: {
            complete: 0,
            incomplete: 0
        }
    }, chartPercent, dataPercent = {
        canvas: undefined,
        colours: colours,
        hole: 0.6,
        data: {
            complete: 0,
            incomplete: 0
        }
    };
    var fixAngle = Math.PI / 2;
    var Piechart = /** @class */ (function () {
        function Piechart(options) {
            this.options = options;
            this.canvas = options.canvas;
            this.ctx = this.canvas.getContext("2d");
            this.draw();
        }
        Piechart.prototype.draw = function () {
            var totalValue = 0, colourIndex = 0, startAngle = 0, ctx = this.ctx, canvas = this.canvas, options = this.options, data = options.data, colours = options.colours, colourCount = colours.length, centerX = canvas.width / 2, centerY = canvas.height / 2, radius = Math.min(centerX, centerY);
            for (var category in data) {
                totalValue += data[category];
            }
            for (category in data) {
                var val = data[category], sliceAngle = 2 * Math.PI * val / totalValue, inner = (this.options.hole || 0) * radius, angle1 = startAngle - fixAngle, angle2 = startAngle + sliceAngle - fixAngle, startOfOuterArcX = radius * Math.cos(angle2) + centerX, startOfOuterArcY = radius * Math.sin(angle2) + centerY;
                ctx.fillStyle = colours[colourIndex++ % colourCount];
                ctx.beginPath();
                ctx.arc(centerX, centerY, inner, angle1, angle2);
                ctx.lineTo(startOfOuterArcX, startOfOuterArcY);
                ctx.arc(centerX, centerY, radius, angle2, angle1, true);
                ctx.closePath();
                ctx.fill();
                startAngle += sliceAngle;
            }
        };
        return Piechart;
    }());
    function update(list) {
        var length = list.length, attempted = 0, points = 0, total = 0, i = 0;
        for (; i < length; i++) {
            var activity = list[i];
            if (activity.attempted) {
                attempted++;
            }
            points += activity.score;
            total += activity.points;
        }
        dataQuestions.data["complete"] = attempted;
        dataQuestions.data["incomplete"] = length - attempted;
        dataQuestions.canvas.nextElementSibling.textContent = Math.floor(attempted) + "/" + length;
        dataScore.data["complete"] = points;
        dataScore.data["incomplete"] = total - points;
        dataScore.canvas.nextElementSibling.textContent = Math.floor(points) + "/" + Math.floor(total);
        dataPercent.data["complete"] = 100 / total * points;
        dataPercent.data["incomplete"] = 100 - dataPercent.data["complete"];
        dataPercent.canvas.nextElementSibling.textContent = Math.floor(dataPercent.data["complete"]) + "%";
        chartQuestions.draw();
        chartScore.draw();
        chartPercent.draw();
    }
    function startup() {
        var currentScreen = 0;
        document.body.removeEventListener("ic-startup", startup);
        if (!window.IC) {
            console.error("Error: No Infuze Creator API available!");
            return;
        }
        var canvas = document.querySelectorAll(".results canvas");
        dataQuestions.canvas = canvas[0];
        chartQuestions = new Piechart(dataQuestions);
        dataScore.canvas = canvas[1];
        chartScore = new Piechart(dataScore);
        dataPercent.canvas = canvas[2];
        chartPercent = new Piechart(dataPercent);
        Array.from(document.body.querySelectorAll("ic-button[ic-button='submit']")).forEach(function (el) {
            el.addEventListener("mouseup", function (e) {
                if (!this.classList.contains("ict-state-disabled")) {
                    window.IC.goto(++currentScreen);
                    if (currentScreen > 4) {
                        console.log("end");
                        Array.from(document.querySelectorAll("footer ic-button")).forEach(function (el) {
                            el.style.display = el.style.display ? "" : "none";
                        });
                        document.querySelector("ic-screens").icWidget.addState("marked");
                        update(window.IC.score());
                    }
                    e.preventDefault();
                    e.stopPropagation();
                    e.stopImmediatePropagation();
                    return false;
                }
            }, true);
        });
        document.body.addEventListener("ic-score", function (e) {
            // TODO: Use data and limit which screens / activities are updating
            //console.log("score", e, window.IC.score())
            update(window.IC.score());
        }, false);
    }
    if (document.body.classList.contains("ict-started")) {
        startup();
    }
    else {
        document.body.addEventListener("ic-startup", startup);
    }
})();
//# sourceMappingURL=results.js.map