///<reference path="../editor/activity/src/api.d.ts" />
///<reference path="../editor/activity/src/prototypes/number/toRoman.d.ts" />
///<reference path="../editor/activity/src/prototypes/string/compress.d.ts" />
///<reference path="../editor/activity/src/prototypes/string/decompress.d.ts" />
///<reference path="../editor/activity/src/prototypes/string/regex.d.ts" />
/*
 * Copyright &copy; 2016-2017 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Rising Stars Assessment (Hodder) wrapper for Infuze Creator
 * Accesses IC API to get score and state data
 * Sends and gets data from AP web service
 */

(function(document: Document, window: Window) {
	/**
	 * Disabled on these domains.
	 * To add a domain simply base64 encode it - <code>btoa("ic.infuze.uk")</code>
	 */
	const DISABLED: string[] = [
		"aWMuaW5mdXplLnVr" // ic.infuze.uk
	];
	/**
	 * Save all answers to current question, otherwise only save the ones that
	 * have changed.
	 */
	const SAVE_ALL_ANSWERS = false;
	/**
	 * DEV: http://development.assessmentplus-risingstars.mmtdigital.co.uk/api
	 * LIVE: http://my.risingstarsassessment.co.uk/api
	 */
	const SERVER = "/api";
	/**
	 * Prefix selector for popup boxes
	 */
	const BOXPREFIX = ".ict-box-";
	/**
	 * Number of seconds between state saves
	 */
	const DEBOUNCE = 30;
	/**
	 * Number of seconds to timeout Ajax
	 */
	const TIMEOUT = 30;
	/**
	 *  How often to save instead of PING
	 */
	const SAVECOUNT = 1;

	const enum Mode {
		NORMAL = 0, // Normal pupil access, normal timer etc
		REVIEW = 1, // No timer, teacher review answers
		VIEW = 2 // Normal timer, no saving
	};

	const enum Response {
		OK = 0, // Continue
		ERROR = 1, // Something went wrong - TODO: retry as normal internet-down state
		EXPIRED = 2 // Token expired - TODO: pupil needs to re-launch test?
	};

	const enum ServerAction {
		PING = 0,
		STATE = 1,
		SCORE = 2,
		END = 3
	}

	const enum ErrorState {
		NONE = 0,
		LOADING = 1,
		CONNECTING = 2,
		DEAD = 3
	}

	interface ICScreenEvent extends Event {
		icData: {
			index: number;
			element: HTMLElement;
			oldIndex: number;
			oldElement: HTMLElement;
		}
	}

	interface APIData {
		token: string;
		responseCode: Response;
		mode: Mode;
		paper: string;
		name: string;
		studentId: number;
		markSession: number;
		pupilPremium: boolean;
		time: number;
		extendedTime: number;
		blobData: string;
		completeUrl: string;
	}

	interface Ping {}

	interface SaveData extends Ping {
		markSessionId: number;
		blobData: string;
	}

	interface SaveAnswers extends SaveData {
		timeTaken: number;
		answerSequence: number;
		answers: {
			question: string;
			mark: number | "N";
		}[];
	}

	interface SaveComplete extends SaveData {
		timeTaken: number;
		answers: {
			question: string;
			mark: number | "N";
		}[];
	}

	function rsa() {
		let countIC = 0;

		if (!window.IC) {
			if (countIC++ < 10) {
				setTimeout(rsa, 500);
			} else {
				console.error("RSA Error: No Infuze Creator API available!");
			}
			return;
		}

		let IC = window.IC,
			connected = false,
			count = 0,
			body = document.body,
			// Data for the current session
			location = window.location,
			token = ((location && location.search || "").match(/t=([a-z0-9\-]+)/) || [])[1],
			mode = Mode.VIEW, // Mode of the activities
			sessionId = 0, // Used to save any data for this test including state and answers
			paper = "", // ?
			name = "", // Pupil name
			premium = false, // Do they have premium?
			time = 0, // Time for the test
			extended = 0, // Added time as a premium pupil
			totalTime = 0, // Total time including premium if allowed
			completeUrl: string,
			pendingReply: boolean,
			currentState: string,
			lastState: string,
			currentScore: string,
			lastScore: string,
			serverScore: {[question: string]: number | "N"} = {},
			sendingScore: {[question: string]: number | "N"} = {},
			pendingScore: {[question: string]: number | "N"} = {},
			attempted: {[screen: string]: boolean} = {},
			needScore: boolean,
			needEnd: boolean,
			currentXHR: XMLHttpRequest,
			timeoutXHR: any,
			action: ServerAction = ServerAction.PING,
			error: ErrorState = ErrorState.NONE,
			answerSequence = 1,
			debounceTimer: any,
			currentIndex: number,
			nextIndex: number;

		function addEventListeners(selector: string, listenerType: string, listener: EventListenerOrEventListenerObject) {
			Array.from(document.querySelectorAll(selector)).forEach(function(el: HTMLElement) {
				el.addEventListener(listenerType, listener)
			});
		}

		function removeEventListeners(selector: string, listenerType: string, listener: EventListenerOrEventListenerObject) {
			Array.from(document.querySelectorAll(selector)).forEach(function(el: HTMLElement) {
				el.removeEventListener(listenerType, listener)
			});
		}

		function displayNoTokenWarning() {
			console.warn("RSA: Not loading via website, aborting.");
		}

		//		function ajax(query: "/infuze/ping", data: Ping): void;
		//		function ajax(query: "/infuze/playerdata", data: SaveData): void;
		function ajax(query: "/infuze/questiondata", data: SaveAnswers): void;
		function ajax(query: "/infuze/testcomplete", data: SaveComplete): void;
		function ajax(query: string): void;
		function ajax(query: string, data?: any) {
			if (token) {
				console.info("RSA: AJAX request", SERVER + (query || ""), data);
				try {
					let xhr = currentXHR = new XMLHttpRequest();

					xhr.open(data ? "POST" : "GET", SERVER + (query || ""), true);
					xhr.setRequestHeader("Content-type", "application/json; charset=utf-8");
					xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
					xhr.onreadystatechange = function() {
						if (this.readyState !== XMLHttpRequest.DONE) {
							// We don't care until it's finished
							return;
						}
						pendingReply = false;
						clearTimeout(timeoutXHR);
						timeoutXHR = undefined;
						if (this.status && this.status !== 200) {
							setState(ErrorState.CONNECTING);
						} else if (currentXHR !== xhr) {
							console.error("RSA: Received out-of-order response!");
							return;
						} else if (this.response) {
							let response: APIData = typeof this.response === "object" && (this.response as APIData).responseCode ? this.response : JSON.parse(this.responseText);

							console.info("RSA: AJAX response", response);
							setState(ErrorState.NONE);
							switch (response.responseCode) {
								case Response.OK:
									if (response.markSession) {
										connected = true;
										sessionId = response.markSession;
										paper = response.paper || "";
										name = response.name || "---";
										premium = response.pupilPremium;
										time = response.time || 0;
										extended = response.extendedTime || 0;
										totalTime = time + (premium ? extended : 0);
										completeUrl = response.completeUrl;
										mode = response.mode;
										lastState = response.blobData;
										body.normalize();
										for (let walker = document.createNodeIterator(body, NodeFilter.SHOW_TEXT), node = walker.nextNode(); node; node = walker.nextNode()) {
											let text = node.textContent,
												newText = text.replace(/%%(name|time)%%/g, function($0, $1) {
													switch ($1) {
														case "name":
															return name;
														case "time":
															return String(Math.floor(totalTime));
													}
													return ""; // Cannot get here, but things might change
												});

											if (text !== newText) {
												node.textContent = newText;
											}
										}
										if (lastState && mode !== Mode.VIEW) {
											IC.state(lastState);
											setTimeout(() => {
												// Scoring is asynchronous, so need to wait for it to finish
												serverScore = getAnswers();
											}, 500);
											if (mode === Mode.NORMAL) {
												showBox("continue");
											}
										}
										if (totalTime) {
											IC.timer(totalTime);
										}
										switch (mode) {
											case Mode.NORMAL:
												console.info("RSA: Starting in NORMAL mode.");
												body.addEventListener("ic-timeout", endTest);
												body.addEventListener("ic-screen", saveScore);
												body.addEventListener("click", clickEndTest);
												addEventListeners(".ict-screen-once .ict-button-next", "click", startTest);
												window.addEventListener("beforeunload", forceSaveState);
												window.addEventListener("unload", forceSaveState);
												setState(ErrorState.NONE);
												break;
											case Mode.VIEW:
												connected = false;
												console.info("RSA: Starting in VIEW mode.");
												addEventListeners(".ict-screen-once .ict-button-next", "click", startTest);
												setState(ErrorState.NONE);
												break;
											case Mode.REVIEW:
												let screens = body.querySelectorAll("ic-screens>ic-screen"),
													lastScreen = screens[screens.length - 1];

												connected = false;
												console.info("RSA: Starting in REVIEW mode.");
												if ((lastScreen as any).icWidget) {
													(lastScreen as any).icWidget.addState("disabled");
												}
												IC.pause();
												IC.resume(true);
												IC.resume = function() {return 0;}; // Force this to be un-resumable
												setState(ErrorState.NONE);
												IC.goto(1); // Go to the first screen, screen 0 is the Start screen
												break;
										}
									} else {
										switch (action) {
											case ServerAction.END:
												needEnd = connected = false;
												if (completeUrl) {
													location.replace(completeUrl);
												}
											case ServerAction.SCORE:
												needScore = false;
												currentIndex = nextIndex;
												lastScore = currentScore;
												if (sendingScore) {
													Object.keys(sendingScore).forEach((question) => {
														serverScore[question] = sendingScore[question];
													});
												}
												sendingScore = undefined;
											case ServerAction.STATE:
												count = 0;
												lastState = currentState;
											case ServerAction.PING:
												if (pendingScore || needScore || needEnd) {
													keepAlive();
												}
												break;
										}
									}
									return;

								case Response.ERROR:
									setState(ErrorState.DEAD);
									console.error("RSA Error: Cannot connect!", response);
									return;

								case Response.EXPIRED:
									setState(ErrorState.DEAD);
									console.error("RSA Token Expired: Cannot connect!", response);
									return;
							}
						}
					};
					pendingReply = true;
					xhr.send(data ? JSON.stringify(data) : undefined);
					timeoutXHR = setTimeout(function() {
						setState(ErrorState.CONNECTING);
						console.error("RSA: AJAX request timeout!");
						pendingReply = false;
						keepAlive();
					}, TIMEOUT * 1000);
					clearTimeout(debounceTimer);
					debounceTimer = setTimeout(debounce, DEBOUNCE * 1000);
					return xhr;
				} catch (e) {
					console.error("RSA Error: Can't create XMLHttpRequest", e);
				}
			}
		}

		function showBox(selector: string) {
			let el = (document.querySelector(BOXPREFIX + selector) as HTMLElement);

			if (el) {
				el.style.display = "block";
			}
		}

		function hideBox(selector: string) {
			let el = (document.querySelector(BOXPREFIX + selector) as HTMLElement);

			if (el) {
				el.style.display = "";
			}
		}

		function setState(state: ErrorState) {
			if (error === state || error === ErrorState.DEAD) {
				return;
			}
			// Open new dialogs
			switch (state) {
				case ErrorState.NONE:
					if (error === ErrorState.CONNECTING) {
						showBox("ok");
					}
					IC.resume(true);
					break;

				case ErrorState.LOADING:
					showBox("loading");
					IC.pause();
					break;

				case ErrorState.CONNECTING:
					if (connected) {
						showBox("warning");
						IC.pause();
						break;
					}
				case ErrorState.DEAD:
					showBox("error");
					IC.pause();
					break;
			}
			// Close old dialogs
			switch (error) {
				case ErrorState.NONE:
					hideBox("ok");
					break;

				case ErrorState.LOADING:
					hideBox("loading");
					break;

				case ErrorState.CONNECTING:
					hideBox("warning");
					break;
			}
			error = state;
		}

		/**
		 * Get the current list of answers as a {question:value} object. Also
		 * sets the global "attempted" key for that answer.
		 */
		function getAnswers() {
			let tempScore: {[question: string]: number | "N"} = {};

			IC.score().forEach(function(mark) {
				for (let i = 0, points = mark.points; i < points; i++) {
					let question = mark["id"].replace(/^(\d+)(.*)$/, function($0, $1, $2) {
						return (parseInt($1, 10) - 1) + ($2 || "");
					}) + (points > 1 ? (i + 1).toRoman().toLowerCase() : ""),
						value = mark.score > i ? 1 : 0,
						realValue: number | "N" = mark.attempted ? value : "N";

					if ((!sendingScore || sendingScore[question] !== realValue) && (SAVE_ALL_ANSWERS || serverScore[question] !== realValue)) {
						let screen = question.replace(/[^\d]/g, "");

						if (tempScore[question]) {
							console.warn("RSA Error: Duplicate question ID!", question);
						}
						attempted[screen] = attempted[screen] || mark.attempted;
						tempScore[question] = serverScore[question] = realValue;
					}
				}
			});
			return tempScore;
		}

		/**
		 * Prevent the API from spamming the server with data, make sure it can
		 * only send at most every 30 seconds.
		 * If we're still waiting to hear back from the API then we skip this as
		 * the timeout and/or response will trigger it again anyway.
		 */
		function debounce() {
			if (!timeoutXHR) {
				if (sendingScore) {
					Object.keys(sendingScore).forEach((question) => {
						serverScore[question] = sendingScore[question];
					});
				}
				debounceTimer = sendingScore = undefined;
				if (pendingScore || needScore || needEnd) {
					keepAlive();
				}
			}
		}

		function keepAlive(forceState?: ServerAction) {
			if (connected && !pendingReply) {
				let scoreData: {question: string, mark: number | "N"}[] = [],
					time = IC.timer() / 60;

				if (typeof forceState === "number") {
					action = forceState;
				} else if (needEnd) {
					action = ServerAction.END
				} else if (needScore || sendingScore || pendingScore) {
					action = ServerAction.SCORE
				} else if (count++ < SAVECOUNT) {
					action = ServerAction.PING;
				} else {
					action = ServerAction.STATE;
				}
				if (action === ServerAction.PING) {
					currentState = undefined;
				} else {
					currentState = IC.state();
				}
				if (action === ServerAction.PING || action === ServerAction.STATE) {
					currentScore = undefined;
				} else {
					/*
					 * Multiple states for the score, all are {question:value}
					 * 
					 * Locally here we work out "tempScore".
					 * We also store "serverScore" for what we believe the server knows.
					 * When sending to the server "tempScore" gets copied to "sendingScore".
					 * If currently sending (within the debounce timer) "tempScore" gets copied to "pendingScore".
					 * When the debounceTimer finishes,
					 *  1. we check if "sendingScore" still exists, and use that instead for "tempScore" if so - we're retrying the last one.
					 *  2. or we check if "pendingScore" exists, and use that instead for "tempScore" if so.
					 */
					let tempScore: {[question: string]: number | "N"};

					if (action === ServerAction.END) {
						sendingScore = pendingScore = undefined;
					}
					if (sendingScore && timeoutXHR) {
						// Can only hit this if we had a timeout
						tempScore = sendingScore;
					} else if (pendingScore && !debounceTimer) {
						tempScore = pendingScore;
						pendingScore = undefined;
					} else {
						tempScore = getAnswers();
					}
					Object.keys(tempScore).forEach((question) => {
						if (attempted[question.replace(/[^\d]/g, "")]) {
							scoreData.push({
								question: question,
								mark: tempScore[question]
							});
						}
					});
					if (action !== ServerAction.END && !scoreData.length && debounceTimer) {
						pendingScore = tempScore;
						return;
					}
					sendingScore = tempScore;
					currentScore = JSON.stringify(scoreData);
					scoreData.length && console.info("RSA: Sending score", scoreData.map(data => data.question + ":" + data.mark).join(", "));
				}
				switch (action) {
					case ServerAction.END:
						ajax("/infuze/testcomplete", {
							markSessionId: sessionId,
							blobData: currentState,
							timeTaken: time >= totalTime ? -1 : time,
							answers: scoreData
						});
						break;

					case ServerAction.SCORE:
					case ServerAction.STATE:
						if (lastState !== currentState) {
							ajax("/infuze/questiondata", {
								markSessionId: sessionId,
								blobData: currentState,
								timeTaken: time,
								answerSequence: answerSequence++,
								answers: scoreData
							});
							break;
						}

					case ServerAction.PING:
						break;
				}
			}
		}

		function saveScore(event?: ICScreenEvent) {
			if (event && event.icData) {
				currentIndex = event.icData.oldIndex;
				nextIndex = event.icData.index;
			}
			needScore = true;
			keepAlive(ServerAction.SCORE);
		}

		function forceSaveState() {
			clearTimeout(debounceTimer);
			sendingScore = pendingScore = debounceTimer = undefined;
			pendingReply = false;
			keepAlive(ServerAction.SCORE);
			window.removeEventListener("beforeunload", forceSaveState);
			window.removeEventListener("unload", forceSaveState);
		}

		function startTest(event: MouseEvent) {
			IC.resume();
		}

		function clickEndTest(event: MouseEvent) {
			let target = event.target as HTMLElement;

			if (target && target.closest(".ict-button-end")) {
				endTest();
			}
		}

		function endTest() {
			body.removeEventListener("ic-timeout", endTest);
			body.removeEventListener("ic-screen", saveScore);
			body.removeEventListener("click", clickEndTest);
			removeEventListeners(".ict-screen-once .ict-button-next", "click", startTest);
			window.removeEventListener("beforeunload", forceSaveState);
			window.removeEventListener("unload", forceSaveState);
			IC.pause();
			needEnd = true;
			keepAlive(ServerAction.END);
			// Navigation occurs after the ajax call has completed
		}

		if (token) {
			setState(ErrorState.LOADING);
			ajax("/infuze/marksessionparams?token=" + token);
		} else if (location.pathname !== "srcdoc" && DISABLED.indexOf(btoa(location.host)) < 0) {
			IC.pause(true);
			showBox("error");
			setTimeout(displayNoTokenWarning, 1);
		} else if (!(window as any).ic_edit) {
			let lastScore = "";

			console.info("RSA: No server token supplied, starting in VIEW mode.");
			setInterval(function() {
				let questions: string[] = [];

				IC && IC.score().forEach(function(mark) {
					let i = 0,
						points = mark.points,
						question = mark["id"].replace(/^(\d+)(.*)$/, function($0, $1, $2) {
							return (parseInt($1, 10) - 1) + ($2 || "");
						});

					for (; i < points; i++) {
						questions.push(question + (points > 1 ? (i + 1).toRoman().toLowerCase() : "") + ":" + (mark.score > i ? 1 : 0));
					}
				});
				let newScore = questions.join(", ");

				if (lastScore !== newScore) {
					lastScore = newScore;
					console.info("RSA:", lastScore);
				}
			}, 1000);
		}
		(IC as any).rsaComplete = function() {
			if (connected) {
				needEnd = true;
			} else {
				console.warn("RSA: Not connected to API!");
			}
		}
	}

	if (/complete|loaded|interactive/.test(document.readyState) && document.body) {
		rsa();
	} else {
		document.addEventListener("DOMContentLoaded", rsa, false);
	}
})(document, window);
