///<reference path="../editor/activity/src/api.d.ts" />
///<reference path="../editor/activity/src/prototypes/number/toRoman.d.ts" />
///<reference path="../editor/activity/src/prototypes/string/compress.d.ts" />
///<reference path="../editor/activity/src/prototypes/string/decompress.d.ts" />
///<reference path="../editor/activity/src/prototypes/string/regex.d.ts" />
/*
 * Copyright &copy; 2016-2017 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Rising Stars Assessment (Hodder) wrapper for Infuze Creator
 * Accesses IC API to get score and state data
 * Sends and gets data from AP web service
 */

(function(document: Document, window: Window) {
	/**
	 * DEV: http://development.assessmentplus-risingstars.mmtdigital.co.uk/api
	 * LIVE: http://my.risingstarsassessment.co.uk/api
	 */
	const SERVER = "https://demo.risingstarsassessment.co.uk/api";
	/**
	 * Number of seconds to timeout Ajax
	 */
	const TIMEOUT = 31;

	const enum Mode {
		NORMAL = 0, // Normal pupil access, normal timer etc
		REVIEW = 1, // No timer, teacher review answers
		VIEW = 2 // Normal timer, no saving
	};

	const enum Response {
		OK = 0, // Continue
		ERROR = 1, // Something went wrong - TODO: retry as normal internet-down state
		EXPIRED = 2 // Token expired - TODO: pupil needs to re-launch test?
	};

	const enum ErrorState {
		NONE = 0,
		LOADING = 1,
		CONNECTING = 2,
		DEAD = 3
	}

	interface APIData {
		token: string;
		responseCode: Response;
		mode: Mode;
		paper: string;
		name: string;
		studentId: number;
		markSession: number;
		pupilPremium: boolean;
		time: number;
		extendedTime: number;
		blobData: string;
	}

	interface Ping {}

	interface SaveData extends Ping {
		markSessionId: number;
		blobData: string;
	}

	interface SaveAnswers extends SaveData {
		timeTaken: number;
		answerSequence: number;
		answers: {
			question: string;
			mark: number | "N";
		}[];
	}

	interface SaveComplete extends SaveData {
		timeTaken: number;
	}

	function rsa() {
		var countIC = 0;

		if (!window.IC) {
			if (countIC++ < 10) {
				setTimeout(rsa, 500);
			} else {
				console.error("Error: No Infuze Creator API available!");
			}
			return;
		}

		var IC = window.IC,
			connected = false,
			// Data for the current session
			token = ((location && location.search || "").match(/t=([a-z0-9\-]+)/) || [])[1],
			mode = Mode.VIEW, // Mode of the activities
			sessionId = 0, // Used to save any data for this test including state and answers
			paper = "", // ?
			name = "", // Pupil name
			premium = false, // Do they have premium?
			time = 0, // Time for the test
			extended = 0, // Added time as a premium pupil
			totalTime = 0, // Total time including premium if allowed
			pendingReply: boolean,
			lastState: string,
			currentXHR: XMLHttpRequest,
			timeoutXHR: number,
			error: ErrorState = ErrorState.NONE,
			answerSequence = 1;


		function addEventListeners(selector: string, listenerType: string, listener: EventListenerOrEventListenerObject) {
			Array.from(document.querySelectorAll(selector)).forEach(function(el: HTMLElement) {
				el.addEventListener(listenerType, listener)
			});
		}

		function removeEventListeners(selector: string, listenerType: string, listener: EventListenerOrEventListenerObject) {
			Array.from(document.querySelectorAll(selector)).forEach(function(el: HTMLElement) {
				el.removeEventListener(listenerType, listener)
			});
		}

		function ajax(query: "/infuze/questiondata", data: SaveAnswers): void;
		function ajax(query: "/infuze/testcomplete", data: SaveComplete): void;
		function ajax(query: string): void;
		function ajax(query: string, data?: any) {
			if (token) {
				try {
					var xhr = currentXHR = new XMLHttpRequest();

					xhr.open(data ? "POST" : "GET", SERVER + (query || ""), true);
					xhr.setRequestHeader("Content-type", "application/json; charset=utf-8");
					xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
					xhr.onreadystatechange = function() {
						if (this.readyState !== XMLHttpRequest.DONE) {
							// We don't care until it's finished
							return;
						}
						pendingReply = false;
						clearTimeout(timeoutXHR);
						if (this.status && this.status !== 200) {
							setState(ErrorState.DEAD);
						} else if (currentXHR !== xhr) {
							console.error("Received out-of-order response!");
							return;
						} else {
							var response: APIData = JSON.parse(this.responseText);

							setState(ErrorState.NONE);
							switch (response.responseCode) {
								case Response.OK:
									if (response.markSession) {
										connected = true;
										sessionId = response.markSession;
										paper = response.paper || "";
										name = response.name || "---";
										premium = response.pupilPremium;
										time = response.time || 0;
										extended = response.extendedTime || 0;
										totalTime = time + (premium ? extended : 0);
										mode = response.mode;
										lastState = response.blobData;
										document.body.normalize();
										for (var walker = document.createNodeIterator(document.body, NodeFilter.SHOW_TEXT), node = walker.nextNode(); node; node = walker.nextNode()) {
											var text = node.textContent,
												newText = text
													.replace(/%%name%%/g, function() {
														return name;
													})
													.replace(/%%time%%/g, function() {
														return String(totalTime);
													});

											if (text !== newText) {
												node.textContent = newText;
											}
										}
										if (lastState) {
											IC.state(lastState);
										}
										if (totalTime) {
											IC.timer(totalTime);
										}
										switch (mode) {
											case Mode.NORMAL:
												document.body.addEventListener("ic-screen", saveScore);
												document.body.addEventListener("ic-timeout", endTest);
												addEventListeners(".ict-button-end", "click", endTest);
											case Mode.VIEW:
											case Mode.REVIEW:
												setState(ErrorState.NONE);
												break;
										}
									}
									return;

								case Response.ERROR:
									setState(ErrorState.DEAD);
									console.error("Error: Cannot connect!", response);
									return;

								case Response.EXPIRED:
									setState(ErrorState.DEAD);
									console.error("Token Expired: Cannot connect!", response);
									return;
							}
						}
					};
					pendingReply = true;
					xhr.send(data ? JSON.stringify(data) : undefined);
					timeoutXHR = setTimeout(function() {
						setState(ErrorState.CONNECTING);
						console.error("Request timeout!");
						ajax(query as any, data);
					}, TIMEOUT * 1000);
					return xhr;
				} catch (e) {
					console.error("Error: Can't create XMLHttpRequest", e);
				}
			}
		}

		function setState(state: ErrorState) {
			if (error === state || error === ErrorState.DEAD) {
				return;
			}
			// Open new dialogs
			switch (state) {
				case ErrorState.NONE:
					if (error === ErrorState.CONNECTING) {
						(document.querySelector(".ict-box-ok") as HTMLElement).style.display = "block";
					}
					IC.resume(true);
					break;

				case ErrorState.LOADING:
					(document.querySelector(".ict-box-loading") as HTMLElement).style.display = "block";
					IC.pause();
					break;

				case ErrorState.CONNECTING:
					if (connected) {
						(document.querySelector(".ict-box-warning") as HTMLElement).style.display = "block";
						IC.pause();
						break;
					}
				case ErrorState.DEAD:
					(document.querySelector(".ict-box-error") as HTMLElement).style.display = "block";
					IC.pause();
					break;
			}
			// Close old dialogs
			switch (error) {
				case ErrorState.NONE:
					(document.querySelector(".ict-box-ok") as HTMLElement).style.display = "";
					break;

				case ErrorState.LOADING:
					(document.querySelector(".ict-box-loading") as HTMLElement).style.display = "";
					break;

				case ErrorState.CONNECTING:
					(document.querySelector(".ict-box-warning") as HTMLElement).style.display = "";
					break;
			}
			error = state;
		}

		function saveScore(event: any) {
			var screenIndex = event.icData.oldIndex,
				scoreData: {question: string, mark: number | "N"}[] = [];

			IC.score().forEach(function(mark) {
				var printScores: string[] = [];

				for (var i = 0, points = mark.points; i < points; i++) {
					var valid = false,
						question = mark["id"].replace(/^(\d+)(.*)$/, function($0, $1, $2) {
							var index = parseInt($1, 10) - 1;

							valid = index === screenIndex;
							return index + ($2 || "");
						}) + (points > 1 ? (i + 1).toRoman().toLowerCase() : ""),
						points = mark.score > i ? 1 : 0;

					if (valid) {
						scoreData.push({
							question: question,
							mark: mark.attempted ? points : "N"
						});
					}
					printScores.push(question + ": " + points);
				}
				console.info(printScores.join(", "));
			});
			ajax("/infuze/questiondata", {
				markSessionId: sessionId,
				blobData: IC.state(),
				timeTaken: Math.round(IC.timer() / 60),
				answerSequence: answerSequence++,
				answers: scoreData
			});
		}

		function endTest() {
			document.body.removeEventListener("ic-screen", saveScore);
			document.body.removeEventListener("ic-timeout", endTest);
			removeEventListeners(".ict-button-end", "click", endTest);
			IC.pause();
			var time = IC.timer() / 60;

			ajax("/infuze/testcomplete", {
				markSessionId: sessionId,
				blobData: IC.state(),
				timeTaken: time >= totalTime ? -1 : Math.ceil(time)
			});
			// TODO: navigate away?
		}

		if (token) {
			setState(ErrorState.LOADING);
			ajax("/infuze/marksessionparams?token=" + token);
		} else if (!(window as any).ic_edit) {
			//console.warn("RSA: No server token supplied");
			var lastScore = "";

			if (IC.score) {
				setInterval(function() {
					var questions: string[] = [];

					IC.score().forEach(function(mark) {
						var i = 0,
							points = mark.points,
							question = mark["id"].replace(/^(\d+)(.*)$/, function($0, $1, $2) {
								return (parseInt($1, 10) - 1) + ($2 || "");
							});

						for (; i < points; i++) {
							questions.push(question + (points > 1 ? (i + 1).toRoman().toLowerCase() : "") + ":" + (mark.score > i ? 1 : 0));
						}
					});
					var newScore = questions.join(", ");

					if (lastScore !== newScore) {
						lastScore = newScore;
						console.info("RSA:", lastScore);
					}
				}, 1000);
			}
		}
		(IC as any).rsaComplete = function() {
			if (connected) {
				endTest();
			} else {
				console.warn("Not connected to API!");
			}
		}
	}

	if (/complete|loaded|interactive/.test(document.readyState) && document.body) {
		rsa();
	} else {
		document.addEventListener("DOMContentLoaded", rsa, false);
	}
})(document, window);
