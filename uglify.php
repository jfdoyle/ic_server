<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

$allow = array(
	"192.168.11.150"
);

if (!in_array(filter_input(INPUT_SERVER, "REMOTE_ADDR", FILTER_SANITIZE_STRING), $allow, true)) {
	die;
}

ignore_user_abort(1);

$build = filter_input(INPUT_POST, "build", FILTER_VALIDATE_BOOLEAN);
$filename = filter_input(INPUT_POST, "filename", FILTER_SANITIZE_STRING);
$version = filter_input(INPUT_POST, "version", FILTER_SANITIZE_STRING);
$sourceFiles = filter_input(INPUT_POST, "sourceFiles", FILTER_SANITIZE_STRING);

$root_path = "\\\\QNAP-NAS\\Web\\ActivityBuilder\\";
$path = pathinfo($filename, PATHINFO_DIRNAME);
$js_path = $root_path . (empty($path) || $path === "." ? "_common/js/" : "");
$map_path = $root_path . (empty($path) || $path === "." ? "_common/maps/" : "");

require_once "include/uglify.php";

//error_log("$filename ($path), " . str_replace("activity/", $root_path . "include/activity/", preg_replace("/(assets|defaults)\//i", $root_path . "\\\$1/", $sourceFiles)) . ", $js_path, $map_path, $version");
uglify(str_replace("../", "$root_path", str_replace("assets/", "{$root_path}assets/", $filename)), //
		str_replace("../", "$root_path", str_replace("assets/", "{$root_path}assets/", $sourceFiles)), //
		$version);
