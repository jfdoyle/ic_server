var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var ic;
(function (ic) {
    try {
        window.console.info("IC v" + VERSION + " (" + DATE + ")");
    }
    catch (e) { }
    ic.version = {
        "css-columns": 1
    };
})(ic || (ic = {}));
function getBoundingClientRect(el, scale) {
    var box = el.getBoundingClientRect();
    if (scale) {
        box = {
            top: box.top / ic.scaleFactor,
            right: box.right / ic.scaleFactor,
            bottom: box.bottom / ic.scaleFactor,
            left: box.left / ic.scaleFactor,
            width: box.width / ic.scaleFactor,
            height: box.height / ic.scaleFactor,
        };
    }
    return box;
}
function appendChild(parent, child) {
    parent.appendChild(child);
}
function persist(target, propertyKey) {
    if (target) {
        if (!target["_persist"]) {
            defineProperty(target, "_persist", []);
        }
        target["_persist"].push(propertyKey);
    }
}
function final(target, propertyKey) {
    var value = target[propertyKey], fn = function (target, propertyKey, value) {
        Object.defineProperty(target, propertyKey, {
            "value": value,
            "writable": false,
            "enumerable": true
        });
    };
    if (isUndefined(value)) {
        Object.defineProperty(target, propertyKey, {
            "set": function (value) {
                fn(this, propertyKey, value);
            },
            "configurable": true
        });
    }
    else {
        fn(target, propertyKey, value);
    }
}
function isArray(arr) {
    return Object.prototype.toString.call(arr) === "[object Array]";
}
function isBoolean(bool) {
    return bool === true || bool === false;
}
function isFunction(fn) {
    return Object.prototype.toString.call(fn) === "[object Function]";
}
function isNumber(num) {
    return typeof num === "number";
}
function isUndefined(val) {
    return val === undefined;
}
function isObject(obj) {
    if (!obj || String(obj) !== "[object Object]") {
        return false;
    }
    var proto = Object.getPrototypeOf(obj), constructor = proto && proto.hasOwnProperty("constructor") && proto.constructor;
    return !proto || (typeof constructor === "function" && String(constructor) === String(Object));
}
function isString(str) {
    return typeof str === "string";
}
function closestScroll(node) {
    for (; node; node = node.parentElement) {
        if (/^(auto|scroll)$/i.test(getComputedStyle(node).overflowY)) {
            return node;
        }
    }
}
var ic;
(function (ic) {
    var requestAnimationFrame = window.requestAnimationFrame, rAFname = {}, rAFwrapper = false, immediateIndex = 0, immediateName = {}, immediateCall = false, immediateSecret = "cb" + Math.random(), scaleTop = 0, scaleLeft = 0, userAgent = navigator.userAgent, iPad = userAgent.includes("Safari") && !userAgent.includes("Chrome") && !userAgent.includes("Edge") && window.orientation !== undefined;
    ic.scaleFactor = 1;
    function fixScaling() {
        var body = document.body, html = document.documentElement, style = html.style, zoom = iPad ? (html.clientWidth / window.innerWidth) : 1;
        style.transform = style.transformOrigin = "";
        var box = html.getBoundingClientRect(), scaleWidth = Math.min(1, innerWidth * zoom / body.clientWidth), scaleHeight = Math.min(1, innerHeight * zoom / body.clientHeight), isHorizontal = scaleWidth > scaleHeight;
        ic.scaleFactor = Math.min(scaleWidth, scaleHeight);
        style.transform = ic.scaleFactor >= 1 ? "" : "scale(" + ic.scaleFactor + ")";
        style.transformOrigin = ic.scaleFactor >= 1 ? "" : (isHorizontal ? (iPad && Math.abs(window.orientation) === 90 ? "25%" : "50%") : "0") + " 0";
        box = html.getBoundingClientRect();
        scaleTop = box.top;
        scaleLeft = box.left;
    }
    ic.fixScaling = fixScaling;
    ;
    function getCoords(event, scale) {
        if (event) {
            var isTouch = !!event.changedTouches, clientX = (isTouch ? event.changedTouches[0] : event).clientX, clientY = (isTouch ? event.changedTouches[0] : event).clientY;
            return [
                scale !== false ? (clientX - scaleLeft) / ic.scaleFactor : clientX,
                scale !== false ? (clientY - scaleTop) / ic.scaleFactor : clientY
            ];
        }
        return [0, 0];
    }
    ic.getCoords = getCoords;
    function callbackAndDelete(obj, args) {
        Object.keys(obj).forEach(function (name) {
            var callback = obj[name];
            delete obj[name];
            callback(args);
        });
    }
    function rAFCallback(time) {
        rAFwrapper = false;
        callbackAndDelete(rAFname, time);
    }
    function immediateCallback() {
        immediateCall = false;
        immediateIndex = 0;
        callbackAndDelete(immediateName);
    }
    function rAF(name, callback) {
        var fn;
        if (isString(name)) {
            fn = callback;
            if (callback) {
                rAFname[name] = callback;
                if (rAFwrapper) {
                    fn = undefined;
                }
                else {
                    rAFwrapper = true;
                    fn = rAFCallback;
                }
            }
            else {
                delete rAFname[name];
            }
        }
        else {
            fn = name;
        }
        if (fn) {
            if (requestAnimationFrame && !document.hidden) {
                requestAnimationFrame(fn);
            }
            else {
                setTimeout(fn, 16);
            }
        }
    }
    ic.rAF = rAF;
    ;
    addEventListener("message", function (event) {
        if (event.source == window && event.data == immediateSecret) {
            event.stopPropagation();
            immediateCallback();
        }
    }, true);
    function setImmediate(name, callback) {
        if (isString(name)) {
            if (callback) {
                immediateName[name] = callback;
            }
            else {
                delete immediateName[name];
                return;
            }
        }
        else if (isFunction(name)) {
            for (var i in immediateName) {
                if (immediateName[i] === name) {
                    return;
                }
            }
            immediateName[String(immediateIndex++)] = name;
        }
        else {
            return;
        }
        if (!immediateCall) {
            immediateCall = true;
            if (postMessage) {
                postMessage(immediateSecret, "*");
            }
            else {
                setTimeout(immediateCallback, 0);
            }
        }
    }
    ic.setImmediate = setImmediate;
    ;
})(ic || (ic = {}));
;
function createElement(tagName) {
    return document.createElement(tagName);
}
function getElementById(elementId) {
    return document.getElementById(elementId);
}
function getElementsByTagName(tagname) {
    return Array.prototype.slice.call(document.getElementsByTagName(tagname));
}
function querySelector(root, selector) {
    if (isString(root)) {
        selector = root;
        root = document;
    }
    return root.querySelector(selector);
}
function querySelectorSVG(root, selector) {
    return querySelector(root, selector);
}
function querySelectorAll(root, selector) {
    if (isString(root)) {
        selector = root;
        root = document;
    }
    return root ? Array.prototype.slice.call(root.querySelectorAll(selector)) : [];
}
function defineProperty(proto, name, value) {
    if (proto && !proto[name]) {
        Object.defineProperty(proto, name, {
            "value": value
        });
    }
}
defineProperty(Array.prototype, "equals", function (target) {
    if (!target || this.length !== target.length) {
        return false;
    }
    for (var index = 0; index < this.length; index++) {
        if (typeof this[index] !== typeof target[index] || !this[index] !== !target[index]) {
            return false;
        }
        switch (typeof (this[index])) {
            case "object":
                if (this[index] !== null && target[index] !== null && (this[index].constructor.toString() !== target[index].constructor.toString() || !this[index].equals(target[index]))) {
                    return false;
                }
                break;
            case "function":
                if (this[index].toString() !== target[index].toString()) {
                    return false;
                }
                break;
            default:
                if (this[index] !== target[index]) {
                    return false;
                }
        }
    }
    return true;
});
defineProperty(Array.prototype, "explode", function () {
    var i, num, arr = this.slice(0);
    for (i = 0; i < arr.length; i++) {
        num = parseInt(arr[i], 10);
        if (String(num) === arr[i]) {
            arr[i] = num;
        }
    }
    return arr;
});
defineProperty(Array.prototype, "find", function (predicate) {
    if (this === null || this === undefined) {
        throw new TypeError('Array.prototype.find called on null or undefined');
    }
    if (typeof predicate !== 'function') {
        throw new TypeError('predicate must be a function');
    }
    var list = Object(this), length = list.length >>> 0, thisArg = arguments[1], value;
    for (var i = 0; i < length; i++) {
        value = list[i];
        if (predicate.call(thisArg, value, i, list)) {
            return value;
        }
    }
    return undefined;
});
defineProperty(Array.prototype, "findIndex", function (predicate) {
    if (this === null || this === undefined) {
        throw new TypeError('Array.prototype.findIndex called on null or undefined');
    }
    if (typeof predicate !== 'function') {
        throw new TypeError('predicate must be a function');
    }
    var list = Object(this), length = list.length >>> 0, thisArg = arguments[1], value;
    for (var i = 0; i < length; i++) {
        value = list[i];
        if (predicate.call(thisArg, value, i, list)) {
            return i;
        }
    }
    return -1;
});
defineProperty(Array.prototype, "first", function () {
    return this[0];
});
defineProperty(Array, "from", function (arrayLike, _mapFn, thisArg) {
    var toStr = Object.prototype.toString, isCallable = function (fn) {
        return typeof fn === 'function' || toStr.call(fn) === '[object Function]';
    }, toInteger = function (value) {
        var number = Number(value);
        if (isNaN(number)) {
            return 0;
        }
        if (number === 0 || !isFinite(number)) {
            return number;
        }
        return (number > 0 ? 1 : -1) * Math.floor(Math.abs(number));
    }, maxSafeInteger = Math.pow(2, 53) - 1, toLength = function (value) {
        var len = toInteger(value);
        return Math.min(Math.max(len, 0), maxSafeInteger);
    }, C = this, items = Object(arrayLike);
    if (arrayLike == null) {
        throw new TypeError('Array.from requires an array-like object - not null or undefined');
    }
    var mapFn = arguments.length > 1 ? arguments[1] : void undefined, T;
    if (typeof mapFn !== 'undefined') {
        if (!isCallable(mapFn)) {
            throw new TypeError('Array.from: when provided, the second argument must be a function');
        }
        if (arguments.length > 2) {
            T = arguments[2];
        }
    }
    var len = toLength(items.length), A = isCallable(C) ? Object(new C(len)) : new Array(len), k = 0, kValue;
    while (k < len) {
        kValue = items[k];
        if (mapFn) {
            A[k] = T === undefined ? mapFn(kValue, k) : mapFn.call(T, kValue, k);
        }
        else {
            A[k] = kValue;
        }
        k += 1;
    }
    A.length = len;
    return A;
});
defineProperty(Array.prototype, "includes", function (searchElement, fromIndex) {
    var i, currentElement, isNaN = searchElement !== searchElement;
    fromIndex = fromIndex || 0;
    for (i = fromIndex >= 0 ? fromIndex : Math.max(0, length + fromIndex); i < this.length; i++) {
        currentElement = this[i];
        if (searchElement === currentElement ||
            (isNaN && currentElement !== currentElement)) {
            return true;
        }
    }
    return false;
});
defineProperty(Array.prototype, "intersect", function (searchArray) {
    if (!isArray(searchArray)) {
        searchArray = [].slice.call(arguments);
    }
    return this.filter(function (value) {
        return searchArray.includes(value);
    });
});
defineProperty(Array.prototype, "last", function () {
    return this[this.length - 1];
});
defineProperty(Array.prototype, "not", function (searchArray) {
    if (!isArray(searchArray)) {
        searchArray = [].slice.call(arguments);
    }
    return this.filter(function (value) {
        return !searchArray.includes(value);
    });
});
defineProperty(Array.prototype, "pushOnce", function () {
    var that = this, i = 0, elements = arguments;
    for (; i < elements.length; i++) {
        if (!that.includes(elements[i])) {
            that.push(elements[i]);
        }
    }
    return that.length;
});
defineProperty(Array.prototype, "remove", function (searchElement) {
    var i = this.indexOf(searchElement);
    return i >= 0 ? this.splice(i, 1) : [];
});
defineProperty(Array.prototype, "shuffle", function () {
    var current = this.length;
    while (current > 0) {
        var i = Math.floor(Math.random() * current--), tmp = this[current];
        this[current] = this[i];
        this[i] = tmp;
    }
    return this;
});
defineProperty(Array.prototype, "wrap", function (between, before, after) {
    if (isUndefined(between)) {
        between = "";
    }
    if (isUndefined(before)) {
        before = "";
    }
    if (isUndefined(after)) {
        after = "";
    }
    return before + this.join(after + between + before) + after;
});
defineProperty(Date, "format", function (format) {
    return (new Date()).format(format);
});
defineProperty(Date.prototype, "format", function (format) {
    var output = "";
    if (isNaN(this)) {
        return "Invalid Date";
    }
    else {
        var that_1 = this, date_1 = that_1.getDate(), day_1 = that_1.getDay(), month_1 = that_1.getMonth(), year_1 = that_1.getFullYear(), hours_1 = that_1.getHours(), minutes_1 = that_1.getMinutes(), seconds_1 = that_1.getSeconds(), skip_1 = false, monthNames_1 = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"], dayNames_1 = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"], pad_1 = function (num) {
            return num <= 9 ? "0" + num : "" + num;
        }, plural_1 = function (num, word) {
            return num + " " + word + (num === 1 ? "" : "s");
        }, replace = function (letter) {
            if (skip_1) {
                skip_1 = false;
                return letter;
            }
            switch (letter) {
                case "\\":
                    skip_1 = true;
                    return "";
                case "d":
                    return pad_1(date_1);
                case "D":
                    return dayNames_1[day_1].substr(0, 3);
                case "j":
                    return date_1;
                case "l":
                    return dayNames_1[day_1];
                case "N":
                    return day_1 + 1;
                case "S":
                    return (date_1 % 10 === 1 && date_1 !== 11 ? "st" : (date_1 % 10 === 2 && date_1 !== 12 ? "nd" : (date_1 % 10 === 3 && date_1 !== 13 ? "rd" : "th")));
                case "w":
                    return day_1;
                case "R":
                    var i = (new Date().getTime() - that_1.getTime()) / 1000;
                    return i < 0
                        ? "In the Future"
                        : i < 20
                            ? "Just Now"
                            : i < 60
                                ? "Less than a Minute Ago"
                                : i < 120
                                    ? "About a Minute Ago"
                                    : i < (60 * 60)
                                        ? plural_1(Math.floor(i / 60), "Minute") + " Ago"
                                        : i < (2 * 60 * 60)
                                            ? "About an Hour Ago"
                                            : i < (12 * 60 * 60)
                                                ? plural_1(Math.floor(i / (60 * 60)), "Hour") + " Ago"
                                                : i < (2 * 24 * 60 * 60) && day_1 === (new Date).getDay()
                                                    ? "Today"
                                                    : i < (3 * 24 * 60 * 60) && (day_1 === 6 ? 0 : day_1 + 1) === (new Date).getDay()
                                                        ? "Yesterday"
                                                        : i < (7 * 24 * 60 * 60)
                                                            ? plural_1(Math.floor(i / (24 * 60 * 60)), "Day") + " Ago"
                                                            : i < (31 * 24 * 60 * 60)
                                                                ? plural_1(Math.floor(i / (7 * 24 * 60 * 60)), "Week") + " Ago"
                                                                : i < (365 * 24 * 60 * 60)
                                                                    ? plural_1(Math.floor(i / (31 * 24 * 60 * 60)), "Month") + " Ago"
                                                                    : plural_1(Math.floor(i / (365 * 24 * 60 * 60)), "Year") + " Ago";
                case "F":
                    return monthNames_1[month_1];
                case "m":
                    return pad_1(month_1 + 1);
                case "M":
                    return monthNames_1[month_1].substr(0, 3);
                case "n":
                    return month_1 + 1;
                case "L":
                    return (((year_1 % 4 === 0) && (year_1 % 100 !== 0)) || (year_1) % 400 === 0) ? "1" : "0";
                case "Y":
                    return year_1;
                case "y":
                    return String(year_1).substr(2);
                case "a":
                    return hours_1 < 12 ? "am" : "pm";
                case "A":
                    return hours_1 < 12 ? "AM" : "PM";
                case "g":
                    return hours_1 % 12 || 12;
                case "G":
                    return hours_1;
                case "h":
                    return pad_1(hours_1 % 12 || 12);
                case "H":
                    return pad_1(hours_1);
                case "i":
                    return pad_1(minutes_1);
                case "s":
                    return pad_1(seconds_1);
                case "u":
                    return String(that_1.getMilliseconds()).substr(0, 3);
                case "O":
                    {
                        var offset = that_1.getTimezoneOffset();
                        return (offset < 0 ? "+" : "-") + pad_1(Math.abs(offset / 60)) + "00";
                    }
                case "P":
                    {
                        var offset = that_1.getTimezoneOffset();
                        return (offset < 0 ? "+" : "-") + pad_1(Math.abs(offset / 60)) + ":" + pad_1(Math.abs(offset % 60));
                    }
                case "T":
                    that_1.setMonth(0);
                    var result = that_1.toTimeString().replace(/^.+ \(?([^\)]+)\)?$/, "$1");
                    that_1.setMonth(month_1);
                    return result;
                case "Z":
                    return -that_1.getTimezoneOffset() * 60;
                case "c":
                    return that_1.format("Y-m-d\\TH:i:sP");
                case "r":
                    return String(that_1);
                case "U":
                    return that_1.getTime() / 1000;
                default:
                    return letter;
            }
        };
        format = format || "c";
        for (var i = 0; i < format.length; i++) {
            output += replace(format[i]);
        }
    }
    return output;
});
defineProperty(Element.prototype, "closest", function (selectors) {
    var element = this;
    while (element && !element.matches(selectors)) {
        element = element.parentElement;
    }
    return element;
});
defineProperty(Element.prototype, "closestWidget", function (selectors) {
    var element = this;
    while (element && !element.tagName.startsWith("IC-") && (!selectors || !element.matches(selectors))) {
        element = element.parentElement;
    }
    return element;
});
defineProperty(Element.prototype, "common", function (element) {
    var el_1 = element ? this : undefined, el_2 = element;
    while (el_1 && el_2 && el_1 !== el_2) {
        while (el_2 && el_1 !== el_2) {
            el_2 = el_2.parentElement;
        }
        if (el_2) {
            break;
        }
        el_1 = el_1.parentElement;
        el_2 = element;
    }
    return el_1;
});
defineProperty(Element.prototype, "fixState", function (currentState, index) {
    var element = this, tagName = element.tagName;
    if (tagName.startsWith("IC-")) {
        var i = void 0, state = void 0, className = void 0, attributeNameClass = void 0, attribute = void 0, tagNameClass = tagName.toLowerCase().replace(/^ic-/, "ict-"), attributes = element.attributes, currentClass_1 = element.classList, addClass_1 = {};
        addClass_1[tagNameClass] = true;
        if (index !== undefined && index >= 0) {
            addClass_1[tagNameClass + "-" + index] = true;
        }
        for (i = 0; i < attributes.length; i++) {
            attribute = attributes[i];
            if (attribute.name.startsWith("ic-")) {
                attributeNameClass = attribute.name.replace(/^ic-/, "ict-");
                if (attributeNameClass === "state") {
                    if (attribute.value.trim()) {
                        attribute.value.split(/\s+/).forEach(function (state) {
                            addClass_1["ict-state-" + state] = true;
                        });
                    }
                }
                else {
                    addClass_1[attributeNameClass] = true;
                    if (attribute.value && !/[^a-z0-9_-]/.test(attribute.value)) {
                        addClass_1[attributeNameClass + "-" + attribute.value] = true;
                    }
                }
            }
        }
        if (currentState) {
            for (state in currentState) {
                if (currentState[state] === true) {
                    addClass_1["ict-state-" + state] = true;
                }
            }
        }
        for (i = currentClass_1.length - 1; i >= 0; i--) {
            className = currentClass_1[i];
            if (className.startsWith("ict-") && !addClass_1[className]) {
                currentClass_1.remove(className);
            }
        }
        Object.keys(addClass_1).forEach(function (className) {
            currentClass_1.add(className);
        });
    }
});
defineProperty(Element.prototype, "matches", Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector);
defineProperty(Node.prototype, "parentElements", function (stop, andSelf) {
    var element = this, isString = typeof stop === "string", stopSelector = isString ? stop : undefined, stopElement = isString ? undefined : stop, result = andSelf ? [element] : [];
    while (element && (!stopSelector || !element.matches(stopSelector)) && (!stopElement || element !== stopElement)) {
        result.push(element = element.parentElement);
    }
    return result;
});
defineProperty(Element.prototype, "remove", function () {
    if (this.parentNode) {
        this.parentNode.removeChild(this);
    }
});
(function (doc, proto) {
    try {
        doc.querySelector(":scope body");
    }
    catch (e) {
        ["querySelector", "querySelectorAll"].forEach(function (method) {
            var native = proto[method];
            proto[method] = function (selectors) {
                if (/(^|,)\s*:scope/.test(selectors)) {
                    var el = this, id = el.id;
                    el.id = "SCOPE_" + Date.now();
                    selectors = selectors.replace(/((^|,)\s*):scope/g, "$1#" + el.id);
                    var result = doc[method](selectors);
                    el.id = id;
                    return result;
                }
                else {
                    return native.call(this, selectors);
                }
            };
        });
    }
})(window.document, Element.prototype);
defineProperty(Math, "range", function (min, num, max) {
    return Math.max(min, Math.min(num, max));
});
defineProperty(Number.prototype, "range", function (min, max) {
    return this >= Math.min(min, max) && this <= Math.max(min, max);
});
defineProperty(Number.prototype, "round", function (digits) {
    var num = +("1e" + Math.max(0, digits));
    return Math.round(this * num) / num;
});
defineProperty(Number.prototype, "toRoman", function (lower) {
    var i, num = Math.floor(this), lookup = {
        "M": 1000,
        "CM": 900,
        "D": 500,
        "CD": 400,
        "C": 100,
        "XC": 90,
        "L": 50,
        "XL": 40,
        "X": 10,
        "IX": 9,
        "V": 5,
        "IV": 4,
        "I": 1
    }, roman = "";
    for (i in lookup) {
        while (num >= lookup[i]) {
            roman += i;
            num -= lookup[i];
        }
    }
    return lower ? roman.toLowerCase() : roman;
});
defineProperty(Object.prototype, "clone", function ObjectCopyCloneFn(target, save) {
    var copy, key, value;
    if (this === null || typeof this !== "object") {
        return this;
    }
    if (this instanceof Date) {
        copy = target || new Date();
        copy.setTime(this.getTime());
        return copy;
    }
    if (this instanceof Function) {
        return this;
    }
    if (this instanceof Array) {
        if (target instanceof Array) {
            copy = target;
            if (save !== false) {
                target.length = 0;
            }
        }
        else {
            copy = [];
        }
        for (key = 0; key < this.length; key++) {
            value = this[key];
            if (isObject(value)) {
                copy[key] = ObjectCopyCloneFn.call(value);
            }
            else {
                copy[key] = value;
            }
        }
        return copy;
    }
    if (this instanceof Object) {
        if (target instanceof Object) {
            copy = target;
            if (save !== false) {
                for (key in target) {
                    if (!this.hasOwnProperty(key) && target.hasOwnProperty(key)) {
                        delete target[key];
                    }
                }
            }
        }
        else {
            copy = {};
        }
        for (key in this) {
            if (this.hasOwnProperty(key)) {
                value = this[key];
                if (value instanceof Object) {
                    copy[key] = ObjectCopyCloneFn.call(value);
                }
                else {
                    copy[key] = value;
                }
            }
        }
        return copy;
    }
    throw new Error("Unable to copy obj! Its type isn't supported.");
});
defineProperty(Object.prototype, "getTree", function (path, def) {
    var data = this, fixedPath = path.explode();
    while (fixedPath.length && data !== undefined) {
        data = data[fixedPath.shift()];
    }
    return data === undefined || data === null ? def : data;
});
defineProperty(Object.prototype, "setTree", function (path, value) {
    var _reduce = function (data) {
        if (isArray(data)) {
            while (data.length && data[data.length - 1] === undefined || data[data.length - 1] === null) {
                data.pop();
            }
        }
    }, _set = function (data, fixedPath, fixedValue, depth) {
        var i = fixedPath[depth];
        if (depth < fixedPath.length - 1) {
            if (isString(data[i]) && isNumber(fixedPath[depth + 1])) {
                data[i] = [data[i]];
            }
            else if (!isObject(data[i]) && !isArray(data[i])) {
                data[i] = isNumber(fixedPath[depth + 1]) ? [] : {};
            }
            if (!_set(data[i], fixedPath, fixedValue, depth + 1) && depth >= 1 && !Object.keys(data[i])) {
                delete data[i];
                _reduce(data);
                return false;
            }
        }
        else if (fixedValue !== data[i]) {
            if (fixedValue === undefined) {
                delete data[i];
                _reduce(data);
                return false;
            }
            data[i] = fixedValue;
        }
        return true;
    };
    if (isObject(path)) {
        for (var i in path) {
            if (path.hasOwnProperty(i)) {
                _set(this, i.explode(), path[i], 0);
            }
        }
    }
    else {
        _set(this, path.explode(), value, 0);
    }
    return this;
});
(function () {
    'use strict';
    function objectOrFunction(x) {
        return typeof x === 'function' || typeof x === 'object' && x !== null;
    }
    function isFunction(x) {
        return typeof x === 'function';
    }
    var _isArray = undefined;
    if (!Array.isArray) {
        _isArray = function (x) {
            return Object.prototype.toString.call(x) === '[object Array]';
        };
    }
    else {
        _isArray = Array.isArray;
    }
    var isArray = _isArray;
    var len = 0;
    var vertxNext = undefined;
    var customSchedulerFn = undefined;
    var asap = function asap(callback, arg) {
        queue[len] = callback;
        queue[len + 1] = arg;
        len += 2;
        if (len === 2) {
            if (customSchedulerFn) {
                customSchedulerFn(flush);
            }
            else {
                scheduleFlush();
            }
        }
    };
    function setScheduler(scheduleFn) {
        customSchedulerFn = scheduleFn;
    }
    function setAsap(asapFn) {
        asap = asapFn;
    }
    function useMutationObserver() {
        var iterations = 0;
        var observer = new MutationObserver(flush);
        var node = document.createTextNode('');
        observer.observe(node, { characterData: true });
        return function () {
            node.data = String(iterations = ++iterations % 2);
        };
    }
    function useSetTimeout() {
        var globalSetTimeout = setTimeout;
        return function () {
            return globalSetTimeout(flush, 1);
        };
    }
    var queue = new Array(1000);
    function flush() {
        for (var i = 0; i < len; i += 2) {
            var callback = queue[i];
            var arg = queue[i + 1];
            callback(arg);
            queue[i] = undefined;
            queue[i + 1] = undefined;
        }
        len = 0;
    }
    var scheduleFlush = undefined;
    if (MutationObserver) {
        scheduleFlush = useMutationObserver();
    }
    else {
        scheduleFlush = useSetTimeout();
    }
    function then(onFulfillment, onRejection) {
        var _arguments = arguments;
        var parent = this;
        var child = new this.constructor(noop);
        if (child[PROMISE_ID] === undefined) {
            makePromise(child);
        }
        var _state = parent._state;
        if (_state) {
            (function () {
                var callback = _arguments[_state - 1];
                asap(function () {
                    return invokeCallback(_state, child, callback, parent._result);
                });
            })();
        }
        else {
            subscribe(parent, child, onFulfillment, onRejection);
        }
        return child;
    }
    function resolve(object) {
        var Constructor = this;
        if (object && typeof object === 'object' && object.constructor === Constructor) {
            return object;
        }
        var promise = new Constructor(noop);
        _resolve(promise, object);
        return promise;
    }
    var PROMISE_ID = Math.random().toString(36).substring(16);
    function noop() { }
    var PENDING = void 0;
    var FULFILLED = 1;
    var REJECTED = 2;
    var GET_THEN_ERROR = new ErrorObject();
    function selfFulfillment() {
        return new TypeError("You cannot resolve a promise with itself");
    }
    function cannotReturnOwn() {
        return new TypeError('A promises callback cannot return that same promise.');
    }
    function getThen(promise) {
        try {
            return promise.then;
        }
        catch (error) {
            GET_THEN_ERROR.error = error;
            return GET_THEN_ERROR;
        }
    }
    function tryThen(then, value, fulfillmentHandler, rejectionHandler, arg) {
        try {
            then.call(value, fulfillmentHandler, rejectionHandler);
        }
        catch (e) {
            return e;
        }
    }
    function handleForeignThenable(promise, thenable, then) {
        asap(function (promise) {
            var sealed = false;
            var error = tryThen(then, thenable, function (value) {
                if (sealed) {
                    return;
                }
                sealed = true;
                if (thenable !== value) {
                    _resolve(promise, value);
                }
                else {
                    fulfill(promise, value);
                }
            }, function (reason) {
                if (sealed) {
                    return;
                }
                sealed = true;
                _reject(promise, reason);
            }, 'Settle: ' + (promise._label || ' unknown promise'));
            if (!sealed && error) {
                sealed = true;
                _reject(promise, error);
            }
        }, promise);
    }
    function handleOwnThenable(promise, thenable) {
        if (thenable._state === FULFILLED) {
            fulfill(promise, thenable._result);
        }
        else if (thenable._state === REJECTED) {
            _reject(promise, thenable._result);
        }
        else {
            subscribe(thenable, undefined, function (value) {
                return _resolve(promise, value);
            }, function (reason) {
                return _reject(promise, reason);
            });
        }
    }
    function handleMaybeThenable(promise, maybeThenable, then$$) {
        if (maybeThenable.constructor === promise.constructor && then$$ === then && maybeThenable.constructor.resolve === resolve) {
            handleOwnThenable(promise, maybeThenable);
        }
        else {
            if (then$$ === GET_THEN_ERROR) {
                _reject(promise, GET_THEN_ERROR.error);
            }
            else if (then$$ === undefined) {
                fulfill(promise, maybeThenable);
            }
            else if (isFunction(then$$)) {
                handleForeignThenable(promise, maybeThenable, then$$);
            }
            else {
                fulfill(promise, maybeThenable);
            }
        }
    }
    function _resolve(promise, value) {
        if (promise === value) {
            _reject(promise, selfFulfillment());
        }
        else if (objectOrFunction(value)) {
            handleMaybeThenable(promise, value, getThen(value));
        }
        else {
            fulfill(promise, value);
        }
    }
    function publishRejection(promise) {
        if (promise._onerror) {
            promise._onerror(promise._result);
        }
        publish(promise);
    }
    function fulfill(promise, value) {
        if (promise._state !== PENDING) {
            return;
        }
        promise._result = value;
        promise._state = FULFILLED;
        if (promise._subscribers.length !== 0) {
            asap(publish, promise);
        }
    }
    function _reject(promise, reason) {
        if (promise._state !== PENDING) {
            return;
        }
        promise._state = REJECTED;
        promise._result = reason;
        asap(publishRejection, promise);
    }
    function subscribe(parent, child, onFulfillment, onRejection) {
        var _subscribers = parent._subscribers;
        var length = _subscribers.length;
        parent._onerror = null;
        _subscribers[length] = child;
        _subscribers[length + FULFILLED] = onFulfillment;
        _subscribers[length + REJECTED] = onRejection;
        if (length === 0 && parent._state) {
            asap(publish, parent);
        }
    }
    function publish(promise) {
        var subscribers = promise._subscribers;
        var settled = promise._state;
        if (subscribers.length === 0) {
            return;
        }
        var child = undefined, callback = undefined, detail = promise._result;
        for (var i = 0; i < subscribers.length; i += 3) {
            child = subscribers[i];
            callback = subscribers[i + settled];
            if (child) {
                invokeCallback(settled, child, callback, detail);
            }
            else {
                callback(detail);
            }
        }
        promise._subscribers.length = 0;
    }
    function ErrorObject() {
        this.error = null;
    }
    var TRY_CATCH_ERROR = new ErrorObject();
    function tryCatch(callback, detail) {
        try {
            return callback(detail);
        }
        catch (e) {
            TRY_CATCH_ERROR.error = e;
            return TRY_CATCH_ERROR;
        }
    }
    function invokeCallback(settled, promise, callback, detail) {
        var hasCallback = isFunction(callback), value = undefined, error = undefined, succeeded = undefined, failed = undefined;
        if (hasCallback) {
            value = tryCatch(callback, detail);
            if (value === TRY_CATCH_ERROR) {
                failed = true;
                error = value.error;
                value = null;
            }
            else {
                succeeded = true;
            }
            if (promise === value) {
                _reject(promise, cannotReturnOwn());
                return;
            }
        }
        else {
            value = detail;
            succeeded = true;
        }
        if (promise._state !== PENDING) {
        }
        else if (hasCallback && succeeded) {
            _resolve(promise, value);
        }
        else if (failed) {
            _reject(promise, error);
        }
        else if (settled === FULFILLED) {
            fulfill(promise, value);
        }
        else if (settled === REJECTED) {
            _reject(promise, value);
        }
    }
    function initializePromise(promise, resolver) {
        try {
            resolver(function resolvePromise(value) {
                _resolve(promise, value);
            }, function rejectPromise(reason) {
                _reject(promise, reason);
            });
        }
        catch (e) {
            _reject(promise, e);
        }
    }
    var id = 0;
    function nextId() {
        return id++;
    }
    function makePromise(promise) {
        promise[PROMISE_ID] = id++;
        promise._state = undefined;
        promise._result = undefined;
        promise._subscribers = [];
    }
    function Enumerator(Constructor, input) {
        this._instanceConstructor = Constructor;
        this.promise = new Constructor(noop);
        if (!this.promise[PROMISE_ID]) {
            makePromise(this.promise);
        }
        if (isArray(input)) {
            this._input = input;
            this.length = input.length;
            this._remaining = input.length;
            this._result = new Array(this.length);
            if (this.length === 0) {
                fulfill(this.promise, this._result);
            }
            else {
                this.length = this.length || 0;
                this._enumerate();
                if (this._remaining === 0) {
                    fulfill(this.promise, this._result);
                }
            }
        }
        else {
            _reject(this.promise, validationError());
        }
    }
    function validationError() {
        return new Error('Array Methods must be provided an Array');
    }
    ;
    Enumerator.prototype._enumerate = function () {
        var length = this.length;
        var _input = this._input;
        for (var i = 0; this._state === PENDING && i < length; i++) {
            this._eachEntry(_input[i], i);
        }
    };
    Enumerator.prototype._eachEntry = function (entry, i) {
        var c = this._instanceConstructor;
        var resolve$$ = c.resolve;
        if (resolve$$ === resolve) {
            var _then = getThen(entry);
            if (_then === then && entry._state !== PENDING) {
                this._settledAt(entry._state, i, entry._result);
            }
            else if (typeof _then !== 'function') {
                this._remaining--;
                this._result[i] = entry;
            }
            else if (c === Promise) {
                var promise = new c(noop);
                handleMaybeThenable(promise, entry, _then);
                this._willSettleAt(promise, i);
            }
            else {
                this._willSettleAt(new c(function (resolve$$) {
                    return resolve$$(entry);
                }), i);
            }
        }
        else {
            this._willSettleAt(resolve$$(entry), i);
        }
    };
    Enumerator.prototype._settledAt = function (state, i, value) {
        var promise = this.promise;
        if (promise._state === PENDING) {
            this._remaining--;
            if (state === REJECTED) {
                _reject(promise, value);
            }
            else {
                this._result[i] = value;
            }
        }
        if (this._remaining === 0) {
            fulfill(promise, this._result);
        }
    };
    Enumerator.prototype._willSettleAt = function (promise, i) {
        var enumerator = this;
        subscribe(promise, undefined, function (value) {
            return enumerator._settledAt(FULFILLED, i, value);
        }, function (reason) {
            return enumerator._settledAt(REJECTED, i, reason);
        });
    };
    function all(entries) {
        return new Enumerator(this, entries).promise;
    }
    function race(entries) {
        var Constructor = this;
        if (!isArray(entries)) {
            return new Constructor(function (_, reject) {
                return reject(new TypeError('You must pass an array to race.'));
            });
        }
        else {
            return new Constructor(function (resolve, reject) {
                var length = entries.length;
                for (var i = 0; i < length; i++) {
                    Constructor.resolve(entries[i]).then(resolve, reject);
                }
            });
        }
    }
    function reject(reason) {
        var Constructor = this;
        var promise = new Constructor(noop);
        _reject(promise, reason);
        return promise;
    }
    function needsResolver() {
        throw new TypeError('You must pass a resolver function as the first argument to the promise constructor');
    }
    function needsNew() {
        throw new TypeError("Failed to construct 'Promise': Please use the 'new' operator, this object constructor cannot be called as a function.");
    }
    function Promise(resolver) {
        this[PROMISE_ID] = nextId();
        this._result = this._state = undefined;
        this._subscribers = [];
        if (noop !== resolver) {
            typeof resolver !== 'function' && needsResolver();
            this instanceof Promise ? initializePromise(this, resolver) : needsNew();
        }
    }
    Promise.all = all;
    Promise.race = race;
    Promise.resolve = resolve;
    Promise.reject = reject;
    Promise._setScheduler = setScheduler;
    Promise._setAsap = setAsap;
    Promise._asap = asap;
    Promise.prototype = {
        constructor: Promise,
        then: then,
        'catch': function _catch(onRejection) {
            return this.then(null, onRejection);
        }
    };
    function polyfill() {
        var local = window;
        var P = local.Promise;
        if (P) {
            var promiseToString = null;
            try {
                promiseToString = Object.prototype.toString.call(P.resolve());
            }
            catch (e) {
            }
            if (promiseToString === '[object Promise]' && !P.cast) {
                return;
            }
        }
        local.Promise = Promise;
    }
    Promise.polyfill = polyfill;
    Promise.Promise = Promise;
    Promise.polyfill();
})();
defineProperty(RegExp.prototype, "expand", function () {
    function expandRegexp(regExp) {
        if (regExp[0] === "^") {
            regExp = regExp.substr(1);
        }
        else {
            regExp = ".*" + regExp;
        }
        if (regExp[regExp.length - 1] === "^") {
            regExp = regExp.substr(0, regExp.length - 2);
        }
        else {
            regExp += ".*";
        }
        var myRegExp = regExp.replace(/^[\n\r\^]+|[\n\r\$]+$/g, ""), matches;
        myRegExp = myRegExp.replace(/\(([^|]*?\))(\?)/g, function ($0, $1, $2) {
            return $2 ? "(" + $1 + "|)?" : $1;
        });
        myRegExp = myRegExp.replace(/(\\?[^\\\)\*])\?/g, function ($0, $1) {
            return "(" + $1 + "|)";
        });
        while ((matches = myRegExp.match(/\[.*?\]/g))) {
            var squareBrackets = matches, sb1 = squareBrackets[0], sb = sb1.substring(1, sb1.length - 1), regHyphen = /.?-.?/;
            while (myRegExp.match(regHyphen) != null) {
                var hyphen = myRegExp.match(regHyphen), stringLeft = myRegExp.split(hyphen[0])[0];
                if (stringLeft.charAt(stringLeft.length - 1) == "|") {
                    stringLeft = stringLeft.substring(0, stringLeft.length - 1);
                }
                var stringRight = myRegExp.split(hyphen[0])[1];
                var hy1 = hyphen[0].charAt(0), hy2 = hyphen[0].charAt(2), hy = "", charList = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ%8A%8C%C0%C1%C2%C3%C4%C5%C6%C7%C8%C9%CA%CB%CC%CD%CE%CF%D0%D1%D2%D3%D4%D5%D6%D8%D9%DA%DB%DC%DD%DE%9Fabcdefghijklmnopqrstuvwxyz%9A%9C%E0%E1%E2%E3%E4%E5%E6%E7%E8%E9%EA%EB%EC%ED%EE%EF%F0%F1%F2%F3%F4%F5%F6%F8%F9%FA%FB%FC%FD%FE%FFéèÿ";
                for (var c = charList.indexOf(hy1); c < charList.indexOf(hy2) + 1; c++) {
                    hy += charList.charAt(c);
                }
                myRegExp = stringLeft + hy + stringRight;
            }
            var sb2 = "(";
            for (var i = 0; i < sb.length; i++) {
                sb2 += sb.charAt(i) + "|";
            }
            sb2 = sb2.substring(0, sb2.length - 1) + ")";
            myRegExp = replaceWord(myRegExp, sb1, sb2);
        }
        myRegExp = myRegExp.replace(/\\?%/g, "%25").replace(/\\(.)/g, function ($0, $1) {
            return "%" + $1.charCodeAt(0).toString(16);
        });
        if (/[^\\][\+\*\.\{\}]/.test(" " + myRegExp)) {
            var altExp = regExp.replace(/\.[\*\?]/g, "");
            return altExp === regExp ? [regExp] : [regExp, altExp];
        }
        myRegExp = replaceNestedOrs(myRegExp) || myRegExp;
        var result = expandOrs(myRegExp);
        for (var i = 0; i < result.length; i++) {
            result[i] = decodeURIComponent(result[i]);
        }
        return result;
    }
    function getNextNestedOr(str) {
        var start = 0, isNested = false, depth = 0;
        for (var i = 0; i < str.length; i++) {
            switch (str[i]) {
                case "(":
                    depth++;
                    if (depth === 1) {
                        start = i;
                    }
                    else if (depth === 2) {
                        isNested = true;
                    }
                    break;
                case ")":
                    depth--;
                    if (!depth && isNested) {
                        return str.substring(start, i + 1);
                    }
                    break;
            }
        }
        return;
    }
    function getGroup(str) {
        var matches = str.match(/[^(|)]*\(([^(|)]*\|[^(|)]*)+\)[^(|)]*/g);
        if (matches) {
            for (var i = 0; i < matches.length; i++) {
                var match = matches[i];
                if (match[0] !== "(" || match[match.length - 1] !== ")") {
                    return match;
                }
            }
        }
    }
    function replaceNestedOrs(myRegExp) {
        var regSingleOrs = /\([^()]*\)/, regSingleOrsTotal = /^\([^()]*\)$/;
        while (getNextNestedOr(myRegExp)) {
            var nestedOrs = getNextNestedOr(myRegExp), nestedOrsOriginal = nestedOrs, matches = void 0;
            while (getGroup(nestedOrs)) {
                var myParent = getGroup(nestedOrs), leftChar = nestedOrs[nestedOrs.indexOf(myParent) - 1], rightChar = nestedOrs[nestedOrs.indexOf(myParent) + myParent.length], outerChars = leftChar + rightChar, leftPar = "", rightPar = "";
                if (outerChars == ")(") {
                    break;
                }
                switch (outerChars) {
                    case "||":
                    case "()":
                        break;
                    case "((":
                    case "))":
                    case "(|":
                    case "|(":
                    case ")|":
                    case "|)":
                        leftPar = "(";
                        rightPar = ")";
                        break;
                    default:
                        alert("ERROR: switch did not work ???\nouterChars = " + outerChars);
                        break;
                }
                var myResult = leftPar + expandOrs(myParent).join("|") + rightPar;
                nestedOrs = replaceWord(nestedOrs, myParent, myResult);
            }
            while ((matches = nestedOrs.match(/(\(|\|)\([^(|)]*\|[^(|)]*\)(\|\([^(|)]*\|[^(|)]*\))*(\)|\|)/))) {
                var plainOrs = matches[0];
                nestedOrs = replaceWord(nestedOrs, plainOrs, "(" + plainOrs.replace(/\(|\)/g, "") + ")");
                if (getGroup(nestedOrs)) {
                    myRegExp = replaceWord(myRegExp, nestedOrsOriginal, nestedOrs);
                    continue;
                }
            }
            while ((matches = nestedOrs.match(/(\([^(]*?\|*?\)){2,99}/))) {
                nestedOrs = nestedOrs.replace(matches[0], expandOrs(matches[0]).join("|"));
            }
            if (getGroup(nestedOrs)) {
                myRegExp = replaceWord(myRegExp, nestedOrsOriginal, nestedOrs);
            }
            else {
                while (nestedOrs.match(regSingleOrs) != null) {
                    if (nestedOrs.match(regSingleOrsTotal)[0] == nestedOrs) {
                        break;
                    }
                    var singleParens = nestedOrs.match(regSingleOrs)[0], myResult = singleParens.substring(1, singleParens.length - 1);
                    nestedOrs = replaceWord(nestedOrs, singleParens, myResult);
                    if (getGroup(nestedOrs)) {
                        myRegExp = replaceWord(myRegExp, nestedOrsOriginal, nestedOrs);
                    }
                }
                myRegExp = replaceWord(myRegExp, nestedOrsOriginal, nestedOrs);
            }
        }
        return myRegExp;
    }
    function expandOrs(str) {
        if (!str || !/\(.*\|.*\)/.test(str)) {
            return [str];
        }
        var parts = [], expanded = [""], lastIndex = 0;
        str.split(/(\(.*?\))/).forEach(function (part) {
            var data = part[0] === "(" && part[part.length - 1] === ")"
                ? part.substr(1, part.length - 2).split("|")
                : part;
            parts.push([
                lastIndex = str.indexOf(part, lastIndex),
                data
            ]);
        });
        parts.forEach(function (part) {
            for (var i = 0, length_1 = expanded.length; i < length_1; i++) {
                if (isString(part[1])) {
                    expanded[i] += part[1];
                }
                else {
                    for (var j = 0; j < part[1].length; j++) {
                        expanded.push(expanded[i] + part[1][j]);
                    }
                }
            }
        });
        return expanded;
    }
    function replaceWord(str, substr, newSubstr) {
        return str.replace(new RegExp(substr.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1"), "g"), newSubstr);
    }
    return expandRegexp(this.source).sort();
});
defineProperty(String.prototype, "endsWith", function (searchString, position) {
    var subjectString = this.toString();
    if (typeof position !== "number" || !isFinite(position) || Math.floor(position) !== position || position > subjectString.length) {
        position = subjectString.length;
    }
    position -= searchString.length;
    var lastIndex = subjectString.indexOf(searchString, position);
    return lastIndex !== -1 && lastIndex === position;
});
defineProperty(String.prototype, "repeat", function (count) {
    if (this == null) {
        throw new TypeError('can\'t convert ' + this + ' to object');
    }
    var str = '' + this;
    count = +count;
    if (count != count) {
        count = 0;
    }
    if (count < 0) {
        throw new RangeError('repeat count must be non-negative');
    }
    if (count == Infinity) {
        throw new RangeError('repeat count must be less than infinity');
    }
    count = Math.floor(count);
    if (str.length == 0 || count == 0) {
        return '';
    }
    if (str.length * count >= 1 << 28) {
        throw new RangeError('repeat count must not overflow maximum string size');
    }
    var rpt = '';
    for (;;) {
        if ((count & 1) == 1) {
            rpt += str;
        }
        count >>>= 1;
        if (count == 0) {
            break;
        }
        str += str;
    }
    return rpt;
});
(function () {
    var fromCharCode = String.fromCharCode, keyStrBase64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", keyStrUriSafe = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+-$";
    function _compress(uncompressed, bitsPerChar, getCharFromInt) {
        if (!uncompressed)
            return "";
        var i, value, context_dictionary = {}, context_dictionaryToCreate = {}, context_c = "", context_wc = "", context_w = "", context_enlargeIn = 2, context_dictSize = 3, context_numBits = 2, context_data = [], context_data_val = 0, context_data_position = 0, ii;
        for (ii = 0; ii < uncompressed.length; ii += 1) {
            context_c = uncompressed.charAt(ii);
            if (!Object.prototype.hasOwnProperty.call(context_dictionary, context_c)) {
                context_dictionary[context_c] = context_dictSize++;
                context_dictionaryToCreate[context_c] = true;
            }
            context_wc = context_w + context_c;
            if (Object.prototype.hasOwnProperty.call(context_dictionary, context_wc)) {
                context_w = context_wc;
            }
            else {
                if (Object.prototype.hasOwnProperty.call(context_dictionaryToCreate, context_w)) {
                    if (context_w.charCodeAt(0) < 256) {
                        for (i = 0; i < context_numBits; i++) {
                            context_data_val = (context_data_val << 1);
                            if (context_data_position == bitsPerChar - 1) {
                                context_data_position = 0;
                                context_data.push(getCharFromInt(context_data_val));
                                context_data_val = 0;
                            }
                            else {
                                context_data_position++;
                            }
                        }
                        value = context_w.charCodeAt(0);
                        for (i = 0; i < 8; i++) {
                            context_data_val = (context_data_val << 1) | (value & 1);
                            if (context_data_position == bitsPerChar - 1) {
                                context_data_position = 0;
                                context_data.push(getCharFromInt(context_data_val));
                                context_data_val = 0;
                            }
                            else {
                                context_data_position++;
                            }
                            value = value >> 1;
                        }
                    }
                    else {
                        value = 1;
                        for (i = 0; i < context_numBits; i++) {
                            context_data_val = (context_data_val << 1) | value;
                            if (context_data_position == bitsPerChar - 1) {
                                context_data_position = 0;
                                context_data.push(getCharFromInt(context_data_val));
                                context_data_val = 0;
                            }
                            else {
                                context_data_position++;
                            }
                            value = 0;
                        }
                        value = context_w.charCodeAt(0);
                        for (i = 0; i < 16; i++) {
                            context_data_val = (context_data_val << 1) | (value & 1);
                            if (context_data_position == bitsPerChar - 1) {
                                context_data_position = 0;
                                context_data.push(getCharFromInt(context_data_val));
                                context_data_val = 0;
                            }
                            else {
                                context_data_position++;
                            }
                            value = value >> 1;
                        }
                    }
                    context_enlargeIn--;
                    if (context_enlargeIn == 0) {
                        context_enlargeIn = Math.pow(2, context_numBits);
                        context_numBits++;
                    }
                    delete context_dictionaryToCreate[context_w];
                }
                else {
                    value = context_dictionary[context_w];
                    for (i = 0; i < context_numBits; i++) {
                        context_data_val = (context_data_val << 1) | (value & 1);
                        if (context_data_position == bitsPerChar - 1) {
                            context_data_position = 0;
                            context_data.push(getCharFromInt(context_data_val));
                            context_data_val = 0;
                        }
                        else {
                            context_data_position++;
                        }
                        value = value >> 1;
                    }
                }
                context_enlargeIn--;
                if (context_enlargeIn == 0) {
                    context_enlargeIn = Math.pow(2, context_numBits);
                    context_numBits++;
                }
                context_dictionary[context_wc] = context_dictSize++;
                context_w = String(context_c);
            }
        }
        if (context_w !== "") {
            if (Object.prototype.hasOwnProperty.call(context_dictionaryToCreate, context_w)) {
                if (context_w.charCodeAt(0) < 256) {
                    for (i = 0; i < context_numBits; i++) {
                        context_data_val = (context_data_val << 1);
                        if (context_data_position == bitsPerChar - 1) {
                            context_data_position = 0;
                            context_data.push(getCharFromInt(context_data_val));
                            context_data_val = 0;
                        }
                        else {
                            context_data_position++;
                        }
                    }
                    value = context_w.charCodeAt(0);
                    for (i = 0; i < 8; i++) {
                        context_data_val = (context_data_val << 1) | (value & 1);
                        if (context_data_position == bitsPerChar - 1) {
                            context_data_position = 0;
                            context_data.push(getCharFromInt(context_data_val));
                            context_data_val = 0;
                        }
                        else {
                            context_data_position++;
                        }
                        value = value >> 1;
                    }
                }
                else {
                    value = 1;
                    for (i = 0; i < context_numBits; i++) {
                        context_data_val = (context_data_val << 1) | value;
                        if (context_data_position == bitsPerChar - 1) {
                            context_data_position = 0;
                            context_data.push(getCharFromInt(context_data_val));
                            context_data_val = 0;
                        }
                        else {
                            context_data_position++;
                        }
                        value = 0;
                    }
                    value = context_w.charCodeAt(0);
                    for (i = 0; i < 16; i++) {
                        context_data_val = (context_data_val << 1) | (value & 1);
                        if (context_data_position == bitsPerChar - 1) {
                            context_data_position = 0;
                            context_data.push(getCharFromInt(context_data_val));
                            context_data_val = 0;
                        }
                        else {
                            context_data_position++;
                        }
                        value = value >> 1;
                    }
                }
                context_enlargeIn--;
                if (context_enlargeIn == 0) {
                    context_enlargeIn = Math.pow(2, context_numBits);
                    context_numBits++;
                }
                delete context_dictionaryToCreate[context_w];
            }
            else {
                value = context_dictionary[context_w];
                for (i = 0; i < context_numBits; i++) {
                    context_data_val = (context_data_val << 1) | (value & 1);
                    if (context_data_position == bitsPerChar - 1) {
                        context_data_position = 0;
                        context_data.push(getCharFromInt(context_data_val));
                        context_data_val = 0;
                    }
                    else {
                        context_data_position++;
                    }
                    value = value >> 1;
                }
            }
            context_enlargeIn--;
            if (context_enlargeIn == 0) {
                context_enlargeIn = Math.pow(2, context_numBits);
                context_numBits++;
            }
        }
        value = 2;
        for (i = 0; i < context_numBits; i++) {
            context_data_val = (context_data_val << 1) | (value & 1);
            if (context_data_position == bitsPerChar - 1) {
                context_data_position = 0;
                context_data.push(getCharFromInt(context_data_val));
                context_data_val = 0;
            }
            else {
                context_data_position++;
            }
            value = value >> 1;
        }
        while (true) {
            context_data_val = (context_data_val << 1);
            if (context_data_position == bitsPerChar - 1) {
                context_data.push(getCharFromInt(context_data_val));
                break;
            }
            else
                context_data_position++;
        }
        return context_data.join('');
    }
    defineProperty(String.prototype, "compress", function (encode) {
        if (typeof encode === "string") {
            switch (encode.toLowerCase()) {
                case "base64":
                    {
                        var compressed = _compress(this, 6, function (a) { return keyStrBase64.charAt(a); });
                        return compressed + "=".repeat(compressed.length % 4);
                    }
                case "uri":
                    return _compress(this, 6, function (a) { return keyStrUriSafe.charAt(a); });
                case "utf-16":
                    return _compress(this, 15, function (a) { return fromCharCode(a + 32); }) + " ";
                case "uint":
                    {
                        var compressed = _compress(this, 16, function (a) { return fromCharCode(a); }), buf = new Uint8Array(compressed.length * 2);
                        for (var i = 0, TotalLen = compressed.length; i < TotalLen; i++) {
                            var current_value = compressed.charCodeAt(i);
                            buf[i * 2] = current_value >>> 8;
                            buf[i * 2 + 1] = current_value % 256;
                        }
                        return buf;
                    }
                default:
                    console.error("Error: Encode type must be one of: base64, uri, utf-16, uint");
            }
        }
        return _compress(this, 16, function (a) { return fromCharCode(a); });
    });
})();
(function () {
    var fromCharCode = String.fromCharCode, keyStrBase64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", keyStrUriSafe = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+-$", baseReverseDic = {};
    function getBaseValue(alphabet, character) {
        if (!baseReverseDic[alphabet]) {
            baseReverseDic[alphabet] = {};
            for (var i = 0; i < alphabet.length; i++) {
                baseReverseDic[alphabet][alphabet.charAt(i)] = i;
            }
        }
        return baseReverseDic[alphabet][character];
    }
    function _decompress(length, resetValue, getNextValue) {
        var dictionary = [], next, enlargeIn = 4, dictSize = 4, numBits = 3, entry = "", result = [], i, w, bits, resb, maxpower, power, c, data = { val: getNextValue(0), position: resetValue, index: 1 };
        for (i = 0; i < 3; i += 1) {
            dictionary[i] = i;
        }
        bits = 0;
        maxpower = Math.pow(2, 2);
        power = 1;
        while (power != maxpower) {
            resb = data.val & data.position;
            data.position >>= 1;
            if (data.position == 0) {
                data.position = resetValue;
                data.val = getNextValue(data.index++);
            }
            bits |= (resb > 0 ? 1 : 0) * power;
            power <<= 1;
        }
        switch (next = bits) {
            case 0:
                bits = 0;
                maxpower = Math.pow(2, 8);
                power = 1;
                while (power != maxpower) {
                    resb = data.val & data.position;
                    data.position >>= 1;
                    if (data.position == 0) {
                        data.position = resetValue;
                        data.val = getNextValue(data.index++);
                    }
                    bits |= (resb > 0 ? 1 : 0) * power;
                    power <<= 1;
                }
                c = fromCharCode(bits);
                break;
            case 1:
                bits = 0;
                maxpower = Math.pow(2, 16);
                power = 1;
                while (power != maxpower) {
                    resb = data.val & data.position;
                    data.position >>= 1;
                    if (data.position == 0) {
                        data.position = resetValue;
                        data.val = getNextValue(data.index++);
                    }
                    bits |= (resb > 0 ? 1 : 0) * power;
                    power <<= 1;
                }
                c = fromCharCode(bits);
                break;
            case 2:
                return "";
        }
        dictionary[3] = c;
        w = c;
        result.push(c);
        while (true) {
            if (data.index > length) {
                return "";
            }
            bits = 0;
            maxpower = Math.pow(2, numBits);
            power = 1;
            while (power != maxpower) {
                resb = data.val & data.position;
                data.position >>= 1;
                if (data.position == 0) {
                    data.position = resetValue;
                    data.val = getNextValue(data.index++);
                }
                bits |= (resb > 0 ? 1 : 0) * power;
                power <<= 1;
            }
            switch (c = bits) {
                case 0:
                    bits = 0;
                    maxpower = Math.pow(2, 8);
                    power = 1;
                    while (power != maxpower) {
                        resb = data.val & data.position;
                        data.position >>= 1;
                        if (data.position == 0) {
                            data.position = resetValue;
                            data.val = getNextValue(data.index++);
                        }
                        bits |= (resb > 0 ? 1 : 0) * power;
                        power <<= 1;
                    }
                    dictionary[dictSize++] = fromCharCode(bits);
                    c = dictSize - 1;
                    enlargeIn--;
                    break;
                case 1:
                    bits = 0;
                    maxpower = Math.pow(2, 16);
                    power = 1;
                    while (power != maxpower) {
                        resb = data.val & data.position;
                        data.position >>= 1;
                        if (data.position == 0) {
                            data.position = resetValue;
                            data.val = getNextValue(data.index++);
                        }
                        bits |= (resb > 0 ? 1 : 0) * power;
                        power <<= 1;
                    }
                    dictionary[dictSize++] = fromCharCode(bits);
                    c = dictSize - 1;
                    enlargeIn--;
                    break;
                case 2:
                    return result.join('');
            }
            if (enlargeIn == 0) {
                enlargeIn = Math.pow(2, numBits);
                numBits++;
            }
            if (dictionary[c]) {
                entry = dictionary[c];
            }
            else {
                if (c === dictSize) {
                    entry = w + w.charAt(0);
                }
                else {
                    return null;
                }
            }
            result.push(entry);
            dictionary[dictSize++] = w + entry.charAt(0);
            enlargeIn--;
            w = entry;
            if (enlargeIn == 0) {
                enlargeIn = Math.pow(2, numBits);
                numBits++;
            }
        }
    }
    defineProperty(String.prototype, "decompress", function (encode) {
        var compressed = this;
        if (typeof encode === "string") {
            switch (encode.toLowerCase()) {
                case "base64":
                    return _decompress(compressed.length, 32, function (index) { return getBaseValue(keyStrBase64, compressed.charAt(index)); });
                case "uri":
                    compressed = compressed.replace(/ /g, "+");
                    return _decompress(compressed.length, 32, function (index) { return getBaseValue(keyStrUriSafe, compressed.charAt(index)); });
                case "utf-16":
                    return _decompress(compressed.length, 16384, function (index) { return compressed.charCodeAt(index) - 32; });
                case "uint":
                    break;
                default:
                    console.error("Error: Encode type must be one of: base64, uri, utf-16, uint");
            }
        }
        return _decompress(compressed.length, 32768, function (index) { return compressed.charCodeAt(index); });
    });
    defineProperty(Uint8Array.prototype, "decompress", function (encode) {
        var i = 0, length = this.length / 2, result = [];
        for (; i < length; i++) {
            result.push(fromCharCode(this[i * 2] * 256 + this[i * 2 + 1]));
        }
        return result.join("").decompress();
    });
})();
defineProperty(String.prototype, "explode", function () {
    var arr = this.split("."), length = arr.length;
    for (var i = 0; i < length; i++) {
        if (i < length - 1 && arr[i].substr(-1) === "\\") {
            arr[i] = arr[i].substr(0, arr[i].length - 1) + "." + arr.splice(i + 1, 1);
            i--;
        }
        else {
            var num = parseInt(arr[i], 10);
            if (String(num) === arr[i]) {
                arr[i] = num;
            }
        }
    }
    return arr;
});
defineProperty(String.prototype, "includes", function (searchString, position) {
    position = position || 0;
    if (position + searchString.length > this.length) {
        return false;
    }
    return this.indexOf(searchString, position) !== -1;
});
defineProperty(String.prototype, "regex", function (regexp) {
    var rx, num, length, matches = this.match(regexp);
    if (matches) {
        if (regexp.global) {
            if (/(^|[^\\]|[^\\](\\\\)*)\([^?]/.test(regexp.source)) {
                rx = new RegExp(regexp.source, (regexp.ignoreCase ? "i" : "") + (regexp.multiline ? "m" : ""));
            }
        }
        else {
            matches.shift();
        }
        length = matches.length;
        while (length--) {
            if (matches[length]) {
                if (rx) {
                    matches[length] = matches[length].regex(rx);
                }
                else {
                    num = parseFloat(matches[length]);
                    if (!isNaN(num) && String(num) === matches[length]) {
                        matches[length] = num;
                    }
                }
            }
        }
        if (!rx && matches.length === 1) {
            return matches[0];
        }
    }
    return matches;
});
defineProperty(String.prototype, "startsWith", function (searchString, position) {
    return this.substr(position || 0, searchString.length) === searchString;
});
defineProperty(String.prototype, "ucfirst", function () {
    return this.charAt(0).toLocaleUpperCase() + this.slice(1).toLowerCase();
});
(function () {
    try {
        var console_1 = window.console || {}, bind_1 = Function.prototype.bind;
        ["log", "warn", "error", "debug", "info", "assert", "trace"].forEach(function (method) {
            console_1[method] = console_1[method] || function () { };
            if (bind_1 && isObject(console_1[method])) {
                console_1[method] = bind_1.call(console_1[method], console_1);
            }
        });
    }
    catch (e) {
    }
}());
defineProperty(Array.prototype, "clean", function () {
    for (var i = 0, arr = []; i < this.length; i++) {
        if (this[i] || this[i] === 0 || this[i] === false) {
            arr.push(this[i]);
        }
    }
    return arr;
});
defineProperty(Array.prototype, "grow", function (length) {
    var add = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        add[_i - 1] = arguments[_i];
    }
    var i = 0, len = Math.abs(length), args = [].slice.call(arguments, 1);
    if (this.length < len) {
        args = args.length ? args : [0];
        while (this.length < len) {
            if (i >= args.length) {
                i = 0;
            }
            this.push(args[i++]);
        }
    }
    else if (length > 0 && this.length > length) {
        this.splice(length, this.length - length);
    }
    return this;
});
defineProperty(Array.prototype, "indexOfCase", function (searchElement, fromIndex) {
    var i, length = this.length >>> 0;
    fromIndex = fromIndex || 0;
    searchElement = searchElement.toUpperCase();
    for (i = Math.max(fromIndex >= 0 ? fromIndex : length - Math.abs(fromIndex), 0); i < length; i++) {
        if (i in this && String(this[i]).toUpperCase() === searchElement) {
            return i;
        }
    }
    return -1;
});
defineProperty(Array.prototype, "naturalSort", function () {
    return this.sort(function (str1, str2) {
        return String(str1).naturalCompare(String(str2));
    });
});
defineProperty(Array.prototype, "naturalCaseSort", function () {
    return this.sort(function (str1, str2) {
        return String(str1).naturalCaseCompare(String(str2));
    });
});
defineProperty(Array.prototype, "removeCase", function (searchElement) {
    var i = this.indexOfCase(searchElement);
    return i >= 0 ? this.splice(i, 1) : [];
});
defineProperty(Array.prototype, "toObject", function () {
    for (var i = 0, obj = {}; i < this.length; i++) {
        obj[this[i]] = true;
    }
    return obj;
});
defineProperty(Array.prototype, "unique", function () {
    var i = this.length;
    while (i-- > 0) {
        if (i > this.indexOf(this[i])) {
            this.splice(i, 1);
        }
    }
    return this;
});
defineProperty(Array.prototype, "unshiftOnce", function () {
    for (var that = this, i = 0, elements = arguments; i < elements.length; i++) {
        if (!that.includes(elements[i])) {
            that.unshift(elements[i]);
        }
    }
    return that.length;
});
if (!("offsetParent" in document.body)) {
    Object.defineProperty(HTMLElement.prototype, "offsetParent", {
        "get": function () {
            var element = this, parent = element.parentElement, html = document.documentElement, found = null, style = window.getComputedStyle(element);
            if (element && style.position !== "fixed") {
                for (; parent; parent = parent.parentElement) {
                    style = window.getComputedStyle(parent);
                    if (style.display === "none") {
                        found = null;
                        break;
                    }
                    else if (!found && (parent === html || style.position !== "initial")) {
                        found = parent;
                    }
                }
            }
            return found;
        }
    });
}
defineProperty(JSON, "decode", function (str, reviver) {
    var obj = JSON.parse(str, reviver), keys = obj["$"], count = {}, decode = function (obj) {
        var i, to;
        if (isObject(obj)) {
            to = {};
            for (i in obj) {
                if (keys[i]) {
                    to[keys[i].valueOf()] = arguments.callee(obj[i]);
                    count[i] = (count[i] || 0) + 1;
                }
                else {
                    to[i] = arguments.callee(obj[i]);
                }
            }
        }
        else if (isArray(obj)) {
            to = [];
            for (i = 0; i < obj.length; i++) {
                to[i] = arguments.callee(obj[i]);
            }
        }
        else {
            to = obj;
        }
        return to;
    };
    if (keys) {
        delete obj["$"];
        return decode(obj);
    }
    return obj;
});
defineProperty(JSON, "encode", function (obj, replacer, space) {
    var key, keys = {}, count = {}, next = 0, nextKey, first = true, getKey = function () {
        var key, digits = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ", length = digits.length;
        do {
            key = nextKey;
            nextKey = (next >= length ? digits[(Math.floor(next / length) - 1) % length] : "") + digits[next % length];
            next++;
        } while (count[nextKey]);
        first = false;
        return key;
    }, check = function (obj) {
        if (isObject(obj)) {
            for (var key in obj) {
                if (/[^0-9]/.test(key)) {
                    count[key] = (count[key] || 0) + 1;
                }
                check(obj[key]);
            }
        }
        else if (isArray(obj)) {
            for (var i = 0; i < obj.length; i++) {
                check(obj[i]);
            }
        }
    }, encode = function (obj) {
        var length, to;
        if (isObject(obj)) {
            to = {};
            for (var key in obj) {
                length = key.length;
                if ((count[key] * length) > (((count[key] + 1) * nextKey.length) + length + (first ? 12 : 6))) {
                    if (!keys[key]) {
                        keys[key] = getKey();
                    }
                    to[keys[key]] = encode(obj[key]);
                }
                else {
                    to[key] = encode(obj[key]);
                }
            }
        }
        else if (isArray(obj)) {
            to = [];
            for (var i = 0; i < obj.length; i++) {
                to[i] = encode(obj[i]);
            }
        }
        else {
            to = obj;
        }
        return to;
    };
    if (isObject(obj) || isArray(obj)) {
        if (obj["$"]) {
            console.warn("Error: Trying to encode an object that already contains a '$' key!!!");
        }
        check(obj);
        console.log("keys", count);
        getKey();
        first = true;
        obj = encode(obj);
        if (Object.keys(keys).length) {
            obj["$"] = {};
            for (key in keys) {
                obj["$"][keys[key]] = key;
            }
        }
    }
    return JSON.stringify(obj, replacer, space);
});
defineProperty(JSON, "shallow", function (obj, depth, replacer, space) {
    var walk = function (o, d) {
        var i, out;
        if (o && isObject(typeof o)) {
            if (isArray(o)) {
                if (d > 0) {
                    out = [];
                    for (i = 0; i < o.length; i++) {
                        out[i] = walk(o[i], d - 1);
                    }
                }
                else {
                    out = "[object Array]";
                }
            }
            else if (d > 0) {
                out = {};
                for (i in o) {
                    if (o.hasOwnProperty(i)) {
                        out[i] = walk(o[i], d - 1);
                    }
                }
            }
            else {
                out = "[object Object]";
            }
        }
        else {
            out = o;
        }
        return out;
    };
    return JSON.stringify(walk(obj, depth || 1), replacer, space);
});
defineProperty(Number.prototype, "addCommas", function (digits) {
    var num = String(isNumber(digits) ? this.toFixed(digits) : this), rx = /^(.*\s)?(\d+)(\d{3}\b)/;
    while (rx.test(num)) {
        num = num.replace(rx, "$1$2,$3");
    }
    return num;
});
defineProperty(Object.prototype, "bestMatch", function (percent) {
    var i, num, best = -1;
    for (i in this) {
        num = parseInt(i);
        if (this.hasOwnProperty(i) && num > best && num <= percent) {
            best = num;
        }
    }
    return best > -1 ? this[best] : undefined;
});
defineProperty(Object.prototype, "diff", function diff(original) {
    for (var i in this) {
        if (this.hasOwnProperty(i) && original.hasOwnProperty(i)) {
            if ((isArray(this[i]) && isArray(original[i])) || (isObject(this[i]) && isObject(original[i]))) {
                this[i] = diff.call(this[i], original[i], true);
                if (!Object.keys(this[i])) {
                    delete this[i];
                }
            }
            else if (this[i] === original[i]) {
                delete this[i];
            }
        }
    }
    return this;
});
defineProperty(Object.prototype, "equals", function (target, callback) {
    var key;
    if (!target) {
        return false;
    }
    for (key in this) {
        if (this.hasOwnProperty(key) && (!callback || callback.call(this, key, target) !== false)) {
            if (!target.hasOwnProperty(key) || typeof this[key] !== typeof target[key] || !this[key] !== !target[key]) {
                return false;
            }
            switch (typeof (this[key])) {
                case "object":
                    if (this[key] !== null && target[key] !== null && (this[key].constructor.toString() !== target[key].constructor.toString() || !this[key].equals(target[key]))) {
                        return false;
                    }
                    break;
                case "function":
                    if (this[key].toString() !== target[key].toString()) {
                        return false;
                    }
                    break;
                default:
                    if (this[key] !== target[key]) {
                        return false;
                    }
            }
        }
    }
    for (key in target) {
        if (!this.hasOwnProperty(key) && (!callback || callback.call(this, key, target) !== false)) {
            return false;
        }
    }
    return true;
});
defineProperty(Object.prototype, "hasTree", function (path) {
    var data = this, fixedPath = path.explode();
    while (fixedPath.length && data !== undefined) {
        data = data[fixedPath.shift()];
    }
    return !(data === undefined || data === null);
});
defineProperty(Object.prototype, "indexOfX", function (value) {
    for (var key in this) {
        if (this.hasOwnProperty(key) && this[key] === value) {
            return key;
        }
    }
    return null;
});
defineProperty(String.prototype, "crc32", function () {
    var i, j, k, length = this.length, crc32_table = String.prototype.crc32.table, crc = -1;
    if (!crc32_table) {
        crc32_table = String.prototype.crc32.table = new Uint32Array(256);
        for (i = 0; i < 256; i++) {
            for (j = i, k = 0; k < 8; k++) {
                j = ((j & 1) ? (0xEDB88320 ^ (j >>> 1)) : (j >>> 1));
            }
            crc32_table[i] = j;
        }
    }
    for (i = 0; i < length; i++) {
        crc = (crc >>> 8) ^ crc32_table[crc & 0xFF ^ (this.charCodeAt(i) & 0xFF)];
    }
    return ((crc ^ -1) >>> 0).toString(16);
});
defineProperty(String.prototype, "md5", function () {
    function md5cycle(x, k) {
        var a = x[0], b = x[1], c = x[2], d = x[3];
        a = ff(a, b, c, d, k[0], 7, -680876936);
        d = ff(d, a, b, c, k[1], 12, -389564586);
        c = ff(c, d, a, b, k[2], 17, 606105819);
        b = ff(b, c, d, a, k[3], 22, -1044525330);
        a = ff(a, b, c, d, k[4], 7, -176418897);
        d = ff(d, a, b, c, k[5], 12, 1200080426);
        c = ff(c, d, a, b, k[6], 17, -1473231341);
        b = ff(b, c, d, a, k[7], 22, -45705983);
        a = ff(a, b, c, d, k[8], 7, 1770035416);
        d = ff(d, a, b, c, k[9], 12, -1958414417);
        c = ff(c, d, a, b, k[10], 17, -42063);
        b = ff(b, c, d, a, k[11], 22, -1990404162);
        a = ff(a, b, c, d, k[12], 7, 1804603682);
        d = ff(d, a, b, c, k[13], 12, -40341101);
        c = ff(c, d, a, b, k[14], 17, -1502002290);
        b = ff(b, c, d, a, k[15], 22, 1236535329);
        a = gg(a, b, c, d, k[1], 5, -165796510);
        d = gg(d, a, b, c, k[6], 9, -1069501632);
        c = gg(c, d, a, b, k[11], 14, 643717713);
        b = gg(b, c, d, a, k[0], 20, -373897302);
        a = gg(a, b, c, d, k[5], 5, -701558691);
        d = gg(d, a, b, c, k[10], 9, 38016083);
        c = gg(c, d, a, b, k[15], 14, -660478335);
        b = gg(b, c, d, a, k[4], 20, -405537848);
        a = gg(a, b, c, d, k[9], 5, 568446438);
        d = gg(d, a, b, c, k[14], 9, -1019803690);
        c = gg(c, d, a, b, k[3], 14, -187363961);
        b = gg(b, c, d, a, k[8], 20, 1163531501);
        a = gg(a, b, c, d, k[13], 5, -1444681467);
        d = gg(d, a, b, c, k[2], 9, -51403784);
        c = gg(c, d, a, b, k[7], 14, 1735328473);
        b = gg(b, c, d, a, k[12], 20, -1926607734);
        a = hh(a, b, c, d, k[5], 4, -378558);
        d = hh(d, a, b, c, k[8], 11, -2022574463);
        c = hh(c, d, a, b, k[11], 16, 1839030562);
        b = hh(b, c, d, a, k[14], 23, -35309556);
        a = hh(a, b, c, d, k[1], 4, -1530992060);
        d = hh(d, a, b, c, k[4], 11, 1272893353);
        c = hh(c, d, a, b, k[7], 16, -155497632);
        b = hh(b, c, d, a, k[10], 23, -1094730640);
        a = hh(a, b, c, d, k[13], 4, 681279174);
        d = hh(d, a, b, c, k[0], 11, -358537222);
        c = hh(c, d, a, b, k[3], 16, -722521979);
        b = hh(b, c, d, a, k[6], 23, 76029189);
        a = hh(a, b, c, d, k[9], 4, -640364487);
        d = hh(d, a, b, c, k[12], 11, -421815835);
        c = hh(c, d, a, b, k[15], 16, 530742520);
        b = hh(b, c, d, a, k[2], 23, -995338651);
        a = ii(a, b, c, d, k[0], 6, -198630844);
        d = ii(d, a, b, c, k[7], 10, 1126891415);
        c = ii(c, d, a, b, k[14], 15, -1416354905);
        b = ii(b, c, d, a, k[5], 21, -57434055);
        a = ii(a, b, c, d, k[12], 6, 1700485571);
        d = ii(d, a, b, c, k[3], 10, -1894986606);
        c = ii(c, d, a, b, k[10], 15, -1051523);
        b = ii(b, c, d, a, k[1], 21, -2054922799);
        a = ii(a, b, c, d, k[8], 6, 1873313359);
        d = ii(d, a, b, c, k[15], 10, -30611744);
        c = ii(c, d, a, b, k[6], 15, -1560198380);
        b = ii(b, c, d, a, k[13], 21, 1309151649);
        a = ii(a, b, c, d, k[4], 6, -145523070);
        d = ii(d, a, b, c, k[11], 10, -1120210379);
        c = ii(c, d, a, b, k[2], 15, 718787259);
        b = ii(b, c, d, a, k[9], 21, -343485551);
        x[0] = add32(a, x[0]);
        x[1] = add32(b, x[1]);
        x[2] = add32(c, x[2]);
        x[3] = add32(d, x[3]);
    }
    function cmn(q, a, b, x, s, t) {
        a = add32(add32(a, q), add32(x, t));
        return add32((a << s) | (a >>> (32 - s)), b);
    }
    function ff(a, b, c, d, x, s, t) {
        return cmn((b & c) | ((~b) & d), a, b, x, s, t);
    }
    function gg(a, b, c, d, x, s, t) {
        return cmn((b & d) | (c & (~d)), a, b, x, s, t);
    }
    function hh(a, b, c, d, x, s, t) {
        return cmn(b ^ c ^ d, a, b, x, s, t);
    }
    function ii(a, b, c, d, x, s, t) {
        return cmn(c ^ (b | (~d)), a, b, x, s, t);
    }
    function md51(s) {
        var n = s.length, state = [1732584193, -271733879, -1732584194, 271733878], i;
        for (i = 64; i <= s.length; i += 64) {
            md5cycle(state, md5blk(s.substring(i - 64, i)));
        }
        s = s.substring(i - 64);
        var tail = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        for (i = 0; i < s.length; i++)
            tail[i >> 2] |= s.charCodeAt(i) << ((i % 4) << 3);
        tail[i >> 2] |= 0x80 << ((i % 4) << 3);
        if (i > 55) {
            md5cycle(state, tail);
            for (i = 0; i < 16; i++)
                tail[i] = 0;
        }
        tail[14] = n * 8;
        md5cycle(state, tail);
        return state;
    }
    function md5blk(s) {
        var md5blks = [], i;
        for (i = 0; i < 64; i += 4) {
            md5blks[i >> 2] = s.charCodeAt(i)
                + (s.charCodeAt(i + 1) << 8)
                + (s.charCodeAt(i + 2) << 16)
                + (s.charCodeAt(i + 3) << 24);
        }
        return md5blks;
    }
    var hex_chr = '0123456789abcdef'.split('');
    function rhex(n) {
        var s = '', j = 0;
        for (; j < 4; j++)
            s += hex_chr[(n >> (j * 8 + 4)) & 0x0F]
                + hex_chr[(n >> (j * 8)) & 0x0F];
        return s;
    }
    function hex(x) {
        for (var i = 0; i < x.length; i++) {
            x[i] = rhex(x[i]);
        }
        return x.join('');
    }
    function add32(a, b) {
        return (a + b) & 0xFFFFFFFF;
    }
    return hex(md51(this));
});
defineProperty(String.prototype, "naturalCompare", function (compareString) {
    var str1 = this, str2 = String(compareString), a_length = str1.length, b_length = str2.length, isDigitChar = function (a) {
        var charCode = a.charCodeAt(0);
        return (charCode >= 48 && charCode <= 57);
    };
    for (var ia = 0, ib = 0; ia < a_length && ib < b_length; ia++, ib++) {
        var achar = str1.charAt(ia), bchar = str2.charAt(ib);
        if (isDigitChar(achar) && isDigitChar(bchar)) {
            for (var anum = 0, alen = 0; isDigitChar(achar); achar = str1.charAt(++ia)) {
                alen++;
                anum = (anum * 10) + parseInt(achar, 10);
            }
            for (var bnum = 0, blen = 0; isDigitChar(bchar); bchar = str2.charAt(++ib)) {
                blen++;
                bnum = (bnum * 10) + parseInt(bchar, 10);
            }
            ia--;
            ib--;
            if (anum === bnum) {
                continue;
            }
            achar = anum;
            bchar = bnum;
        }
        if (achar < bchar) {
            return -1;
        }
        if (achar > bchar) {
            return 1;
        }
    }
    return a_length === b_length ? 0 : a_length > b_length ? 1 : -1;
});
defineProperty(String.prototype, "naturalCaseCompare", function (compareString) {
    return this.toLowerCase().naturalCompare(String(compareString).toLowerCase());
});
defineProperty(String.prototype, "regexIndexOf", function (regexp, fromIndex) {
    var index = this.substring(fromIndex || 0).search(regexp);
    return index >= 0 ? index + (fromIndex || 0) : index;
});
defineProperty(String.prototype, "regexLastIndexOf", function (regexp, fromIndex) {
    regexp = regexp.global ? regexp : new RegExp(regexp.source, "g" + (regexp.ignoreCase ? "i" : "") + (regexp.multiline ? "m" : ""));
    if (fromIndex === undefined) {
        fromIndex = this.length;
    }
    else if (fromIndex < 0) {
        fromIndex = 0;
    }
    var result, stringToWorkWith = this.substring(0, fromIndex + 1), index = -1;
    while ((result = regexp.exec(stringToWorkWith)) != null) {
        index = result.index;
        regexp.lastIndex = index + 1;
    }
    return index;
});
defineProperty(String.prototype, "reverse", function () {
    return this.split("").reverse().join("");
});
defineProperty(String.prototype, "text", function () {
    return this.replace(/<\/?[a-z][^>]*>/gi, "");
});
defineProperty(String.prototype, "unicode", function (html) {
    return this.replace(/[\u0080-\uffff]/g, html
        ? function (char) {
            return "&#" + char.charCodeAt(0).toString() + ";";
        }
        : function (char) {
            return "\\u" + ("0000" + char.charCodeAt(0).toString(16)).slice(-4);
        });
});
defineProperty(window, "atob", function (input) {
    var output = "", i = 0, _keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
    while (i < input.length) {
        var enc1 = _keyStr.indexOf(input.charAt(i++)), enc2 = _keyStr.indexOf(input.charAt(i++)), enc3 = _keyStr.indexOf(input.charAt(i++)), enc4 = _keyStr.indexOf(input.charAt(i++)), chr1 = (enc1 << 2) | (enc2 >> 4), chr2 = ((enc2 & 15) << 4) | (enc3 >> 2), chr3 = ((enc3 & 3) << 6) | enc4;
        output = output + String.fromCharCode(chr1);
        if (enc3 !== 64) {
            output = output + String.fromCharCode(chr2);
        }
        if (enc4 !== 64) {
            output = output + String.fromCharCode(chr3);
        }
    }
    return output;
});
defineProperty(window, "btoa", function (str) {
    if (arguments.length !== 1) {
        throw new SyntaxError("Not enough arguments");
    }
    str = "" + str;
    var i, b10, padchar = "=", alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", getbyte = function (s, i) {
        var x = s.charCodeAt(i);
        if (x > 255) {
            throw new DOMException(DOMException.INVALID_CHARACTER_ERR);
        }
        return x;
    }, x = [], imax = str.length - str.length % 3;
    if (str.length === 0) {
        return str;
    }
    for (i = 0; i < imax; i += 3) {
        b10 = (getbyte(str, i) << 16) | (getbyte(str, i + 1) << 8) | getbyte(str, i + 2);
        x.push(alpha.charAt(b10 >> 18), alpha.charAt((b10 >> 12) & 0x3F), alpha.charAt((b10 >> 6) & 0x3f), alpha.charAt(b10 & 0x3F));
    }
    switch (str.length - imax) {
        case 1:
            b10 = getbyte(str, i) << 16;
            x.push(alpha.charAt(b10 >> 18) + alpha.charAt((b10 >> 12) & 0x3F) + padchar + padchar);
            break;
        case 2:
            b10 = (getbyte(str, i) << 16) | (getbyte(str, i + 1) << 8);
            x.push(alpha.charAt(b10 >> 18) + alpha.charAt((b10 >> 12) & 0x3F) + alpha.charAt((b10 >> 6) & 0x3F) + padchar);
            break;
    }
    return x.join("");
});
defineProperty(window, "ResizeObserver", (function () {
    var ResizeObserver = (function () {
        function ResizeObserver(callback) {
            ResizeObserver.resizeObservers.push(this);
            this.__callback = callback;
            this.__observationTargets = [];
            this.__activeTargets = [];
        }
        ResizeObserver.prototype.observe = function (target) {
            var resizeObservationIndex = this.findTargetIndex(target);
            console.log("ResizeObserver.observe", target);
            if (!target || resizeObservationIndex >= 0) {
                return;
            }
            this.__observationTargets.push(new ResizeObservation(target));
        };
        ;
        ResizeObserver.prototype.unobserve = function (target) {
            var resizeObservationIndex = this.findTargetIndex(target);
            if (resizeObservationIndex === -1) {
                return;
            }
            this.__observationTargets.splice(resizeObservationIndex, 1);
        };
        ;
        ResizeObserver.prototype.disconnect = function () {
            this.__observationTargets = [];
            this.__activeTargets = [];
        };
        ;
        ResizeObserver.prototype.findTargetIndex = function (target) {
            for (var index = 0, collection = this.__observationTargets; index < collection.length; index += 1) {
                if (collection[index].target === target) {
                    return index;
                }
            }
        };
        ResizeObserver.prototype.__populateActiveTargets = function () {
            this.__activeTargets = [];
            for (var key in this.__observationTargets) {
                var resizeObservation = this.__observationTargets[key];
                if (resizeObservation.isActive()) {
                    this.__activeTargets.push(resizeObservation);
                }
            }
        };
        ;
        ResizeObserver.broadcastActiveObservations = function () {
            for (var index = 0; index < ResizeObserver.resizeObservers.length; index += 1) {
                ResizeObserver.resizeObservers[index].__populateActiveTargets();
            }
            for (var roIndex = 0; roIndex < ResizeObserver.resizeObservers.length; roIndex++) {
                var resizeObserver = ResizeObserver.resizeObservers[roIndex];
                if (resizeObserver.__activeTargets.length === 0) {
                    continue;
                }
                for (var atIndex = 0, entries = []; atIndex < resizeObserver.__activeTargets.length; atIndex += 1) {
                    var resizeObservation = resizeObserver.__activeTargets[atIndex], entry = new ResizeObserverEntry(resizeObservation.target);
                    entries.push(entry);
                    resizeObservation.update();
                }
                resizeObserver.__callback(entries, resizeObserver);
                resizeObserver.__activeTargets = [];
            }
        };
        ResizeObserver.resizeObservers = [];
        return ResizeObserver;
    }());
    var ResizeObserverEntry = (function () {
        function ResizeObserverEntry(target) {
            this.target = target;
        }
        Object.defineProperty(ResizeObserverEntry.prototype, "contentRect", {
            get: function () {
                return this.target.getBoundingClientRect();
            },
            enumerable: true,
            configurable: true
        });
        return ResizeObserverEntry;
    }());
    var ResizeObservation = (function () {
        function ResizeObservation(target) {
            this.target = target;
            this.update();
        }
        ResizeObservation.prototype.update = function () {
            var box = this.target.getBoundingClientRect();
            this._broadcastWidth = box.width;
            this._broadcastHeight = box.height;
        };
        Object.defineProperty(ResizeObservation.prototype, "broadcastWidth", {
            get: function () {
                return this._broadcastWidth;
            },
            enumerable: true,
            configurable: true
        });
        ;
        Object.defineProperty(ResizeObservation.prototype, "broadcastHeight", {
            get: function () {
                return this._broadcastHeight;
            },
            enumerable: true,
            configurable: true
        });
        ;
        ResizeObservation.prototype.isActive = function () {
            var box = this.target.getBoundingClientRect();
            if (box.width !== this.broadcastWidth ||
                box.height !== this.broadcastHeight) {
                return true;
            }
            return false;
        };
        ;
        return ResizeObservation;
    }());
    function frameHandler() {
        ResizeObserver.broadcastActiveObservations();
        requestAnimationFrame(frameHandler);
    }
    requestAnimationFrame(frameHandler);
    return ResizeObserver;
})());
$.explode = function (path) {
    var i, arr = [];
    if (path && $.isArray(path)) {
        arr = path.slice(0);
    }
    if ($.isString(path)) {
        arr = path.split(".");
        for (i = 0; i < arr.length - 1; i++) {
            if (arr[i].substr(-1) === "\\") {
                arr[i] = arr[i].substr(0, arr[i].length - 1) + "." + arr.splice(i + 1, 1);
                i--;
            }
        }
    }
    for (i = 0; i < arr.length; i++) {
        if (!/[^\d]/.test(arr[i])) {
            arr[i] = "" + parseInt(arr[i], 10);
        }
    }
    return arr;
};
$.False = function () {
    return false;
};
$.isBoolean = function (bool) {
    return bool === true || bool === false;
};
$.isNumber = function (num) {
    return typeof num === "number";
};
$.isString = function (str) {
    return typeof str === "string";
};
$.log = function (text) {
    var console = window["console"];
    if (text && console && console["log"]) {
        console["log"](text);
    }
    return text;
};
var uniqueClassNumber = 0;
$.make = function (create, type, id, classes, properties, styles, children, content) {
    var i, j, tmp, tmp2, arg, $el, args = arguments;
    for (i = 0; i < args.length; i++) {
        arg = args[i];
        if (!i && ($.isBoolean(arg) || !arg)) {
            if (!arg) {
                break;
            }
        }
        else if (!$el) {
            $el = $(!isString(arg) || (/[ #<.]/.test(arg) ? arg : createElement(arg)));
        }
        else if (arg) {
            if (isObject(arg)) {
                $el.css(arg);
            }
            else if (isArray(arg)) {
                if (arg.length) {
                    $el[arg[0]].apply($el, arg.slice(1));
                }
            }
            else if (isString(arg)) {
                var stringArg = arg;
                if (stringArg[0] === "#") {
                    $el.attr("id", stringArg.replace("#", ""));
                }
                else if (stringArg[0] === "<") {
                    $el.append(stringArg);
                }
                else if (stringArg[0] === "=") {
                    $el.html(stringArg.substr(1));
                }
                else {
                    var colon = stringArg.indexOf(":"), equal = stringArg.indexOf("=");
                    if (colon >= 0 && (equal < 0 || equal > colon)) {
                        tmp = stringArg.regex(/\s*([a-z-]+):(\s*(?:url\([^\)]*\)|[^;]+)+)\s*;?/gi);
                        for (j = 0; j < tmp.length; j++) {
                            $el.css(tmp[j][0], tmp[j][1]);
                        }
                    }
                    else if (equal > 0) {
                        tmp = stringArg.regex(/(.*?)=(.*)/);
                        tmp2 = stringArg[0] === "?";
                        $el[tmp2 ? "data" : "attr"](tmp2 ? tmp[0].substr(1) : tmp[0], tmp[1] === undefined ? true : tmp[1]);
                    }
                    else if (stringArg.length) {
                        $el.addClass(stringArg);
                    }
                }
            }
            else if (arg.jquery || arg instanceof Node) {
                $el.append(arg);
            }
        }
    }
    $el = $el || $();
    if ($el.length) {
    }
    return $el;
};
$.makeObject = function () {
    var pairs = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        pairs[_i] = arguments[_i];
    }
    var i, obj = {}, args = [].slice.call(arguments);
    for (i = 0; i < args.length; i++) {
        obj[args[i++]] = args[i];
    }
    return obj;
};
$.prefix = function (prop) {
    if (prop.includes("-")) {
        prop = prop.replace(/-([a-z])/gi, function ($0, $1) {
            return $1.toUpperCase();
        });
    }
    if (!$.cssProps[prop]) {
        $.cssProps[prop] = Modernizr.prefixed(prop);
    }
    return $.cssProps[prop];
};
var rAF = window.requestAnimationFrame, rAFname = {}, rAFwrapper = false;
$.rAF = function (name, callback) {
    var fn;
    if ($.isString(name)) {
        fn = callback;
        if (callback) {
            rAFname[name] = callback;
            if (rAFwrapper) {
                fn = undefined;
            }
            else {
                rAFwrapper = true;
                fn = function (time) {
                    var callback;
                    rAFwrapper = false;
                    for (var name in rAFname) {
                        if (rAFname.hasOwnProperty(name)) {
                            callback = rAFname[name];
                            if (callback) {
                                rAFname[name] = undefined;
                                callback(time);
                            }
                        }
                    }
                };
            }
        }
        else {
            rAFname[name] = undefined;
        }
    }
    else {
        fn = name;
    }
    if (fn) {
        if (rAF && !document.hidden) {
            rAF(fn);
        }
        else {
            window.setTimeout(fn, 16);
        }
    }
};
(function () {
    var globalRules;
    var globalStylesheet;
    $.stylesheet = function (selectors, rules, context) {
        var i, tmp, selector, createRule = function ($0, $1, $2) {
            rules[$1] = $2;
        };
        if (!globalStylesheet) {
            globalStylesheet = $("<style style=\"text/css\"/>").appendTo("head");
            globalRules = {};
        }
        if (globalStylesheet) {
            if (!rules) {
                if (isString(selectors) && /\{.*?\}/.test(selectors)) {
                    selectors = selectors.regex(/\s*(.*?)\s*\{([^\}]*)\}/g);
                    for (i = 0; i < selectors.length; i++) {
                        $.stylesheet(selectors[i][0], selectors[i][1], context);
                    }
                }
                else if ($.isPlainObject(selectors)) {
                    for (i in selectors) {
                        if (selectors.hasOwnProperty(i)) {
                            $.stylesheet(i, selectors[i], context);
                        }
                    }
                }
            }
            else {
                if (isString(rules)) {
                    if (rules.includes("{")) {
                        rules = rules.replace(/(^[^{]*\{|\}.*)/g, "");
                    }
                    rules = rules.split(";");
                }
                if (isArray(rules)) {
                    tmp = rules;
                    rules = {};
                    for (i = 0; i < tmp.length; i++) {
                        if ($.isString(tmp[i])) {
                            tmp[i].replace(/^\s*([^\s:]+)\s*:\s*(.*?)\s*$/, createRule);
                        }
                    }
                }
                selectors = $.isArray(selectors) ? selectors : selectors.split(",");
                context = $.isString(context) ? context + " " : "";
                for (i = 0; i < selectors.length; i++) {
                    selectors[i] = context + selectors[i].trim();
                }
                for (i = 0; i < selectors.length; i++) {
                    selector = selectors[i];
                    globalRules[selector] = $.extend(globalRules[selector] || {}, rules);
                }
                ic.setImmediate("style", update);
            }
        }
        return this;
    };
    function update() {
        if (globalStylesheet) {
            var style, stylecc, select, rule, rules = globalRules, text = "";
            for (select in rules) {
                if (rules.hasOwnProperty(select)) {
                    text += select + "{";
                    rule = rules[select];
                    for (style in rule) {
                        if (rule.hasOwnProperty(style)) {
                            stylecc = $.camelCase(style);
                            text += (style !== "float" && $.cssProps.hasOwnProperty(stylecc) ? $.cssProps[stylecc].replace(/([A-Z])/g, "-$1").toLowerCase() : style) + ":" + rule[style] + ";";
                        }
                    }
                    text += "}";
                }
            }
            globalStylesheet.text(text);
        }
    }
    ;
    function empty() {
        if (globalStylesheet) {
            globalRules = {};
            globalStylesheet.text("");
        }
        return this;
    }
    ;
}());
$.True = function () {
    return true;
};
var TreeModule;
(function (TreeModule) {
    TreeModule.nodes = {};
    var copyId;
    var copyCut;
    function callback(fn) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        if (fn) {
            return fn.apply($.tree, args);
        }
    }
    function treeToggleChildren() {
        var $this = $(this), $li = $this.closest("li"), id = $li.data("id"), isOpen = !$li.toggleClass("aba-tree-children-open").hasClass("aba-tree-children-open");
        loadChildren(id);
        $li.children("ul")["slide" + (isOpen ? "Up" : "Down")]();
        return false;
    }
    function treeClickChild() {
        var $this = $(this), $li = $this.closest("li"), id = $li.data("id"), isParent = TreeModule.isParent(id);
        if (TreeModule.options.autoExpand && isParent && !$li.hasClass("aba-tree-children-open")) {
            treeToggleChildren.call(this);
        }
        if (callback(TreeModule.options.onBeforeClick, id) !== false) {
            callback(TreeModule.options.onClick, id);
            TreeModule.currentId = id;
            isParent = isParent;
            var $parents = $li.parentsUntil(TreeModule.$root, "li");
            TreeModule.$root.find(".aba-tree-active").not($li).removeClass("aba-tree-active");
            TreeModule.$root.find(".aba-tree-path").not($parents).removeClass("aba-tree-path");
            $li.addClass("aba-tree-active");
            $parents.addClass("aba-tree-path");
        }
        TreeModule.$root.focus();
        return false;
    }
    function treeDragChild(event) {
        var data = event.data || {}, $drag = data.drag, $helper = data.helper, dropList = "mouseup.tree touchend.tree", moveList = "mousemove.tree touchmove .tree", dragList = moveList + " " + dropList, hover = function (drop) {
            if (data.lastdrop !== drop && data.lastdrop) {
                $(data.lastdrop).removeClass("aba-tree-hover-" + data.lastpos);
            }
            if (drop) {
                var $drop = $(drop), rect = $drop.getRect(), height = rect.height, top = event.pageY - rect.top, pos = $drop.hasClass("aba-tree-children")
                    ? (top < height / 4 ? "prev" : top > height - height / 4 ? "next" : "inner")
                    : (top < height / 2 ? "prev" : "next");
                if (data.lastdrop !== drop || data.lastpos !== pos) {
                    $drop.removeClass("aba-tree-hover-" + data.lastpos).addClass("aba-tree-hover-" + pos);
                    data.lastpos = pos;
                }
            }
            data.lastdrop = drop;
        };
        if (event.which > 1 || event.isDefaultPrevented()) {
            return;
        }
        if (event.type === "touchstart" || event.type === "touchmove") {
            event.pageX = event.originalEvent.changedTouches[0].pageX;
            event.pageY = event.originalEvent.changedTouches[0].pageY;
        }
        switch (event.type) {
            case "touchstart":
            case "mousedown":
                $drag = $(this);
                var rect = $drag.getRect(), canDrag = callback(TreeModule.options.canDrag, $drag.data("id"));
                $(document)
                    .css("cursor", canDrag ? "ns-resize" : "not-allowed")
                    .on(canDrag ? dragList : dropList, {
                    drag: $drag,
                    lastdrop: null,
                    lastpos: "",
                    pageY: event.pageY,
                    pageX: event.pageX,
                    top: rect.top - event.pageY,
                    left: rect.left - event.pageX
                }, treeDragChild);
                return false;
            case "touchmove":
            case "mousemove":
                if ($drag && ($helper || Math.abs(data.pageY - event.pageY) > 10)) {
                    if (!$helper) {
                        data.helper = $helper = $drag.clone(true).removeClass().addClass("aba-tree-dragging").insertBefore($drag.addClass("aba-tree-selected"));
                        data.containerRect = TreeModule.$root.getRect();
                    }
                    ic.setImmediate("tree", function () {
                        if ($helper) {
                            $helper.css({
                                top: event.pageY + data.top,
                                left: event.pageX + data.left
                            });
                            hover(data.drop = $(document.elementFromPoint(event.pageX, event.pageY)).closest("li").not(".aba-tree-selected")[0]);
                        }
                    });
                }
                return false;
            case "touchend":
            case "mouseup":
                $(document)
                    .css("cursor", "")
                    .off(".tree");
                if ($helper) {
                    $helper.remove();
                    $drag.removeClass("aba-tree-selected");
                    if (data.drop) {
                        callback(TreeModule.options.onDrop, $drag.data("id"), $(data.drop).data("id"), data.lastpos);
                        hover(null);
                    }
                }
                else {
                    treeClickChild.call($drag[0]);
                }
                return false;
        }
    }
    function sort(id) {
        var $children = !id ? TreeModule.$root : get(id).children("ul");
        $children
            .children()
            .sort(function (a, b) {
            var a_index = index($.data(a, "id")), b_index = index($.data(b, "id"));
            if (a_index === b_index) {
                a_index = a.textContent;
                b_index = b.textContent;
            }
            return a_index === b_index ? 0 : a_index > b_index ? 1 : -1;
        })
            .appendTo($children)
            .filter(":not(.hidden)")
            .removeClass("aba-tree-lastchild")
            .last()
            .addClass("aba-tree-lastchild");
    }
    TreeModule.sort = sort;
    var sortId = [];
    function deferSort() {
        for (var i = 0; i < sortId.length; i++) {
            sort(sortId[i]);
        }
        sortId = [];
    }
    function add(id) {
        if (id) {
            var $li = get(id), parentId = getParentId(id) || "", $parent = get(parentId) || null;
            if (!$li.length) {
                TreeModule.nodes[id] = TreeModule.nodes[id] || [];
                TreeModule.nodes[id][0] = $li = $.make("li", callback(TreeModule.options.isParent, id) ? "aba-tree-children" : "aba-tree-node", $.make("span"), $.make("a", "=" + id)).data("id", id);
            }
            var content = callback(TreeModule.options.getHtml, id, $li.children("a"));
            if (content) {
                $li.children("a").html(content);
            }
            if ($parent.length) {
                if (parentId !== undefined) {
                    if (!parentId) {
                        $parent = TreeModule.$root;
                    }
                    else {
                        var tmp = $parent.children("ul");
                        if (!tmp.length) {
                            tmp = $.make("ul").appendTo($parent);
                        }
                        $parent = tmp;
                    }
                }
                if (!$parent.is($li.parent())) {
                    $parent.append($li);
                }
                sortId.pushOnce(parentId, id);
                ic.setImmediate(deferSort);
                callback(TreeModule.options.onAfterHtml, id, $li, $parent);
            }
            else {
                console.warn("no parent", parentId, "for", id);
            }
        }
    }
    TreeModule.add = add;
    ;
    function click(id) {
        var $el = get(id);
        if ($el.length) {
            treeClickChild.call($el);
            if (TreeModule.options.autoExpand) {
                expand(id);
            }
            return true;
        }
        return false;
    }
    TreeModule.click = click;
    ;
    function cancel() {
        TreeModule.$root.find(".aba-tree-active").removeClass("aba-tree-active");
        TreeModule.$root.find(".aba-tree-path").removeClass("aba-tree-path");
    }
    TreeModule.cancel = cancel;
    ;
    function closest(id) {
        id = id === undefined ? TreeModule.currentId : id;
        return get(id).hasClass("aba-tree-children") ? id : getParentId(id);
    }
    TreeModule.closest = closest;
    ;
    function doCopy() {
        copyId = TreeModule.currentId;
        copyCut = false;
    }
    TreeModule.doCopy = doCopy;
    function doCut() {
        doCopy();
        copyCut = true;
    }
    TreeModule.doCut = doCut;
    function doPaste() {
        if (copyId) {
            callback(copyCut ? TreeModule.options.onDrop : TreeModule.options.onPaste, copyId, TreeModule.currentId, isParent() ? "inner" : "next");
            if (copyCut) {
                copyId = undefined;
            }
        }
    }
    TreeModule.doPaste = doPaste;
    function empty() {
        TreeModule.nodes = {
            "": [TreeModule.$root.empty()]
        };
        TreeModule.currentId = "";
    }
    TreeModule.empty = empty;
    ;
    function expand(id) {
        get(id === undefined ? TreeModule.currentId : id).parents("li.aba-tree-children").addClass("aba-tree-children-open");
    }
    TreeModule.expand = expand;
    ;
    function get(id) {
        return (TreeModule.nodes[id === undefined ? TreeModule.currentId : id] || [])[0] || $();
    }
    TreeModule.get = get;
    ;
    function getNextId(id) {
        return get(id === undefined ? TreeModule.currentId : id).next().data("id");
    }
    TreeModule.getNextId = getNextId;
    ;
    function getParentId(id) {
        return callback(TreeModule.options.getParent, id === undefined ? TreeModule.currentId : id);
    }
    TreeModule.getParentId = getParentId;
    ;
    function getPrevId(id) {
        return get(id === undefined ? TreeModule.currentId : id).prev().data("id");
    }
    TreeModule.getPrevId = getPrevId;
    ;
    function goChild() {
        if (callback(TreeModule.options.isParent, TreeModule.currentId)) {
            var $parent = get();
            if ($parent.hasClass("aba-tree-children")) {
                if (!$parent.hasClass("aba-tree-children-open")) {
                    treeToggleChildren.call($parent);
                }
                treeClickChild.call($parent.find(">ul>li:first"));
            }
        }
        return false;
    }
    TreeModule.goChild = goChild;
    function goNext() {
        var next = getNextId() || getNextId(getParentId());
        if (next) {
            click(next);
        }
        return false;
    }
    TreeModule.goNext = goNext;
    function goParent() {
        var parent = getParentId();
        if (parent) {
            click(parent);
        }
        return false;
    }
    TreeModule.goParent = goParent;
    function goPrev() {
        var prev = getPrevId() || getParentId();
        if (prev) {
            click(prev);
        }
        return false;
    }
    TreeModule.goPrev = goPrev;
    function goToggle() {
        if (isParent()) {
            treeToggleChildren.call(get());
        }
        return false;
    }
    TreeModule.goToggle = goToggle;
    function index(id) {
        return callback(TreeModule.options.getIndex, id === undefined ? TreeModule.currentId : id);
    }
    TreeModule.index = index;
    ;
    function isParent(id) {
        return callback(TreeModule.options.isParent, id === undefined ? TreeModule.currentId : id);
    }
    TreeModule.isParent = isParent;
    ;
    function init(opts) {
        if (opts) {
            var tree = opts.tree;
            TreeModule.$root = (tree instanceof jQuery ? tree : $(tree || ".aba-tree"))
                .addClass("aba-tree")
                .attr("tabindex", "0")
                .on("mousedown touchstart", ".aba-tree-children>span:first-child", treeToggleChildren)
                .on("mousedown touchstart", "li", treeDragChild)
                .on("click", $.False)
                .on("keydown", function (event) {
                switch (event.which) {
                    case 32:
                        goToggle();
                        break;
                    case 37:
                        goParent();
                        break;
                    case 38:
                        goPrev();
                        break;
                    case 39:
                        goChild();
                        break;
                    case 40:
                        goNext();
                        break;
                    case 88:
                        if (event.ctrlKey || event.metaKey) {
                            doCut();
                            break;
                        }
                        return;
                    case 67:
                        if (event.ctrlKey || event.metaKey) {
                            doCopy();
                            break;
                        }
                        return;
                    case 86:
                        if (event.ctrlKey || event.metaKey) {
                            doPaste();
                            break;
                        }
                        return;
                    default:
                        return;
                }
                event.preventDefault();
            });
            TreeModule.options = opts;
            empty();
        }
    }
    TreeModule.init = init;
    ;
    function loadChildren(id) {
        id = id === undefined ? TreeModule.currentId : id;
        if (!TreeModule.nodes[id] || !TreeModule.nodes[id][1]) {
            TreeModule.nodes[id] = TreeModule.nodes[id] || [];
            TreeModule.nodes[id][1] = true;
            callback(TreeModule.options.getChildren, id);
        }
    }
    TreeModule.loadChildren = loadChildren;
    ;
    function remove(id) {
        id = id === undefined ? TreeModule.currentId : id;
        if (id) {
            var $el = get(id);
            if (id === TreeModule.currentId) {
                click(getNextId() || getPrevId() || getParentId());
            }
            $el.remove();
            delete TreeModule.nodes[id];
        }
    }
    TreeModule.remove = remove;
    ;
})(TreeModule || (TreeModule = {}));
;
$.tree = TreeModule;
$.expr.pseudos.abc = function (el, index, selector) {
    if (!selector[3]) {
        return $.hasData(el);
    }
    var attr = selector[3].split("=");
    if (attr.length === 1) {
        return $(el).data(attr[0]) !== undefined;
    }
    return $(el).data(attr[0]) == attr[1];
};
$.expr.pseudos.focusable = function (el) {
    var nodeName = el.nodeName.toLowerCase(), tabIndex = $.attr(el, "tabindex");
    return (/input|select|textarea|button|object/.test(nodeName) || $.attr(el, "contenteditable")
        ? !el.disabled
        : "a" === nodeName || "area" === nodeName
            ? el.href || !isNaN(tabIndex)
            : !isNaN(tabIndex))
        && ("area" === nodeName ? $(el).parents(":visible") : $(el).closest(":visible")).length;
};
$.expr.pseudos.css = function (el, index, selector) {
    var $el = $(el), attr;
    if (!selector[3]) {
        return !!$el.attr("style");
    }
    attr = selector[3].split("=");
    if (attr.length === 1) {
        return $el.css(attr[0]) !== $el.parent().css(attr[0]);
    }
    return $el.css(attr[0]) == attr[1];
};
function formatSeconds(seconds) {
    var secs = parseInt("" + (seconds % 60)), mins = parseInt("" + (seconds / 60));
    return mins + ":" + (secs > 9 ? "" : "0") + secs;
}
var ua = navigator.userAgent.toLowerCase();
var safariComplain = ua.includes("safari/") && ua.includes("windows") ? function () {
    safariComplain = $.noop;
    alert("Please install QuickTime to enable Audio or Video in Safari on Windows");
} : $.noop;
$.fn.audio = function () {
    $(this).each(function () {
        var $audio = $(this);
        if ($audio.data("abc-audio")) {
            return;
        }
        if (!this.load) {
            safariComplain();
        }
        else {
            this.load();
        }
        $audio
            .data("abc-audio", true)
            .on("play", function () {
            $("audio,video").not(this).pause();
            $("iframe").each(function () {
                try {
                    $(this.contentDocument).find("audio,video").pause();
                }
                catch (e) {
                }
            });
            $(this).closest(".abc-audio").removeClass("pause").addClass("play");
        })
            .on("pause", function () {
            $(this).closest(".abc-audio").removeClass("play").addClass("pause");
        })
            .on("ended.abc-audio", function () {
            var $this = $(this);
            $this.closest(".abc-audio").removeClass("play pause");
            this.currentTime = 0;
            this.pause();
            $this.siblings("svg").find("path").attr("d", "M0 0");
        })
            .on("timeupdate.abc-audio", function () {
            var $this = $(this), currentTime = this.currentTime, duration = this.duration, percent = currentTime / duration, angle = Math.range(0, percent * 360, 359), rad = (angle * Math.PI / 180), x = Math.sin(rad) * 153, y = Math.cos(rad) * -153, mid = (angle > 180) ? 1 : 0, $control = $this.closest(".abc-audio-wrapper").children(".abc-audio-control");
            percent = (percent * 100) + "%";
            $control.children("span").css("left", percent).text(formatSeconds(currentTime));
            $control.children("hr").css("width", percent);
            $this.siblings("svg").find("path").attr("d", "M 0 0" + (angle > 0 ? " v -153 A 153 153 1 " + mid + " 1 " + x + " " + y + " z" : ""));
        });
    });
    return this;
};
$.fn.autoTextSize = function (options) {
    return this.css("font-size", this.findTextSize(options) + "%");
};
$.fn.closestWidget = function () {
    for (var parent = this[0]; parent; parent = parent.parentElement) {
        if (/^IC-/i.test(parent.tagName)) {
            break;
        }
    }
    return $(parent);
};
$.fn.common = function (selector) {
    if (selector && this.is(selector)) {
        return this;
    }
    var i, $parents = (selector ? this : this.eq(0)).family(), $targets = selector ? $(selector) : this.slice(1);
    for (i = 0; i < $targets.length; i++) {
        $parents = $parents.has($targets.eq(i).family());
    }
    return $parents;
};
$.fn.commonFirst = function (selector) {
    return this.common(selector).first();
};
$.fn.convertCSS = function (value, horiz) {
    if (value === undefined || value === "") {
        return 0;
    }
    var val = parseFloat(value);
    switch (value.replace(/[^a-z%]/g, "").substr(0, 4)) {
        case "none":
            val = 0;
            break;
        case "calc":
            var operand, num, sum = value.replace(/^calc\(|\)$/g, "").split(" ");
            val = this.convertCSS(sum.shift(), horiz);
            while (sum.length) {
                if (operand) {
                    num = this.convertCSS(sum.shift(), horiz);
                    if (operand === "+") {
                        val += num;
                    }
                    else if (operand === "-") {
                        val -= num;
                    }
                    else if (operand === "*") {
                        val *= num;
                    }
                    else if (operand === "/") {
                        val /= num;
                    }
                    operand = "";
                }
                else {
                    operand = sum.shift();
                }
            }
            break;
        case "em":
            val *= parseFloat(this.css("font-size"));
            break;
        case "percent":
            val *= this.parent()[horiz ? "width" : "height"]() / 100;
            break;
        case "rem":
            val *= parseFloat($("html").css("font-size"));
            break;
        case "vh":
            val *= window.innerHeight / 100;
            break;
        case "vw":
            val *= window.innerWidth / 100;
            break;
        case "vmin":
            val *= Math.min(window.innerWidth, window.innerHeight) / 100;
            break;
        case "vmax":
            val *= Math.max(window.innerWidth, window.innerHeight) / 100;
            break;
        case "px":
        case "":
            break;
        default:
            console.warn("convertCSS: Unknown conversion type!! (" + value + ")");
            return 0;
    }
    return val;
};
$.fn.family = function () {
    var i, el, nodes = $();
    for (i = 0; i < this.length; i++) {
        for (el = this[i]; el !== document; el = el.parentNode) {
            nodes.push(el);
        }
    }
    return nodes;
};
$.fn.findTextSize = function (options) {
    var i, element, length = this.length, step, $size, $this, lastGood, width, height, $hidden, opts = $.extend({}, $.fn.findTextSize.defaults, options), maxWidth = opts.maxWidth || 0, maxHeight = opts.maxHeight || 0, maxSize = opts.maxSize || 200, minSize = opts.minSize || 50, fontSize = maxSize, getHighestBit = function (num) {
        num |= (num >> 1);
        num |= (num >> 2);
        num |= (num >> 4);
        num |= (num >> 8);
        num |= (num >> 16);
        return num + 1;
    };
    if (minSize !== maxSize) {
        for (i = 0; i < length; i++) {
            element = this.get(i);
            step = getHighestBit(Math.ceil(maxSize - minSize) * 2);
            $size = isString(opts.useParent) ? $(element).closest(opts.useParent) : $(opts.useParent ? element.parentNode : element);
            width = Math.ceil(maxWidth || $size.width());
            height = Math.ceil(maxHeight || $size.height());
            $this = $(element)
                .style({
                overflow: "hidden",
                maxWidth: width + "px",
                maxHeight: "none",
                width: "auto",
                height: "auto",
                fontSize: "1px"
            });
            $hidden = $this.find(":css(position=absolute)").filter(":visible").not(element).style({ display: "none" });
            lastGood = minSize;
            while (step > 0.1) {
                step /= 2;
                fontSize = Math.min(maxSize, fontSize + step);
                element.style.fontSize = fontSize + "%";
                if (element.scrollWidth <= width && element.scrollHeight <= height) {
                    if (fontSize === maxSize) {
                        break;
                    }
                    lastGood = fontSize;
                }
                else if (fontSize > minSize) {
                    fontSize = lastGood;
                }
                else {
                    break;
                }
            }
            maxSize = fontSize;
            $this.style();
            $hidden.style();
        }
    }
    return fontSize;
};
$.fn.findTextSize.defaults = {
    useParent: true,
    maxWidth: 0,
    maxHeight: 0,
    minSize: 50,
    maxSize: 200
};
$.fn.fixNatural = function () {
    return this.each(function (i, el) {
        if (this.naturalWidth === undefined) {
            var theImage = new Image();
            theImage.src = this.src;
            this.naturalWidth = theImage.width;
            this.naturalHeight = theImage.height;
        }
    });
};
$.fn.getRect = function () {
    var el = this[0];
    if (el) {
        var rect = el.getBoundingClientRect();
        return {
            top: rect.top,
            right: rect.right,
            bottom: rect.bottom,
            left: rect.left,
            height: rect.height || (rect.bottom - rect.top),
            width: rect.width || (rect.right - rect.left)
        };
    }
};
$.fn.hasState = function (state) {
    return !!(this[0].icState || {})[state];
};
$.fn.hoverable = function (className) {
    return this.hover(function () {
        $(this).addClass(className || "ui-state-hover");
    }, function () {
        $(this).removeClass(className || "ui-state-hover");
    });
};
$.fn.imageLoad = function (callback, repeat) {
    return this.each(function () {
        var $el = $(this), complete = this.complete;
        if (complete) {
            callback.call(this, $el);
        }
        if (!complete || repeat) {
            $el[repeat ? "on" : "one"]("load", callback.bind(this, $el));
        }
    });
};
$.fn.insertAt = function (index, parent) {
    var i, $parent = $(parent);
    if (index > 0) {
        $parent = $parent.children().eq(index - 1);
    }
    for (i = this.length - 1; i >= 0; i--) {
        if (index <= 0) {
            $parent.prepend(this);
        }
        else {
            $parent.after(this);
        }
    }
    return this;
};
$.fn.loading = $.loading = function (toggle, timeout) {
    var show = true, className = "ui-loading", $parent = $(!this || this === $ ? "body" : this), $spinner = $parent.data(className + "-element");
    if (toggle === undefined) {
        show = $spinner === undefined;
    }
    else if ($.isBoolean(toggle)) {
        show = toggle;
    }
    if (show && !$spinner) {
        $parent
            .data(className + "-element", $spinner = $.make("div", className + " fa fa-cog fa-spin"))
            .data(className + "-timeout", window.setTimeout(function () {
            $spinner.appendTo($parent).modal(true);
        }, timeout || 100));
    }
    else if (!show && $spinner) {
        window.clearTimeout($parent.data(className + "-timeout"));
        $spinner.modal(false).remove();
        $parent.removeData([className + "-element", className + "-timeout"]);
    }
    return this;
};
$.fn.max = function (prop, def) {
    var val = Number.MIN_VALUE;
    this.each(function (i, el) {
        val = Math.max(val, parseFloat($(el).css(prop)));
    });
    return (val === Number.MIN_VALUE ? def : val) || 0;
};
$.fn.maxHeight = function () {
    for (var i = 0, height = 0; i < this.length; i++) {
        height = Math.max(height, this.eq(i).height());
    }
    return height;
};
$.fn.maxWidth = function () {
    for (var i = 0, width = 0; i < this.length; i++) {
        width = Math.max(width, this.eq(i).width());
    }
    return width;
};
$.fn.min = function (prop, def) {
    var val = Number.MAX_VALUE;
    this.each(function (i, el) {
        val = Math.min(val, parseFloat($(el).css(prop)));
    });
    return (val === Number.MAX_VALUE ? def : val) || 0;
};
$.fn.modal = function (toggle, duration) {
    duration = duration === true ? 400 : duration;
    return this.each(function (i, element) {
        var show = true, $el = $(element), $data = $el.data("ui-modal");
        if (toggle === undefined) {
            show = $data === undefined;
        }
        else if ($.isBoolean(toggle)) {
            show = toggle;
        }
        if (show && !$data) {
            $data = $.make("div", "ui-front ui-widget-overlay", duration ? "ui-widget-slow" : null, duration ? "opacity:0" : null);
            $el
                .before($data)
                .addClass("ui-front")
                .data("ui-modal", $data);
            $("#abc>.abc-footer").addClass("disabled");
            if (duration) {
                $data.velocity("fadeIn", duration);
            }
        }
        else if (!show && $data) {
            duration = duration || $data.hasClass("ui-widget-slow");
            $el
                .removeClass("ui-front")
                .removeData("ui-modal");
            $("#abc>.abc-footer").removeClass("disabled");
            if (duration) {
                $data.velocity("fadeOut", duration, function () {
                    $(this).remove();
                });
            }
            else {
                $data.remove();
            }
        }
    });
};
$.fn.naturalHeight = function () {
    return this.length ? this[0].naturalHeight || this.fixNatural()[0].naturalHeight : null;
};
$.fn.naturalWidth = function () {
    return this.length ? this[0].naturalWidth || this.fixNatural()[0].naturalWidth : null;
};
$.fn.onFontLoaded = function (callback, font) {
    if (!$.isFunction(callback)) {
        return this;
    }
    return this.each(function (i, el) {
        var $div = $.make("div", "ui-font-loader", "font-family:sans-serif", ["text", "giItT1WQy@!-/#"]).appendTo(el);
        window.setTimeout($.fn.onFontLoaded.check.bind($div, 0, $div.width(), $div.height(), callback), $.fn.onFontLoaded.defaults.delay);
        $div.css("font-family", font || "inherit");
    });
};
$.fn.onFontLoaded.check = function (count, width, height, callback) {
    if (width !== this.width() || height !== this.height() || count >= $.fn.onFontLoaded.defaults.maximum) {
        this.remove();
        callback();
    }
    else {
        window.setTimeout($.fn.onFontLoaded.check.bind(this, count + 1, width, height, callback), $.fn.onFontLoaded.defaults.delay);
    }
};
$.fn.onFontLoaded.defaults = {
    "delay": 50,
    "maximum": 40
};
$.fn.pause = function () {
    return this.each(function () {
        try {
            this.pause();
        }
        catch (e) {
        }
    });
};
$.fn.play = function () {
    return this.each(function () {
        try {
            this.play();
        }
        catch (e) {
        }
    });
};
$.fn.rxRemoveClass = function (className) {
    return this.each(function () {
        $(this).removeClass(this.className.regex(className));
    });
};
$.fn.shuffle = function (shuffle) {
    return shuffle === false ? this : [].shuffle.call($(this));
};
$.fn.slideTo = function (target, options) {
    var isParent = !target || target === "parent", $target = isParent ? null : $(target);
    if (isParent || $target.length) {
        return this.each(function () {
            var $this = $(this), offset = $this.offset();
            if (!isParent && !$target.is($this.parent())) {
                $this.appendTo($target);
            }
            $this.stop(true);
            if (options === 0 || ($.isPlainObject(options) && options.duration === 0)) {
                $this.css({
                    top: 0,
                    left: 0
                });
            }
            else {
                $this.offset(offset).animate({
                    top: 0,
                    left: 0
                }, options);
            }
        });
    }
    return this;
};
$.fn.style = function (props) {
    var i, tmp, style, index, styles, _saved = "savedStyles";
    for (index = 0; index < this.length; index++) {
        style = this.get(index).style;
        styles = this[_saved + index];
        if ($.isArray(props)) {
            if (!styles) {
                styles = this[_saved + index] = {};
            }
            for (i = 0; i < props.length; i++) {
                tmp = $.prefix(props[i]);
                styles[tmp] = style[tmp];
            }
        }
        else if ($.isPlainObject(props)) {
            if (!styles) {
                styles = this[_saved + index] = {};
            }
            for (i in props) {
                if (props.hasOwnProperty(i)) {
                    tmp = $.prefix(i);
                    styles[tmp] = style[tmp];
                    style[tmp] = props[i];
                }
            }
        }
        else if (styles) {
            for (i in styles) {
                if (styles.hasOwnProperty(i)) {
                    style[i] = styles[i];
                }
            }
            delete this[_saved + index];
        }
    }
    return this;
};
$.fn.textSpace = function () {
    var text = "";
    $(this).find("*").contents().each(function () {
        if (this.nodeType === 3) {
            text += (text ? " " : "") + this.textContent;
        }
    });
    return text;
};
$.fn.verticalCenter = function (height) {
    var isString = $.isString(height), padding = [];
    this
        .each(function (i, element) {
        var $this = $(element), font = parseFloat($this.css("font-size"));
        height = (((isString ? $(height, element).height() : (height || $this.parent().height())) - $this.height()) / 2);
        padding[i] = Math.max(0, height / font);
    })
        .each(function (i, element) {
        $(element).css("padding-top", padding[i] + "em");
    });
    return this;
};
$.fn.wrapGroup = function (wrappingElement) {
    var $next, $this = $(this);
    while ($this.length) {
        $next = $this.first();
        $next.nextAll().each(function (index) {
            if (!$this.eq(index + 1).is(this)) {
                return false;
            }
            $next.push(this);
        });
        $this = $this.not($next.wrapAll(wrappingElement));
    }
    return this;
};
var Config = (function () {
    function Config() {
    }
    Config.server = "../";
    Config.pollInterval = 10000;
    Config.pollWatchers = 5000;
    Config.backup = false;
    Config.replicate = false;
    return Config;
}());
var PERM;
(function (PERM) {
    PERM[PERM["NONE"] = 0] = "NONE";
    PERM[PERM["ROOT"] = 1] = "ROOT";
    PERM[PERM["OWNER"] = 2] = "OWNER";
    PERM[PERM["MANAGE"] = 4] = "MANAGE";
    PERM[PERM["VIEW"] = 8] = "VIEW";
    PERM[PERM["NODE_CREATE"] = 16] = "NODE_CREATE";
    PERM[PERM["NODE_DELETE"] = 32] = "NODE_DELETE";
    PERM[PERM["NODE_CONTENT"] = 64] = "NODE_CONTENT";
    PERM[PERM["NODE_THEME"] = 128] = "NODE_THEME";
    PERM[PERM["FILES"] = 256] = "FILES";
    PERM[PERM["ALL"] = 511] = "ALL";
    PERM[PERM["EDIT"] = 503] = "EDIT";
})(PERM || (PERM = {}));
var Permission = (function () {
    function Permission(raw) {
        this._raw = raw || 0;
    }
    Object.defineProperty(Permission.prototype, "raw", {
        get: function () {
            return this._raw;
        },
        set: function (raw) {
            this._raw = raw || 0;
        },
        enumerable: true,
        configurable: true
    });
    Permission.prototype.getBit = function (bit) {
        return !!(this._raw & bit);
    };
    Permission.prototype.setBit = function (bit, set) {
        if (set) {
            this._raw |= bit;
        }
        else {
            this._raw &= ~bit;
        }
    };
    Object.defineProperty(Permission.prototype, "isRoot", {
        get: function () {
            return this.getBit(1);
        },
        set: function (val) {
            this.setBit(1, val);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Permission.prototype, "isOwner", {
        get: function () {
            return this.getBit(2);
        },
        set: function (val) {
            this.setBit(2, val);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Permission.prototype, "isManager", {
        get: function () {
            return this.getBit(4);
        },
        set: function (val) {
            this.setBit(4, val);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Permission.prototype, "canView", {
        get: function () {
            return this.getBit(8);
        },
        set: function (val) {
            this.setBit(8, val);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Permission.prototype, "canCreate", {
        get: function () {
            return this.getBit(16);
        },
        set: function (val) {
            this.setBit(16, val);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Permission.prototype, "canDelete", {
        get: function () {
            return this.getBit(32);
        },
        set: function (val) {
            this.setBit(32, val);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Permission.prototype, "canContent", {
        get: function () {
            return this.getBit(64);
        },
        set: function (val) {
            this.setBit(64, val);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Permission.prototype, "canTheme", {
        get: function () {
            return this.getBit(128);
        },
        set: function (val) {
            this.setBit(128, val);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Permission.prototype, "canEdit", {
        get: function () {
            return this.getBit(503);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Permission.prototype, "canFile", {
        get: function () {
            return this.getBit(256);
        },
        set: function (val) {
            this.setBit(256, val);
        },
        enumerable: true,
        configurable: true
    });
    Permission.prototype.has = function (need) {
        var perms = need instanceof Permission ? need._raw : need;
        return (this._raw & perms) !== 0;
    };
    Permission.prototype.is = function (need) {
        var perms = need instanceof Permission ? need._raw : need;
        return (this._raw & perms) === need;
    };
    return Permission;
}());
;
function fixPrototype(className, property) {
    var proto = className.prototype || {};
    if (property) {
        if (proto.hasOwnProperty(property)) {
            Object.defineProperty(proto, property, { "enumerable": false });
        }
    }
    else {
        for (property in proto) {
            fixPrototype(className, property);
        }
    }
}
var Editor;
(function (Editor) {
    Editor.change = false;
    Editor.permissions = new Permission(0);
    Editor.ctrlKey = false;
    function keyboardModifiers(event) {
        Editor.ctrlKey = event.ctrlKey;
    }
    document.addEventListener("keydown", keyboardModifiers);
    document.addEventListener("keyup", keyboardModifiers);
    var ajaxLoggedOutData = [];
    function ajaxSuccessHandler(data) {
        var originalCall = this;
        if (isObject(data) && data.error === "Not logged in") {
            if (document.body.style.opacity !== "1") {
                window.location.href = Config.server;
            }
            else if (!$("#login_form").length) {
                ajaxLoggedOutData.push(originalCall);
                $("#ic_dialog").dialog("close");
                Editor.online = false;
                var $username, $password, doLogin = function () {
                    console.log("login");
                    $.post(Config.server + "ajax.php", {
                        username: Editor.username ? Editor.username : $username.val(),
                        password: $password.val()
                    }, function () {
                        $form.dialog("close").remove();
                        $iframe.remove();
                        Editor.online = true;
                        Button.update();
                        while (ajaxLoggedOutData.length) {
                            Editor.ajax(ajaxLoggedOutData.shift());
                        }
                    });
                }, $iframe = $.make("iframe", "src=/ajax.php", "#login_target", "name=login_target", "display:none"), $form = $.make("form", "#login_form", "name=login_form", "target=login_target", "autocomplete=on", "title=Login to Infuze Creator", $.make("table", "margin: 0 auto;", $.make("tbody", $.make(Editor.online, "tr", $.make(Editor.online, "td", "colspan=2", "text-align: left;", "=Your current session has been logged out and you need to login to continue.<br/><br/>")), $.make("tr", $.make("td", "text-align: right;", "=Email:"), $.make("td", $username = $.make("input", "type=email", "name=username", Editor.username ? "value=" + Editor.username : null, Editor.username ? "disabled=" : null).on("keyup", function (event) {
                    if (event.which === 13) {
                        $password.focus();
                    }
                }))), $.make("tr", $.make("td", "text-align: right;", "=Password:"), $.make("td", $password = $.make("input", "type=password", "name=password").on("keyup", function (event) {
                    if (event.which === 13) {
                        $form.submit();
                    }
                }))))));
                $iframe.appendTo("body");
                $form.appendTo("body").submit(doLogin).dialog({
                    modal: true,
                    buttons: {
                        "Login": function () {
                            $form.submit();
                        }
                    }
                });
                $form.prev().children("button").remove();
            }
        }
        else {
            originalCall.success.apply(window, arguments);
        }
    }
    function preventDefault(event) {
        event.preventDefault();
    }
    Editor.preventDefault = preventDefault;
    function ajax(url, settings) {
        if (Editor.online !== false) {
            var newSettings = settings || url;
            if (Config.server) {
                if (isString(url)) {
                    settings.url = url;
                }
                else {
                    settings = url;
                    url = undefined;
                }
                if (isObject(settings)) {
                    newSettings = settings.clone();
                    if (isString(newSettings.url)) {
                        newSettings.url = Config.server + newSettings.url;
                    }
                    if (isFunction(newSettings.success)) {
                        newSettings.success = ajaxSuccessHandler.bind(settings);
                    }
                }
            }
            return $.ajax(newSettings);
        }
    }
    Editor.ajax = ajax;
    function resizeLayout() {
        if (Editor.SourcePanel.instance) {
            ic.setImmediate("resize_layout", function () {
                Editor.SourcePanel.instance.editor.resize();
                Editor.saveSettings();
            });
        }
    }
    function setHtml(html) {
        if (Editor.currentActivity) {
            if (Editor.currentActivity.html !== html) {
                Editor.currentActivity.html = html.replace(/[\n\r\t]+/g, "");
            }
            if (Editor.currentActivity.changed) {
                Editor.Panel.trigger(3);
                if (!Editor.change) {
                    Editor.change = true;
                    Button.update();
                }
            }
        }
        else {
            console.error("Error: Trying to update an activity without having one selected");
        }
    }
    Editor.setHtml = setHtml;
    function checkWatchers() {
        if (Editor.currentActivity) {
            ajax({
                type: "POST",
                url: "ajax.php",
                global: false,
                dataType: "json",
                data: {
                    "action": "set_current",
                    "id": Editor.currentActivity._id
                },
                success: function (data) {
                    if (!data.error) {
                        Editor.currentActivity.watchers = data;
                        Editor.EditPanel.instance.updateWatchers();
                    }
                }
            });
        }
    }
    Editor.checkWatchers = checkWatchers;
    document.addEventListener("visibilitychange", checkWatchers);
    window.addEventListener("focus", checkWatchers);
    function setActivity(activity) {
        if (Editor.currentActivity) {
            if (Editor.currentActivity.$html) {
                Editor.currentActivity.html = Editor.currentActivity.$html;
                Editor.currentActivity.changed = false;
            }
            if (Editor.currentActivity !== activity) {
                Editor.currentActivity.watchers.remove(Editor.username);
            }
        }
        Editor.currentActivity = activity;
        checkWatchers();
        if (Editor.currentActivity) {
            Editor.currentActivity.watchers.pushOnce(Editor.username);
        }
        Editor.change = false;
        Editor.Panel.trigger(2);
        Editor.Panel.trigger(3);
        Editor.change = false;
        Button.update();
    }
    Editor.setActivity = setActivity;
    ;
    function log(text) {
        var $footer = $("#ic_footer");
        if ($footer.html() !== "") {
            $footer
                .velocity("stop", true)
                .velocity({
                colorAlpha: 0
            }, 50);
        }
        if (text !== "") {
            $footer
                .velocity({
                colorAlpha: 1
            }, {
                begin: function () {
                    $footer.html(text);
                }
            })
                .velocity({
                colorAlpha: 0
            }, {
                delay: 10000,
                complete: function () {
                    $footer.html("");
                }
            });
        }
    }
    Editor.log = log;
    ;
    function dialog(text) {
        var opts = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            opts[_i - 1] = arguments[_i];
        }
        var i, arg, title, callback, complete, error, string, $string, args = arguments;
        for (i = 1; i < args.length; i++) {
            arg = args[i];
            if (isString(arg)) {
                if (!title) {
                    title = arg;
                }
                else {
                    string = arg;
                }
            }
            else if (isBoolean(arg)) {
                error = arg;
            }
            else if (isFunction(arg)) {
                if (!callback) {
                    callback = arg;
                }
                else {
                    complete = arg;
                }
            }
        }
        if (isBoolean(callback)) {
            error = callback;
            callback = null;
        }
        if (!$("#login_form")[0]) {
            $("#ic_dialog").dialog("close");
            $.make("div", "#ic_dialog", "title=" + (title || "Confirm"), "font-weight:bold;font-size:2em;margin-top:0.2em;", error ? "border:1px solid red" : "", error ? $.make("span", "fa fa-exclamation-triangle", "float:left;color:red;") : null, $.make("span", "margin-left:0.5em;", "=" + text), isString(string) ? $string = $.make("textarea", "placeholder=" + string, "=" + string, "display:block;margin:0.5em auto;max-width:50vw;border:1px solid #ddd;padding: 0.5em 0.25em;") : null)
                .dialog({
                buttons: callback ? {
                    "Okay": function () {
                        if (callback.call(this, $string ? $string.val() : true) !== false && (!complete || complete.call(this) !== false)) {
                            $(this).dialog("close");
                        }
                    },
                    "Cancel": function () {
                        if (!complete || complete.call(this) !== false) {
                            $(this).dialog("close");
                        }
                    }
                } : {
                    "Okay": function () {
                        if (!complete || complete.call(this) !== false) {
                            $(this).dialog("close");
                        }
                    }
                },
                close: function () {
                    $(this).remove();
                },
                modal: true,
                draggable: true,
                resizable: false,
                width: "auto"
            });
            if ($string) {
                $string.keyup();
            }
        }
    }
    Editor.dialog = dialog;
    ;
    function fixTextAreaHeight() {
        var height = this.scrollHeight;
        if (height) {
            $(this).innerHeight(height);
        }
    }
    Editor.fixTextAreaHeight = fixTextAreaHeight;
    Editor.panels = {};
    var panelCount = 6;
    function loadPanel() {
        var container = this.box, side = container.parentElement.id.split("_").pop();
        Editor.panels[side] = container;
        if (!--panelCount) {
            ic.setImmediate("gui_loaded", gui_loaded);
        }
    }
    function startup() {
        console["log"]("Starting Infuze Creator...");
        Editor.panel_layout = $("#layout").w2layout({
            name: "layout",
            padding: 0,
            resizer: 6,
            onResize: resizeLayout,
            panels: [
                {
                    type: "top",
                    size: 107,
                    overflow: "hidden",
                    content: {
                        render: loadPanel
                    }
                },
                {
                    type: "left",
                    size: Editor.settings.size["left"] + "%",
                    resizable: true,
                    content: {
                        render: loadPanel
                    },
                    tabs: {
                        tabs: []
                    }
                },
                {
                    type: "main",
                    overflow: "hidden",
                    content: {
                        render: loadPanel
                    },
                    tabs: {
                        tabs: []
                    }
                },
                {
                    type: "preview",
                    hidden: true,
                    size: Editor.settings.size["preview"] + "%",
                    resizable: true,
                    content: {
                        render: loadPanel
                    },
                    tabs: {
                        tabs: []
                    }
                },
                {
                    type: "right",
                    hidden: true,
                    size: Editor.settings.size["right"] + "%",
                    resizable: true,
                    content: {
                        render: loadPanel
                    },
                    tabs: {
                        tabs: []
                    }
                },
                {
                    type: "bottom",
                    size: 24,
                    overflow: "hidden",
                    content: {
                        render: loadPanel
                    }
                }
            ]
        });
    }
    Editor.startup = startup;
    function startGUI() {
        Object.keys(Editor).forEach(function (className) {
            var PanelClass = Editor[className];
            if (PanelClass && PanelClass.prototype && PanelClass.prototype instanceof Editor.Panel) {
                try {
                    new PanelClass();
                }
                catch (error) {
                    if (error !== "No permission") {
                        throw error;
                    }
                }
            }
        });
        Editor.Panel.startup();
        $(".w2ui-panel-tabs tr").sortable({
            connectWith: ".w2ui-panel-tabs tr",
            containment: "document",
            distance: 10,
            forcePlaceholderSize: true,
            helper: function (event, element) {
                var helper = createElement("span");
                helper.innerText = element[0].innerText.trim();
                helper.id = "ic_drag_panel";
                document.body.appendChild(helper);
                return helper;
            },
            items: ">td:not([width])",
            placeholder: "ic-drag-panel-target",
            revert: "invalid",
            scroll: false,
            start: function () {
                $("iframe").css("pointer-events", "none");
            },
            stop: function () {
                $("iframe").css("pointer-events", "");
            },
            update: function (event, ui) {
                Editor.saveSettings();
                window.location.reload(false);
            }
        });
        Editor.Panel.trigger(0);
        $("#aba-button-logout").on("click", function () {
            window.location.href = Config.server + "logout.php";
        });
        $(document).on("change keyup keydown paste cut", "textarea", fixTextAreaHeight);
    }
    function gui_loaded() {
        ajax({
            type: "POST",
            url: "ajax.php",
            global: false,
            dataType: "json",
            data: {
                "action": "get_group"
            },
            success: function (data) {
                if (data.error) {
                    return dialog("Error: " + data.error, "Server Error", true);
                }
                Editor.permissions.raw = data.permission;
                if (Editor.permissions.has(1 | 256)) {
                    var body = document.body, drop = getElementById("file_drop"), counter = 0, show_drop = function () {
                        if (!counter++) {
                            drop.classList.add("show");
                        }
                    }, hide_drop = function () {
                        if (!--counter) {
                            drop.classList.remove("show");
                        }
                    };
                    body.addEventListener("dragenter", show_drop, false);
                    body.addEventListener("dragleave", hide_drop, false);
                    body.addEventListener("drop", hide_drop, false);
                }
                Config.backup = !!data.backup;
                Config.replicate = !!data.replicate;
                startGUI();
                $("#aba-text-account")
                    .text(Editor.username = data.email);
                $("#aba-gravatar").attr("src", "https://www.gravatar.com/avatar/" + Editor.username.trim().toLowerCase().md5() + ".jpg?s=24&d=identicon");
                Editor.rootId = data[0].node;
                Editor.findActivityId = location.hash.regex(/^#([0-9]+)/) || Editor.rootId;
                var i, nodes = Editor.permissions.isRoot ? [0] : [];
                for (i in data) {
                    if (/^\d+$/.test(i)) {
                        nodes.push(data[i].node);
                    }
                }
                if (!!Editor.getSetting("options.advanced")) {
                    document.body.classList.add("advanced");
                }
                ICNodes.loadParents(Editor.findActivityId);
                ICNodes.load(nodes);
                console.log("root", Editor.permissions, nodes);
                Editor.online = true;
                Editor.Panel.trigger(1);
            }
        });
        document.body.style.opacity = "1";
    }
})(Editor || (Editor = {}));
var ICNodeType;
(function (ICNodeType) {
    ICNodeType[ICNodeType["Dummy"] = -1] = "Dummy";
    ICNodeType[ICNodeType["Organisation"] = 0] = "Organisation";
    ICNodeType[ICNodeType["Templates"] = 1] = "Templates";
    ICNodeType[ICNodeType["Project"] = 2] = "Project";
    ICNodeType[ICNodeType["Folder"] = 3] = "Folder";
    ICNodeType[ICNodeType["Group"] = 4] = "Group";
    ICNodeType[ICNodeType["Screen"] = 10] = "Screen";
    ICNodeType[ICNodeType["Theme"] = 11] = "Theme";
    ICNodeType[ICNodeType["Link"] = 12] = "Link";
    ICNodeType[ICNodeType["Template"] = 13] = "Template";
})(ICNodeType || (ICNodeType = {}));
var ICNodeTypeChildren = (_a = {},
    _a[-1] = [0],
    _a[0] = [1, 2, 3],
    _a[1] = [3, 11, 13],
    _a[2] = [4, 10, 12],
    _a[3] = [1, 2],
    _a[4] = [10, 12],
    _a[10] = [],
    _a[11] = [],
    _a[12] = [],
    _a[13] = [],
    _a);
var ICNodeTypeName = (_b = {},
    _b[-1] = "dummy",
    _b[0] = "organisation",
    _b[1] = "templates",
    _b[2] = "project",
    _b[3] = "folder",
    _b[4] = "group",
    _b[10] = "screen",
    _b[11] = "theme",
    _b[12] = "link",
    _b[13] = "template",
    _b);
function ICNodeTypeHtml(type) {
    switch (type) {
        case -1:
        case 3:
            return "%%content%%";
        case 4:
            return "<div unwrap>%%content%%</div>";
        case 0:
            return "<title>Infuze Creator</title>"
                + "<link rel=\"stylesheet\" type=\"text/css\" href=\"activity/build/activity.css\"/>"
                + "<link rel=\"stylesheet\" type=\"text/css\" href=\"../assets/3/theme.css\"/>"
                + "<script type=\"text/javascript\" src=\"activity/build/activity.js\" defer></script>"
                + "%%content%%";
        case 2:
            return "<ic-screens><ic-screen>%%content%%</ic-screen></ic-screens>";
        case 10:
            return "<ic-box></ic-box>";
        case 1:
        case 11:
        case 12:
        case 13:
            return "";
    }
    console.error("ICNodeTypeHtml() with invalid type:", this);
    return "";
}
var ICNodeFlags;
(function (ICNodeFlags) {
    ICNodeFlags[ICNodeFlags["LOCK"] = 1] = "LOCK";
    ICNodeFlags[ICNodeFlags["DELETED"] = 2] = "DELETED";
    ICNodeFlags[ICNodeFlags["CHECK"] = 4] = "CHECK";
    ICNodeFlags[ICNodeFlags["IMPORTANT"] = 8] = "IMPORTANT";
    ICNodeFlags[ICNodeFlags["URGENT"] = 16] = "URGENT";
    ICNodeFlags[ICNodeFlags["IGNORE"] = 32] = "IGNORE";
    ICNodeFlags[ICNodeFlags["STATE_INCOMPLETE"] = 64] = "STATE_INCOMPLETE";
    ICNodeFlags[ICNodeFlags["STATE_CHANGE"] = 128] = "STATE_CHANGE";
    ICNodeFlags[ICNodeFlags["STATE_CHECK"] = 256] = "STATE_CHECK";
    ICNodeFlags[ICNodeFlags["STATE_COMPLETE"] = 512] = "STATE_COMPLETE";
    ICNodeFlags[ICNodeFlags["QA_BAD"] = 1024] = "QA_BAD";
    ICNodeFlags[ICNodeFlags["QA_UNSURE"] = 2048] = "QA_UNSURE";
    ICNodeFlags[ICNodeFlags["QA_GOOD"] = 4096] = "QA_GOOD";
    ICNodeFlags[ICNodeFlags["STAR_NONE"] = 8192] = "STAR_NONE";
    ICNodeFlags[ICNodeFlags["STAR_HALF"] = 16384] = "STAR_HALF";
    ICNodeFlags[ICNodeFlags["STAR_FULL"] = 32768] = "STAR_FULL";
    ICNodeFlags[ICNodeFlags["ACCESSIBLE"] = 65536] = "ACCESSIBLE";
    ICNodeFlags[ICNodeFlags["IMAGE"] = 131072] = "IMAGE";
    ICNodeFlags[ICNodeFlags["SOUND"] = 262144] = "SOUND";
    ICNodeFlags[ICNodeFlags["TEXT"] = 524288] = "TEXT";
    ICNodeFlags[ICNodeFlags["VIDEO"] = 1048576] = "VIDEO";
})(ICNodeFlags || (ICNodeFlags = {}));
var ICNode = (function () {
    function ICNode(data) {
        var _this = this;
        if (data) {
            data.clone(this);
        }
        ICNode.privateKeys.forEach(function (key) {
            Object.defineProperty(_this, key, {
                "enumerable": false,
                "writable": true
            });
        });
    }
    ICNode.prototype.toJSON = function () {
        var ret = {};
        for (var property in this) {
            if (this.hasOwnProperty(property)) {
                ret[property] = this[property];
            }
        }
        return ret;
    };
    ;
    Object.defineProperty(ICNode.prototype, "isHidden", {
        get: function () {
            return !!(this._flags & 2 || this._flags & 32);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ICNode.prototype, "changed", {
        get: function () {
            return !!this.$changed;
        },
        set: function (val) {
            if (val === false) {
                this.$html = undefined;
            }
            this.$changed = val;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ICNode.prototype, "content", {
        get: function () {
            if (this.$content) {
                return this.$content;
            }
            var walker = document.createNodeIterator(this.dom, NodeFilter.SHOW_TEXT, null, false);
            for (var node = walker.nextNode(); node; node = walker.nextNode()) {
                var text = node.textContent;
                if (text.includes("%%content%%")) {
                    var split = text.regex(/^([\s\S]*?)(%%content%%)([\s\S]*?)/);
                    if (split[0] || split[2]) {
                        if (split[0]) {
                            node.parentNode.insertBefore(document.createTextNode(split[0]), node);
                        }
                        if (split[2]) {
                            node.parentNode.insertBefore(document.createTextNode(split[2]), node.nextSibling);
                        }
                        node.textContent = split[1];
                    }
                    return this.$content = node;
                }
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ICNode.prototype, "screen", {
        get: function () {
            if (this.$screen) {
                return this.$screen;
            }
            var content = this.content;
            if (content) {
                for (var parent = content.parentElement; parent; parent = parent.parentElement) {
                    if (parent.tagName === "IC-SCREEN") {
                        return this.$screen = parent;
                    }
                }
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ICNode.prototype, "dom", {
        get: function () {
            if (!this.$dom) {
                var template = createElement("template");
                template.innerHTML = this.html;
                this.$dom = template.content;
                this.$content = this.$screen = undefined;
            }
            return this.$dom;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ICNode.prototype, "pretty", {
        get: function () {
            if (!this.$pretty) {
                this.formatHtml();
            }
            return this.$pretty;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ICNode.prototype, "html", {
        get: function () {
            return this._html || ICNodeTypeHtml(this._type);
        },
        set: function (html) {
            if (html === ICNodeTypeHtml(this._type)) {
                html = "";
            }
            if (this._html !== html) {
                if (this.$html === undefined) {
                    this.$html = this._html;
                }
                this._html = html;
                this.$dom = this.$pretty = undefined;
                for (var parent = this.getParent(); parent; parent = parent.getParent()) {
                    if (parent.$dom) {
                        parent.$dom = undefined;
                    }
                }
                this.$changed = true;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ICNode.prototype, "icon", {
        get: function () {
            switch (this._type) {
                case -1:
                    return "fa-stop";
                case 0:
                    return "fa-university";
                case 1:
                    return "fa-cubes";
                case 2:
                    return "fa-database";
                case 3:
                    return "fa-folder-open";
                case 4:
                    return "fa-object-ungroup";
                case 10:
                    return "fa-object-group";
                case 11:
                    return "fa-paint-brush";
                case 12:
                    return "fa-link";
                case 13:
                    return "fa-cube";
            }
            console.error("Activity.getIcon() with invalid type:", this);
            return "fa-question-circle";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ICNode.prototype, "index", {
        get: function () {
            return this._id ? ICNodes.get(this._parent)._children.indexOf(this._id) : -1;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ICNode.prototype, "isGroup", {
        get: function () {
            return this._type < 10;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ICNode.prototype, "hasChildren", {
        get: function () {
            return !!this._children;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ICNode.prototype, "saving", {
        get: function () {
            return !!this.$save;
        },
        set: function (state) {
            this.$save = state;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ICNode.prototype, "dirty", {
        get: function () {
            return !!this.$dirty;
        },
        set: function (state) {
            this.$dirty = state;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ICNode.prototype, "watchers", {
        get: function () {
            return this.$watchers || [];
        },
        set: function (watchers) {
            this.$watchers = watchers || [];
        },
        enumerable: true,
        configurable: true
    });
    ICNode.prototype.formatHtml = function () {
        if (this.$children) {
            this.$dom = this.$screen = this.$content = this.$children = undefined;
        }
        var dom = this.dom;
        if (!dom) {
            console.warn("WARNING: No DOM available", this);
            this.$pretty = undefined;
            return;
        }
        var output = [], pretty = [], selfClosing = /^(area|base|br|col|embed|hr|img|input|keygen|link|meta|param|source|track|wbr)$/i, selfClosingSVG = /^(path|circle|ellipse|line|rect|use|stop|polyline|polygon)$/i, index = {
            "act": 1,
            "acts": 1,
            "drag": 1,
            "drop": 1,
            "option": 1,
            "select": 1,
            "text": 1,
            "toggle": 1,
            "p": 1
        }, indexes = {}, removeClass = /\s*(?:ict-[a-z0-9_\-]+|mce-[a-z0-9_\-]+|ice-[a-z0-9_\-]+)/g, removeClassAll = /\s*(?:mce-[a-z0-9_\-]+|ice-[a-z0-9_\-]+)/g, multipleActivities = dom.querySelectorAll("ic-activity,ic-activities").length > 1, walker = document.createNodeIterator(dom, NodeFilter.SHOW_TEXT | NodeFilter.SHOW_ELEMENT | NodeFilter.SHOW_COMMENT, this.$children ? function (node) {
            return node.nodeType === Node.ELEMENT_NODE && node.hasAttribute("ice") ? NodeFilter.FILTER_REJECT : NodeFilter.FILTER_ACCEPT;
        } : null, false), walk = function (node, prefix) {
            for (; node; node = walker.nextNode()) {
                switch (node.nodeType) {
                    case Node.COMMENT_NODE:
                        var commentNode = node, text = "<!-- " + commentNode.textContent.trim() + "-->";
                        output.push(text);
                        pretty.push(prefix + text);
                        break;
                    case Node.TEXT_NODE:
                        var textNode = node, text = textNode.textContent.replace(/[\u00A0-\u9999<>\&]/gim, function (i) {
                            return "&#" + i.charCodeAt(0) + ";";
                        });
                        output.push(text);
                        pretty.push(prefix + text);
                        break;
                    case Node.ELEMENT_NODE:
                        var elementNode = node, nodeName = elementNode.nodeName, tagName = nodeName.toLowerCase(), isICTag = tagName.startsWith("ic-"), attributes = "", prettyAttributes = "", state = {}, id, thisIndex = index, parentId = "", isActivity = false;
                        switch (tagName) {
                            case "ic-activity":
                            case "ic-activities":
                                isActivity = true;
                                if (tagName === "ic-activity") {
                                    id = "act";
                                }
                                else {
                                    id = "acts";
                                }
                                indexes[id + index[id]] = {
                                    "act": 1,
                                    "drag": 1,
                                    "drop": 1,
                                    "option": 1,
                                    "select": 1,
                                    "text": 1,
                                    "toggle": 1,
                                };
                                break;
                            case "ic-draggable":
                                id = "drag";
                                break;
                            case "ic-droppable":
                                id = "drop";
                                state["empty"] = !elementNode.firstElementChild;
                                break;
                            case "ic-option":
                                id = "option";
                                break;
                            case "ic-select":
                                id = "select";
                                break;
                            case "ic-text":
                                id = "text";
                                break;
                            case "ic-toggle":
                                id = "toggle";
                                break;
                            case "p":
                                id = "p";
                                break;
                            default:
                                id = null;
                                break;
                        }
                        if (isICTag) {
                            elementNode.fixState(state, index[id] || -1);
                            if (elementNode.hasAttribute("ic-rounding")) {
                                elementNode.style.width = elementNode.style.height = "";
                            }
                        }
                        if (id) {
                            if (id === "p") {
                                id += index[id]++;
                                if (elementNode.style.position === "relative") {
                                    elementNode.style.position = "";
                                }
                            }
                            else {
                                if (!isActivity) {
                                    for (var parent = elementNode.parentElement; parent; parent = parent.parentElement) {
                                        if (parent.nodeName.toLowerCase().startsWith("ic-") && /^acts?\d$/.test(parent.id)) {
                                            thisIndex = indexes[parentId = parent.id];
                                            break;
                                        }
                                    }
                                }
                                id += thisIndex[id]++;
                                if (multipleActivities) {
                                    id = parentId + id;
                                }
                            }
                            elementNode.setAttribute("id", id);
                        }
                        else if (isICTag) {
                            elementNode.removeAttribute("id");
                        }
                        for (var i = 0; i < elementNode.attributes.length; i++) {
                            var attribute = elementNode.attributes[i], name = attribute.name, value = attribute.value;
                            if (["ice", "contenteditable", "spellcheck", "readonly"].includes(name)
                                || /^(data-mce|mce-data)/.test(name)
                                || (/^(data-json|style|class)/.test(name) && !value)) {
                                continue;
                            }
                            if (name === "style") {
                                value = value.replace(/([:;])\s+/g, "$1").replace(/&/g, "&amp;").replace(/"/g, "'").replace(/;?$/, ";");
                                attributes += " " + (name + "=\"" + value + "\"");
                                prettyAttributes += " " + (name + "=\"" + value + "\"");
                            }
                            else {
                                if (name === "class") {
                                    value = value.replace(isICTag ? removeClass : removeClassAll, "").trim();
                                    if (!value) {
                                        continue;
                                    }
                                }
                                if (!isICTag || name !== "id") {
                                    attributes += " " + (name + (value ? "=\"" + value.replace(/&/g, "&amp;").replace(/\"/g, "&quot;") + "\"" : ""));
                                }
                                if (!isICTag || !["data-anchor", "data-mark", "data-percent", "data-points"].includes(name)) {
                                    prettyAttributes += " " + (name + (value ? "=\"" + value.replace(/&/g, "&amp;").replace(/\"/g, "&quot;") + "\"" : ""));
                                }
                            }
                        }
                        if (selfClosing.test(tagName) || (selfClosingSVG.test(tagName) && !elementNode.firstChild)) {
                            output.push("<" + tagName + attributes + "/>");
                            pretty.push(prefix + "<" + tagName + prettyAttributes + "/>");
                        }
                        else if (tagName === "template") {
                            var oldWalker = walker;
                            if (pretty.length && pretty.last()) {
                                pretty.push("");
                            }
                            output.push("<" + tagName + attributes + ">");
                            pretty.push(prefix + "<" + tagName + prettyAttributes + ">");
                            walker = document.createNodeIterator(node.content, NodeFilter.SHOW_TEXT | NodeFilter.SHOW_ELEMENT | NodeFilter.SHOW_COMMENT, null, false);
                            walk(walker.nextNode(), prefix + "\t");
                            output.push("</" + tagName + ">");
                            pretty.push(prefix + "</" + tagName + ">", "");
                            walker = oldWalker;
                        }
                        else {
                            var child = elementNode.firstChild;
                            if (child && (child.nodeType === Node.TEXT_NODE || child.nodeType === Node.ELEMENT_NODE || child.nodeType === Node.COMMENT_NODE)) {
                                output.push("<" + tagName + attributes + ">");
                                pretty.push(prefix + "<" + tagName + prettyAttributes + ">");
                                walk(walker.nextNode(), prefix + "\t");
                                output.push("</" + tagName + ">");
                                pretty.push(prefix + "</" + tagName + ">");
                            }
                            else {
                                output.push("<" + tagName + attributes + "></" + tagName + ">");
                                pretty.push(prefix + "<" + tagName + prettyAttributes + "></" + tagName + ">");
                            }
                        }
                        break;
                    default:
                        console.error("Error: Unknown TreeWalker node", node);
                        break;
                }
                if (!node.nextSibling) {
                    break;
                }
            }
        };
        walk(walker.nextNode(), "");
        this.$pretty = pretty.join("\n") + "\n";
        return this.html = output.join("");
    };
    ICNode.prototype.internalBuild = function (skipParent, child) {
        var _this = this;
        if (this.$children) {
            this.$dom = this.$screen = this.$content = this.$children = undefined;
        }
        var childList = !child && child !== 0 ? this.getChildrenId() : [child], dom = this.dom, content = this.content, screen = this.screen, screenOrContent = screen || content, parentNode = (screenOrContent ? screenOrContent.parentNode : null) || dom;
        if (!isBoolean(skipParent)) {
            ICNode.requireList = [];
        }
        for (var i = 0; i < dom.children.length; i++) {
            var el = dom.children[i];
            if (!el.hasAttribute("ice")) {
                el.setAttribute("ice", String(this._id));
            }
        }
        if (screenOrContent) {
            if (childList.length) {
                childList.forEach(function (id) {
                    if (skipParent === false || _this._children.includes(id)) {
                        var activity = ICNodes.get(id), clone;
                        if (!id
                            || (activity && skipParent !== false
                                && (activity.is(0)
                                    || activity.is(1)
                                    || activity.is(2)
                                    || !!(activity._flags & (2 | 32))))) {
                            return;
                        }
                        if (!activity) {
                            console.error("Error: Empty child for building", _this._id, id);
                            ICNode.requireList.pushOnce(id);
                            return;
                        }
                        if (activity.is(4) && skipParent !== false) {
                            activity.internalBuild(true);
                        }
                        clone = activity.dom.cloneNode(true);
                        for (var i = 0; i < clone.children.length; i++) {
                            var child = clone.children[i];
                            if (!child.hasAttribute("ice")) {
                                child.setAttribute("ice", String(id));
                            }
                        }
                        if (clone.children.length > 1) {
                            var div = createElement("div");
                            div.setAttribute("unwrap", "");
                            div.setAttribute("ice", String(id));
                            div.appendChild(clone);
                            clone = div;
                        }
                        if (screen && (clone.children.length !== 1 || (clone.children[0] || {}).nodeName !== "IC-SCREEN")) {
                            var el = screen.cloneNode(true), walker = document.createNodeIterator(el, NodeFilter.SHOW_TEXT, null, false), node;
                            while ((node = walker.nextNode())) {
                                if (node.textContent === "%%content%%") {
                                    node.parentNode.replaceChild(clone, node);
                                    break;
                                }
                            }
                            parentNode.insertBefore(el, screen);
                        }
                        else {
                            parentNode.insertBefore(clone, screenOrContent);
                        }
                    }
                    else {
                        console.error("Error: Trying to add a non child (" + id + ") to build.");
                    }
                });
                querySelectorAll(dom, "div[unwrap][ice]").forEach(function (group) {
                    var parentNode = group.parentNode, id = group.getAttribute("ice");
                    while (group.hasChildNodes()) {
                        if (group.firstChild.nodeType === Node.ELEMENT_NODE) {
                            group.firstChild.setAttribute("ice", id);
                        }
                        parentNode.insertBefore(group.firstChild, group);
                    }
                    parentNode.removeChild(group);
                });
            }
            parentNode.removeChild(screenOrContent);
            this.$children = true;
        }
        if (isNumber(skipParent)) {
            dom = ICNodes.get(skipParent).internalBuild(false, this._id);
        }
        else if (!skipParent && this._parent && ICNodes.get(this._parent).html) {
            dom = this.getParent().internalBuild(false, this._id);
        }
        return dom;
    };
    ICNode.prototype.loadChildren = function () {
        var _this = this;
        var want = [], resolver, promise = new Promise(function (resolve) { return resolver = resolve; }), checkNeeded = function (id) {
            if (id) {
                var activity = ICNodes.get(id);
                if (activity) {
                    activity.getChildrenId().forEach(checkNeeded);
                }
                else {
                    want.pushOnce(id);
                }
            }
        };
        this.getChildrenId().forEach(checkNeeded);
        if (!want.length) {
            resolver();
        }
        else {
            ICNodes.load(want)
                .then(function () {
                _this.loadChildren()
                    .then(function () {
                    resolver();
                });
            });
        }
        return promise;
    };
    ICNode.prototype.build = function (fakeParent) {
        var _this = this;
        var need = [], resolver, promise = new Promise(function (resolve) { return resolver = resolve; }), loadChildren = function () {
            _this.loadChildren().then(function () {
                resolver();
            });
        };
        this.getParentsId().forEach(function (id) {
            if (id && !ICNodes.has(id)) {
                need.push(id);
            }
        });
        if (need.length) {
            ICNodes.load(need).then(loadChildren);
        }
        else {
            loadChildren();
        }
        return promise.then(function () {
            return _this.internalBuild(fakeParent);
        });
    };
    ICNode.prototype.getChildren = function () {
        var i, childList = this.getChildrenId(), children = [];
        for (i = 0; childList && i < childList.length; i++) {
            children.push(ICNodes.get(childList[i]));
        }
        return children;
    };
    ICNode.prototype.getChildrenId = function () {
        return this._children.slice(0) || [];
    };
    ICNode.prototype.getParent = function () {
        return ICNodes.get(this._parent);
    };
    ICNode.prototype.getParentId = function () {
        return this._parent;
    };
    ICNode.prototype.getSiblings = function () {
        return this._id ? ICNodes.get(this._parent).getChildren() : [];
    };
    ICNode.prototype.getSiblingsId = function () {
        return this._id ? ICNodes.get(this._parent).getChildrenId() : [];
    };
    ICNode.prototype.getParents = function (andSelf) {
        var parent = this.getParent(), parents = andSelf ? [this] : [];
        while (parent) {
            parents.push(parent);
            parent = parent.getParent();
        }
        return parents;
    };
    ICNode.prototype.getParentsId = function (andSelf) {
        var parent = this.getParent(), parents = andSelf ? [this._id] : [];
        while (parent) {
            parents.push(parent._id);
            parent = parent.getParent();
        }
        return parents;
    };
    ICNode.prototype.getTemplateFolders = function () {
        var _this = this;
        var resolver = function (resolve) {
            var templates = [], load = [];
            _this.getParents(true).forEach(function (parent) {
                parent.getSiblingsId().forEach(function (id) {
                    var activity = ICNodes.get(id);
                    if (!activity) {
                        load.push(id);
                    }
                    else if (activity.is(1)
                        && !(activity._flags & 2 ||
                            (!Editor.permissions.isRoot && activity._flags & 32))) {
                        templates.push(activity);
                    }
                });
            });
            if (load.length) {
                ICNodes.load(load).then(resolver.bind(_this, resolve));
            }
            else {
                resolve(templates);
            }
        };
        return new Promise(resolver);
    };
    ICNode.prototype.getTemplates = function () {
        var _this = this;
        return this.getTemplateFolders().then(function (templates) {
            var resolver = function (resolve) {
                var themes = [], load = [], checkChildren = function (parent) {
                    parent.getChildrenId().forEach(function (id) {
                        var activity = ICNodes.get(id);
                        if (!activity) {
                            load.push(id);
                        }
                        else if (activity.is(3)) {
                            checkChildren(activity);
                        }
                        else if (activity.is(13)
                            && !(activity._flags & 2 ||
                                (!Editor.permissions.isRoot && activity._flags & 32))) {
                            themes.push(activity);
                        }
                    });
                };
                templates.forEach(function (parent) {
                    checkChildren(parent);
                });
                if (load.length) {
                    ICNodes.load(load).then(resolver.bind(_this, resolve));
                }
                else {
                    resolve(themes);
                }
            };
            return new Promise(resolver);
        });
    };
    ICNode.prototype.getThemes = function () {
        var _this = this;
        return this.getTemplateFolders().then(function (templates) {
            var resolver = function (resolve) {
                var themes = [], load = [];
                templates.forEach(function (parent) {
                    parent.getChildrenId().forEach(function (id) {
                        var activity = ICNodes.get(id);
                        if (!activity) {
                            load.push(id);
                        }
                        else if (activity.is(11)) {
                            themes.push(activity);
                        }
                    });
                });
                if (load.length) {
                    ICNodes.load(load).then(resolver.bind(_this, resolve));
                }
                else {
                    resolve(themes);
                }
            };
            return new Promise(resolver);
        });
    };
    ICNode.prototype.is = function (check) {
        return this._type === check;
    };
    ICNode.prototype.set = function (data) {
        var isEditing = Editor.currentActivity && Editor.currentActivity._id === this._id, thisData = this, newData = data, checkValidKeys = function (key, target) {
            if (key === "_html" && target.$html) {
                return !(target.$html === this[key]);
            }
            return !key.startsWith("$");
        }, copyData = function () {
            var changeHtml = false, changeName = false;
            thisData._build = newData._build;
            thisData._children = newData._children;
            thisData._created = newData._created;
            thisData._creator = newData._creator;
            thisData._date = newData._date;
            thisData._editor = newData._editor;
            thisData._files = newData._files;
            thisData._flags = newData._flags;
            thisData._parent = newData._parent;
            thisData._type = newData._type;
            if (thisData._name !== newData._name) {
                thisData._name = newData._name;
                changeName = true;
            }
            if (thisData._html !== newData._html) {
                thisData.html = newData._html;
                changeHtml = true;
            }
            thisData.saving = thisData.dirty = thisData.changed = false;
            if (changeName) {
                $.tree.add(String(thisData._id));
            }
            if (changeHtml && isEditing) {
                Editor.setActivity(thisData);
            }
        };
        if (this.saving || !this.changed || !isEditing) {
            copyData();
        }
        else if (!this.dirty && !data.equals(this, checkValidKeys)) {
            if (confirm("This screen has been updated by " + (data._editor || "someone") + ", do you wish to undo your changes?")) {
                copyData();
            }
            else {
                this.dirty = true;
            }
        }
        return this;
    };
    ICNode.privateKeys = ["$changed", "$children", "$content", "$dirty", "$dom", "$html", "$pretty", "$save", "$screen", "$watchers"];
    ICNode.requireList = [];
    return ICNode;
}());
;
fixPrototype(ICNode);
var _a, _b;
var ICNodes;
(function (ICNodes) {
    ;
    var activities = {
        0: new ICNode({
            "_id": 0,
            "_type": -1,
            "_children": []
        })
    };
    var loading = {}, loadingList = [], loadingPromises = [];
    function forEach(callback) {
        for (var id in activities) {
            callback(activities[id], parseInt(id, 10), activities);
        }
    }
    ICNodes.forEach = forEach;
    function isGroup(id) {
        return activities[id] && activities[id].isGroup;
    }
    ICNodes.isGroup = isGroup;
    function getParent(id) {
        return activities[id] && activities[id].getParent();
    }
    ICNodes.getParent = getParent;
    function getParentId(id) {
        return activities[id] && activities[id].getParentId();
    }
    ICNodes.getParentId = getParentId;
    function getParents(id, andSelf) {
        return (activities[id] && activities[id].getParents(andSelf)) || [];
    }
    ICNodes.getParents = getParents;
    function getChildren(id) {
        return (activities[id] && activities[id].getChildren()) || [];
    }
    ICNodes.getChildren = getChildren;
    function getIcon(id) {
        return activities[id] ? activities[id].icon : "fa-question-circle";
    }
    ICNodes.getIcon = getIcon;
    function getIndex(id) {
        return activities[id] ? get(id).index : -1;
    }
    ICNodes.getIndex = getIndex;
    function get(id) {
        return activities[id];
    }
    ICNodes.get = get;
    function loaded(id) {
        return !!activities[id];
    }
    ICNodes.loaded = loaded;
    function has(id) {
        return activities.hasOwnProperty("" + id);
    }
    ICNodes.has = has;
    function set(data) {
        var id = data._id;
        if (activities[id]) {
            activities[id].set(data);
        }
        else {
            activities[id] = new ICNode(data);
        }
        return activities[id];
    }
    ICNodes.set = set;
    function getNamePrefix(id, prefix) {
        var i = 0, parent = isNumber(id) ? get(id) : id;
        if (parent) {
            while (i === 1 || parent.getChildren().filter(function (activity) {
                return activity._name === prefix + (i ? " (" + i + ")" : "");
            }).length) {
                i++;
            }
        }
        return prefix = prefix + (i ? " (" + i + ")" : "");
    }
    ICNodes.getNamePrefix = getNamePrefix;
    function inherit(id) {
        var copy = new ICNode(), activity = id instanceof ICNode ? id : get(id), files = [];
        activity.getParents(true).reverse().forEach(function (parent) {
            parent.clone(copy, true);
            if (parent._files) {
                parent._files.forEach(function (filename) {
                    files.pushOnce(filename);
                });
            }
        });
        if (!activity.hasChildren) {
            delete copy._children;
        }
        if (!activity._build) {
            delete copy._build;
        }
        activity._files = files;
        return copy;
    }
    ICNodes.inherit = inherit;
    ;
    function uninherit(activity) {
        return activity.diff(inherit(activity._parent));
    }
    ICNodes.uninherit = uninherit;
    ;
    function loadParents(id) {
        Editor.ajax({
            type: "POST",
            url: "ajax.php",
            global: false,
            dataType: "json",
            data: {
                action: "get_parents",
                id: id
            },
            success: function (data) {
                if (data.error) {
                    return Editor.dialog("Error: " + data.error, "Server Error");
                }
                for (var id, i = data.length - 1; i >= 0; i--) {
                    id = data[i];
                    if (ICNodes.has(id)) {
                        data.splice(i, 1);
                        [].push.apply(data, ICNodes.get(id).getChildrenId());
                    }
                }
                ICNodes.load(loadingList = data);
            }.bind(ICNodes)
        });
    }
    ICNodes.loadParents = loadParents;
    function onLoaded(callback, data, index) {
        if (data.error) {
            return Editor.dialog("Error: " + data.error, "Server Error");
        }
        else if (data.success) {
            if (callback) {
                callback(null, -1);
            }
            return Editor.log("Success: " + data.success);
        }
        if (isArray(data)) {
            var data_array = data;
            for (var i = 0; i < data_array.length; i++) {
                onLoaded(callback, data_array[i], i);
            }
        }
        else if (isObject(data)) {
            var activity = set(data), id = activity._id, old_files = (get(data._id) || {})._files || [], new_files = activity._files || [];
            cachedNodes = null;
            delete loading[0];
            delete loading[id];
            if (callback) {
                callback(activity, index);
            }
            $.tree.add(String(id));
            old_files.forEach(function (filename) {
                if (!new_files || !new_files.includes(filename)) {
                    $.tree.remove(id + "_" + filename.replace(/^assets\/\d+\//, ""));
                }
            });
            new_files.forEach(function (filename) {
                if (!old_files || !old_files.includes(filename)) {
                    $.tree.add(id + "_" + filename.replace(/^assets\/\d+\//, ""));
                }
            });
            if (!activity._parent) {
                activities[0]._children.pushOnce(id);
                activities[0]._children.sort(function (a, b) {
                    var a_name = activities[a]._name || "", b_name = activities[b]._name || "";
                    return a_name < b_name ? -1 : a_name > b_name ? 1 : 0;
                });
            }
            if (id === Editor.findActivityId) {
                $.tree.click(String(id));
                Editor.setActivity(activity);
                Editor.findActivityId = -1;
            }
            if (loadingList.includes(id)) {
                loadingList.splice(loadingList.indexOf(id), 1);
                if (activity.isGroup) {
                    ICNodes.load(activity.getChildrenId());
                }
            }
            if (!Object.keys(loading).length) {
                loadingPromises.forEach(function (resolve) {
                    resolve(activities);
                });
                loadingPromises = [];
            }
        }
        else {
            console.error("Activity.loaded() with invalid data", data);
        }
    }
    ICNodes.onLoaded = onLoaded;
    function load(request, callback, force) {
        if (request) {
            if (!isObject(request)) {
                var id_list = (isArray(request) ? request : [request]);
                if (!force) {
                    var isWaiting = false;
                    id_list = id_list.filter(function (id, index) {
                        if (!loading[id]) {
                            if (!ICNodes.has(id) || !id) {
                                loading[id] = true;
                                return true;
                            }
                        }
                        else {
                            isWaiting = true;
                        }
                        if (callback) {
                            callback(ICNodes.get(id), index);
                        }
                    });
                    if (!id_list.length) {
                        if (isWaiting) {
                            return new Promise(function (resolve) {
                                loadingPromises.push(resolve);
                            });
                        }
                        return Promise.resolve(activities);
                    }
                }
                request = {
                    "action": "get_node",
                    "id": id_list.length === 1 ? id_list[0] : id_list
                };
            }
            return Editor.ajax({
                type: "POST",
                url: "ajax.php",
                global: !force,
                dataType: "json",
                data: request,
                success: onLoaded.bind(ICNodes, callback)
            });
        }
    }
    ICNodes.load = load;
    var cachedNodes;
    var cachedSince;
    function getCachedNodes() {
        if (!cachedNodes) {
            var activity, time = 0, nodes = [];
            for (var id in activities) {
                activity = activities[id];
                if (activities.hasOwnProperty(id) && activity._date) {
                    time = Math.max(time, Date.parse(activity._date) || 0, Date.parse(activity._created) || 0);
                    if (activity.isGroup && activity.hasChildren && loaded(activity.getChildrenId().first())) {
                        nodes.pushOnce(activity._id);
                    }
                }
            }
            cachedNodes = nodes;
            cachedSince = time;
        }
        return cachedNodes;
    }
    function poll() {
        var nodes = getCachedNodes();
        if (Editor.online && nodes.length && cachedSince) {
            ICNodes.load({
                "action": "poll_nodes",
                id: nodes,
                since: new Date(cachedSince).format()
            }, null, true);
        }
    }
    window.setInterval(poll, Config.pollInterval);
})(ICNodes || (ICNodes = {}));
;
var Templates;
(function (Templates) {
    function clickType() {
        var $this = $(this);
        $("#ic_add_activity")
            .children("div")
            .children("div")
            .css("display", "")
            .eq($this.data("index"))
            .css("display", "block");
        $this.addClass("active").siblings().removeClass("active");
    }
    function addDefaultActivity(event) {
        var activity = new ICNode(event.data);
        activity._parent = parseInt($.tree.closest(), 10);
        console.log("Adding activity: ", activity);
        if ($("#ic_add_activity.ui-dialog-content").dialog("destroy").length) {
            $("#aba-button-save").button("enable");
            alert("Please remember that this activity is not saved until you hit the save button!");
        }
        Editor.setActivity(activity);
        $.tree.cancel();
        if (!Editor.change) {
            Editor.change = true;
        }
        return false;
    }
    function create() {
        var i, j, k, folder, section, $section, $subsection, template, $type, index, active, nodeId = parseInt($.tree.currentId, 10), parents = ICNodes.getParents(nodeId), sources = [], folders = ICNodes.getChildren(3), $add = $("#ic_add_activity"), $list = $add.children("ul"), $divs = $add.children("div"), makeTemplate = function (activity) {
            var i, data = {
                _id: 0,
                _name: activity._name,
                "type": activity._type
            };
            for (i in activity) {
                if (i[0] !== "_" && activity.hasOwnProperty(i)) {
                    data[i] = activity[i];
                }
            }
            return $.make("div", "template", $.make("span", $.make("span", "=" + activity._name)), $.make("div", $.make("img", "src=assets/" + activity._id + "/_screenshot.png", "data-id=" + activity._id)))
                .on("click", null, data, addDefaultActivity);
        };
        active = $list.children(".active").text();
        $list.empty();
        $divs.empty();
        for (i = 0; i < parents.length; i++) {
            sources.push.apply(sources, parents[i].getChildren().filter(function (activity) {
                return activity.is(1);
            }));
        }
        console.log("templates:", sources);
        for (i = 0; i < sources.length; i++) {
            folders = sources[i].getChildren();
            for (j = 0; j < folders.length; j++) {
                folder = folders[j];
                if (folder.is(3)) {
                    continue;
                }
                $section = $subsection = null;
                index = $list.children().length;
                $.make("li", "=" + folder._name, active === folder._name ? "active" : null).data("index", index).appendTo($list).on("click", clickType);
                $type = $.make("div").appendTo($divs);
                for (j = 0; folder._children && j < folder._children.length; j++) {
                    section = ICNodes.get(folder._children[j]);
                    if (section.is(3)) {
                        $subsection = null;
                        for (k = 0; k < section._children.length; k++) {
                            template = ICNodes.get(section._children[k]);
                            if (template.is(10) || template.is(12)) {
                                if (!$subsection) {
                                    $subsection = $type.children().filter(function () {
                                        return $(">p", this).text() === section._name;
                                    });
                                    if (!$subsection.length) {
                                        $subsection = $.make("div", $.make("p", "=" + section._name), $.make("hr")).appendTo($type);
                                    }
                                }
                                makeTemplate(template).appendTo($subsection);
                            }
                        }
                    }
                    else {
                        $section = $section || $.make("div", $.make("p", "=" + folder._name), $.make("hr")).prependTo($type);
                        makeTemplate(section).appendTo($section);
                    }
                }
            }
        }
        $list.children().sort(function (a, b) {
            var a_text = a.textContent, b_text = b.textContent;
            return a_text > b_text ? 1 : a_text < b_text ? -1 : 0;
        }).appendTo($list);
        clickType.call($list.children(active ? ".active" : undefined)[0]);
        $("#ic_add_activity").dialog({
            width: window.innerWidth * 0.75,
            height: window.innerHeight * 0.75,
            modal: true,
            close: function () {
                $("#ic_add_activity").dialog("destroy");
            },
            buttons: {
                "Cancel": function () {
                    $("#ic_add_activity").dialog("destroy");
                }
            }
        });
    }
})(Templates || (Templates = {}));
;
var Editor;
(function (Editor) {
    function getRule(el) {
        if (!el) {
            return;
        }
        if (el.icRule) {
            return el.icRule;
        }
        var walker = document.createNodeIterator(el, NodeFilter.SHOW_ELEMENT, null, false), rule = {
            require: [],
            permit: true
        };
        for (var node = walker.nextNode(); node; node = walker.nextNode()) {
            if (node.nodeName.startsWith("IC-")) {
                console.log("looking for ", node.nodeName, Editor.rules);
                var currentRule = Editor.rules[node.nodeName];
                if (currentRule) {
                    var require = currentRule.require, permit = currentRule.permit;
                    if (permit === true) {
                        permit = Object.keys(Editor.rules);
                    }
                    if (node !== el) {
                        for (var parent = node.parentElement; parent !== el; parent = parent.parentElement) {
                            var nodeName = parent.nodeName;
                            if (nodeName.startsWith("IC-")) {
                                if (require && require.includes(nodeName)) {
                                    require = undefined;
                                }
                            }
                            else if (permit === false || (isArray(permit) && !permit.includes(nodeName))) {
                                console.error("Invalid parent for widget", el, node);
                                return undefined;
                            }
                        }
                    }
                    if (isArray(require)) {
                        rule.require.pushOnce.apply(rule.require, require);
                    }
                    if (isArray(permit)) {
                        if (isArray(rule.permit)) {
                            rule.permit = rule.permit.intersect(permit);
                        }
                        else {
                            rule.permit = permit.slice();
                        }
                    }
                }
                else {
                    console.error("Unable to find rule for '" + node.nodeName + "'", node, el);
                }
            }
        }
        return el.icRule = rule;
    }
    Editor.getRule = getRule;
    function isValidWidget(el, parent) {
        var rule = getRule(el);
        if (rule) {
            var require = rule.require, permit = rule.permit;
            if (permit === true) {
                permit = Object.keys(Editor.rules);
            }
            if (!parent) {
                parent = el.parentElement;
            }
            for (; parent; parent = parent.parentElement) {
                var nodeName = parent.nodeName;
                if (nodeName.startsWith("IC-")) {
                    if (require && require.includes(nodeName)) {
                        require = undefined;
                        permit = true;
                    }
                    else if (permit === false || (isArray(permit) && !permit.includes(nodeName))) {
                        return false;
                    }
                }
            }
            return !require || !require.length || require.includes("IC-SCREENS") || require.includes("IC-SCREEN");
        }
        return true;
    }
    Editor.isValidWidget = isValidWidget;
})(Editor || (Editor = {}));
;
var ActivityFile = (function () {
    function ActivityFile(file) {
        ActivityFile.list.push(this);
        if (file instanceof File) {
            this.name = file.name;
            this.size = file.size;
            this.type = file.type;
        }
        else {
            this.name = file;
        }
        this.nameServer = this.name;
        this.type = this.type || ActivityFile.mimetypes[this.name.regex(/\.([^\.]+)$/)] || "application/octet-stream";
    }
    ActivityFile.prototype.delete = function (cancel) {
        this.deleted = !cancel;
    };
    ActivityFile.prototype.rename = function (newName) {
        var old = ActivityFile.find(newName);
        if (old) {
            old.delete();
        }
        this.name = newName;
    };
    ActivityFile.clear = function () {
        this.list.length = 0;
    };
    ActivityFile.find = function (name) {
        var i, file, list = this.list;
        for (i = 0; i < list.length; i++) {
            file = list[i];
            if (!file.deleted && file.name === name) {
                return file;
            }
        }
        return null;
    };
    ActivityFile.get = function (name) {
        var file = this.find(name);
        if (file.data) {
            return "data:" + file.type + ";base64," + file.data;
        }
        return Config.server + "asstes/" + Editor.currentActivity._id + "/" + file.nameServer;
    };
    ActivityFile.list = [];
    ActivityFile.mimetypes = {
        "css": "text/css",
        "scss": "text/scss",
        "htm": "text/html",
        "html": "text/html",
        "js": "text/javascript",
        "bmp": "image/bmp",
        "gif": "image/gif",
        "jpg": "image/jpeg",
        "jpeg": "image/jpeg",
        "png": "image/png",
        "svg": "image/svg+xml",
        "m4a": "audio/mpeg",
        "mp3": "audio/mpeg3",
        "avi": "video/avi",
        "m4v": "video/mpeg",
        "mov": "video/quicktime",
    };
    return ActivityFile;
}());
var Editor;
(function (Editor) {
    var defaultSettings = {
        "size": {
            "left": 15,
            "preview": 25,
            "right": 15
        },
        "panel": {
            "left": [
                "projects",
                "users",
                "components"
            ],
            "main": [
                "preview",
                "edit"
            ],
            "preview": [
                "source",
                "explore",
                "properties"
            ],
            "right": [
                "marking"
            ],
        },
        "active": {
            "left": "projects",
            "main": "preview",
            "preview": "source",
            "right": "properties"
        },
        "projects": {
            "files": false,
            "recycle": false
        },
        "options": {
            "image_list": false,
            "resource_list": "thumbs",
            "resource_type": "all",
            "resource_sort": "alpha",
            "resource_ext": "",
            "preview_marks": false,
            "advanced": false
        }
    };
    Editor.settings = {};
    function getSetting(path, def) {
        return Editor.settings.getTree(path, def);
    }
    Editor.getSetting = getSetting;
    function setSetting(path, value) {
        ic.setImmediate("saveSettings", saveSettings);
        return Editor.settings.setTree(path, value);
    }
    Editor.setSetting = setSetting;
    function loadSettings() {
        var data = JSON.parse(localStorage.getItem("ic-config") || "{}");
        if (typeof data === "object" && Object.keys(data).equals(Object.keys(defaultSettings))) {
            Editor.settings = data;
        }
        else {
            Editor.settings = $.extend(true, {}, defaultSettings);
        }
        return Editor.settings;
    }
    Editor.loadSettings = loadSettings;
    ;
    function saveSettings() {
        ic.setImmediate("saveSettings");
        Editor.settings.size["left"] = parseInt(Editor.panel_layout.get("left").size);
        Editor.settings.size["preview"] = parseInt(Editor.panel_layout.get("preview").size);
        Editor.settings.size["right"] = parseInt(Editor.panel_layout.get("right").size);
        ["left", "main", "preview", "right"].forEach(function (side) {
            Editor.settings.panel[side] = [];
            Editor.settings.active[side] = "";
            $("#layout_layout_panel_" + side + ">.w2ui-panel-tabs .w2ui-tab").each(function (i, el) {
                var panel = this.innerText.trim().toLowerCase();
                Editor.settings.panel[side].push(panel);
                if (!i || this.classList.contains("active")) {
                    Editor.settings.active[side] = panel;
                }
            });
        });
        localStorage.setItem("ic-config", JSON.stringify(Editor.settings));
    }
    Editor.saveSettings = saveSettings;
    ;
    loadSettings();
})(Editor || (Editor = {}));
;
var Button = (function () {
    function Button(selector, perm, click, enable) {
        this.selector = selector;
        this.perm = perm;
        this.click = click;
        this.enable = enable;
        Button.list.push(this);
        $(document).on("click", selector, this, Button.onClick);
    }
    Button.update = function () {
        Button.list.forEach(function (button) {
            var $button = $(button.selector);
            if (button.perm && !Editor.permissions.has(button.perm)) {
                $button.hide();
            }
            else {
                $button.toggleClass("disabled", !button.enable(Editor.currentActivity));
            }
        });
    };
    Button.onClick = function (event) {
        var $this = $(this), button = event.data, activity = Editor.currentActivity;
        if (button.enable(activity)) {
            if ($this.hasClass("radio")) {
                if ($this.hasClass("active")) {
                    return false;
                }
                $this.addClass("active").siblings().removeClass("active");
            }
            button.click(activity, event);
        }
    };
    Button.list = [];
    return Button;
}());
new Button("#ic_button_advanced", 1, function () {
    var classList = document.body.classList, isAdvanced = classList.contains("advanced");
    Editor.setSetting("options.advanced", !isAdvanced);
    if (isAdvanced) {
        classList.remove("advanced");
    }
    else {
        classList.add("advanced");
    }
}, function () {
    return true;
});
new Button("#ic_button_backup", 1, function () {
    $().w2popup({
        title: "<i class=\"fa fa-hdd-o\"></i> Backup",
        body: "<iframe id=\"backup\" style=\"width:100%;height:99%;background:white;\" src=\"" + Config.server + "backup.php\"></iframe>",
        style: "",
        width: 500,
        height: 300
    });
}, function (activity) {
    return Editor.permissions.isRoot && Config.backup;
});
new Button("#ic_button_export", 1 | 4, function () {
    function build_activity(id, event) {
        var activity = ICNodes.get(id), $row = $("#build_" + activity._id);
        if (event.ctrlKey) {
            console.info("Building all");
            $(event.target).closest("table").find(".fa-tasks").click();
            return;
        }
        activity.build().then(function (build) {
            var hadFirstScreen, treeWidgets = ["IC-SCREENS", "IC-SCREEN", "IC-ACTIVITIES", "IC-ACTIVITY", "IC-DROPPABLE", "IC-SELECT"], treeWidgetsSelector = treeWidgets.join(","), jsonWidgetsSelector = treeWidgets.wrap(",", "", "[data-json]"), screens = build.querySelectorAll("ic-screen").length, currentScreen = 0, styleScript = /^(style|script)$/i, walker = document.createNodeIterator(build, NodeFilter.SHOW_ELEMENT | NodeFilter.SHOW_TEXT | NodeFilter.SHOW_COMMENT, null, false);
            $row.addClass("disabled")
                .find("td:nth-child(2)")
                .addClass("disabled");
            for (var node = walker.nextNode(); node; node = walker.nextNode()) {
                switch (node.nodeType) {
                    case Node.COMMENT_NODE:
                        walker.previousNode();
                        (node.parentElement || build).removeChild(node);
                        break;
                    case Node.TEXT_NODE:
                        var textContent = node.textContent, parent_1 = node.parentElement, text = textContent.replace(/%%(title|screens?)%%/g, function ($0, $1) {
                            switch ($1) {
                                case "title":
                                    return activity._name;
                                case "screen":
                                    return String(currentScreen);
                                case "screens":
                                    return String(screens);
                            }
                        });
                        if (parent_1 && !styleScript.test(parent_1.nodeName) && parent_1.closest("IC-SCREEN")) {
                            text = text.compress("uri");
                        }
                        if (textContent !== text) {
                            node.textContent = text;
                        }
                        break;
                    case Node.ELEMENT_NODE:
                        var el = node, tagName = el.tagName;
                        el.removeAttribute("editor-img");
                        el.removeAttribute("editor-title");
                        el.removeAttribute("ice");
                        el.removeAttribute("data-template");
                        if (tagName.startsWith("IC-")) {
                            if (!el.hasAttribute("data-json")) {
                                el.setAttribute("data-json", "{}");
                            }
                            if (tagName === "IC-SCREEN") {
                                if (!hadFirstScreen) {
                                    hadFirstScreen = true;
                                }
                                else {
                                    el.style.display = "none";
                                }
                                currentScreen++;
                                el.setAttribute("x", "");
                            }
                            if (el.hasAttribute("data-json")) {
                                var closestTreeWidget = el.parentElement && el.parentElement.closest(treeWidgetsSelector);
                                if (closestTreeWidget) {
                                    var indexKeys = closestTreeWidget.icIndexKeys;
                                    if (!indexKeys) {
                                        indexKeys = closestTreeWidget.icIndexKeys = {
                                            "IC-SCREEN": 1,
                                            "IC-ACTIVITIES": 1,
                                            "IC-ACTIVITY": 1,
                                            "IC-DROPPABLE": 1,
                                            "IC-DRAGGABLE": 1,
                                            "IC-SELECT": 1,
                                            "IC-OPTION": 1,
                                            "IC-TOGGLE": 1,
                                            "IC-TEXT": 1,
                                        };
                                    }
                                    indexKeys[tagName] = indexKeys[tagName] || 1;
                                    el.icIndex = indexKeys[tagName]++;
                                }
                            }
                        }
                        else if (tagName === "P" && /^p\d+$/.test(el.id)) {
                            el.removeAttribute("id");
                        }
                        break;
                }
            }
            for (var node = walker.previousNode(); node; node = walker.previousNode()) {
                switch (node.nodeType) {
                    case Node.ELEMENT_NODE:
                        var el = node, tagName = el.tagName;
                        if (tagName.startsWith("IC-") && el.hasAttribute("data-json")) {
                            var closestTreeWidget = el.parentElement && el.parentElement.closest(jsonWidgetsSelector);
                            if (closestTreeWidget) {
                                var json = JSON.parse(el.getAttribute("data-json")), lcName = tagName.substring(3).toLowerCase();
                                if (Object.keys(json).length) {
                                    var parentJson = JSON.parse(closestTreeWidget.getAttribute("data-json"));
                                    parentJson[lcName] = parentJson[lcName] || {};
                                    parentJson[lcName][el.icIndex] = json;
                                    closestTreeWidget.setAttribute("data-json", JSON.stringify(parentJson));
                                }
                                el.removeAttribute("data-json");
                            }
                            else {
                                el.setAttribute("data-json", el.getAttribute("data-json").compress("uri"));
                            }
                        }
                }
            }
            var dom = createElement("html"), parent = build.querySelector("head") || build, meta = createElement("meta"), date = Date.format ? "Infuze Creator (" + Date.format("Y-m-d H:i") + ")" : "";
            meta.setAttribute("name", "creator");
            meta.setAttribute("content", "Infuze Limited");
            parent.insertBefore(meta, parent.firstChild);
            meta = createElement("meta");
            if (date) {
                meta.setAttribute("name", "generator");
                meta.setAttribute("content", date);
                parent.insertBefore(meta, parent.firstChild);
            }
            if (!build.querySelector("meta[charset]")) {
                meta = createElement("meta");
                meta.setAttribute("charset", "utf-8");
                parent.insertBefore(meta, parent.firstChild);
            }
            dom.appendChild(build);
            var html = dom.innerHTML, length = html.length, compressed = html.compress("uri"), compressedLength = compressed.length;
            console.info("Building", id, ", length:", length, ", compressed:", compressedLength);
            Editor.ajax({
                type: "POST",
                url: "ajax.php",
                global: false,
                dataType: "json",
                data: {
                    "PHP_SESSION_UPLOAD_PROGRESS": "upload",
                    "action": "publish",
                    "id": id,
                    "html": compressed
                },
                success: function (data) {
                    Editor.ajax({
                        type: "POST",
                        url: "ajax.php",
                        global: false,
                        dataType: "json",
                        data: {
                            action: "get_builds",
                            "id": id
                        },
                        success: showPrevious
                    });
                }
            });
            $row.children("td:first-child").animate({ foo: 100 }, {
                duration: compressedLength / 5,
                step: function (percent) {
                    percent = percent * 0.9 + 5;
                    $(this).css("background", "linear-gradient(to right,darkgreen " + percent + "%,#171717 " + (percent + 0.01) + "%)");
                },
                complete: function () {
                }
            });
        });
    }
    function addBuild(activity) {
        var path = Config.server + "publish/" + String(activity._id).crc32();
        $.make("tr", "#build_" + activity._id, $.make("td", "colspan=3", $.make("i", "fa fa-minus"), $.make("label", "=" + activity._name)).on({
            click: function () {
                $(this).closest("tr").next("tr.date").toggleClass("hidden");
                $(this).parent().find(".fa-chevron-right,.fa-chevron-down").toggleClass("fa-chevron-right").toggleClass("fa-chevron-down");
            }
        }), $.make("td", $.make("a", "class=fa fa-tasks").on("click", build_activity.bind(window, activity._id))), $.make("td", $.make("a", "href=" + path + "/", "target=_blank", "class=fa fa-external-link-square")), $.make("td", $.make("a", "href=" + path + ".zip", "download=" + activity._name + ".zip", "class=fa fa-download"))).appendTo($("#build_popup>table"));
    }
    function showPrevious(data) {
        Object.keys(data).forEach(function (id) {
            var $row = $("#build_" + id), bestDate, builds = data[id];
            if (builds.length) {
                var $tr = $("#build_" + id + "+.date"), name_1 = $row.find("a[download]").attr("download").replace(".zip", "");
                $row.find(".fa-minus").removeClass("fa-minus").addClass("fa-chevron-right");
                if ($tr.length) {
                    $tr.empty();
                }
                else {
                    $tr = $.make("tr", "date hidden").insertAfter($row);
                }
                var $td = $.make("td", "colspan=6").appendTo($tr), $div = $.make("div").appendTo($td), $table_1 = $.make("table").appendTo($div);
                builds.reverse().forEach(function (dateString) {
                    var date = new Date(dateString), path = Config.server + "publish/" + String(id).crc32() + "." + date.format("Y-m-d_H-i-s");
                    if (!bestDate || date > bestDate) {
                        bestDate = date;
                    }
                    $.make("tr", "date", $.make("td", "colspan=4", $.make("label", "=" + date.format("D M d Y H:i:s"))), $.make("td", $.make("a", "href=" + path + "/", "target=_blank", "class=fa fa-external-link-square")), $.make("td", $.make("a", "href=" + path + ".zip", "download=" + name_1 + "." + date.format("Y-m-d_H-i-s") + ".zip", "class=fa fa-download"))).appendTo($table_1);
                });
            }
            $row
                .removeClass("disabled")
                .attr("title", bestDate ? bestDate.format("D M d Y H:i:s") : "")
                .children("td:first-child")
                .finish()
                .css({
                background: "",
                cursor: "pointer"
            });
        });
    }
    $().w2popup({
        title: "<i class=\"fa fa-cloud-download\"></i> Build",
        body: "<div id=\"build_popup\"><table><thead><tr><td colspan=3>&nbsp;</td><td>Build</td><td>Launch</td><td>Download</td></tr></thead><tbody></tbody></table></div>",
        style: "",
        width: 500,
        height: 500,
        onOpen: function () {
            ic.setImmediate("build", function () {
                var activity = ICNodes.get(Editor.currentActivity._id);
                if (activity.is(3)) {
                    activity.getChildren().forEach(addBuild);
                }
                else {
                    addBuild(activity);
                }
                Editor.ajax({
                    type: "POST",
                    url: "ajax.php",
                    global: false,
                    dataType: "json",
                    data: {
                        action: "get_builds",
                        "id": activity.is(3) ? activity.getChildrenId() : activity._id
                    },
                    success: showPrevious
                });
            });
        }
    });
}, function (activity) {
    return !Editor.change && activity && (activity.is(2) || activity.is(3) || activity.is(10));
});
new Button("#ic_button_replicate", 1, function () {
    $().w2popup({
        title: "<i class=\"fa fa-hdd-o\"></i> Replicate",
        body: "<iframe id=\"replicate\" style=\"width:100%;height:99%;background:white;\" src=\"" + Config.server + "replicate.php\"></iframe>",
        style: "",
        width: 500,
        height: 300
    });
}, function (activity) {
    return Editor.permissions.isRoot && Config.replicate;
});
new Button("#ic_button_report", 1 | 4, function (activity) {
    $().w2popup({
        title: "<i class=\"fa fa-eye\"></i> Report",
        body: "<div id=\"report_popup\"><table><thead><tr><td>ID</td><td>Parent</td><td>Visible</td><td>Flags</td><td>Type</td><td>Name</td></tr></thead><tbody id=\"report_body\"></tbody></table></div>",
        style: "",
        width: 1000,
        height: 800,
        resizable: true
    });
    activity.loadChildren()
        .then(function () {
        var tableBody = $("#report_body")[0], makeRow = function (activity) {
            if (activity && activity._id) {
                var html = activity._html || "";
                $.make("tr", "#report_" + activity._id, $.make("td", "=" + activity._id), $.make("td", "=" + activity._parent), $.make("td", "=" + (activity.isHidden || activity.getParent().isHidden ? "hidden" : "")), $.make("td", "="
                    + (activity._flags & 2 ? "!" : "")
                    + (activity._flags & 32 ? "?" : "")
                    + (html.includes("<audio") ? "a" : "")
                    + (html.includes("<ic-droppable") ? "d" : "")
                    + (html.includes("<ic-toggle") ? "x" : "")
                    + (html.includes("<img") ? "i" : "")
                    + (html.includes("<ic-select") ? "s" : "")
                    + (html.includes("<ic-text") ? "t" : "")), $.make("td", "=" + ICNodeTypeName[activity._type]), $.make("td", "=" + activity._name)).appendTo(tableBody);
                activity.getChildren().forEach(makeRow);
            }
        };
        console.log("report", activity, activity._id);
        makeRow(activity);
    });
}, function (activity) {
    return activity && activity.isGroup;
});
new Button("#ic_button_save", 1 | 4 | 16 | 64 | 128, function (activity) {
    if (activity && Editor.change) {
        if (Editor.SourcePanel && Editor.SourcePanel.instance && Editor.SourcePanel.instance.errors) {
            alert("There is currently an error in the source, please fix it in order to save!");
            return;
        }
        if (Editor.currentActivity.is(0) && !/[&?]expert\b/.test(location.search)) {
            alert("Saving Organisations is currently disabled!");
            Editor.fileCache = {};
            Editor.change = false;
            Button.update();
            return;
        }
        var compile = 0, compileTheme = !!Editor.fileCache["theme.scss"], localCache = {}, cachedTime = new Date().getTime(), fn = function () {
            ICNodes.load({
                action: "set_node",
                id: activity._id,
                parent: activity._parent,
                data: activity,
                files: Editor.fileCache
            }, function (activity) {
                console.log("Saved");
                Editor.fileCache = {};
                Editor.change = false;
                Button.update();
                if (activity) {
                    activity.saving = false;
                    if ($.tree.currentId !== String(activity._id)) {
                        location.hash = String(activity._id);
                    }
                }
            });
        };
        console.log("Saving: project(" + activity._parent + "), activity(" + activity._id + ") = ", activity);
        activity.saving = true;
        for (var name in Editor.fileCache) {
            if (name.endsWith(".scss")) {
                var scss = atob(Editor.fileCache[name]);
                if (name[0] === "_") {
                    if (compileTheme) {
                        continue;
                    }
                    compileTheme = true;
                    name = "theme.scss";
                    scss = "@import '" + name + "';";
                }
                compile++;
                console.log("Converting sass file:", name);
                window.Sass.importer(function (request, done) {
                    if (request.current) {
                        var file = request.current, currentPath = Config.server + "assets/" + activity._id + "/";
                        if (!file.endsWith(".scss")) {
                            file += ".scss";
                        }
                        if (!file.includes("/assets/")) {
                            file = currentPath + file;
                        }
                        if (file.includes(currentPath)) {
                            var filename = file.replace(/^.*?\/assets\/\d+\//i, "");
                            if (Editor.fileCache[filename]) {
                                done({
                                    content: atob(Editor.fileCache[filename])
                                });
                                return;
                            }
                            if (localCache[filename]) {
                                done({
                                    content: localCache[filename]
                                });
                                return;
                            }
                        }
                        if (!file.startsWith("http")) {
                            file = location.href.replace(/[^\/]*$/, "") + file;
                        }
                        $.ajax({
                            url: file + "?_=" + cachedTime,
                            dataType: "text"
                        }).done(function (data) {
                            localCache[name.replace(/^.*?\/assets\/\d+\//i, "")] = data;
                            done({
                                content: data
                            });
                        }).fail(function () {
                            console.warn("Compiling scss, ajax error:", file, request);
                            done({
                                content: ""
                            });
                        });
                    }
                    else {
                        console.warn("Compiling scss, unknown request:", request);
                        done();
                    }
                });
                window.Sass.compile(scss, {
                    style: window.Sass.style.compressed
                }, function (result) {
                    if (result.status) {
                        console.warn("Sass compilation error:", result);
                        alert("Sass compilation error:\n\n" + JSON.stringify(result));
                    }
                    else {
                        var prefixed = result.text ? window.autoprefixer.process(result.text) : { css: "", messages: [] }, messages = prefixed.messages;
                        if (messages.length) {
                            console.warn("Autoprefixer error:", messages.join("\n"));
                            alert("Autoprefixer error:\n\n" + messages.join("\n"));
                        }
                        else {
                            Editor.fileCache[name.replace(/\.scss$/i, ".css")] = btoa(prefixed.css);
                        }
                    }
                    if (!--compile) {
                        fn();
                    }
                });
            }
        }
        if (!compile) {
            fn();
        }
    }
}, function (activity) {
    return activity && Editor.change;
});
$(window).on("keydown", function (event) {
    if ((event.ctrlKey || event.metaKey) && /s/i.test(String.fromCharCode(event.which))) {
        $("#ic_button_save").click();
        event.preventDefault();
        return false;
    }
});
new Button("#ic_file_label", 1 | 256, function (activity) {
}, function (activity) {
    if (!this.started) {
        this.started = true;
        $(document).on("change", "input[type='file']", function (event) {
            var input = this, i, file, reader, confirm = [], fileData = {}, files = this.files;
            console.log(event, files);
            for (i = 0; i < files.length; i++) {
                file = files[i];
                if (/\.min\.js$/i.test(file.name)) {
                    continue;
                }
                if (Editor.currentActivity._files.includes("assets/" + Editor.currentActivity._id + "/" + file.name)) {
                    confirm.push(file.name);
                }
                fileData[file.name] = reader = new FileReader();
                reader.onloadend = function () {
                    var fn = function () {
                        for (var name in fileData) {
                            Editor.fileCache[name] = (fileData[name].result || "").replace(/^.*?,/, "");
                            if (/\.scss$/i.test(name)) {
                                delete Editor.fileCache[name.replace(/\.scss$/i, ".css")];
                            }
                        }
                        Editor.change = true;
                        Button.update();
                        if (Editor.imagePopup) {
                            ic.setImmediate("image_popup", function () {
                                Editor.imagePopup.close();
                                Editor.imagePopup = undefined;
                                $("#image_bank_list").attr("id", "");
                                Editor.openImageBank();
                            });
                        }
                        input.value = "";
                    };
                    console.log("onloadend", confirm);
                    for (var name in fileData) {
                        if (fileData[name].readyState < 2) {
                            return;
                        }
                    }
                    if (confirm.length) {
                        Editor.dialog("Are you sure you wish to replace " + (confirm.length > 1 ? "<br/>" : "") + confirm.wrap(",<br/>", "'<span class=\"mono\">", "</span>'") + "?<br/>This cannot be undone!", "Confirm Replace", fn);
                    }
                    else {
                        fn();
                    }
                };
                reader.readAsDataURL(file);
            }
            return false;
        });
    }
    return !!activity;
});
var Editor;
(function (Editor) {
    var PanelEvent;
    (function (PanelEvent) {
        PanelEvent[PanelEvent["STARTUP"] = 0] = "STARTUP";
        PanelEvent[PanelEvent["LOADED"] = 1] = "LOADED";
        PanelEvent[PanelEvent["ACTIVITY"] = 2] = "ACTIVITY";
        PanelEvent[PanelEvent["DOM_UPDATED"] = 3] = "DOM_UPDATED";
        PanelEvent[PanelEvent["SELECT_WIDGET"] = 4] = "SELECT_WIDGET";
        PanelEvent[PanelEvent["DRAGSTART"] = 5] = "DRAGSTART";
        PanelEvent[PanelEvent["DRAGGING"] = 6] = "DRAGGING";
        PanelEvent[PanelEvent["DROPPED"] = 7] = "DROPPED";
    })(PanelEvent = Editor.PanelEvent || (Editor.PanelEvent = {}));
    function getPanelSide(name, def) {
        var panels = Editor.getSetting("panel", {});
        Object.keys(panels).forEach(function (side) {
            if (Array.isArray(panels[side]) && panels[side].includes(name)) {
                def = side;
            }
        });
        if (panels[def] && !panels[def].includes(name)) {
            panels[def].push(name);
        }
        return def;
    }
    var Panel = (function () {
        function Panel(name, perm, side) {
            this.name = name;
            this.perm = perm;
            this.side = side;
            if (perm && !Editor.permissions.has(perm)) {
                throw "No permission";
            }
            Panel.panels.pushOnce(this);
            if (["left", "main", "preview", "right"].includes(side)) {
                this.side = side = getPanelSide(name, side);
            }
            var template = getElementById("template_" + name);
            Editor.panels[side].appendChild(template.content);
            this.$ = $(this.el = Editor.panels[side].lastElementChild);
            template.parentNode.removeChild(template);
        }
        Panel.prototype.startup = function () { };
        ;
        Panel.prototype.onClick = function (event) {
            var panel = this;
            panel.$.show().siblings().hide();
            ic.setImmediate("saveSettings", Editor.saveSettings);
        };
        ;
        Panel.startup = function () {
            ["left", "main", "preview", "right"].forEach(function (side) {
                var tabs = w2ui["layout_" + side + "_tabs"], active = Editor.settings.active[side];
                if (!Panel.find(active)) {
                    active = null;
                }
                (Editor.settings.panel[side] || []).forEach(function (name) {
                    var panel = Panel.find(name);
                    if (panel) {
                        active = active || name;
                        tabs.add({
                            id: name,
                            caption: name.ucfirst(),
                            onClick: panel.onClick.bind(panel)
                        });
                    }
                });
                tabs.select(active);
                var activePanel = Panel.find(active);
                if (activePanel) {
                    activePanel.$.show().siblings().hide();
                }
            });
            Panel.panels.forEach(function (panel) {
                panel.startup();
            });
            var layout = w2ui["layout"];
            if (Editor.permissions.has(1 | 128)) {
                layout.set("preview", { hidden: false });
                layout.set("right", { hidden: false });
            }
            layout.refresh();
        };
        Panel.forEach = function (callback) {
            this.panels.forEach(callback);
        };
        Panel.trigger = function (event) {
            var data = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                data[_i - 1] = arguments[_i];
            }
            var args = arguments, result = undefined;
            Panel.panels.forEach(function (panel) {
                result = panel.on.apply(panel, args) || result;
            });
            return result;
        };
        Panel.triggerDomUpdated = function () {
            Panel.trigger(3);
        };
        Panel.triggerActivity = function () {
            Panel.trigger(2);
        };
        Panel.find = function (search) {
            var panel;
            for (var i = 0; i < this.panels.length; i++) {
                panel = this.panels[i];
                if (panel.name === search) {
                    return panel;
                }
            }
            return null;
        };
        Panel.panels = [];
        return Panel;
    }());
    Editor.Panel = Panel;
    ;
    $(window).one("resize", function () {
        $("body").css("opacity", 1);
    });
    $(window).on({
        resize: function () {
            $("#edit_frame,#size_frame").hide();
        }
    });
})(Editor || (Editor = {}));
;
var Editor;
(function (Editor) {
    var BottomPanel = (function (_super) {
        __extends(BottomPanel, _super);
        function BottomPanel() {
            var _this = _super.call(this, "footer", 0, "bottom") || this;
            if (BottomPanel.instance) {
                throw new Error("Error: Instantiation failed: Use BottomPanel.instance instead of new.");
            }
            Object.defineProperty(BottomPanel, "instance", {
                value: _this
            });
            return _this;
        }
        BottomPanel.prototype.on = function (event, args) {
            switch (event) {
            }
        };
        ;
        return BottomPanel;
    }(Editor.Panel));
    Editor.BottomPanel = BottomPanel;
})(Editor || (Editor = {}));
;
var Editor;
(function (Editor) {
    function haveSameAttributes(attribute) {
        var _this = this;
        var name = attribute.name.toLowerCase();
        if (name.startsWith("ic-")) {
            return attribute.value === this.getAttribute(name);
        }
        if (name === "style") {
            return attribute.value.split(";").every(function (style) {
                if (!style || style.indexOf(":") < 0) {
                    return true;
                }
                var _a = style.match(/^\s*(.*?)\s*:\s*(.*)\s*$/), all = _a[0], key = _a[1], value = _a[2];
                return _this.style.hasOwnProperty(key) && _this.style.getPropertyValue(key) == value;
            });
        }
        return true;
    }
    Editor.REMOVETEMPLATES = ["data-template", "data-optional", "data-repeat"];
    function isSameTemplate(element, template) {
        var repeating = false;
        if (element && element.nodeType !== Node.ELEMENT_NODE) {
            element = element.nextElementSibling;
        }
        if (template && template.nodeType !== Node.ELEMENT_NODE) {
            template = template.nextElementSibling;
        }
        while (element && template) {
            if (element.nodeName !== template.nodeName
                || !Array.prototype.every.call(template.attributes, haveSameAttributes, element)
                || template.firstElementChild && !isSameTemplate(element.firstElementChild, template.firstElementChild)) {
                if (template.hasAttribute("data-optional")) {
                    template = template.nextElementSibling;
                    continue;
                }
                else if (repeating) {
                    repeating = false;
                    template = template.nextElementSibling;
                    if (!element) {
                        break;
                    }
                    continue;
                }
                else {
                    return (element && !template) || false;
                }
            }
            element = element.nextElementSibling;
            if (element && template.hasAttribute("data-repeat")) {
                repeating = true;
            }
            else {
                template = template.nextElementSibling;
                while (!element && template && template.hasAttribute("data-optional")) {
                    template = template.nextElementSibling;
                }
            }
        }
        return !template;
    }
    function countNodes(node) {
        var count = 0;
        while (node) {
            count += 1 + countNodes(node.firstElementChild) + node.attributes.length;
            node = node.nextElementSibling;
        }
        return count;
    }
    function findTemplate(element) {
        var best, bestLength;
        components
            .filter(function (template) { return template.content.firstElementChild && isSameTemplate(element, template.content.firstElementChild); })
            .forEach(function (template) {
            var length = template.icNodeChildCount;
            if (!length) {
                template.icNodeChildCount = length = countNodes(template.content.firstElementChild);
            }
            console.log("match", element, template);
            if (!best || length > bestLength) {
                best = template;
                bestLength = length;
            }
        });
        return best;
    }
    Editor.findTemplate = findTemplate;
    var components;
    var lastActivity;
    var ComponentsPanel = (function (_super) {
        __extends(ComponentsPanel, _super);
        function ComponentsPanel() {
            var _this = _super.call(this, "components", 1, "left") || this;
            if (ComponentsPanel.instance) {
                throw new Error("Error: Instantiation failed: Use ComponentsPanel.instance instead of new.");
            }
            Object.defineProperty(ComponentsPanel, "instance", {
                value: _this
            });
            return _this;
        }
        ComponentsPanel.prototype.on = function (event, args) {
            switch (event) {
            }
        };
        ;
        return ComponentsPanel;
    }(Editor.Panel));
    Editor.ComponentsPanel = ComponentsPanel;
    function addTemplate(template) {
        var icon = $(template).data("icon");
        if (!components.find(function (node) { return node.title === template.title; }) && icon) {
            $.make("article", "component", $.make("div", icon ? "background-image:url(" + icon + ")" : template.content.cloneNode(true)), $.make("h3", "=" + template.title))
                .data("template", template)
                .appendTo("#ic_components")
                .draggable({
                appendTo: "body",
                containment: "window",
                cursor: "grabbing",
                helper: "clone",
                opacity: 0.7,
                revert: true,
                revertDuration: 200,
                scroll: false,
                zIndex: 100,
                start: function (event, ui) {
                    $("iframe").css("pointer-events", "none");
                    Editor.Panel.trigger(5, $(this).data("template"));
                },
                drag: function (event, ui) {
                    Editor.Panel.trigger(6, $(this).data("template"), { top: event.clientY, left: event.clientX });
                },
                stop: function (event, ui) {
                    $("iframe").css("pointer-events", "");
                    Editor.Panel.trigger(7, $(this).data("template"), { top: event.clientY, left: event.clientX });
                }
            });
        }
        components.push(template);
    }
    function changeActivity() {
        var activity = Editor.currentActivity;
        if (activity && lastActivity !== activity) {
            lastActivity = activity;
            Editor.currentActivity.build().then(function (dom) {
                $("script[src^='activity/']", dom).each(function (index, scriptNode) {
                    var activityPath = (scriptNode.getAttribute("src") || "").regex(/^(activity\/(?:build|version\/latest|version\/v[\d\.]+)\/)activity\.(?:min\.)?js$/);
                    if (activityPath && Editor.currentRulesPath !== activityPath) {
                        fetch(activityPath + "rules.json")
                            .then(function (response) {
                            if (response.status >= 200 && response.status < 300) {
                                return response;
                            }
                            else {
                                var error = new Error(response.statusText);
                                throw error;
                            }
                        })
                            .then(function (response) {
                            return response.json();
                        })
                            .then(function (data) {
                            Editor.currentRulesPath = activityPath;
                            Editor.rules = data;
                        });
                    }
                });
                var componentNodes = [3], panel = getElementById("ic_components");
                $("link[rel='stylesheet'][href^='../assets/']", dom).each(function (index, linkNode) {
                    var activityId = (linkNode.getAttribute("href") || "").regex(/^\.\.\/assets\/(\d+)\/theme\.css$/);
                    if (activityId) {
                        componentNodes.unshiftOnce(activityId);
                    }
                });
                while (panel && panel.firstChild) {
                    panel.removeChild(panel.firstChild);
                }
                ;
                components = [];
                componentNodes.forEach(function (activityId) {
                    ICNodes.load(activityId).then(function () {
                        var node = ICNodes.get(activityId);
                        querySelectorAll(node.dom, "template").forEach(function (template) { return addTemplate(template); });
                    });
                });
            });
        }
    }
    Editor.changeActivity = changeActivity;
})(Editor || (Editor = {}));
;
var Editor;
(function (Editor) {
    var updating;
    var MediaType;
    (function (MediaType) {
        MediaType[MediaType["UNKNOWN"] = 0] = "UNKNOWN";
        MediaType[MediaType["IMAGE"] = 1] = "IMAGE";
        MediaType[MediaType["VIDEO"] = 2] = "VIDEO";
        MediaType[MediaType["AUDIO"] = 3] = "AUDIO";
    })(MediaType = Editor.MediaType || (Editor.MediaType = {}));
    ;
    function findKey(map, value) {
        var key;
        map.forEach(function (v, k) {
            if (v === value) {
                key = k;
            }
        });
        return key;
    }
    Editor.findKey = findKey;
    function updateHtml() {
        ic.setImmediate("editpanel.sethtml", function () {
            EditPanel.instance.setHtml();
            overlay.update();
        });
    }
    var inject;
    (function (inject) {
        var ic_menu;
        var ic_start_widget;
        function ic_edit(id) {
            var types = [
                "ic-activity",
                "ic-audio",
                "ic-box",
                "ic-button",
                "ic-draggable",
                "ic-droppable",
                "ic-text",
                "ic-toggle"
            ], types2 = [];
            types.forEach(function (name) {
                types2.push(name + "[" + name + "|" + name + "-display" + "]");
            });
            tinymce.init({
                selector: "#" + id,
                auto_focus: id,
                plugins: "paste",
                custom_elements: types.wrap(",", "~"),
                extended_valid_elements: "@[id|class|style|data-json|ic-state|ic-columns|ic-border|ic-padding|ic-margin|ic-*|ic-*-*]," + types2.join(","),
                fontsize_formats: "60% 75% 88% 100% 120% 150% 200%",
                toolbar: "bold italic underline strikethrough subscript superscript | fontsizeselect | removeformat",
                remove_trailing_brs: false,
                paste_postprocess: function (plugin, args) {
                    console.log(args.node);
                },
                paste_as_text: true,
                menubar: false,
                inline: true,
                setup: function (editor) {
                    editor.on("change cut paste keyup undo redo", function (event) {
                        if (event.type !== "keyup" || event.charCode) {
                            window.parent.ic.setImmediate("tinymce", function () {
                                window.parent.Editor.EditPanel.instance.setHtml();
                            });
                        }
                    });
                    editor.on("selectionchange", function (event) {
                        editor.bodyElement.dispatchEvent(new KeyboardEvent("keyup", {}));
                    });
                    editor.on("blur", function (event) {
                        window.getSelection().removeAllRanges();
                        window.parent.Editor.EditPanel.instance.el.querySelector("iframe").style.pointerEvents = "";
                    });
                }
            });
        }
        inject.ic_edit = ic_edit;
        function ic_action(action, elements) {
            console.log("action", action, elements);
            var update;
            switch (action) {
                case "box_split":
                case "box_split_letter":
                    elements.forEach(function (element) {
                        var el, rx = action === "box_split"
                            ? /(<.*?>|&[a-z]+;|\s+|[\w]+|[\d]+|[^\s\w\d<])/gi
                            : /(<.*?>|&[a-z];|[^<])/g, walker = document.createNodeIterator(element, NodeFilter.SHOW_TEXT, null, false);
                        while ((el = walker.nextNode())) {
                            el.textContent.match(rx).forEach(function (word) {
                                var toggle = document.createElement("ic-toggle");
                                toggle.textContent = word;
                                toggle.setAttribute("ic-toggle", "multirange");
                                toggle.setAttribute("ic-toggle-display", "text");
                                el.parentElement.insertBefore(toggle, el);
                            });
                            el.parentElement.removeChild(el);
                        }
                        ;
                    });
                    update = true;
                    break;
            }
            if (update) {
                window.parent.ic.setImmediate("tinymce", function () {
                    window.parent.Editor.EditPanel.instance.setHtml();
                });
            }
        }
        inject.ic_action = ic_action;
        function ic_context(event) {
            if (ic_menu && ic_menu.parentNode) {
                if (event.button === 2 || event.type === "mouseup") {
                    var action = event.target.getAttribute("ice-act");
                    document.body.removeChild(ic_menu);
                    ic_menu = null;
                    if (action) {
                        ic_action(action, [].slice.call(document.querySelectorAll(".ice-select")));
                    }
                }
            }
            else if (event.target.closestWidget) {
                var widget = event.target.closestWidget();
                if (widget && widget.closest("ic-screen")) {
                    switch (event.type) {
                        case "contextmenu":
                            var selected = document.querySelectorAll(".ice-select");
                            if (!selected.length) {
                                widget.classList.add("ice-select");
                                selected = [widget];
                            }
                            if (!event.ctrlKey) {
                                var target_1, columns = selected.length === 1 ? selected[0].getAttribute("ic-columns") || 0 : -1;
                                [].some.call(selected, function (element) {
                                    var name = element.tagName.replace("IC-", "").toLowerCase();
                                    if (!target_1) {
                                        target_1 = name;
                                    }
                                    else if (target_1 !== name) {
                                        target_1 = null;
                                        return true;
                                    }
                                });
                                ic_menu = document.createElement("div");
                                ic_menu.id = "ic_context_menu";
                                ic_menu.innerHTML = "<div>"
                                    + "<h1>" + target_1.ucfirst() + "</h1>"
                                    + "<div></div>"
                                    + "<ul>"
                                    + (columns >= 0 ? "<li><a>Columns<span>&rtrif;</span><ul>"
                                        + "<li>1</li>"
                                        + "<li>2</li>"
                                        + "<li>3</li>"
                                        + "<li>4</li>"
                                        + "<li>5</li>"
                                        + "</ul></a></li>"
                                        : "")
                                    + "</ul>"
                                    + (target_1 === "box" ? "<ul ice-for=\"box\">"
                                        + "<li><span>ic-box</span></li>"
                                        + "<li><a ice-act=\"box_split\">Split into Toggles (per word)</a></li>"
                                        + "<li><a ice-act=\"box_split_letter\">Split into Toggles (per letter)</a></li>"
                                        + "<li><hr></li>"
                                        + "<li><a ice-act=\"a3\">Example 1</a></li>"
                                        + "<li><a ice-act=\"a4\">Example 2</a></li>"
                                        + "</ul>"
                                        : "")
                                    + (target_1 === "toggle" ? "<ul ice-for=\"toggle\">"
                                        + "<li><span>ic-toggle</span></li>"
                                        + "</ul>"
                                        + "</div>"
                                        : "");
                                ic_menu.style.paddingLeft = String(event.pageX - 2) + "px";
                                ic_menu.style.paddingTop = String(event.pageY - 2) + "px";
                                document.body.appendChild(ic_menu);
                                event.preventDefault();
                                return false;
                            }
                            break;
                    }
                }
            }
        }
        inject.ic_context = ic_context;
        function ic_startup(id) {
            ic_menu = null;
            document.addEventListener("contextmenu", ic_context, false);
            document.addEventListener("mousedown", ic_context, true);
            document.addEventListener("mouseup", ic_context, true);
            [].forEach.call(document.querySelectorAll("p[id]"), function (el) {
                var closest = el.closest("[ice]");
                if (!closest || closest.getAttribute("ice") != id) {
                    el.removeAttribute("id");
                }
            });
        }
        inject.ic_startup = ic_startup;
    })(inject || (inject = {}));
    ;
    var currentImage;
    function openImageBank() {
        var setImagePath = function () {
            currentImage.src = $(this).data("src");
            Editor.imagePopup.close();
            Editor.imagePopup = undefined;
            updateHtml();
            Editor.currentEditWidget = currentImage.closestWidget();
            querySelectorAll(".ice-select").forEach(function (element) {
                if (element !== Editor.currentEditWidget) {
                    element.classList.remove("ice-select");
                }
            });
            Editor.currentEditWidget.classList.add("ice-select");
            overlay.update();
        }, $popup = $("<div>");
        console.log("open image bank", this);
        Editor.imagePopup = $().w2popup({
            title: "<i class=\"fa fa-file-image-o\"></i> Select Image",
            body: "<div id=\"image_bank_popup\" class=\"ext\"><span><span><i class=\"fa fa-th-large active\"></i><i class=\"fa fa-list\"></i></span><div><input id=\"image_bank_search\" placeholder=\"type to filter\" type=\"text\"/></div><label class=\"fa fa-folder-open\"><input type=\"file\"/></label></span><div id=\"image_bank_list\"></div></div>",
            style: "",
            width: window.innerWidth * 0.8,
            height: window.innerHeight * 0.8,
            onOpen: function () {
                ic.setImmediate("image_bank", function () {
                    $("#image_bank_list").append($popup.children());
                    $("#image_bank_popup").toggleClass("thumbs", Editor.getSetting("options.image_list")).toggleClass("list", !Editor.getSetting("options.image_list"));
                    $("#image_bank_popup>span>span>i")
                        .on("click", function () {
                        Editor.setSetting("options.image_list", $("#image_bank_popup").toggleClass("list thumbs").hasClass("list"));
                    });
                    $("#image_bank_search")
                        .focus()
                        .on("keyup change", function () {
                        var value = this.value.trim().toLowerCase();
                        $("#image_bank_list").children().each(function (index, el) {
                            var image = this;
                            image.style.display = image.innerText.toLowerCase().indexOf(value) >= 0 ? "" : "none";
                        });
                    });
                });
            }
        });
        Editor.currentActivity.getTemplateFolders().then(function (nodes) {
            var files = [], id = Editor.currentActivity._id, src = currentImage.getAttribute("src"), $target = $("#image_bank_list").add($popup).first(), getFileType = function (path) {
                switch (path.regex(/\.([^\.]+)$/).toLowerCase()) {
                    case "gif":
                    case "jpg":
                    case "png":
                    case "svg":
                        return MediaType.IMAGE;
                    case "mp3":
                    case "m4a":
                    case "ogg":
                        return MediaType.AUDIO;
                    case "mp4":
                    case "m4v":
                        return MediaType.VIDEO;
                }
                return MediaType.UNKNOWN;
            };
            nodes.pushOnce.apply(nodes, Editor.currentActivity.getParents(true));
            nodes.pushOnce.apply(nodes, Editor.currentActivity.getSiblings());
            console.log(nodes);
            nodes.forEach(function (node) {
                files.pushOnce.apply(files, node._files || []);
            });
            for (var file in Editor.fileCache) {
                if (Editor.fileCache.hasOwnProperty(file) && getFileType(file) === MediaType.IMAGE) {
                    var filePath = "../assets/" + id + "/" + file, fileName = file.regex(/([^\/]+)$/), fileExt = fileName.regex(/\.([^\.]+)$/).toLowerCase();
                    files.remove(filePath);
                    $("<a class=\"new\"><div data-type=\"" + fileExt + "\" style=\"background-image:url('data:image/" + fileExt + ";base64," + Editor.fileCache[fileName] + "');\"></div><h3>" + fileName.replace(/\.[^\.]+$/, "") + "</h3></a>")
                        .toggleClass("active", src === filePath)
                        .data("src", filePath)
                        .on("click", setImagePath)
                        .appendTo($target);
                }
            }
            files.naturalCaseSort().forEach(function (path) {
                if (getFileType(path) === MediaType.IMAGE) {
                    var filePath = "../" + path, fileName = path.regex(/([^\/]+)$/), fileExt = fileName.regex(/\.([^\.]+)$/).toLowerCase();
                    $("<a><div data-type=\"" + fileExt + "\" style=\"background-image:url('" + filePath + "');\"></div><h3>" + fileName.replace(/\.[^\.]+$/, "") + "</h3></a>")
                        .toggleClass("active", src === filePath)
                        .data("src", filePath)
                        .on("click", setImagePath)
                        .appendTo($target);
                }
            });
        });
    }
    Editor.openImageBank = openImageBank;
    function stopPropagation(event) {
        if (!event.target.closest(".mce-tinymce,.mce-panel")) {
            event.stopPropagation();
        }
    }
    var focusElement, templates = new Map(), wizards = new Map();
    var MouseMode;
    (function (MouseMode) {
        MouseMode[MouseMode["SELECT"] = 0] = "SELECT";
        MouseMode[MouseMode["MOVE"] = 1] = "MOVE";
        MouseMode[MouseMode["NW"] = 2] = "NW";
        MouseMode[MouseMode["N"] = 3] = "N";
        MouseMode[MouseMode["NE"] = 4] = "NE";
        MouseMode[MouseMode["W"] = 5] = "W";
        MouseMode[MouseMode["E"] = 6] = "E";
        MouseMode[MouseMode["SW"] = 7] = "SW";
        MouseMode[MouseMode["S"] = 8] = "S";
        MouseMode[MouseMode["SE"] = 9] = "SE";
    })(MouseMode || (MouseMode = {}));
    var overlay;
    (function (overlay_1) {
        var elements = new Map(), boundingBoxCache = new Map(), startX, startY;
        function add(el) {
            if (el && !elements.get(el)) {
                var position = window.getComputedStyle(el).position === "absolute", container = document.querySelector("#ic_edit>.ic_edit"), overlay_2 = container.appendChild(getElementById("template_overlay" + (position ? "_resize" : "")).content.cloneNode(true).firstChild), templateElement = templates.get(el), options = ((templateElement && templateElement.getAttribute("data-template")) || "").toLowerCase().split(/[\s,|]+/);
                if (!el.nodeName.startsWith("IC-")) {
                    overlay_2.classList.add("element");
                }
                if (getComputedStyle(el).position.toLowerCase() === "absolute") {
                    overlay_2.classList.add("move");
                }
                if (el.style.width || el.style.height) {
                    overlay_2.classList.add("resizable");
                }
                if (options.includes("autowidth") || options.includes("autoheight") || el.style.height === "auto") {
                    [].forEach.call(overlay_2.querySelectorAll(".nw,.n,.ne,.w,.e,.sw,.s"), function (el) { el.style.display = "none"; });
                }
                elements.set(el, overlay_2);
                update();
            }
        }
        overlay_1.add = add;
        function find(element) {
            if (element) {
                return findKey(elements, element);
            }
        }
        overlay_1.find = find;
        function select(element, only) {
            var realElement;
            elements.forEach(function (overlay, el) {
                overlay.classList.remove("passive");
                if (element === overlay || element === el) {
                    realElement = el;
                    overlay.classList.add("active");
                    console.log("active:", templates.get(el), el);
                }
                else {
                    if (only) {
                        overlay.classList.remove("active");
                    }
                }
            });
            if (only && realElement) {
                var parent_2 = realElement, size_1, move_1;
                while (parent_2 && !move_1 && !size_1) {
                    var overlay_3 = elements.get(parent_2);
                    if (overlay_3) {
                        var classList = overlay_3.classList;
                        if (!size_1 && classList.contains("resizable")) {
                            overlay_3.classList.add("passive");
                            size_1 = true;
                        }
                        if (!move_1 && classList.contains("move")) {
                            overlay_3.classList.add("passive");
                            move_1 = true;
                        }
                    }
                    parent_2 = parent_2.parentElement;
                }
                return elements.get(realElement);
            }
        }
        overlay_1.select = select;
        function get() {
            var active = [];
            elements.forEach(function (overlay, el) {
                if (overlay.classList.contains("active")) {
                    active.push(el);
                }
            });
            return active;
        }
        overlay_1.get = get;
        function remove(el) {
            var overlay = elements.get(el);
            if (overlay) {
                elements.delete(el);
                overlay.parentElement.removeChild(overlay);
                update();
                Editor.Panel.trigger(4, el);
            }
        }
        overlay_1.remove = remove;
        function clear() {
            elements.forEach(function (overlay, el) {
                overlay.parentElement.removeChild(overlay);
            });
            elements.clear();
            boundingBoxCache.clear();
        }
        overlay_1.clear = clear;
        function getSpacingRect() {
            if (elements.size) {
                var rect_1 = {
                    top: Number.NEGATIVE_INFINITY,
                    left: Number.NEGATIVE_INFINITY,
                    right: Number.POSITIVE_INFINITY,
                    bottom: Number.POSITIVE_INFINITY,
                    width: Number.POSITIVE_INFINITY,
                    height: Number.POSITIVE_INFINITY
                };
                elements.forEach(function (overlay, el) {
                    if (overlay.classList.contains("active")) {
                        var box = boundingBoxCache.get(el), parentRect = boundingBoxCache.get(el.offsetParent);
                        rect_1.top = Math.max(rect_1.top, parentRect.top - box.top);
                        rect_1.left = Math.max(rect_1.left, parentRect.left - box.left);
                        rect_1.right = Math.min(rect_1.right, parentRect.right - box.right);
                        rect_1.bottom = Math.min(rect_1.bottom, parentRect.bottom - box.bottom);
                        rect_1.width = Math.min(rect_1.width, box.width);
                        rect_1.height = Math.min(rect_1.height, box.height);
                    }
                });
                return rect_1;
            }
            return {
                top: 0,
                left: 0,
                right: 0,
                bottom: 0,
                width: 0,
                height: 0
            };
        }
        overlay_1.getSpacingRect = getSpacingRect;
        function start(clientX, clientY) {
            startX = clientX;
            startY = clientY;
            boundingBoxCache.clear();
            elements.forEach(function (overlay, el) {
                if (overlay.classList.contains("active")) {
                    var parent_3 = el.offsetParent;
                    if (!boundingBoxCache.get(parent_3)) {
                        var style = getComputedStyle(parent_3), borderBox = style.boxSizing === "border-box", box = parent_3.getBoundingClientRect(), left = box.left + (borderBox ? parseFloat(style.paddingLeft) : 0), right = box.right - (borderBox ? parseFloat(style.paddingRight) : 0), top_1 = box.top + (borderBox ? parseFloat(style.paddingTop) : 0), bottom = box.bottom - (borderBox ? parseFloat(style.paddingBottom) : 0);
                        boundingBoxCache.set(parent_3, {
                            left: left,
                            right: right,
                            top: top_1,
                            bottom: bottom,
                            width: right - left,
                            height: bottom - top_1
                        });
                    }
                    if (!boundingBoxCache.get(el)) {
                        boundingBoxCache.set(el, el.getBoundingClientRect());
                    }
                }
            });
        }
        overlay_1.start = start;
        function move(clientX, clientY, singleCall) {
            if (singleCall) {
                start(0, 0);
            }
            var rect = getSpacingRect(), deltaX = Math.range(rect.left, clientX - startX, rect.right), deltaY = Math.range(rect.top, clientY - startY, rect.bottom);
            elements.forEach(function (overlay, el) {
                if (overlay.classList.contains("active")) {
                    var elBox = boundingBoxCache.get(el), parentBox = boundingBoxCache.get(el.offsetParent), style = el.style;
                    style.left = ((elBox.left - parentBox.left + deltaX) * 100 / parentBox.width) + "%";
                    style.top = ((elBox.top - parentBox.top + deltaY) * 100 / parentBox.height) + "%";
                }
            });
            update();
        }
        overlay_1.move = move;
        function size(mode, clientX, clientY) {
            var rect = getSpacingRect(), deltaX = 0, deltaY = 0, deltaWidth = 0, deltaHeight = 0;
            switch (mode) {
                case 2:
                case 5:
                case 7:
                    deltaWidth = -(deltaX = Math.range(rect.left, clientX - startX, rect.width));
                    break;
                case 4:
                case 6:
                case 9:
                    deltaWidth = Math.range(-rect.width, clientX - startX, rect.right);
                    break;
            }
            switch (mode) {
                case 2:
                case 3:
                case 4:
                    deltaHeight = -(deltaY = Math.range(rect.top, clientY - startY, rect.height));
                    break;
                case 7:
                case 8:
                case 9:
                    deltaHeight = Math.range(-rect.height, clientY - startY, rect.bottom);
                    break;
            }
            elements.forEach(function (overlay, el) {
                var classList = overlay.classList;
                if ((classList.contains("active") || classList.contains("passive")) && classList.contains("resizable") && overlay.getElementsByClassName("dragging")) {
                    var elBox = boundingBoxCache.get(el), parentBox = boundingBoxCache.get(el.offsetParent), style = el.style, options = (overlay.getAttribute("data-template") || "").toLowerCase().split(/[\s,|]+/), noP = !el.getElementsByTagName("p").length, scrollWidth = void 0;
                    style.left = ((elBox.left - parentBox.left + deltaX) * 100 / parentBox.width) + "%";
                    if (!options.includes("autowidth")) {
                        scrollWidth = 10;
                        if (noP) {
                            style.width = "auto";
                            scrollWidth = el.scrollWidth;
                        }
                        style.width = (Math.max(scrollWidth || 10, elBox.width + deltaWidth) * 100 / parentBox.width) + "%";
                    }
                    style.top = ((elBox.top - parentBox.top + deltaY) * 100 / parentBox.height) + "%";
                    if (!(options.includes("autoheight") || style.height === "auto")) {
                        var scrollHeight = 0;
                        if (noP || scrollWidth) {
                            style.height = "auto";
                            scrollHeight = el.scrollHeight;
                        }
                        style.height = (Math.max(scrollHeight || 10, elBox.height + deltaHeight) * 100 / parentBox.height) + "%";
                    }
                }
            });
            update();
        }
        overlay_1.size = size;
        function internalUpdate() {
            elements.forEach(function (overlay, el) {
                var box = el.getBoundingClientRect(), style = overlay.style;
                style.top = box.top + "px";
                style.left = box.left + "px";
                style.width = box.width + "px";
                style.height = box.height + "px";
            });
        }
        function update() {
            ic.setImmediate("panel_edit_overlay_update", internalUpdate);
        }
        overlay_1.update = update;
    })(overlay = Editor.overlay || (Editor.overlay = {}));
    ;
    var EditPanel = (function (_super) {
        __extends(EditPanel, _super);
        function EditPanel() {
            var _this = _super.call(this, "edit", 1 | 64, "main") || this;
            _this.onScroll = function (event) {
                var startX = event.clientX, startY = event.clientY, contentsBox = _this.contents.getBoundingClientRect(), left = startX - contentsBox.left, top = startY - contentsBox.top, iframe = _this.el.querySelector("iframe"), doc = iframe.contentDocument, win = iframe.contentWindow, el = doc && doc.elementFromPoint(left, top), deltaY = event.deltaY;
                while (el) {
                    if (/auto|scroll/.test(win.getComputedStyle(el).overflow)) {
                        if (deltaY < 0 && el.scrollTop
                            || deltaY > 0 && el.scrollTop + el.clientHeight < el.scrollHeight) {
                            el.scrollTop += deltaY;
                            overlay.update();
                            return;
                        }
                    }
                    el = el.parentElement;
                }
            };
            _this.onKeyDown = function (event) {
                var ctrlKey = event.ctrlKey, delta = event.shiftKey ? (ctrlKey ? Number.MAX_VALUE : 100) : (ctrlKey ? 20 : 1);
                switch (event.key) {
                    case "Backspace":
                    case "Delete":
                        var elements = overlay.get();
                        if (elements.length && confirm("Are you sure you wish to delete the selected component" + (elements.length > 1 ? "s" : "") + "?")) {
                            elements.forEach(function (el) {
                                el.parentElement.removeChild(el);
                            });
                            overlay.clear();
                            updateHtml();
                        }
                        break;
                    case "ArrowLeft":
                        overlay.move(-delta, 0, true);
                        break;
                    case "ArrowUp":
                        overlay.move(0, -delta, true);
                        break;
                    case "ArrowRight":
                        overlay.move(delta, 0, true);
                        break;
                    case "ArrowDown":
                        overlay.move(0, delta, true);
                        break;
                    default:
                        return;
                }
                event.preventDefault();
            };
            _this.createWizard = function (element, template, parent) {
                console.log("createWizard", element, template, parent);
                if (!wizards.has(element)) {
                    var options = (template.getAttribute("data-template") || "").split(","), doc = template.content, $contents = void 0, hasRepeat = options.includes("repeat"), $wizard = $.make("div", "template" + (hasRepeat ? " repeat" : ""), $.make("div", $.make("p", "=" + template.getAttribute("title")), $contents = $.make("div")))
                        .data("element", element)
                        .on("click", function (event) {
                        var block = event.target.closest(".template");
                        if (!block.classList.contains("active")) {
                            Array.from(wizards.keys()).forEach((function (wizard) {
                                if (wizards.get(wizard) === block) {
                                    overlay.select(wizard, !event.ctrlKey);
                                }
                            }));
                            _this.activeWizards();
                        }
                        event.stopImmediatePropagation();
                    }), wizard = $wizard[0];
                    if (options.includes("repeat")) {
                        $wizard.addClass("sort");
                        $wizard.prepend($.make("i", "fa fa-stack", $.make("i", "fa fa-stack-1x fa-circle"), $.make("i", "fa fa-stack-1x fa-minus-circle")), $.make("div", "delete", "=&nbsp;Delete?&nbsp;"));
                        $wizard.append($.make("i", "fa fa-" + (element.hasAttribute("ic-random") ? "random" : "bars")));
                    }
                    if (doc.querySelector("ic-toggle")) {
                        $wizard.addClass("shrink");
                        $contents.append($.make("label", $.make("span", "=Correct"), $.make("input", "type=checkbox", /"answer":"checked"/.test(element.getAttribute("data-json")) ? "checked=" : null)
                            .data({
                            "type": "answer",
                            "element": element
                        })
                            .on("change", function (event) {
                            Editor.MarkingPanel.instance.onOptionChange.call(this, event, true);
                            updateHtml();
                        })));
                    }
                    if (options.includes("text")) {
                        $wizard.addClass("grow").removeClass("shrink");
                        $contents.append($.make("label", $.make("span", "=Content"), $.make("p", "editable", "=" + element.innerHTML)));
                    }
                    if (!$wizard.hasClass(".grow")
                        && !$wizard.hasClass(".shrink")
                        && Array.prototype.find.call(doc.childNodes, function (node) { return node.nodeName === "IC-BOX"; })
                        && !doc.querySelector("ic-box>*")) {
                        $wizard.addClass("title");
                    }
                    console.log("options", options, doc.childNodes, Array.prototype.find.call(doc.childNodes, function (node) { return node.nodeName === "IC-BOX"; }), doc.querySelector("ic-box>*"));
                    wizards.set(element, wizard);
                    if (parent) {
                        var parentElement = wizards.get(parent).firstElementChild;
                        if (parentElement.tagName.toLowerCase() !== "div") {
                            parentElement = parentElement.nextElementSibling.nextElementSibling;
                        }
                        parentElement.lastElementChild.appendChild(wizard);
                    }
                    else {
                        $("#ic_properties").append(wizard);
                    }
                }
            };
            _this.editTextWizards = function () {
                $("#ic_properties .fa-bars,#ic_properties .fa-random")
                    .on("dblclick", function (event) {
                    var $target = $(event.target), $template = $target.closest(".template"), element = $template.data("element");
                    if (element) {
                        var enable_1 = element.hasAttribute("ic-random"), $siblings = event.ctrlKey ? $target : $target.closest(".ui-sortable").find(">div>.ui-sortable-handle");
                        $siblings.each(function (index, template) {
                            var $template = $(template).closest(".template"), element = $template.data("element");
                            if (enable_1) {
                                element.removeAttribute("ic-random");
                                $(template).addClass("fa-bars").removeClass("fa-random");
                            }
                            else {
                                element.setAttribute("ic-random", "");
                                $(template).addClass("fa-random").removeClass("fa-bars");
                            }
                        });
                        updateHtml();
                    }
                })
                    .closest(".template").parent().sortable({
                    handle: ".fa-bars",
                    tolerance: "pointer",
                    items: ">.sort",
                    forcePlaceholderSize: true,
                    start: function (event, ui) {
                        ui.placeholder.height(ui.helper[0].scrollHeight);
                    },
                    change: function (event, ui) {
                        var element = findKey(wizards, ui.item[0]), elementParent = element.parentElement, elementNext = element.nextSibling, swap = findKey(wizards, ui.placeholder.next()[0]) || null, swapParent = swap && swap.parentElement, swapNext = swap && swap.nextSibling;
                        if (!swap || elementParent === swapParent) {
                            elementParent.insertBefore(element, element === swap ? null : swap || null);
                        }
                        else {
                            if (elementNext !== swap) {
                                elementParent.insertBefore(swap, elementNext);
                            }
                            if (swapNext !== element) {
                                swapParent.insertBefore(element, swapNext);
                            }
                        }
                        overlay.update();
                        updateHtml();
                    }
                });
                tinymce.init({
                    selector: "p.editable",
                    plugins: "paste",
                    fontsize_formats: "60% 75% 88% 100% 120% 150% 200%",
                    toolbar: "bold italic underline strikethrough subscript superscript | fontsizeselect | removeformat",
                    remove_trailing_brs: false,
                    paste_as_text: true,
                    menubar: false,
                    branding: false,
                    statusbar: false,
                    inline: true,
                    setup: function (editor) {
                        editor.on("change cut paste keyup undo redo", function (event) {
                            var target = event.target;
                            if (target.bodyElement) {
                                target = target.bodyElement;
                            }
                            if (target instanceof HTMLElement) {
                                var element = $(target).closest(".template").data("element"), html = tinymce.activeEditor.getContent();
                                if (element.innerHTML !== html) {
                                    $(element).html(html);
                                    overlay.update();
                                    updateHtml();
                                }
                            }
                        });
                    }
                });
            };
            _this.clearWizards = function () {
                Array.from(wizards.values()).forEach(function (element) {
                    element.parentElement && element.parentElement.removeChild(element);
                });
                wizards.clear();
            };
            _this.activeWizards = function () {
                var active = overlay.get();
                Array.from(wizards.keys()).forEach(function (element) {
                    var wizard = wizards.get(element);
                    if (active.includes(element)) {
                        wizard.classList.add("active");
                    }
                    else {
                        wizard.classList.remove("active");
                    }
                });
            };
            _this.onMouseDown = function (event) {
                var target = event.target, classList = target.classList, mode = 1, startX = event.clientX, startY = event.clientY, ice = target.closest("[ice]"), isOverlay = classList.contains("overlay"), isActive = isOverlay && classList.contains("active"), isMove = isActive && classList.contains("move"), isResize = classList.contains("resize"), isContainer = classList.contains("ic_edit");
                if (ice && ice.getAttribute("ice") !== String(Editor.currentActivity._id)) {
                    overlay.clear();
                    console.log("Not clicking on current activity", ice);
                    return;
                }
                if (isOverlay && !isActive) {
                    overlay.select(target, !event.ctrlKey);
                    _this.activeWizards();
                    Editor.Panel.trigger(4, target);
                }
                else if (isContainer) {
                    var contentsBox = _this.contents.getBoundingClientRect(), left = startX - contentsBox.left, top_2 = startY - contentsBox.top, iframe = _this.el.querySelector("iframe"), doc = iframe.contentDocument, el = doc && doc.elementFromPoint(left, top_2), first_1, base_1;
                    if (!event.ctrlKey) {
                        _this.clearWizards();
                        templates.clear();
                        overlay.clear();
                    }
                    el.parentElements("[ice]", true).forEach(function (element) {
                        var template = Editor.findTemplate(element);
                        if (template) {
                            templates.set(element, template);
                            first_1 = first_1 || element;
                            base_1 = element;
                        }
                    });
                    if (base_1) {
                        var walker = document.createTreeWalker(base_1, NodeFilter.SHOW_ELEMENT), element = walker.currentNode, lastTemplate = void 0;
                        while (element) {
                            var template = templates.get(element);
                            if (!template) {
                                template = Editor.findTemplate(element);
                                if (template) {
                                    templates.set(element, template);
                                }
                            }
                            if (template) {
                                var parent_4 = element.parentElements(base_1).intersect(Array.from(templates.keys())).first();
                                overlay.add(element);
                                _this.createWizard(element, template, parent_4);
                            }
                            lastTemplate = template;
                            element = walker.nextNode();
                        }
                        console.log("templates", templates);
                        overlay.select(first_1);
                        _this.editTextWizards();
                        _this.activeWizards();
                        Editor.Panel.trigger(4, first_1);
                    }
                }
                else if (isMove || isResize) {
                    var dragStarted_1 = false;
                    event.preventDefault();
                    overlay.start(startX, startY);
                    if (isResize) {
                        if (classList.contains("nw")) {
                            mode = 2;
                        }
                        else if (classList.contains("n")) {
                            mode = 3;
                        }
                        else if (classList.contains("ne")) {
                            mode = 4;
                        }
                        else if (classList.contains("w")) {
                            mode = 5;
                        }
                        else if (classList.contains("e")) {
                            mode = 6;
                        }
                        else if (classList.contains("sw")) {
                            mode = 7;
                        }
                        else if (classList.contains("s")) {
                            mode = 8;
                        }
                        else if (classList.contains("se")) {
                            mode = 9;
                        }
                    }
                    $("iframe").css("pointer-events", "none");
                    $(document).on({
                        "mousemove.drag touchmove.drag": function (event) {
                            var clientX = event.clientX, clientY = event.clientY, deltaX = clientX - startX, deltaY = clientY - startY;
                            event.preventDefault();
                            if (!dragStarted_1 && (Math.abs(deltaX) > 3 || Math.abs(deltaY) > 3)) {
                                dragStarted_1 = true;
                                classList.add("dragging");
                            }
                            if (dragStarted_1) {
                                var snap = event.shiftKey ? 100 : event.ctrlKey ? 20 : 1;
                                clientX -= deltaX % snap;
                                clientY -= deltaY % snap;
                                if (mode === 1) {
                                    overlay.move(clientX, clientY);
                                }
                                else {
                                    overlay.size(mode, clientX, clientY);
                                }
                            }
                        },
                        "mouseup.drag touchend.drag": function (event) {
                            event.preventDefault();
                            $(document).off(".drag");
                            $("iframe").css("pointer-events", "");
                            if (dragStarted_1) {
                                classList.remove("dragging");
                                ic.setImmediate("edit.sethtml", updateHtml);
                            }
                            else {
                                window.console.log("click");
                            }
                        }
                    });
                }
            };
            _this.handleDblClick = function (event) {
                var box = _this.contents.getBoundingClientRect(), left = event.clientX - box.left, top = event.clientY - box.top, iframe = _this.el.querySelector("iframe"), doc = iframe.contentDocument, target = doc && doc.elementFromPoint(left, top), ice = target.closest("[ice]");
                window.console.log("double click", target);
                if (target && ice && ice.getAttribute("ice") === String(Editor.currentActivity._id)) {
                    var img = target.closest("img");
                    if (img) {
                        currentImage = img;
                        openImageBank();
                        return false;
                    }
                    var subframe = querySelector(target, "iframe");
                    if (subframe) {
                        var src = prompt("Please enter URL for the iFrame", subframe.src || "");
                        if (src !== null) {
                            subframe.src = src.replace("https://youtu.be/", "https://www.youtube.com/embed/");
                            updateHtml();
                        }
                        return false;
                    }
                    var p = target.closest("p[id]");
                    if (p) {
                        if (p.querySelectorAll("[ic-toggle],img").length) {
                            alert("Currently unable to edit text with inline images or toggles\n\n(TinyMCE bug #3275)");
                        }
                        else {
                            overlay.clear();
                            _this.el.querySelector("iframe").style.pointerEvents = "all";
                            if (p.classList.contains("mce-content-body")) {
                                p.focus();
                            }
                            else {
                                iframe.contentWindow.eval("ic_edit('" + p.id + "')");
                            }
                        }
                        return false;
                    }
                    var popup_1 = target.closest("[editor-img]");
                    if (popup_1) {
                        var img_1 = createElement("img");
                        img_1.src = popup_1.getAttribute("editor-img");
                        img_1.onload = function () {
                            var width = img_1.naturalWidth, height = img_1.naturalHeight, maxWidth = window.innerWidth - 25, maxHeight = window.innerHeight - 61, widthScale = maxWidth / width, heightScale = maxHeight / height, scale = Math.min(1, widthScale, heightScale);
                            if (scale < 1) {
                                if (widthScale <= heightScale) {
                                    img_1.style.width = maxWidth + "px";
                                    img_1.style.height = "auto";
                                }
                                else {
                                    img_1.style.width = "auto";
                                    img_1.style.height = maxHeight + "px";
                                }
                            }
                            else {
                                img_1.style.width = "auto";
                                img_1.style.height = "100%";
                            }
                            $().w2popup({
                                title: "<i class=\"fa fa-cogs\"></i> " + (popup_1.getAttribute("editor-title") || "Screenshot"),
                                body: "<div id=\"screenshot_popup\" style=\"padding-top:7px;\"><img src=\"" + img_1.src + "\" style=\"width:" + img_1.style.width + ";height:" + img_1.style.height + ";\"></div>",
                                style: "",
                                width: width * scale + 14,
                                height: height * scale + 51,
                                onOpen: function () {
                                }
                            });
                        };
                        return false;
                    }
                }
            };
            if (EditPanel.instance) {
                throw new Error("Error: Instantiation failed: Use EditPanel.instance instead of new.");
            }
            Object.defineProperty(EditPanel, "instance", {
                value: _this
            });
            window.console.log(_this.el);
            _this.contents = _this.el.querySelector(".ic_edit");
            _this.contents.addEventListener("dblclick", _this.handleDblClick, false);
            _this.contents.addEventListener("keydown", _this.onKeyDown, false);
            _this.contents.addEventListener("wheel", _this.onScroll, false);
            _this.el.addEventListener("mousedown", _this.onMouseDown, true);
            _this.el.addEventListener("touchstart", _this.onMouseDown, true);
            (new ResizeObserver(overlay.update)).observe(document.getElementById("ic_edit"));
            return _this;
        }
        EditPanel.prototype.on = function (event, template, position) {
            switch (event) {
                case 2:
                    Editor.changeActivity();
                    break;
                case 3:
                    if (!updating) {
                        this.setActivityHtml();
                    }
                    break;
                case 5:
                    break;
                case 6:
                    {
                        var $iframe = $("#ic_edit iframe"), box = $iframe.getRect(), doc = $iframe[0].contentDocument, hoverElement = doc.elementFromPoint(position.left - box.left, position.top - box.top);
                        if (hoverElement && template) {
                            var best = void 0;
                            while (hoverElement) {
                                if (hoverElement.nodeName.startsWith("IC-") && Editor.isValidWidget(template.content.firstChild, hoverElement)) {
                                    if (Editor.ctrlKey) {
                                        best = hoverElement;
                                    }
                                    else {
                                        break;
                                    }
                                }
                                if (hoverElement.hasAttribute("ice")) {
                                    break;
                                }
                                hoverElement = hoverElement.parentElement;
                            }
                            if (Editor.ctrlKey) {
                                hoverElement = best;
                            }
                            if (focusElement !== hoverElement) {
                                focusElement = hoverElement;
                                var data = template.getAttribute("data-template"), options = data ? data.split(/[\s,]/) : [];
                                overlay.clear();
                                if (hoverElement) {
                                    overlay.add(hoverElement);
                                }
                                $("#ic_edit>.ic_edit>.overlay").css("display", hoverElement ? "block" : "none");
                            }
                        }
                        else {
                            $("#ic_edit>.ic_edit>.overlay").css("display", "none");
                        }
                        break;
                    }
                case 7:
                    {
                        var box = $("#ic_edit iframe").getRect();
                        if (box.left <= position.left && box.right >= position.left
                            && box.top <= position.top && box.bottom >= position.top
                            && focusElement) {
                            var data = template.getAttribute("data-template"), options = data ? data.split(/[\s,]/) : [], doc = $("#ic_edit iframe")[0].contentDocument, lastId_1 = 0, classNames = Object.keys(Editor.rules).join(","), $component = $(template.content)
                                .children()
                                .clone(true)
                                .appendTo(focusElement), component = $component[0], pElements = querySelectorAll(component, "p");
                            $("#ic_edit>.ic_edit>.overlay").css("display", "none");
                            if (pElements.length) {
                                querySelectorAll(doc, "p[id]").forEach(function (p) {
                                    lastId_1 = Math.max(lastId_1, p.id.regex(/p(\d+)/));
                                });
                                pElements.forEach(function (p) {
                                    p.id = "p" + (++lastId_1);
                                });
                            }
                            $component.find(classNames).addBack(classNames).each(function (index, el) {
                                el.fixState();
                            });
                            if ($component.siblings("[ice='" + Editor.currentActivity._id + "']").length) {
                                $component.attr("ice", Editor.currentActivity._id);
                            }
                            if (Editor.ctrlKey || options.includes("position")) {
                                var relativeBox = focusElement.getBoundingClientRect(), top_3 = ((position.top - box.top - relativeBox.top) * 100 / relativeBox.height).round(3), left = ((position.left - box.left - relativeBox.left) * 100 / relativeBox.width).round(3);
                                if (!$component.is(classNames)) {
                                    $component = $("<ic-box></ic-box>").insertBefore($component).append($component);
                                }
                                $component
                                    .removeAttr(Editor.REMOVETEMPLATES.join(" "))
                                    .css({
                                    position: "absolute",
                                    top: top_3 + "%",
                                    left: left + "%",
                                    width: component.style.width || (Math.min(100 - left, 25) + "%"),
                                    height: component.style.height || (Math.min(100 - top_3, 25) + "%")
                                })
                                    .find(Editor.REMOVETEMPLATES.wrap("],[", "[", "]"))
                                    .removeAttr(Editor.REMOVETEMPLATES.join(" "));
                            }
                            if ($component.find("iframe").length) {
                                $component.find("iframe").each(function (i, el) {
                                    el.src = (prompt("Please enter URL for the iFrame", el.src || "") || "").replace("https://youtu.be/", "https://www.youtube.com/embed/");
                                });
                            }
                            if (($component.is("img") || $component.find("img").length) && !template.classList.contains("resource")) {
                                currentImage = ($component.is("img") ? $component : $component.find("img"))[0];
                                openImageBank();
                            }
                            updateHtml();
                            Editor.currentEditWidget = $component[0].closestWidget();
                            querySelectorAll(".ice-select").forEach(function (element) {
                                if (element !== Editor.currentEditWidget) {
                                    element.classList.remove("ice-select");
                                }
                            });
                            Editor.currentEditWidget.classList.add("ice-select");
                            overlay.clear();
                            overlay.add(Editor.currentEditWidget);
                            return true;
                        }
                        break;
                    }
            }
        };
        ;
        EditPanel.prototype.setHtml = function () {
            var dom = Editor.currentActivity.dom, $root = $("#ic_edit iframe").contents().find("[ice='" + Editor.currentActivity._id + "']");
            while (dom.hasChildNodes()) {
                dom.removeChild(dom.firstChild);
            }
            if ($root.length) {
                $root.each(function (i, el) {
                    dom.appendChild(el.cloneNode(true));
                });
            }
            Editor.currentActivity.formatHtml();
            updating = true;
            Editor.setHtml(Editor.currentActivity.html);
            updating = false;
        };
        EditPanel.prototype.setActivityHtml = function () {
            var iframe = createElement("iframe");
            this.clearWizards();
            overlay.clear();
            iframe.onload = function () {
                var doc = this.contentDocument;
                function fixScript(el) {
                    var script = createElement("script");
                    [].forEach.call(el.attributes, function (attribute) {
                        script.setAttribute(attribute.name, attribute.value);
                    });
                    script.text = el.text;
                    script.icReplaced = true;
                    return script;
                }
                Editor.currentActivity.build().then(function (dom) {
                    var head = doc.head, body = doc.body, inHead = true, link = createElement("link"), script = createElement("script");
                    head.parentElement.setAttribute("data-uid", String(Editor.currentActivity._id).crc32());
                    [].forEach.call(dom.childNodes, function (el) {
                        if (inHead && !["BASE", "LINK", "META", "NOSCRIPT", "SCRIPT", "STYLE", "TITLE"].includes(el.nodeName)) {
                            inHead = false;
                        }
                        if (el.nodeName === "SCRIPT") {
                            el = fixScript(el);
                        }
                        else {
                            el = el.cloneNode(true);
                        }
                        (inHead ? head : body).appendChild(el);
                    });
                    link.rel = "stylesheet";
                    link.type = "text/css";
                    link.href = "include/preview.css";
                    head.appendChild(link);
                    script.type = "text/javascript";
                    script.src = "bower_components/tinymce/tinymce.min.js";
                    script.defer = true;
                    head.appendChild(script);
                    script = createElement("script");
                    script.text = "var ic_menu,ic_start_widget,ic_edit=" + inject.ic_edit.toString() + ";" + inject.ic_action.toString() + ";" + inject.ic_context.toString() + ";(" + inject.ic_startup.toString() + ")('" + Editor.currentActivity._id + "')";
                    head.appendChild(script);
                    querySelectorAll(doc, "input").forEach(function (el) {
                        el.readOnly = true;
                    });
                    body.addEventListener("mousedown", stopPropagation, true);
                    body.addEventListener("mouseup", stopPropagation, true);
                    body.addEventListener("click", stopPropagation, true);
                });
            };
            iframe.sandbox.add("allow-same-origin", "allow-scripts");
            iframe.srcdoc = "<!DOCTYPE html>\n<html><head></head><body></body></html>";
            iframe.src = "blank.html";
            $("#ic_edit>div>iframe").replaceWith(iframe);
            EditPanel.instance.updateWatchers();
        };
        ;
        EditPanel.prototype.updateWatchers = function () {
            var activity = Editor.currentActivity;
            var $gravatars = $("#ic_edit").parent().siblings(".w2ui-tabs").find("td:last-child").empty();
            if (activity.watchers.length) {
                activity.watchers.forEach(function (email, index) {
                    $gravatars.append("<img title=\"" + email + "\" alt=\"\" src=\"https://www.gravatar.com/avatar/" + email.trim().toLowerCase().md5() + ".jpg?s=20&d=identicon\" style=\"right:" + (22 * index + 2) + "px\">");
                });
            }
        };
        return EditPanel;
    }(Editor.Panel));
    Editor.EditPanel = EditPanel;
})(Editor || (Editor = {}));
;
var Editor;
(function (Editor) {
    var ExplorePanel = (function (_super) {
        __extends(ExplorePanel, _super);
        function ExplorePanel() {
            var _this = _super.call(this, "explore", 1, "preview") || this;
            _this.clickFile = function (event) {
                var text, basefile, basepath, $this = $(event.target), activity = Editor.currentActivity, files = activity._files, $tr = $this.closest("tr"), mode = $tr.data("text"), view = $tr.data("view"), filename = $tr.data("file"), force = "?_=" + Date.now();
                if (filename) {
                    var filepath = Config.server + "assets/" + activity._id + "/" + encodeURIComponent(filename);
                    $tr.addClass("selected").siblings().removeClass("selected");
                    $("#ic_mode_explore_download").attr("href", filepath);
                    $("#ic_view,#ic_file").css("width", view && mode ? "50%" : "");
                    $("#ic_view").toggle(!!view);
                    $("#ic_file").toggle(!!mode);
                    if (view) {
                        switch (view) {
                            case "image":
                                $("#ic_view").html("<img src=\"" + filepath + force + "\" alt=\"" + filename + "\" title=\"" + filename + "\"/>");
                                var $img = $("#ic_view img");
                                if ($img[0].complete) {
                                    _this.centerView();
                                }
                                else {
                                    $img.one("load", _this.centerView);
                                }
                                break;
                            case "font":
                                text = "ic_view_" + activity._id + "_" + filename.replace(/\.[^\.]+$/, "");
                                basefile = filename.replace(/[^\.]+$/i, "");
                                basepath = filepath.replace(/[^\.]+$/i, "");
                                $.stylesheet("@font-face /*" + text + "*/", [
                                    "font-family:'" + text + "'",
                                    "src:"
                                        + (files.includes(basefile + "woff2") ? "url('" + basepath + "woff2" + force + "') format('woff2')," : "")
                                        + (files.includes(basefile + "woff") ? "url('" + basepath + "woff" + force + "') format('woff')," : "")
                                        + (files.includes(basefile + "ttf") ? "url('" + basepath + "ttf" + force + "') format('truetype')" : "")
                                ]);
                                text = "<div style=\"font-family:'" + text + "';\" contenteditable=\"true\">Grumpy wizards make toxic brew for the evil Queen and Jack.</div>";
                                $("#ic_view").html(text + text + text + text).children().on("keyup paste", function () {
                                    var $this = $(this);
                                    $this.siblings().text($this.text());
                                });
                                break;
                            case "audio":
                                $("#ic_view").html("<audio controls preload><source src=\"" + filepath.replace(/\.ogg$/i, ".mp3") + force + "\" type=\"audio/mpeg\"/><source src=\"" + filepath.replace(/\.mp3$/i, ".ogg") + force + "\" type=\"audio/ogg\"/></audio>");
                                break;
                            default:
                                $("#ic_view").empty();
                        }
                    }
                    $("#ic_file").data("file", filename).data("content", "");
                    if (mode) {
                        $.get(filepath + force, function (data) {
                            var $abaFile = $("#ic_file"), editor = $abaFile.data("editor");
                            $abaFile.data("content", data);
                            editor.getSession().setMode("ace/mode/" + mode);
                            editor.setValue(data, -1);
                            if (view === "svg") {
                                $("#ic_view").html(data);
                                _this.centerView();
                            }
                        }, "text");
                    }
                    $("#ic_mode_explore_download,#ic_mode_explore_rename,#ic_mode_explore_delete").removeClass("disabled");
                    $("#ic_mode_explore_save").addClass("disabled");
                }
                return false;
            };
            if (ExplorePanel.instance) {
                throw new Error("Error: Instantiation failed: Use ExplorePanel.instance instead of new.");
            }
            Object.defineProperty(ExplorePanel, "instance", {
                value: _this
            });
            return _this;
        }
        ExplorePanel.prototype.on = function (event, args) {
            switch (event) {
                case 2:
                    this.onExplore();
                    break;
            }
        };
        ;
        ExplorePanel.prototype.centerView = function () {
            var parent = $("#ic_view").getRect(), $img = $("#ic_view img,#ic_view svg"), rect = $img.getRect();
            $img.css("top", Math.max(0, (parent.height - rect.height) / 2));
        };
        ExplorePanel.prototype.clickRename = function () {
            var file = $("#ic_file").data("file");
            Editor.dialog("Please enter the new name for '" + file + "':<br/><br/><input type=\"text\" value=\"" + file + "\"/><br/><br/>This cannot be undone!", "Confirm Rename", function () {
                var newname = $("#ic_dialog input").val(), node = Editor.currentActivity;
                if (newname && node._files.includes(newname)) {
                    Editor.dialog("That name already exists!", "Rename Error", true);
                }
                else if (newname) {
                    Editor.ajax({
                        type: "POST",
                        url: "ajax.php",
                        dataType: "json",
                        data: {
                            "action": "rename_file",
                            "id": node._id,
                            "name": file,
                            "newname": newname
                        },
                        success: function (data) {
                            if (data.error) {
                                return Editor.dialog("Error: " + data.error, "Server Error");
                            }
                            console.log(data);
                            Editor.log("Success: File renamed");
                            node._files[node._files.indexOf(file)] = newname;
                            $("#ic_explore > div > table .selected")
                                .data("file", newname)
                                .children()
                                .eq(1)
                                .text(newname);
                            $.tree.remove(Editor.currentActivity._id + "_" + file.replace(/^assets\/\d+\//, ""));
                            $.tree.add(Editor.currentActivity._id + "_" + newname.replace(/^assets\/\d+\//, ""));
                        }
                    });
                }
            });
        };
        ExplorePanel.prototype.clickDelete = function () {
            var file = $("#ic_file").data("file");
            Editor.dialog("Are you sure you wish to delete '" + file + "'?<br/>This cannot be undone!", "Confirm Deletion", function () {
                Editor.ajax({
                    type: "POST",
                    url: "ajax.php",
                    dataType: "json",
                    data: {
                        "action": "delete_file",
                        "id": $.tree.currentId,
                        "name": file
                    },
                    success: function (data) {
                        if (data.error) {
                            return Editor.dialog("Error: " + data.error, "Server Error");
                        }
                        var activity = Editor.currentActivity;
                        console.log(data);
                        Editor.log("Success: File deleted");
                        if (activity._files) {
                            activity._files.splice(activity._files.indexOf(file), 1);
                        }
                        $("#ic_explore > div > table .selected").remove();
                        $("#ic_view,#ic_file").hide();
                        $("#ic_dialog").parent().velocity({ opacity: 0 }, {
                            delay: 1000,
                            complete: function () {
                                $("#ic_dialog").dialog("close");
                            }
                        });
                        $.tree.remove(activity._id + "_" + file.replace(/^assets\/\d+\//, ""));
                    }
                });
                $("#ic_mode_explore_download,#ic_mode_explore_rename,#ic_mode_explore_delete").addClass("disabled");
            });
        };
        ExplorePanel.prototype.editTextChange = function () {
            var $abaFile = $("#ic_file"), filename = $abaFile.data("file"), original = $abaFile.data("content"), file = $("#ic_file").data("editor").getValue();
            if (original === file) {
                delete Editor.fileCache[filename];
            }
            else {
                Editor.fileCache[filename] = btoa(file.unicode());
                if (!Editor.change) {
                    Editor.change = true;
                    Button.update();
                }
            }
        };
        ExplorePanel.prototype.onExplore = function () {
            var i, filename, image, edit, view, node = Editor.currentActivity, id = node._id, files = node._files.naturalCaseSort(), $explorer = $("#ic_explore");
            if (!$explorer.hasClass("ui-layout-container")) {
                $("#ic_mode_explore_rename").on("click", this.clickRename);
                $("#ic_mode_explore_delete").on("click", this.clickDelete);
                $("#ic_file").on("keyup", this.editTextChange);
                var editor = ace.edit("ic_file"), oldTextInput = editor.onTextInput;
                editor.onTextInput = function () {
                    $("#ic_mode_explore_save").removeClass("disabled");
                    return oldTextInput.apply(this, arguments);
                };
                editor.setTheme("ace/theme/xcode");
                editor.getSession().setUseWrapMode(true);
                editor.$blockScrolling = Infinity;
                $("#ic_file").data("editor", editor);
            }
            $explorer.find("tbody").empty();
            $("#ic_view,#ic_file").hide();
            for (i = 0; i < files.length; i++) {
                filename = files[i];
                if (!/^assets\//.test(filename)) {
                    console.error("Error: Found a bad filename:", filename);
                    continue;
                }
                if (filename.regex(/assets\/(\d+)\//) !== id) {
                    continue;
                }
                filename = filename.regex(/assets\/\d+\/(.*)$/);
                edit = false;
                view = false;
                switch (filename.regex(/\.([^\.]+)$/)) {
                    case "svg":
                        edit = view = "svg";
                        image = "-image";
                        break;
                    case "png":
                    case "jpg":
                        view = "image";
                        image = "-image";
                        break;
                    case "html":
                        image = "-text";
                        edit = "html";
                        break;
                    case "js":
                        image = "-text";
                        edit = "javascript";
                        break;
                    case "json":
                        image = "-text";
                        edit = "json";
                        break;
                    case "css":
                    case "scss":
                        image = "-text";
                        edit = "scss";
                        break;
                    case "txt":
                        image = "-text";
                        edit = "text";
                        break;
                    case "mp3":
                    case "ogg":
                        view = "audio";
                        image = "-audio";
                        break;
                    case "m4v":
                        image = "-video";
                        break;
                    case "ttf":
                    case "woff":
                    case "woff2":
                        view = "font";
                        image = "-pdf";
                        break;
                    default:
                        image = "";
                        break;
                }
                $.make("tr", $.make("td", $.make("span", "fa fa-file" + image + "-o")), $.make("td", "=" + filename))
                    .data("file", filename)
                    .data("text", edit)
                    .data("view", view)
                    .on("click", this.clickFile)
                    .appendTo($explorer.find("tbody"));
            }
            if (!files.length) {
                $.make("tr", $.make("th", "colspan=2", "=<i>no files</i>")).appendTo($explorer.find("tbody"));
            }
            $("#ic_mode_explore_download,#ic_mode_explore_rename,#ic_mode_explore_delete,#ic_mode_explore_save").addClass("disabled");
        };
        return ExplorePanel;
    }(Editor.Panel));
    Editor.ExplorePanel = ExplorePanel;
})(Editor || (Editor = {}));
;
var ic;
(function (ic) {
    ;
})(ic || (ic = {}));
;
var Editor;
(function (Editor) {
    var updating;
    var MarkingPanel = (function (_super) {
        __extends(MarkingPanel, _super);
        function MarkingPanel() {
            var _this = _super.call(this, "marking", 1, "right") || this;
            if (MarkingPanel.instance) {
                throw new Error("Error: Instantiation failed: Use MarkingPanel.instance instead of new.");
            }
            Object.defineProperty(MarkingPanel, "instance", {
                value: _this
            });
            return _this;
        }
        MarkingPanel.prototype.on = function (event, args) {
            switch (event) {
                case 3:
                    if (!updating) {
                        this.setActivityHtml();
                    }
                    break;
            }
        };
        ;
        MarkingPanel.prototype.onFocusChange = function () {
            Editor.currentActivity.formatHtml();
            updating = true;
            Editor.setHtml(Editor.currentActivity.html);
            updating = false;
        };
        MarkingPanel.prototype.onOptionChange = function (event, notMarkingPanel) {
            var $this = $(this), isMultiple = $this.is("textarea"), element = $this.data("element") || querySelector(Editor.currentActivity.dom, "#" + $this.closest("tr").data("element")), mark = JSON.parse(element.getAttribute("data-json") || "{}"), value = $this.is("input[type=checkbox]") ? ($this.prop("checked") ? "checked" : "") : $this.val(), empty = (value || "").trim() === "";
            switch ($this.data("type")) {
                case "value":
                    if (empty) {
                        delete mark.value;
                    }
                    else {
                        if (isMultiple) {
                            value = value.trim().split(/[\n\r]+/);
                            if (!value.equals(mark.value)) {
                                mark.value = value;
                            }
                        }
                        else if (mark.value !== value) {
                            mark.value = value;
                        }
                    }
                    break;
                case "answer":
                    if (empty) {
                        delete mark.answer;
                    }
                    else {
                        if (isMultiple) {
                            value = value.trim().split(/[\n\r]+/);
                            if (!value.equals(mark.answer)) {
                                mark.answer = value;
                            }
                        }
                        else if (mark.answer !== value) {
                            mark.answer = value;
                        }
                    }
                    break;
                case "points":
                    if (empty) {
                        delete mark.points;
                    }
                    else if (mark.points !== value) {
                        mark.points = /^[0-9]+$/.test(value) ? parseInt(value) : value;
                    }
                    break;
                case "round":
                    if (!this.checked) {
                        delete mark.round;
                    }
                    else {
                        mark.round = true;
                    }
                    break;
                case "accept":
                    if (empty) {
                        element.removeAttribute("data-accept");
                    }
                    else {
                        element.setAttribute("data-accept", value);
                    }
                    break;
                case "duplicates":
                    if (!this.checked) {
                        delete mark.duplicates;
                    }
                    else {
                        mark.duplicates = true;
                    }
                    break;
                case "id":
                    if (empty) {
                        delete mark.id;
                    }
                    else if (mark.id !== value) {
                        mark.id = value;
                    }
                    break;
            }
            var json = {};
            if (mark.value) {
                json.value = mark.value;
            }
            if (mark.answer) {
                json.answer = mark.answer;
            }
            if (mark.points !== undefined) {
                json.points = mark.points;
            }
            if (mark.round) {
                json.round = mark.round;
            }
            if (mark.duplicates) {
                json.duplicates = mark.duplicates;
            }
            if (mark.id) {
                json.id = mark.id;
            }
            if (Object.keys(json).length) {
                element.setAttribute("data-json", JSON.stringify(json));
            }
            else {
                element.removeAttribute("data-json");
            }
            if (notMarkingPanel !== true && event.type !== "keyup") {
                MarkingPanel.instance.onFocusChange();
            }
        };
        MarkingPanel.prototype.setActivityHtml = function () {
            var $panel = this.$.empty(), $table = $.make("table", $.make("tr", $.make("th", "="), $.make("th", "=Group"), $.make("th", "=Value"), $.make("th", "=Answer"), $.make("th", "=Points"), $.make("th", "=Round"), $.make("th", "=Accept"), $.make("th", "=Dup"), $.make("th", "=ID")));
            if (Editor.currentActivity && !Editor.currentActivity.isGroup && !Editor.currentActivity.pretty) {
                ic.setImmediate("marking.editor", this.setActivityHtml.bind(this));
            }
            if (Editor.currentActivity && !Editor.currentActivity.isGroup && Editor.currentActivity.pretty) {
                ic.setImmediate("marking.editor");
                $(Editor.currentActivity.dom).find("ic-activities,ic-activity,ic-draggable,ic-dropdown,ic-droppable,ic-option,ic-select,ic-text,ic-toggle").each(function (i, element) {
                    var $tr = $.make("tr").appendTo($table), mark = JSON.parse(element.getAttribute("data-json") || "{}"), name = element.tagName.replace(/^IC-/, "").ucfirst(), value = mark.value || "", valuePlaceholder = "default", hasValue = true, hasMultipleValues = false, answer = mark.answer || "", hasAnswer = true, hasMultipleAnswers = false, points = mark.points, pointsPlaceholder = 0, hasPoints = true, round = mark.round, hasRound = false, accept = "", hasAccept = false, duplicates = mark.duplicates, hasDuplicates = false, groupElement = element.closest("[ic-group]"), group = groupElement ? parseInt(groupElement.getAttribute("ic-group"), 10) : 0, hasID = false, id = mark.id || "";
                    if (!element.id) {
                        return;
                    }
                    switch (element.tagName) {
                        case "IC-ACTIVITIES":
                        case "IC-ACTIVITY":
                            $tr.addClass("line-above");
                            hasValue = hasAnswer = false;
                            hasDuplicates = hasRound = hasID = true;
                            pointsPlaceholder = "?";
                            break;
                        case "IC-DRAGGABLE":
                            hasAnswer = hasPoints = false;
                            if (element.innerText.trim()) {
                                valuePlaceholder = element.innerText.trim();
                            }
                            break;
                        case "IC-DROPDOWN":
                            value = value || element.innerText;
                            break;
                        case "IC-DROPPABLE":
                            pointsPlaceholder = isArray(answer)
                                ? Math.min(Math.max(parseInt(element.getAttribute("ic-droppable-count"), 10) || element.querySelectorAll(":scope>ic-draggable").length || 1, 1), answer.length)
                                : answer || $(element).is("[ic-draggable=sortable]") ? 1 : -1;
                            hasMultipleValues = hasValue = !!pointsPlaceholder;
                            valuePlaceholder = "#drag<index>#";
                            hasMultipleAnswers = true;
                            break;
                        case "IC-OPTION":
                            hasAnswer = false;
                            pointsPlaceholder = 1;
                            valuePlaceholder = element.innerText.trim() ? element.innerText.trim() : "";
                            break;
                        case "IC-SELECT":
                            valuePlaceholder = "#option<index>#";
                            hasPoints = false;
                            hasMultipleAnswers = true;
                            break;
                        case "IC-TEXT":
                            hasValue = false;
                            hasAccept = hasMultipleAnswers = true;
                            pointsPlaceholder = 1;
                            accept = element.getAttribute("data-accept") || "";
                            break;
                        case "IC-TOGGLE":
                            valuePlaceholder = element.innerText.trim() || "";
                            switch (element.getAttribute("ic-toggle")) {
                                case "range":
                                case "multirange":
                                    pointsPlaceholder = /[^ .,:;'"]/.test(valuePlaceholder) ? 1 : 0;
                                    break;
                                case "text":
                                    pointsPlaceholder = -1;
                                    break;
                                default:
                                    pointsPlaceholder = valuePlaceholder ? 1 : -1;
                                    break;
                            }
                            break;
                        default:
                            console.error("Error: Unknown widget type - ", element);
                    }
                    $.make("td", "=" + name + (element.id ? "<br/>#" + element.id + "#" : "")).appendTo($tr);
                    $.make("td", "=" + (group >= 0 ? "<i>" + group + "</i>" : "")).appendTo($tr);
                    if (hasValue) {
                        if (hasMultipleValues) {
                            $.make("td", $.make("textarea", "placeholder=" + valuePlaceholder).val(isArray(value) ? value.join("\n") : value).data("type", "value")).appendTo($tr);
                        }
                        else {
                            $.make("td", $.make("input", "type=text", "placeholder=" + valuePlaceholder).val(value).data("type", "value")).appendTo($tr);
                        }
                    }
                    else {
                        $.make("td", "=n/a").appendTo($tr);
                    }
                    if (hasAnswer) {
                        if (hasMultipleAnswers) {
                            $.make("td", $.make("textarea").val(isArray(answer) ? answer.join("\n") : answer).data("type", "answer")).appendTo($tr);
                        }
                        else {
                            $.make("td", $.make("input", "type=text").val(answer).data("type", "answer")).appendTo($tr);
                        }
                    }
                    else {
                        $.make("td", "=n/a").appendTo($tr);
                    }
                    if (hasPoints) {
                        $.make("td", $.make("textarea", "placeholder=default: " + pointsPlaceholder).val(points).data("type", "points")).appendTo($tr);
                    }
                    else {
                        $.make("td", "=n/a").appendTo($tr);
                    }
                    if (hasRound) {
                        $.make("td", $.make("input", "type=checkbox").prop("checked", round ? "checked" : "").data("type", "round")).appendTo($tr);
                    }
                    else {
                        $.make("td", "=n/a").appendTo($tr);
                    }
                    if (hasAccept) {
                        $.make("td", $.make("input", "type=text", "placeholder=.*").val(accept).data("type", "accept")).appendTo($tr);
                    }
                    else {
                        $.make("td", "=n/a").appendTo($tr);
                    }
                    if (hasDuplicates) {
                        $.make("td", $.make("input", "type=checkbox").prop("checked", duplicates ? "checked" : "").data("type", "duplicates")).appendTo($tr);
                    }
                    else {
                        $.make("td", "=n/a").appendTo($tr);
                    }
                    if (hasID) {
                        $.make("td", $.make("input", "type=text", "placeholder=#@", "min-width:4em;").val(id).data("type", "id")).appendTo($tr);
                    }
                    else {
                        $.make("td", "=n/a").appendTo($tr);
                    }
                    $tr.data("element", element.id);
                });
                $table
                    .appendTo($panel)
                    .find("input,textarea")
                    .on({
                    "keyup": this.onOptionChange,
                    "change": this.onOptionChange,
                    "blur": this.onFocusChange
                })
                    .filter("textarea")
                    .each(Editor.fixTextAreaHeight);
            }
        };
        return MarkingPanel;
    }(Editor.Panel));
    Editor.MarkingPanel = MarkingPanel;
})(Editor || (Editor = {}));
;
var Editor;
(function (Editor) {
    function toggleShowMarks() {
        var screens = $("#ic_preview>iframe")[0].contentDocument.getElementsByTagName("ic-screens")[0], state = PreviewPanel.instance.showMarks = !this.classList.contains("active");
        this.classList.toggle("active");
        Editor.setSetting("options.preview_marks", state);
        if (screens) {
            screens.icWidget.toggleState("marked", state);
        }
    }
    var StoryboardState;
    (function (StoryboardState) {
        StoryboardState[StoryboardState["PAUSED"] = -1] = "PAUSED";
        StoryboardState[StoryboardState["PLAYING"] = 0] = "PLAYING";
        StoryboardState[StoryboardState["STEPPING"] = 1] = "STEPPING";
        StoryboardState[StoryboardState["SKIPPING"] = 2] = "SKIPPING";
    })(StoryboardState || (StoryboardState = {}));
    var hasStoryboards, storyboardState, storyboardName, storyboardAction;
    function checkStoryboard() {
        var frame = $("#ic_preview>iframe")[0], skip;
        if (frame) {
            var doc = frame.contentDocument, win = doc && doc.defaultView, result = win && win.eval("window.IC&&[window.IC.hasStoryboards,window.IC.storyboardState,window.IC.storyboardName,window.IC.storyboardAction]");
            if (isArray(result)) {
                skip = true;
                if (hasStoryboards !== result[0]) {
                    hasStoryboards = result[0];
                    $("#show_storyboard").toggle(!!hasStoryboards);
                }
                if (hasStoryboards) {
                    if (storyboardState !== result[1]) {
                        storyboardState = result[1];
                        $("#show_storyboard").toggleClass("playing", storyboardState >= 0);
                    }
                    if (storyboardName !== result[2] || storyboardAction !== result[3]) {
                        storyboardName = result[2];
                        storyboardAction = result[3];
                        var text = ((storyboardName || "") + (storyboardAction ? ": " + storyboardAction : "")) || "---";
                        $("#show_storyboard>span")
                            .text(text)
                            .attr("title", text);
                    }
                }
            }
        }
        if (!skip && hasStoryboards) {
            hasStoryboards = false;
            $("#show_storyboard").hide();
        }
        ic.rAF(checkStoryboard);
    }
    checkStoryboard();
    function setStoryboards(eventObject) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        if (hasStoryboards) {
            $("#ic_preview>iframe")[0].contentDocument.defaultView.eval("window.IC.storyboardState=" + eventObject.data + ";");
        }
    }
    var PreviewPanel = (function (_super) {
        __extends(PreviewPanel, _super);
        function PreviewPanel() {
            var _this = _super.call(this, "preview", 8, "main") || this;
            _this.showMarks = false;
            if (PreviewPanel.instance) {
                throw new Error("Error: Instantiation failed: Use PreviewPanel.instance instead of new.");
            }
            Object.defineProperty(PreviewPanel, "instance", {
                value: _this
            });
            _this.showMarks = !!Editor.getSetting("options.preview_marks");
            if (_this.showMarks) {
                getElementById("show_marks").classList.add("active");
            }
            getElementById("show_marks").addEventListener("click", toggleShowMarks);
            $("#show_storyboard>.fa-pause").on("click", -1, setStoryboards);
            $("#show_storyboard>.fa-play").on("click", 0, setStoryboards);
            $("#show_storyboard>.fa-step-forward").on("click", 1, setStoryboards);
            $("#show_storyboard>.fa-fast-forward").on("click", 2, setStoryboards);
            return _this;
        }
        PreviewPanel.prototype.on = function (event, args) {
            switch (event) {
                case 3:
                    this.setActivityHtml();
                    break;
            }
        };
        ;
        PreviewPanel.prototype.setActivityHtml = function () {
            var iframe = createElement("iframe");
            iframe.onload = function () {
                var doc = this.contentDocument, fixScript = function (el) {
                    var script = doc.createElement("script");
                    script.text = el.text;
                    el.async = el.defer = false;
                    [].forEach.call(el.attributes, function (attribute) {
                        if (attribute.name !== "defer") {
                            script.setAttribute(attribute.name, attribute.value);
                        }
                    });
                    return script;
                };
                doc.documentElement.style.visibility = "hidden";
                Editor.currentActivity.build().then(function (dom) {
                    var head = doc.head, body = doc.body, inHead = true;
                    head.parentElement.setAttribute("data-uid", String(Editor.currentActivity._id).crc32());
                    doc.defaultView.eval("var IC=IC||{};IC.storyboardState=" + -1 + ";");
                    [].forEach.call(dom.childNodes, function (el) {
                        ((inHead = inHead && ["BASE", "LINK", "META", "NOSCRIPT", "SCRIPT", "STYLE", "TITLE"].includes(el.nodeName))
                            ? head
                            : body).appendChild(el.nodeName === "SCRIPT"
                            ? fixScript(el)
                            : el.cloneNode(true));
                    });
                    var screens = doc.getElementsByTagName("ic-screens")[0], fixScripts = function () {
                        if (screens.icWidget) {
                            querySelectorAll(doc, "script").forEach(function (script) {
                                var parent = script.parentElement;
                                if (parent !== head && parent !== body) {
                                    parent.replaceChild(fixScript(script), script);
                                }
                            });
                            if (PreviewPanel.instance.showMarks) {
                                screens.icWidget.addState("marked");
                            }
                        }
                        else {
                            ic.rAF(fixScripts);
                        }
                    };
                    if (screens) {
                        fixScripts();
                    }
                });
            };
            iframe.sandbox.add("allow-same-origin", "allow-scripts");
            iframe.srcdoc = "<!DOCTYPE html>\n<html><head></head><body></body></html>";
            iframe.src = "blank.html";
            $("#ic_preview>iframe").replaceWith(iframe);
        };
        ;
        return PreviewPanel;
    }(Editor.Panel));
    Editor.PreviewPanel = PreviewPanel;
})(Editor || (Editor = {}));
;
var Editor;
(function (Editor) {
    var ProjectsPanel = (function (_super) {
        __extends(ProjectsPanel, _super);
        function ProjectsPanel() {
            var _this = _super.call(this, "projects", 8, "left") || this;
            if (ProjectsPanel.instance) {
                throw new Error("Error: Instantiation failed: Use ProjectsPanel.instance instead of new.");
            }
            Object.defineProperty(ProjectsPanel, "instance", {
                value: _this
            });
            $.tree.init({
                tree: $("#ic_activities>ul"),
                autoExpand: true,
                isParent: _this.isParent,
                getChildren: _this.getChildren,
                getParent: _this.getParent,
                getIndex: _this.getIndex,
                getHtml: _this.getTitle,
                onAfterHtml: _this.onAfterTitle,
                onBeforeClick: _this.onBeforeClick,
                onClick: _this.onTreeClick,
                canDrag: _this.onDrag,
                onDrop: _this.onDrop,
                onPaste: _this.onPaste
            });
            window.setInterval(function () {
                $("#ic_activities .aba-flag-timer:not(.hidden)").each(function () {
                    var $this = $(this), date = $this.data("date");
                    if (date) {
                        $this.attr("title", date.format("R"));
                        if ((new Date().getTime() - date.getTime()) / 1000 > 3600) {
                            $this.fadeOut(function () {
                                $this.addClass("hidden").css("display", "");
                            });
                        }
                    }
                });
            }, 30000);
            return _this;
        }
        ProjectsPanel.prototype.on = function (event, args) {
            switch (event) {
                case 1:
                    this.get_templates(0);
                    break;
            }
        };
        ;
        ProjectsPanel.prototype.isParent = function (id) {
            var num = parseInt(id, 10);
            if (/[^0-9]/.test(id)) {
                return false;
            }
            return ICNodes.isGroup(num);
        };
        ProjectsPanel.prototype.getParent = function (id) {
            var num = parseInt(id, 10);
            if (/[^0-9]/.test(id)) {
                return String(num);
            }
            var parent = ICNodes.getParentId(num);
            return parent ? String(parent) : "";
        };
        ProjectsPanel.prototype.getIndex = function (id) {
            var num = parseInt(id, 10);
            if (/[^0-9]/.test(id)) {
                return -1;
            }
            return ICNodes.getIndex(num);
        };
        ProjectsPanel.prototype.getChildren = function (id) {
            var num = parseInt(id, 10), activity = ICNodes.get(num);
            if (!activity) {
                ICNodes.load(num);
            }
            else if (activity.isGroup) {
                var nodes = [];
                activity.getChildrenId().forEach(function (id) {
                    if (!ICNodes.has(id)) {
                        nodes.push(id);
                    }
                });
                ICNodes.load(nodes);
            }
        };
        ProjectsPanel.prototype.getTitle = function (id, $el) {
            var num = parseInt(id, 10);
            if (/[^0-9]/.test(id)) {
                var icon = "fa-file-o";
                switch (id.regex(/\.([^\.]+)$/)) {
                    case "gif":
                    case "jpg":
                    case "jpeg":
                    case "png":
                        icon = "fa-file-image-o";
                        break;
                    case "m4a":
                    case "mp3":
                    case "ogg":
                        icon = "fa-file-audio-o";
                        break;
                    case "mp4":
                    case "ogv":
                        icon = "fa-file-video-o";
                        break;
                    case "css":
                    case "js":
                    case "scss":
                    case "svg":
                        icon = "fa-file-code-o";
                        break;
                    default:
                        break;
                }
                $el
                    .html("<span class=\"fa fa-fw " + icon + "\"></span><span>" + id.replace(/^\d+_/, "") + "</span>")
                    .parent()
                    .addClass("aba-tree-file");
            }
            else {
                var i, tmp, $tmp, activity = ICNodes.get(num), $flags = $el.next(), date = new Date(activity._date), age = (new Date().getTime() - date.getTime()) / 1000;
                $el.html("<span class=\"fa fa-fw " + activity.icon + "\"></span><span class=\"aba-tree-name\">" + activity._name + "</span>");
                if (!$flags.length) {
                    tmp = ["append"];
                    $flags = $.make("span", "flags", $.make("span", "aba-flag-show-modules", "fa fa-cogs", "color:firebrick"), $.make("span", "aba-flag-show-feedback", "fa fa-check-circle-o", "color:firebrick", "title=Has Feedback"), $.make("span", "aba-flag-show-root", "fa fa-home", "color:blue"), $.make("span", "aba-flag-show-files", "fa fa-file", "color:blue", "title=Has files"), tmp, $.make("span", "aba-flag-show-comments", "fa fa-comment", "color:blue"), $.make("span", "aba-flag-deleted", "fa fa-trash", "color:black"), $.make("span", "aba-flag-disabled", "fa fa-ban", "color:black"), $.make("span", "aba-flag-timer fa fa-clock-o", "color:red")).insertAfter($el);
                }
                $el.parent()
                    .toggleClass("hidden", !!(activity._flags & 32 && !Editor.permissions.isRoot))
                    .toggleClass("aba-flag-trash", !!(activity._flags & 2))
                    .toggleClass("aba-tree-template", activity.is(1))
                    .removeClass("aba-tree-warning");
                if (activity.is(1)) {
                    $el.parent().toggleClass("hidden", !(Editor.permissions.isOwner || Editor.permissions.isManager || Editor.permissions.canTheme));
                }
                $flags.find(".aba-flag-show-files")
                    .toggleClass("hidden", !((Editor.permissions.isOwner || Editor.permissions.isManager) && activity._files && activity._files.length));
                $flags.find(".aba-flag-deleted")
                    .toggleClass("hidden", !(activity._flags & 2));
                $flags.find(".aba-flag-disabled")
                    .toggleClass("hidden", !(activity._flags & 32));
                $tmp = $flags.find(".aba-flag-timer")
                    .toggleClass("hidden", age > 3600)
                    .attr("title", "Changed " + date.format("R"))
                    .data("date", date);
                if (age < 60) {
                    for (i = 10; i >= 0; i--) {
                        $tmp.fadeOut().fadeIn();
                    }
                }
            }
            return "";
        };
        ProjectsPanel.prototype.deferShowDuplicates = function () {
            this.checkDuplicates.forEach(function (parent) {
                var i, isSame, this_text, prev_text, $this, $prev, $siblings = $(">li.aba-tree-node:not(.aba-flag-trash)>a", parent)
                    .sort(function (a, b) {
                    var a_text = a.textContent, b_text = b.textContent;
                    return a_text < b_text ? -1 : a_text > b_text ? 1 : 0;
                });
                for (i = 1; i < $siblings.length; i++) {
                    $this = $siblings.eq(i);
                    $prev = $siblings.eq(i - 1);
                    this_text = $this.text();
                    prev_text = $prev.text();
                    isSame = !!this_text && this_text === prev_text;
                    $this.closest("li").toggleClass("aba-tree-warning", isSame);
                    if (isSame) {
                        $prev.closest("li").addClass("aba-tree-warning");
                    }
                }
            });
            this.checkDuplicates = [];
        };
        ProjectsPanel.prototype.onAfterTitle = function (id, $el, $parent) {
            var that = ProjectsPanel.instance;
            if (!that.checkDuplicates) {
                that.checkDuplicates = [];
            }
            that.checkDuplicates.pushOnce($parent[0]);
            ic.setImmediate("deferShowDuplicates", that.deferShowDuplicates.bind(that));
        };
        ProjectsPanel.prototype.onBeforeClick = function (id) {
        };
        ProjectsPanel.prototype.onTreeClick = function (id) {
            var num = parseInt(id, 10);
            if (/[^0-9]/.test(id)) {
                var filename = Config.server + "assets/" + id.replace("_", "/"), src = "";
                switch (id.regex(/\.([^\.]+)$/)) {
                    case "gif":
                    case "jpg":
                    case "jpeg":
                    case "png":
                        src = "<img src=\"" + filename + "\" style=\"margin:0 auto;\"/>";
                        break;
                    case "m4a":
                    case "mp3":
                    case "ogg":
                        src = "<audio src=\"" + filename + " style=\"margin:0 auto;\"\"></audio>";
                        break;
                    case "mp4":
                    case "ogv":
                        src = "<vidio src=\"" + filename + " style=\"margin:0 auto;\"\"></vidio>";
                        break;
                    case "css":
                    case "js":
                    case "scss":
                    case "svg":
                        src = "<iframe src=\"" + filename + "\" style=\"width:100%;height:100%;border:0\"></iframe>";
                        break;
                    default:
                        src = filename;
                        break;
                }
                console.log("click on file", filename);
                location.hash = id;
                $("#ic_preview>iframe").attr("srcdoc", "<!DOCTYPE html>\n<html style=\"height:100%;\"><body style=\"height:100%;margin:0;overflow:hidden;\">" + src + "</body></html>");
            }
            else if (ICNodes.has(num)) {
                location.hash = id;
                Editor.fileCache = {};
                Editor.change = false;
                Editor.setActivity(ICNodes.get(num));
                Button.update();
            }
        };
        ProjectsPanel.prototype.onDrag = function (id) {
            return (Editor.permissions.has(1) || Editor.permissions.is(16 | 32)) && (!Editor.currentActivity || Editor.currentActivity._id !== id || !Editor.change);
        };
        ProjectsPanel.prototype.onDrop = function (id, target, moveType) {
            var parent, pos, old_parent = $.tree.getParentId(id), old_pos = $.tree.index(id);
            switch (moveType) {
                case "inner":
                    parent = target;
                    pos = -1;
                    break;
                case "prev":
                    parent = $.tree.getParentId(target);
                    pos = $.tree.index(target);
                    break;
                case "next":
                    parent = $.tree.getParentId(target);
                    pos = $.tree.index(target) + 1;
                    break;
                default:
                    return;
            }
            if (parent === old_parent && pos >= 0 && pos > old_pos) {
                pos--;
            }
            console.log("id", id, "target", target, "type", moveType, "old_parent", old_parent, "old_pos", old_pos, "new_parent", parent, "new_pos", pos, "target_pos", $.tree.index(target));
            if ((old_parent !== parent || old_pos !== pos) && (parent || Editor.permissions.isRoot)) {
                ICNodes.load({
                    "action": "move_node",
                    "id": id,
                    "parent": parent,
                    "pos": pos
                }, function (activity, index) {
                    if (!index) {
                        Editor.findActivityId = activity._id;
                    }
                });
            }
        };
        ProjectsPanel.prototype.onPaste = function (id, target) {
            var isParent = $.tree.isParent(target), parent = isParent ? target : $.tree.getParentId(target), activity = ICNodes.get(id);
            if (!ICNodes.get(parent).getParentsId(true).includes(id)
                && Editor.permissions.isRoot
                || (parent &&
                    (Editor.permissions.isOwner && !activity.is(0) && !activity.is(1))
                    || activity.is(10)
                    || activity.is(11))) {
                ICNodes.load({
                    "action": "clone_node",
                    "id": id,
                    "parent": parent,
                    "pos": isParent ? -1 : $.tree.index(target) + 1
                }, function (activity, index) {
                    if (!index) {
                        Editor.findActivityId = activity._id;
                    }
                });
            }
        };
        ProjectsPanel.prototype.get_templates = function (id) {
            ICNodes.load({
                "action": "get_templates",
                "id": id
            });
        };
        return ProjectsPanel;
    }(Editor.Panel));
    Editor.ProjectsPanel = ProjectsPanel;
})(Editor || (Editor = {}));
;
var Editor;
(function (Editor) {
    var PropertyArea;
    (function (PropertyArea) {
        PropertyArea[PropertyArea["ATTRIBUTE"] = 0] = "ATTRIBUTE";
        PropertyArea[PropertyArea["JSON"] = 1] = "JSON";
        PropertyArea[PropertyArea["STYLE"] = 2] = "STYLE";
        PropertyArea[PropertyArea["WIDGET"] = 3] = "WIDGET";
        PropertyArea[PropertyArea["DATA"] = 4] = "DATA";
    })(PropertyArea || (PropertyArea = {}));
    var Property = (function () {
        function Property(element) {
            this.element = element;
            var property = element.dataset["value"].regex(/^([^\.]+)\.([^=]*)(?:=(.*))?$/);
            this.path = property[1];
            this.value = property[2];
            switch (property[0]) {
                case "attribute":
                    this.area = 0;
                    break;
                case "data":
                    this.area = 4;
                    break;
                case "json":
                    this.area = 1;
                    break;
                case "style":
                    this.area = 2;
                    this.path = $.camelCase(this.path);
                    break;
                case "widget":
                    this.area = 3;
                    break;
                default:
                    throw new Error("new Property(): Unknown type \"" + property[0] + "\"");
            }
            element.icProperty = this;
            $(element).on("change", this, Property.change);
            if (element.nodeName === "INPUT" && element.type === "number") {
                $(element).on("mousewheel", this, Property.change);
            }
        }
        Property.update = function ($panel, widget) {
            $panel.find("[data-value]").each(function () {
                var value, property = this.icProperty || new Property(this);
                switch (property.area) {
                    case 0:
                        if (widget) {
                            value = $(widget).attr(property.path);
                        }
                        break;
                    case 4:
                        value = JSON.parse($(widget).attr("data-json") || "{}").getTree(property.path);
                        break;
                    case 1:
                        if (Editor.currentActivity) {
                            value = Editor.currentActivity.getTree(property.path);
                        }
                        break;
                    case 2:
                        if (widget) {
                            value = window.getComputedStyle(widget, property.path) || "";
                            if (property.value) {
                                value = String(value).regex(new RegExp(property.value.replace("*", "(.*)"), ""));
                            }
                        }
                        break;
                    case 3:
                        if (widget) {
                            value = widget[property.path || "tagName"];
                        }
                        break;
                    default:
                        console.log("Unknown Property Area", property);
                        return;
                }
                if (property.element.nodeName === "INPUT" && property.element.type === "checkbox") {
                    property.element.checked = property.value ? value === property.value : !!value;
                }
                else {
                    $(property.element).val(value);
                }
            });
        };
        Property.change = function (event) {
            var value, property = event.data, widget = Editor.currentEditWidget, input = this;
            if (input.nodeName === "INPUT" && input.type === "checkbox") {
                value = input.checked;
            }
            else {
                value = $(this).val();
            }
            if (/^[\-+]?\d*\.?\d+(?:e[\-+]?\d+)?$/i.test(value)) {
                value = parseFloat(value);
            }
            switch (property.area) {
                case 0:
                    if (!widget) {
                        return;
                    }
                    console.log("Trying to set attribute", property.path, "to", value);
                    if (Property.styleOnParent.includes(property.path)) {
                        if (value) {
                            $(widget.parentElement).attr(property.path, value);
                        }
                        else {
                            $(widget.parentElement).removeAttr(property.path);
                        }
                    }
                    if (value) {
                        $(widget).attr(property.path, value);
                    }
                    else {
                        $(widget).removeAttr(property.path);
                    }
                    widget.fixState();
                    Editor.EditPanel.instance.setHtml();
                    break;
                case 4:
                    if (!widget) {
                        return;
                    }
                    var data = JSON.parse($(widget).attr("data-json") || "{}").setTree(property.path, value === false ? undefined : value);
                    if (Object.keys(data)) {
                        $(widget).attr("data-json", JSON.stringify(data));
                    }
                    else {
                        $(widget).removeAttr("data-json");
                    }
                    Editor.EditPanel.instance.setHtml();
                    break;
                case 1:
                    Editor.currentActivity.setTree(property.path, value);
                    Editor.change = true;
                    Button.update();
                    return;
                case 2:
                    if (!widget) {
                        return;
                    }
                    if (property.value) {
                        if (property.value.includes("*")) {
                            value = property.value.replace("*", value);
                        }
                        else {
                            value = value ? property.value : "";
                        }
                    }
                    console.log("Trying to set style", property.path, "to", value);
                    if (Property.styleOnParent.includes(property.path)) {
                        widget.parentElement.style[property.path] = !isNumber(value) ? value : value >= 0 ? value + "%" : "";
                        widget.style[property.path] = !isNumber(value) ? value : value >= 0 ? value + "%" : "";
                    }
                    else {
                        widget.style[property.path] = !isNumber(value) || value >= 0 ? value : "";
                    }
                    if (property.path === "position") {
                        Editor.Panel.trigger(4, undefined);
                    }
                    Editor.EditPanel.instance.setHtml();
                    break;
                case 3:
                    console.warn("Trying to change a widget value");
                    break;
            }
            RequireGroup.check(PropertiesPanel.instance.$, widget);
        };
        Property.styleOnParent = [
            "ic-position",
            "position",
            "width",
            "height",
            "top",
            "right",
            "bottom",
            "left",
        ];
        return Property;
    }());
    Editor.Property = Property;
    var RequireArea;
    (function (RequireArea) {
        RequireArea[RequireArea["SUPPORT"] = 0] = "SUPPORT";
        RequireArea[RequireArea["WIDGET"] = 1] = "WIDGET";
        RequireArea[RequireArea["WIDGET_ATTRIBUTE"] = 2] = "WIDGET_ATTRIBUTE";
        RequireArea[RequireArea["WIDGET_PARENT"] = 3] = "WIDGET_PARENT";
        RequireArea[RequireArea["WIDGET_PARENT_ATTRIBUTE"] = 4] = "WIDGET_PARENT_ATTRIBUTE";
        RequireArea[RequireArea["WIDGET_STYLE"] = 5] = "WIDGET_STYLE";
    })(RequireArea || (RequireArea = {}));
    var RequireCompare;
    (function (RequireCompare) {
        RequireCompare[RequireCompare["EXIST"] = 0] = "EXIST";
        RequireCompare[RequireCompare["LESS_THAN"] = 1] = "LESS_THAN";
        RequireCompare[RequireCompare["LESS_THAN_OR_EQUAL"] = 2] = "LESS_THAN_OR_EQUAL";
        RequireCompare[RequireCompare["EQUAL"] = 3] = "EQUAL";
        RequireCompare[RequireCompare["NOT_EQUAL"] = 4] = "NOT_EQUAL";
        RequireCompare[RequireCompare["MORE_THAN_OR_EQUAL"] = 5] = "MORE_THAN_OR_EQUAL";
        RequireCompare[RequireCompare["MORE_THAN"] = 6] = "MORE_THAN";
        RequireCompare[RequireCompare["NOT"] = 7] = "NOT";
    })(RequireCompare || (RequireCompare = {}));
    var Require = (function () {
        function Require(area, path, compare, value) {
            this.area = area;
            this.path = path;
            this.compare = compare;
            this.value = value;
        }
        return Require;
    }());
    var RequireGroup = (function () {
        function RequireGroup(element) {
            this.element = element;
            this.requirements = [];
            var requirements = this.requirements;
            element.dataset["require"].split(" ").forEach(function (require) {
                var area, compare, value, query = require.regex(/^([^<=>!]*)(<|<=|>=|=|>|!=|!)?(.*)$/), path = query[0].split("."), stripPath = 0;
                switch (path[0]) {
                    case "support":
                        area = 0;
                        stripPath = 1;
                        break;
                    case "widget":
                        stripPath = 1;
                        if (!path[1]) {
                            area = 1;
                        }
                        else if (path[1] === "style") {
                            stripPath = 2;
                            area = 5;
                        }
                        else if (path[1] === "parent") {
                            stripPath = 2;
                            if (!path[2]) {
                                area = 3;
                            }
                            else {
                                area = 4;
                            }
                        }
                        else {
                            area = 2;
                        }
                        break;
                    default:
                        throw new Error("new RequireGroup(): Unknown type \"" + path[0] + "\"");
                }
                path = path.slice(stripPath);
                switch (query[1]) {
                    case "<":
                        compare = 1;
                        break;
                    case "<=":
                        compare = 2;
                        break;
                    case "=":
                        compare = 3;
                        break;
                    case ">=":
                        compare = 5;
                        break;
                    case ">":
                        compare = 6;
                        break;
                    case "!=":
                        compare = 4;
                        break;
                    case "!":
                        compare = 7;
                        break;
                    default:
                        compare = 0;
                        break;
                }
                value = query[2];
                requirements.push(new Require(area, path, compare, value));
            });
            element.icRequire = this;
        }
        RequireGroup.check = function ($panel, widget) {
            $panel.find("[data-require]").each(function () {
                var i, left, right, parent, require, group = this.icRequire || new RequireGroup(this), show = !!widget;
                for (i = 0; show && i < group.requirements.length; i++) {
                    require = group.requirements[i];
                    right = require.value;
                    switch (require.area) {
                        case 0:
                            left = Editor.rules[require.path[0].toUpperCase()] ? 1 : 0;
                            break;
                        case 1:
                            left = widget.nodeName.toLowerCase();
                            break;
                        case 5:
                            left = widget.style[require.path[0]];
                            break;
                        case 2:
                            left = widget.attributes[require.path[0]] && widget.attributes[require.path[0]].value;
                            break;
                        case 3:
                        case 4:
                            parent = widget.parentElement;
                            if (parent.classList.contains("cke_widget_wrapper")) {
                                parent = parent.parentElement;
                            }
                            if (require.area === 3) {
                                left = parent.nodeName.toLowerCase();
                            }
                            else {
                                left = parent.attributes[require.path[0]] && parent.attributes[require.path[0]].value;
                            }
                            break;
                        default:
                            throw new Error("RequireGroup.check(): Unknown type \"" + require.area + "\"");
                    }
                    switch (require.compare) {
                        case 1:
                            show = parseInt(left) < parseInt(right);
                            break;
                        case 2:
                            show = parseInt(left) <= parseInt(right);
                            break;
                        case 3:
                            show = left == right;
                            break;
                        case 4:
                            show = left != right;
                            break;
                        case 5:
                            show = parseInt(left) >= parseInt(right);
                            break;
                        case 6:
                            show = parseInt(left) > parseInt(right);
                            break;
                        case 7:
                            show = !left;
                            break;
                        case 0:
                            show = !!left;
                            break;
                    }
                }
                $(group.element).toggleClass("disabled", !show).find("input,textarea,select").addBack("input,textarea,select").prop("disabled", !show);
            });
        };
        return RequireGroup;
    }());
    Editor.RequireGroup = RequireGroup;
    var PropertiesPanel = (function (_super) {
        __extends(PropertiesPanel, _super);
        function PropertiesPanel() {
            var _this = _super.call(this, "properties", 1, "preview") || this;
            if (PropertiesPanel.instance) {
                throw new Error("Error: Instantiation failed: Use Properties.getInstance() instead of new.");
            }
            Object.defineProperty(PropertiesPanel, "instance", {
                value: _this
            });
            return _this;
        }
        PropertiesPanel.prototype.on = function (event, args) {
            switch (event) {
                case 0:
                    RequireGroup.check(this.$);
                    Property.update(this.$);
                    break;
                case 3:
                case 4:
                    var widget = !args || args.nodeName === "IMG" ? null : args;
                    this.getClasses();
                    RequireGroup.check(this.$, widget);
                    Property.update(this.$, widget);
                    break;
            }
        };
        ;
        PropertiesPanel.prototype.getClasses = function () {
        };
        PropertiesPanel.tabs = {};
        PropertiesPanel.vars = {};
        PropertiesPanel.requires = {};
        PropertiesPanel.onOptionChange = function ($el, path, callback, star, event) {
            var value = false;
            if ($el.is("button")) {
                value = undefined;
            }
            else if ($el.is("select")) {
                value = $el[0].options[$el[0].selectedIndex].value;
                if (value.search(/^[\-+]?\d*\.?\d+(?:e[\-+]?\d+)?$/i) >= 0) {
                    value = parseFloat(value);
                }
            }
            else if ($el.is("textarea") || $el.attr("type") === "text") {
                value = $el.val();
            }
            else if ($el.attr("type") === "checkbox") {
                value = $el[0].checked;
            }
            if (path[0] === "-") {
                PropertiesPanel.vars[path.replace(/^(-+)/, "")] = value;
            }
            else if (path[0] !== "!" && !path.includes("*") && value !== undefined) {
                if ($el.data("default") == value) {
                }
                else {
                }
            }
            if (callback !== true) {
                this.checkRequire();
            }
            return false;
        };
        return PropertiesPanel;
    }(Editor.Panel));
    Editor.PropertiesPanel = PropertiesPanel;
})(Editor || (Editor = {}));
;
var Editor;
(function (Editor) {
    var scrollTop, scrollHeight;
    var ResourcesPanel = (function (_super) {
        __extends(ResourcesPanel, _super);
        function ResourcesPanel() {
            var _this = _super.call(this, "resources", 1, "right") || this;
            _this.checkScroll = function () {
                var el = getElementById("resource_list");
                if (el && (scrollTop !== el.scrollTop || scrollHeight !== el.scrollHeight)) {
                    scrollTop = el.scrollTop;
                    scrollHeight = el.scrollHeight;
                    var container = el.getBoundingClientRect(), top = container.top - 25, bottom = container.bottom + 25;
                    [].forEach.call(el.children, function (el) {
                        var box = el.getBoundingClientRect();
                        el.classList.toggle("defer", !(box.bottom >= top && box.top <= bottom));
                    });
                }
                rAF(_this.checkScroll);
            };
            if (ResourcesPanel.instance) {
                throw new Error("Error: Instantiation failed: Use ResourcesPanel.instance instead of new.");
            }
            Object.defineProperty(ResourcesPanel, "instance", {
                value: _this
            });
            $("#resource_list").draggable({
                appendTo: "body",
                containment: "window",
                cursor: "grabbing",
                cursorAt: {
                    top: 25,
                    left: 25
                },
                handle: ".media",
                opacity: 0.7,
                scroll: false,
                zIndex: 100,
                helper: function (event) {
                    console.log("arguments", arguments);
                    var template, $media = $(event.target.closest(".media")), media = $media[0].cloneNode(true), src = $media.data("src");
                    if (media.classList.contains("image")) {
                        template = $("<template class=\"resource\"><img src=\"" + src + "\" style=\"width:100%;height:auto;\"/></template>")[0];
                    }
                    else if (media.classList.contains("video")) {
                        template = $("<template class=\"resource\"><ic-box><video src=\"" + src + "\"></video></ic-box></template>")[0];
                    }
                    else if (media.classList.contains("audio")) {
                        template = $("<template class=\"resource\"><ic-audio><audio src=\"" + src + "\"></audio></ic-audio></template>")[0];
                    }
                    console.log("template", template);
                    $(this).data("template", template);
                    media.classList.add("component");
                    media.firstElementChild.className = "";
                    return media;
                },
                start: function (event, ui) {
                    $("iframe").css("pointer-events", "none");
                    Editor.Panel.trigger(5, $(this).data("template"));
                },
                drag: function (event, ui) {
                    Editor.Panel.trigger(6, $(this).data("template"), { top: event.clientY, left: event.clientX });
                },
                stop: function (event, ui) {
                    $("iframe").css("pointer-events", "");
                    Editor.Panel.trigger(7, $(this).data("template"), { top: event.clientY, left: event.clientX });
                }
            });
            rAF(_this.checkScroll);
            return _this;
        }
        ResourcesPanel.prototype.on = function (event, args) {
            switch (event) {
                case 2:
                    this.changeActivity();
                    break;
            }
        };
        ;
        ResourcesPanel.prototype.sort = function () {
            var $list = $("#resource_list"), sort = Editor.getSetting("options.resource_sort"), parents = sort === "tree" ? Editor.currentActivity.getParentsId(true) : null, siblings = sort === "tree" ? Editor.currentActivity.getSiblingsId().reverse() : null;
            $list.children().sort(function (a, b) {
                var atext = a.lastElementChild.textContent, btext = b.lastElementChild.textContent;
                switch (sort) {
                    case "tree":
                        var anumber = parseInt(atext, 10), bnumber = parseInt(btext, 10), aindex = parents.indexOf(anumber) || Number.NEGATIVE_INFINITY, bindex = parents.indexOf(bnumber) || Number.NEGATIVE_INFINITY;
                        if (aindex === -1) {
                            aindex = -siblings.indexOf(anumber);
                        }
                        if (bindex === -1) {
                            bindex = -siblings.indexOf(bnumber);
                        }
                        if (aindex > bindex) {
                            return 1;
                        }
                        if (aindex < bindex) {
                            return -1;
                        }
                    default:
                        return atext.substr(atext.indexOf("/")).naturalCaseCompare(btext.substr(btext.indexOf("/")));
                }
            }).appendTo($list);
            this.update();
        };
        ResourcesPanel.prototype.update = function () {
            this.scrollTop = -1;
        };
        ResourcesPanel.prototype.changeActivity = function () {
            var activity = Editor.currentActivity;
            if (activity && this.lastActivity !== activity) {
                this.lastActivity = activity;
                console.log("change activity", this);
                scrollTop = undefined;
                ic.setImmediate("resource", function () {
                    $("#ic_resources>span>span").each(function (index, el) {
                        var $this = $(this), select = $this.data("select"), value = Editor.getSetting("options." + select);
                        $("#ic_resources").addClass($this.children("i" + (isUndefined(value) ? ":first-child" : "[data-option='" + value + "']")).addClass("active").data("option"));
                    });
                    $("#ic_resources>span>span>i").on("click", function () {
                        var $this = $(this), $siblings = $this.siblings("i").removeClass("active"), select = $this.parent().data("select"), isSort = select === "resource_sort";
                        $this.toggleClass("active", $siblings.length ? true : undefined);
                        Editor.setSetting("options." + select, $siblings.length || $this.hasClass("active") ? $this.data("option") : "");
                        $("#ic_resources").removeClass();
                        $("#ic_resources>span>span>i.active").each(function (index, el) {
                            $("#ic_resources").addClass($(this).data("option"));
                        });
                        if (isSort) {
                            ResourcesPanel.instance.sort();
                        }
                        else {
                            ResourcesPanel.instance.update();
                        }
                    });
                    $("#resource_search").on("keyup change", function () {
                        var value = this.value.trim().toLowerCase();
                        $("#resource_list").children().each(function (index, el) {
                            var image = this;
                            image.style.display = image.innerText.toLowerCase().indexOf(value) >= 0 ? "" : "none";
                        });
                        ResourcesPanel.instance.update();
                    });
                });
                Editor.currentActivity.getTemplateFolders().then(function (nodes) {
                    var files = [], id = Editor.currentActivity._id, lastFolder = -1, $target = $("#resource_list").empty(), getFileType = function (path) {
                        switch (path.regex(/\.([^\.]+)$/).toLowerCase()) {
                            case "gif":
                            case "jpg":
                            case "png":
                            case "svg":
                                return Editor.MediaType.IMAGE;
                            case "mp3":
                            case "m4a":
                            case "ogg":
                                return Editor.MediaType.AUDIO;
                            case "mp4":
                            case "m4v":
                                return Editor.MediaType.VIDEO;
                        }
                        return Editor.MediaType.UNKNOWN;
                    }, showFile = function (path, data) {
                        var filePath = "../" + path, fileName = path.regex(/([^\/]+)$/), fileExt = fileName.regex(/\.([^\.]+)$/).toLowerCase(), folder = path.regex(/(\d+)/), url = data || encodeURI(filePath).replace(/'/g, "%27"), $tile = $("<a download href=\"" + url + "\" target=\"_blank\" class=\"media defer\" title=\"" + filePath + "\"></a>");
                        switch (getFileType(path)) {
                            case Editor.MediaType.IMAGE:
                                $tile
                                    .addClass("image")
                                    .append("<div class=\"fa fa-file-image-o\" data-type=\"" + fileExt + "\" style=\"background-image:url('" + url + "');\"></div>");
                                break;
                            case Editor.MediaType.AUDIO:
                                $tile
                                    .addClass("audio")
                                    .append("<div class=\"fa fa-file-audio-o\" data-type=\"" + fileExt + "\"><audio src=\"" + url + "\" controls></audio></div>");
                                break;
                            case Editor.MediaType.VIDEO:
                                $tile
                                    .addClass("video")
                                    .append("<div class=\"fa fa-file-video-o\" data-type=\"" + fileExt + "\"><video src=\"" + url + "\" controls></video></div>");
                                break;
                            default:
                                return;
                        }
                        if (lastFolder !== folder) {
                            lastFolder = folder;
                            $("<div><h3><span>" + folder + "/&#009;&#009;&#009;&#009;&#009;</span>" + (ICNodes.get(folder)._name || folder) + "</h3></div>")
                                .appendTo($target);
                        }
                        $tile
                            .append("<h3><span>" + path.regex(/(\d+)/) + "/</span>" + fileName.replace(/\.[^\.]+$/, "") + "<span>." + fileExt + "</span></h3>")
                            .on("click", Editor.preventDefault)
                            .data("src", filePath)
                            .appendTo($target);
                    };
                    nodes.pushOnce.apply(nodes, Editor.currentActivity.getParents(true));
                    nodes.pushOnce.apply(nodes, Editor.currentActivity.getSiblings());
                    nodes.forEach(function (node) {
                        if (!(node._flags & (2 | 32))) {
                            files.pushOnce.apply(files, node._files || []);
                        }
                    });
                    for (var fileName in Editor.fileCache) {
                        if (Editor.fileCache.hasOwnProperty(fileName)) {
                            var filePath = "assets/" + id + "/" + fileName, fileExt = fileName.regex(/\.([^\.]+)$/).toLowerCase();
                            files.remove("../" + filePath);
                            showFile(filePath, "data:image/" + fileExt + ";base64," + Editor.fileCache[fileName]);
                        }
                    }
                    files.naturalCaseSort().forEach(function (path) {
                        showFile(path);
                    });
                    ResourcesPanel.instance.sort();
                });
            }
        };
        return ResourcesPanel;
    }(Editor.Panel));
    Editor.ResourcesPanel = ResourcesPanel;
})(Editor || (Editor = {}));
;
var Editor;
(function (Editor) {
    var updating;
    var SourcePanel = (function (_super) {
        __extends(SourcePanel, _super);
        function SourcePanel() {
            var _this = _super.call(this, "source", 1, "preview") || this;
            if (SourcePanel.instance) {
                throw new Error("Error: Instantiation failed: Use SourcePanel.instance instead of new.");
            }
            Object.defineProperty(SourcePanel, "instance", {
                value: _this
            });
            _this.element = createElement("div");
            var editor = _this.editor = ace.edit("ic_source"), session = _this.session = editor.getSession();
            editor.setTheme("ace/theme/xcode");
            editor.$blockScrolling = Infinity;
            editor.setShowPrintMargin(false);
            editor.on("change", _this.onChange);
            session.setMode("ace/mode/html");
            session.setUseSoftTabs(false);
            editor.setOption("showInvisibles", true);
            session.on("changeAnnotation", _this.onAnnotation);
            setInterval(_this.onChangeCallback, 100);
            return _this;
        }
        SourcePanel.prototype.on = function (event, args) {
            switch (event) {
                case 3:
                    if (!updating) {
                        this.setActivityHtml();
                    }
                    break;
            }
        };
        ;
        SourcePanel.prototype.onClick = function (event) {
            _super.prototype.onClick.call(this, event);
            this.editor.resize();
        };
        SourcePanel.prototype.onAnnotation = function () {
            var that = SourcePanel.instance, annotations = that.session.getAnnotations() || [], i = annotations.length, errors = false, change = false;
            while (i--) {
                var annotation = annotations[i];
                if (/doctype first\. Expected/.test(annotation.text)) {
                    change = true;
                    annotations.splice(i, 1);
                }
                else if (!errors && annotation.type === "error") {
                    errors = true;
                }
            }
            if (change) {
                that.session.setAnnotations(annotations);
            }
            if (!errors && that.errors) {
                that.setActivityHtml();
            }
            that.errors = errors;
        };
        SourcePanel.prototype.onChangeCallback = function () {
            var that = SourcePanel.instance;
            if (that.changed && !that.errors) {
                var source = that.editor.getValue();
                that.changed = false;
                if (that.lastSource !== source) {
                    that.editing = true;
                    updating = true;
                    Editor.setHtml(source);
                    updating = false;
                    that.lastSource = Editor.currentActivity.pretty;
                }
            }
        };
        SourcePanel.prototype.onChange = function (change) {
            SourcePanel.instance.changed = true;
        };
        SourcePanel.prototype.setActivityHtml = function () {
            var source = Editor.currentActivity.pretty;
            if (!source) {
                ic.setImmediate("source.editor", this.setActivityHtml.bind(this));
            }
            else {
                ic.setImmediate("source.editor");
                if (this.lastSource !== source || this.lastId !== Editor.currentActivity._id) {
                    this.lastSource = source;
                    if (this.lastId === Editor.currentActivity._id) {
                        if (!this.editing) {
                            this.editor.setValue(source, -1);
                        }
                        this.editing = false;
                    }
                    else {
                        this.lastId = Editor.currentActivity._id;
                        this.session.setValue(source);
                    }
                }
            }
        };
        return SourcePanel;
    }(Editor.Panel));
    Editor.SourcePanel = SourcePanel;
})(Editor || (Editor = {}));
;
var Editor;
(function (Editor) {
    var TopPanel = (function (_super) {
        __extends(TopPanel, _super);
        function TopPanel() {
            var _this = _super.call(this, "header", 0, "top") || this;
            if (TopPanel.instance) {
                throw new Error("Error: Instantiation failed: Use TopPanel.instance instead of new.");
            }
            Object.defineProperty(TopPanel, "instance", {
                value: _this
            });
            return _this;
        }
        TopPanel.prototype.on = function (event, args) {
            switch (event) {
            }
        };
        ;
        return TopPanel;
    }(Editor.Panel));
    Editor.TopPanel = TopPanel;
})(Editor || (Editor = {}));
;
var Editor;
(function (Editor) {
    var UsersPanel = (function (_super) {
        __extends(UsersPanel, _super);
        function UsersPanel() {
            var _this = _super.call(this, "users", 1 | 2, "left") || this;
            if (UsersPanel.instance) {
                throw new Error("Error: Instantiation failed: Use UsersPanel.instance instead of new.");
            }
            Object.defineProperty(UsersPanel, "instance", {
                value: _this
            });
            if (!w2ui.user) {
                $().w2form({
                    name: "user",
                    style: "border:0px;background-color:transparent;",
                    formHTML: '<div class="w2ui-page page-0">' +
                        '    <div class="w2ui-field">' +
                        '        <label>#ID:</label>' +
                        '        <div>' +
                        '           <input name="user_id" type="text" maxlength="10" style="width:300px;" disabled/>' +
                        '        </div>' +
                        '    </div>' +
                        '    <div class="w2ui-field w2ui-required user-email">' +
                        '        <label>Email:</label>' +
                        '        <div>' +
                        '           <input name="email" type="email" maxlength="512" style="width:300px;"/>' +
                        '        </div>' +
                        '    </div>' +
                        '    <div class="w2ui-field user-password">' +
                        '        <label>Set Password:</label>' +
                        '        <div>' +
                        '           <input name="reset" type="text" maxlength="512" style="width:300px;"/>' +
                        '        </div>' +
                        '    </div>' +
                        '    <hr/>' +
                        '    <div class="w2ui-field">' +
                        '        <label>Admin User:</label>' +
                        '        <div>' +
                        '            <label><input name="flag_admin" type="checkbox"/>' +
                        '            Manage everything</label>' +
                        '        </div>' +
                        '    </div>' +
                        '    <hr/>' +
                        '    <div class="w2ui-field">' +
                        '        <label>Owner:</label>' +
                        '        <div>' +
                        '            <label><input name="flag_owner" type="checkbox"/>' +
                        '            Manage users</label>' +
                        '        </div>' +
                        '    </div>' +
                        '    <div class="w2ui-field">' +
                        '        <label>Manager:</label>' +
                        '        <div>' +
                        '            <label><input name="flag_manager" type="checkbox"/>' +
                        '            Manage projects</label>' +
                        '        </div>' +
                        '    </div>' +
                        '    <hr/>' +
                        '    <div class="w2ui-field">' +
                        '        <label>View:</label>' +
                        '        <div>' +
                        '            <label><input name="flag_view" type="checkbox"/>' +
                        '            View all screens</label>' +
                        '        </div>' +
                        '    </div>' +
                        '    <div class="w2ui-field">' +
                        '        <label>Create:</label>' +
                        '        <div>' +
                        '            <label><input name="flag_create" type="checkbox"/>' +
                        '            Create screens</label>' +
                        '        </div>' +
                        '    </div>' +
                        '    <div class="w2ui-field">' +
                        '        <label>Delete:</label>' +
                        '        <div>' +
                        '            <label><input name="flag_delete" type="checkbox"/>' +
                        '            Delete screens</label>' +
                        '        </div>' +
                        '    </div>' +
                        '    <hr/>' +
                        '    <div class="w2ui-field">' +
                        '        <label>Content:</label>' +
                        '        <div>' +
                        '            <label><input name="flag_content" type="checkbox"/>' +
                        '            Edit screen content</label>' +
                        '        </div>' +
                        '    </div>' +
                        '    <div class="w2ui-field">' +
                        '        <label>Theme:</label>' +
                        '        <div>' +
                        '            <label><input name="flag_theme" type="checkbox"/>' +
                        '            Edit screen look</label>' +
                        '        </div>' +
                        '    </div>' +
                        '    <div class="w2ui-field">' +
                        '        <label>Files:</label>' +
                        '        <div>' +
                        '            <label><input name="flag_files" type="checkbox"/>' +
                        '            Edit and upload files</label>' +
                        '        </div>' +
                        '    </div>' +
                        '</div>' +
                        '<div class="w2ui-buttons">' +
                        '    <button class="btn" name="save">Save</button>' +
                        '</div>',
                    fields: [
                        {
                            field: "user_id",
                            type: "text"
                        },
                        {
                            field: "email",
                            type: "email",
                            required: true
                        },
                        {
                            field: "reset",
                            type: "text"
                        },
                        {
                            field: "flag_admin",
                            type: "checkbox"
                        },
                        {
                            field: "flag_owner",
                            type: "checkbox"
                        },
                        {
                            field: "flag_manager",
                            type: "checkbox"
                        },
                        {
                            field: "flag_view",
                            type: "checkbox"
                        },
                        {
                            field: "flag_create",
                            type: "checkbox"
                        },
                        {
                            field: "flag_delete",
                            type: "checkbox"
                        },
                        {
                            field: "flag_content",
                            type: "checkbox"
                        },
                        {
                            field: "flag_theme",
                            type: "checkbox"
                        },
                        {
                            field: "flag_files",
                            type: "checkbox"
                        }
                    ],
                    actions: {
                        "save": _this.saveUser.bind(_this)
                    }
                });
            }
            return _this;
        }
        UsersPanel.prototype.on = function (event, args) {
            switch (event) {
                case 1:
                    this.onLoaded();
                    break;
            }
        };
        ;
        UsersPanel.prototype.loadGroups = function () {
            var _this = this;
            Editor.ajax({
                type: "POST",
                url: "ajax.php",
                global: false,
                dataType: "json",
                data: {
                    action: "get_group",
                    id: -1,
                },
                success: function (data) {
                    console.log("Groups: ", data);
                    if (!_this.groups) {
                        w2ui.users.remove("load");
                        _this.groups = [];
                    }
                    _this.groups.forEach(function (group) {
                        if (group) {
                            w2ui.users.remove("group-" + group.id);
                        }
                    });
                    data.sort(function (a, b) {
                        var a_lower = a.name.toLowerCase(), b_lower = b.name.toLowerCase();
                        return a_lower > b_lower ? 1 : a_lower < b_lower ? -1 : 0;
                    }).forEach(function (group) {
                        _this.groups[group.id] = group;
                        w2ui.users.add([
                            { id: "group-" + group.id, text: group.name, icon: "fa fa-users", count: "#" + group.node }
                        ]);
                    });
                }
            });
        };
        UsersPanel.prototype.loadUsers = function (id) {
            var _this = this;
            Editor.ajax({
                type: "POST",
                url: "ajax.php",
                global: false,
                dataType: "json",
                data: {
                    action: "get_users",
                    id: id,
                },
                success: function (data) {
                    console.log("Users: ", data);
                    _this.users = _this.users || [];
                    _this.users.forEach(function (user, index) {
                        if (user && user.group === id) {
                            w2ui.users.remove("user-" + user.id);
                        }
                    });
                    data.sort(function (a, b) {
                        var a_lower = a.email.toLowerCase(), b_lower = b.email.toLowerCase();
                        return a_lower > b_lower ? 1 : a_lower < b_lower ? -1 : 0;
                    }).forEach(function (user) {
                        _this.users[user.id] = user;
                        user.group = _this.currentGroup;
                        w2ui.users.insert("group-" + id, null, [
                            { id: "user-" + user.id, text: user.email === Editor.username ? "<em>" + user.email + "</em>" : user.email, icon: "fa fa-user" }
                        ]);
                        w2ui.users.expand("group-" + id);
                    });
                }
            });
        };
        UsersPanel.prototype.editUser = function (id) {
            var user = id >= 0 ? this.users[id] : null, permission = new Permission(user ? user.permission : 0);
            this.currentId = id;
            this.popup = $().w2popup({
                title: "<i class=\"fa fa-user\"></i> " + (user ? "Edit" : "Create") + " User",
                body: "<div id=\"user\" style=\"width:100%;height:100%;\"></div>",
                style: "padding: 15px 0px 0px 0px",
                width: 500,
                height: 490,
                onToggle: function (event) {
                    $(w2ui.user.box).hide();
                    event.onComplete = function () {
                        $(w2ui.user.box).show();
                        w2ui.user.resize();
                    };
                },
                onOpen: function (event) {
                    w2ui.user.clear();
                    w2ui.user.record = {
                        user_id: String(id),
                        email: user ? user.email : "",
                        flag_admin: permission.isRoot,
                        flag_owner: permission.isOwner,
                        flag_manager: permission.isManager,
                        flag_view: permission.canView,
                        flag_create: permission.canCreate,
                        flag_delete: permission.canDelete,
                        flag_content: permission.canContent,
                        flag_theme: permission.canTheme,
                        flag_files: permission.canFile
                    };
                    w2ui.user.refresh();
                    event.onComplete = function () {
                        $("#user").w2render("user");
                        $(".user-email input").prop("disabled", id >= 0 ? true : false);
                        if (!Editor.permissions.isRoot) {
                            $(".user-password").hide();
                        }
                    };
                }
            });
        };
        UsersPanel.prototype.saveUser = function () {
            var _this = this;
            var record = w2ui.user.record, user = record.user_id > 0 ? this.users[record.user_id] : null, old_permission = new Permission(user ? user.permission : 0), new_permission = new Permission(0);
            new_permission.isRoot = record.flag_admin;
            new_permission.isOwner = record.flag_owner;
            new_permission.isManager = record.flag_manager;
            new_permission.canView = record.flag_view;
            new_permission.canCreate = record.flag_create;
            new_permission.canDelete = record.flag_delete;
            new_permission.canContent = record.flag_content;
            new_permission.canTheme = record.flag_theme;
            new_permission.canFile = record.flag_files;
            console.log("record", record, old_permission.raw, new_permission.raw);
            if (user) {
                if (!old_permission.is(new_permission)) {
                    console.log("Permissions have changed");
                    Editor.ajax({
                        type: "POST",
                        url: "ajax.php",
                        global: false,
                        dataType: "json",
                        data: {
                            action: "set_permissions",
                            id: record.user_id,
                            permissions: new_permission.raw
                        },
                        success: function (data) {
                            _this.loadUsers(_this.currentGroup);
                        }
                    });
                }
                if (record.email !== user.email) {
                    console.log("Email has changed");
                    alert("Email change not implemented");
                }
                if (Editor.permissions.isRoot && record.reset) {
                    console.log("Password has changed");
                    Editor.ajax({
                        type: "POST",
                        url: "ajax.php",
                        global: false,
                        dataType: "json",
                        data: {
                            action: "change_password",
                            id: record.user_id,
                            oldPassword: "",
                            newPassword: record.reset
                        },
                        success: function (data) {
                            _this.loadUsers(_this.currentGroup);
                        }
                    });
                }
            }
            else {
                console.log("Create a new user");
                Editor.ajax({
                    type: "POST",
                    url: "ajax.php",
                    global: false,
                    dataType: "json",
                    data: {
                        action: "create_user",
                        group: this.currentGroup,
                        email: record.email,
                        permissions: new_permission.raw,
                        password: record.reset
                    },
                    success: function (data) {
                        _this.loadUsers(_this.currentGroup);
                    }
                });
            }
            this.popup.close();
        };
        UsersPanel.prototype.onLoaded = function () {
            var _this = this;
            if (Editor.permissions.isRoot) {
                $("#ic_users>div.ic_users").w2sidebar({
                    name: "users",
                    nodes: [{ id: "load", text: "Load...", icon: "fa fa-cloud-download" }]
                });
                w2ui.users.on("*", function (event) {
                    if (event.type === "click") {
                        var eventId = event.target, isGroup = _this.isGroup = /^group-/.test(eventId), id = _this.currentId = eventId.regex(/-([0-9]+)$/);
                        if (eventId === "load") {
                            _this.loadGroups();
                        }
                        else if (isGroup) {
                            _this.currentGroup = id;
                            if (!_this.groups[id].loaded) {
                                _this.groups[id].loaded = true;
                                _this.loadUsers(id);
                            }
                        }
                        Button.update();
                    }
                });
            }
            else {
                $("#tabs_layout_" + this.side + "_tabs_tab_users,#ic_users").hide();
            }
        };
        return UsersPanel;
    }(Editor.Panel));
    Editor.UsersPanel = UsersPanel;
})(Editor || (Editor = {}));
;
new Button("#ic_button_clone", 1 | 4 | 16, function (activity) {
    if (activity.isGroup && !confirm("Are you sure you wish to clone this entire tree?")) {
        return;
    }
    ICNodes.load({
        "action": "clone_node",
        "id": $.tree.currentId
    }, function (activity, index) {
        if (!index) {
            Editor.findActivityId = activity._id;
        }
    });
}, function (activity) {
    return activity && (Editor.permissions.isRoot
        || (Editor.permissions.isOwner && !activity.is(0) && !activity.is(1))
        || activity.is(10)
        || activity.is(11));
});
new Button("#ic_button_disable", 1, function () {
    ICNodes.load({
        "action": "disable_node",
        "id": $.tree.currentId
    });
}, function (activity) {
    return activity && !activity.is(0) && !Editor.change && !!activity._parent;
});
new Button("#ic_button_folder", 1 | 4 | 16, function (activity, event) {
    var parentId = $.tree.closest();
    ICNodes.load({
        "action": "create_node",
        "parent": parentId,
        "data": {
            "_name": ICNodes.getNamePrefix(parseInt(parentId, 10), "New Folder"),
            "_type": 3
        }
    }, function (activity, index) {
        if (!index) {
            Editor.findActivityId = activity._id;
        }
    });
}, function (activity) {
    return activity && ICNodeTypeChildren[activity._type].includes(3);
});
new Button("#ic_button_group", 1 | 4 | 16, function (activity, event) {
    var parentId = $.tree.closest();
    ICNodes.load({
        "action": "create_node",
        "parent": parentId,
        "data": {
            "_name": ICNodes.getNamePrefix(parseInt(parentId, 10), "New Group"),
            "_type": 4
        }
    }, function (activity, index) {
        if (!index) {
            Editor.findActivityId = activity._id;
        }
    });
}, function (activity) {
    return activity && ICNodeTypeChildren[activity._type].includes(4);
});
new Button("#ic_button_organisation", 1, function (activity, event) {
    var name = prompt("Please enter the name for the new Organisation folder, blank to cancel...", ICNodes.getNamePrefix(0, "New Organisation"));
    if (name) {
        ICNodes.load({
            "action": "create_node",
            "parent": 0,
            "data": {
                "_name": name,
                "_type": 0
            }
        }, function (activity, index) {
            if (!index) {
                Editor.findActivityId = activity._id;
            }
        });
    }
}, function (activity) {
    return !activity || activity.is(0);
});
new Button("#ic_button_project", 1 | 4, function () {
    var parentId = parseInt($.tree.closest(), 10);
    ICNodes.load({
        "action": "create_node",
        "parent": parentId,
        "data": {
            "_name": ICNodes.getNamePrefix(parentId, "New Project"),
            "_type": 2
        }
    }, function (activity, index) {
        if (!index) {
            Editor.findActivityId = activity._id;
        }
    });
}, function (activity) {
    return activity && ICNodeTypeChildren[activity._type].includes(2);
});
new Button("#ic_button_add", 1 | 4 | 16, function () {
    var parentId = parseInt($.tree.closest(), 10), createNode = function () {
        var template = $(this).find("iframe").data("template");
        w2popup.close();
        ICNodes.load({
            "action": "create_node",
            "parent": parentId,
            "data": {
                "_name": ICNodes.getNamePrefix(parentId, "New Screen"),
                "_type": 10,
                "_html": template.html
            }
        }, function (activity, index) {
            if (!index) {
                Editor.findActivityId = activity._id;
            }
        });
    }, loadPreview = function (event, ui) {
        var $panel = ui.newPanel || $("#new_screen_popup>div").first(), projectId = (Editor.currentActivity.getParents(true).find(function (activity) {
            return activity.is(2) || activity.is(3) || activity.is(0);
        }) || {})._id, loadIFrame = function () {
            var iframe = this, $iframe = $(iframe), doc = iframe.contentDocument, template = $iframe.data("template");
            template.build(projectId).then(function (dom) {
                var head = doc.head, body = doc.body, inHead = true;
                $(dom).contents().each(function (i, el) {
                    if (el.nodeName === "SCRIPT") {
                        return;
                    }
                    else if (inHead && !["TITLE", "META", "LINK", "STYLE"].includes(el.nodeName)) {
                        var link = doc.createElement("link");
                        link.rel = "stylesheet";
                        link.type = "text/css";
                        link.href = "include/template.css";
                        head.appendChild(link);
                        inHead = false;
                    }
                    (inHead ? head : body).appendChild(el);
                });
                var walker = document.createNodeIterator(doc, NodeFilter.SHOW_ELEMENT, null, false), el = document.createElement("SPAN");
                for (var node = walker.nextNode(); node; node = walker.nextNode()) {
                    if (node.nodeName.startsWith("IC-")) {
                        el.fixState.call(node);
                    }
                }
                $iframe.prev().css("visibility", "hidden");
            });
        };
        $panel.find("img:not([style])+iframe").each(loadIFrame);
    }, $popup = $("<div>"), w2popup = $().w2popup({
        title: "<i class=\"fa fa-object-group\"></i> New Screen",
        body: "<div id=\"new_screen_popup\"></div>",
        style: "",
        width: window.innerWidth * 0.8,
        height: window.innerHeight * 0.8,
        onOpen: function () {
            ic.setImmediate("new_screen", function () {
                $("#new_screen_popup").append($popup.children()).accordion({
                    heightStyle: "content",
                    beforeActivate: loadPreview,
                    create: loadPreview
                });
            });
        }
    });
    Editor.currentActivity.getTemplates().then(function (templates) {
        var panels = {};
        templates.forEach(function (template) {
            var parent = template.getParent()._name, name = template._name;
            if (!panels[parent]) {
                panels[parent] = {};
            }
            if (!panels[parent][name]) {
                panels[parent][name] = template;
            }
        });
        Object.keys(panels).sort(function (a, b) {
            a = a.toUpperCase();
            b = b.toUpperCase();
            if (a === "TEMPLATES" || a < b) {
                return -1;
            }
            if (b === "TEMPLATES" || a > b) {
                return 1;
            }
            return 0;
        }).forEach(function (panelName) {
            var panel = panels[panelName], $panel = $.make("div"), hasScreens = false, $target = $("#new_screen_popup").add($popup).first();
            Object.keys(panel).sort().forEach(function (screenName) {
                var template = panel[screenName];
                if (!hasScreens) {
                    hasScreens = true;
                    $.make("h3", "=" + panelName).add($panel).appendTo($target);
                }
                $.make("div", $.make("div", $.make("img", "src=include/icons/screenshot.png"), $.make("iframe", "ns_" + template._id)
                    .data("template", template)), $.make("h4", "=" + screenName))
                    .on("click", createNode)
                    .appendTo($panel);
            });
        });
        $("#new_screen_popup.ui-accordion").accordion("refresh");
    });
}, function (activity) {
    var parent = activity ? activity.getParent() : null;
    return activity && (ICNodeTypeChildren[activity._type].includes(10)
        || (parent && ICNodeTypeChildren[parent._type].includes(10)));
});
new Button("#ic_button_template", 1 | 4, function () {
    var parentId = parseInt($.tree.closest(), 10);
    ICNodes.load({
        "action": "create_node",
        "parent": parentId,
        "data": {
            "_name": ICNodes.getNamePrefix(parentId, "Template"),
            "_type": 13
        }
    }, function (activity, index) {
        if (!index) {
            Editor.findActivityId = activity._id;
        }
    });
}, function (activity) {
    return activity
        && activity.getParents(true).some(function (parent) {
            return parent && parent.is(1);
        });
});
new Button("#ic_button_remove", 1 | 4 | 32, function () {
    ICNodes.load({
        "action": "delete_node",
        "id": $.tree.currentId
    });
}, function (activity) {
    return activity && (Editor.permissions.isRoot || !activity.is(0)) && !Editor.change && !!activity._parent;
});
new Button("#ic_button_search", 1 | 4 | 8, function () {
    var popup, click = function () {
        var id = parseFloat($(this).val());
        popup.close();
        if (ICNodes.has(id)) {
            $.tree.click(String(id));
        }
        else {
            ICNodes.loadParents(Editor.findActivityId = id);
        }
    }, search = function () {
        var $id = $("#search input[name='id']"), crc = $("#search input[name='crc']").val(), id = parseFloat($id.val()), contains = $("#search input[name='contains']").val(), $select = $("#search select[name='results']");
        if (id && !isNaN(id)) {
            click.call($id[0]);
        }
        else if (crc) {
            if (!/^[0-9a-f]{1,8}$/i.test(crc)) {
                return;
            }
            for (var i = 0; i < 100000; i++) {
                if (String(i).crc32() === crc) {
                    $id.val(String(i));
                    click.call($id[0]);
                    return;
                }
            }
        }
        else if (contains) {
            popup.lock();
            Editor.ajax({
                type: "POST",
                url: "ajax.php",
                dataType: "json",
                data: {
                    action: "find_node",
                    path: contains
                },
                success: function (data) {
                    var key;
                    popup.unlock();
                    $select.empty();
                    for (key in data) {
                        $select.append("<option value=\"" + key + "\">" + data[key] + "</option>");
                    }
                }
            });
        }
    };
    if (!w2ui.search) {
        $().w2form({
            name: "search",
            style: "border:0px;background-color:transparent;",
            formHTML: '<div class="w2ui-page page-0">' +
                '    <div class="w2ui-field">' +
                '        <label>#ID:</label>' +
                '        <div style="display:inline-block;margin:0 0 3px 8px;float:left;">' +
                '           <input name="id" type="number" autocomplete="off" inputmode="numeric" maxlength="10" style="width:100px;"/>' +
                '        </div>' +
                '        <div style="display:inline-block;margin:0 0 3px 0;">' +
                '	        <label>#CRC:</label>' +
                '           <input name="crc" type="search" autocomplete="off" autocorrect="off" maxlength="8" style="width:100px;"/>' +
                '        </div>' +
                '    </div>' +
                '    <div class="w2ui-field">' +
                '        <label>Contains:</label>' +
                '        <div>' +
                '            <input name="contains" type="text" maxlength="100" style="width:300px;"/>' +
                '        </div>' +
                '    </div>' +
                '    <div class="w2ui-field">' +
                '        <label>Results:</label>' +
                '        <div>' +
                '            <select name="results" multiple style="width:300px;height:90px;"></select>' +
                '        </div>' +
                '    </div>' +
                '</div>' +
                '<div class="w2ui-buttons">' +
                '    <button class="btn" name="reset">Reset</button>' +
                '    <button class="btn" name="search">Search</button>' +
                '</div>',
            fields: [
                {
                    field: "id",
                    type: "number"
                },
                {
                    field: "contains",
                    type: "text"
                }
            ],
            actions: {
                "reset": function () {
                    this.clear();
                    $("#search select[name='results']").empty();
                },
                "search": search
            }
        });
    }
    popup = $().w2popup({
        title: "<i class=\"fa fa-question-circle\"></i> Search",
        body: "<div id=\"search\" style=\"width:100%;height:100%;\"></div>",
        style: "padding: 15px 0px 0px 0px",
        width: 500,
        height: 300,
        onToggle: function (event) {
            $(w2ui.search.box).hide();
            event.onComplete = function () {
                $(w2ui.search.box).show();
                w2ui.search.resize();
            };
        },
        onOpen: function (event) {
            event.onComplete = function () {
                $("#search").w2render("search");
                $("#search select[name='results']").on("change", click);
                $("#search input").on("keypress", function (event) {
                    if (event.which == 13) {
                        search();
                        return false;
                    }
                });
            };
        }
    });
}, function (activity) {
    return true;
});
new Button("#ic_button_files", 1 | 4 | 256, function (activity, event) {
    var state = !Editor.getSetting("projects.files", false);
    Editor.setSetting("projects.files", state);
    $("#ic_activities").toggleClass("show-files", !!state);
}, function (activity) {
    $("#ic_activities").toggleClass("show-files", !!Editor.getSetting("projects.files", false));
    return true;
});
new Button("#ic_button_recycle", 1 | 4 | 16 | 32, function (activity, event) {
    var state = !Editor.getSetting("projects.recycle", false);
    Editor.setSetting("projects.recycle", state);
    $("#ic_activities").toggleClass("show-trash", !!state);
}, function (activity) {
    $("#ic_activities").toggleClass("show-trash", !!Editor.getSetting("projects.recycle", false));
    return true;
});
new Button("#ic_button_user_add", 1 | 2, function () {
    var panel = Editor.UsersPanel.instance;
    panel.editUser(-1);
}, function (activity) {
    var panel = Editor.UsersPanel.instance;
    return panel && panel.currentId && panel.isGroup === true;
});
new Button("#ic_button_user_edit", 1 | 2, function () {
    var panel = Editor.UsersPanel.instance;
    panel.editUser(panel.currentId);
}, function (activity) {
    var panel = Editor.UsersPanel.instance;
    return panel && panel.currentId && panel.isGroup === false;
});
new Button("#ic_button_user_group_link", 1 | 2, function () {
    var usersPanel = Editor.UsersPanel.instance, id = usersPanel.currentGroup, node = parseInt($.tree.currentId);
    if (node && usersPanel.groups[id].node !== node && confirm("Are you sure you wish to change the linked node to #" + node + " (" + ICNodes.get(node)._name + ")?")) {
        Editor.ajax({
            type: "POST",
            url: "ajax.php",
            global: false,
            dataType: "json",
            data: {
                action: "set_group_node",
                id: id,
                node: node
            },
            success: function (data) {
                Editor.UsersPanel.instance.loadGroups();
            }
        });
    }
}, function (activity) {
    var panel = Editor.UsersPanel.instance;
    return panel && panel.currentGroup && panel.isGroup && activity && (activity.is(0) || activity.is(2));
});
new Button("#ic_button_user_group_add", 1 | 2, function () {
    var node = parseInt($.tree.currentId), name = prompt("Please enter a new group name, empty to cancel", ICNodes.get(node)._name || "");
    if (name) {
        Editor.ajax({
            type: "POST",
            url: "ajax.php",
            global: false,
            dataType: "json",
            data: {
                action: "create_group",
                node: String(node),
                name: name
            },
            success: function (data) {
                Editor.UsersPanel.instance.loadGroups();
            }
        });
    }
}, function (activity) {
    return Editor.UsersPanel.instance && activity && (activity.is(0) || activity.is(2));
});
new Button("#ic_button_user_group_edit", 1 | 2, function () {
    var id = Editor.UsersPanel.instance.currentGroup, usersPanel = Editor.UsersPanel.instance, oldName = usersPanel.groups[usersPanel.currentGroup].name || "", name = prompt("Please enter a new group name, empty to cancel", oldName);
    if (name && name !== oldName) {
        Editor.ajax({
            type: "POST",
            url: "ajax.php",
            global: false,
            dataType: "json",
            data: {
                action: "set_group_name",
                id: id,
                name: name
            },
            success: function (data) {
                Editor.UsersPanel.instance.loadGroups();
            }
        });
    }
}, function (activity) {
    var panel = Editor.UsersPanel.instance;
    return panel && panel.currentGroup && panel.isGroup;
});
var Editor;
(function (Editor) {
    var ui;
    (function (ui) {
        var resources;
        (function (resources) {
            function createPanel(parent, onlyType) {
                Editor.currentActivity.getTemplateFolders().then(function (nodes) {
                    nodes.pushOnce.apply(nodes, Editor.currentActivity.getParents(true));
                    nodes.pushOnce.apply(nodes, Editor.currentActivity.getSiblings());
                    ReactDOM.render(React.createElement(Resources, { nodes: nodes, type: onlyType }), parent);
                });
            }
            resources.createPanel = createPanel;
            var List;
            (function (List) {
                List[List["THUMBS"] = 0] = "THUMBS";
                List[List["ICONS"] = 1] = "ICONS";
                List[List["LIST"] = 2] = "LIST";
                List[List["DETAILS"] = 3] = "DETAILS";
            })(List || (List = {}));
            var ListName = (_a = {},
                _a[0] = "thumbs",
                _a[1] = "icons",
                _a[2] = "list",
                _a[3] = "details",
                _a);
            var ListEnum = {
                "thumbs": 0,
                "icons": 1,
                "list": 2,
                "details": 3
            };
            var Type;
            (function (Type) {
                Type[Type["ALL"] = 0] = "ALL";
                Type[Type["IMAGE"] = 1] = "IMAGE";
                Type[Type["AUDIO"] = 2] = "AUDIO";
                Type[Type["VIDEO"] = 3] = "VIDEO";
            })(Type || (Type = {}));
            var TypeName = (_b = {},
                _b[0] = "all",
                _b[1] = "image",
                _b[2] = "audio",
                _b[3] = "video",
                _b);
            var TypeEnum = {
                "all": 0,
                "image": 1,
                "audio": 2,
                "video": 3
            };
            var Sort;
            (function (Sort) {
                Sort[Sort["ALPHA"] = 0] = "ALPHA";
                Sort[Sort["TREE"] = 1] = "TREE";
            })(Sort || (Sort = {}));
            var SortName = (_c = {},
                _c[0] = "alpha",
                _c[1] = "tree",
                _c);
            var SortEnum = {
                "alpha": 0,
                "tree": 1
            };
            ;
            var Resources = (function (_super) {
                __extends(Resources, _super);
                function Resources(props) {
                    var _this = _super.call(this, props) || this;
                    _this.state = {
                        list: ListEnum[Editor.getSetting("options.resource_list", ListName[0])],
                        sort: SortEnum[Editor.getSetting("options.resource_sort", SortName[0])],
                        type: TypeEnum[Editor.getSetting("options.resource_type", TypeName[0])],
                        info: Editor.getSetting("options.resource_ext", "ext") === "ext"
                    };
                    return _this;
                }
                Resources.prototype.setList = function (value) {
                    this.setState({ list: value });
                    Editor.setSetting("options.resource_list", ListName[value]);
                    Editor.saveSettings();
                };
                Resources.prototype.setSort = function (value) {
                    this.setState({ sort: value });
                    Editor.setSetting("options.resource_sort", SortName[value]);
                    Editor.saveSettings();
                };
                Resources.prototype.setType = function (value) {
                    this.setState({ type: value });
                    Editor.setSetting("options.resource_type", TypeName[value]);
                    Editor.saveSettings();
                };
                Resources.prototype.toggleInfo = function () {
                    var info = this.state.info;
                    this.setState({ info: !info });
                    Editor.setSetting("options.resource_ext", info ? "" : "ext");
                    Editor.saveSettings();
                };
                Resources.prototype.render = function () {
                    var _this = this;
                    var props = this.props, state = this.state, stateList = state.list, stateType = state.type, stateSort = state.sort, stateInfo = state.info, nodes = props.nodes;
                    console.log("nodes", nodes);
                    return (React.createElement("div", { className: "resources " + ListName[stateList] + " " + TypeName[props.type || stateType] + " " + SortName[stateSort] + " " + (stateInfo ? "ext" : "") },
                        React.createElement("span", null,
                            React.createElement("span", null,
                                React.createElement("i", { onClick: function () { _this.setList(0); }, className: "fa fa-th-large" + (stateList === 0 ? " active" : "") }),
                                React.createElement("i", { onClick: function () { _this.setList(1); }, className: "fa fa-th" + (stateList === 1 ? " active" : "") }),
                                React.createElement("i", { onClick: function () { _this.setList(2); }, className: "fa fa-th-list" + (stateList === 2 ? " active" : "") }),
                                React.createElement("i", { onClick: function () { _this.setList(3); }, className: "fa fa-list" + (stateList === 3 ? " active" : "") })),
                            React.createElement("span", { style: props.type ? { display: "none" } : {} },
                                React.createElement("i", { onClick: function () { _this.setType(0); }, className: "fa fa-circle-thin" + (stateType === 0 ? " active" : "") }),
                                React.createElement("i", { onClick: function () { _this.setType(1); }, className: "fa fa-camera" + (stateType === 1 ? " active" : "") }),
                                React.createElement("i", { onClick: function () { _this.setType(2); }, className: "fa fa-volume-up" + (stateType === 2 ? " active" : "") }),
                                React.createElement("i", { onClick: function () { _this.setType(3); }, className: "fa fa-video-camera" + (stateType === 3 ? " active" : "") })),
                            React.createElement("span", null,
                                React.createElement("i", { onClick: function () { _this.setSort(0); }, className: "fa fa-sort-alpha-asc" + (stateSort === 0 ? " active" : "") }),
                                React.createElement("i", { onClick: function () { _this.setSort(1); }, className: "fa fa-sort-amount-asc" + (stateSort === 1 ? " active" : "") })),
                            React.createElement("span", { "data-select": "resource_ext" },
                                React.createElement("i", { onClick: function () { _this.toggleInfo(); }, className: "fa fa-info" + (stateInfo ? " active" : "") })),
                            React.createElement("div", null,
                                React.createElement("input", { id: "resource_search", placeholder: "filter", type: "text" })),
                            React.createElement("label", { className: "fa fa-folder-open" },
                                React.createElement("input", { type: "file" }))),
                        React.createElement("div", null, nodes.map(function (node) {
                            return !(node._flags & (2 | 32)) && node._files && node._files.length
                                ? [
                                    React.createElement(resources.Folder, { id: node._id }),
                                    node._files
                                        ? node._files.map(function (file) { return React.createElement(resources.File, { path: file }); })
                                        : null
                                ]
                                : null;
                        }))));
                };
                return Resources;
            }(React.Component));
            resources.Resources = Resources;
            var _a, _b, _c;
        })(resources = ui.resources || (ui.resources = {}));
    })(ui = Editor.ui || (Editor.ui = {}));
})(Editor || (Editor = {}));
var Editor;
(function (Editor) {
    var ui;
    (function (ui) {
        var resources;
        (function (resources) {
            ;
            function getFileType(path) {
                switch (path.regex(/\.([^\.]+)$/).toLowerCase()) {
                    case "gif":
                    case "jpg":
                    case "png":
                    case "svg":
                        return Editor.MediaType.IMAGE;
                    case "mp3":
                    case "m4a":
                    case "ogg":
                        return Editor.MediaType.AUDIO;
                    case "mp4":
                    case "m4v":
                        return Editor.MediaType.VIDEO;
                }
                return Editor.MediaType.UNKNOWN;
            }
            var File = (function (_super) {
                __extends(File, _super);
                function File() {
                    return _super !== null && _super.apply(this, arguments) || this;
                }
                File.prototype.render = function () {
                    var props = this.props, path = props.path, data = props.data, filePath = "../" + path, fileName = path.regex(/([^\/]+)$/), fileExt = fileName.regex(/\.([^\.]+)$/).toLowerCase(), fileType = getFileType(path), url = data || encodeURI(filePath).replace(/'/g, "%27"), className = fileType === Editor.MediaType.IMAGE
                        ? "image"
                        : fileType === Editor.MediaType.AUDIO
                            ? "audio"
                            : "video";
                    return fileType === Editor.MediaType.UNKNOWN
                        ? null
                        : (React.createElement("a", { ref: "resource_file_" + path, onClick: function (event) { return event.preventDefault(); }, download: true, href: url, target: "_blank", className: "media defer " + className, title: filePath },
                            fileType === Editor.MediaType.IMAGE
                                ? React.createElement("div", { className: "fa fa-file-image-o", "data-type": fileExt, style: { backgroundImage: "url('" + url + "')" } })
                                : fileType === Editor.MediaType.AUDIO
                                    ? React.createElement("div", { className: "fa fa-file-audio-o", "data-type": fileExt },
                                        React.createElement("audio", { src: url, controls: true }))
                                    : React.createElement("div", { className: "fa fa-file-audio-o", "data-type": fileExt },
                                        React.createElement("video", { src: url, controls: true })),
                            React.createElement("h3", null,
                                React.createElement("span", null, path.regex(/(\d+)/) + "/"),
                                fileName.replace(/\.[^\.]+$/, ""),
                                React.createElement("span", null, "." + fileExt))));
                };
                return File;
            }(React.Component));
            resources.File = File;
        })(resources = ui.resources || (ui.resources = {}));
    })(ui = Editor.ui || (Editor.ui = {}));
})(Editor || (Editor = {}));
var Editor;
(function (Editor) {
    var ui;
    (function (ui) {
        var resources;
        (function (resources) {
            ;
            var Folder = (function (_super) {
                __extends(Folder, _super);
                function Folder() {
                    return _super !== null && _super.apply(this, arguments) || this;
                }
                Folder.prototype.render = function () {
                    var props = this.props, id = props.id;
                    return (React.createElement("div", null,
                        React.createElement("h3", null,
                            React.createElement("span", null, id + "/"),
                            ICNodes.get(id)._name || id)));
                };
                return Folder;
            }(React.Component));
            resources.Folder = Folder;
        })(resources = ui.resources || (ui.resources = {}));
    })(ui = Editor.ui || (Editor.ui = {}));
})(Editor || (Editor = {}));
var Editor;
(function (Editor) {
    Editor.support = ic.version;
    Editor.canHaveState = Object.keys(Editor.support).filter(function (name) {
        return /^ic-/.test(name);
    }).join(",");
})(Editor || (Editor = {}));
window.addEventListener("options-test", null, Object.defineProperty({}, "capture", {
    get: function () {
        var oldEventListener = document.addEventListener;
        if (!oldEventListener.isTouched) {
            var addEventListenerTouch = function (eventType, listener, options, wantsUntrusted) {
                switch (eventType) {
                    case "touchstart":
                    case "touchmove":
                    case "touchend":
                        if (options === true || options === false) {
                            options = {
                                capture: options
                            };
                        }
                        else if (!options) {
                            options = {};
                        }
                        if (!options.passive) {
                            options.passive = false;
                        }
                        break;
                }
                return oldEventListener.call(this, eventType, listener, options, wantsUntrusted);
            };
            addEventListenerTouch.isTouched = true;
            Object.defineProperty(document, "addEventListener", {
                value: addEventListenerTouch,
                configurable: true
            });
            Object.defineProperty(document.body, "addEventListener", {
                value: addEventListenerTouch,
                configurable: true
            });
        }
    }
}));
$(Editor.startup);
//# sourceMappingURL=editor.js.map