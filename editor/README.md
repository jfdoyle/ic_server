# Infuze Creator

An activity creation system using HTML5.

## Editor

This is the Editor section, responsible for creating the static html used for the activities and running them both directly and within a standalone launcher.

--

*See roadmap document for details.*

Copyright &copy; 2016 Infuze, All Rights Reserved.
