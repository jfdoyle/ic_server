module.exports = function(grunt) {
	grunt.registerTask("default", [
		"ts",
		"changed:uglify",
		"googlefonts",
		"newer:sass",
		"newer:postcss",
		"newer:htmlmin"
	]);
};
