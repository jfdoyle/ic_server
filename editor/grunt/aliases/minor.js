module.exports = function(grunt) {
	grunt.registerTask("minor", [
		"bump-only:minor",
		"default",
		"bump-commit"
	]);
};
