module.exports = {
	main: {
		options: {
			screwIE8: true,
			sourceMap: true,
			sourceMapIn: "include/editor.js.map",
			sourceMapName: "include/editor.min.js.map",
			banner: '/*! <%= pkg.name %> v<%= pkg.version %> (<%= grunt.template.today("dddd dS mmmm yyyy, h:MM:ss TT") %>) */',
			compress: {
				drop_console: true,
				drop_debugger: true,
				pure_getters: true,
				unsafe: true
			}
		},
		files: {
			"include/editor.min.js": ["include/editor.js"]
		}
	}
};
