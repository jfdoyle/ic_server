module.exports = {
	main: {
		options: {
			outputStyle: "compressed"
		},
		files: {
			"build/editor.css": ["src/editor.scss"],
			"build/preview.css": ["src/preview.scss"],
			"build/template.css": ["src/template.scss"]
		}
	}
};
