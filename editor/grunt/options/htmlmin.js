module.exports = {
	main: {
		options: {
			removeComments: true,
			collapseWhitespace: true,
			collapseInlineTagWhitespace: true
		},
		files: {
			"index.html": "src/index.html"
		}
	}
};
