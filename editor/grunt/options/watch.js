module.exports = {
	options: {
		livereload: true
	},
	activity: {
		files: ["activity/build/activity.js", "activity/build/activity.css", "activity/version/**/activity.js", "activity/version/**/activity.css"]
	},
	scripts: {
		files: ["src/**/*.ts", "src/**/*.tsx", "activity/src/**/*.ts"],
		tasks: ["ts", "changed:uglify"]
	},
	css: {
		files: "src/**/*.scss",
		tasks: ["sass", "postcss"]
	},
	html: {
		files: "src/**/*.html",
		tasks: ["htmlmin"]
	}
};
