module.exports = {
	main: {
		options: {
			fontPath: "include/fonts/",
			httpPath: "fonts/",
			cssFile: "build/fonts.css",
			fonts: [
				{
					family: "Titillium Web",
					styles: [
						300, 400, 700
					]
				}
			]
		}
	}
};
