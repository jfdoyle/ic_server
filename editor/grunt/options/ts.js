module.exports = {
	options: {
		"additionalFlags": "--experimentalDecorators --lib dom,es6,scripthost", // --strictNullChecks
		"fast": "never"
	},
	main: {
		"tsconfig": true
	}
};
