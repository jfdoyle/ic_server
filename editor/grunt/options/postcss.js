module.exports = {
	main: {
		options: {
			processors: [
				require("autoprefixer")({browsers: ["last 2 versions"]})
			]
		},
		files: {
			"include/editor.css": ["build/editor.css"],
			"include/preview.css": ["build/preview.css"],
			"include/template.css": ["build/template.css"],
			"include/fonts.css": ["build/fonts.css"]
		}
	}
};
