///<reference path="../defineProperty.ts" />
///<reference path="naturalCompare.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(String.prototype, "naturalCompare", function(this: string, compareString: string): number {
	var str1 = this,
		str2 = String(compareString),
		a_length = str1.length,
		b_length = str2.length,
		isDigitChar = function(a: string): boolean {
			var charCode = a.charCodeAt(0);

			return (charCode >= 48 && charCode <= 57); // 0-9
		};

	for (var ia = 0, ib = 0; ia < a_length && ib < b_length; ia++ , ib++) {
		var achar: string | number = str1.charAt(ia),
			bchar: string | number = str2.charAt(ib);

		if (isDigitChar(achar) && isDigitChar(bchar)) {
			for (var anum = 0, alen = 0; isDigitChar(achar); achar = str1.charAt(++ia)) {
				alen++;
				anum = (anum * 10) + parseInt(achar, 10);
			}
			for (var bnum = 0, blen = 0; isDigitChar(bchar); bchar = str2.charAt(++ib)) {
				blen++;
				bnum = (bnum * 10) + parseInt(bchar, 10);
			}
			ia--;
			ib--;
			if (anum === bnum) {
				continue;
			}
			achar = anum;
			bchar = bnum;
		}
		if (achar < bchar) {
			return -1;
		}
		if (achar > bchar) {
			return 1;
		}
	}
	return a_length === b_length ? 0 : a_length > b_length ? 1 : -1;
});
