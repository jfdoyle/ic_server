/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface String {
	/**
	 * The <strong><code>regexIndexOf()</code></strong> method returns the index
	 * within the calling String object of the last occurrence of the specified
	 * regular expression, starting the search at <code>fromIndex</code>.
	 * Returns <code>-1</code> if the value is not found.
	 * 
	 * @param regexp A string representing the value to search for.
	 * 
	 * @param fromIndex The index at which to start the searching forwards in
	 * the string. It can be any integer. The default value is <code>0</code>,
	 * so the whole array is searched. If <code>fromIndex < 0</code> the entire
	 * string is searched. If <code>fromIndex >= str.length</code>, the string
	 * is not searched and <code>-1</code> is returned. Unless
	 * <code>searchValue</code> is an empty string, then <code>str.length</code>
	 * is returned.
	 * 
	 * @returns The index of the first occurrence of the specified value;
	 * <strong>-1</strong> if not found.
	 */
	regexIndexOf(regexp: RegExp, fromIndex?: number): number;
}
