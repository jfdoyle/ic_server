///<reference path="../defineProperty.ts" />
///<reference path="text.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(String.prototype, "text", function(this: string) {
	return this.replace(/<\/?[a-z][^>]*>/gi, "");
});
