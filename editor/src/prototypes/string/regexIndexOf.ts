///<reference path="../defineProperty.ts" />
///<reference path="regexIndexOf.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(String.prototype, "regexIndexOf", function(this: string, regexp: RegExp, fromIndex?: number) {
	var index = this.substring(fromIndex || 0).search(regexp);

	return index >= 0 ? index + (fromIndex || 0) : index;
});

