/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface String {
	/**
	 * The <strong><code>regexIndexOf()</code></strong> method returns the index
	 * within the calling String object of the last occurrence of the specified
	 * regular expression, searching backwards from <code>fromIndex</code>.
	 * Returns <code>-1</code> if the value is not found.
	 * 
	 * @param regexp A string representing the value to search for.
	 * 
	 * @param fromIndex The index at which to start searching backwards in the
	 * string. Starting with this index, the left part of the string will be
	 * searched. It can be any integer. The default value is
	 * <code>+Infinity</code>. If <code>fromIndex >= str.length</code>, the
	 * whole string is searched. If <code>fromIndex < 0</code>, the behavior
	 * will be the same as if it would be <code>0</code>.
	 * 
	 * @returns The index of the first occurrence of the specified value;
	 * <strong>-1</strong> if not found.
	 */
	regexLastIndexOf(regexp: RegExp, fromIndex?: number): number;
}
