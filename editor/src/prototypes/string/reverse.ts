///<reference path="../defineProperty.ts" />
///<reference path="reverse.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(String.prototype, "reverse", function(this: string) {
	return this.split("").reverse().join("");
});
