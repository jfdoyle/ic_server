/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface String {
	/**
	 * Unicode encode unicode or 8-bit (or higher) characters
	 * 
	 * @param html Output html encoding rather than ansi
	 * 
	 * @returns Unicoded string
	 */
	unicode(html?: boolean): string;
}
