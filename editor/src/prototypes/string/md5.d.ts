/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface String {
	/**
	 * Create an md5 hash
	 * converted from http://www.myersdaily.org/joseph/javascript/md5-text.html
	 * @method md5
	 * @for String
	 * @return {string}
	 */
	md5(): string;
}
