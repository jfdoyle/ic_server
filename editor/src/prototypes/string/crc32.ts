///<reference path="../defineProperty.ts" />
///<reference path="crc32.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(String.prototype, "crc32", function(this: string) {
	var i: number,
		j: number,
		k: number,
		length: number = this.length,
		crc32_table = (String.prototype.crc32 as any).table,
		crc = -1; // Begin with all bits set ( 0xffffffff )

	if (!crc32_table) {
		crc32_table = (String.prototype.crc32 as any).table = new Uint32Array(256);
		for (i = 0; i < 256; i++) {
			for (j = i, k = 0; k < 8; k++) {
				j = ((j & 1) ? (0xEDB88320 ^ (j >>> 1)) : (j >>> 1));
			}
			crc32_table[i] = j;
		}
	}
	for (i = 0; i < length; i++) {
		crc = (crc >>> 8) ^ crc32_table[crc & 0xFF ^ (this.charCodeAt(i) & 0xFF)];
	}
	return ((crc ^ -1) >>> 0).toString(16);

});
