/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface String {
	/**
	 * The <strong><code>reverse()</code></strong> method reverses the order of
	 * a string.
	 * 
	 * @returns A new string with the characters in the reverse order to the
	 * source string.
	 */
	reverse(): string;
}
