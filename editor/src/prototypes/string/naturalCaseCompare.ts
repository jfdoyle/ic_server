///<reference path="../defineProperty.ts" />
///<reference path="naturalCaseCompare.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(String.prototype, "naturalCaseCompare", function(this: string, compareString: string): number {
	return this.toLowerCase().naturalCompare(String(compareString).toLowerCase());
});
