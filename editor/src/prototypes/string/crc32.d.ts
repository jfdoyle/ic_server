/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface String {
	/**
	 * The <strong><code>crc32()</code></strong> method computes a crc32 hash of
	 * a string.
	 * 
	 * @returns A hex crc32 hash of the source string.
	 */
	crc32(): string;
}
