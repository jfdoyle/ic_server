/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface String {
	/**
	 * The <strong><code>naturalCaseCompare()</code></strong> method returns a
	 * number indicating whether a case insensitive reference string comes
	 * before or after or is the same as the given string in sort order.
	 * 
	 * @param compareString The string against which the referring string is
	 * compared (case insensitive)
	 * 
	 * @returns A <strong>negative</strong> number if the reference string
	 * occurs before the compare string; <strong>positive</strong> if the
	 * reference string occurs after the compare string; <strong>0</strong> if
	 * they are equivalent.
	 */
	naturalCaseCompare(compareString: string): number;
}
