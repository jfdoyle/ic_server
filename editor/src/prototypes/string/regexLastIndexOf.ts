///<reference path="../defineProperty.ts" />
///<reference path="regexLastIndexOf.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(String.prototype, "regexLastIndexOf", function(this: string, regexp: RegExp, fromIndex?: number) {
	regexp = regexp.global ? regexp : new RegExp(regexp.source, "g" + (regexp.ignoreCase ? "i" : "") + (regexp.multiline ? "m" : ""));
	if (fromIndex === undefined) {
		fromIndex = this.length;
	} else if (fromIndex < 0) {
		fromIndex = 0;
	}
	var result: RegExpExecArray,
		stringToWorkWith = this.substring(0, fromIndex + 1),
		index = -1;

	while ((result = regexp.exec(stringToWorkWith)) != null) {
		index = result.index;
		regexp.lastIndex = index + 1;
	}
	return index;
});
