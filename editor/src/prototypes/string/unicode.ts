///<reference path="../defineProperty.ts" />
///<reference path="unicode.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(String.prototype, "unicode", function(this: string, html?: boolean) {
	return this.replace(/[\u0080-\uffff]/g, html
		? function(char) {
			return "&#" + char.charCodeAt(0).toString() + ";";
		}
		: function(char) {
			return "\\u" + ("0000" + char.charCodeAt(0).toString(16)).slice(-4);
		});
});
