/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Number {
	/**
	 * Split numbers with commas every third digit from the right
	 * @method addCommas
	 * @for Number
	 * @param {number} digits
	 * @return {string}
	 */
	addCommas(digits?: number): string;
}
