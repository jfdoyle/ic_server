///<reference path="../defineProperty.ts" />
///<reference path="addCommas.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Number.prototype, "addCommas", function(this: number, digits: number) {
	var num = String(isNumber(digits) ? this.toFixed(digits) : this),
		rx = /^(.*\s)?(\d+)(\d{3}\b)/;

	while (rx.test(num)) {
		num = num.replace(rx, "$1$2,$3");
	}
	return num;
});
