///<reference path="../defineProperty.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

if (!("offsetParent" in document.body)) {
	Object.defineProperty(HTMLElement.prototype, "offsetParent", {
		"get": function(this: HTMLElement): HTMLElement {
			var element = this,
				parent = element.parentElement,
				html = document.documentElement,
				found: HTMLElement = null,
				style = window.getComputedStyle(element);

			if (element && style.position !== "fixed") {
				for (; parent; parent = parent.parentElement) {
					style = window.getComputedStyle(parent);
					if (style.display === "none") {
						found = null;
						break;
					} else if (!found && (parent === html || style.position !== "initial")) {
						found = parent;
					}
				}
			}
			return found;
		}
	});
}
