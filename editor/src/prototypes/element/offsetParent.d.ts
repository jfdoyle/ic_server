/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface HTMLElement {
	/**
	 * The <strong><code>HTMLElement.offsetParent</code></strong> read-only
	 * property returns a reference to the object which is the closest (nearest
	 * in the containment hierarchy) positioned containing element. If the
	 * element is non-positioned, the nearest table cell or root element (
	 * <code>html</code> in standards compliant mode; <code>body</code> in
	 * quirks rendering mode) is the <code>offsetParent</code>.
	 * <code>offsetParent</code> returns null when the element has
	 * <code>style.display</code> set to "none". The <code>offsetParent</code>
	 * is useful because <code>offsetTop</code> and <code>offsetLeft</code> are
	 * relative to its padding edge.
	 */
	offsetParent: HTMLElement;
}
