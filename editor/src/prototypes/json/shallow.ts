///<reference path="../defineProperty.ts" />
///<reference path="shallow.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(JSON, "shallow", function(obj, depth, replacer, space) {
	var walk = function(o, d) {
		var i, out;
		if (o && isObject(typeof o)) {
			if (isArray(o)) {
				if (d > 0) {
					out = [];
					for (i = 0; i < o.length; i++) {
						out[i] = walk(o[i], d - 1);
					}
				} else {
					out = "[object Array]";
				}
			} else if (d > 0) {
				out = {};
				for (i in o) {
					if ((o as {}).hasOwnProperty(i)) {
						out[i] = walk(o[i], d - 1);
					}
				}
			} else {
				out = "[object Object]";
			}
		} else {
			out = o;
		}
		return out;
	};
	return JSON.stringify(walk(obj, depth || 1), replacer, space);
});
