/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JSON {
	/**
	 * Decode previously encoded JSON data.
	 */
	decode(text: string, reviver?: (key: any, value: any) => any): any;
}
