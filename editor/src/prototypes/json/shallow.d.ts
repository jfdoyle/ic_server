/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JSON {
	/**
	 * Do a shallow JSON for printability
	 * @static
	 * @method shallow
	 * @for JSON
	 * @param {Object} obj
	 * @param {number} [depth]
	 * @param {function(?)} [replacer]
	 * @param {string} [space]
	 * @return {string}
	 */
	shallow(obj: any, depth?: number, replacer?: any, space?: string): string;
}
