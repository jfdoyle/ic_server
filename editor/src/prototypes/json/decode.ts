///<reference path="../defineProperty.ts" />
///<reference path="decode.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(JSON, "decode", function(str: string, reviver?: (key: any, value: any) => any) {
	var obj = JSON.parse(str, reviver),
		keys: { [key: string]: string } = obj["$"],
		count = {},
		decode = function(obj) {
			var i,
				to;

			if (isObject(obj)) {
				to = {};
				for (i in obj) {
					if (keys[i]) {
						to[keys[i].valueOf()] = arguments.callee(obj[i]);
						count[i] = (count[i] || 0) + 1;
					} else {
						to[i] = arguments.callee(obj[i]);
					}
				}
			} else if (isArray(obj)) {
				to = [];
				for (i = 0; i < obj.length; i++) {
					to[i] = arguments.callee(obj[i]);
				}
			} else {
				to = obj;
			}
			return to;
		};

	if (keys) {
		delete obj["$"];
		return decode(obj);
	}
	return obj;
});
