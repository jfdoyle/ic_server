///<reference path="../defineProperty.ts" />
///<reference path="encode.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(JSON, "encode", function(obj: any, replacer?: any[], space?: string | number) {
	var key: string,
		keys: { [key: string]: string } = {},
		count: { [key: string]: number } = {},
		next = 0,
		nextKey: string,
		first = true,
		getKey = function() {
			var key,
				digits = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ",
				length = digits.length;

			do {
				key = nextKey;
				nextKey = (next >= length ? digits[(Math.floor(next / length) - 1) % length] : "") + digits[next % length];
				next++;
			} while (count[nextKey]); // First time we're called we ignore "key", but already have count[] filled
			first = false;
			return key;
		},
		check = function(obj) { // Count how many of each key - to decide if we replace them
			if (isObject(obj)) {
				for (var key in obj) {
					if (/[^0-9]/.test(key)) {
						count[key] = (count[key] || 0) + 1;
					}
					check(obj[key]);
				}
			} else if (isArray(obj)) {
				for (var i = 0; i < obj.length; i++) {
					check(obj[i]);
				}
			}
		},
		encode = function(obj) { // Replace keys where the saved length is more than the used length
			var length: number,
				to;

			if (isObject(obj)) {
				to = {};
				for (var key in obj) {
					length = key.length;
					if ((count[key] * length) > (((count[key] + 1) * nextKey.length) + length + (first ? 12 : 6))) { // total (length of key) > total (length of encoded key) + length of key translation
						if (!keys[key]) {
							keys[key] = getKey();
						}
						to[keys[key]] = encode(obj[key]);
					} else {
						to[key] = encode(obj[key]);
					}
				}
			} else if (isArray(obj)) {
				to = [];
				for (var i = 0; i < obj.length; i++) {
					to[i] = encode(obj[i]);
				}
			} else {
				to = obj;
			}
			return to;
		};

	if (isObject(obj) || isArray(obj)) {
		if (obj["$"]) {
			console.warn("Error: Trying to encode an object that already contains a '$' key!!!");
		}
		check(obj);
		console.log("keys", count)
		getKey(); // Load up the first key, prevent key conflicts
		first = true; // For the "Should I?" check
		obj = encode(obj);
		if (Object.keys(keys).length) {
			obj["$"] = {};
			for (key in keys) {
				obj["$"][keys[key]] = key;
			}
		}
	}
	return JSON.stringify(obj, replacer, space);
});
