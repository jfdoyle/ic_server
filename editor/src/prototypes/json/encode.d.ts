/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JSON {
	/**
	 * JSON encode a tree, reducing space of object keys.
	 */
	encode(obj: any, depth?: number, replacer?: any, space?: string): string;
}
