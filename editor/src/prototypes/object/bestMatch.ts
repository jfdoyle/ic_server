///<reference path="../defineProperty.ts" />
///<reference path="bestMatch.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Object.prototype, "bestMatch", function(this: Object, percent: number) {
	var i: string,
		num: number,
		best = -1;

	for (i in this) {
		num = parseInt(i);
		if (this.hasOwnProperty(i) && num > best && num <= percent) {
			best = num;
		}
	}
	return best > -1 ? this[best] : undefined;
});
