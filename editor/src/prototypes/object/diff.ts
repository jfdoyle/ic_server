///<reference path="../defineProperty.ts" />
///<reference path="diff.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Object.prototype, "diff", function diff(this: Object, original: Object | any[]) {
	for (var i in this) {
		if ((this as {}).hasOwnProperty(i) && original.hasOwnProperty(i)) {
			if ((isArray(this[i]) && isArray(original[i])) || (isObject(this[i]) && isObject(original[i]))) {
				this[i] = diff.call(this[i], original[i], true);
				if (!Object.keys(this[i])) {
					delete this[i];
				}
			} else if (this[i] === original[i]) {
				delete this[i];
			}
		}
	}
	return this;
});
