///<reference path="../defineProperty.ts" />
///<reference path="indexOfX.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Object.prototype, "indexOfX", function(this: Object, value: string) {
	// Named with an X due to conflicts
	for (var key in this) {
		if (this.hasOwnProperty(key) && this[key] === value) {
			return key;
		}
	}
	return null;
});
