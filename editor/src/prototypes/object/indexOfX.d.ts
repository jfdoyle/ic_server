/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Object {
	/**
	 * Return the key in an object which has a set value
	 * @method indexOfX
	 * @for Object
	 * @param {string} value
	 * @return {string}
	 */
	indexOfX(value: any): any;
}
