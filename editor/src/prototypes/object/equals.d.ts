/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Object {
	/**
	 * Object tree comparison
	 * @param {Object} target
	 * @return {Boolean}
	 */
	equals(target: Object, callback?: (key: string, target: Object) => boolean): boolean;
}
