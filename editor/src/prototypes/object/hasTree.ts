///<reference path="../defineProperty.ts" />
///<reference path="hasTree.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Object.prototype, "hasTree", function(this: Object, path: string | string[]) {
	var data = this,
		fixedPath: (string | number)[] = path.explode();

	while (fixedPath.length && data !== undefined) {
		data = data[fixedPath.shift()];
	}
	return !(data === undefined || data === null);
});
