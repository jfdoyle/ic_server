/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Object {
	/**
	 * Find the best match in a numbered object
	 * @method bestMatch
	 * @for Object
	 * @param {number} percent or plain number
	 * @return {*}
	 */
	bestMatch(percent: number): any;
}
