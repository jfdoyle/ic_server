///<reference path="../defineProperty.ts" />
///<reference path="equals.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Object.prototype, "equals", function(this: Object, target: Object, callback: (key: string, target: any) => boolean) {
	var key;

	if (!target) {
		return false;
	}
	for (key in this) {
		if (this.hasOwnProperty(key) && (!callback || callback.call(this, key, target) !== false)) {
			if (!target.hasOwnProperty(key) || typeof this[key] !== typeof target[key] || !this[key] !== !target[key]) {
				return false;
			}
			switch (typeof (this[key])) {
				case "object":
					if (this[key] !== null && target[key] !== null && (this[key].constructor.toString() !== target[key].constructor.toString() || !this[key].equals(target[key]))) {
						return false;
					}
					break;
				case "function":
					if (this[key].toString() !== target[key].toString()) {
						return false;
					}
					break;
				default:
					if (this[key] !== target[key]) {
						return false;
					}
			}
		}
	}
	for (key in target) {
		if (!this.hasOwnProperty(key) && (!callback || callback.call(this, key, target) !== false)) {
			return false;
		}
	}
	return true;
});
