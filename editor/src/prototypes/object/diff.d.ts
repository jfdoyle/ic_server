/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Object {
	/**
	 * Remove all keys of an Object that match an original Object
	 * @method diff
	 * @for Object
	 * @param {Object} original The original Object we're comparing with
	 * @return {Object}
	 */
	diff(original: Object | any[]): Object;
}
