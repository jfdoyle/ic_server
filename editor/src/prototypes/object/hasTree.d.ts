/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Object {
	/**
	 * Check a nested data exists in an Object
	 * @method hasTree
	 * @param {(String|Array)} path Path to data, either dot separated string or an array
	 * @return {*}
	 */
	hasTree(path: string | string[]): boolean;
}
