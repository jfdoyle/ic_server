///<reference path="../defineProperty.ts" />
///<reference path="clean.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Array.prototype, "clean", function() {
	for (var i = 0, arr: any[] = []; i < this.length; i++) {
		if (this[i] || this[i] === 0 || this[i] === false) {
			arr.push(this[i]);
		}
	}
	return arr;
});
