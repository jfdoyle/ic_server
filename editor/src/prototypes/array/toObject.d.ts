/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Array<T> {
	/**
	 * Convert to an object as key: true (rather than index: key)
	 * @method toObject
	 * @for Array
	 */
	toObject(): { [key: string]: boolean };
}
