/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Array<T> {
	/**
	 * Return the case insensitive index of an array
	 * @method indexOfCase
	 * @for Array
	 * @param {string} searchElement
	 * @param {number} [fromIndex]
	 */
	indexOfCase(searchElement: string, fromIndex?: number): number;
}
