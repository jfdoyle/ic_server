/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Array<T> {
	/**
	 * Return a new array without any empty values
	 * @method clean
	 * @for Array
	 */
	clean(): Array<T>;
}
