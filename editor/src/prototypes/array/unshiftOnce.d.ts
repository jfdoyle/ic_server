/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Array<T> {
	/**
	 * Add one or more elements to an array if they are not already added.
	 * 
	 * @param elements to add
	 * 
	 * @return new length of array
	 */
	unshiftOnce(...elements: Array<T>): number;
}
