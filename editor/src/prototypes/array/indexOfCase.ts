///<reference path="../defineProperty.ts" />
///<reference path="indexOfCase.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Array.prototype, "indexOfCase", function(this: string[], searchElement: string, fromIndex?: number) {
	var i: number,
		length = this.length >>> 0;

	fromIndex = fromIndex || 0;
	searchElement = searchElement.toUpperCase();
	for (i = Math.max(fromIndex >= 0 ? fromIndex : length - Math.abs(fromIndex), 0); i < length; i++) {
		if (i in this && String(this[i]).toUpperCase() === searchElement) {
			return i;
		}
	}
	return -1;
});
