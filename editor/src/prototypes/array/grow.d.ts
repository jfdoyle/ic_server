/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Array<T> {
	/**
	 * Grow (or shrink) an array
	 * @method grow
	 * @for Array
	 * @param {number} length New length to have, use a negative to only grow
	 * @param {*} add* Add these values, multiple args will cycle in order till full
	 * @return this
	 */
	grow(length: number, ...add: Array<T>): Array<T>;
}
