///<reference path="../defineProperty.ts" />
///<reference path="unshiftOnce.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Array.prototype, "unshiftOnce", function(this: any[]): number {
	for (var that = this, i = 0, elements = arguments; i < elements.length; i++) {
		if (!that.includes(elements[i])) {
			that.unshift(elements[i]);
		}
	}
	return that.length;
});
