/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Array<T> {
	/**
	 * Remove a case insensitive entry from an array
	 * @method removeCase
	 * @for Array
	 * @param {string} searchElement to remove
	 */
	removeCase(searchElement: T): Array<T>;
}
