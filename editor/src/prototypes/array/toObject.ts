///<reference path="../defineProperty.ts" />
///<reference path="toObject.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Array.prototype, "toObject", function(this: string[]) {
	for (var i = 0, obj: { [key: string]: any } = {}; i < this.length; i++) {
		obj[this[i]] = true;
	}
	return obj;
});
