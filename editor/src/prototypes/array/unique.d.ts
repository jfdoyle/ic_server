/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Array<T> {
	/**
	 * Remove duplicate elements in an array
	 * @method unique
	 * @for Array
	 */
	unique(): Array<T>;
}
