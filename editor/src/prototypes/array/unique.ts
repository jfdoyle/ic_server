///<reference path="../defineProperty.ts" />
///<reference path="unique.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Array.prototype, "unique", function(this: any[]) {
	var i = this.length;

	while (i-- > 0) {
		if (i > this.indexOf(this[i])) {
			this.splice(i, 1);
		}
	}
	return this;
});
