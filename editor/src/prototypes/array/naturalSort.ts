///<reference path="../defineProperty.ts" />
///<reference path="naturalSort.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Array.prototype, "naturalSort", function(this: any[]) {
	return this.sort(function(str1, str2) {
		return String(str1).naturalCompare(String(str2));
	});
});
