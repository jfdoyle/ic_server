///<reference path="../defineProperty.ts" />
///<reference path="grow.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Array.prototype, "grow", function(this: any[], length: number, ...add: any[]) {
	var i = 0,
		len = Math.abs(length),
		args = [].slice.call(arguments, 1);

	if (this.length < len) {
		args = args.length ? args : [0];
		while (this.length < len) {
			if (i >= args.length) {
				i = 0;
			}
			this.push(args[i++]);
		}
	} else if (length > 0 && this.length > length) {
		this.splice(length, this.length - length);
	}
	return this;
});
