///<reference path="../defineProperty.ts" />
///<reference path="removeCase.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Array.prototype, "removeCase", function(searchElement: any): any[] {
	var i = (this as any[]).indexOfCase(searchElement);

	return i >= 0 ? (this as any[]).splice(i, 1) : [];
});
