/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Array<T> {
	/**
	 * The <strong><code>naturalSort()</code></strong> method sorts the elements
	 * of an array in place and returns the array. The sort is not necessarily
	 * <a href="https://en.wikipedia.org/wiki/Sorting_algorithm#Stability">stable</a>.
	 * The default sort order is according to case insensitive string Unicode
	 * code points. Unlike  the standard <code>sort()</code> method, this will
	 * sort numbers in numeric order, such that 1 < 2 < 10.
	 * 
	 * @returns The sorted array.
	 */
	naturalCaseSort(): Array<T>;
}
