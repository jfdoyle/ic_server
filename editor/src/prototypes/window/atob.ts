///<reference path="../defineProperty.ts" />
///<reference path="atob.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */
defineProperty(window, "atob", function(this: Window, input: string) {
	var output = "", i = 0,
		_keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

	input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
	while (i < input.length) {
		var enc1 = _keyStr.indexOf(input.charAt(i++)),
			enc2 = _keyStr.indexOf(input.charAt(i++)),
			enc3 = _keyStr.indexOf(input.charAt(i++)),
			enc4 = _keyStr.indexOf(input.charAt(i++)),
			chr1 = (enc1 << 2) | (enc2 >> 4),
			chr2 = ((enc2 & 15) << 4) | (enc3 >> 2),
			chr3 = ((enc3 & 3) << 6) | enc4;

		output = output + String.fromCharCode(chr1);
		if (enc3 !== 64) {
			output = output + String.fromCharCode(chr2);
		}
		if (enc4 !== 64) {
			output = output + String.fromCharCode(chr3);
		}
	}
	//	output = Base64._utf8_decode(output);
	return output;
});
