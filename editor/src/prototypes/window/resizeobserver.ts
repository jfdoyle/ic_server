///<reference path="../defineProperty.ts" />
///<reference path="resizeobserver.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(window, "ResizeObserver", (function() {
	class ResizeObserver {
		static resizeObservers: ResizeObserver[] = [];

		private __callback: (entries?: ResizeObserverEntry[], observer?: ResizeObserver) => void;
		private __observationTargets: ResizeObservation[];
		private __activeTargets: ResizeObservation[];

		constructor(callback: (entries?: ResizeObserverEntry[], observer?: ResizeObserver) => void) {
			ResizeObserver.resizeObservers.push(this);
			this.__callback = callback;
			this.__observationTargets = [];
			this.__activeTargets = [];
		}

		observe(target: Element): void {
			var resizeObservationIndex = this.findTargetIndex(target);

			console.log("ResizeObserver.observe", target)
			if (!target || resizeObservationIndex >= 0) {
				return;
			}
			this.__observationTargets.push(new ResizeObservation(target));
		};

		unobserve(target: Element): void {
			var resizeObservationIndex = this.findTargetIndex(target);

			if (resizeObservationIndex === -1) {
				return;
			}
			this.__observationTargets.splice(resizeObservationIndex, 1);
		};

		disconnect(): void {
			this.__observationTargets = [];
			this.__activeTargets = [];
		};

		private findTargetIndex(target: Element): number {
			for (var index = 0, collection = this.__observationTargets; index < collection.length; index += 1) {
				if (collection[index].target === target) {
					return index;
				}
			}
		}

		private __populateActiveTargets(): void {
			this.__activeTargets = [];
			for (var key in this.__observationTargets) {
				var resizeObservation = this.__observationTargets[key];

				if (resizeObservation.isActive()) {
					this.__activeTargets.push(resizeObservation);
				}
			}
		};

		static broadcastActiveObservations(): void {
			for (var index = 0; index < ResizeObserver.resizeObservers.length; index += 1) {
				ResizeObserver.resizeObservers[index].__populateActiveTargets();
			}
			for (var roIndex = 0; roIndex < ResizeObserver.resizeObservers.length; roIndex++) {
				var resizeObserver = ResizeObserver.resizeObservers[roIndex];

				if (resizeObserver.__activeTargets.length === 0) {
					continue;
				}
				for (var atIndex = 0, entries = []; atIndex < resizeObserver.__activeTargets.length; atIndex += 1) {
					var resizeObservation = resizeObserver.__activeTargets[atIndex],
						entry = new ResizeObserverEntry(resizeObservation.target);

					entries.push(entry);
					resizeObservation.update();
				}
				resizeObserver.__callback(entries, resizeObserver);
				resizeObserver.__activeTargets = [];
			}
		}
	}

	class ResizeObserverEntry {
		constructor(public readonly target: Element) {
		}

		get contentRect(): ClientRect {
			return this.target.getBoundingClientRect();
		}
	}

	class ResizeObservation {
		private _broadcastWidth: number;
		private _broadcastHeight: number;

		constructor(public readonly target: Element) {
			this.update();
		}

		update(): void {
			var box = this.target.getBoundingClientRect();

			this._broadcastWidth = box.width;
			this._broadcastHeight = box.height;
		}

		get broadcastWidth(): number {
			return this._broadcastWidth;
		};

		get broadcastHeight(): number {
			return this._broadcastHeight;
		};

		isActive(): boolean {
			var box = this.target.getBoundingClientRect();

			if (box.width !== this.broadcastWidth ||
				box.height !== this.broadcastHeight) {
				return true;
			}
			return false;
		};
	}

	function frameHandler() {
		ResizeObserver.broadcastActiveObservations();
		requestAnimationFrame(frameHandler);
	}

	requestAnimationFrame(frameHandler);

	return ResizeObserver;
})());
