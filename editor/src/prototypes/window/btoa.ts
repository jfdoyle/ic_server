///<reference path="../defineProperty.ts" />
///<reference path="btoa.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(window, "btoa", function(this: Window, str: string) {
	if (arguments.length !== 1) {
		throw new SyntaxError("Not enough arguments");
	}
	// convert to string
	str = "" + str;

	var i, b10,
		padchar = "=",
		alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",
		getbyte = function(s, i) {
			var x = s.charCodeAt(i);
			if (x > 255) {
				throw new (DOMException as any)(DOMException.INVALID_CHARACTER_ERR);
			}
			return x;
		},
		x = [],
		imax = str.length - str.length % 3;

	if (str.length === 0) {
		return str;
	}
	for (i = 0; i < imax; i += 3) {
		b10 = (getbyte(str, i) << 16) | (getbyte(str, i + 1) << 8) | getbyte(str, i + 2);
		x.push(alpha.charAt(b10 >> 18),
			alpha.charAt((b10 >> 12) & 0x3F),
			alpha.charAt((b10 >> 6) & 0x3f),
			alpha.charAt(b10 & 0x3F));
	}
	switch (str.length - imax) {
		case 1:
			b10 = getbyte(str, i) << 16;
			x.push(alpha.charAt(b10 >> 18) + alpha.charAt((b10 >> 12) & 0x3F) + padchar + padchar);
			break;
		case 2:
			b10 = (getbyte(str, i) << 16) | (getbyte(str, i + 1) << 8);
			x.push(alpha.charAt(b10 >> 18) + alpha.charAt((b10 >> 12) & 0x3F) + alpha.charAt((b10 >> 6) & 0x3F) + padchar);
			break;
	}
	return x.join("");
});
