/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Window {
	/**
	 * Base64 decode - only needed for IE9
	 * @param {string} input
	 * @return {string}
	 */
	atob(input: string): string;
}
