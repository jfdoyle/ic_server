/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Window {
	/**
	 * Base64 encode - only needed for IE9
	 * @param {string} str
	 * @return {string}
	 */
	btoa(input: string): string;
}
