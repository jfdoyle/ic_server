/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface ResizeObserver {
	(callback: (entries?: ResizeObserverEntry[], observer?: ResizeObserver) => void): ResizeObserver;
	observe(target: Element): void;
	unobserve(target: Element): void;
	disconnect(): void;
}

interface ResizeObserverEntry {
	readonly target: Element;
	readonly contentRect: ClientRect;
}

declare var ResizeObserver: ResizeObserver;