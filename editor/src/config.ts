///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

abstract class Config {
	/**
	 * Path to the server
	 */
	static server: string = "../";

	/**
	 * Time between polling for changes (ms)
	 */
	static pollInterval: number = 10000;

	/**
	 * Time between polling for watcher changes (ms)
	 */
	static pollWatchers: number = 5000;

	/**
	 * Whether we can hit the backup button
	 */
	static backup: boolean = false;

	/**
	 * Whether we can hit the replicate button
	 */
	static replicate: boolean = false;
}
