///<reference path="panel.ts" />
///<reference path="../permissions.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface StyleSheet {
	cssRules: CSSRuleList;
}

interface HTMLElement {
	icRequire: Editor.RequireGroup;
	icProperty: Editor.Property;
}

namespace Editor {
	const enum PropertyArea {
		ATTRIBUTE,
		JSON,
		STYLE,
		WIDGET,
		DATA,
	}

	export class Property {
		static styleOnParent = [
			"ic-position",
			"position",
			"width",
			"height",
			"top",
			"right",
			"bottom",
			"left",
		];

		area: PropertyArea;
		path: string;
		value: string;

		constructor(public element: HTMLInputElement) {
			var property = element.dataset["value"].regex(/^([^\.]+)\.([^=]*)(?:=(.*))?$/) as string[];

			this.path = property[1];
			this.value = property[2]; // Only useful for toggle checkbox types
			switch (property[0]) {
				case "attribute":
					this.area = PropertyArea.ATTRIBUTE;
					break;
				case "data":
					this.area = PropertyArea.DATA;
					break;
				case "json":
					this.area = PropertyArea.JSON;
					break;
				case "style":
					this.area = PropertyArea.STYLE;
					this.path = $.camelCase(this.path);
					break;
				case "widget":
					this.area = PropertyArea.WIDGET;
					break;
				default:
					throw new Error("new Property(): Unknown type \"" + property[0] + "\"");
			}
			element.icProperty = this;
			$(element).on("change", this as any, Property.change);
			if (element.nodeName === "INPUT" && element.type === "number") {
				$(element).on("mousewheel", this as any, Property.change);
			}
		}

		/**
		 * Called when the widget selection has changed
		 */
		static update($panel: JQuery, widget?: HTMLElement) {
			//var $offsetParent = $(widget).offsetParent();

			$panel.find("[data-value]").each(function() {
				var value,
					property = (this as HTMLElement).icProperty || new Property(this as HTMLInputElement);

				switch (property.area) {
					case PropertyArea.ATTRIBUTE:
						if (widget) {
							value = $(widget).attr(property.path);
						}
						break;
					case PropertyArea.DATA:
						value = (JSON.parse($(widget).attr("data-json") || "{}") as Object).getTree(property.path);
						break;
					case PropertyArea.JSON:
						if (Editor.currentActivity) {
							value = Editor.currentActivity.getTree(property.path);
						}
						break;
					case PropertyArea.STYLE:
						if (widget) {
							value = window.getComputedStyle(widget, property.path) || "";
							if (property.value) {
								value = String(value).regex(new RegExp(property.value.replace("*", "(.*)"), "")); // TODO Make this regexp safe
							}
						}
						break;
					case PropertyArea.WIDGET:
						if (widget) {
							value = (widget as any)[property.path || "tagName"];
						}
						break;
					default:
						console.log("Unknown Property Area", property);
						return;
				}
				if (property.element.nodeName === "INPUT" && property.element.type === "checkbox") {
					property.element.checked = property.value ? value === property.value : !!value;
				} else {
					// console.log("update", widget, property.element, value)
					$(property.element).val(value);
				}
			});
		}

		/**
		 * Called when the properties panel has changed
		 */
		static change(event: JQuery.Event) {
			var value: string | number | boolean,
				property: Property = event.data,
				widget: HTMLElement = Editor.currentEditWidget,
				input: HTMLInputElement = this as any;

			if (input.nodeName === "INPUT" && input.type === "checkbox") {
				value = input.checked;
			} else {
				value = $(this).val() as string;
			}
			if (/^[\-+]?\d*\.?\d+(?:e[\-+]?\d+)?$/i.test(value as string)) {
				value = parseFloat(value as string);
			}
			switch (property.area) {
				case PropertyArea.ATTRIBUTE:
					if (!widget) {
						return;
					}
					console.log("Trying to set attribute", property.path, "to", value)
					if (Property.styleOnParent.includes(property.path)) {
						if (value) {
							$(widget.parentElement).attr(property.path, value as string);
						} else {
							$(widget.parentElement).removeAttr(property.path);
						}
					}
					if (value) {
						$(widget).attr(property.path, value as string);
					} else {
						$(widget).removeAttr(property.path);
					}
					widget.fixState();
					EditPanel.instance.setHtml();
					break;

				case PropertyArea.DATA:
					if (!widget) {
						return;
					}
					var data = (JSON.parse($(widget).attr("data-json") || "{}") as Object).setTree(property.path, value === false ? undefined : value);
					if (Object.keys(data)) {
						$(widget).attr("data-json", JSON.stringify(data));
					} else {
						$(widget).removeAttr("data-json");
					}
					EditPanel.instance.setHtml();
					break;

				case PropertyArea.JSON:
					Editor.currentActivity.setTree(property.path, value);
					Editor.change = true;
					Button.update();
					return;

				case PropertyArea.STYLE:
					if (!widget) {
						return;
					}
					if (property.value) {
						if (property.value.includes("*")) {
							value = property.value.replace("*", value as string);
						} else {
							value = value ? property.value : "";
						}
					}
					console.log("Trying to set style", property.path, "to", value)
					if (Property.styleOnParent.includes(property.path)) {
						(widget.parentElement.style as any)[property.path] = !isNumber(value) ? value : value >= 0 ? value + "%" : "";
						(widget.style as any)[property.path] = !isNumber(value) ? value : value >= 0 ? value + "%" : "";
					} else {
						(widget.style as any)[property.path] = !isNumber(value) || value >= 0 ? value : "";
					}
					if (property.path === "position") {
						Panel.trigger(PanelEvent.SELECT_WIDGET, undefined);
					}
					EditPanel.instance.setHtml();
					break;

				case PropertyArea.WIDGET:
					console.warn("Trying to change a widget value")
					break;
			}
			RequireGroup.check(PropertiesPanel.instance.$, widget);
		}
	}

	const enum RequireArea {
		SUPPORT,
		WIDGET,
		WIDGET_ATTRIBUTE,
		WIDGET_PARENT,
		WIDGET_PARENT_ATTRIBUTE,
		WIDGET_STYLE,
	}

	const enum RequireCompare {
		EXIST,
		LESS_THAN,
		LESS_THAN_OR_EQUAL,
		EQUAL,
		NOT_EQUAL,
		MORE_THAN_OR_EQUAL,
		MORE_THAN,
		NOT
	}

	class Require {
		constructor(
			public area: RequireArea,
			public path: string[],
			public compare: RequireCompare,
			public value: string | number) {
		}
	}

	export class RequireGroup {
		requirements: Require[] = [];

		constructor(public element: HTMLElement) {
			var requirements = this.requirements;

			element.dataset["require"].split(" ").forEach(function(require) {
				var area, compare, value,
					query = require.regex(/^([^<=>!]*)(<|<=|>=|=|>|!=|!)?(.*)$/) as string[],
					path = query[0].split(".") as string[],
					stripPath = 0;

				switch (path[0]) {
					case "support":
						area = RequireArea.SUPPORT;
						stripPath = 1;
						break;
					case "widget":
						stripPath = 1;
						if (!path[1]) {
							area = RequireArea.WIDGET;
						} else if (path[1] === "style") {
							stripPath = 2;
							area = RequireArea.WIDGET_STYLE;
						} else if (path[1] === "parent") {
							stripPath = 2;
							if (!path[2]) {
								area = RequireArea.WIDGET_PARENT;
							} else {
								area = RequireArea.WIDGET_PARENT_ATTRIBUTE;
							}
						} else {
							area = RequireArea.WIDGET_ATTRIBUTE;
						}
						break;
					default:
						throw new Error("new RequireGroup(): Unknown type \"" + path[0] + "\"");
				}
				path = path.slice(stripPath);
				switch (query[1]) {
					case "<":
						compare = RequireCompare.LESS_THAN;
						break;
					case "<=":
						compare = RequireCompare.LESS_THAN_OR_EQUAL;
						break;
					case "=":
						compare = RequireCompare.EQUAL;
						break;
					case ">=":
						compare = RequireCompare.MORE_THAN_OR_EQUAL;
						break;
					case ">":
						compare = RequireCompare.MORE_THAN;
						break;
					case "!=":
						compare = RequireCompare.NOT_EQUAL;
						break;
					case "!":
						compare = RequireCompare.NOT;
						break;
					default:
						compare = RequireCompare.EXIST;
						break;
				}
				value = query[2];
				requirements.push(new Require(area, path, compare, value))
			});
			element.icRequire = this;
		}

		static check($panel: JQuery, widget?: HTMLElement) {
			$panel.find("[data-require]").each(function(this: HTMLElement) {
				var i: number,
					left: string | number,
					right: string | number,
					parent: HTMLElement,
					require: Require,
					group = this.icRequire || new RequireGroup(this),
					show = !!widget;

				for (i = 0; show && i < group.requirements.length; i++) {
					require = group.requirements[i];
					right = require.value;
					switch (require.area) {
						case RequireArea.SUPPORT:
							left = Editor.rules[require.path[0].toUpperCase()] ? 1 : 0;
							break;
						case RequireArea.WIDGET:
							left = widget.nodeName.toLowerCase();
							break;
						case RequireArea.WIDGET_STYLE:
							left = (widget.style as any)[require.path[0]];
							break;
						case RequireArea.WIDGET_ATTRIBUTE:
							left = (widget.attributes as any)[require.path[0]] && (widget.attributes as any)[require.path[0]].value;
							break;
						case RequireArea.WIDGET_PARENT:
						case RequireArea.WIDGET_PARENT_ATTRIBUTE:
							parent = widget.parentElement;
							if (parent.classList.contains("cke_widget_wrapper")) {
								parent = parent.parentElement;
							}
							if (require.area === RequireArea.WIDGET_PARENT) {
								left = parent.nodeName.toLowerCase();
							} else {
								left = (parent.attributes as any)[require.path[0]] && (parent.attributes as any)[require.path[0]].value;
							}
							break;
						default:
							throw new Error("RequireGroup.check(): Unknown type \"" + require.area + "\"");
					}
					switch (require.compare) {
						case RequireCompare.LESS_THAN:
							show = parseInt(left as string) < parseInt(right as string);
							break;
						case RequireCompare.LESS_THAN_OR_EQUAL:
							show = parseInt(left as string) <= parseInt(right as string);
							break;
						case RequireCompare.EQUAL:
							show = left == right;
							break;
						case RequireCompare.NOT_EQUAL:
							show = left != right;
							break;
						case RequireCompare.MORE_THAN_OR_EQUAL:
							show = parseInt(left as string) >= parseInt(right as string);
							break;
						case RequireCompare.MORE_THAN:
							show = parseInt(left as string) > parseInt(right as string);
							break;
						case RequireCompare.NOT:
							show = !left;
							break;
						case RequireCompare.EXIST:
							show = !!left;
							break;
					}
					// console.log("require", require, left, right)
				}
				$(group.element).toggleClass("disabled", !show).find("input,textarea,select").addBack("input,textarea,select").prop("disabled", !show);
			});
		}
	}

	export class PropertiesPanel extends Panel {
		static readonly instance: PropertiesPanel;

		private activity: ICNode;

		private constructor() {
			super("properties", PERM.ROOT, "preview");

			if (PropertiesPanel.instance) {
				throw new Error("Error: Instantiation failed: Use Properties.getInstance() instead of new.");
			}
			Object.defineProperty(PropertiesPanel, "instance", {
				value: this
			});
		}

		on(event: PanelEvent, args?: any) {
			switch (event) {
				case PanelEvent.STARTUP:
					RequireGroup.check(this.$);
					Property.update(this.$);
					break;

				case PanelEvent.DOM_UPDATED:
				case PanelEvent.SELECT_WIDGET:
					var widget = !args || (args as HTMLElement).nodeName === "IMG" ? null : args;

					// console.log("update properties", args, widget)
					this.getClasses();
					RequireGroup.check(this.$, widget);
					Property.update(this.$, widget);
					break;
			}
		};

		getClasses() {
//			if (this.activity !== Editor.currentActivity) {
//				var rules: string[] = [],
//					styleSheets = (($("#ic_edit iframe")[0] as HTMLIFrameElement || {} as HTMLIFrameElement).contentDocument || {} as Document).styleSheets || [] as any as StyleSheetList,
//					addOption = function(rule: string, tagName?: string) {
//						var parts = rule.split("-"),
//							$options = $((tagName ? "section[data-require*='widget=" + tagName + "']>ul," : "") + "#ic_theme_options").last();
//
//						// console.log(rule, tagName);
//						for (var i = parts.length - 2; i > 0; i--) {
//							var prefix = parts.slice(1, i + 1).join("-"),
//								suffix = parts.slice(i + 1).join("-"),
//								$select = $options.find("select[data-value='attribute.ic-" + prefix + "']");
//
//							if ($select && $select.length) {
//								break;
//							}
//						}
//						// If we can't find our box, or it's a tagName and we want it to show deeper levels
//						if (!$select || !$select.length || (tagName && i < 2 && parts.length > 3)) {
//							prefix = parts.slice(1, parts.length - 1).join("-");
//							suffix = parts[parts.length - 1];
//							$.make("li", "auto-option", tagName ? "data-require=widget=" + tagName : null,
//								$.make("label", "=" + prefix.ucfirst().replace(/-/g, " ") + ": ",
//									$select = $.make("select", "data-value=attribute.ic-" + prefix,
//										$.make("option", "value=", "=---")
//									)
//								)
//							).appendTo($options)
//						}
//						if (!$select.find("option[value='" + suffix + "']").length) {
//							$.make("option", "auto-option", "value=" + suffix, "=" + suffix).appendTo($select);
//						}
//					};
//
//				if (styleSheets.length) {
//					this.activity = Editor.currentActivity;
//					$(".auto-options").remove();
//					[].forEach.call(styleSheets || [], function(styleSheet: StyleSheet) {
//						if (styleSheet.href && !styleSheet.href.includes("preview.css")) {
//							[].forEach.call(styleSheet.cssRules || [], function(cssRule: CSSRule) {
//								(cssRule.cssText.regex(/\.(ict-[a-zA-Z0-9]+-[a-zA-Z0-9\-]+)/g) as string[] || []).forEach(function(className) {
//									var tagName = className.replace(/^ict-([a-z]+).*?$/, "ic-$1"),
//										isWidget = !!Editor.rules[tagName.toUpperCase()];
//
//									if (!isWidget || !/^ict-[a-z]+-[0-9]+$/i.test(className)) {
//										rules.pushOnce(className);
//									}
//								});
//								(cssRule.cssText.regex(/\[ic-([a-zA-Z0-9]+)=["']([a-zA-Z0-9\-]+)["']\]/g) as string[][] || []).forEach(function(attribute) {
//									rules.pushOnce("ict-" + attribute[0] + "-" + attribute[1]);
//								});
//							});
//						}
//					});
//					rules.naturalCaseSort().forEach(function(className) {
//						var tagName = className.replace(/^ict-([a-z]+).*?$/, "ic-$1"),
//							isWidget = !!Editor.rules[tagName.toUpperCase()];
//
//						addOption(className, isWidget ? tagName : undefined);
//					});
//				}
//				return rules.sort();
//			}
		}

		static tabs: {[key: string]: any} = {};
		static vars: {[key: string]: any} = {};
		static requires: {[key: string]: any} = {};

		/**
		 * When someone changes an option
		 * @param {jQuerySelector} $el
		 * @param {string} path
		 * @param {Function} callback
		 * @param {string} star
		 * @param {Event} event
		 */
		static onOptionChange = function($el: JQuery, path: string, callback: Function | boolean, star: string, event: any) {
			var value: string | number | boolean = false;
			//activityData = Editor.currentActivity.data;

			if ($el.is("button")) {
				value = undefined;
			} else if ($el.is("select")) {
				value = (($el[0] as HTMLSelectElement).options[($el[0] as HTMLSelectElement).selectedIndex] as HTMLOptionElement).value as string;
				if (value.search(/^[\-+]?\d*\.?\d+(?:e[\-+]?\d+)?$/i) >= 0) {
					value = parseFloat(value);
				}
			} else if ($el.is("textarea") || $el.attr("type") === "text") {
				value = $el.val() as string;
			} else if ($el.attr("type") === "checkbox") {
				value = ($el[0] as HTMLInputElement).checked;
			}
			//		console.log("Value: (" + (typeof value) + ") " + value);
			if (path[0] === "-") { // handler variable
				PropertiesPanel.vars[path.replace(/^(-+)/, "")] = value;
			} else if (path[0] !== "!" && !path.includes("*") && value !== undefined) { // Not a placeholder or wildcard
				if ($el.data("default") == value) { // Deliberate loose typing
					//activityData.setTree(path, undefined);
				} else {
					//activityData.setTree(path, value);
				}
			}
			//			if (isFunction(callback)) {
			//				callback = (callback as Function).call(activityData, $el, path.replace(/^!+/, ""), value, star);
			//				//			console.log("callback: ", callback);
			//			}
			if (callback !== true) {
				this.checkRequire(); // doLayout redraws and corrects the tabs
			}
			return false;
		};

	}
};
