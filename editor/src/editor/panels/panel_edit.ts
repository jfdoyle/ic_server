///<reference path="panel.ts" />
///<reference path="../../node/node.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace Editor {
	let updating: boolean;

	export enum MediaType {
		UNKNOWN,
		IMAGE,
		VIDEO,
		AUDIO
	};

	export function findKey<K, V>(map: Map<K, V>, value: V): K {
		let key: K;

		map.forEach(function(v, k) {
			if (v === value) {
				key = k;
			}
		})
		return key;
	}

	function updateHtml() {
		ic.setImmediate("editpanel.sethtml", function() {
			EditPanel.instance.setHtml();
			overlay.update();
		});
	}

	// ********** EDIT IFRAME CONTEXT **********

	namespace inject {
		let ic_menu: HTMLElement; // Not really here, just keep TS happy...
		let ic_start_widget: HTMLElement; // Not really here, just keep TS happy...

		export function ic_edit(id: number) {
			//console.log("ic_edit", id);
			let types = [
				"ic-activity",
				"ic-audio",
				"ic-box",
				"ic-button",
				"ic-draggable",
				"ic-droppable",
				"ic-text",
				"ic-toggle"
			],
				types2: string[] = [];

			types.forEach(function(name) {
				types2.push(name + "[" + name + "|" + name + "-display" + "]");
			});
			//			(window.parent as any).ic.setImmediate("edit.mouseup", function() {
			//				((window.parent as any).Editor.EditPanel.instance as EditPanel).moveableElement();
			//			});
			tinymce.init({
				selector: "#" + id,
				auto_focus: id,
				plugins: "paste",
				//valid_elements: "*[*]",
				custom_elements: types.wrap(",", "~"),
				extended_valid_elements: "@[id|class|style|data-json|ic-state|ic-columns|ic-border|ic-padding|ic-margin|ic-*|ic-*-*]," + types2.join(","),
				//protect: [
				//	/<(ic-(?:activity|audio|box|button|draggable|droppable|text|toggle)).*?<\/\1>/
				//],
				fontsize_formats: "60% 75% 88% 100% 120% 150% 200%",
				toolbar: "bold italic underline strikethrough subscript superscript | fontsizeselect | removeformat", // | ic_size
				remove_trailing_brs: false,
				paste_postprocess: function(plugin: any, args: any) {
					console.log(args.node);
				},
				paste_as_text: true,
				//forced_root_block: false,
				//hidden_input: false,
				menubar: false,
				inline: true,
				setup: function(editor: any) {
					editor.on("change cut paste keyup undo redo", function(event: Event) {
						// TODO: Put this on a delay?
						if (event.type !== "keyup" || (event as KeyboardEvent).charCode) {
							(window.parent as any).ic.setImmediate("tinymce", function() {
								((window.parent as any).Editor.EditPanel.instance as EditPanel).setHtml();
							});
						}
					});
					editor.on("selectionchange", function(event: any) {
						(editor.bodyElement as HTMLElement).dispatchEvent(new KeyboardEvent("keyup", {}));
					});
					editor.on("blur", function(event: any) {
						window.getSelection().removeAllRanges();
						((window.parent as any).Editor.EditPanel.instance as EditPanel).el.querySelector("iframe").style.pointerEvents = "";
					});
				}
			});
		}

		export function ic_action(action: string, elements: HTMLElement[]) {
			console.log("action", action, elements)
			let update: boolean;

			switch (action) {
				case "box_split":
				case "box_split_letter":
					elements.forEach(function(element) {
						let el: Text,
							rx = action === "box_split"
								? /(<.*?>|&[a-z]+;|\s+|[\w]+|[\d]+|[^\s\w\d<])/gi
								: /(<.*?>|&[a-z];|[^<])/g,
							walker = document.createNodeIterator(element, NodeFilter.SHOW_TEXT, null, false);

						while ((el = walker.nextNode() as Text)) {
							el.textContent.match(rx).forEach(function(word: string) {
								let toggle = document.createElement("ic-toggle");

								toggle.textContent = word;
								toggle.setAttribute("ic-toggle", "multirange");
								toggle.setAttribute("ic-toggle-display", "text");
								el.parentElement.insertBefore(toggle, el);
							});
							el.parentElement.removeChild(el);
						};
					});
					update = true;
					break;
			}
			if (update) {
				(window.parent as any).ic.setImmediate("tinymce", function() {
					((window.parent as any).Editor.EditPanel.instance as EditPanel).setHtml();
				});
			}
		}

		export function ic_context(event: MouseEvent) {
			//		console.log(event.type, this, ic_menu, event)
			if (ic_menu && ic_menu.parentNode) {
				if (event.button === 2 || event.type === "mouseup") {
					let action = (event.target as HTMLElement).getAttribute("ice-act");

					document.body.removeChild(ic_menu);
					ic_menu = null;
					if (action) {
						ic_action(action, [].slice.call(document.querySelectorAll(".ice-select")));
					}
				}
			} else if ((event.target as HTMLElement).closestWidget) {
				let widget = (event.target as HTMLElement).closestWidget();

				if (widget && widget.closest("ic-screen")) {
					switch (event.type) {
						case "contextmenu":
							let selected = document.querySelectorAll(".ice-select");

							if (!selected.length) {
								widget.classList.add("ice-select");
								selected = [widget] as any;
							}
							if (!event.ctrlKey) {
								//							console.log(event.type, event.target)
								let target: string,
									columns = selected.length === 1 ? selected[0].getAttribute("ic-columns") || 0 : -1;

								[].some.call(selected, function(element: HTMLElement) {
									let name = element.tagName.replace("IC-", "").toLowerCase();

									if (!target) {
										target = name;
									} else if (target !== name) {
										target = null;
										return true;
									}
								});
								ic_menu = document.createElement("div");
								ic_menu.id = "ic_context_menu";
								ic_menu.innerHTML = "<div>"
									+ "<h1>" + target.ucfirst() + "</h1>"
									+ "<div></div>"
									+ "<ul>"
									+ (columns >= 0 ? "<li><a>Columns<span>&rtrif;</span><ul>"
										+ "<li>1</li>"
										+ "<li>2</li>"
										+ "<li>3</li>"
										+ "<li>4</li>"
										+ "<li>5</li>"
										+ "</ul></a></li>"
										: "")
									+ "</ul>"
									// IC-BOX
									+ (target === "box" ? "<ul ice-for=\"box\">"
										+ "<li><span>ic-box</span></li>"
										+ "<li><a ice-act=\"box_split\">Split into Toggles (per word)</a></li>"
										+ "<li><a ice-act=\"box_split_letter\">Split into Toggles (per letter)</a></li>"
										+ "<li><hr></li>"
										+ "<li><a ice-act=\"a3\">Example 1</a></li>"
										+ "<li><a ice-act=\"a4\">Example 2</a></li>"
										+ "</ul>"
										: "")
									// IC-TOGGLE
									+ (target === "toggle" ? "<ul ice-for=\"toggle\">"
										+ "<li><span>ic-toggle</span></li>"
										+ "</ul>"
										+ "</div>"
										: "");
								ic_menu.style.paddingLeft = String(event.pageX - 2) + "px";
								ic_menu.style.paddingTop = String(event.pageY - 2) + "px";
								document.body.appendChild(ic_menu);
								event.preventDefault();
								return false;
							}
							break;
					}
				}
			}
		}

		export function ic_startup(id: string) {
			ic_menu = null;
			document.addEventListener("contextmenu", ic_context, false);
			document.addEventListener("mousedown", ic_context, true);
			document.addEventListener("mouseup", ic_context, true);
			[].forEach.call(document.querySelectorAll("p[id]"), function(el: HTMLParagraphElement) {
				let closest = el.closest("[ice]");

				if (!closest || closest.getAttribute("ice") != id) {
					el.removeAttribute("id");
				}
			});
		}
	};

	// ********** EDITOR CONTEXT **********

	let currentImage: HTMLImageElement;
	export let imagePopup: W2UI.W2Popup;
	export let currentEditWidget: HTMLElement;

	export function openImageBank() {
		let setImagePath = function() {
			currentImage.src = $(this).data("src");
			imagePopup.close();
			imagePopup = undefined;
			updateHtml();
			currentEditWidget = currentImage.closestWidget();
			querySelectorAll(".ice-select").forEach(function(element: HTMLElement) {
				if (element !== currentEditWidget) {
					element.classList.remove("ice-select");
				}
			});
			currentEditWidget.classList.add("ice-select");
			overlay.update();
		},
			$popup = $("<div>");

		console.log("open image bank", this)
		imagePopup = $().w2popup({
			title: "<i class=\"fa fa-file-image-o\"></i> Select Image",
			body: "<div id=\"image_bank_popup\" class=\"ext\"><span><span><i class=\"fa fa-th-large active\"></i><i class=\"fa fa-list\"></i></span><div><input id=\"image_bank_search\" placeholder=\"type to filter\" type=\"text\"/></div><label class=\"fa fa-folder-open\"><input type=\"file\"/></label></span><div id=\"image_bank_list\"></div></div>",
			style: "",
			width: window.innerWidth * 0.8,
			height: window.innerHeight * 0.8,
			onOpen: function() {
				ic.setImmediate("image_bank", function() {
					$("#image_bank_list").append($popup.children());
					$("#image_bank_popup").toggleClass("thumbs", Editor.getSetting("options.image_list")).toggleClass("list", !Editor.getSetting("options.image_list"));
					$("#image_bank_popup>span>span>i")
						.on("click", function() {
							Editor.setSetting("options.image_list", $("#image_bank_popup").toggleClass("list thumbs").hasClass("list"));
						});
					$("#image_bank_search")
						.focus()
						.on("keyup change", function() {
							let value = (this as HTMLInputElement).value.trim().toLowerCase();

							$("#image_bank_list").children().each(function(index, el) {
								let image = this as HTMLElement;

								image.style.display = image.innerText.toLowerCase().indexOf(value) >= 0 ? "" : "none";
							});
						});
				});
			}
		});

		Editor.currentActivity.getTemplateFolders().then(function(nodes) {
			let files: string[] = [],
				id = Editor.currentActivity._id,
				src = currentImage.getAttribute("src"),
				$target = $("#image_bank_list").add($popup).first(), // In case we're late loading
				getFileType = function(path: string): MediaType {
					switch ((path.regex(/\.([^\.]+)$/) as string).toLowerCase()) {
						case "gif":
						case "jpg":
						case "png":
						case "svg":
							return MediaType.IMAGE;

						case "mp3":
						case "m4a":
						case "ogg":
							return MediaType.AUDIO;

						case "mp4":
						case "m4v":
							return MediaType.VIDEO;
					}
					return MediaType.UNKNOWN;
				};

			nodes.pushOnce.apply(nodes, Editor.currentActivity.getParents(true));
			nodes.pushOnce.apply(nodes, Editor.currentActivity.getSiblings());
			console.log(nodes)
			nodes.forEach(function(node) {
				files.pushOnce.apply(files, node._files || []);
			})
			for (let file in Editor.fileCache) {
				if (Editor.fileCache.hasOwnProperty(file) && getFileType(file) === MediaType.IMAGE) {
					let filePath = "../assets/" + id + "/" + file,
						fileName = file.regex(/([^\/]+)$/) as string,
						fileExt = (fileName.regex(/\.([^\.]+)$/) as string).toLowerCase();

					files.remove(filePath);
					$("<a class=\"new\"><div data-type=\"" + fileExt + "\" style=\"background-image:url('data:image/" + fileExt + ";base64," + Editor.fileCache[fileName] + "');\"></div><h3>" + fileName.replace(/\.[^\.]+$/, "") + "</h3></a>")
						.toggleClass("active", src === filePath)
						.data("src", filePath)
						.on("click", setImagePath)
						.appendTo($target);
				}
			}
			files.naturalCaseSort().forEach(function(path: string) {
				if (getFileType(path) === MediaType.IMAGE) {
					let filePath = "../" + path,
						fileName = path.regex(/([^\/]+)$/) as string,
						fileExt = (fileName.regex(/\.([^\.]+)$/) as string).toLowerCase();

					$("<a><div data-type=\"" + fileExt + "\" style=\"background-image:url('" + filePath + "');\"></div><h3>" + fileName.replace(/\.[^\.]+$/, "") + "</h3></a>")
						.toggleClass("active", src === filePath)
						.data("src", filePath)
						.on("click", setImagePath)
						.appendTo($target);
				}
			});
		});
	}

	function stopPropagation(event: MouseEvent) {
		if (!(event.target as HTMLElement).closest(".mce-tinymce,.mce-panel")) {
			event.stopPropagation();
		}
	}

	let focusElement: HTMLElement,
		templates: Map<HTMLElement, HTMLTemplateElement> = new Map(),
		wizards: Map<HTMLElement, HTMLElement> = new Map();

	const enum MouseMode {
		SELECT,
		MOVE,
		NW,
		N,
		NE,
		W,
		E,
		SW,
		S,
		SE
	}

	export namespace overlay {
		/**
		 * Map of activity element -> overlay element
		 */
		let elements: Map<HTMLElement, HTMLElement> = new Map(),
			boundingBoxCache: Map<HTMLElement, ClientRect> = new Map(),
			startX: number,
			startY: number;

		export function add(el: HTMLElement) {
			if (el && !elements.get(el)) {
				let position = window.getComputedStyle(el).position === "absolute",
					container = document.querySelector("#ic_edit>.ic_edit"),
					overlay = container.appendChild((getElementById("template_overlay" + (position ? "_resize" : "")) as HTMLTemplateElement).content.cloneNode(true).firstChild) as HTMLElement,
					templateElement = templates.get(el),
					options = ((templateElement && templateElement.getAttribute("data-template")) || "").toLowerCase().split(/[\s,|]+/);

				//				console.log("overlay", overlay)
				//				if (!elements.size) {
				//					overlay.classList.add("active");
				//				}
				if (!el.nodeName.startsWith("IC-")) {
					overlay.classList.add("element");
				}
				if (getComputedStyle(el).position.toLowerCase() === "absolute") {
					overlay.classList.add("move");
				}
				if (el.style.width || el.style.height) {
					overlay.classList.add("resizable");
				}
				if (options.includes("autowidth") || options.includes("autoheight") || el.style.height === "auto") {
					[].forEach.call(overlay.querySelectorAll(".nw,.n,.ne,.w,.e,.sw,.s"), (el: HTMLElement) => {el.style.display = "none";});
				}
				elements.set(el, overlay);
				update();
			}
		}

		export function find(element: HTMLElement) {
			if (element) {
				return findKey(elements, element);
			}
		}

		export function select(element: HTMLElement, only?: boolean) {
			let realElement: HTMLElement;

			elements.forEach(function(overlay, el) {
				overlay.classList.remove("passive");
				if (element === overlay || element === el) {
					realElement = el;
					overlay.classList.add("active");
					console.log("active:", templates.get(el), el);
				} else {
					if (only) {
						overlay.classList.remove("active");
					}
				}
			});
			if (only && realElement) {
				let parent = realElement,
					size: boolean,
					move: boolean;

				while (parent && !move && !size) {
					let overlay = elements.get(parent);

					if (overlay) {
						let classList = overlay.classList;

						if (!size && classList.contains("resizable")) {
							overlay.classList.add("passive");
							size = true;
						}
						if (!move && classList.contains("move")) {
							overlay.classList.add("passive");
							move = true;
						}
					}
					parent = parent.parentElement;
				}
				return elements.get(realElement);
			}
		}

		export function get(): HTMLElement[] {
			let active: HTMLElement[] = [];

			elements.forEach(function(overlay, el) {
				if (overlay.classList.contains("active")) {
					active.push(el);
				}
			});
			return active;
		}

		export function remove(el: HTMLElement) {
			let overlay = elements.get(el);

			if (overlay) {
				elements.delete(el);
				overlay.parentElement.removeChild(overlay);
				update();
				Panel.trigger(PanelEvent.SELECT_WIDGET, el);
			}
		}

		export function clear() {
			elements.forEach(function(overlay, el) {
				overlay.parentElement.removeChild(overlay);
			});
			elements.clear();
			boundingBoxCache.clear();
		}

		export function getSpacingRect(): ClientRect {
			if (elements.size) {
				let rect = {
					top: Number.NEGATIVE_INFINITY,
					left: Number.NEGATIVE_INFINITY,
					right: Number.POSITIVE_INFINITY,
					bottom: Number.POSITIVE_INFINITY,
					width: Number.POSITIVE_INFINITY,
					height: Number.POSITIVE_INFINITY
				};

				elements.forEach(function(overlay, el) {
					if (overlay.classList.contains("active")) {
						let box = boundingBoxCache.get(el),
							parentRect = boundingBoxCache.get(el.offsetParent as HTMLElement);

						rect.top = Math.max(rect.top, parentRect.top - box.top);
						rect.left = Math.max(rect.left, parentRect.left - box.left);
						rect.right = Math.min(rect.right, parentRect.right - box.right);
						rect.bottom = Math.min(rect.bottom, parentRect.bottom - box.bottom);
						rect.width = Math.min(rect.width, box.width);
						rect.height = Math.min(rect.height, box.height);
					}
				});
				return rect;
			}
			return {
				top: 0,
				left: 0,
				right: 0,
				bottom: 0,
				width: 0,
				height: 0
			};
		}

		export function start(clientX: number, clientY: number) {
			startX = clientX;
			startY = clientY;
			boundingBoxCache.clear();
			elements.forEach(function(overlay, el) {
				if (overlay.classList.contains("active")) {
					let parent = el.offsetParent as HTMLElement;

					if (!boundingBoxCache.get(parent)) {
						let style = getComputedStyle(parent),
							borderBox = style.boxSizing === "border-box",
							box = parent.getBoundingClientRect(),
							left = box.left + (borderBox ? parseFloat(style.paddingLeft) : 0),
							right = box.right - (borderBox ? parseFloat(style.paddingRight) : 0),
							top = box.top + (borderBox ? parseFloat(style.paddingTop) : 0),
							bottom = box.bottom - (borderBox ? parseFloat(style.paddingBottom) : 0);

						boundingBoxCache.set(parent, {
							left: left,
							right: right,
							top: top,
							bottom: bottom,
							width: right - left,
							height: bottom - top
						});
					}
					if (!boundingBoxCache.get(el)) {
						boundingBoxCache.set(el, el.getBoundingClientRect());
					}
				}
			});
		}

		export function move(clientX: number, clientY: number, singleCall?: boolean) {
			if (singleCall) {
				start(0, 0);
			}
			let rect = getSpacingRect(),
				deltaX = Math.range(rect.left, clientX - startX, rect.right),
				deltaY = Math.range(rect.top, clientY - startY, rect.bottom);

			//console.log("left", deltaX, " = ", rect.left, clientX - startX, rect.right, "top", deltaY, " = ", rect.top, clientY - startY, rect.bottom)
			elements.forEach(function(overlay, el) {
				if (overlay.classList.contains("active")) {
					let elBox = boundingBoxCache.get(el),
						parentBox = boundingBoxCache.get(el.offsetParent as HTMLElement),
						style = el.style;

					style.left = ((elBox.left - parentBox.left + deltaX) * 100 / parentBox.width) + "%";
					style.top = ((elBox.top - parentBox.top + deltaY) * 100 / parentBox.height) + "%";
				}
			});
			update();
		}

		export function size(mode: MouseMode, clientX: number, clientY: number) {
			let rect = getSpacingRect(),
				deltaX = 0,
				deltaY = 0,
				deltaWidth = 0,
				deltaHeight = 0;

			switch (mode) {
				case MouseMode.NW:
				case MouseMode.W:
				case MouseMode.SW:
					deltaWidth = -(deltaX = Math.range(rect.left, clientX - startX, rect.width));
					break;

				case MouseMode.NE:
				case MouseMode.E:
				case MouseMode.SE:
					deltaWidth = Math.range(-rect.width, clientX - startX, rect.right);
					break;
			}
			switch (mode) {
				case MouseMode.NW:
				case MouseMode.N:
				case MouseMode.NE:
					deltaHeight = -(deltaY = Math.range(rect.top, clientY - startY, rect.height));
					break;

				case MouseMode.SW:
				case MouseMode.S:
				case MouseMode.SE:
					deltaHeight = Math.range(-rect.height, clientY - startY, rect.bottom);
					break;
			}

			elements.forEach(function(overlay, el) {
				let classList = overlay.classList;

				if ((classList.contains("active") || classList.contains("passive")) && classList.contains("resizable") && overlay.getElementsByClassName("dragging")) {
					let elBox = boundingBoxCache.get(el),
						parentBox = boundingBoxCache.get(el.offsetParent as HTMLElement),
						style = el.style,
						options = (overlay.getAttribute("data-template") || "").toLowerCase().split(/[\s,|]+/),
						noP = !el.getElementsByTagName("p").length,
						scrollWidth: number;

					//console.log("left", deltaX, " = ", rect.left, clientX - startX, rect.width, ", ", elBox.left, parentBox.width, ((elBox.left - parentBox.left + deltaX) * 100 / parentBox.width) + "%")
					style.left = ((elBox.left - parentBox.left + deltaX) * 100 / parentBox.width) + "%";
					if (!options.includes("autowidth")) {
						scrollWidth = 10;

						if (noP) {
							style.width = "auto";
							scrollWidth = el.scrollWidth;
						}
						//console.log("width", deltaWidth, " = ", -rect.width, clientX - startX, rect.right, ", ", elBox.width, parentBox.width, ((elBox.width + deltaWidth) * 100 / parentBox.width) + "%")
						style.width = (Math.max(scrollWidth || 10, elBox.width + deltaWidth) * 100 / parentBox.width) + "%";
					}
					//console.log("top", deltaY, " = ", rect.top, clientY - startY, rect.height, ", ", elBox.top, parentBox.height, ((elBox.top - parentBox.top + deltaY) * 100 / parentBox.height) + "%")
					style.top = ((elBox.top - parentBox.top + deltaY) * 100 / parentBox.height) + "%";
					if (!(options.includes("autoheight") || style.height === "auto")) {
						let scrollHeight = 0;

						if (noP || scrollWidth) {
							style.height = "auto";
							scrollHeight = el.scrollHeight;
						}
						//console.log("height", deltaHeight, " = ", -rect.height, clientY - startY, rect.bottom, ", ", elBox.height, parentBox.height, ((elBox.height + deltaHeight) * 100 / parentBox.height) + "%")
						style.height = (Math.max(scrollHeight || 10, elBox.height + deltaHeight) * 100 / parentBox.height) + "%";
					}
				}
			});
			update();
		}

		function internalUpdate() {
			elements.forEach(function(overlay, el) {
				let box = el.getBoundingClientRect(),
					style = overlay.style;

				style.top = box.top + "px";
				style.left = box.left + "px";
				style.width = box.width + "px";
				style.height = box.height + "px";
			});
		}

		export function update() {
			ic.setImmediate("panel_edit_overlay_update", internalUpdate);
		}
	};

	export class EditPanel extends Panel {
		static readonly instance: EditPanel;

		contents: HTMLElement;

		private constructor() {
			super("edit", PERM.ROOT | PERM.NODE_CONTENT, "main");

			if (EditPanel.instance) {
				throw new Error("Error: Instantiation failed: Use EditPanel.instance instead of new.");
			}
			Object.defineProperty(EditPanel, "instance", {
				value: this
			});
			window.console.log(this.el)
			this.contents = this.el.querySelector(".ic_edit") as HTMLElement;
			this.contents.addEventListener("dblclick", this.handleDblClick, false);
			this.contents.addEventListener("keydown", this.onKeyDown, false);
			this.contents.addEventListener("wheel", this.onScroll, false);
			this.el.addEventListener("mousedown", this.onMouseDown, true);
			this.el.addEventListener("touchstart", this.onMouseDown, true);
			((new (ResizeObserver as any)(overlay.update)) as ResizeObserver).observe(document.getElementById("ic_edit"));
		}

		onScroll = (event: WheelEvent) => {
			let startX = event.clientX,
				startY = event.clientY,
				contentsBox = this.contents.getBoundingClientRect(),
				left = startX - contentsBox.left,
				top = startY - contentsBox.top,
				iframe = this.el.querySelector("iframe") as HTMLIFrameElement,
				doc = iframe.contentDocument,
				win = iframe.contentWindow,
				el = doc && doc.elementFromPoint(left, top) as HTMLElement,
				deltaY = event.deltaY;

			while (el) {
				if (/auto|scroll/.test(win.getComputedStyle(el).overflow)) {
					if (deltaY < 0 && el.scrollTop
						|| deltaY > 0 && el.scrollTop + el.clientHeight < el.scrollHeight) {
						el.scrollTop += deltaY;
						overlay.update();
						return;
					}
				}
				el = el.parentElement;
			}
		}

		onKeyDown = (event: KeyboardEvent) => {
			let ctrlKey = event.ctrlKey,
				delta = event.shiftKey ? (ctrlKey ? Number.MAX_VALUE : 100) : (ctrlKey ? 20 : 1);

			switch (event.key) {
				case "Backspace":
				case "Delete":
					let elements = overlay.get();

					if (elements.length && confirm("Are you sure you wish to delete the selected component" + (elements.length > 1 ? "s" : "") + "?")) {
						elements.forEach(function(el) {
							el.parentElement.removeChild(el);
						});
						overlay.clear();
						updateHtml();
					}
					break;

				case "ArrowLeft":
					overlay.move(-delta, 0, true);
					break;

				case "ArrowUp":
					overlay.move(0, -delta, true);
					break;

				case "ArrowRight":
					overlay.move(delta, 0, true);
					break;

				case "ArrowDown":
					overlay.move(0, delta, true);
					break;

				default:
					return;
			}
			event.preventDefault();
		};

		/**
		 * Create a partial wizard, nesting if needed inside parent / properties
		 */
		createWizard = (element: HTMLElement, template: HTMLTemplateElement, parent: HTMLElement) => {
			console.log("createWizard", element, template, parent)
			if (!wizards.has(element)) {
				let options = (template.getAttribute("data-template") || "").split(","),
					doc = template.content,
					$contents: JQuery,
					hasRepeat = options.includes("repeat"),
					$wizard = $.make("div", "template" + (hasRepeat ? " repeat" : ""),
						$.make("div",
							$.make("p", "=" + template.getAttribute("title")),
							$contents = $.make("div")
						))
						.data("element", element)
						.on("click", (event) => {
							let block = event.target.closest(".template");

							//console.log("click", event.target, block)
							if (!block.classList.contains("active")) {
								Array.from(wizards.keys()).forEach((wizard => {
									if (wizards.get(wizard) === block) {
										overlay.select(wizard, !event.ctrlKey);
									}
								}));
								this.activeWizards();
							}
							event.stopImmediatePropagation();
						}),
					wizard = $wizard[0];

				if (options.includes("repeat")) {
					$wizard.addClass("sort");
					$wizard.prepend(
						$.make("i", "fa fa-stack",
							$.make("i", "fa fa-stack-1x fa-circle"),
							$.make("i", "fa fa-stack-1x fa-minus-circle")
						),
						// TODO: Delete option
						$.make("div", "delete", "=&nbsp;Delete?&nbsp;"));
					$wizard.append(
						$.make("i", "fa fa-" + (element.hasAttribute("ic-random") ? "random" : "bars")));
				}
				if (doc.querySelector("ic-toggle")) {
					$wizard.addClass("shrink");
					$contents.append(
						$.make("label",
							$.make("span", "=Correct"),
							$.make("input", "type=checkbox", /"answer":"checked"/.test(element.getAttribute("data-json")) ? "checked=" : null)
								.data({
									"type": "answer",
									"element": element
								})
								.on("change", function(event) {
									MarkingPanel.instance.onOptionChange.call(this, event, true);
									updateHtml();
								})));
				}
				if (options.includes("text")) {
					$wizard.addClass("grow").removeClass("shrink");
					$contents.append(
						$.make("label",
							$.make("span", "=Content"),
							$.make("p", "editable", "=" + element.innerHTML)));
				}


				// TODO: options, include classes? popup to add more
				//				$contents.append(
				//					$.make("label", "shrink",
				//						$.make("span", "=Testing option"),
				//						$.make("span", "=Testing value")));



				if (!$wizard.hasClass(".grow")
					&& !$wizard.hasClass(".shrink")
					&& Array.prototype.find.call(doc.childNodes, (node: HTMLElement) => node.nodeName === "IC-BOX")
					&& !doc.querySelector("ic-box>*")) {
					$wizard.addClass("title");
				}

				console.log("options", options, doc.childNodes, Array.prototype.find.call(doc.childNodes, (node: HTMLElement) => node.nodeName === "IC-BOX"), doc.querySelector("ic-box>*"))
				wizards.set(element, wizard);
				if (parent) {
					let parentElement = wizards.get(parent).firstElementChild;

					if (parentElement.tagName.toLowerCase() !== "div") {
						parentElement = parentElement.nextElementSibling.nextElementSibling;
					}
					parentElement.lastElementChild.appendChild(wizard);
				} else {
					$("#ic_properties").append(wizard);
				}
			}
		}

		editTextWizards = () => {
			$("#ic_properties .fa-bars,#ic_properties .fa-random")
				.on("dblclick", function(event: JQuery.Event) {
					let $target = $(event.target),
						$template = $target.closest(".template"),
						element = $template.data("element") as HTMLElement;

					if (element) {
						let enable = element.hasAttribute("ic-random"),
							$siblings = event.ctrlKey ? $target : $target.closest(".ui-sortable").find(">div>.ui-sortable-handle");

						$siblings.each(function(index, template) {
							let $template = $(template).closest(".template"),
								element = $template.data("element") as HTMLElement;

							if (enable) {
								element.removeAttribute("ic-random");
								$(template).addClass("fa-bars").removeClass("fa-random");
							} else {
								element.setAttribute("ic-random", "");
								$(template).addClass("fa-random").removeClass("fa-bars");
							}
						});
						updateHtml();
					}
				})
				.closest(".template").parent().sortable({
					//					axis: "y",
					//					containment: "parent",
					handle: ".fa-bars",
					tolerance: "pointer",
					items: ">.sort",
					forcePlaceholderSize: true,
					start: function(event, ui) {
						ui.placeholder.height(ui.helper[0].scrollHeight);
					},
					change: function(event, ui) {
						let element = findKey(wizards, ui.item[0]),
							elementParent = element.parentElement,
							elementNext = element.nextSibling,
							swap = findKey(wizards, ui.placeholder.next()[0]) || null,
							swapParent = swap && swap.parentElement,
							swapNext = swap && swap.nextSibling;

						if (!swap || elementParent === swapParent) {
							elementParent.insertBefore(element, element === swap ? null : swap || null);
						} else {
							// TODO: Handle a sortable that's spread around the screen
							if (elementNext !== swap) {
								elementParent.insertBefore(swap, elementNext);
							}
							if (swapNext !== element) {
								swapParent.insertBefore(element, swapNext);
							}
						}
						overlay.update();
						updateHtml();
					}
				});

			tinymce.init({
				selector: "p.editable",
				plugins: "paste",
				//valid_elements: "*[*]",
				//protect: [
				//	/<(ic-(?:activity|audio|box|button|draggable|droppable|text|toggle)).*?<\/\1>/
				//],
				fontsize_formats: "60% 75% 88% 100% 120% 150% 200%",
				toolbar: "bold italic underline strikethrough subscript superscript | fontsizeselect | removeformat", // | ic_size
				remove_trailing_brs: false,
				paste_as_text: true,
				//forced_root_block: false,
				//hidden_input: false,
				menubar: false,
				branding: false,
				statusbar: false,
				inline: true,
				setup: function(editor: any) {
					editor.on("change cut paste keyup undo redo", function(event: Event) {
						// TODO: Put this on a delay?
						//						if (event.type !== "keyup" || (event as KeyboardEvent).charCode) {
						//						}
						let target: HTMLElement = event.target as HTMLElement;

						if ((target as any).bodyElement) {
							target = (target as any).bodyElement;
						}
						if (target instanceof HTMLElement) {
							let element = $(target).closest(".template").data("element") as HTMLElement,
								html = tinymce.activeEditor.getContent();

							if (element.innerHTML !== html) {
								$(element).html(html);
								overlay.update();
								updateHtml();
							}
						}

					});
					//					editor.on("selectionchange", function(event) {
					//						(editor.bodyElement as HTMLElement).dispatchEvent(new KeyboardEvent("keyup", {}));
					//					});
					//					editor.on("blur", function(event) {
					//					});
				}
			});
		}

		clearWizards = () => {
			Array.from(wizards.values()).forEach((element: HTMLElement) => {
				element.parentElement && element.parentElement.removeChild(element);
			});
			wizards.clear();
		}

		activeWizards = () => {
			let active = overlay.get();

			Array.from(wizards.keys()).forEach((element: HTMLElement) => {
				let wizard = wizards.get(element);

				if (active.includes(element)) {
					wizard.classList.add("active");
				} else {
					wizard.classList.remove("active");
				}
			});
		}

		onMouseDown = (event: MouseEvent) => {
			let target = event.target as HTMLElement,
				classList = target.classList,
				mode = MouseMode.MOVE,
				startX = event.clientX,
				startY = event.clientY,
				ice = target.closest("[ice]"),
				isOverlay = classList.contains("overlay"),
				isActive = isOverlay && classList.contains("active"),
				isMove = isActive && classList.contains("move"),
				isResize = classList.contains("resize"),
				isContainer = classList.contains("ic_edit");

			if (ice && ice.getAttribute("ice") !== String(Editor.currentActivity._id)) {
				overlay.clear();
				console.log("Not clicking on current activity", ice)
				return;
			}
			if (isOverlay && !isActive) {
				overlay.select(target, !event.ctrlKey);
				this.activeWizards();
				Panel.trigger(PanelEvent.SELECT_WIDGET, target);
			} else if (isContainer) {
				// clicking directly on the edit panel
				let contentsBox = this.contents.getBoundingClientRect(),
					left = startX - contentsBox.left,
					top = startY - contentsBox.top,
					iframe = this.el.querySelector("iframe") as HTMLIFrameElement,
					doc = iframe.contentDocument,
					el = doc && doc.elementFromPoint(left, top) as HTMLElement,
					first: HTMLElement,
					base: HTMLElement;

				if (!event.ctrlKey) {
					this.clearWizards();
					templates.clear();
					overlay.clear();
				}
				el.parentElements("[ice]", true).forEach(element => {
					let template = findTemplate(element);

					if (template) {
						templates.set(element, template);
						first = first || element;
						base = element;
						//return (template.getAttribute("data-template") || "").includes("component");
					}
				});
				if (base) {
					let walker = document.createTreeWalker(base, NodeFilter.SHOW_ELEMENT),
						element = walker.currentNode as HTMLElement,
						lastTemplate: HTMLTemplateElement;

					while (element) {
						let template = templates.get(element);

						if (!template) {
							template = findTemplate(element);
							if (template) {
								templates.set(element, template);
							}
						}
						if (template) {
							let parent = element.parentElements(base).intersect(Array.from(templates.keys())).first();

							overlay.add(element);
							this.createWizard(element, template, parent);
							//if (lastTemplate && lastTemplate !== template && template.classList.contains("repeat")) {
							// TODO: Create the "+" button for adding a repeat
							//}
						}
						lastTemplate = template;
						element = walker.nextNode() as HTMLElement;
					}
					//					target.parentElements("[ice]", true).find(element => templates.has(element)).classList.add("active");
					console.log("templates", templates)
					overlay.select(first);
					this.editTextWizards();
					this.activeWizards();
					Panel.trigger(PanelEvent.SELECT_WIDGET, first);
				}
			} else if (isMove || isResize) {
				let dragStarted = false;

				event.preventDefault();
				overlay.start(startX, startY);
				if (isResize) {
					if (classList.contains("nw")) {
						mode = MouseMode.NW;
					} else if (classList.contains("n")) {
						mode = MouseMode.N;
					} else if (classList.contains("ne")) {
						mode = MouseMode.NE;
					} else if (classList.contains("w")) {
						mode = MouseMode.W;
					} else if (classList.contains("e")) {
						mode = MouseMode.E;
					} else if (classList.contains("sw")) {
						mode = MouseMode.SW;
					} else if (classList.contains("s")) {
						mode = MouseMode.S;
					} else if (classList.contains("se")) {
						mode = MouseMode.SE;
					}
					//					target = overlay.select(target.parentElement, true);
					//					classList = target.classList;
				}
				$("iframe").css("pointer-events", "none");
				$(document).on({
					"mousemove.drag touchmove.drag": (event: JQuery.Event) => {
						let clientX = event.clientX,
							clientY = event.clientY,
							deltaX = clientX - startX,
							deltaY = clientY - startY;

						event.preventDefault();
						if (!dragStarted && (Math.abs(deltaX) > 3 || Math.abs(deltaY) > 3)) {
							dragStarted = true;
							classList.add("dragging");
						}
						if (dragStarted) {
							let snap = event.shiftKey ? 100 : event.ctrlKey ? 20 : 1;

							clientX -= deltaX % snap;
							clientY -= deltaY % snap;
							if (mode === MouseMode.MOVE) {
								overlay.move(clientX, clientY);
							} else {
								overlay.size(mode, clientX, clientY);
							}
						}
					},
					"mouseup.drag touchend.drag": (event: JQuery.Event) => {
						event.preventDefault();
						$(document).off(".drag");
						$("iframe").css("pointer-events", "");
						if (dragStarted) {
							classList.remove("dragging");
							ic.setImmediate("edit.sethtml", updateHtml);
						} else {
							// click
							window.console.log("click")
						}
					}
				});
			}
		};

		handleDblClick = (event: MouseEvent) => {
			let box = this.contents.getBoundingClientRect(),
				left = event.clientX - box.left,
				top = event.clientY - box.top,
				iframe = this.el.querySelector("iframe") as HTMLIFrameElement,
				doc = iframe.contentDocument,
				target = doc && doc.elementFromPoint(left, top) as HTMLElement,
				ice = target.closest("[ice]");

			window.console.log("double click", target)
			if (target && ice && ice.getAttribute("ice") === String(currentActivity._id)) {
				let img = target.closest("img") as HTMLImageElement;

				if (img) {
					currentImage = img;
					openImageBank();
					return false;
				}
				let subframe = querySelector(target, "iframe") as HTMLIFrameElement;

				if (subframe) {
					let src = prompt("Please enter URL for the iFrame", subframe.src || "");

					if (src !== null) {
						subframe.src = src.replace("https://youtu.be/", "https://www.youtube.com/embed/");
						updateHtml();
					}
					return false;
				}
				let p = target.closest("p[id]") as HTMLParagraphElement;

				if (p) {
					if (p.querySelectorAll("[ic-toggle],img").length) {
						alert("Currently unable to edit text with inline images or toggles\n\n(TinyMCE bug #3275)");
					} else {
						overlay.clear();
						this.el.querySelector("iframe").style.pointerEvents = "all";
						if (p.classList.contains("mce-content-body")) {
							p.focus();
						} else {
							(iframe.contentWindow as any).eval("ic_edit('" + p.id + "')");
						}
					}
					return false;
				}
				let popup = target.closest("[editor-img]") as HTMLElement;

				if (popup) {
					let img = createElement("img");

					img.src = popup.getAttribute("editor-img");
					img.onload = function() {
						let width = img.naturalWidth,
							height = img.naturalHeight,
							maxWidth = window.innerWidth - 25,
							maxHeight = window.innerHeight - 61,
							widthScale = maxWidth / width,
							heightScale = maxHeight / height,
							scale = Math.min(1, widthScale, heightScale);

						if (scale < 1) {
							if (widthScale <= heightScale) {
								img.style.width = maxWidth + "px";
								img.style.height = "auto";
							} else {
								img.style.width = "auto";
								img.style.height = maxHeight + "px";
							}
						} else {
							img.style.width = "auto";
							img.style.height = "100%";
						}
						$().w2popup({
							title: "<i class=\"fa fa-cogs\"></i> " + (popup.getAttribute("editor-title") || "Screenshot"),
							body: "<div id=\"screenshot_popup\" style=\"padding-top:7px;\"><img src=\"" + img.src + "\" style=\"width:" + img.style.width + ";height:" + img.style.height + ";\"></div>",
							style: "",
							width: width * scale + 14,
							height: height * scale + 51,
							onOpen: function() {
							}
						});
					}
					return false;
				}
			}
		}

		on(event: PanelEvent, template?: HTMLTemplateElement, position?: {top: number, left: number}) {
			switch (event) {
				case PanelEvent.ACTIVITY:
					changeActivity();
					break;

				case PanelEvent.DOM_UPDATED:
					if (!updating) {
						this.setActivityHtml();
					}
					break;

				case PanelEvent.DRAGSTART:
					// TODO: cache drop areas?
					//console.log("detect drag start", template)
					break;

				case PanelEvent.DRAGGING:
					{
						let $iframe = $("#ic_edit iframe"),
							box = $iframe.getRect(),
							doc = ($iframe[0] as HTMLIFrameElement).contentDocument as Document,
							hoverElement = doc.elementFromPoint(position.left - box.left, position.top - box.top) as HTMLElement;

						//console.log("detect dragging", template)
						if (hoverElement && template) {
							let best: HTMLElement;

							while (hoverElement) {
								if (hoverElement.nodeName.startsWith("IC-") && isValidWidget(template.content.firstChild as HTMLElement, hoverElement)) {
									if (ctrlKey) {
										best = hoverElement;
									} else {
										break;
									}
								}
								if (hoverElement.hasAttribute("ice")) {
									break;
								}
								hoverElement = hoverElement.parentElement;
							}
							if (ctrlKey) {
								hoverElement = best;
							}
							if (focusElement !== hoverElement) {
								focusElement = hoverElement;
								let data = template.getAttribute("data-template"),
									options = data ? data.split(/[\s,]/) : [];

								//console.log("drag over element", hoverElement);
								//if (options.includes("position")) {
								// TODO: Should this show the container it'll be inside for relative positioning?
								//} else {
								overlay.clear();
								if (hoverElement) {
									overlay.add(hoverElement);
								}
								$("#ic_edit>.ic_edit>.overlay").css("display", hoverElement ? "block" : "none");
								//}
							}
						} else {
							$("#ic_edit>.ic_edit>.overlay").css("display", "none");
						}
						break;
					}

				case PanelEvent.DROPPED:
					{
						let box = $("#ic_edit iframe").getRect();

						if (box.left <= position.left && box.right >= position.left
							&& box.top <= position.top && box.bottom >= position.top
							&& focusElement) {
							let data = template.getAttribute("data-template"),
								options = data ? data.split(/[\s,]/) : [],
								doc = ($("#ic_edit iframe")[0] as HTMLIFrameElement).contentDocument as Document,
								lastId: number = 0,
								classNames = Object.keys(rules).join(","),
								$component = $(template.content)
									.children()
									.clone(true)
									.appendTo(focusElement),
								component = $component[0],
								pElements = querySelectorAll(component, "p");

							//console.log("detect dropped", $component, focusElement, position, box)
							$("#ic_edit>.ic_edit>.overlay").css("display", "none");
							if (pElements.length) {
								querySelectorAll(doc, "p[id]").forEach(function(p: HTMLParagraphElement) {
									lastId = Math.max(lastId, p.id.regex(/p(\d+)/) as number);
								});
								pElements.forEach(function(p: HTMLParagraphElement) {
									p.id = "p" + (++lastId);
								});
							}
							$component.find(classNames).addBack(classNames).each(function(index, el) {
								el.fixState();
							});
							if ($component.siblings("[ice='" + currentActivity._id + "']").length) {
								$component.attr("ice", currentActivity._id);
							}
							if (ctrlKey || options.includes("position")) {
								let relativeBox = focusElement.getBoundingClientRect(),
									top = ((position.top - box.top - relativeBox.top) * 100 / relativeBox.height).round(3),
									left = ((position.left - box.left - relativeBox.left) * 100 / relativeBox.width).round(3);

								if (!$component.is(classNames)) {
									$component = $("<ic-box></ic-box>").insertBefore($component).append($component);
								}
								$component
									.removeAttr(REMOVETEMPLATES.join(" "))
									.css({
										position: "absolute",
										top: top + "%",
										left: left + "%",
										width: component.style.width || (Math.min(100 - left, 25) + "%"),
										height: component.style.height || (Math.min(100 - top, 25) + "%")
									})
									.find(REMOVETEMPLATES.wrap("],[", "[", "]"))
									.removeAttr(REMOVETEMPLATES.join(" "));
							}
							if ($component.find("iframe").length) {
								$component.find("iframe").each(function(i, el: HTMLIFrameElement) {
									el.src = (prompt("Please enter URL for the iFrame", el.src || "") || "").replace("https://youtu.be/", "https://www.youtube.com/embed/");
								});
							}
							if (($component.is("img") || $component.find("img").length) && !template.classList.contains("resource")) {
								currentImage = ($component.is("img") ? $component : $component.find("img"))[0] as HTMLImageElement;
								openImageBank();
							}
							updateHtml();
							currentEditWidget = $component[0].closestWidget();
							querySelectorAll(".ice-select").forEach(function(element: HTMLElement) {
								if (element !== currentEditWidget) {
									element.classList.remove("ice-select");
								}
							});
							currentEditWidget.classList.add("ice-select");
							overlay.clear();
							overlay.add(currentEditWidget);
							return true;
						}
						break;
					}
			}
		};

		setHtml() {
			let dom = currentActivity.dom,
				$root = $("#ic_edit iframe").contents().find("[ice='" + currentActivity._id + "']");

			while (dom.hasChildNodes()) {
				dom.removeChild(dom.firstChild);
			}
			if ($root.length) {
				$root.each(function(i, el) {
					dom.appendChild(el.cloneNode(true));
				});
			}
			currentActivity.formatHtml();
			updating = true;
			setHtml(currentActivity.html);
			updating = false;
		}

		setActivityHtml() {
			let iframe = createElement("iframe") as HTMLIFrameElement;
			this.clearWizards();

			overlay.clear();
			iframe.onload = function() {
				let doc = (this as HTMLIFrameElement).contentDocument;

				function fixScript(el: HTMLScriptElement) {
					let script = createElement("script");

					[].forEach.call(el.attributes, function(attribute: Attr) {
						script.setAttribute(attribute.name, attribute.value);
					});
					script.text = (el as HTMLScriptElement).text;
					(script as any).icReplaced = true;
					return script;
				}

				Editor.currentActivity.build().then(function(dom) {
					let head = doc.head,
						body = doc.body,
						inHead = true,
						link = createElement("link"),
						script = createElement("script");
					//	div = doc.createElement("div");

					head.parentElement.setAttribute("data-uid", String(currentActivity._id).crc32());
					[].forEach.call(dom.childNodes, function(el: HTMLElement) {
						if (inHead && !["BASE", "LINK", "META", "NOSCRIPT", "SCRIPT", "STYLE", "TITLE"].includes(el.nodeName)) {
							inHead = false;
						}
						if (el.nodeName === "SCRIPT") {
							el = fixScript(el as HTMLScriptElement);
						} else {
							el = el.cloneNode(true) as HTMLElement;
						}
						(inHead ? head : body).appendChild(el);
					});
					link.rel = "stylesheet";
					link.type = "text/css";
					link.href = "include/preview.css";
					head.appendChild(link);
					script.type = "text/javascript";
					script.src = "bower_components/tinymce/tinymce.min.js";
					script.defer = true;
					head.appendChild(script);
					script = createElement("script");
					script.text = "var ic_menu,ic_start_widget,ic_edit=" + inject.ic_edit.toString() + ";" + inject.ic_action.toString() + ";" + inject.ic_context.toString() + ";(" + inject.ic_startup.toString() + ")('" + currentActivity._id + "')";
					head.appendChild(script);
					// TODO: toolbar default style
					//						div.id = "tinymce_toolbar";
					//						body.appendChild(div);
					//					[].forEach.call(doc.querySelectorAll("script[src]"), function(el: HTMLScriptElement) {
					//						let script = doc.createElement("script");
					//
					//						for (let i = 0; i < el.attributes.length; i++) {
					//							let attribute = el.attributes[i];
					//
					//							script.setAttribute(attribute.name, attribute.value);
					//						}
					//						el.parentElement.replaceChild(script, el);
					//					});
					querySelectorAll(doc, "input").forEach(function(el: HTMLInputElement) {
						el.readOnly = true;
					});
					body.addEventListener("mousedown", stopPropagation, true);
					body.addEventListener("mouseup", stopPropagation, true);
					body.addEventListener("click", stopPropagation, true);
				});
			};
			iframe.sandbox.add("allow-same-origin", "allow-scripts");
			(iframe as any).srcdoc = "<!DOCTYPE html>\n<html><head></head><body></body></html>";
			iframe.src = "blank.html";
			$("#ic_edit>div>iframe").replaceWith(iframe);
			EditPanel.instance.updateWatchers();
		};

		updateWatchers() {
			let activity = Editor.currentActivity;

			let $gravatars = $("#ic_edit").parent().siblings(".w2ui-tabs").find("td:last-child").empty();
			if (activity.watchers.length) {
				activity.watchers.forEach(function(email, index) {
					$gravatars.append("<img title=\"" + email + "\" alt=\"\" src=\"https://www.gravatar.com/avatar/" + email.trim().toLowerCase().md5() + ".jpg?s=20&d=identicon\" style=\"right:" + (22 * index + 2) + "px\">");
				});
			}
		}
	}
};
