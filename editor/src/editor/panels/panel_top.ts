///<reference path="panel.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace Editor {
	export class TopPanel extends Panel {
		static readonly instance: TopPanel;

		private constructor() {
			super("header", PERM.NONE, "top");

			if (TopPanel.instance) {
				throw new Error("Error: Instantiation failed: Use TopPanel.instance instead of new.");
			}
			Object.defineProperty(TopPanel, "instance", {
				value: this
			});
		}

		on(event: PanelEvent, args?: any) {
			switch (event) {
			}
		};
	}
};
