/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace Editor {
	export const enum PanelEvent {
		STARTUP,
		LOADED,
		ACTIVITY,
		DOM_UPDATED,
		SELECT_WIDGET,
		DRAGSTART,
		DRAGGING,
		DROPPED
	}

	function getPanelSide(name: string, def: string): string {
		var panels = getSetting("panel", {});

		Object.keys(panels).forEach(function(side: "left" | "main" | "preview" | "right") {
			if (Array.isArray(panels[side]) && panels[side].includes(name)) {
				def = side;
			}
		});
		if (panels[def] && !panels[def].includes(name)) {
			panels[def].push(name);
		}
		return def;
	}

	/**
	 * Definition of a panel, extended at need
	 */
	export abstract class Panel {
		static panels: Panel[] = [];
		static active: Panel;

		public $: JQuery;
		public el: HTMLElement;

		constructor(public name: string, public perm: PERM, public side: string) {
			if (perm && !Editor.permissions.has(perm)) {
				throw "No permission";
			}
			Panel.panels.pushOnce(this);
			if (["left", "main", "preview", "right"].includes(side)) {
				this.side = side = getPanelSide(name, side);
			}
			//console.log("Appending", this.$, "to", side, Editor.panels[side])
			var template = getElementById("template_" + name) as HTMLTemplateElement;

			// TODO: Make sure we're inserting only a single div node?
			Editor.panels[side].appendChild(template.content);
			this.$ = $(this.el = Editor.panels[side].lastElementChild as HTMLElement);
			template.parentNode.removeChild(template); // Cleanup after ourselves
		}

		abstract on(event: PanelEvent, args?: any): void;

		startup() {};

		onClick(event: MouseEvent) {
			var panel = this as Panel;

			panel.$.show().siblings().hide();
			ic.setImmediate("saveSettings", saveSettings);
		};

		static startup() {
			["left", "main", "preview", "right"].forEach(function(side) {
				var tabs = w2ui["layout_" + side + "_tabs"] as W2UI.W2Tabs,
					active: string = settings.active[side];

				if (!Panel.find(active)) {
					active = null;
				}
				((settings.panel[side] || []) as string[]).forEach(function(name) {
					var panel = Panel.find(name);

					if (panel) {
						active = active || name;
						tabs.add({
							id: name,
							caption: name.ucfirst(),
							onClick: panel.onClick.bind(panel)
						});
					}
				});
				tabs.select(active);
				var activePanel = Panel.find(active)
				if (activePanel) {
					activePanel.$.show().siblings().hide();
				}
			});
			Panel.panels.forEach(function(panel) {
				panel.startup();
			});
			var layout = (w2ui["layout"] as W2UI.W2Layout);

			if (Editor.permissions.has(PERM.ROOT | PERM.NODE_THEME)) {
				layout.set("preview", {hidden: false});
				layout.set("right", {hidden: false});
			}
			layout.refresh();
		}

		static forEach(callback: (panel: Panel, i?: number, list?: Panel[]) => void) {
			this.panels.forEach(callback);
		}

		static trigger(event: PanelEvent, ...data: any[]) {
			var args = arguments,
				result = undefined;

			Panel.panels.forEach(function(panel) {
//				try {
					result = panel.on.apply(panel, args) || result;
//				} catch (error) {
//					console.error(error);
//				}
			});
			return result;
		}

		static triggerDomUpdated() {
			Panel.trigger(PanelEvent.DOM_UPDATED);
		}

		static triggerActivity() {
			Panel.trigger(PanelEvent.ACTIVITY);
		}

		static find(search: string): Panel {
			var panel: Panel;

			for (var i = 0; i < this.panels.length; i++) {
				panel = this.panels[i];
				if (panel.name === search) {
					return panel;
				}
			}
			return null;
		}
	};

	$(window).one("resize", function() {
		$("body").css("opacity", 1);
	});

	$(window).on({
		resize: function() {
			$("#edit_frame,#size_frame").hide();
			//		},
			//		mousedown: function(event: MouseEvent) {
		}
	});
};
