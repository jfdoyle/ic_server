///<reference path="../../button.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

new Button("#ic_button_user_group_edit", PERM.ROOT | PERM.OWNER,
	function() {
		var id = Editor.UsersPanel.instance.currentGroup,
			usersPanel = Editor.UsersPanel.instance,
			oldName = usersPanel.groups[usersPanel.currentGroup].name || "",
			name = prompt("Please enter a new group name, empty to cancel", oldName);

		if (name && name !== oldName) {
			Editor.ajax({
				type: "POST",
				url: "ajax.php",
				global: false,
				dataType: "json",
				data: {
					action: "set_group_name",
					id: id,
					name: name
				},
				success: function(data) {
					Editor.UsersPanel.instance.loadGroups();
				}
			});
		}

	},
	function(activity) {
		var panel = Editor.UsersPanel.instance;

		return panel && panel.currentGroup && panel.isGroup;
	}
);
