///<reference path="../../button.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

new Button("#ic_button_user_group_add", PERM.ROOT | PERM.OWNER,
	function() {
		var node = parseInt($.tree.currentId),
			name = prompt("Please enter a new group name, empty to cancel", ICNodes.get(node)._name || "");

		if (name) {
			Editor.ajax({
				type: "POST",
				url: "ajax.php",
				global: false,
				dataType: "json",
				data: {
					action: "create_group",
					node: String(node),
					name: name
				},
				success: function(data) {
					Editor.UsersPanel.instance.loadGroups();
				}
			});
		}

	},
	function(activity) {
		return Editor.UsersPanel.instance && activity && (activity.is(ICNodeType.Organisation) || activity.is(ICNodeType.Project));
	}
);
