///<reference path="../../button.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

new Button("#ic_button_user_add", PERM.ROOT | PERM.OWNER,
	function() {
		var panel = Editor.UsersPanel.instance;

		panel.editUser(-1);
	},
	function(activity) {
		var panel = Editor.UsersPanel.instance;

		return panel && panel.currentId && panel.isGroup === true;
	}
);
