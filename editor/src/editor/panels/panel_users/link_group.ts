///<reference path="../../button.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

new Button("#ic_button_user_group_link", PERM.ROOT | PERM.OWNER,
	function() {
		var usersPanel = Editor.UsersPanel.instance,
			id = usersPanel.currentGroup,
			node = parseInt($.tree.currentId);

		if (node && usersPanel.groups[id].node !== node && confirm("Are you sure you wish to change the linked node to #" + node + " (" + ICNodes.get(node)._name + ")?")) {
			Editor.ajax({
				type: "POST",
				url: "ajax.php",
				global: false,
				dataType: "json",
				data: {
					action: "set_group_node",
					id: id,
					node: node
				},
				success: function(data) {
					Editor.UsersPanel.instance.loadGroups();
				}
			});
		}
	},
	function(activity) {
		var panel = Editor.UsersPanel.instance;

		return panel && panel.currentGroup && panel.isGroup && activity && (activity.is(ICNodeType.Organisation) || activity.is(ICNodeType.Project));
	}
);
