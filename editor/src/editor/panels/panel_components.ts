///<reference path="panel.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface HTMLTemplateElement {
	icNodeChildCount?: number;
}

namespace Editor {
	function haveSameAttributes(this: HTMLElement, attribute: Attr): boolean {
		let name = attribute.name.toLowerCase();

		if (name.startsWith("ic-")) {
			return attribute.value === this.getAttribute(name);
		}
		if (name === "style") {
			return attribute.value.split(";").every((style: string) => {
				if (!style || style.indexOf(":") < 0) {
					return true;
				}
				let [all, key, value] = style.match(/^\s*(.*?)\s*:\s*(.*)\s*$/);

				return this.style.hasOwnProperty(key) && this.style.getPropertyValue(key) == value;
			});
		}
		// TODO: compare template css with target, some keys need to be present, others need the same value
		return true;
	}

	export const REMOVETEMPLATES = ["data-template", "data-optional", "data-repeat"];

	function isSameTemplate(element: HTMLElement, template: HTMLElement): boolean {
		var repeating = false;

		if (element && element.nodeType !== Node.ELEMENT_NODE) {
			element = element.nextElementSibling as HTMLElement;
		}
		if (template && template.nodeType !== Node.ELEMENT_NODE) {
			template = template.nextElementSibling as HTMLElement;
		}
		while (element && template) {
			if (element.nodeName !== template.nodeName
				|| !Array.prototype.every.call(template.attributes, haveSameAttributes, element)
				|| template.firstElementChild && !isSameTemplate(element.firstElementChild as HTMLElement, template.firstElementChild as HTMLElement)) {
				if (template.hasAttribute("data-optional")) {
					template = template.nextElementSibling as HTMLElement;
					continue;
				} else if (repeating) {
					repeating = false;
					template = template.nextElementSibling as HTMLElement;
					if (!element) {
						break;
					}
					continue;
				} else {
					//					console.groupCollapsed(String((element && !template) || false))
					//					console.log("element", element)
					//					console.log("element attr", element.getAttribute(element.nodeName))
					//					console.log("element children", element && element.children)
					//					console.log("template", template)
					//					console.log("template attr", template.getAttribute(template.nodeName))
					//					console.log("template children", template && template.children)
					//					console.groupEnd()
					return (element && !template) || false;
				}
			}
			element = element.nextElementSibling as HTMLElement;
			if (element && template.hasAttribute("data-repeat")) {
				repeating = true;
			} else {
				template = template.nextElementSibling as HTMLElement;
				while (!element && template && template.hasAttribute("data-optional")) {
					template = template.nextElementSibling as HTMLElement;
				}
			}
		}
		// If the template and element run out at the same time then they match
		//		console.log("isSameTemplate2", element, template, template ? element === template : !element)
		return !template;
	}


	function countNodes(node: Element) {
		let count = 0;

		while (node) {
			count += 1 + countNodes(node.firstElementChild) + node.attributes.length;
			node = node.nextElementSibling;
		}
		return count;
	}

	export function findTemplate(element: HTMLElement): HTMLTemplateElement {
		var best: HTMLTemplateElement,
			bestLength: number;

		components
			.filter(template => template.content.firstElementChild && isSameTemplate(element, template.content.firstElementChild as HTMLElement))
			//				{
			//				console.groupCollapsed("isSameTemplate: " + element.nodeName + ", " + template.content.firstElementChild.nodeName)
			//				console.log("isSameTemplate", element, template.content.firstElementChild)
			//				var result = isSameTemplate(element, template.content.firstElementChild as HTMLElement)
			//				console.groupEnd()
			//				console.log(result)
			//				return result;
			//			})
			.forEach(template => {
				var length = template.icNodeChildCount; // template.innerHTML.length - though doesn't capture complexity

				if (!length) {
					template.icNodeChildCount = length = countNodes(template.content.firstElementChild);
				}
				console.log("match", element, template)
				if (!best || length > bestLength) {
					best = template;
					bestLength = length;
				}
			});
		return best;
	}

	var components: HTMLTemplateElement[];
	var lastActivity: ICNode;

	export class ComponentsPanel extends Panel {
		static readonly instance: ComponentsPanel;

		private constructor() {
			super("components", PERM.ROOT, "left");

			if (ComponentsPanel.instance) {
				throw new Error("Error: Instantiation failed: Use ComponentsPanel.instance instead of new.");
			}
			Object.defineProperty(ComponentsPanel, "instance", {
				value: this
			});

		}

		on(event: PanelEvent, args?: any) {
			switch (event) {
				// Called from Edit Panel
				//				case PanelEvent.ACTIVITY:
				//					changeActivity();
				//					break;
			}
		};
	}

	function addTemplate(template: HTMLTemplateElement) {
		var icon = $(template).data("icon");

		if (!components.find(node => node.title === template.title) && icon) {
			$.make("article", "component",
				$.make("div", icon ? "background-image:url(" + icon + ")" : template.content.cloneNode(true)),
				$.make("h3", "=" + template.title))
				.data("template", template)
				.appendTo("#ic_components")
				.draggable({
					appendTo: "body",
					containment: "window",
					cursor: "grabbing",
					helper: "clone",
					opacity: 0.7,
					revert: true,
					revertDuration: 200,
					scroll: false,
					zIndex: 100,
					start: function(event: any, ui: JQueryUI.DraggableEventUIParams) {
						$("iframe").css("pointer-events", "none");
						Panel.trigger(PanelEvent.DRAGSTART, $(this).data("template"));
					},
					drag: function(event: any, ui: JQueryUI.DraggableEventUIParams) {
						Panel.trigger(PanelEvent.DRAGGING, $(this).data("template"), {top: event.clientY, left: event.clientX});
					},
					stop: function(event: any, ui: JQueryUI.DraggableEventUIParams) {
						$("iframe").css("pointer-events", "");
						//console.log($(this).data("uiDraggable").originalPosition)
						Panel.trigger(PanelEvent.DROPPED, $(this).data("template"), {top: event.clientY, left: event.clientX});
					}
				});
		}
		components.push(template);
	}

	export function changeActivity() {
		var activity = Editor.currentActivity;

		if (activity && lastActivity !== activity) {
			lastActivity = activity;
			Editor.currentActivity.build().then((dom) => {
				$("script[src^='activity/']", dom).each(function(index: number, scriptNode: HTMLScriptElement) {
					var activityPath = (scriptNode.getAttribute("src") || "").regex(/^(activity\/(?:build|version\/latest|version\/v[\d\.]+)\/)activity\.(?:min\.)?js$/) as string;

					if (activityPath && Editor.currentRulesPath !== activityPath) {
						//console.log("Load rules for", activityPath)
						fetch(activityPath + "rules.json")
							.then(function(response) {
								if (response.status >= 200 && response.status < 300) {
									return response;
								} else {
									var error = new Error(response.statusText);
									//error.response = response;
									throw error
								}
							})
							.then(function(response) {
								return response.json();
							})
							.then(function(data: any) {
								Editor.currentRulesPath = activityPath;
								Editor.rules = data as Rules;
							});
					}
				});
				var componentNodes: number[] = [3], // TODO: Don't hard-code the main template
					panel = getElementById("ic_components");

				$("link[rel='stylesheet'][href^='../assets/']", dom).each((index: number, linkNode: HTMLLinkElement) => {
					var activityId = (linkNode.getAttribute("href") || "").regex(/^\.\.\/assets\/(\d+)\/theme\.css$/) as number;

					if (activityId) {
						componentNodes.unshiftOnce(activityId);
					}
				});
				while (panel && panel.firstChild) {
					panel.removeChild(panel.firstChild);
				};
				components = [];
				//<link rel="stylesheet" type="text/css" href="activity/build/activity.css"/>
				componentNodes.forEach((activityId: number) => {
					//console.log("Load components for", activityId, ICNodes.get(activityId))
					ICNodes.load(activityId).then(() => {
						var node = ICNodes.get(activityId);

						querySelectorAll(node.dom, "template").forEach((template: HTMLTemplateElement) => addTemplate(template));
					});
				})
			});
		}
	}
};
