///<reference path="panel.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace Editor {
	interface ICGroup {
		id: number;
		name: string;
		node: number;
		loaded?: boolean;
	}

	interface ICUser {
		id: number;
		email: string;
		permission: number;
		group: number;
		seen: string;
		update: string;
	}

	export class UsersPanel extends Panel {
		static readonly instance: UsersPanel;

		groups: ICGroup[];
		users: ICUser[];
		popup: any;

		isGroup: boolean;
		currentGroup: number;
		currentId: number;

		private constructor() {
			super("users", PERM.ROOT | PERM.OWNER, "left");

			if (UsersPanel.instance) {
				throw new Error("Error: Instantiation failed: Use UsersPanel.instance instead of new.");
			}
			Object.defineProperty(UsersPanel, "instance", {
				value: this
			});

			if (!(w2ui as any).user) {
				$().w2form({
					name: "user",
					style: "border:0px;background-color:transparent;",
					formHTML:
					'<div class="w2ui-page page-0">' +
					'    <div class="w2ui-field">' +
					'        <label>#ID:</label>' +
					'        <div>' +
					'           <input name="user_id" type="text" maxlength="10" style="width:300px;" disabled/>' +
					'        </div>' +
					'    </div>' +
					'    <div class="w2ui-field w2ui-required user-email">' +
					'        <label>Email:</label>' +
					'        <div>' +
					'           <input name="email" type="email" maxlength="512" style="width:300px;"/>' +
					'        </div>' +
					'    </div>' +
					'    <div class="w2ui-field user-password">' +
					'        <label>Set Password:</label>' +
					'        <div>' +
					'           <input name="reset" type="text" maxlength="512" style="width:300px;"/>' +
					'        </div>' +
					'    </div>' +
					'    <hr/>' +
					'    <div class="w2ui-field">' +
					'        <label>Admin User:</label>' +
					'        <div>' +
					'            <label><input name="flag_admin" type="checkbox"/>' +
					'            Manage everything</label>' +
					'        </div>' +
					'    </div>' +
					'    <hr/>' +
					'    <div class="w2ui-field">' +
					'        <label>Owner:</label>' +
					'        <div>' +
					'            <label><input name="flag_owner" type="checkbox"/>' +
					'            Manage users</label>' +
					'        </div>' +
					'    </div>' +
					'    <div class="w2ui-field">' +
					'        <label>Manager:</label>' +
					'        <div>' +
					'            <label><input name="flag_manager" type="checkbox"/>' +
					'            Manage projects</label>' +
					'        </div>' +
					'    </div>' +
					'    <hr/>' +
					'    <div class="w2ui-field">' +
					'        <label>View:</label>' +
					'        <div>' +
					'            <label><input name="flag_view" type="checkbox"/>' +
					'            View all screens</label>' +
					'        </div>' +
					'    </div>' +
					'    <div class="w2ui-field">' +
					'        <label>Create:</label>' +
					'        <div>' +
					'            <label><input name="flag_create" type="checkbox"/>' +
					'            Create screens</label>' +
					'        </div>' +
					'    </div>' +
					'    <div class="w2ui-field">' +
					'        <label>Delete:</label>' +
					'        <div>' +
					'            <label><input name="flag_delete" type="checkbox"/>' +
					'            Delete screens</label>' +
					'        </div>' +
					'    </div>' +
					'    <hr/>' +
					'    <div class="w2ui-field">' +
					'        <label>Content:</label>' +
					'        <div>' +
					'            <label><input name="flag_content" type="checkbox"/>' +
					'            Edit screen content</label>' +
					'        </div>' +
					'    </div>' +
					'    <div class="w2ui-field">' +
					'        <label>Theme:</label>' +
					'        <div>' +
					'            <label><input name="flag_theme" type="checkbox"/>' +
					'            Edit screen look</label>' +
					'        </div>' +
					'    </div>' +
					'    <div class="w2ui-field">' +
					'        <label>Files:</label>' +
					'        <div>' +
					'            <label><input name="flag_files" type="checkbox"/>' +
					'            Edit and upload files</label>' +
					'        </div>' +
					'    </div>' +
					'</div>' +
					'<div class="w2ui-buttons">' +
					'    <button class="btn" name="save">Save</button>' +
					'</div>',
					fields: [
						{
							field: "user_id",
							type: "text"
						},
						{
							field: "email",
							type: "email",
							required: true
						},
						{
							field: "reset",
							type: "text"
						},
						{
							field: "flag_admin",
							type: "checkbox"
						},
						{
							field: "flag_owner",
							type: "checkbox"
						},
						{
							field: "flag_manager",
							type: "checkbox"
						},
						{
							field: "flag_view",
							type: "checkbox"
						},
						{
							field: "flag_create",
							type: "checkbox"
						},
						{
							field: "flag_delete",
							type: "checkbox"
						},
						{
							field: "flag_content",
							type: "checkbox"
						},
						{
							field: "flag_theme",
							type: "checkbox"
						},
						{
							field: "flag_files",
							type: "checkbox"
						}
					],
					actions: {
						"save": this.saveUser.bind(this)
					}
				});
			}
		}

		on(event: PanelEvent, args?: any) {
			switch (event) {
				case PanelEvent.LOADED:
					this.onLoaded();
					break;
			}
		};

		loadGroups() {
			Editor.ajax({
				type: "POST",
				url: "ajax.php",
				global: false,
				dataType: "json",
				data: {
					action: "get_group",
					id: -1,
				},
				success: (data: ICGroup[]) => {
					console.log("Groups: ", data);
					if (!this.groups) {
						((w2ui as any).users as W2UI.W2Sidebar).remove("load");
						this.groups = [];
					}
					this.groups.forEach((group) => {
						if (group) {
							((w2ui as any).users as W2UI.W2Sidebar).remove("group-" + group.id);
						}
					});
					data.sort(function(a, b) {
						var a_lower = a.name.toLowerCase(),
							b_lower = b.name.toLowerCase();

						return a_lower > b_lower ? 1 : a_lower < b_lower ? -1 : 0;
					}).forEach((group) => {
						this.groups[group.id] = group;
						((w2ui as any).users as W2UI.W2Sidebar).add([
							{ id: "group-" + group.id, text: group.name, icon: "fa fa-users", count: "#" + group.node }
						]);
					});
				}
			});
		}

		loadUsers(id: number) {
			Editor.ajax({
				type: "POST",
				url: "ajax.php",
				global: false,
				dataType: "json",
				data: {
					action: "get_users",
					id: id,
				},
				success: (data: ICUser[]) => {
					console.log("Users: ", data);
					this.users = this.users || [];
					this.users.forEach((user, index) => {
						if (user && user.group === id) {
							((w2ui as any).users as W2UI.W2Sidebar).remove("user-" + user.id);
						}
					});
					data.sort(function(a, b) {
						var a_lower = a.email.toLowerCase(),
							b_lower = b.email.toLowerCase();

						return a_lower > b_lower ? 1 : a_lower < b_lower ? -1 : 0;
					}).forEach((user) => {
						this.users[user.id] = user;
						user.group = this.currentGroup;
						((w2ui as any).users as W2UI.W2Sidebar).insert("group-" + id, null, [
							{ id: "user-" + user.id, text: user.email === username ? "<em>" + user.email + "</em>" : user.email, icon: "fa fa-user" }
						]);
						((w2ui as any).users as W2UI.W2Sidebar).expand("group-" + id);
					});
				}
			});
		}

		editUser(id: number) {
			var user = id >= 0 ? this.users[id] : null,
				permission = new Permission(user ? user.permission : 0);

			this.currentId = id;
			this.popup = $().w2popup({
				title: "<i class=\"fa fa-user\"></i> " + (user ? "Edit" : "Create") + " User",
				body: "<div id=\"user\" style=\"width:100%;height:100%;\"></div>",
				style: "padding: 15px 0px 0px 0px",
				width: 500,
				height: 490,
				onToggle: function(event) {
					$((w2ui as any).user.box).hide();
					event.onComplete = function() {
						$((w2ui as any).user.box).show();
						(w2ui as any).user.resize();
					}
				},
				onOpen: function(event) {
					(w2ui as any).user.clear();
					(w2ui as any).user.record = {
						user_id: String(id),
						email: user ? user.email : "",
						flag_admin: permission.isRoot,
						flag_owner: permission.isOwner,
						flag_manager: permission.isManager,
						flag_view: permission.canView,
						flag_create: permission.canCreate,
						flag_delete: permission.canDelete,
						flag_content: permission.canContent,
						flag_theme: permission.canTheme,
						flag_files: permission.canFile
					};
					(w2ui as any).user.refresh();
					event.onComplete = function() {
						($("#user") as any).w2render("user");
						$(".user-email input").prop("disabled", id >= 0 ? true : false);
						if (!Editor.permissions.isRoot) {
							$(".user-password").hide();
						}
					}
				}
			});
		}

		saveUser() {
			var record = (w2ui as any).user.record,
				user = record.user_id > 0 ? this.users[record.user_id] : null,
				old_permission = new Permission(user ? user.permission : 0),
				new_permission = new Permission(0);

			new_permission.isRoot = record.flag_admin;
			new_permission.isOwner = record.flag_owner;
			new_permission.isManager = record.flag_manager;
			new_permission.canView = record.flag_view;
			new_permission.canCreate = record.flag_create;
			new_permission.canDelete = record.flag_delete;
			new_permission.canContent = record.flag_content;
			new_permission.canTheme = record.flag_theme;
			new_permission.canFile = record.flag_files;
			console.log("record", record, old_permission.raw, new_permission.raw)
			if (user) {
				if (!old_permission.is(new_permission)) {
					console.log("Permissions have changed");
					Editor.ajax({
						type: "POST",
						url: "ajax.php",
						global: false,
						dataType: "json",
						data: {
							action: "set_permissions",
							id: record.user_id,
							permissions: new_permission.raw
						},
						success: (data) => {
							this.loadUsers(this.currentGroup);
						}
					});
				}
				if (record.email !== user.email) {
					console.log("Email has changed");
					alert("Email change not implemented");
				}
				if (Editor.permissions.isRoot && record.reset) {
					console.log("Password has changed");
					Editor.ajax({
						type: "POST",
						url: "ajax.php",
						global: false,
						dataType: "json",
						data: {
							action: "change_password",
							id: record.user_id,
							oldPassword: "",
							newPassword: record.reset
						},
						success: (data) => {
							this.loadUsers(this.currentGroup);
						}
					});
				}
			} else {
				console.log("Create a new user");
				Editor.ajax({
					type: "POST",
					url: "ajax.php",
					global: false,
					dataType: "json",
					data: {
						action: "create_user",
						group: this.currentGroup,
						email: record.email,
						permissions: new_permission.raw,
						password: record.reset
					},
					success: (data) => {
						this.loadUsers(this.currentGroup);
					}
				});
			}
			this.popup.close();
		}

		onLoaded() {
			if (permissions.isRoot) {
				$("#ic_users>div.ic_users").w2sidebar({
					name: "users",
					nodes: [{ id: "load", text: "Load...", icon: "fa fa-cloud-download" }]
				});
				((w2ui as any).users as W2UI.W2Sidebar).on("*", (event) => {
					//console.log("Event: " + event.type + " Target: " + event.target);
					if (event.type === "click") {
						var eventId = event.target as string,
							isGroup = this.isGroup = /^group-/.test(eventId),
							id = this.currentId = eventId.regex(/-([0-9]+)$/) as number;

						if (eventId === "load") {
							this.loadGroups();
						} else if (isGroup) {
							this.currentGroup = id;
							if (!this.groups[id].loaded) {
								this.groups[id].loaded = true;
								this.loadUsers(id)
							}
						}
						Button.update();
					}
				});
			} else {
				$("#tabs_layout_" + this.side + "_tabs_tab_users,#ic_users").hide();
			}
		}
	}
};
