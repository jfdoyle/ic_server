///<reference path="panel.ts" />
///<reference path="../../node/node.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace Editor {
	export class ExplorePanel extends Panel {
		static readonly instance: ExplorePanel;

		private constructor() {
			super("explore", PERM.ROOT, "preview");

			if (ExplorePanel.instance) {
				throw new Error("Error: Instantiation failed: Use ExplorePanel.instance instead of new.");
			}
			Object.defineProperty(ExplorePanel, "instance", {
				value: this
			});
		}

		on(event: PanelEvent, args?: any) {
			switch (event) {
				case PanelEvent.ACTIVITY:
					this.onExplore();
					break;
			}
		};

		centerView() {
			var parent = $("#ic_view").getRect(),
				$img = $("#ic_view img,#ic_view svg"),
				rect = $img.getRect();
			$img.css("top", Math.max(0, (parent.height - rect.height) / 2));
		}

		clickFile = (event: JQuery.Event) => {
			var text, basefile, basepath,
				$this = $(event.target),
				activity = Editor.currentActivity,
				files = activity._files,
				$tr = $this.closest("tr"),
				mode = $tr.data("text") as string,
				view = $tr.data("view") as string,
				filename = $tr.data("file") as string,
				force = "?_=" + Date.now();

			if (filename) {
				var filepath = Config.server + "assets/" + activity._id + "/" + encodeURIComponent(filename);

				$tr.addClass("selected").siblings().removeClass("selected");
				$("#ic_mode_explore_download").attr("href", filepath);
				$("#ic_view,#ic_file").css("width", view && mode ? "50%" : "");
				$("#ic_view").toggle(!!view);
				$("#ic_file").toggle(!!mode);
				if (view) {
					switch (view) {
						case "image":
							$("#ic_view").html("<img src=\"" + filepath + force + "\" alt=\"" + filename + "\" title=\"" + filename + "\"/>");
							var $img = $("#ic_view img");
							if (($img[0] as HTMLImageElement).complete) {
								this.centerView();
							} else {
								$img.one("load", this.centerView);
							}
							break;
						case "font":
							text = "ic_view_" + activity._id + "_" + filename.replace(/\.[^\.]+$/, "");
							basefile = filename.replace(/[^\.]+$/i, "");
							basepath = filepath.replace(/[^\.]+$/i, "");
							$.stylesheet("@font-face /*" + text + "*/", [
								"font-family:'" + text + "'",
								"src:"
								+ (files.includes(basefile + "woff2") ? "url('" + basepath + "woff2" + force + "') format('woff2')," : "") /* Modern Browsers */
								+ (files.includes(basefile + "woff") ? "url('" + basepath + "woff" + force + "') format('woff')," : "") /* Modern Browsers */
								+ (files.includes(basefile + "ttf") ? "url('" + basepath + "ttf" + force + "') format('truetype')" : "") /* Safari, Android, iOS */
							]);
							text = "<div style=\"font-family:'" + text + "';\" contenteditable=\"true\">Grumpy wizards make toxic brew for the evil Queen and Jack.</div>";
							$("#ic_view").html(text + text + text + text).children().on("keyup paste", function() {
								var $this = $(this);
								$this.siblings().text($this.text());
							});
							break;
						case "audio":
							$("#ic_view").html("<audio controls preload><source src=\"" + filepath.replace(/\.ogg$/i, ".mp3") + force + "\" type=\"audio/mpeg\"/><source src=\"" + filepath.replace(/\.mp3$/i, ".ogg") + force + "\" type=\"audio/ogg\"/></audio>");
							break;
						default:
							$("#ic_view").empty();
					}
				}
				$("#ic_file").data("file", filename).data("content", "");
				if (mode) {
					$.get(filepath + force, (data) => {
						var $abaFile = $("#ic_file"),
							editor = $abaFile.data("editor");

						$abaFile.data("content", data);
						editor.getSession().setMode("ace/mode/" + mode);
						editor.setValue(data, -1);
						if (view === "svg") {
							$("#ic_view").html(data);
							this.centerView();
						}
					}, "text");
				}
				$("#ic_mode_explore_download,#ic_mode_explore_rename,#ic_mode_explore_delete").removeClass("disabled");
				$("#ic_mode_explore_save").addClass("disabled");
			}
			return false;
		}

		clickRename() {
			var file = $("#ic_file").data("file");

			Editor.dialog("Please enter the new name for '" + file + "':<br/><br/><input type=\"text\" value=\"" + file + "\"/><br/><br/>This cannot be undone!", "Confirm Rename", () => {
				var newname = $("#ic_dialog input").val() as string,
					node = Editor.currentActivity;

				if (newname && node._files.includes(newname)) {
					Editor.dialog("That name already exists!", "Rename Error", true);
				} else if (newname) {
					Editor.ajax({
						type: "POST",
						url: "ajax.php",
						dataType: "json",
						data: {
							"action": "rename_file",
							"id": node._id,
							"name": file,
							"newname": newname
						},
						success: (data) => {
							if (data.error) {
								return Editor.dialog("Error: " + data.error, "Server Error");
							}
							console.log(data);
							Editor.log("Success: File renamed");
							node._files[node._files.indexOf(file)] = newname;
							$("#ic_explore > div > table .selected")
								.data("file", newname)
								.children()
								.eq(1)
								.text(newname);
							$.tree.remove(Editor.currentActivity._id + "_" + file.replace(/^assets\/\d+\//, ""));
							$.tree.add(Editor.currentActivity._id + "_" + newname.replace(/^assets\/\d+\//, ""));
						}
					});
				}
			});
		}

		clickDelete() {
			var file = $("#ic_file").data("file");

			Editor.dialog("Are you sure you wish to delete '" + file + "'?<br/>This cannot be undone!", "Confirm Deletion", () => {
				Editor.ajax({
					type: "POST",
					url: "ajax.php",
					dataType: "json",
					data: {
						"action": "delete_file",
						"id": $.tree.currentId,
						"name": file
					},
					success: function(data) {
						if (data.error) {
							return Editor.dialog("Error: " + data.error, "Server Error");
						}
						var activity: ICNode = Editor.currentActivity;

						console.log(data);
						Editor.log("Success: File deleted");
						if (activity._files) {
							activity._files.splice(activity._files.indexOf(file), 1);
						}
						$("#ic_explore > div > table .selected").remove();
						$("#ic_view,#ic_file").hide();
						$("#ic_dialog").parent().velocity({opacity: 0}, {
							delay: 1000,
							complete: function() {
								$("#ic_dialog").dialog("close");
							}
						});
						$.tree.remove(activity._id + "_" + file.replace(/^assets\/\d+\//, ""));
					}
				});
				$("#ic_mode_explore_download,#ic_mode_explore_rename,#ic_mode_explore_delete").addClass("disabled");
			});
		}

		editTextChange() {
			var $abaFile = $("#ic_file"),
				filename = $abaFile.data("file"),
				original = $abaFile.data("content"),
				file = $("#ic_file").data("editor").getValue();


			if (original === file) {
				//console.log("unchanged")
				delete Editor.fileCache[filename];
			} else {
				//console.log("changed")
				Editor.fileCache[filename] = btoa(file.unicode());
				if (!Editor.change) {
					Editor.change = true;
					Button.update();
				}
			}
		}

		onExplore() {
			var i: number,
				filename: string,
				image: string,
				edit: string | boolean,
				view: string | boolean,
				node = Editor.currentActivity,
				id = node._id,
				files = node._files.naturalCaseSort(),
				$explorer = $("#ic_explore");

			if (!$explorer.hasClass("ui-layout-container")) {
				$("#ic_mode_explore_rename").on("click", this.clickRename);
				$("#ic_mode_explore_delete").on("click", this.clickDelete);
				$("#ic_file").on("keyup", this.editTextChange);

				var editor = ace.edit("ic_file"),
					oldTextInput = editor.onTextInput;

				editor.onTextInput = function() {
					$("#ic_mode_explore_save").removeClass("disabled");
					return oldTextInput.apply(this, arguments);
				};
				editor.setTheme("ace/theme/xcode");
				editor.getSession().setUseWrapMode(true);
				editor.$blockScrolling = Infinity;
				$("#ic_file").data("editor", editor);
			}
			$explorer.find("tbody").empty();
			$("#ic_view,#ic_file").hide();
			for (i = 0; i < files.length; i++) {
				filename = files[i];
				if (!/^assets\//.test(filename)) {
					// TODO: Remove the need for this - all filenames should have same structure
					console.error("Error: Found a bad filename:", filename);
					continue;
				}
				if (filename.regex(/assets\/(\d+)\//) !== id) {
					// Only show this asset folder
					continue;
				}
				filename = filename.regex(/assets\/\d+\/(.*)$/) as string;
				edit = false;
				view = false;
				switch (filename.regex(/\.([^\.]+)$/)) {
					case "svg":
						edit = view = "svg";
						image = "-image";
						break;
					case "png":
					case "jpg":
						view = "image";
						image = "-image";
						break;
					case "html":
						image = "-text";
						edit = "html";
						break;
					case "js":
						image = "-text";
						edit = "javascript";
						break;
					case "json":
						image = "-text";
						edit = "json";
						break;
					case "css":
					case "scss":
						image = "-text";
						edit = "scss";
						break;
					case "txt":
						image = "-text";
						edit = "text";
						break;
					case "mp3":
					case "ogg":
						view = "audio";
						image = "-audio";
						break;
					case "m4v":
						image = "-video";
						break;
					case "ttf":
					case "woff":
					case "woff2":
						view = "font";
						image = "-pdf";
						break;
					default:
						image = "";
						break;
				}
				$.make("tr",
					$.make("td",
						$.make("span", "fa fa-file" + image + "-o")),
					$.make("td", "=" + filename))
					.data("file", filename)
					.data("text", edit)
					.data("view", view)
					.on("click", this.clickFile)
					.appendTo($explorer.find("tbody"));
			}

			if (!files.length) {
				$.make("tr", $.make("th", "colspan=2", "=<i>no files</i>")).appendTo($explorer.find("tbody"));
			}
			$("#ic_mode_explore_download,#ic_mode_explore_rename,#ic_mode_explore_delete,#ic_mode_explore_save").addClass("disabled");
		}
	}
};
