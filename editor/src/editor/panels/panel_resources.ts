///<reference path="panel.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace Editor {
	let scrollTop: number,
		scrollHeight: number;

	export class ResourcesPanel extends Panel {
		static readonly instance: ResourcesPanel;

		lastActivity: ICNode;
		scrollTop: number;

		private constructor() {
			super("resources", PERM.ROOT, "right");

			if (ResourcesPanel.instance) {
				throw new Error("Error: Instantiation failed: Use ResourcesPanel.instance instead of new.");
			}
			Object.defineProperty(ResourcesPanel, "instance", {
				value: this
			});
			$("#resource_list").draggable({
				appendTo: "body",
				containment: "window",
				cursor: "grabbing",
				cursorAt: {
					top: 25,
					left: 25
				},
				handle: ".media",
				opacity: 0.7,
				scroll: false,
				zIndex: 100,
				helper: function(event: any) {
					console.log("arguments", arguments)
					var template: HTMLTemplateElement,
						$media = $(event.target.closest(".media")),
						media = $media[0].cloneNode(true) as HTMLElement,
						src = $media.data("src");

					if (media.classList.contains("image")) {
						template = $("<template class=\"resource\"><img src=\"" + src + "\" style=\"width:100%;height:auto;\"/></template>")[0] as HTMLTemplateElement;
					} else if (media.classList.contains("video")) {
						template = $("<template class=\"resource\"><ic-box><video src=\"" + src + "\"></video></ic-box></template>")[0] as HTMLTemplateElement;
					} else if (media.classList.contains("audio")) {
						template = $("<template class=\"resource\"><ic-audio><audio src=\"" + src + "\"></audio></ic-audio></template>")[0] as HTMLTemplateElement;
					}
					console.log("template", template)
					$(this).data("template", template);
					media.classList.add("component");
					media.firstElementChild.className = "";
					return media;
				},
				start: function(event: any, ui: JQueryUI.DraggableEventUIParams) {
					$("iframe").css("pointer-events", "none");
					Panel.trigger(PanelEvent.DRAGSTART, $(this).data("template"));
				},
				drag: function(event: any, ui: JQueryUI.DraggableEventUIParams) {
					Panel.trigger(PanelEvent.DRAGGING, $(this).data("template"), {top: event.clientY, left: event.clientX});
				},
				stop: function(event: any, ui: JQueryUI.DraggableEventUIParams) {
					$("iframe").css("pointer-events", "");
					//console.log($(this).data("uiDraggable").originalPosition)
					Panel.trigger(PanelEvent.DROPPED, $(this).data("template"), {top: event.clientY, left: event.clientX});
				}
			});
			rAF(this.checkScroll);
		}

		on(event: PanelEvent, args?: any) {
			switch (event) {
				case PanelEvent.ACTIVITY:
					this.changeActivity();
					break;
			}
		};

		checkScroll = () => {
			//var el = querySelector(".resources>div");
			var el = getElementById("resource_list");

			if (el && (scrollTop !== el.scrollTop || scrollHeight !== el.scrollHeight)) {
				scrollTop = el.scrollTop;
				scrollHeight = el.scrollHeight;
				var container = el.getBoundingClientRect(),
					top = container.top - 25,
					bottom = container.bottom + 25;

				[].forEach.call(el.children, function(el: HTMLElement) {
					var box = el.getBoundingClientRect();

					el.classList.toggle("defer", !(box.bottom >= top && box.top <= bottom));
				});
			}
			rAF(this.checkScroll);
		}

		sort() {
			var $list = $("#resource_list"),
				sort = Editor.getSetting("options.resource_sort"),
				parents = sort === "tree" ? currentActivity.getParentsId(true) : null,
				siblings = sort === "tree" ? currentActivity.getSiblingsId().reverse() : null;

			$list.children().sort(function(a: HTMLElement, b: HTMLElement) {
				var atext = a.lastElementChild.textContent,
					btext = b.lastElementChild.textContent;

				switch (sort) {
					case "tree":
						// This screen first, before siblings, then parents in order
						var anumber = parseInt(atext, 10),
							bnumber = parseInt(btext, 10),
							aindex = parents.indexOf(anumber) || Number.NEGATIVE_INFINITY,
							bindex = parents.indexOf(bnumber) || Number.NEGATIVE_INFINITY;

						if (aindex === -1) {
							aindex = -siblings.indexOf(anumber)
						}
						if (bindex === -1) {
							bindex = -siblings.indexOf(bnumber)
						}
						if (aindex > bindex) {
							return 1;
						}
						if (aindex < bindex) {
							return -1;
						}
					// Deliberate fallthrough
					default:
						return atext.substr(atext.indexOf("/")).naturalCaseCompare(btext.substr(btext.indexOf("/")));
				}
			}).appendTo($list);
			this.update();
		}

		update() {
			this.scrollTop = -1;
		}

		changeActivity() {
			var activity = Editor.currentActivity;

			if (activity && this.lastActivity !== activity) {
				this.lastActivity = activity;

				console.log("change activity", this)
				//				Editor.ui.resources.createPanel(querySelector("#ic_resources"));
				scrollTop = undefined;

				ic.setImmediate("resource", function() {
					// Load options, set classes, highlight correct buttons
					$("#ic_resources>span>span").each(function(index, el) {
						var $this = $(this),
							select = $this.data("select"),
							value = Editor.getSetting("options." + select);

						$("#ic_resources").addClass($this.children("i" + (isUndefined(value) ? ":first-child" : "[data-option='" + value + "']")).addClass("active").data("option"));
					});
					// Click on a button, change classes
					$("#ic_resources>span>span>i").on("click", function() {
						var $this = $(this),
							$siblings = $this.siblings("i").removeClass("active"),
							select = $this.parent().data("select"),
							isSort = select === "resource_sort";

						$this.toggleClass("active", $siblings.length ? true : undefined);
						Editor.setSetting("options." + select, $siblings.length || $this.hasClass("active") ? $this.data("option") : "");
						$("#ic_resources").removeClass();
						$("#ic_resources>span>span>i.active").each(function(index, el) {
							$("#ic_resources").addClass($(this).data("option"));
						});
						if (isSort) {
							ResourcesPanel.instance.sort();
						} else {
							ResourcesPanel.instance.update();
						}
					});
					// Handle typing into the search box
					$("#resource_search").on("keyup change", function() {
						var value = (this as HTMLInputElement).value.trim().toLowerCase();

						$("#resource_list").children().each(function(index, el) {
							var image = this as HTMLElement;

							image.style.display = image.innerText.toLowerCase().indexOf(value) >= 0 ? "" : "none";
						});
						ResourcesPanel.instance.update();
					});
				});

				Editor.currentActivity.getTemplateFolders().then(function(nodes) {
					var files: string[] = [],
						id = Editor.currentActivity._id,
						lastFolder = -1,
						$target = $("#resource_list").empty(), // In case we're late loading
						getFileType = function(path: string): MediaType {
							switch ((path.regex(/\.([^\.]+)$/) as string).toLowerCase()) {
								case "gif":
								case "jpg":
								case "png":
								case "svg":
									return MediaType.IMAGE;

								case "mp3":
								case "m4a":
								case "ogg":
									return MediaType.AUDIO;

								case "mp4":
								case "m4v":
									return MediaType.VIDEO;
							}
							return MediaType.UNKNOWN;
						},
						showFile = function(path: string, data?: string) {
							var filePath = "../" + path,
								fileName = path.regex(/([^\/]+)$/) as string,
								fileExt = (fileName.regex(/\.([^\.]+)$/) as string).toLowerCase(),
								folder = path.regex(/(\d+)/) as number,
								url = data || encodeURI(filePath).replace(/'/g, "%27"),
								$tile = $("<a download href=\"" + url + "\" target=\"_blank\" class=\"media defer\" title=\"" + filePath + "\"></a>");

							switch (getFileType(path)) {
								case MediaType.IMAGE:
									$tile
										.addClass("image")
										.append("<div class=\"fa fa-file-image-o\" data-type=\"" + fileExt + "\" style=\"background-image:url('" + url + "');\"></div>");
									break;

								case MediaType.AUDIO:
									$tile
										.addClass("audio")
										.append("<div class=\"fa fa-file-audio-o\" data-type=\"" + fileExt + "\"><audio src=\"" + url + "\" controls></audio></div>");
									break;

								case MediaType.VIDEO:
									$tile
										.addClass("video")
										.append("<div class=\"fa fa-file-video-o\" data-type=\"" + fileExt + "\"><video src=\"" + url + "\" controls></video></div>");
									break;

								default:
									return;
							}
							if (lastFolder !== folder) {
								lastFolder = folder;
								$("<div><h3><span>" + folder + "/&#009;&#009;&#009;&#009;&#009;</span>" + (ICNodes.get(folder)._name || folder) + "</h3></div>")
									.appendTo($target);
							}
							$tile
								.append("<h3><span>" + path.regex(/(\d+)/) + "/</span>" + fileName.replace(/\.[^\.]+$/, "") + "<span>." + fileExt + "</span></h3>")
								.on("click", preventDefault)
								.data("src", filePath)
								.appendTo($target);
						};

					nodes.pushOnce.apply(nodes, Editor.currentActivity.getParents(true));
					nodes.pushOnce.apply(nodes, Editor.currentActivity.getSiblings());
					nodes.forEach(function(node) {
						if (!(node._flags & (ICNodeFlags.DELETED | ICNodeFlags.IGNORE))) {
							files.pushOnce.apply(files, node._files || []);
						}
					})
					for (var fileName in Editor.fileCache) {
						if (Editor.fileCache.hasOwnProperty(fileName)) {
							var filePath = "assets/" + id + "/" + fileName,
								fileExt = (fileName.regex(/\.([^\.]+)$/) as string).toLowerCase();

							files.remove("../" + filePath);
							showFile(filePath, "data:image/" + fileExt + ";base64," + Editor.fileCache[fileName]);
						}
					}
					files.naturalCaseSort().forEach(function(path) {
						showFile(path);
					});
					ResourcesPanel.instance.sort();
				});
			}
		}
	}
};
