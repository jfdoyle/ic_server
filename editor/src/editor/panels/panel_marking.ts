///<reference path="panel.ts" />
///<reference path="../../node/node.ts" />
///<reference path="../../../activity/src/widget/json.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace Editor {
	var updating: boolean;

	export class MarkingPanel extends Panel {
		static readonly instance: MarkingPanel;

		private constructor() {
			super("marking", PERM.ROOT, "right");

			if (MarkingPanel.instance) {
				throw new Error("Error: Instantiation failed: Use MarkingPanel.instance instead of new.");
			}
			Object.defineProperty(MarkingPanel, "instance", {
				value: this
			});
		}

		on(event: PanelEvent, args?: any) {
			switch (event) {
				case PanelEvent.DOM_UPDATED:
					if (!updating) {
						this.setActivityHtml();
					}
					break;
			}
		};

		onFocusChange() {
			currentActivity.formatHtml();
			updating = true;
			setHtml(currentActivity.html);
			updating = false;
		}

		onOptionChange(event: JQuery.Event, notMarkingPanel?: boolean) {
			var $this = $(this),
				isMultiple = $this.is("textarea"),
				element: HTMLElement = $this.data("element") || querySelector(currentActivity.dom, "#" + $this.closest("tr").data("element")),
				mark = JSON.parse(element.getAttribute("data-json") || "{}") as ic.JsonData,
				value: string | string[] = $this.is("input[type=checkbox]") ? ($this.prop("checked") ? "checked" : "") : $this.val() as string,
				empty = (value || "").trim() === "";

			// console.log(this, event)
			switch ($this.data("type")) {
				case "value":
					if (empty) {
						delete mark.value;
					} else {
						if (isMultiple) {
							value = (value as string).trim().split(/[\n\r]+/);
							if (!(value as string[]).equals(mark.value as string[])) {
								mark.value = value;
							}
						} else if (mark.value !== value) {
							mark.value = value;
						}
					}
					break;

				case "answer":
					if (empty) {
						delete mark.answer;
					} else {
						if (isMultiple) {
							value = (value as string).trim().split(/[\n\r]+/);
							if (!(value as string[]).equals(mark.answer as string[])) {
								mark.answer = value;
							}
						} else if (mark.answer !== value) {
							mark.answer = value;
						}
					}
					break;

				case "points":
					if (empty) {
						delete mark.points;
					} else if (mark.points !== value) {
						mark.points = /^[0-9]+$/.test(value) ? parseInt(value) : value;
					}
					break;

				case "round":
					if (!(this as any as HTMLInputElement).checked) {
						delete mark.round;
					} else {
						mark.round = true;
					}
					break;

				case "accept":
					if (empty) {
						element.removeAttribute("data-accept");
					} else {
						element.setAttribute("data-accept", value);
					}
					break;

				case "duplicates":
					if (!(this as any as HTMLInputElement).checked) {
						delete mark.duplicates;
					} else {
						mark.duplicates = true;
					}
					break;

				case "id":
					if (empty) {
						delete mark.id;
					} else if (mark.id !== value) {
						mark.id = value;
					}
					break;
			}
			var json: ic.JsonData = {};

			if (mark.value) {
				json.value = mark.value;
			}
			if (mark.answer) {
				json.answer = mark.answer;
			}
			if (mark.points !== undefined) {
				json.points = mark.points;
			}
			if (mark.round) {
				json.round = mark.round;
			}
			if (mark.duplicates) {
				json.duplicates = mark.duplicates;
			}
			if (mark.id) {
				json.id = mark.id;
			}
			//console.log("setting", element, JSON.stringify(json))
			if (Object.keys(json).length) {
				element.setAttribute("data-json", JSON.stringify(json));
			} else {
				element.removeAttribute("data-json");
			}
			if (notMarkingPanel !== true && event.type !== "keyup") { // change or blur
				MarkingPanel.instance.onFocusChange();
			}
		}

		setActivityHtml() {
			var $panel = this.$.empty(),
				$table = $.make("table",
					$.make("tr",
						$.make("th", "="),
						$.make("th", "=Group"),
						$.make("th", "=Value"),
						$.make("th", "=Answer"),
						$.make("th", "=Points"),
						$.make("th", "=Round"),
						$.make("th", "=Accept"),
						$.make("th", "=Dup"),
						$.make("th", "=ID")));

			//console.log("Marking", window, preview_ic)
			if (currentActivity && !currentActivity.isGroup && !currentActivity.pretty) {
				ic.setImmediate("marking.editor", this.setActivityHtml.bind(this));
			}
			if (currentActivity && !currentActivity.isGroup && currentActivity.pretty) {
				ic.setImmediate("marking.editor");
				$(currentActivity.dom).find("ic-activities,ic-activity,ic-draggable,ic-dropdown,ic-droppable,ic-option,ic-select,ic-text,ic-toggle").each(function(i, element: HTMLElement) {
					// console.log("Checking answers", widget)
					var $tr = $.make("tr").appendTo($table),
						mark = JSON.parse(element.getAttribute("data-json") || "{}") as ic.JsonData,
						name = element.tagName.replace(/^IC-/, "").ucfirst(),
						value = mark.value || "",
						valuePlaceholder = "default",
						hasValue = true,
						hasMultipleValues = false,
						answer = mark.answer || "",
						hasAnswer = true,
						hasMultipleAnswers = false,
						points = mark.points,
						pointsPlaceholder: string | number = 0,// ((widget.markable ? widget.markable.points : 0) || 0),
						hasPoints = true,
						round = mark.round,
						hasRound = false,
						accept = "",
						hasAccept = false,
						duplicates = mark.duplicates,
						hasDuplicates = false,
						groupElement = element.closest("[ic-group]"),
						group = groupElement ? parseInt(groupElement.getAttribute("ic-group"), 10) : 0,
						hasID = false,
						id = mark.id || "";

					if (!element.id) {
						return;
					}
					switch (element.tagName) {
						case "IC-ACTIVITIES":
						case "IC-ACTIVITY":
							$tr.addClass("line-above");
							hasValue = hasAnswer = false;
							hasDuplicates = hasRound = hasID = true;
							pointsPlaceholder = "?";
							//console.log(element, element.icWidget)
							//pointsPlaceholder = ((widget.markable ? widget.markable.max : 0) || 0);
							break;

						case "IC-DRAGGABLE":
							hasAnswer = hasPoints = false;
							if (element.innerText.trim()) {
								valuePlaceholder = element.innerText.trim();
							}
							break;

						case "IC-DROPDOWN":
							value = value || element.innerText;
							break;

						case "IC-DROPPABLE":
							pointsPlaceholder = isArray(answer)
								? Math.min(Math.max(parseInt(element.getAttribute("ic-droppable-count"), 10) || element.querySelectorAll(":scope>ic-draggable").length || 1, 1), answer.length)
								: answer || $(element).is("[ic-draggable=sortable]") ? 1 : -1
							hasMultipleValues = hasValue = !!pointsPlaceholder;
							valuePlaceholder = "#drag<index>#";
							hasMultipleAnswers = true;
							break;

						case "IC-OPTION":
							hasAnswer = false;
							pointsPlaceholder = 1;
							valuePlaceholder = element.innerText.trim() ? element.innerText.trim() : "";
							break;

						case "IC-SELECT":
							valuePlaceholder = "#option<index>#";
							hasPoints = false;
							hasMultipleAnswers = true;
							break;

						case "IC-TEXT":
							hasValue = false;
							hasAccept = hasMultipleAnswers = true;
							pointsPlaceholder = 1;
							accept = element.getAttribute("data-accept") || "";
							break;

						case "IC-TOGGLE":
							valuePlaceholder = element.innerText.trim() || "";
							switch (element.getAttribute("ic-toggle")) {
								case "range":
								case "multirange":
									pointsPlaceholder = /[^ .,:;'"]/.test(valuePlaceholder) ? 1 : 0;
									break;

								case "text":
									pointsPlaceholder = -1;
									break;

								default:
									pointsPlaceholder = valuePlaceholder ? 1 : -1;
									break;
							}
							break;

						default:
							console.error("Error: Unknown widget type - ", element);
					}

					$.make("td", "=" + name + (element.id ? "<br/>#" + element.id + "#" : "")).appendTo($tr);
					$.make("td", "=" + (group >= 0 ? "<i>" + group + "</i>" : "")).appendTo($tr);
					if (hasValue) {
						if (hasMultipleValues) {
							$.make("td", $.make("textarea", "placeholder=" + valuePlaceholder).val(isArray(value) ? (value as string[]).join("\n") : value).data("type", "value")).appendTo($tr);
						} else {
							$.make("td", $.make("input", "type=text", "placeholder=" + valuePlaceholder).val(value).data("type", "value")).appendTo($tr);
						}
					} else {
						$.make("td", "=n/a").appendTo($tr);
					}
					if (hasAnswer) {
						if (hasMultipleAnswers) {
							$.make("td", $.make("textarea").val(isArray(answer) ? (answer as string[]).join("\n") : answer).data("type", "answer")).appendTo($tr);
						} else {
							$.make("td", $.make("input", "type=text").val(answer).data("type", "answer")).appendTo($tr);
						}
					} else {
						$.make("td", "=n/a").appendTo($tr);
					}
					if (hasPoints) {
						$.make("td", $.make("textarea", "placeholder=default: " + pointsPlaceholder).val(points).data("type", "points")).appendTo($tr);
					} else {
						$.make("td", "=n/a").appendTo($tr);
					}
					if (hasRound) {
						$.make("td", $.make("input", "type=checkbox").prop("checked", round ? "checked" : "").data("type", "round")).appendTo($tr);
					} else {
						$.make("td", "=n/a").appendTo($tr);
					}
					if (hasAccept) {
						$.make("td", $.make("input", "type=text", "placeholder=.*").val(accept).data("type", "accept")).appendTo($tr);
					} else {
						$.make("td", "=n/a").appendTo($tr);
					}
					if (hasDuplicates) {
						$.make("td", $.make("input", "type=checkbox").prop("checked", duplicates ? "checked" : "").data("type", "duplicates")).appendTo($tr);
					} else {
						$.make("td", "=n/a").appendTo($tr);
					}
					if (hasID) {
						$.make("td", $.make("input", "type=text", "placeholder=#@", "min-width:4em;").val(id).data("type", "id")).appendTo($tr);
					} else {
						$.make("td", "=n/a").appendTo($tr);
					}
					$tr.data("element", element.id);
				});
				$table
					.appendTo($panel)
					.find("input,textarea")
					.on({
						"keyup": this.onOptionChange,
						"change": this.onOptionChange,
						"blur": this.onFocusChange
					})
					.filter("textarea")
					.each(fixTextAreaHeight);
			}
		}
	}
};
