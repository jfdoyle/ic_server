///<reference path="../../button.ts" />
///<reference path="../../../node/nodes.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

new Button("#ic_button_template", PERM.ROOT | PERM.MANAGE,
	function() {
		var parentId = parseInt($.tree.closest(), 10);

		ICNodes.load({
			"action": "create_node",
			"parent": parentId,
			"data": {
				"_name": ICNodes.getNamePrefix(parentId, "Template"),
				"_type": ICNodeType.Template
			}
		}, function(activity, index) {
			if (!index) {
				Editor.findActivityId = activity._id
			}
		});
	},
	function(activity) {
		return activity
			&& activity.getParents(true).some(function(parent) {
				return parent && parent.is(ICNodeType.Templates);
			});
	}
);
