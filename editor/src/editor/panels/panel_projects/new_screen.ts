///<reference path="../../button.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

new Button("#ic_button_add", PERM.ROOT | PERM.MANAGE | PERM.NODE_CREATE,
	function() {
		var parentId = parseInt($.tree.closest(), 10),
			createNode = function() {
				var template = $(this as HTMLElement).find("iframe").data("template") as ICNode;

				w2popup.close();
				ICNodes.load({
					"action": "create_node",
					"parent": parentId,
					"data": {
						"_name": ICNodes.getNamePrefix(parentId, "New Screen"), // template._name
						"_type": ICNodeType.Screen,
						"_html": template.html
					}
				}, function(activity, index) {
					if (!index) {
						Editor.findActivityId = activity._id
					}
				});
			},
			loadPreview = function(event: Event, ui: JQueryUI.AccordionUIParams) {
				var $panel = ui.newPanel || $("#new_screen_popup>div").first(),
					projectId: number = (Editor.currentActivity.getParents(true).find(function(activity) {
						return activity.is(ICNodeType.Project) || activity.is(ICNodeType.Folder) || activity.is(ICNodeType.Organisation);
					}) || ({} as ICNode))._id,
					loadIFrame = function() {
						var iframe = this as HTMLIFrameElement,
							$iframe = $(iframe),
							doc = iframe.contentDocument,
							template = $iframe.data("template") as ICNode;

						template.build(projectId).then(function(dom) {
							var head = doc.head,
								body = doc.body,
								inHead = true;

							$(dom).contents().each(function(i, el) {
								if (el.nodeName === "SCRIPT") {
									return;
								} else if (inHead && !["TITLE", "META", "LINK", "STYLE"].includes(el.nodeName)) {
									var link = doc.createElement("link");

									link.rel = "stylesheet";
									link.type = "text/css";
									link.href = "include/template.css";
									head.appendChild(link);
									inHead = false;
								}
								(inHead ? head : body).appendChild(el);
							});
							var walker = document.createNodeIterator(doc, NodeFilter.SHOW_ELEMENT, null, false),
								el = document.createElement("SPAN");

							for (var node = walker.nextNode(); node; node = walker.nextNode()) {
								if (node.nodeName.startsWith("IC-")) {
									el.fixState.call(node);
								}
							}
							$iframe.prev().css("visibility", "hidden");
							//ic.setImmediate("loadIFrame", function() {
							//$iframe.parent().parent().next().find("img:not([style])+iframe").each(loadIFrame);
							//});
						});
					};

				$panel.find("img:not([style])+iframe").each(loadIFrame); // .first()
			},
			$popup = $("<div>"),
			w2popup = $().w2popup({
				title: "<i class=\"fa fa-object-group\"></i> New Screen",
				body: "<div id=\"new_screen_popup\"></div>",
				style: "",
				width: window.innerWidth * 0.8,
				height: window.innerHeight * 0.8,
				onOpen: function() {
					ic.setImmediate("new_screen", function() {
						$("#new_screen_popup").append($popup.children()).accordion({
							heightStyle: "content",
							beforeActivate: loadPreview,
							create: loadPreview
						});
					});
				}
			});

		Editor.currentActivity.getTemplates().then(function(templates) {
			var panels: { [name: string]: { [name: string]: ICNode } } = {};

			templates.forEach(function(template) {
				var parent = template.getParent()._name,
					name = template._name;

				if (!panels[parent]) {
					panels[parent] = {};
				}
				if (!panels[parent][name]) {
					panels[parent][name] = template;
				}
			});
			Object.keys(panels).sort(function(a, b) {
				a = a.toUpperCase();
				b = b.toUpperCase();
				if (a === "TEMPLATES" || a < b) {
					return -1;
				}
				if (b === "TEMPLATES" || a > b) {
					return 1;
				}
				return 0;
			}).forEach(function(panelName) {
				var panel = panels[panelName],
					$panel = $.make("div"),
					hasScreens = false,
					$target = $("#new_screen_popup").add($popup).first(); // In case we're late loading

				Object.keys(panel).sort().forEach(function(screenName) {
					var template = panel[screenName];

					if (!hasScreens) {
						hasScreens = true;
						$.make("h3", "=" + panelName).add($panel).appendTo($target);
					}
					$.make("div",
						$.make("div",
							$.make("img", "src=include/icons/screenshot.png"),
							$.make("iframe", "ns_" + template._id)
								.data("template", template)),
						$.make("h4", "=" + screenName))
						.on("click", createNode)
						.appendTo($panel);
				});
			});
			$("#new_screen_popup.ui-accordion").accordion("refresh");
		});
	},
	function(activity) {
		var parent = activity ? activity.getParent() : null;

		return activity && (ICNodeTypeChildren[activity._type].includes(ICNodeType.Screen)
			|| (parent && ICNodeTypeChildren[parent._type].includes(ICNodeType.Screen)));
	}
);
