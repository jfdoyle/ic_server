///<reference path="../../button.ts" />
///<reference path="../../settings.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

new Button("#ic_button_recycle", PERM.ROOT | PERM.MANAGE | PERM.NODE_CREATE | PERM.NODE_DELETE,
	function(activity, event: JQuery.Event) {
		var state = !Editor.getSetting("projects.recycle", false);

		Editor.setSetting("projects.recycle", state);
		$("#ic_activities").toggleClass("show-trash", !!state);
	},
	function(activity) {
		// TODO: Check for advanced settings
		$("#ic_activities").toggleClass("show-trash", !!Editor.getSetting("projects.recycle", false));
		return true;
	}
);
