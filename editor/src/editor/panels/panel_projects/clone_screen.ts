///<reference path="../../button.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

new Button("#ic_button_clone", PERM.ROOT | PERM.MANAGE | PERM.NODE_CREATE,
	function(activity) {
		if (activity.isGroup && !confirm("Are you sure you wish to clone this entire tree?")) {
			return;
		}
		ICNodes.load({
			"action": "clone_node",
			"id": $.tree.currentId
		}, function(activity, index) {
			if (!index) {
				Editor.findActivityId = activity._id
			}
		});
	},
	function(activity) {
		return activity && (Editor.permissions.isRoot
			|| (Editor.permissions.isOwner && !activity.is(ICNodeType.Organisation) && !activity.is(ICNodeType.Templates))
			|| activity.is(ICNodeType.Screen)
			|| activity.is(ICNodeType.Theme));
	}
);
