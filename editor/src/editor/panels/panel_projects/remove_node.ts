///<reference path="../../button.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

new Button("#ic_button_remove", PERM.ROOT | PERM.MANAGE | PERM.NODE_DELETE,
	function() {
		ICNodes.load({
			"action": "delete_node",
			"id": $.tree.currentId
		});
	},
	function(activity) {
		return activity && (Editor.permissions.isRoot || !activity.is(ICNodeType.Organisation)) && !Editor.change && !!activity._parent;
	}
);
