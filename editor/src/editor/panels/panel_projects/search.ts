///<reference path="../../button.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

new Button("#ic_button_search", PERM.ROOT | PERM.MANAGE | PERM.VIEW,
	function() {
		var popup,
			click = function() {
				var id = parseFloat($(this).val() as string);

				popup.close();
				if (ICNodes.has(id)) {
					$.tree.click(String(id));
				} else {
					ICNodes.loadParents(Editor.findActivityId = id);
				}
			},
			search = function() {
				var $id = $("#search input[name='id']"),
					crc = $("#search input[name='crc']").val() as string,
					id = parseFloat($id.val() as string),
					contains = $("#search input[name='contains']").val(),
					$select = $("#search select[name='results']");

				if (id && !isNaN(id)) {
					click.call($id[0]);
				} else if (crc) {
					if (!/^[0-9a-f]{1,8}$/i.test(crc)) {
						return;
					}
					for (let i = 0; i < 100000; i++) {
						if (String(i).crc32() === crc) {
							$id.val(String(i));
							click.call($id[0]);
							return;
						}
					}
				} else if (contains) {
					popup.lock();
					Editor.ajax({
						type: "POST",
						url: "ajax.php",
						dataType: "json",
						data: {
							action: "find_node",
							path: contains
						},
						success: function(data) {
							var key: string;

							popup.unlock();
							$select.empty();
							for (key in data) {
								$select.append("<option value=\"" + key + "\">" + data[key] + "</option>");
							}
						}
					});
				}
			};

		if (!(w2ui as any).search) {
			$().w2form({
				name: "search",
				style: "border:0px;background-color:transparent;",
				formHTML:
				'<div class="w2ui-page page-0">' +
				'    <div class="w2ui-field">' +
				'        <label>#ID:</label>' +
				'        <div style="display:inline-block;margin:0 0 3px 8px;float:left;">' +
				'           <input name="id" type="number" autocomplete="off" inputmode="numeric" maxlength="10" style="width:100px;"/>' +
				'        </div>' +
				'        <div style="display:inline-block;margin:0 0 3px 0;">' +
				'	        <label>#CRC:</label>' +
				'           <input name="crc" type="search" autocomplete="off" autocorrect="off" maxlength="8" style="width:100px;"/>' +
				'        </div>' +
				'    </div>' +
				'    <div class="w2ui-field">' +
				'        <label>Contains:</label>' +
				'        <div>' +
				'            <input name="contains" type="text" maxlength="100" style="width:300px;"/>' +
				'        </div>' +
				'    </div>' +
				'    <div class="w2ui-field">' +
				'        <label>Results:</label>' +
				'        <div>' +
				'            <select name="results" multiple style="width:300px;height:90px;"></select>' +
				'        </div>' +
				'    </div>' +
				'</div>' +
				'<div class="w2ui-buttons">' +
				'    <button class="btn" name="reset">Reset</button>' +
				'    <button class="btn" name="search">Search</button>' +
				'</div>',
				fields: [
					{
						field: "id",
						type: "number"
					},
					{
						field: "contains",
						type: "text"
					}
				],
				actions: {
					"reset": function() {
						this.clear();
						$("#search select[name='results']").empty();
					},
					"search": search
				}
			});
		}
		popup = $().w2popup({
			title: "<i class=\"fa fa-question-circle\"></i> Search",
			body: "<div id=\"search\" style=\"width:100%;height:100%;\"></div>",
			style: "padding: 15px 0px 0px 0px",
			width: 500,
			height: 300,
			onToggle: function(event) {
				$((w2ui as any).search.box).hide();
				event.onComplete = function() {
					$((w2ui as any).search.box).show();
					(w2ui as any).search.resize();
				}
			},
			onOpen: function(event) {
				//(w2ui as any).search.clear();
				event.onComplete = function() {
					($("#search") as any).w2render("search");
					$("#search select[name='results']").on("change", click);
					$("#search input").on("keypress", function(event) {
						if (event.which == 13) {
							search();
							return false;
						}
					});
				}
			}
		});
	},
	function(activity) {
		return true;
	}
);
