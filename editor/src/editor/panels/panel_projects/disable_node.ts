///<reference path="../../button.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

new Button("#ic_button_disable", PERM.ROOT,
	function() {
		ICNodes.load({
			"action": "disable_node",
			"id": $.tree.currentId
		});
	},
	function(activity) {
		return activity && !activity.is(ICNodeType.Organisation) && !Editor.change && !!activity._parent;
	}
);
