///<reference path="../../button.ts" />
///<reference path="../../settings.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

new Button("#ic_button_files", PERM.ROOT | PERM.MANAGE | PERM.FILES,
	function(activity, event: JQuery.Event) {
		var state = !Editor.getSetting("projects.files", false);

		Editor.setSetting("projects.files", state);
		$("#ic_activities").toggleClass("show-files", !!state);
	},
	function(activity) {
		// TODO: Check for advanced settings
		$("#ic_activities").toggleClass("show-files", !!Editor.getSetting("projects.files", false));
		return true;
	}
);
