///<reference path="../../button.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

new Button("#ic_button_organisation", PERM.ROOT,
	function(activity, event: JQuery.Event) {
		var name = prompt("Please enter the name for the new Organisation folder, blank to cancel...", ICNodes.getNamePrefix(0, "New Organisation"));

		if (name) {
			ICNodes.load({
				"action": "create_node",
				"parent": 0,
				"data": {
					"_name": name,
					"_type": ICNodeType.Organisation
				}
			}, function(activity, index) {
				if (!index) {
					Editor.findActivityId = activity._id
				}
			});
		}
	},
	function(activity) {
		return !activity || activity.is(ICNodeType.Organisation);
	}
);
