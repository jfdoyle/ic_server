///<reference path="../../button.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

new Button("#ic_button_folder", PERM.ROOT | PERM.MANAGE | PERM.NODE_CREATE,
	function(activity, event: JQuery.Event) {
		var parentId = $.tree.closest();

		ICNodes.load({
			"action": "create_node",
			"parent": parentId,
			"data": {
				"_name": ICNodes.getNamePrefix(parseInt(parentId, 10), "New Folder"),
				"_type": ICNodeType.Folder
			}
		}, function(activity, index) {
			if (!index) {
				Editor.findActivityId = activity._id
			}
		});
	},
	function(activity) {
		return activity && ICNodeTypeChildren[activity._type].includes(ICNodeType.Folder);
	}
);
