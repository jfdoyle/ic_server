///<reference path="../../button.ts" />
///<reference path="../../../node/nodes.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

new Button("#ic_button_templates", PERM.ROOT | PERM.MANAGE,
	function() {
		var parentId = parseInt($.tree.closest(), 10);

		ICNodes.load({
			"action": "create_node",
			"parent": parentId,
			"data": {
				"_name": ICNodes.getNamePrefix(parentId, "Templates"),
				"_type": ICNodeType.Templates
			}
		}, function(activity, index) {
			if (!index) {
				Editor.findActivityId = activity._id
			}
		});
	},
	function(activity) {
		return activity && ICNodeTypeChildren[activity._type].includes(ICNodeType.Templates)
			&& !activity.getChildren().some(function(child) {
				// Make sure we're unique
				return child && child.is(ICNodeType.Templates);
			});
	}
);
