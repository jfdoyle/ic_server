///<reference path="panel.ts" />
///<reference path="../../node/node.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace Editor {
	export class ProjectsPanel extends Panel {
		static readonly instance: ProjectsPanel;

		private constructor() {
			super("projects", PERM.VIEW, "left");

			if (ProjectsPanel.instance) {
				throw new Error("Error: Instantiation failed: Use ProjectsPanel.instance instead of new.");
			}
			Object.defineProperty(ProjectsPanel, "instance", {
				value: this
			});
			//this.$.parent().siblings(".w2ui-panel-title").append(title_html.replace(/[\t\n\r]/g, ""));

			$.tree.init({
				tree: $("#ic_activities>ul"),
				autoExpand: true,
				isParent: this.isParent,
				getChildren: this.getChildren,
				getParent: this.getParent,
				getIndex: this.getIndex,
				getHtml: this.getTitle,
				onAfterHtml: this.onAfterTitle,
				onBeforeClick: this.onBeforeClick,
				onClick: this.onTreeClick,
				canDrag: this.onDrag,
				onDrop: this.onDrop,
				onPaste: this.onPaste
			} as TreeOptions);

			window.setInterval(function() {
				$("#ic_activities .aba-flag-timer:not(.hidden)").each(function() {
					var $this = $(this),
						date = $this.data("date") as Date;

					if (date) {
						$this.attr("title", date.format("R"));
						if ((new Date().getTime() - date.getTime()) / 1000 > 3600) {
							$this.fadeOut(function() {
								$this.addClass("hidden").css("display", "");
							});
						}
					}
				});
			}, 30000); // every 30 secs
		}

		//		startup() {
		//			(w2ui["layout_" + this.side + "_tabs"] as W2UI.W2Tabs).right = title_html.replace(/[\t\n\r]/g, "");
		//		}

		on(event: PanelEvent, args?: any) {
			switch (event) {
				case PanelEvent.LOADED:
					this.get_templates(0);
					break;
			}
		};

		isParent(id: string): boolean {
			var num = parseInt(id, 10);

			if (/[^0-9]/.test(id)) {
				// file
				return false;
			}
			return ICNodes.isGroup(num);
		}

		getParent(id: string): string {
			var num = parseInt(id, 10);

			if (/[^0-9]/.test(id)) {
				// file
				return String(num);
			}
			var parent = ICNodes.getParentId(num);

			return parent ? String(parent) : "";
		}

		getIndex(id: string): number {
			var num = parseInt(id, 10);

			if (/[^0-9]/.test(id)) {
				// file
				return -1;
			}
			return ICNodes.getIndex(num);
		}

		getChildren(id: string): void {
			var num = parseInt(id, 10),
				activity = ICNodes.get(num);

			if (!activity) {
				ICNodes.load(num);
			} else if (activity.isGroup) {
				var nodes = [];

				activity.getChildrenId().forEach(function(id) {
					if (!ICNodes.has(id)) {
						nodes.push(id);
					}
				});
				ICNodes.load(nodes);
			}
		}

		getTitle(id: string, $el: JQuery): string {
			//				console.log("getHtml", id, $el)
			var num = parseInt(id, 10);

			if (/[^0-9]/.test(id)) {
				var icon = "fa-file-o";

				switch (id.regex(/\.([^\.]+)$/)) {
					case "gif":
					case "jpg":
					case "jpeg":
					case "png":
						icon = "fa-file-image-o";
						break;
					case "m4a":
					case "mp3":
					case "ogg":
						icon = "fa-file-audio-o";
						break;
					case "mp4":
					case "ogv":
						icon = "fa-file-video-o";
						break;
					case "css":
					case "js":
					case "scss":
					case "svg":
						icon = "fa-file-code-o";
						break;
					default:
						break;
				}
				$el
					.html("<span class=\"fa fa-fw " + icon + "\"></span><span>" + id.replace(/^\d+_/, "") + "</span>")
					.parent()
					.addClass("aba-tree-file");
			} else {
				var i, tmp,
					$tmp: JQuery,
					activity = ICNodes.get(num),
					$flags = $el.next(),
					date = new Date(activity._date),
					age = (new Date().getTime() - date.getTime()) / 1000;

				$el.html("<span class=\"fa fa-fw " + activity.icon + "\"></span><span class=\"aba-tree-name\">" + activity._name + "</span>");
				if (!$flags.length) {
					tmp = ["append"];
					$flags = $.make("span", "flags",
						//				$.make("span", "aba-flag-show-scss", "fa fa-paint-brush", "color:firebrick", "title=Has Custom CSS"),
						$.make("span", "aba-flag-show-modules", "fa fa-cogs", "color:firebrick"),
						$.make("span", "aba-flag-show-feedback", "fa fa-check-circle-o", "color:firebrick", "title=Has Feedback"),
						$.make("span", "aba-flag-show-root", "fa fa-home", "color:blue"),
						$.make("span", "aba-flag-show-files", "fa fa-file", "color:blue", "title=Has files"),
						tmp,
						$.make("span", "aba-flag-show-comments", "fa fa-comment", "color:blue"),
						$.make("span", "aba-flag-deleted", "fa fa-trash", "color:black"),
						$.make("span", "aba-flag-disabled", "fa fa-ban", "color:black"),
						$.make("span", "aba-flag-timer fa fa-clock-o", "color:red")
					).insertAfter($el);
				}
				$el.parent()
					.toggleClass("hidden", !!(activity._flags & ICNodeFlags.IGNORE && !Editor.permissions.isRoot))
					.toggleClass("aba-flag-trash", !!(activity._flags & ICNodeFlags.DELETED))
					.toggleClass("aba-tree-template", activity.is(ICNodeType.Templates))
					.removeClass("aba-tree-warning");
				if (activity.is(ICNodeType.Templates)) {
					$el.parent().toggleClass("hidden", !(Editor.permissions.isOwner || Editor.permissions.isManager || Editor.permissions.canTheme));
				}
				//		$flags.find(".aba-flag-show-scss")
				//			.toggleClass("hidden", !(Editor.permissions.canEdit && activity.theme && activity.theme.style));
				//				$flags.find(".aba-flag-show-modules")
				//					.toggleClass("hidden", !(Editor.permissions.canEdit && activity.modules))
				//					.attr("title", "Has Modules: " + Object.keys(activity.modules || {}).wrap(", ", "\"", "\""));
				//				$flags.find(".aba-flag-show-feedback")
				//					.toggleClass("hidden", !(Editor.permissions.canEdit && activity.feedback));
				//		$flags.find(".aba-flag-show-root")
				//			.toggleClass("hidden", !(($aba.isOwner || $aba.isManager) && activity._root))
				//			.attr("title", "Root: " + (activity._root || 0));
				$flags.find(".aba-flag-show-files")
					.toggleClass("hidden", !((Editor.permissions.isOwner || Editor.permissions.isManager) && activity._files && activity._files.length));
				$flags.find(".aba-flag-deleted")
					.toggleClass("hidden", !(activity._flags & ICNodeFlags.DELETED));
				$flags.find(".aba-flag-disabled")
					.toggleClass("hidden", !(activity._flags & ICNodeFlags.IGNORE));
				$tmp = $flags.find(".aba-flag-timer")
					.toggleClass("hidden", age > 3600)
					.attr("title", "Changed " + date.format("R"))
					.data("date", date);
				if (age < 60) {
					for (i = 10; i >= 0; i--) {
						$tmp.fadeOut().fadeIn();
					}
				}
			}
			return "";
		}

		private checkDuplicates: HTMLElement[];

		private deferShowDuplicates() {
			this.checkDuplicates.forEach(function(parent) {
				// Mark any that are duplicate paths
				var i, isSame, this_text, prev_text,
					$this: JQuery,
					$prev: JQuery,
					$siblings = $(">li.aba-tree-node:not(.aba-flag-trash)>a", parent)
						.sort(function(a, b) {
							var a_text = a.textContent,
								b_text = b.textContent;
							return a_text < b_text ? -1 : a_text > b_text ? 1 : 0;
						});

				for (i = 1; i < $siblings.length; i++) {
					$this = $siblings.eq(i);
					$prev = $siblings.eq(i - 1);
					this_text = $this.text();
					prev_text = $prev.text();
					isSame = !!this_text && this_text === prev_text;
					$this.closest("li").toggleClass("aba-tree-warning", isSame); // Toggle on own - so it resets when not needed
					if (isSame) {
						$prev.closest("li").addClass("aba-tree-warning"); // Add on previous as we've already passed it
					}
				}
			});
			this.checkDuplicates = [];
		}

		onAfterTitle(id: string, $el: JQuery, $parent: JQuery) {
			var that = ProjectsPanel.instance;

			if (!that.checkDuplicates) {
				that.checkDuplicates = [];
			}
			that.checkDuplicates.pushOnce($parent[0]);
			ic.setImmediate("deferShowDuplicates", that.deferShowDuplicates.bind(that));
		}

		onBeforeClick(id) {
			//if (Editor.change && !confirm("You have unsaved changes, are you sure you wish to lose them?")) {
			//	return false;
			//}
		}

		onTreeClick(id: string) { // id clicked on
			var num = parseInt(id, 10);

			if (/[^0-9]/.test(id)) {
				var filename = Config.server + "assets/" + id.replace("_", "/"),
					src = "";

				switch (id.regex(/\.([^\.]+)$/)) {
					case "gif":
					case "jpg":
					case "jpeg":
					case "png":
						src = "<img src=\"" + filename + "\" style=\"margin:0 auto;\"/>";
						break;
					case "m4a":
					case "mp3":
					case "ogg":
						src = "<audio src=\"" + filename + " style=\"margin:0 auto;\"\"></audio>";
						break;
					case "mp4":
					case "ogv":
						src = "<vidio src=\"" + filename + " style=\"margin:0 auto;\"\"></vidio>";
						break;
					case "css":
					case "js":
					case "scss":
					case "svg":
						// Use source display too
						src = "<iframe src=\"" + filename + "\" style=\"width:100%;height:100%;border:0\"></iframe>";
						break;
					default:
						src = filename;
						break;
				}

				console.log("click on file", filename);
				location.hash = id;
				$("#ic_preview>iframe").attr("srcdoc", "<!DOCTYPE html>\n<html style=\"height:100%;\"><body style=\"height:100%;margin:0;overflow:hidden;\">" + src + "</body></html>");
				// file
			} else if (ICNodes.has(num)) {
				location.hash = id;
				Editor.fileCache = {}; // Clear cache
				Editor.change = false;
				Editor.setActivity(ICNodes.get(num));
				Button.update();
			}
		}

		onDrag(id) {
			// console.log("onDrag", !Editor.currentActivity || Editor.currentActivity._id !== id || !Editor.change)
			return (Editor.permissions.has(PERM.ROOT) || Editor.permissions.is(PERM.NODE_CREATE | PERM.NODE_DELETE)) && (!Editor.currentActivity || Editor.currentActivity._id !== id || !Editor.change);
		}

		onDrop(id, target, moveType) {
			// TODO: Add D&D to new tree
			var parent, pos,
				old_parent = $.tree.getParentId(id),
				old_pos = $.tree.index(id);

			switch (moveType) {
				case "inner":
					parent = target;
					pos = -1;
					break;
				case "prev":
					parent = $.tree.getParentId(target);
					pos = $.tree.index(target);
					break;
				case "next":
					parent = $.tree.getParentId(target);
					pos = $.tree.index(target) + 1;
					break;
				default:
					return; // cancelled?
			}
			if (parent === old_parent && pos >= 0 && pos > old_pos) {
				pos--;
			}
			console.log("id", id, "target", target, "type", moveType, "old_parent", old_parent, "old_pos", old_pos, "new_parent", parent, "new_pos", pos, "target_pos", $.tree.index(target));
			if ((old_parent !== parent || old_pos !== pos) && (parent || Editor.permissions.isRoot)) { // Only root can put us in the root
				ICNodes.load({
					"action": "move_node",
					"id": id,
					"parent": parent,
					"pos": pos
				}, function(activity, index) {
					if (!index) {
						Editor.findActivityId = activity._id
					}
				});
			}
		}

		onPaste(id, target) {
			var isParent = $.tree.isParent(target),
				parent = isParent ? target : $.tree.getParentId(target),
				activity = ICNodes.get(id);

			if (!ICNodes.get(parent).getParentsId(true).includes(id)
				&& Editor.permissions.isRoot
				|| (parent && // Only root can put us in the root
					(Editor.permissions.isOwner && !activity.is(ICNodeType.Organisation) && !activity.is(ICNodeType.Templates))
					|| activity.is(ICNodeType.Screen)
					|| activity.is(ICNodeType.Theme))) {
				ICNodes.load({
					"action": "clone_node",
					"id": id,
					"parent": parent,
					"pos": isParent ? -1 : $.tree.index(target) + 1
				}, function(activity, index) {
					if (!index) {
						Editor.findActivityId = activity._id
					}
				});
			}
		}

		get_templates(id) { // TODO: Implement templates
			ICNodes.load({
				"action": "get_templates",
				"id": id
			});
		}
	}
};
