///<reference path="panel.ts" />
///<reference path="../../node/node.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface HTMLIFrameElement {
	srcdoc: string;
}

interface Window {
	eval(x: string): any;
}

namespace Editor {
	function toggleShowMarks(this: HTMLElement) {
		var screens = ($("#ic_preview>iframe")[0] as HTMLIFrameElement).contentDocument.getElementsByTagName("ic-screens")[0] as HTMLElement,
			state = PreviewPanel.instance.showMarks = !this.classList.contains("active");

		this.classList.toggle("active");
		Editor.setSetting("options.preview_marks", state);
		if (screens) {
			(screens as any).icWidget.toggleState("marked", state);
		}
	}

	const enum StoryboardState {
		PAUSED = -1,
		PLAYING = 0,
		STEPPING = 1,
		SKIPPING = 2
	}

	var hasStoryboards: boolean,
		storyboardState: StoryboardState,
		storyboardName: string,
		storyboardAction: string;

	function checkStoryboard() {
		var frame = $("#ic_preview>iframe")[0] as HTMLIFrameElement,
			skip: boolean;

		if (frame) {
			var doc = frame.contentDocument,
				win = doc && doc.defaultView as Window,
				result = win && win.eval("window.IC&&[window.IC.hasStoryboards,window.IC.storyboardState,window.IC.storyboardName,window.IC.storyboardAction]");

			if (isArray(result)) {
				skip = true;
				if (hasStoryboards !== result[0]) {
					hasStoryboards = result[0];
					$("#show_storyboard").toggle(!!hasStoryboards);
				}
				if (hasStoryboards) {
					if (storyboardState !== result[1]) {
						storyboardState = result[1];
						$("#show_storyboard").toggleClass("playing", storyboardState >= 0);
					}
					if (storyboardName !== result[2] || storyboardAction !== result[3]) {
						storyboardName = result[2];
						storyboardAction = result[3]; var text = ((storyboardName || "") + (storyboardAction ? ": " + storyboardAction : "")) || "---";
						$("#show_storyboard>span")
							.text(text)
							.attr("title", text);
					}
				}
			}
		}
		if (!skip && hasStoryboards) {
			hasStoryboards = false;
			$("#show_storyboard").hide();
		}
		ic.rAF(checkStoryboard);
	}
	checkStoryboard();

	function setStoryboards(eventObject: JQuery.Event, ...args: any[]) {
		if (hasStoryboards) {
			($("#ic_preview>iframe")[0] as HTMLIFrameElement).contentDocument.defaultView.eval("window.IC.storyboardState=" + eventObject.data + ";");
		}
	}

	export class PreviewPanel extends Panel {
		static readonly instance: PreviewPanel;

		showMarks = false;

		private constructor() {
			super("preview", PERM.VIEW, "main");

			if (PreviewPanel.instance) {
				throw new Error("Error: Instantiation failed: Use PreviewPanel.instance instead of new.");
			}
			Object.defineProperty(PreviewPanel, "instance", {
				value: this
			});
			this.showMarks = !!Editor.getSetting("options.preview_marks");
			if (this.showMarks) {
				getElementById("show_marks").classList.add("active");
			}
			getElementById("show_marks").addEventListener("click", toggleShowMarks);
			$("#show_storyboard>.fa-pause").on("click", StoryboardState.PAUSED as any, setStoryboards);
			$("#show_storyboard>.fa-play").on("click", StoryboardState.PLAYING as any, setStoryboards);
			$("#show_storyboard>.fa-step-forward").on("click", StoryboardState.STEPPING as any, setStoryboards);
			$("#show_storyboard>.fa-fast-forward").on("click", StoryboardState.SKIPPING as any, setStoryboards);
		}

		on(event: PanelEvent, args?: any) {
			switch (event) {
				case PanelEvent.DOM_UPDATED:
					this.setActivityHtml();
					break;
			}
		};

		setActivityHtml() {
			var iframe = createElement("iframe") as HTMLIFrameElement;

			iframe.onload = function() {
				var doc = (this as HTMLIFrameElement).contentDocument,
					fixScript = function(el: HTMLScriptElement) {
						var script = doc.createElement("script");

						script.text = (el as HTMLScriptElement).text;
						el.async = el.defer = false;
						[].forEach.call(el.attributes, function(attribute: Attr) {
							if (attribute.name !== "defer") {
								script.setAttribute(attribute.name, attribute.value);
							}
						});
						return script;
					};

				doc.documentElement.style.visibility = "hidden";
				Editor.currentActivity.build().then(function(dom) {
					var head = doc.head,
						body = doc.body,
						inHead = true;

					head.parentElement.setAttribute("data-uid", String(currentActivity._id).crc32());
					doc.defaultView.eval("var IC=IC||{};IC.storyboardState=" + StoryboardState.PAUSED + ";");
					[].forEach.call(dom.childNodes, function(el: HTMLElement) {
						((inHead = inHead && ["BASE", "LINK", "META", "NOSCRIPT", "SCRIPT", "STYLE", "TITLE"].includes(el.nodeName))
							? head
							: body).appendChild(el.nodeName === "SCRIPT"
								? fixScript(el as HTMLScriptElement)
								: el.cloneNode(true) as HTMLElement);
					});
					var screens = doc.getElementsByTagName("ic-screens")[0] as HTMLElement,
						fixScripts = function() {
							if ((screens as any).icWidget) {
								querySelectorAll(doc, "script").forEach(function(script: HTMLScriptElement) {
									var parent = script.parentElement;

									if (parent !== head && parent !== body) {
										parent.replaceChild(fixScript(script), script);
									}
								});
								if (PreviewPanel.instance.showMarks) {
									(screens as any).icWidget.addState("marked");
								}
							} else {
								ic.rAF(fixScripts);
							}
						};

					if (screens) {
						fixScripts();
					}
				});
			};
			iframe.sandbox.add("allow-same-origin", "allow-scripts");
			iframe.srcdoc = "<!DOCTYPE html>\n<html><head></head><body></body></html>";
			iframe.src = "blank.html";
			$("#ic_preview>iframe").replaceWith(iframe);
		};
	}
};
