///<reference path="panel.ts" />
///<reference path="../../node/node.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

// TODO: Remove this when https://github.com/DefinitelyTyped/DefinitelyTyped/pull/7764 is pulled
module AceAjax {
	export interface Editor {
		on(ev: string, callback: (e: any) => any): void;
	}
}

namespace Editor {
	var updating: boolean;

	export class SourcePanel extends Panel {
		static readonly instance: SourcePanel;

		public editor: AceAjax.Editor;
		public session: AceAjax.IEditSession;
		public lastSource: string;
		private lastId: number;

		public errors: boolean;

		private element: HTMLElement;

		private changed: boolean; // the content has changed
		private editing: boolean; // we sent an update so skip this next refresh

		private constructor() {
			super("source", PERM.ROOT, "preview");

			if (SourcePanel.instance) {
				throw new Error("Error: Instantiation failed: Use SourcePanel.instance instead of new.");
			}
			Object.defineProperty(SourcePanel, "instance", {
				value: this
			});

			this.element = createElement("div");

			var editor: AceAjax.Editor = this.editor = ace.edit("ic_source"),
				session = this.session = editor.getSession();

			editor.setTheme("ace/theme/xcode");
			editor.$blockScrolling = Infinity; // Prevent a really annoying warning message that has existed for far too long
			editor.setShowPrintMargin(false);
			editor.on("change", this.onChange);
			session.setMode("ace/mode/html");
			session.setUseSoftTabs(false);
			editor.setOption("showInvisibles", true);
			session.on("changeAnnotation", this.onAnnotation);

			setInterval(this.onChangeCallback, 100);
		}

		on(event: PanelEvent, args?: any) {
			switch (event) {
				case PanelEvent.DOM_UPDATED:
					if (!updating) {
						this.setActivityHtml();
					}
					break;
			}
		};

		onClick(event: MouseEvent) {
			super.onClick(event);
			this.editor.resize();
		}

		onAnnotation() {
			var that = SourcePanel.instance,
				annotations: AceAjax.Annotation[] = that.session.getAnnotations() || [],
				i = annotations.length,
				errors = false,
				change = false;

			while (i--) {
				var annotation = annotations[i];

				if (/doctype first\. Expected/.test(annotation.text)) {
					// Not all source is a full HTML document - so remove the warning if not
					change = true;
					annotations.splice(i, 1);
				} else if (!errors && annotation.type === "error") {
					errors = true;
				}
			}
			if (change) {
				that.session.setAnnotations(annotations);
			}
			if (!errors && that.errors) {
				// Annotations update after main code, so let the live preview update when we notice it's fixed
				that.setActivityHtml();
			}
			that.errors = errors;
			//		console.log("annotations", annotations)
		}

		onChangeCallback() {
			var that = SourcePanel.instance;

			if (that.changed && !that.errors) {
				var source = that.editor.getValue();

				that.changed = false;
				if (that.lastSource !== source) {
					that.editing = true;
					updating = true;
					setHtml(source);
					updating = false;
					that.lastSource = currentActivity.pretty;
				}
			}
		}

		onChange(change: AceAjax.EditorChangeEvent) {
			SourcePanel.instance.changed = true;
		}

		setActivityHtml() {
			var source = currentActivity.pretty;

			if (!source) {
				ic.setImmediate("source.editor", this.setActivityHtml.bind(this));
			} else {
				ic.setImmediate("source.editor");
				if (this.lastSource !== source || this.lastId !== currentActivity._id) {
					this.lastSource = source;
					if (this.lastId === currentActivity._id) {
						if (!this.editing) {
							this.editor.setValue(source, -1);
						}
						this.editing = false;
					} else {
						this.lastId = Editor.currentActivity._id;
						this.session.setValue(source);
					}
				}
			}
		}
	}
};
