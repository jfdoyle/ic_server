///<reference path="panel.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace Editor {
	export class BottomPanel extends Panel {
		static readonly instance: BottomPanel;

		private constructor() {
			super("footer", PERM.NONE, "bottom");

			if (BottomPanel.instance) {
				throw new Error("Error: Instantiation failed: Use BottomPanel.instance instead of new.");
			}
			Object.defineProperty(BottomPanel, "instance", {
				value: this
			});
		}

		on(event: PanelEvent, args?: any) {
			switch (event) {
			}
		};
	}
};
