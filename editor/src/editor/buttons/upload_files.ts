///<reference path="../button.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

new Button("#ic_file_label", PERM.ROOT | PERM.FILES,
	function(activity) {
		// Don't do anything
	},
	function(activity) {
		// TODO: Check with permissions
		if (!this.started) {
			this.started = true;
			$(document).on("change", "input[type='file']", function(this: HTMLInputElement, event) {
				var input = this,
					i: number,
					file: File,
					reader: FileReader,
					confirm: string[] = [],
					fileData: {[name: string]: FileReader} = {},
					files = this.files;

				console.log(event, files);
				for (i = 0; i < files.length; i++) {
					file = files[i];

					if (/\.min\.js$/i.test(file.name)) {
						continue;
					}
					if (Editor.currentActivity._files.includes("assets/" + Editor.currentActivity._id + "/" + file.name)) {
						confirm.push(file.name);
					}

					fileData[file.name] = reader = new FileReader();

					reader.onloadend = function() {
						var fn = function() {
							for (var name in fileData) {
								Editor.fileCache[name] = (fileData[name].result as string || "").replace(/^.*?,/, "");
								if (/\.scss$/i.test(name)) {
									delete Editor.fileCache[name.replace(/\.scss$/i, ".css")];
								}
							}
							Editor.change = true;
							Button.update();
							if (Editor.imagePopup) {
								ic.setImmediate("image_popup", function() {
									Editor.imagePopup.close();
									Editor.imagePopup = undefined;
									$("#image_bank_list").attr("id", "");
									Editor.openImageBank();
								});
							}
							input.value = "";
						};

						console.log("onloadend", confirm)

						for (var name in fileData) {
							if (fileData[name].readyState < 2) {
								return;
							}
							// console.log(name, fileData[name].result);
						}

						if (confirm.length) {
							Editor.dialog("Are you sure you wish to replace " + (confirm.length > 1 ? "<br/>" : "") + confirm.wrap(",<br/>", "'<span class=\"mono\">", "</span>'") + "?<br/>This cannot be undone!", "Confirm Replace", fn);
						} else {
							fn();
						}
					};
					reader.readAsDataURL(file);
				}
				return false;
			});
		}
		return !!activity;
	}
);
