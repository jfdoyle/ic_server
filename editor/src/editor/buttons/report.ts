///<reference path="../button.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

new Button("#ic_button_report", PERM.ROOT | PERM.MANAGE,
	function(activity) {
		$().w2popup({
			title: "<i class=\"fa fa-eye\"></i> Report",
			body: "<div id=\"report_popup\"><table><thead><tr><td>ID</td><td>Parent</td><td>Visible</td><td>Flags</td><td>Type</td><td>Name</td></tr></thead><tbody id=\"report_body\"></tbody></table></div>",
			style: "",
			width: 1000,
			height: 800,
			resizable: true
		});

		activity.loadChildren()
			.then(() => {
				var tableBody = $("#report_body")[0],
					makeRow = (activity: ICNode) => {
						if (activity && activity._id) {
							var html = ((activity as any)._html as string) || "";

							$.make("tr", "#report_" + activity._id,
								$.make("td", "=" + activity._id),
								$.make("td", "=" + activity._parent),
								$.make("td", "=" + (activity.isHidden || activity.getParent().isHidden ? "hidden" : "")),
								$.make("td", "="
									+ (activity._flags & ICNodeFlags.DELETED ? "!" : "")
									+ (activity._flags & ICNodeFlags.IGNORE ? "?" : "")
									+ (html.includes("<audio") ? "a" : "")
									+ (html.includes("<ic-droppable") ? "d" : "")
									+ (html.includes("<ic-toggle") ? "x" : "")
									+ (html.includes("<img") ? "i" : "")
									+ (html.includes("<ic-select") ? "s" : "")
									+ (html.includes("<ic-text") ? "t" : "")
								),
								$.make("td", "=" + ICNodeTypeName[activity._type]),
								$.make("td", "=" + activity._name)
							).appendTo(tableBody);

							activity.getChildren().forEach(makeRow);
						}
					};

				console.log("report", activity, activity._id)
				makeRow(activity);
			});
	},
	function(activity) {
		return activity && activity.isGroup;
	}
);
