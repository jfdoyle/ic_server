///<reference path="../button.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

new Button("#ic_button_save", PERM.ROOT | PERM.MANAGE | PERM.NODE_CREATE | PERM.NODE_CONTENT | PERM.NODE_THEME,
	function(activity) {
		if (activity && Editor.change) {
			if (Editor.SourcePanel && Editor.SourcePanel.instance && Editor.SourcePanel.instance.errors) {
				alert("There is currently an error in the source, please fix it in order to save!");
				return;
			}
			if (Editor.currentActivity.is(ICNodeType.Organisation) && !/[&?]expert\b/.test(location.search)) {
				alert("Saving Organisations is currently disabled!");
				Editor.fileCache = {};
				Editor.change = false;
				Button.update();
				return;
			}
			var compile = 0,
				compileTheme = !!Editor.fileCache["theme.scss"],
				localCache: {[name: string]: string} = {},
				cachedTime = new Date().getTime(),
				fn = function() {
					ICNodes.load({
						action: "set_node",
						id: activity._id,
						parent: activity._parent,
						data: activity,
						files: Editor.fileCache
					}, function(activity) {
						console.log("Saved")
						Editor.fileCache = {};
						Editor.change = false;
						Button.update();
						if (activity) {
							activity.saving = false;
							if ($.tree.currentId !== String(activity._id)) { // Create - it will click on the activity after we return
								location.hash = String(activity._id);
							}
						}
					});
				}

			console.log("Saving: project(" + activity._parent + "), activity(" + activity._id + ") = ", activity);
			activity.saving = true;
			for (var name in Editor.fileCache) {
				if (name.endsWith(".scss")) {
					var scss = atob(Editor.fileCache[name]);

					if (name[0] === "_") {
						if (compileTheme) {
							continue;
						}
						compileTheme = true;
						name = "theme.scss";
						scss = "@import '" + name + "';";
					}
					compile++;
					console.log("Converting sass file:", name);
					(window as any).Sass.importer(function(request, done) {
						if (request.current) {
							var file: string = request.current,
								currentPath = Config.server + "assets/" + activity._id + "/";

							if (!file.endsWith(".scss")) {
								file += ".scss";
							}
							if (!file.includes("/assets/")) {
								// TODO: get the current included-by path instead of current activity path?
								file = currentPath + file;
							}
							if (file.includes(currentPath)) {
								// Do we have a local copy in the cache?
								var filename = file.replace(/^.*?\/assets\/\d+\//i, "");

								if (Editor.fileCache[filename]) {
									done({
										content: atob(Editor.fileCache[filename])
									});
									return;
								}
								if (localCache[filename]) {
									done({
										content: localCache[filename]
									});
									return;
								}
							}
							if (!file.startsWith("http")) {
								file = location.href.replace(/[^\/]*$/, "") + file;
							}
							$.ajax({
								url: file + "?_=" + cachedTime,
								dataType: "text"
							}).done(function(data) {
								localCache[name.replace(/^.*?\/assets\/\d+\//i, "")] = data;
								done({
									content: data
								});
							}).fail(function() {
								console.warn("Compiling scss, ajax error:", file, request);
								done({
									content: ""
								});
							});
							// console.log("Sass import", request, file);
						} else {
							console.warn("Compiling scss, unknown request:", request);
							done();
						}
					});
					(window as any).Sass.compile(scss, {
						style: (window as any).Sass.style.compressed
					}, function(result) {
						if (result.status) {
							console.warn("Sass compilation error:", result)
							alert("Sass compilation error:\n\n" + JSON.stringify(result));
						} else {
							var prefixed = result.text ? (window as any).autoprefixer.process(result.text) : {css: "", messages: []},
								messages = prefixed.messages;

							if (messages.length) {
								console.warn("Autoprefixer error:", messages.join("\n"))
								alert("Autoprefixer error:\n\n" + messages.join("\n"));
							} else {
								Editor.fileCache[name.replace(/\.scss$/i, ".css")] = btoa(prefixed.css);
							}
						}
						if (!--compile) {
							fn();
						}
					});
				}
			}
			if (!compile) {
				fn();
			}
		}
	},
	function(activity) {
		return activity && Editor.change;
	}
);

$(window).on("keydown", function(event) {
	if ((event.ctrlKey || event.metaKey) && /s/i.test(String.fromCharCode(event.which))) {
		$("#ic_button_save").click();
		event.preventDefault();
		return false;
	}
});
