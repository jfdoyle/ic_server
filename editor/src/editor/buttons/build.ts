///<reference path="../button.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */
declare function unescape(str: string): string;

interface Element {
	icIndexKeys: {
		"IC-SCREEN": number;
		"IC-ACTIVITIES": number;
		"IC-ACTIVITY": number;
		"IC-DROPPABLE": number;
		"IC-DRAGGABLE": number;
		"IC-SELECT": number;
		"IC-OPTION": number;
		"IC-TOGGLE": number;
		"IC-TEXT": number;
		[key: string]: number;
	};
	icIndex: number;
}

new Button("#ic_button_export", PERM.ROOT | PERM.MANAGE,
	function() {
		function build_activity(id: number, event: MouseEvent) {
			let activity = ICNodes.get(id),
				$row = $("#build_" + activity._id);

			if (event.ctrlKey) {
				console.info("Building all");
				$(event.target).closest("table").find(".fa-tasks").click();
				return;
			}
			activity.build().then(function(build) {
				let hadFirstScreen: boolean,
					treeWidgets: string[] = ["IC-SCREENS", "IC-SCREEN", "IC-ACTIVITIES", "IC-ACTIVITY", "IC-DROPPABLE", "IC-SELECT"],
					treeWidgetsSelector = treeWidgets.join(","),
					jsonWidgetsSelector = treeWidgets.wrap(",", "", "[data-json]"),
					screens = build.querySelectorAll("ic-screen").length,
					currentScreen = 0,
					styleScript = /^(style|script)$/i,
					walker = document.createNodeIterator(build, NodeFilter.SHOW_ELEMENT | NodeFilter.SHOW_TEXT | NodeFilter.SHOW_COMMENT, null, false);

				$row.addClass("disabled")
					.find("td:nth-child(2)")
					.addClass("disabled");
				/*
				 * First go down through the tree, fix and Json required, and
				 * make sure we have the correct indexes for tree widgets...
				 */
				for (let node = walker.nextNode(); node; node = walker.nextNode()) {
					switch (node.nodeType) {
						case Node.COMMENT_NODE:
							walker.previousNode();
							(node.parentElement || build).removeChild(node);
							break;

						case Node.TEXT_NODE:
							let textContent = node.textContent,
								parent = node.parentElement,
								text = textContent.replace(/%%(title|screens?)%%/g, function($0, $1) {
									switch ($1) {
										case "title":
											return activity._name;
										case "screen":
											return String(currentScreen);
										case "screens":
											return String(screens);
									}
								});

							// Compress text that's not in a style or script element
							if (parent && !styleScript.test(parent.nodeName) && parent.closest("IC-SCREEN")) {
								text = text.compress("uri");
							}
							if (textContent !== text) {
								node.textContent = text;
							}
							break;

						case Node.ELEMENT_NODE:
							let el = node as HTMLElement,
								tagName = el.tagName;

							el.removeAttribute("editor-img");
							el.removeAttribute("editor-title");
							el.removeAttribute("ice");
							el.removeAttribute("data-template");
							if (tagName.startsWith("IC-")) {
								if (!el.hasAttribute("data-json")) {
									el.setAttribute("data-json", "{}");
								}
								if (tagName === "IC-SCREEN") {
									if (!hadFirstScreen) {
										hadFirstScreen = true;
									} else {
										el.style.display = "none";
									}
									currentScreen++;
									el.setAttribute("x", ""); // To let us know it's been text compressed
								}
								if (el.hasAttribute("data-json")) {
									let closestTreeWidget = el.parentElement && el.parentElement.closest(treeWidgetsSelector);

									if (closestTreeWidget) {
										let indexKeys = closestTreeWidget.icIndexKeys;

										if (!indexKeys) {
											indexKeys = closestTreeWidget.icIndexKeys = {
												"IC-SCREEN": 1,
												"IC-ACTIVITIES": 1,
												"IC-ACTIVITY": 1,
												"IC-DROPPABLE": 1,
												"IC-DRAGGABLE": 1,
												"IC-SELECT": 1,
												"IC-OPTION": 1,
												"IC-TOGGLE": 1,
												"IC-TEXT": 1,
											};
										}
										indexKeys[tagName] = indexKeys[tagName] || 1;
										el.icIndex = indexKeys[tagName]++;
									}
								}
							} else if (tagName === "P" && /^p\d+$/.test(el.id)) {
								el.removeAttribute("id");
							}
							break;
					}
				}
				/*
				 * Then work back up the tree from the bottom, merging JSON up
				 * into its parents. If the json entry is empty, then simply
				 * remove it instead - entries are stored by index rather than
				 * in an array, so it is safe to do so.
				 * 
				 * Finally when an element has json but no parent element we can
				 * compress the json so it takes less space and becomes
				 * unreadable.
				 */
				for (let node = walker.previousNode(); node; node = walker.previousNode()) {
					switch (node.nodeType) {
						case Node.ELEMENT_NODE:
							let el = node as HTMLElement,
								tagName = el.tagName;

							if (tagName.startsWith("IC-") && el.hasAttribute("data-json")) {
								let closestTreeWidget = el.parentElement && el.parentElement.closest(jsonWidgetsSelector);

								if (closestTreeWidget) {
									let json = JSON.parse(el.getAttribute("data-json")),
										lcName = tagName.substring(3).toLowerCase();

									if (Object.keys(json).length) {
										let parentJson = JSON.parse(closestTreeWidget.getAttribute("data-json"));

										parentJson[lcName] = parentJson[lcName] || {};
										parentJson[lcName][el.icIndex] = json;
										closestTreeWidget.setAttribute("data-json", JSON.stringify(parentJson));
									}
									el.removeAttribute("data-json");
								} else {
									el.setAttribute("data-json", el.getAttribute("data-json").compress("uri"));
								}
							}
					}
				}
				let dom = createElement("html"),
					parent = build.querySelector("head") || build,
					meta = createElement("meta"),
					date = (Date as any).format ? "Infuze Creator (" + (Date as any).format("Y-m-d H:i") + ")" : "";

				meta.setAttribute("name", "creator");
				meta.setAttribute("content", "Infuze Limited");
				parent.insertBefore(meta, parent.firstChild);
				meta = createElement("meta");
				if (date) {
					meta.setAttribute("name", "generator");
					meta.setAttribute("content", date);
					parent.insertBefore(meta, parent.firstChild);
				}
				//<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
				if (!build.querySelector("meta[charset]")) {
					meta = createElement("meta");
					meta.setAttribute("charset", "utf-8");
					parent.insertBefore(meta, parent.firstChild);
				}
				/*
				 * Finally lets add our content to a temporary element, then
				 * check how much space we save... We need to do this to be able
				 * to get the .innerHTML source (a template doesn't supply it).
				 */
				dom.appendChild(build);

				let html = dom.innerHTML, //.unicode(true),
					length = html.length,
					compressed = html.compress("uri"),
					compressedLength = compressed.length;

				console.info("Building", id, ", length:", length, ", compressed:", compressedLength);

				Editor.ajax({
					type: "POST",
					url: "ajax.php",
					global: false,
					dataType: "json",
					data: {
						"PHP_SESSION_UPLOAD_PROGRESS": "upload",
						"action": "publish",
						"id": id,
						"html": compressed
					},
					success: function(data) {
						//console.log("Reply from server build", data);						
						Editor.ajax({
							type: "POST",
							url: "ajax.php",
							global: false,
							dataType: "json",
							data: {
								action: "get_builds",
								"id": id
							},
							success: showPrevious
						});
					}
				});
				$row.children("td:first-child").animate({foo: 100}, {
					duration: compressedLength / 5,
					step: function(percent) {
						percent = percent * 0.9 + 5; // Make sure we never completely fill as this is fake...
						$(this).css("background", "linear-gradient(to right,darkgreen " + percent + "%,#171717 " + (percent + 0.01) + "%)");
					},
					complete: function() {

					}
				});
			});
		}

		function addBuild(activity: ICNode) {
			let path = Config.server + "publish/" + String(activity._id).crc32();

			$.make("tr", "#build_" + activity._id,
				$.make("td", "colspan=3",
					$.make("i", "fa fa-minus"),
					$.make("label", "=" + activity._name)).on({
						click: function(this: HTMLElement) {
							$(this).closest("tr").next("tr.date").toggleClass("hidden");
							$(this).parent().find(".fa-chevron-right,.fa-chevron-down").toggleClass("fa-chevron-right").toggleClass("fa-chevron-down");
						}
					}),
				$.make("td",
					$.make("a", "class=fa fa-tasks").on("click", build_activity.bind(window, activity._id))),
				$.make("td",
					$.make("a", "href=" + path + "/", "target=_blank", "class=fa fa-external-link-square")),
				$.make("td",
					$.make("a", "href=" + path + ".zip", "download=" + activity._name + ".zip", "class=fa fa-download"))
			).appendTo($("#build_popup>table"));
		}

		function showPrevious(data: {[id: string]: string[]}) {
			//console.log("Reply from server build", data);
			Object.keys(data).forEach((id) => {
				let $row = $("#build_" + id),
					bestDate: Date,
					builds = data[id];

				if (builds.length) {
					let $tr = $("#build_" + id + "+.date"),
						name = $row.find("a[download]").attr("download").replace(".zip", "");

					$row.find(".fa-minus").removeClass("fa-minus").addClass("fa-chevron-right");
					if ($tr.length) {
						$tr.empty();
					} else {
						$tr = $.make("tr", "date hidden").insertAfter($row);
					}
					let $td = $.make("td", "colspan=6").appendTo($tr),
						$div = $.make("div").appendTo($td),
						$table = $.make("table").appendTo($div);

					builds.reverse().forEach(dateString => {
						let date = new Date(dateString),
							path = Config.server + "publish/" + String(id).crc32() + "." + date.format("Y-m-d_H-i-s");

						if (!bestDate || date > bestDate) {
							bestDate = date;
						}
						$.make("tr", "date",
							$.make("td", "colspan=4",
								$.make("label", "=" + date.format("D M d Y H:i:s"))),
							$.make("td",
								$.make("a", "href=" + path + "/", "target=_blank", "class=fa fa-external-link-square")),
							$.make("td",
								$.make("a", "href=" + path + ".zip", "download=" + name + "." + date.format("Y-m-d_H-i-s") + ".zip", "class=fa fa-download"))
						).appendTo($table);
					});
				}
				$row
					.removeClass("disabled")
					.attr("title", bestDate ? bestDate.format("D M d Y H:i:s") : "") //xhr.getResponseHeader('Last-Modified'))
					.children("td:first-child")
					.finish()
					.css({
						background: "",
						cursor: "pointer"
					});
			});
		}

		$().w2popup({
			title: "<i class=\"fa fa-cloud-download\"></i> Build",
			body: "<div id=\"build_popup\"><table><thead><tr><td colspan=3>&nbsp;</td><td>Build</td><td>Launch</td><td>Download</td></tr></thead><tbody></tbody></table></div>",
			style: "",
			width: 500,
			height: 500,
			onOpen: function() {
				ic.setImmediate("build", function() {
					let activity = ICNodes.get(Editor.currentActivity._id);

					if (activity.is(ICNodeType.Folder)) {
						activity.getChildren().forEach(addBuild);
					} else {
						addBuild(activity);
					}
					Editor.ajax({
						type: "POST",
						url: "ajax.php",
						global: false,
						dataType: "json",
						data: {
							action: "get_builds",
							"id": activity.is(ICNodeType.Folder) ? activity.getChildrenId() : activity._id
						},
						success: showPrevious
					});
				});
			}
		});
	},
	function(activity) {
		return !Editor.change && activity && (activity.is(ICNodeType.Project) || activity.is(ICNodeType.Folder) || activity.is(ICNodeType.Screen));
	}
);
