///<reference path="../button.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

new Button("#ic_button_advanced", PERM.ROOT,
	function() {
		let classList = document.body.classList,
			isAdvanced = classList.contains("advanced");

		Editor.setSetting("options.advanced", !isAdvanced);
		if (isAdvanced) {
			classList.remove("advanced");
		} else {
			classList.add("advanced");
		}
	},
	function() {
		return true;
	}
);
