///<reference path="../button.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

new Button("#ic_button_replicate", PERM.ROOT,
	function() {
		$().w2popup({
			title: "<i class=\"fa fa-hdd-o\"></i> Replicate",
			body: "<iframe id=\"replicate\" style=\"width:100%;height:99%;background:white;\" src=\"" + Config.server + "replicate.php\"></iframe>",
			style: "",
			width: 500,
			height: 300
		});
	},
	function(activity) {
		return Editor.permissions.isRoot && Config.replicate;
	}
);
