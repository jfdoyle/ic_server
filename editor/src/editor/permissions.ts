/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

const enum PERM {
	NONE = 0, // no permission - used for editor options etc

	ROOT = 1 << 0, // can edit and view everything everywhere
	OWNER = 1 << 1, // can edit and view everything in organisation
	MANAGE = 1 << 2, // can edit projects but not users in organisation
	VIEW = 1 << 3, // can view projects, but not edit them without further flags
	NODE_CREATE = 1 << 4, // can create nodes
	NODE_DELETE = 1 << 5, // can delete nodes
	NODE_CONTENT = 1 << 6, // can edit content
	NODE_THEME = 1 << 7, // can edit theme
	FILES = 1 << 8, // can edit and upload files

	ALL = (1 << 9) - 1, // Used as a shortcut elsewhere
	EDIT = ALL ^ PERM.VIEW, // Can do more than just view things
}

class Permission {
	private _raw: number;

	constructor(raw?: number) {
		this._raw = raw || 0;
	}

	set raw(raw: number) {
		this._raw = raw || 0;
	}

	get raw(): number {
		return this._raw;
	}

	private getBit(bit: number): boolean {
		return !!(this._raw & bit);
	}

	private setBit(bit: number, set: boolean) {
		if (set) {
			this._raw |= bit;
		} else {
			this._raw &= ~bit;
		}
	}

	get isRoot(): boolean {
		return this.getBit(PERM.ROOT);
	}

	set isRoot(val: boolean) {
		this.setBit(PERM.ROOT, val);
	}

	get isOwner(): boolean {
		return this.getBit(PERM.OWNER);
	}

	set isOwner(val: boolean) {
		this.setBit(PERM.OWNER, val);
	}

	get isManager(): boolean {
		return this.getBit(PERM.MANAGE);
	}

	set isManager(val: boolean) {
		this.setBit(PERM.MANAGE, val);
	}

	get canView(): boolean {
		return this.getBit(PERM.VIEW);
	}

	set canView(val: boolean) {
		this.setBit(PERM.VIEW, val);
	}

	get canCreate(): boolean {
		return this.getBit(PERM.NODE_CREATE);
	}

	set canCreate(val: boolean) {
		this.setBit(PERM.NODE_CREATE, val);
	}

	get canDelete(): boolean {
		return this.getBit(PERM.NODE_DELETE);
	}

	set canDelete(val: boolean) {
		this.setBit(PERM.NODE_DELETE, val);
	}

	get canContent(): boolean {
		return this.getBit(PERM.NODE_CONTENT);
	}

	set canContent(val: boolean) {
		this.setBit(PERM.NODE_CONTENT, val);
	}

	get canTheme(): boolean {
		return this.getBit(PERM.NODE_THEME);
	}

	set canTheme(val: boolean) {
		this.setBit(PERM.NODE_THEME, val);
	}

	get canEdit(): boolean {
		return this.getBit(PERM.EDIT);
	}

	set canFile(val: boolean) {
		this.setBit(PERM.FILES, val);
	}

	get canFile(): boolean {
		return this.getBit(PERM.FILES);
	}

	/**
	 * Checks if this has at least one of the permissions requested
	 */
	has(need: number | Permission): boolean {
		var perms: number = need instanceof Permission ? need._raw : (need as number);

		return (this._raw & perms) !== 0;
	}

	/**
	 * Checks that this has all of the permissions requested
	 */
	is(need: number | Permission): boolean {
		var perms: number = need instanceof Permission ? need._raw : (need as number);

		return (this._raw & perms) === need;
	}
};
