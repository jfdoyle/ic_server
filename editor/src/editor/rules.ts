///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface HTMLElement {
	icRule: Editor.Rule;
}

namespace Editor {
	export interface Rule {
		require: string[];
		permit: string[] | boolean;
	}

	export interface Rules {
		[tagName: string]: Rule
	}

	// Filled from EditPanel.setActivityHtml() when an activity.js script is found
	export var rules: Rules;

	export function getRule(el: HTMLElement): Rule {
		if (!el) {
			return;
		}
		if (el.icRule) {
			return el.icRule;
		}
		var walker = document.createNodeIterator(el, NodeFilter.SHOW_ELEMENT, null, false),
			rule: Rule = {
				require: [],
				permit: true
			};

		for (var node = walker.nextNode(); node; node = walker.nextNode()) {
			if (node.nodeName.startsWith("IC-")) {
				console.log("looking for ", node.nodeName, rules)
				var currentRule = rules[node.nodeName];

				if (currentRule) {
					var require = currentRule.require,
						permit = currentRule.permit;

					if (permit === true) {
						permit = Object.keys(rules);
					}
					if (node !== el) {
						for (var parent = node.parentElement; parent !== el; parent = parent.parentElement) {
							var nodeName = parent.nodeName;

							if (nodeName.startsWith("IC-")) {
								if (require && require.includes(nodeName)) {
									require = undefined;
								}
							} else if (permit === false || (isArray(permit) && !(permit as string[]).includes(nodeName))) {
								console.error("Invalid parent for widget", el, node);
								return undefined;
							}
						}
					}
					if (isArray(require)) {
						rule.require.pushOnce.apply(rule.require, require);
					}
					if (isArray(permit)) {
						if (isArray(rule.permit)) {
							rule.permit = (rule.permit as string[]).intersect(permit);
						} else {
							rule.permit = permit.slice();
						}
					}
				} else {
					console.error("Unable to find rule for '" + node.nodeName + "'", node, el);
				}
			}
		}
		return el.icRule = rule;
	}

	export function isValidWidget(el: HTMLElement, parent?: HTMLElement): boolean {
		var rule = getRule(el);

		if (rule) {
			var require = rule.require,
				permit = rule.permit;

			if (permit === true) {
				permit = Object.keys(rules);
			}
			if (!parent) {
				parent = el.parentElement;
			}
			for (; parent; parent = parent.parentElement) {
				var nodeName = parent.nodeName;

				if (nodeName.startsWith("IC-")) {
					if (require && require.includes(nodeName)) {
						require = undefined;
						permit = true;
					} else if (permit === false || (isArray(permit) && !(permit as string[]).includes(nodeName))) {
						//console.log("Cannot have parent", el.nodeName, nodeName)
						return false;
					}
				}
			}
			return !require || !require.length || require.includes("IC-SCREENS") || require.includes("IC-SCREEN");
		}
		return true;
	}
};
