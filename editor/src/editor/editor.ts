///<reference path="../config.ts" />
///<reference path="../node/node.ts" />
///<reference path="permissions.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

//$(document).ajaxStart(function() {
//	console.log("Sending AJAX", arguments)
//});
//$(document).ajaxStop(function() {
//	console.log("AJAX Reply", arguments)
//});

function fixPrototype(className: any, property?: string): void {
	var proto = className.prototype || {};

	if (property) {
		if (proto.hasOwnProperty(property)) {
			Object.defineProperty(proto, property, {"enumerable": false});
		}
	} else {
		for (property in proto) {
			fixPrototype(className, property);
		}
	}
}

namespace Editor {
	export var username: string;

	/**
	 * Logged in and online state
	 * TODO: ask for login again when this changes
	 */
	export var online: boolean;

	export var currentActivity: ICNode;
	export var findActivityId: number;
	export var rootId: number;
	export var change: boolean = false;
	export var fileCache: {[key: string]: string};

	export var permissions: Permission = new Permission(0);

	export var currentRulesPath: string;

	export var ctrlKey: boolean = false;

	function keyboardModifiers(event: KeyboardEvent) {
		Editor.ctrlKey = event.ctrlKey;
	}

	document.addEventListener("keydown", keyboardModifiers);
	document.addEventListener("keyup", keyboardModifiers);

	var ajaxLoggedOutData: JQueryAjaxSettings[] = [];

	function ajaxSuccessHandler(data) {
		var originalCall = this as JQueryAjaxSettings;

		if (isObject(data) && data.error === "Not logged in") {
			if (document.body.style.opacity !== "1") {
				window.location.href = Config.server;
			} else if (!$("#login_form").length) {
				ajaxLoggedOutData.push(originalCall);
				$("#ic_dialog").dialog("close");
				Editor.online = false;
				var $username: JQuery,
					$password: JQuery,
					doLogin = function() {
						console.log("login");
						$.post(Config.server + "ajax.php", {
							username: username ? username : $username.val(),
							password: $password.val()
						}, function() {
							$form.dialog("close").remove();
							$iframe.remove();
							Editor.online = true;
							Button.update();
							while (ajaxLoggedOutData.length) {
								Editor.ajax(ajaxLoggedOutData.shift());
							}
						});
					},
					$iframe = $.make("iframe", "src=/ajax.php", "#login_target", "name=login_target", "display:none"),
					$form = $.make("form", "#login_form", "name=login_form", "target=login_target", "autocomplete=on", "title=Login to Infuze Creator",
						$.make("table", "margin: 0 auto;",
							$.make("tbody",
								$.make(online, "tr",
									$.make(online, "td", "colspan=2", "text-align: left;", "=Your current session has been logged out and you need to login to continue.<br/><br/>")),
								$.make("tr",
									$.make("td", "text-align: right;", "=Email:"),
									$.make("td",
										$username = $.make("input", "type=email", "name=username", username ? "value=" + username : null, username ? "disabled=" : null).on("keyup", function(event) {
											if (event.which === 13) {
												$password.focus();
											}
										}))),
								$.make("tr",
									$.make("td", "text-align: right;", "=Password:"),
									$.make("td",
										$password = $.make("input", "type=password", "name=password").on("keyup", function(event) {
											if (event.which === 13) {
												$form.submit();
											}
										}))
								)
							)
						)
					);
				$iframe.appendTo("body");
				$form.appendTo("body").submit(doLogin).dialog({
					modal: true,
					buttons: {
						"Login": function() {
							$form.submit();
						}
					}
				});//.find("input:not(.ui-spinner-input)").uniform();
				$form.prev().children("button").remove();
			}
		} else {
			(originalCall.success as any).apply(window, arguments);
		}
	}

	export function preventDefault(event: JQuery.Event) {
		event.preventDefault();
	}

	export function ajax(settings?: JQueryAjaxSettings): Promise<any>;
	export function ajax(url: string, settings?: JQueryAjaxSettings): Promise<any>;
	export function ajax(url: string | JQueryAjaxSettings, settings?: JQueryAjaxSettings): Promise<any> {
		if (online !== false) {
			var newSettings: JQueryAjaxSettings = settings || url as JQueryAjaxSettings;

			if (Config.server) {
				if (isString(url)) {
					settings.url = url;
				} else {
					settings = url as any;
					url = undefined;
				}
				if (isObject(settings)) {
					newSettings = settings.clone();
					if (isString(newSettings.url)) {
						newSettings.url = Config.server + newSettings.url;
					}
					if (isFunction(newSettings.success)) {
						newSettings.success = ajaxSuccessHandler.bind(settings);
					}
				}
			}
			return $.ajax(newSettings) as any as Promise<any>;
		}
	}

	function resizeLayout() {
		if (SourcePanel.instance) {
			ic.setImmediate("resize_layout", function() {
				SourcePanel.instance.editor.resize();
				saveSettings();
			});
		}
	}

	export function setHtml(html: string) {
		// TODO: Check write permission
		if (currentActivity) {
			if (currentActivity.html !== html) {
				currentActivity.html = html.replace(/[\n\r\t]+/g, "");
			}
			if (currentActivity.changed) {
				Panel.trigger(PanelEvent.DOM_UPDATED);
				if (!Editor.change) {
					Editor.change = true;
					Button.update();
				}
			}
		} else {
			console.error("Error: Trying to update an activity without having one selected");
		}
	}

	export function checkWatchers() {
		if (currentActivity) {
			ajax({
				type: "POST",
				url: "ajax.php",
				global: false,
				dataType: "json",
				data: {
					"action": "set_current",
					"id": currentActivity._id
				},
				success: function(data) {
					if (!data.error) {
						currentActivity.watchers = data;
						Editor.EditPanel.instance.updateWatchers();
					}
				}
			});
		}
	}

	document.addEventListener("visibilitychange", checkWatchers);
	window.addEventListener("focus", checkWatchers);

	export function setActivity(activity: ICNode) {
		if (currentActivity) {
			if (currentActivity.$html) {
				// When clicking on the same activity again, clear any changes
				currentActivity.html = currentActivity.$html;
				currentActivity.changed = false;
			}
			if (currentActivity !== activity) {
				currentActivity.watchers.remove(username);
			}
		}
		currentActivity = activity;
		checkWatchers();
		if (currentActivity) {
			currentActivity.watchers.pushOnce(username);
		}
		change = false;
		Panel.trigger(PanelEvent.ACTIVITY);
		Panel.trigger(PanelEvent.DOM_UPDATED);
		change = false;
		Button.update();
	};

	/**
	 * Log a line to the footer
	 * @param {String} text
	 */
	export function log(text: string) {
		var $footer = $("#ic_footer");

		if ($footer.html() !== "") {
			$footer
				.velocity("stop", true as any)
				.velocity({
					colorAlpha: 0
				}, 50);
		}
		if (text !== "") {
			$footer
				.velocity({
					colorAlpha: 1
				}, {
					begin: function() {
						$footer.html(text); // So it queues after the old text is removed
					}
				})
				.velocity({
					colorAlpha: 0
				}, {
					delay: 10000,
					complete: function() {
						$footer.html("");
					}
				});
		}
	};

	/**
	 * Display a dialog to the user, oprionally asking for a reply
	 * @method dialog
	 * @param {string} text
	 * param {string} title
	 * param {?function} callback
	 * param {?boolean} error
	 */
	export function dialog(text: string, ...opts: (string | Function | boolean)[]) {
		var i, arg, title, callback, complete, error, string, $string,
			args = arguments;

		for (i = 1; i < args.length; i++) {
			arg = args[i];
			if (isString(arg)) {
				if (!title) {
					title = arg;
				} else {
					string = arg;
				}
			} else if (isBoolean(arg)) {
				error = arg;
			} else if (isFunction(arg)) {
				if (!callback) {
					callback = arg;
				} else {
					complete = arg;
				}
			}
		}

		if (isBoolean(callback)) {
			error = callback;
			callback = null;
		}
		if (!$("#login_form")[0]) {
			$("#ic_dialog").dialog("close");
			$.make("div", "#ic_dialog", "title=" + (title || "Confirm"), "font-weight:bold;font-size:2em;margin-top:0.2em;", error ? "border:1px solid red" : "",
				error ? $.make("span", "fa fa-exclamation-triangle", "float:left;color:red;") : null,
				$.make("span", "margin-left:0.5em;", "=" + text),
				isString(string) ? $string = $.make("textarea", "placeholder=" + string, "=" + string, "display:block;margin:0.5em auto;max-width:50vw;border:1px solid #ddd;padding: 0.5em 0.25em;") : null)
				.dialog({
					buttons: callback ? {
						"Okay": function() {
							if (callback.call(this, $string ? $string.val() : true) !== false && (!complete || complete.call(this) !== false)) {
								$(this).dialog("close");
							}
						},
						"Cancel": function() {
							if (!complete || complete.call(this) !== false) {
								$(this).dialog("close");
							}
						}
					} : {
							"Okay": function() {
								if (!complete || complete.call(this) !== false) {
									$(this).dialog("close");
								}
							}
						},
					close: function() {
						$(this).remove();
					},
					modal: true,
					draggable: true,
					resizable: false,
					width: "auto"
				});
			//		$dialog.find("input,textarea").uniform();
			if ($string) {
				$string.keyup();
			}
		}
	};

	export function fixTextAreaHeight() {
		var height = (this as HTMLTextAreaElement).scrollHeight;

		if (height) {
			$(this).innerHeight(height);
		}
	}

	export var panels: {[key: string]: HTMLElement} = {};
	var panelCount = 6;

	function loadPanel() {
		var container = (this as W2UI.W2Common).box as HTMLElement,
			side = container.parentElement.id.split("_").pop();

		panels[side] = container;
		if (!--panelCount) {
			ic.setImmediate("gui_loaded", gui_loaded);
		}
	}

	export var panel_layout: W2UI.W2Layout;

	export function startup() {
		console["log"]("Starting Infuze Creator...")

		panel_layout = $("#layout").w2layout({
			name: "layout",
			padding: 0,
			resizer: 6,
			onResize: resizeLayout,
			panels: [
				{
					type: "top",
					size: 107,
					overflow: "hidden",
					content: {
						render: loadPanel
					}
				},
				{
					type: "left",
					size: settings.size["left"] + "%",
					//					title: "&nbsp;",
					resizable: true,
					content: {
						render: loadPanel
					},
					tabs: {
						tabs: []
					}
				},
				{
					type: "main",
					//					title: "&nbsp;",
					overflow: "hidden",
					content: {
						render: loadPanel
					},
					tabs: {
						tabs: []
					}
				},
				{
					type: "preview",
					hidden: true, // Load hidden as settings and permissions can change this
					size: settings.size["preview"] + "%",
					//					title: "&nbsp;",
					resizable: true,
					content: {
						render: loadPanel
					},
					tabs: {
						tabs: []
					}
				},
				{
					type: "right",
					hidden: true, // Load hidden as settings and permissions can change this
					size: settings.size["right"] + "%",
					//					title: "&nbsp;",
					resizable: true,
					content: {
						render: loadPanel
					},
					tabs: {
						tabs: []
					}
				},
				{
					type: "bottom",
					size: 24,
					overflow: "hidden",
					content: {
						render: loadPanel
					}
				}
			]
		});
	}

	function startGUI() {
		Object.keys(Editor).forEach((className) => {
			var PanelClass: {new(...args: any[]): Panel;} = Editor[className];

			if (PanelClass && PanelClass.prototype && PanelClass.prototype instanceof Panel) {
				//console.log("Creating new", className)
				try {
					new PanelClass();
				} catch (error) {
					if (error !== "No permission") {
						throw error;
					}
				}
			}
		});
		Panel.startup();
		$(".w2ui-panel-tabs tr").sortable({
			connectWith: ".w2ui-panel-tabs tr",
			containment: "document",
			distance: 10,
			forcePlaceholderSize: true,
			helper: function(event, element) {
				var helper = createElement("span");

				helper.innerText = (element as any as HTMLElement[])[0].innerText.trim();
				helper.id = "ic_drag_panel";
				document.body.appendChild(helper);
				return helper;
			},
			items: ">td:not([width])",
			placeholder: "ic-drag-panel-target",
			revert: "invalid",
			scroll: false,
			start: function() {
				$("iframe").css("pointer-events", "none");
			},
			stop: function() {
				$("iframe").css("pointer-events", "");
			},
			update: function(event, ui) {
				saveSettings();
				window.location.reload(false);
			}
		});

		Panel.trigger(PanelEvent.STARTUP);

		//$("#ic_footer").addClass("ui-widget ui-widget-content");

		$("#aba-button-logout").on("click", function() {
			window.location.href = Config.server + "logout.php";
		});
		//		$(".aba-button").on("click", Button.onClickHandler);

		$(document).on("change keyup keydown paste cut", "textarea", fixTextAreaHeight);
		//$("#buttons").on("mouseenter", "[data-tooltip]", function(event) {
		//	log($(this).attr("data-tooltip"));
		//});
	}

	function gui_loaded() {
		ajax({
			type: "POST",
			url: "ajax.php",
			global: false,
			dataType: "json",
			data: {
				"action": "get_group"
			},
			success: function(data) {
				if (data.error) {
					return dialog("Error: " + data.error, "Server Error", true);
				}
				//console.log("group:", data);
				permissions.raw = data.permission;
				if (permissions.has(PERM.ROOT | PERM.FILES)) {
					var body = document.body,
						drop = getElementById("file_drop"),
						counter = 0,
						show_drop = function() {
							if (!counter++) {
								drop.classList.add("show");
							}
						},
						hide_drop = function() {
							if (!--counter) {
								drop.classList.remove("show");
							}
						};

					body.addEventListener("dragenter", show_drop, false);
					body.addEventListener("dragleave", hide_drop, false);
					body.addEventListener("drop", hide_drop, false);
				}
				Config.backup = !!data.backup;
				Config.replicate = !!data.replicate;
				startGUI();
				$("#aba-text-account")
					.text(username = data.email)
				//	.on("click", clickAccount);
				$("#aba-gravatar").attr("src", "https://www.gravatar.com/avatar/" + username.trim().toLowerCase().md5() + ".jpg?s=24&d=identicon");
				rootId = data[0].node;
				findActivityId = (location.hash.regex(/^#([0-9]+)/) as number) || rootId;
				// TODO: better way of doing this to prevent duplicate requests?
				var i: string,
					nodes = permissions.isRoot ? [0] : [];
				for (i in data) {
					if (/^\d+$/.test(i)) {
						nodes.push(data[i].node);
					}
				}
				if (!!Editor.getSetting("options.advanced")) {
					document.body.classList.add("advanced");
				}
				ICNodes.loadParents(findActivityId);
				ICNodes.load(nodes);
				console.log("root", permissions, nodes)
				online = true;
				Panel.trigger(PanelEvent.LOADED);
			}
		});
		document.body.style.opacity = "1";
	}
}
