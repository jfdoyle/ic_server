/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface HTMLElement {
	uiLineOptions: ic.DrawLineData;
	uiLines: HTMLElement[];
}

namespace ic {
	export interface DrawLineOptions {
		options?: Object;
		source?: HTMLElement;
		corners?: boolean;
		from?: string;
		to?: string;
		replace?: boolean;
	}

	export interface DrawLineData {
		target: HTMLElement;
		parent: HTMLElement;
		from: string;
		to: string;
		corners: boolean;
	}

	export var drawLineOptions: DrawLineOptions = {
		source: null,
		corners: true,
		from: "center",
		to: "center",
		replace: true
	};

	/**
	 * Draw a line between two elements
	 */
	export function drawLine(from: HTMLElement | HTMLElement[], target: "destroy" | "update" | HTMLElement | HTMLElement[], options?: DrawLineOptions) {
		var fromList = (from as HTMLElement[]).length ? (from as HTMLElement[]) : [(from as HTMLElement)],
			get_relative = function(from_pos: number, from_size: number, to_pos: number, to_size: number, small: string, big: string, corners: boolean) {
				if (from_pos + (corners ? from_size / 4 : 0) > to_pos + to_size) {
					return small;
				}
				if (from_pos + (corners ? from_size / 4 * 3 : from_size) < to_pos) {
					return big;
				}
				return "center";
			},
			get_pos = function(from: HTMLElement, to: HTMLElement, relative: string, corners: boolean, offset: ClientRect) {
				var rect_from = from.getBoundingClientRect(),
					rect_to = to.getBoundingClientRect(),
					rel = relative.split(" ");

				rel[1] = rel[1] || rel[0];
				if (rel[0] === "auto") {
					rel[0] = get_relative(rect_from.left, rect_from.width, rect_to.left, rect_to.width, "left", "right", corners);
				}
				if (rel[1] === "auto") {
					rel[1] = get_relative(rect_from.top, rect_from.height, rect_to.top, rect_to.height, "top", "bottom", corners);
				}
				if (!corners && !rel.includes("center")) {
					rel[rect_from.height > rect_from.width ? 0 : 1] = "center";
				}
				var pos = {
					left: (rel[0] === "left" ? rect_from.left : rel[0] === "right" ? rect_from.right : (rect_from.width / 2 + rect_from.left)) - offset.left,
					top: (rel[1] === "top" ? rect_from.top : rel[1] === "bottom" ? rect_from.bottom : (rect_from.height / 2 + rect_from.top)) - offset.top
				};
				return pos;
			},
			update_line = function(line: HTMLElement) {
				if (!line.offsetParent) {
					return;
				}
				var opts = line.uiLineOptions,
					parent_rect = line.offsetParent.getBoundingClientRect(),
					from = get_pos(opts.parent, opts.target, opts.from, opts.corners, parent_rect),
					to = get_pos(opts.target, opts.parent, opts.to, opts.corners, parent_rect),
					css = getComputedStyle(line),
					style = line.style;


				style.top = (from.top - parseFloat(css.paddingTop)) + "px";
				style.left = (from.left - parseFloat(css.paddingLeft)) + "px";
				style.width = Math.sqrt(Math.pow(from.left - to.left, 2) + Math.pow(from.top - to.top, 2)) + "px";
				style.transform = "rotate(" + Math.atan2(to.top - from.top, to.left - from.left) + "rad)";
			};

		if (target === "destroy") {
			return fromList.forEach(function(el) {
				var opts = el.uiLineOptions;

				if (opts) {
					// We're a line, so remove from parent
				} else if (el.uiLines) {
					// We're a parent, so remove all lines
					el.uiLines.forEach(function(line) {
						line.parentElement.removeChild(line);
					});
					delete el.uiLines;
				}
			});
		}
		if (!target || target === "update") {
			return fromList.forEach(function(el) {
				var opts = el.uiLineOptions;

				if (opts) {
					// We're a line, so update
					update_line(el);
				} else if (el.uiLines) {
					// We're a parent, so update all lines
					drawLine(el.uiLines, "update");
				}
			});
		}
		if (target instanceof HTMLElement || target.length) {
			var targetList = (target as HTMLElement[]).length ? (target as HTMLElement[]) : [(target as HTMLElement)],
				opts: DrawLineOptions = drawLineOptions.clone((options || {}).clone(), true);

			return fromList.forEach(function(el) {
				var i, j, dest,
					line: HTMLElement,
					source = opts.source || el,
					lines = el.uiLines || [];

				if (opts.replace && lines.length) {
					lines.forEach(function(line) {
						line.parentElement.removeChild(line);
					});
					lines = [];
				}
				for (i = 0; i < targetList.length; i++) {
					dest = targetList[i];
					for (j = 0; j < lines.length; j++) {
						line = lines[j];
						if (line.uiLineOptions.target === dest) {
							break;
						}
					}
					if (j === lines.length) {
						line = document.createElement("IC-LINE");
						line.className = "ict-line";
						line.uiLineOptions = {
							target: dest,
							from: opts.from,
							parent: source,
							to: opts.to,
							corners: opts.corners
						};
						lines.push(line);
						el.uiLines = lines;
						el.appendChild(line);
					}
					update_line(line);
				}
			});
		}
		console.error("drawLine called with bad options!", arguments);
		return this;
	};

	window.addEventListener("resize", function() {
		[].forEach.call(document.getElementsByTagName("ic-line"), function(el: HTMLElement) {
			drawLine(el, "update");
		});
	});
}