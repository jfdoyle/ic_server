///<reference path="../../activity/src/version.ts" />
/**
 * Default JSON for activities
 * Nesting is "classNames" -> "tagName" -> "ic-behaviour" / "state"
 */
namespace Editor {
	export var support: { [attribute: string]: number } = ic.version;

	export var canHaveState = Object.keys(support).filter(function(name) {
		return /^ic-/.test(name);
	}).join(",");
}
