///<reference path="permissions.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

class Button {
	private static list: Button[] = [];

	constructor(
		public selector: string,
		public perm: PERM,
		public click: (activity?: ICNode, event?: JQuery.Event) => boolean | void,
		public enable: (activity?: ICNode) => boolean
	) {
		Button.list.push(this);
		$(document).on("click", selector, this as any, Button.onClick);
	}

	static update() {
		Button.list.forEach(function(button) {
			var $button = $(button.selector);

			if (button.perm && !Editor.permissions.has(button.perm)) {
				$button.hide();
			} else {
				$button.toggleClass("disabled", !button.enable(Editor.currentActivity));
			}
		});
	}

	/**
	 * Call an event handler with the current handler as "this"
	 * @param {Event} event
	 */
	private static onClick(event: JQuery.Event) {
		var $this = $(this),
			button = event.data as Button,
			activity = Editor.currentActivity;

		if (button.enable(activity)) {
			if ($this.hasClass("radio")) {
				if ($this.hasClass("active")) {
					return false;
				}
				$this.addClass("active").siblings().removeClass("active");
			}
			button.click(activity, event);
		}
	}
}
