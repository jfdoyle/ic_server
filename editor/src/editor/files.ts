///<reference path="permissions.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

class ActivityFile {
	private static list: ActivityFile[] = [];

	private static mimetypes: { [ext: string]: string } = {
		"css": "text/css",
		"scss": "text/scss",
		"htm": "text/html",
		"html": "text/html",
		"js": "text/javascript",
		"bmp": "image/bmp",
		"gif": "image/gif",
		"jpg": "image/jpeg",
		"jpeg": "image/jpeg",
		"png": "image/png",
		"svg": "image/svg+xml",
		"m4a": "audio/mpeg",
		"mp3": "audio/mpeg3",
		"avi": "video/avi",
		"m4v": "video/mpeg",
		"mov": "video/quicktime",
	};

	name: string;
	nameServer: string; // Name on the server for renaming
	size: number;
	type: string;
	data: string;
	deleted: boolean;

	constructor(
		file: string | File
	) {
		ActivityFile.list.push(this);
		if (file instanceof File) {
			this.name = file.name;
			this.size = file.size;
			this.type = file.type;
		} else {
			this.name = file as string;
		}
		this.nameServer = this.name;
		this.type = this.type || ActivityFile.mimetypes[this.name.regex(/\.([^\.]+)$/) as string] || "application/octet-stream";
	}

	delete(cancel?: boolean) {
		this.deleted = !cancel;
	}

	rename(newName: string) {
		var old = ActivityFile.find(newName);

		if (old) {
			old.delete();
		}
		this.name = newName;
	}

	static clear() {
		this.list.length = 0;
	}

	static find(name: string): ActivityFile {
		var i,
			file: ActivityFile,
			list = this.list

		for (i = 0; i < list.length; i++) {
			file = list[i];
			if (!file.deleted && file.name === name) {
				return file;
			}
		}
		return null;
	}

	static get(name: string): string {
		var file = this.find(name);

		if (file.data) {
			return "data:" + file.type + ";base64," + file.data;
		}
		return Config.server + "asstes/" + Editor.currentActivity._id + "/" + file.nameServer;
	}
}
