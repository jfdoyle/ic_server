/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace Editor.ui.resources {

	interface FileProps {
		path: string;
		data?: string;
	};

	function getFileType(path: string): MediaType {
		switch ((path.regex(/\.([^\.]+)$/) as string).toLowerCase()) {
			case "gif":
			case "jpg":
			case "png":
			case "svg":
				return MediaType.IMAGE;

			case "mp3":
			case "m4a":
			case "ogg":
				return MediaType.AUDIO;

			case "mp4":
			case "m4v":
				return MediaType.VIDEO;
		}
		return MediaType.UNKNOWN;
	}

	export class File extends React.Component<FileProps, {}> {
		render() {
			let props = this.props,
				path = props.path,
				data = props.data,
				filePath = "../" + path,
				fileName = path.regex(/([^\/]+)$/) as string,
				fileExt = (fileName.regex(/\.([^\.]+)$/) as string).toLowerCase(),
				fileType = getFileType(path),
				url = data || encodeURI(filePath).replace(/'/g, "%27"),
				className = fileType === MediaType.IMAGE
					? "image"
					: fileType === MediaType.AUDIO
						? "audio"
						: "video";

			return fileType === MediaType.UNKNOWN
				? null
				: (
					<a ref={"resource_file_" + path} onClick={event => event.preventDefault()} download href={url} target="_blank" className={"media defer " + className} title={filePath}>
						{
							fileType === MediaType.IMAGE
								? <div className={"fa fa-file-image-o"} data-type={fileExt} style={{backgroundImage: "url('" + url + "')"}}></div>
								: fileType === MediaType.AUDIO
									? <div className="fa fa-file-audio-o" data-type={fileExt}><audio src={url} controls></audio></div>
									: <div className="fa fa-file-audio-o" data-type={fileExt}><video src={url} controls></video></div>
						}
						<h3>
							<span>
								{path.regex(/(\d+)/) + "/"}
							</span>
							{fileName.replace(/\.[^\.]+$/, "")}
							<span>
								{"." + fileExt}
							</span>
						</h3>
					</a>
				);
		}
	}
}
