/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace Editor.ui.resources {
	interface FolderProps {
		id: number;
	};

	export class Folder extends React.Component<FolderProps, {}> {
		render() {
			let props = this.props,
				id = props.id;

			return (
				<div>
					<h3>
						<span>
							{id + "/"}
						</span>
						{ICNodes.get(id)._name || id}
					</h3>
				</div>
			);
		}
	}
}
