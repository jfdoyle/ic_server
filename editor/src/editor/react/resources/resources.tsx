/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace Editor.ui.resources {

	export function createPanel(parent: HTMLElement, onlyType?: Type) {
		Editor.currentActivity.getTemplateFolders().then(function(nodes) {
			nodes.pushOnce.apply(nodes, Editor.currentActivity.getParents(true));
			nodes.pushOnce.apply(nodes, Editor.currentActivity.getSiblings());
			ReactDOM.render(<Resources nodes={nodes} type={onlyType} />, parent);
		});
	}

	const enum List {
		THUMBS,
		ICONS,
		LIST,
		DETAILS
	}

	const ListName = {
		[List.THUMBS]: "thumbs",
		[List.ICONS]: "icons",
		[List.LIST]: "list",
		[List.DETAILS]: "details"
	}

	const ListEnum: {[name: string]: List} = {
		"thumbs": List.THUMBS,
		"icons": List.ICONS,
		"list": List.LIST,
		"details": List.DETAILS
	}

	const enum Type {
		ALL,
		IMAGE,
		AUDIO,
		VIDEO
	}

	const TypeName = {
		[Type.ALL]: "all",
		[Type.IMAGE]: "image",
		[Type.AUDIO]: "audio",
		[Type.VIDEO]: "video"
	}

	const TypeEnum: {[name: string]: Type} = {
		"all": Type.ALL,
		"image": Type.IMAGE,
		"audio": Type.AUDIO,
		"video": Type.VIDEO
	}

	const enum Sort {
		ALPHA,
		TREE
	}

	const SortName = {
		[Sort.ALPHA]: "alpha",
		[Sort.TREE]: "tree"
	}

	const SortEnum: {[name: string]: Sort} = {
		"alpha": Sort.ALPHA,
		"tree": Sort.TREE
	}

	interface ResourcesProps {
		nodes: ICNode[];
		type?: Type;
	};

	interface ResourceState {
		list: List;
		sort: Sort;
		type: Type;
		info: boolean,
	}

	export class Resources extends React.Component<ResourcesProps, ResourceState> {
		constructor(props: ResourcesProps) {
			super(props);
			this.state = {
				list: ListEnum[getSetting("options.resource_list", ListName[List.THUMBS])],
				sort: SortEnum[getSetting("options.resource_sort", SortName[Sort.ALPHA])],
				type: TypeEnum[getSetting("options.resource_type", TypeName[Type.ALL])],
				info: getSetting("options.resource_ext", "ext") === "ext"
			}
		}

		setList(value: List) {
			this.setState({list: value});
			setSetting("options.resource_list", ListName[value]);
			saveSettings();
		}

		setSort(value: Sort) {
			this.setState({sort: value});
			setSetting("options.resource_sort", SortName[value]);
			saveSettings();
		}

		setType(value: Type) {
			this.setState({type: value});
			setSetting("options.resource_type", TypeName[value]);
			saveSettings();
		}

		toggleInfo() {
			let info = this.state.info;

			this.setState({info: !info});
			setSetting("options.resource_ext", info ? "" : "ext");
			saveSettings();
		}

		render() {
			let props = this.props,
				state = this.state,
				stateList = state.list,
				stateType = state.type,
				stateSort = state.sort,
				stateInfo = state.info,
				nodes = props.nodes;

			console.log("nodes", nodes)

			return (
				<div className={"resources " + ListName[stateList] + " " + TypeName[props.type || stateType] + " " + SortName[stateSort] + " " + (stateInfo ? "ext" : "")} >
					<span>
						<span>
							<i onClick={() => {this.setList(List.THUMBS);}} className={"fa fa-th-large" + (stateList === List.THUMBS ? " active" : "")}></i>
							<i onClick={() => {this.setList(List.ICONS);}} className={"fa fa-th" + (stateList === List.ICONS ? " active" : "")}></i>
							<i onClick={() => {this.setList(List.LIST);}} className={"fa fa-th-list" + (stateList === List.LIST ? " active" : "")}></i>
							<i onClick={() => {this.setList(List.DETAILS);}} className={"fa fa-list" + (stateList === List.DETAILS ? " active" : "")}></i>
						</span>
						<span style={props.type ? {display: "none"} : {}} >
							<i onClick={() => {this.setType(Type.ALL);}} className={"fa fa-circle-thin" + (stateType === Type.ALL ? " active" : "")}></i>
							<i onClick={() => {this.setType(Type.IMAGE);}} className={"fa fa-camera" + (stateType === Type.IMAGE ? " active" : "")}></i>
							<i onClick={() => {this.setType(Type.AUDIO);}} className={"fa fa-volume-up" + (stateType === Type.AUDIO ? " active" : "")}></i>
							<i onClick={() => {this.setType(Type.VIDEO);}} className={"fa fa-video-camera" + (stateType === Type.VIDEO ? " active" : "")}></i>
						</span>
						<span>
							<i onClick={() => {this.setSort(Sort.ALPHA);}} className={"fa fa-sort-alpha-asc" + (stateSort === Sort.ALPHA ? " active" : "")}></i>
							<i onClick={() => {this.setSort(Sort.TREE);}} className={"fa fa-sort-amount-asc" + (stateSort === Sort.TREE ? " active" : "")}></i>
						</span>
						<span data-select="resource_ext">
							<i onClick={() => {this.toggleInfo();}} className={"fa fa-info" + (stateInfo ? " active" : "")}></i>
						</span>
						<div>
							<input id="resource_search" placeholder="filter" type="text" />
						</div>
						<label className="fa fa-folder-open">
							<input type="file" />
						</label>
					</span>
					<div>
						{nodes.map(node => {
							return !(node._flags & (ICNodeFlags.DELETED | ICNodeFlags.IGNORE)) && node._files && node._files.length
								? [
									<Folder id={node._id} />,
									node._files
										? node._files.map(file => <File path={file} />)
										: null
								]
								: null;
						})}
					</div>
				</div >
			);
		}
	}
}
