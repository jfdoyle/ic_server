///<reference path="editor.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Settings {
	size: {
		left: number;
		preview: number;
		right: number;
	};
	panel: {
		left: string[];
		main: string[];
		preview: string[];
		right: string[];
	},
	active: {
		left: string;
		main: string;
		preview: string;
		right: string;
	},
	projects?: {
		files?: boolean;
		recycle?: boolean;
	},
	options?: {
		image_list?: boolean;
		resource_list?: string;
		resource_type?: string;
		resource_sort?: string;
		resource_ext?: string;
		preview_marks?: boolean;
		advanced?: boolean;
	}
}

namespace Editor {

	var defaultSettings: Settings = {
		"size": {
			"left": 15,
			"preview": 25,
			"right": 15
		},
		"panel": {
			"left": [
				"projects",
				"users",
				"components"
			],
			"main": [
				"preview",
				"edit"
			],
			"preview": [
				"source",
				"explore",
				"properties"
			],
			"right": [
				"marking"
			],
		},
		"active": {
			"left": "projects",
			"main": "preview",
			"preview": "source",
			"right": "properties"
		},
		"projects": {
			"files": false,
			"recycle": false
		},
		"options": {
			"image_list": false,
			"resource_list": "thumbs",
			"resource_type": "all",
			"resource_sort": "alpha",
			"resource_ext": "",
			"preview_marks": false,
			"advanced": false
		}
	};

	export var settings: Settings = {} as Settings;

	export function getSetting(path: string | string[], def?: any): any {
		return settings.getTree(path, def);
	}

	export function setSetting(path: string | string[], value: any) {
		ic.setImmediate("saveSettings", saveSettings);
		return settings.setTree(path, value);
	}

	export function loadSettings() {
		var data = JSON.parse(localStorage.getItem("ic-config") || "{}") as Settings;

		if (typeof data === "object" && Object.keys(data).equals(Object.keys(defaultSettings))) {
			settings = data;
		} else {
			settings = $.extend(true, {}, defaultSettings);
		}
		return settings;
	};

	export function saveSettings() {
		ic.setImmediate("saveSettings");

		settings.size["left"] = parseInt(Editor.panel_layout.get("left").size as string);
		settings.size["preview"] = parseInt(Editor.panel_layout.get("preview").size as string);
		settings.size["right"] = parseInt(Editor.panel_layout.get("right").size as string);

		["left", "main", "preview", "right"].forEach(function(side) {
			settings.panel[side] = [];
			settings.active[side] = "";
			$("#layout_layout_panel_" + side + ">.w2ui-panel-tabs .w2ui-tab").each(function(i, el) {
				var panel = (this as HTMLElement).innerText.trim().toLowerCase();

				settings.panel[side].push(panel);
				if (!i || (this as HTMLElement).classList.contains("active")) {
					settings.active[side] = panel;
				}
			});
		});

		localStorage.setItem("ic-config", JSON.stringify(settings));
	};

	loadSettings();
};
