///<reference types="ace" />
///<reference types="jquery" />
///<reference types="jqueryui" />
///<reference types="jquery.ui.layout" />
///<reference types="velocity-animate" />
///<reference types="modernizr" />
///<reference types="w2ui" />
///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

window.addEventListener("options-test", null, Object.defineProperty({}, "capture", {
	get: function() {
		var oldEventListener = document.addEventListener;

		if (!(oldEventListener as any).isTouched) {
			var addEventListenerTouch = function(eventType: string, listener: any, options: boolean | {[key: string]: any}, wantsUntrusted?: boolean) {
				switch (eventType) {
					case "touchstart":
					case "touchmove":
					case "touchend":
						if (options === true || options === false) {
							options = {
								capture: options
							}
						} else if (!options) {
							options = {};
						}
						if (!options.passive) {
							options.passive = false;
						}
						break;
				}
				// Just in case the spec changes to allow its return to be used and this patch is still in use...
				return oldEventListener.call(this, eventType, listener, options, wantsUntrusted);
			};

			(addEventListenerTouch as any).isTouched = true;
			Object.defineProperty(document, "addEventListener", {
				value: addEventListenerTouch,
				configurable: true
			});
			Object.defineProperty(document.body, "addEventListener", {
				value: addEventListenerTouch,
				configurable: true
			});
		}
	}
}));

$(Editor.startup);
