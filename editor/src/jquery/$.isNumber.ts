///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQueryStatic {
    isNumber(str: any): boolean;
}

/**
 * Check if a variable is a number
 * @static
 * @method isNumber
 * @for jQuery
 * @param {*} num
 * @return {boolean}
 */
$.isNumber = function(num) {
	return typeof num === "number";
};
