///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQueryStatic {
    explode(path: string[]): string[];
    explode(path: string): string[];
}

/**
 * Explode a dot.separated.path into an array if necessary
 * @static
 * @method explode
 * @for jQuery
 * @param {String|Array} path Path to explode
 * @return {Array}
 */
$.explode = function(path: string | string[]) {
	var i,
		arr: string[] = [];

	if (path && $.isArray(path)) {
		arr = (path as string[]).slice(0);
	}
	if ($.isString(path)) {
		arr = (path as string).split(".");
		for (i = 0; i < arr.length - 1; i++) {
			if (arr[i].substr(-1) === "\\") {
				arr[i] = arr[i].substr(0, arr[i].length - 1) + "." + arr.splice(i + 1, 1);
				i--;
			}
		}
	}
	for (i = 0; i < arr.length; i++) {
		if (!/[^\d]/.test(arr[i])) {
			arr[i] = "" + parseInt(arr[i], 10);
		}
	}
	return arr;
};
