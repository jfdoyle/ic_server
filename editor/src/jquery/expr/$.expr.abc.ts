///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

/**
 * Pseudo-selector that finds elements with jQuery data attached
 * @method :abc
 * @for jQuery
 * @param {type} el
 * @param {type} index
 * @param {type} selector
 * @return {boolean}
 */
($ as any).expr.pseudos.abc = function(el, index, selector) {
	if (!selector[3]) {
		return $.hasData(el);
	}
	var attr = selector[3].split("=");
	if (attr.length === 1) {
		return $(el).data(attr[0]) !== undefined;
	}
	// Deliberate loose typing
	return $(el).data(attr[0]) == attr[1];
};
