///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQueryStatic {
	attr(el: HTMLElement, arre: string): any;
}

/**
 * Pseudo-selector that finds elements capable of having focus
 * @method :focusable
 * @for jQuery
 * @param {jQuerySelector} el
 * @return {boolean}
 */
($ as any).expr.pseudos.focusable = function(el: HTMLInputElement | HTMLAnchorElement) {
	var nodeName = el.nodeName.toLowerCase(),
		tabIndex = $.attr(el, "tabindex");

	return (/input|select|textarea|button|object/.test(nodeName) || $.attr(el, "contenteditable")
		? !(el as HTMLInputElement).disabled
		: "a" === nodeName || "area" === nodeName
			? (el as HTMLAnchorElement).href || !isNaN(tabIndex)
			: !isNaN(tabIndex))
		// the element and all of its ancestors must be visible
		// the browser may report that the area is hidden
		&& ("area" === nodeName ? $(el).parents(":visible") : $(el).closest(":visible")).length;
};
