///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

/**
 * Pseudo-selector that finds styled elements
 * @method :css
 * @for jQuery
 * @param {type} el
 * @param {type} index
 * @param {type} selector
 * @return {boolean}
 */
($ as any).expr.pseudos.css = function(el, index, selector) {
	var $el = $(el), attr;
	if (!selector[3]) {
		return !!$el.attr("style");
	}
	attr = selector[3].split("=");
	if (attr.length === 1) {
		return $el.css(attr[0]) !== $el.parent().css(attr[0]);
	}
	// Deliberate loose typing
	return $el.css(attr[0]) == attr[1];
};
