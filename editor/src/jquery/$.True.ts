///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQueryStatic {
    True(): boolean;
}

/**
 * Same as $.noop, but always returns true
 * @static
 * @method false
 * @for jQuery
 * @return {true}
 */
$.True = function() {
	return true;
};
