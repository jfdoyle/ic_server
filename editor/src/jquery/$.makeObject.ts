/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQueryStatic {
    makeObject(...pairs: any[]): {};
}

/**
 * Create an object from key/value pairs
 * @static
 * @method makeObject
 * @for jQuery
 * @param {string} key
 * @param {*} value
 * @return {Object}
 */
$.makeObject = function(...pairs: any[]) {
	var i, obj = {}, args = [].slice.call(arguments);
	for (i = 0; i < args.length; i++) {
		obj[args[i++]] = args[i];
	}
	return obj;
};
