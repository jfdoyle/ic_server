///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQueryStatic {
	stylesheet(selectors: string | string[] | Object, rules?: string | string[] | {[s: string]: string}, contect?: string): string;
	camelCase(key: string): string;
}

(function() {

	var globalRules: {[s: string]: string};
	var globalStylesheet: JQuery; //HTMLStyleElement;


	/**
	 * Create css stylesheet code on the fly - this allows extended selectors like :hover etc that normal .css doesn't support.
	 * @static
	 * @method stylesheet
	 * @for jQuery
	 * @chainable
	 * @param {String|Array} selectors Any css style selector, or a full stylesheet text string (if no rules option)
	 * @param {String|Array} [rules] A string or array of styles
	 * @param {string} [context] A selector that is prepended to this
	 */
	$.stylesheet = function(selectors: string | string[] | {[rule: string]: string}, rules: string | string[] | {[rule: string]: string}, context?: string) {
		var i: any,
			tmp: any[],
			selector: string,
			createRule = function($0: string, $1: string, $2: string) {
				(rules as {[rule: string]: string})[$1] = $2;
			};

		// Only works if we have globalStylesheet access, so create here or forever fail
		if (!globalStylesheet) {
			globalStylesheet = $("<style style=\"text/css\"/>").appendTo("head");
			// List of stylesheet rules = "key: {css: value, css: value}"
			globalRules = {};
		}

		if (globalStylesheet) {
			if (!rules) {
				if (isString(selectors) && /\{.*?\}/.test(selectors as string)) {
					// If we're passing a single string of css then split it and parse them separately
					selectors = (selectors as string).regex(/\s*(.*?)\s*\{([^\}]*)\}/g) as string[];
					for (i = 0; i < selectors.length; i++) {
						$.stylesheet(selectors[i][0], selectors[i][1], context);
					}
				} else if ($.isPlainObject(selectors)) {
					for (i in (selectors as Object)) {
						if (selectors.hasOwnProperty(i)) {
							$.stylesheet(i, (selectors as {[rule: string]: string})[i], context);
						}
					}
				}
			} else {
				// Split the rules into a key:value object list
				if (isString(rules)) {
					if (rules.includes("{")) {
						rules = (rules as string).replace(/(^[^{]*\{|\}.*)/g, "");
					}
					rules = (rules as string).split(";");
				}
				if (isArray(rules)) {
					tmp = rules;
					rules = {};
					for (i = 0; i < tmp.length; i++) {
						if ($.isString(tmp[i])) {
							tmp[i].replace(/^\s*([^\s:]+)\s*:\s*(.*?)\s*$/, createRule);
						}
					}
				}
				// Split the selectors into a context+selector list
				selectors = $.isArray(selectors) ? selectors : (selectors as string).split(",");
				context = $.isString(context) ? context + " " : "";
				for (i = 0; i < (selectors as string[]).length; i++) {
					(selectors as string[])[i] = context + (selectors as string[])[i].trim();
				}

				// Go through all selectors and add rules
				for (i = 0; i < (selectors as string[]).length; i++) {
					selector = (selectors as string[])[i];
					globalRules[selector] = $.extend(globalRules[selector] || {}, rules) as any;
				}
				// Update the stylesheet
				ic.setImmediate("style", update);
			}
		}
		return this;
	};

	/**
	 * Copy our rules into our global stylesheet
	 */
	function update() {
		if (globalStylesheet) {
			var style: string,
				stylecc: string,
				select: string,
				rule: string[],
				rules = globalRules,
				text = "";

			for (select in rules) {
				if (rules.hasOwnProperty(select)) {
					text += select + "{";
					rule = rules[select] as any as string[];
					for (style in rule) {
						if (rule.hasOwnProperty(style)) {
							stylecc = $.camelCase(style);
							text += (style !== "float" && $.cssProps.hasOwnProperty(stylecc) ? $.cssProps[stylecc].replace(/([A-Z])/g, "-$1").toLowerCase() : style) + ":" + rule[style] + ";";
						}
					}
					text += "}";
				}
			}
			globalStylesheet.text(text);
		}
	};

	/**
	 * Empty our stylesheet
	 */
	function empty() {
		if (globalStylesheet) {
			globalRules = {};
			globalStylesheet.text("");
		}
		return this;
	};

}());
