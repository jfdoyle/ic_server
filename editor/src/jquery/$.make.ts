///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQueryStatic {
	make(...args): JQuery;
}

var uniqueClassNumber = 0;

/**
 * Create an element by passing a series of strings
 * @static
 * @method make
 * @for jQuery
 * @param {boolean} [create=true] Pass false to skip creating this and return an empty selector
 * @param {string} type Such as "div", "span" etc
 * @param {string} [id] (unordered) An ID for this element, start with a hash: "#id"
 * @param {string} [classes]* (unordered) Class names
 * @param {string} [properties]* (unordered) Properties in the form "property=value" or "property="
 * @param {string} [styles]* (unordered) Styles in the form "style:value"
 * @param {string} [children]* (unordered) jQuery elements to be appended as children
 * @param {string} [content] (unordered) Html content in the form "=content"<br/>NOTE: This will replace anything added with "children" before it!
 * @return {jQuerySelector}
 */
$.make = function(create, type, id, classes, properties, styles, children, content) {
	var i: number,
		j: number,
		tmp: string[],
		tmp2: boolean,
		arg: string | number | string[] | JQuery,
		$el: JQuery,
		args = arguments;

	for (i = 0; i < args.length; i++) {
		arg = args[i];
		if (!i && ($.isBoolean(arg) || !arg)) {
			if (!arg) {
				break; // empty selector
			}
		} else if (!$el) {
			$el = $(!isString(arg) || (/[ #<.]/.test(arg) ? arg : createElement(arg) as any)); // could be a selector for quick editing
			//			$el = $($.isString(arg) ? (arg[0] === "<" ? arg : "<" + arg + ">") : arg);
		} else if (arg) {
			if (isObject(arg)) { // CSS Styles
				$el.css(arg as any);
			} else if (isArray(arg)) { // A jQuery.fn call
				if ((arg as string[]).length) {
					($el as any as Function[])[(arg as number[])[0]].apply($el, (arg as string[]).slice(1));
				}
			} else if (isString(arg)) {
				var stringArg = arg as string;

				if (stringArg[0] === "#") { // ID
					$el.attr("id", stringArg.replace("#", ""));
				} else if (stringArg[0] === "<") { // quick create
					$el.append(stringArg);
				} else if (stringArg[0] === "=") { // text/html content
					//						$el.append(document.createTextNode(arg.substr(1)));
					$el.html(stringArg.substr(1));
				} else {
					var colon = stringArg.indexOf(":"),
						equal = stringArg.indexOf("=");

					if (colon >= 0 && (equal < 0 || equal > colon)) { // CSS Styles
						tmp = stringArg.regex(/\s*([a-z-]+):(\s*(?:url\([^\)]*\)|[^;]+)+)\s*;?/gi) as string[];
						for (j = 0; j < tmp.length; j++) {
							$el.css(tmp[j][0], tmp[j][1]);
						}
					} else if (equal > 0) { // Data or Attribute
						tmp = stringArg.regex(/(.*?)=(.*)/) as string[];
						tmp2 = stringArg[0] === "?"; // Data starts with a question mark
						($el[tmp2 ? "data" : "attr"] as any)(tmp2 ? tmp[0].substr(1) : tmp[0], tmp[1] === undefined ? true : tmp[1]);
					} else if (stringArg.length) { // Class
						$el.addClass(stringArg);
					}
				}
			} else if ((arg as any).jquery || (arg as any) instanceof Node) { // jQuerySelector
				$el.append(arg);
			}
		}
	}
	$el = $el || $();
	if ($el.length) {
		//		if ($.currentHandler && !$.currentHandler._loaded && $.currentHandler.context) {
		//			if ($el.is(_img) && !$el[0].complete) {
		//				$.currentHandler[_fnAddLoading]();
		//				$el.on(_load + _space + _error, function(handler) {
		//					this.off();
		//					handler[_fnFinishLoading]();
		//				}.bind($el, $.currentHandler));
		//			}
		//			if (!BUILD) {
		//				if ($el.is(_img) && /^assets\/[0-9]+\//.test($el.attr("src"))) {
		//					$el.attr("data-img-src", $el.attr("src").regex(/^assets\/[0-9]+\/(.+)$/));
		//				} else if (/assets\/[0-9]+\//.test($el.attr("style"))) {
		//					$el.attr("data-img-bg", $el.attr("style").regex(/assets\/[0-9]+\/([^\?\)"]+)/));
		//				}
		//				$el.find("embed.svg[src]").each(function() {
		//					var className = "svg-replace-" + uniqueClassNumber++,
		//						$replace = $make(_span, className),
		//						$embed = $(this).replaceWith($replace);
		//					$.currentHandler[_fnAddLoading]();
		//					$.ajax($embed.attr("src"), {
		//						cache: true,
		//						global: false,
		//						success: [
		//							function(data) {
		//								$(_dot + this).replaceWith(data);
		//							}.bind(className),
		//							$.currentHandler[_fnFinishLoading].bind($.currentHandler)
		//						],
		//						dataType: "text"
		//					});
		//				});
		//			}
		//		}
	}
	return $el;
};
