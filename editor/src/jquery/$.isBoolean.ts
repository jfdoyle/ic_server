///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQueryStatic {
    isBoolean(str: any): boolean;
}

/**
 * Check if a variable is boolean
 * @static
 * @method isBoolean
 * @for jQuery
 * @param {*} bool
 * @return {boolean}
 */
$.isBoolean = function(bool) {
	return bool === true || bool === false;
};
