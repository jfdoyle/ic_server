///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQueryStatic {
    False(): boolean;
}

/**
 * Same as $.noop, but always returns false
 * @static
 * @method false
 * @for jQuery
 * @return {false}
 */
$.False = function() {
	return false;
};
