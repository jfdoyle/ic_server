///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQueryStatic {
    prefix(prop: string): string;
	cssProps: Object;
}

/**
 * Add a browser-specific prefix to a string if required (uses Modernizr)
 * @static
 * @method prefix
 * @for jQuery
 * @param {string} prop Property to prefix
 * @return {string}
 */
$.prefix = function(prop) {
	if (prop.includes("-")) {
		prop = prop.replace(/-([a-z])/gi, function($0, $1: string) {
			return $1.toUpperCase();
		});
	}
	if (!$.cssProps[prop]) {
		$.cssProps[prop] = Modernizr.prefixed(prop);
	}
	return $.cssProps[prop];
};
