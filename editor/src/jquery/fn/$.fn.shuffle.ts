///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQuery {
    shuffle(shuffle: boolean): JQuery;
}

/**
 * Shuffle the order of elements
 * @method shuffle
 * @for jQuery
 * @chainable
 * @param {boolean} [shuffle=true] Skip shuffling if false (useful for chaining)
 */
$.fn.shuffle = function(shuffle) {
	return shuffle === false ? this : [].shuffle.call($(this));
};
