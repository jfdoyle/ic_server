///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQuery {
    rxRemoveClass(className: RegExp): JQuery;
}

/**
 * Remove classes by regex - pass something like /\b(className)\b/i
 * @method rxRemoveClass
 * @for jQuery
 * @chainable
 * @param {RegExp} className Pattern to remove
 */
$.fn.rxRemoveClass = function(className) {
	return this.each(function() {
		$(this).removeClass(this.className.regex(className));
	});
};
