///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQuery {
    closestWidget(): JQuery;
}

/**
 * Find the closest widget ancestor of an element
 * @method closestWidget
 * @for jQuery
 * @return {jQuerySelector}
 */
$.fn.closestWidget = function() {
	for (var parent: HTMLElement = this[0]; parent; parent = parent.parentElement) {
		if (/^IC-/i.test(parent.tagName)) {
			break;
		}
	}
	return $(parent);
};
