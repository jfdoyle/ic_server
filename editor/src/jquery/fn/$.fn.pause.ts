///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQuery {
    pause(): JQuery;
}

/**
 * Pause an audio or video element
 * @method pause
 * @for jQuery
 * @chainable
 * @return {jQuerySelector}
 */
$.fn.pause = function() {
	return this.each(function() {
		try {
			this.pause();
		} catch (e) {
		}
	});
};
