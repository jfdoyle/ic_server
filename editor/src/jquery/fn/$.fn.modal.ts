///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQuery {
	modal(toggle?: boolean, duration?: number | boolean): JQuery;
}


/**
 * Show or hide a jQuery style modal on an element
 * @method modal
 * @for jQuery
 * @chainable
 * @param {boolean} [toggle] If set then show or hide, otherwise toggle
 * @param {Boolean|Number} [duration] Makes the change use fadeIn/fadeOut instead of immediate, pass a true for a default of 400ms
 */
$.fn.modal = function(this: JQuery, toggle?: boolean, duration?: number | boolean) {
	duration = duration === true ? 400 : duration;
	return this.each(function(i, element) {
		var show = true,
			$el = $(element),
			$data = $el.data("ui-modal");

		if (toggle === undefined) {
			show = $data === undefined;
		} else if ($.isBoolean(toggle)) {
			show = toggle;
		}
		if (show && !$data) {
			$data = $.make("div", "ui-front ui-widget-overlay", duration ? "ui-widget-slow" : null, duration ? "opacity:0" : null);
			$el
				.before($data)
				.addClass("ui-front")
				.data("ui-modal", $data);
			$("#abc>.abc-footer").addClass("disabled");
			if (duration) {
				$data.velocity("fadeIn", duration);
			}
		} else if (!show && $data) {
			duration = duration || $data.hasClass("ui-widget-slow");
			$el
				.removeClass("ui-front")
				.removeData("ui-modal");
			$("#abc>.abc-footer").removeClass("disabled");
			if (duration) {
				$data.velocity("fadeOut", duration, function() {
					$(this).remove();
				});
			} else {
				$data.remove();
			}
		}
	});
};
