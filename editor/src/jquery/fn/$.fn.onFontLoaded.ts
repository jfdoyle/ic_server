///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQuery {
	onFontLoaded(fn: Function, font?: string): JQuery;
}

/**
 * Call a function when fonts are loaded
 * @method onFontLoaded
 * @for jQuery
 * @chainable
 * @param {function()} callback Function to call
 * @param {string} [font] Font to wait for, otherwise just use inherited font
 */
$.fn.onFontLoaded = function(this: JQuery, callback: Function, font?: string): JQuery {
	if (!$.isFunction(callback)) {
		return this;
	}
	return this.each(function(i, el) {
		var $div = $.make("div", "ui-font-loader", "font-family:sans-serif", ["text", "giItT1WQy@!-/#"]).appendTo(el);
		//		console.log("onFontLoaded: ", $div.width(), $div.height(), font || "inherit");
		window.setTimeout(($.fn.onFontLoaded as any).check.bind($div, 0, $div.width(), $div.height(), callback), ($.fn.onFontLoaded as any).defaults.delay);
		$div.css("font-family", font || "inherit");
	});
};

/**
 * Check size of element
 * @this {Element}
 * @param {number} count
 * @param {number} width
 * @param {number} height
 * @param {function()} callback
 */
($.fn.onFontLoaded as any).check = function(this: JQuery, count, width, height, callback) {
	//	console.log("onFontLoad.check(" + count + ", " + (width || 0) + ", " + (height || 0) + ") -> (" + this.width() + ", " + this.height() + ") = " + this.css(_fontFamily));
	if (width !== this.width() || height !== this.height() || count >= ($.fn.onFontLoaded as any).defaults.maximum) {
		this.remove();
		callback();
	} else {
		window.setTimeout(($.fn.onFontLoaded as any).check.bind(this, count + 1, width, height, callback), ($.fn.onFontLoaded as any).defaults.delay);
	}
};

/**
 * onFontLoaded defaults â€“ added as a property on our plugin function
 */
($.fn.onFontLoaded as any).defaults = {
	/**
	 * Number of milliseonds between checks
	 */
	"delay": 50,
	/**
	 * Number of checks to do
	 */
	"maximum": 40 // 2 seconds @ 50ms
};
