///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQuery {
	imageLoad(callback: (image: JQuery) => void, repeat?: boolean): JQuery;
}

/**
 * Perform a callback when an image has loaded, optionally on every load
 * @method imageLoad
 * @for jQuery
 * @chainable
 * @param {function(jQuerySelector)} callback
 * @param {boolean} [repeat=false]
 */
$.fn.imageLoad = function(this: JQuery, callback: (image: JQuery) => void, repeat?: boolean): JQuery {
	return this.each(function(this: HTMLImageElement) {
		var $el = $(this),
			complete = this.complete;

		if (complete) {
			callback.call(this, $el);
		}
		if (!complete || repeat) {
			($el[repeat ? "on" : "one"] as any)("load", callback.bind(this, $el));
		}
	});
};
