///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQuery {
    common(selector?: string): JQuery;
}

/**
 * Find the common ancestors in or against a selector
 * @method common
 * @for jQuery
 * @param {String|jQuerySelector|Element} [selector] An optional second set of elements, otherwise find within itself
 * @return {jQuerySelector}
 */
$.fn.common = function(selector) {
	if (selector && this.is(selector)) {
		return this;
	}
	var i,
		$parents = (selector ? this : this.eq(0)).family(),
		$targets = selector ? $(selector) : this.slice(1);

	for (i = 0; i < $targets.length; i++) {
		$parents = $parents.has($targets.eq(i).family());
	}
	return $parents;
};
