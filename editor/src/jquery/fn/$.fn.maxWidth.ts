///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQuery {
	maxWidth(): number;
}

/**
 * Get the maximum width of all selected elements
 * @method maxWidth
 * @for jQuery
 * @return {number}
 */
$.fn.maxWidth = function(this: JQuery): number {
	for (var i = 0, width = 0; i < this.length; i++) {
		width = Math.max(width, this.eq(i).width());
	}
	return width;
};
