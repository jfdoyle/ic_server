///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQuery {
	textSpace(): string;
}

/**
 * Return the text contents with a space between each element
 * @method textSpace
 * @for jQuery
 * @return {String}
 */
$.fn.textSpace = function(this: JQuery): string {
	var text = "";

	$(this).find("*").contents().each(function(this: HTMLElement) {
		if (this.nodeType === 3) {
			text += (text ? " " : "") + this.textContent;
		}
	});
	return text;
};
