///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQuery {
	audio(): JQuery;
}

/**
 * Format a number of seconds to 0:00 minutes
 * @param {number} seconds
 * @return {string}
 */
function formatSeconds(seconds: number) {
	var secs = parseInt("" + (seconds % 60)),
		mins = parseInt("" + (seconds / 60));

	return mins + ":" + (secs > 9 ? "" : "0") + secs;
}

/**
 * Safari on Windows is one seriously out of date bug, this just reports on one set of issues...
 */
var ua = navigator.userAgent.toLowerCase();
var safariComplain = ua.includes("safari/") && ua.includes("windows") ? function() {

	safariComplain = $.noop;
	alert("Please install QuickTime to enable Audio or Video in Safari on Windows");
} : $.noop;

/**
 * Setup audio button handing
 * @method audio
 * @for jQuery
 * @return this
 */
$.fn.audio = function() {
	$(this).each(function(this: HTMLAudioElement) {
		var $audio = $(this);

		if ($audio.data("abc-audio")) {
			return;
		}
		if (!this.load) {
			// Quicktime required
			safariComplain();
		} else {
			this.load();
		}
		$audio
			.data("abc-audio", true)
			.on("play", function() {
				$("audio,video").not(this).pause();
				$("iframe").each(function(this: HTMLIFrameElement) {
					try {
						$(this.contentDocument).find("audio,video").pause();
					} catch (e) {
					}
				});
				$(this).closest(".abc-audio").removeClass("pause").addClass("play");
			})
			.on("pause", function() {
				$(this).closest(".abc-audio").removeClass("play").addClass("pause");
			})
			.on("ended.abc-audio", function(this: HTMLAudioElement) {
				var $this = $(this);

				$this.closest(".abc-audio").removeClass("play pause");
				this.currentTime = 0;
				this.pause();
				$this.siblings("svg").find("path").attr("d", "M0 0");
			})
			.on("timeupdate.abc-audio", function(this: HTMLAudioElement) {
				var $this = $(this),
					currentTime = this.currentTime,
					duration = this.duration,
					percent: any = currentTime / duration,
					angle = Math.range(0, percent * 360, 359),
					rad = (angle * Math.PI / 180),
					x = Math.sin(rad) * 153,
					y = Math.cos(rad) * -153,
					mid = (angle > 180) ? 1 : 0,
					$control = $this.closest(".abc-audio-wrapper").children(".abc-audio-control");

				percent = (percent * 100) + "%";
				$control.children("span").css("left", percent).text(formatSeconds(currentTime));
				$control.children("hr").css("width", percent);
				$this.siblings("svg").find("path").attr("d", "M 0 0" + (angle > 0 ? " v -153 A 153 153 1 " + mid + " 1 " + x + " " + y + " z" : ""));
				//					console.log(formatSeconds(currentTime) + " / " + formatSeconds(duration))
			});
	});
	return this;
};
