///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQuery {
    wrapGroup(wrappingElement: JQuery): JQuery;
}

/**
 * Wrap an HTML structure around all elements in the set of matched elements, respecting tree boundaries
 * @method wrapGroup
 * @for jQuery
 * @chainable
 * @param {String} wrappingElement
 */
$.fn.wrapGroup = function(wrappingElement) {
	var $next,
		$this = $(this);

	while ($this.length) {
		$next = $this.first();
		$next.nextAll().each(function(index) {
			if (!$this.eq(index + 1).is(this)) {
				return false;
			}
			$next.push(this);
		});
		$this = $this.not($next.wrapAll(wrappingElement));
	}
	return this;
};
