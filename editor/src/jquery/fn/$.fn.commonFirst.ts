///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQuery {
    commonFirst(selector?: string): JQuery;
}

/**
 * Find the first common ancestor in or against a selector
 * @method commonFirst
 * @for jQuery
 * @param {String|jQuerySelector|Element} [selector] An optional second set of elements, otherwise find within itself
 * @return {jQuerySelector}
 */
$.fn.commonFirst = function(selector) {
	return this.common(selector).first();
};
