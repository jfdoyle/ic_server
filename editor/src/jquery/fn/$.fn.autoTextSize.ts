///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQuery {
    autoTextSize(options?: Object): JQuery;
}

/**
 * Set the font-size so it can snugly fit the container
 * @method autoTextSize
 * @for jQuery
 * @chainable
 * @param {Object} [options] Options for finding a good size
 * @param {number} [options.maxWidth=0] Maximum width to fit (or parent)
 * @param {number} [options.maxHeight=0] Maximum height to fit (or parent)
 * @param {number} [options.minSize=50] Minimum font size (percent)
 * @param {number} [options.maxSize=200] Maximum font size (percent)
 */
$.fn.autoTextSize = function(options) {
	return this.css("font-size", this.findTextSize(options) + "%");
};
