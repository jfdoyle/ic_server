///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQuery {
    fixNatural(): JQuery;
}

/**
 * Fix the naturalWidth & naturalHeight of elements if the browser doesn't support it
 * @method fixNatural
 * @for jQuery
 * @chainable
 */
$.fn.fixNatural = function() {
	return this.each(function(i, el) {
		if (this.naturalWidth === undefined) {
			var theImage = new Image();

			theImage.src = this.src;
			this.naturalWidth = theImage.width;
			this.naturalHeight = theImage.height;
		}
	});
};
