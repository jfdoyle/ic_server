///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQuery {
	convertCSS(value: string, horiz?: boolean): number;
}

/**
 * Convert a css number type string into pixel units
 * @method convertCSS
 * @for jQuery
 * @param {string} value
 * @param {boolean} [horiz]
 * @return {number}
 */
$.fn.convertCSS = function(this: JQuery, value: string, horiz?: boolean): number {
	if (value === undefined || value === "") {
		return 0;
	}
	var val = parseFloat(value);
	//	console.log("convertCSS", value, value.replace(/[^a-z%]/g, ""), horiz)
	switch (value.replace(/[^a-z%]/g, "").substr(0, 4)) {
		case "none":
			val = 0;
			break;
		case "calc":
			var operand, num,
				sum = value.replace(/^calc\(|\)$/g, "").split(" ");

			val = this.convertCSS(sum.shift(), horiz);
			while (sum.length) {
				if (operand) {
					num = this.convertCSS(sum.shift(), horiz);
					if (operand === "+") {
						val += num;
					} else if (operand === "-") {
						val -= num;
					} else if (operand === "*") {
						val *= num;
					} else if (operand === "/") {
						val /= num;
					}
					operand = "";
				} else {
					operand = sum.shift();
				}
			}
			break;
		case "em":
			val *= parseFloat(this.css("font-size"));
			break;
		case "percent":
			val *= this.parent()[horiz ? "width" : "height"]() / 100;
			break
		case "rem":
			val *= parseFloat($("html").css("font-size"));
			break;
		case "vh":
			val *= window.innerHeight / 100;
			break;
		case "vw":
			val *= window.innerWidth / 100;
			break;
		case "vmin":
			val *= Math.min(window.innerWidth, window.innerHeight) / 100;
			break;
		case "vmax":
			val *= Math.max(window.innerWidth, window.innerHeight) / 100;
			break;
		case "px":
		case "":
			break;
		default:
			console.warn("convertCSS: Unknown conversion type!! (" + value + ")");
			return 0;
	}
	//	console.log("=", val);
	return val;
};
