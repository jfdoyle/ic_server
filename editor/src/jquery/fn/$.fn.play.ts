///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQuery {
    play(): JQuery;
}

/**
 * Play an audio or video element
 * @method play
 * @for jQuery
 * @chainable
 * @return {jQuerySelector}
 */
$.fn.play = function() {
	return this.each(function() {
		try {
			this.play();
		} catch (e) {
		}
	});
};
