///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQuery {
	verticalCenter(height: number | string): JQuery;
}

/**
 * Vertically align an element within it's parent frame
 * @method verticalCenter
 * @for jQuery
 * @chainable
 * @param {String|Number} [height] Pixel height or relative selector or blank for parent height
 */
$.fn.verticalCenter = function(this: JQuery, height: number | string) {
	var isString = $.isString(height),
		padding: number[] = [];

	this	// Separate the reading from the writing for performance
		.each(function(i, element) {
			var $this = $(element),
				font = parseFloat($this.css("font-size"));

			height = (((isString ? $(height as string, element).height() : (height as number || $this.parent().height())) - $this.height()) / 2);
			padding[i] = Math.max(0, height / font);
		})
		.each(function(i, element) {
			$(element).css("padding-top", padding[i] + "em");
		});
	return this;
};
