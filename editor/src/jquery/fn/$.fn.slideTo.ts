///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQuery {
	slideTo(target: JQuery | string, options: number | JQueryAnimationOptions): JQuery;
}

/**
 * Slide one element into another (appendTo at end)
 * @method slideTo
 * @for jQuery
 * @chainable
 * @param {jQuerySelector|String} target Target element or "parent"
 * @param {Object|Number} [options] Velocity options or duration - passed directly to Velocity
 */
$.fn.slideTo = function(this: JQuery, target: JQuery | string, options: number | JQueryAnimationOptions): JQuery {
	var isParent = !target || target === "parent",
		$target = isParent ? null : $(target);

	if (isParent || $target.length) {
		return this.each(function() {
			var $this = $(this),
				offset = $this.offset();

			if (!isParent && !$target.is($this.parent())) {
				$this.appendTo($target);
			}
			$this.stop(true);
			if (options === 0 || ($.isPlainObject(options) && (options as any).duration === 0)) {
				$this.css({
					top: 0,
					left: 0
				});
			} else {
				$this.offset(offset).animate({
					top: 0,
					left: 0
				}, options as JQueryAnimationOptions);
			}
		});
	}
	return this;
};
