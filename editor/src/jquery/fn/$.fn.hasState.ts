/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQuery {
    hasState(state: string): boolean
}

/**
 * Checks if an element has a specific state
 * @param {string} state
 * @returns {boolean}
 */
$.fn.hasState = function(state) {
	return !!(this[0].icState || {})[state];
};
