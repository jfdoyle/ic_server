///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQuery {
    style(props?: string[] | Object): JQuery;
}

/**
 * Save or restore a list of styles for a selector.
 * NOTE: this is stored in the Selector for speed and not on the elements
 * @method style
 * @for jQuery
 * @chainable
 * @param {Array|Object} [props] Property names to save. If an array then simply save, if an object then save and set, if unused then restore
 */
$.fn.style = function(props) {
	var i, tmp, style, index, styles,
		_saved = "savedStyles";

	for (index = 0; index < this.length; index++) {
		style = this.get(index).style;
		styles = this[_saved + index];
		if ($.isArray(props)) {
			if (!styles) {
				styles = this[_saved + index] = {};
			}
			for (i = 0; i < props.length; i++) {
				tmp = $.prefix(props[i]);
				styles[tmp] = style[tmp];
			}
		} else if ($.isPlainObject(props)) {
			if (!styles) {
				styles = this[_saved + index] = {};
			}
			for (i in props) {
				if (props.hasOwnProperty(i)) {
					tmp = $.prefix(i);
					styles[tmp] = style[tmp];
					style[tmp] = props[i];
				}
			}
		} else if (styles) {
			for (i in styles) {
				if (styles.hasOwnProperty(i)) {
					style[i] = styles[i];
				}
			}
			delete this[_saved + index];
		}
	}
	return this;
};
