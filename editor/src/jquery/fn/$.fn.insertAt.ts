///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQuery {
	insertAt(index: number, parent?: JQuery | Element): JQuery;
}

/**
 * Insert an element at a specified position
 * @method insertAt
 * @for jQuery
 * @chainable
 * @param {number} index Index to insert at
 * @param {Element|jQuery} parent Parent to insert element(s) into
 */
$.fn.insertAt = function(this: JQuery, index: number, parent?: JQuery | Element): JQuery {
	var i,
		$parent = $(parent);

	if (index > 0) {
		$parent = $parent.children().eq(index - 1);
	}
	for (i = this.length - 1; i >= 0; i--) {
		if (index <= 0) {
			$parent.prepend(this);
		} else {
			$parent.after(this);
		}
	}
	return this;
};
