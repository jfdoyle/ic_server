///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQuery {
	hoverable(className: string): JQuery;
}

/**
 * Toggle one or more classes when hovering over an element
 * @method hoverable
 * @for jQuery
 * @chainable
 * @param {string} [className="ui-state-hover"] A space separated list of classes
 */
$.fn.hoverable = function(this: JQuery, className: string): JQuery {
	return this.hover(function() {
		$(this).addClass(className || "ui-state-hover");
	}, function() {
		$(this).removeClass(className || "ui-state-hover");
	});
};
