///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQuery {
	max(prop: string, def?: number): number;
}

/**
 * Find the maximum size of a css value for a group of elements
 * @method max
 * @for jQuery
 * @param {string} prop Property to find
 * @param {number} [def=0] Default value if not found
 * @return {number}
 */
$.fn.max = function(this: JQuery, prop: string, def?: number): number {
	var val = Number.MIN_VALUE;

	this.each(function(i, el) {
		val = Math.max(val, parseFloat($(el).css(prop)));
	});
	return (val === Number.MIN_VALUE ? def : val) || 0;
};
