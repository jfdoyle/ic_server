///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQuery {
    naturalWidth(): number;
}

/**
 * Get the naturalWidth of an element
 * @method naturalWidth
 * @for jQuery
 * @return {number}
 */
$.fn.naturalWidth = function() {
	return this.length ? this[0].naturalWidth || this.fixNatural()[0].naturalWidth : null;
};
