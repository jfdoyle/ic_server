///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQuery {
    getRect(): ClientRect;
}

/**
 * Shortcut to getBoundingClientRect
 * @method getRect
 * @for jQuery
 */
$.fn.getRect = function() {
	var el = this[0];

	if (el) {
		var rect = el.getBoundingClientRect();
		return {
			top: rect.top,
			right: rect.right,
			bottom: rect.bottom,
			left: rect.left,
			height: rect.height || (rect.bottom - rect.top),
			width: rect.width || (rect.right - rect.left)
		};
	}
};
