///<reference path="../_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQuery {
    loading(toggle?: boolean, timeout?: number): JQuery;
}

interface JQueryStatic {
    loading(toggle?: boolean, timeout?: number): JQuery;
}

/**
 * Show a css spinner with a modal background
 * @method loading
 * @for jQuery
 * @chainable
 * @param {boolean} [toggle]
 * @param {number=} [timeout]
 */
$.fn.loading = $.loading = function(toggle, timeout) {
	var show = true,
		className = "ui-loading",
		$parent = $(!this || this === $ ? "body" : this),
		$spinner = $parent.data(className + "-element");

	if (toggle === undefined) {
		show = $spinner === undefined;
	} else if ($.isBoolean(toggle)) {
		show = toggle;
	}
	if (show && !$spinner) {
		//			console.log("Start loading, parent:", $parent);
		$parent
			.data(className + "-element", $spinner = $.make("div", className + " fa fa-cog fa-spin"))
			.data(className + "-timeout", window.setTimeout(function() {
				$spinner.appendTo($parent).modal(true);
			}, timeout || 100));
	} else if (!show && $spinner) {
		//			console.log("Finish loading, parent:", $parent);
		window.clearTimeout($parent.data(className + "-timeout"));
		$spinner.modal(false).remove();
		$parent.removeData([className + "-element", className + "-timeout"]);
	}
	return this;
};
