///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQuery {
    family(): JQuery;
    push(args: any): JQuery; // TODO make safe?
}

/**
 * Get all parents of an element including itself
 * @method family
 * @for jQuery
 * @return {jQuerySelector}
 */
$.fn.family = function() {
	var i, el,
		nodes = $();

	for (i = 0; i < this.length; i++) {
		for (el = this[i]; el !== document; el = el.parentNode) {
			nodes.push(el);
		}
	}
	return nodes;
};
