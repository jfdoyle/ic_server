///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQuery {
	maxHeight(): number;
}

/**
 * Get the maximum height of all selected elements
 * @method maxHeight
 * @for jQuery
 * @return {number}
 */
$.fn.maxHeight = function(this: JQuery): number {
	for (var i = 0, height = 0; i < this.length; i++) {
		height = Math.max(height, this.eq(i).height());
	}
	return height;
};
