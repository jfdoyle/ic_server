///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQuery {
    naturalHeight(): number;
}

/**
 * Get the naturalHeight of an element
 * @method naturalHeight
 * @for jQuery
 * @return {number}
 */
$.fn.naturalHeight = function() {
	return this.length ? this[0].naturalHeight || this.fixNatural()[0].naturalHeight : null;
};
