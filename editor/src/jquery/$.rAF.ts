///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQueryStatic {
	rAF(callback: FrameRequestCallback): void;
	rAF(name: string, callback?: FrameRequestCallback): void;
}

var rAF = window.requestAnimationFrame,
	rAFname: { [name: string]: FrameRequestCallback } = {},
	rAFwrapper = false;

/**
 * Request an animation frame, optionally give a name to only call once per frame
 * @static
 * @method rAF
 * @for jQuery
 * @param {(string|function())} [name] Name of callback so it's only called once per frame
 * @param {function()} callback Function to call on the next frame
 */
$.rAF = function(name: string | FrameRequestCallback, callback?: FrameRequestCallback) {
	var fn: FrameRequestCallback;

	if ($.isString(name)) {
		fn = callback;
		if (callback) {
			rAFname[name as string] = callback;
			if (rAFwrapper) {
				fn = undefined;
			} else {
				rAFwrapper = true;
				fn = function(time: number) {
					var callback: FrameRequestCallback;

					rAFwrapper = false;
					for (var name in rAFname) {
						if (rAFname.hasOwnProperty(name)) {
							callback = rAFname[name];
							if (callback) {
								rAFname[name] = undefined;
								callback(time);
							}
						}
					}
				};
			}
		} else {
			rAFname[name as string] = undefined;
		}
	} else {
		fn = name as FrameRequestCallback;
	}
	if (fn) {
		if (rAF && !document.hidden) {
			rAF(fn);
		} else {
			window.setTimeout(fn, 16);
		}
	}
};
