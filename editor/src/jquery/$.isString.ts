///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQueryStatic {
    isString(str: any): boolean;
}

/**
 * Check if a variable is a string
 * @static
 * @method isString
 * @for jQuery
 * @param {*} str
 * @return {boolean}
 */
$.isString = function(str) {
	return typeof str === "string";
};
