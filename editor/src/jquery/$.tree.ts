///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

/*
 * .aba-tree = root class
 * .aba-tree-children = is a folder
 * .aba-tree-children-open = is an open folder
 * .aba-tree-active = is *the* active element
 * .aba-tree-lastchild = is the last child of an open folder
 *
 * ul.aba-tree
 * -> li
 *		-> span = folder / element icon
 *		-> a = name
 *			-> span...
 *		-> ul = children
 *
 * init({ // options
 *		tree: $element,
 *		autoExpand: boolean, // to autoexpand any node clicked on
 *		isParent: function(id), // return true if a possible parent
 *		getChildren: function(id), // id of parent
 *		getParent: function(id), // get parent id of id
 *		getIndex: function(id), // get zero-based index location
 *		onClick: function(id), // id clicked on
 * })
 */

interface TreeOptions {
	tree: JQuery;
	autoExpand: boolean;
	isParent: (id: string) => boolean;
	getChildren: (id: string) => void;
	getHtml: (id: string, $li: JQuery) => string;
	getParent: (id: string) => string;
	getIndex: (id: string) => number;
	onAfterHtml: (id: string, $li: JQuery, $parent: JQuery) => void;
	onBeforeClick: (id: string) => boolean;
	onClick: (id: string) => void;
	canDrag: (id: string) => boolean;
	canDrop: (id: string, target: number, pos: string) => boolean; // TODO: Implement drop limits
	onDrop: (id: string, target: number, pos: string) => void;
	onPaste: (id: string, target: number) => void;
}

interface Tree {
	currentId: string;
	add: (id: string) => void;
	click: (id?: string) => boolean;
	cancel: () => void;
	closest: (id?: string) => string;
	empty: () => void;
	expand: (id?: string) => void;
	get: (id?: string) => JQuery;
	getNextId: (id?: string) => string;
	getParentId: (id?: string) => string;
	getPrevId: (id?: string) => string;
	goChild: () => boolean;
	goNext: () => boolean;
	goParent: () => boolean;
	goPrev: () => boolean;
	goToggle: () => boolean;
	index: (id?: string) => number;
	init: (options: TreeOptions) => void;
	isParent: (id?: string) => boolean;
	loadChildren: (id?: string) => void;
	remove: (id?: string) => void;
}

namespace TreeModule {

	export var $root: JQuery;
	export var currentId: string;
	export var nodes: { [id: string]: Object } = {};
	export var options: TreeOptions;

	var copyId: string;
	var copyCut: boolean;

	function callback(fn, ...args) {
		if (fn) {
//			try {
				return fn.apply($.tree, args);
//			} catch (e) {
//				console.error("Error in $.tree callback:", e);
//			}
		}
	}

	function treeToggleChildren(): boolean { // Handle clicking to fold/unfold tree
		var $this = $(this),
			$li = $this.closest("li"),
			id = $li.data("id"),
			isOpen = !$li.toggleClass("aba-tree-children-open").hasClass("aba-tree-children-open");

		loadChildren(id);
		$li.children("ul")["slide" + (isOpen ? "Up" : "Down")]();
		return false;
	}

	function treeClickChild(): boolean {
		var $this = $(this),
			$li = $this.closest("li"),
			id = $li.data("id"),
			isParent = TreeModule.isParent(id);

		//		console.log("treeClickChild", this)
		if (options.autoExpand && isParent && !$li.hasClass("aba-tree-children-open")) {
			treeToggleChildren.call(this);
		}
		if (callback(options.onBeforeClick, id) !== false) {
			callback(options.onClick, id);
			currentId = id;
			isParent = isParent;
			var $parents = $li.parentsUntil($root, "li");
			$root.find(".aba-tree-active").not($li).removeClass("aba-tree-active");
			$root.find(".aba-tree-path").not($parents).removeClass("aba-tree-path");
			$li.addClass("aba-tree-active");
			$parents.addClass("aba-tree-path");
			//var rect = $li.getRect(),
			//	treeRect = $root.getRect();
			//			console.log("scrollIntoView", rect, treeRect)
			//			if ((rect.top || rect.bottom) && (rect.top < treeRect.top || rect.bottom > treeRect.bottom)) {
			//				$root.animate({ scrollTop: rect.top - treeRect.top });
			//			}
		}
		$root.focus();
		return false;
	}

	function treeDragChild(event: JQuery.Event) {
		var data = event.data || {} as any,
			$drag = data.drag,
			$helper = data.helper,
			dropList = "mouseup.tree touchend.tree",
			moveList = "mousemove.tree touchmove .tree",
			dragList = moveList + " " + dropList,
			hover = function(drop) {
				if (data.lastdrop !== drop && data.lastdrop) {
					$(data.lastdrop).removeClass("aba-tree-hover-" + data.lastpos);
				}
				if (drop) {
					var $drop = $(drop),
						rect = $drop.getRect(),
						height = rect.height,
						top = event.pageY - rect.top,
						pos = $drop.hasClass("aba-tree-children")
							? (top < height / 4 ? "prev" : top > height - height / 4 ? "next" : "inner")
							: (top < height / 2 ? "prev" : "next");

					if (data.lastdrop !== drop || data.lastpos !== pos) {
						$drop.removeClass("aba-tree-hover-" + data.lastpos).addClass("aba-tree-hover-" + pos);
						data.lastpos = pos;
					}
				}
				data.lastdrop = drop;
			};

		if (event.which > 1 || event.isDefaultPrevented()) {
			return;
		}
		if (event.type === "touchstart" || event.type === "touchmove") {
			event.pageX = (event.originalEvent as TouchEvent).changedTouches[0].pageX;
			event.pageY = (event.originalEvent as TouchEvent).changedTouches[0].pageY;
		}
		switch (event.type) {
			case "touchstart":
			case "mousedown":
				$drag = $(this);
				var rect = $drag.getRect(),
					canDrag = callback(options.canDrag, $drag.data("id"));

				$(document)
					.css("cursor", canDrag ? "ns-resize" : "not-allowed")
					.on(canDrag ? dragList : dropList as any, {
						drag: $drag,
						lastdrop: null,
						lastpos: "",
						pageY: event.pageY,
						pageX: event.pageX,
						top: rect.top - event.pageY,
						left: rect.left - event.pageX
					} as any, treeDragChild);
				return false;

			case "touchmove":
			case "mousemove":
				if ($drag && ($helper || Math.abs(data.pageY - event.pageY) > 10)) {
					if (!$helper) {
						data.helper = $helper = $drag.clone(true).removeClass().addClass("aba-tree-dragging").insertBefore($drag.addClass("aba-tree-selected"));
						data.containerRect = $root.getRect();
					}
					ic.setImmediate("tree", function() {
						if ($helper) {
							$helper.css({
								top: event.pageY + data.top,
								left: event.pageX + data.left
							});
							hover(data.drop = $(document.elementFromPoint(event.pageX, event.pageY)).closest("li").not(".aba-tree-selected")[0]);
						}
					});
				}
				return false;

			case "touchend":
			case "mouseup":
				$(document)
					.css("cursor", "")
					.off(".tree");
				if ($helper) {
					$helper.remove();
					$drag.removeClass("aba-tree-selected");
					if (data.drop) {
						callback(options.onDrop, $drag.data("id"), $(data.drop).data("id"), data.lastpos);
						hover(null);
					}
				} else {
					treeClickChild.call($drag[0]);
				}
				return false;
		}
	}

	export function sort(id: string): void {
		var $children = !id ? $root : get(id).children("ul");

		$children
			.children()
			.sort(function(a, b) {
				var a_index = index($.data(a, "id")),
					b_index = index($.data(b, "id"));

				if (a_index === b_index) {
					a_index = a.textContent;
					b_index = b.textContent;
				}
				return a_index === b_index ? 0 : a_index > b_index ? 1 : -1;
			})
			.appendTo($children)
			// TODO: Check if this is right - show breadcrumb trail
			.filter(":not(.hidden)")
			.removeClass("aba-tree-lastchild")
			.last()
			.addClass("aba-tree-lastchild");
	}

	var sortId: string[] = [];

	function deferSort() {
		for (var i = 0; i < sortId.length; i++) {
			sort(sortId[i]);
		}
		sortId = [];
	}

	export function add(id: string): void {
		//		console.log("Tree", id, options, ($root || [])[0])
		if (id) {
			var $li = get(id),
				parentId = getParentId(id) || "",
				$parent = get(parentId) || null;

			if (!$li.length) {
				nodes[id] = nodes[id] || [];
				nodes[id][0] = $li = $.make("li", callback(options.isParent, id) ? "aba-tree-children" : "aba-tree-node", $.make("span"), $.make("a", "=" + id)).data("id", id);
			}
			var content = callback(options.getHtml, id, $li.children("a"));
			if (content) {
				$li.children("a").html(content);
			}
			if ($parent.length) {
				if (parentId !== undefined) {
					if (!parentId) {
						$parent = $root;
					} else {
						var tmp = $parent.children("ul");
						if (!tmp.length) {
							tmp = $.make("ul").appendTo($parent);
						}
						$parent = tmp;
					}
				}
				if (!$parent.is($li.parent())) {
					$parent.append($li);
				}
				sortId.pushOnce(parentId, id);
				ic.setImmediate(deferSort);
				callback(options.onAfterHtml, id, $li, $parent);
			} else {
				console.warn("no parent", parentId, "for", id)
			}
		}
	};

	export function click(id?: string): boolean {
		var $el = get(id);

		if ($el.length) {
			treeClickChild.call($el);
			if (options.autoExpand) {
				expand(id);
			}
			return true;
		}
		return false;
	};

	export function cancel(): void {
		$root.find(".aba-tree-active").removeClass("aba-tree-active");
		$root.find(".aba-tree-path").removeClass("aba-tree-path");
	};

	export function closest(id?: string): string {
		id = id === undefined ? currentId : id;
		return get(id).hasClass("aba-tree-children") ? id : getParentId(id);
	};

	export function doCopy() {
		//console.log("doCopy", currentId)
		copyId = currentId;
		copyCut = false;
	}

	export function doCut() {
		//console.log("doCut", currentId)
		doCopy();
		copyCut = true;
	}

	export function doPaste() {
		//console.log("doPaste", copyId, copyCut)
		if (copyId) {
			callback(copyCut ? options.onDrop : options.onPaste, copyId, currentId, isParent() ? "inner" : "next");
			if (copyCut) {
				copyId = undefined;
			}
		}
	}

	export function empty(): void {
		nodes = {
			"": [$root.empty()]
		};
		currentId = "";
	};

	export function expand(id?: string): void {
		get(id === undefined ? currentId : id).parents("li.aba-tree-children").addClass("aba-tree-children-open");
	};

	export function get(id?: string): JQuery {
		return (nodes[id === undefined ? currentId : id] || [])[0] || $();
	};

	export function getNextId(id?: string): string {
		return get(id === undefined ? currentId : id).next().data("id");
	};

	export function getParentId(id?: string): string {
		return callback(options.getParent, id === undefined ? currentId : id);
	};

	export function getPrevId(id?: string): string {
		return get(id === undefined ? currentId : id).prev().data("id");
	};

	export function goChild(): boolean {
		if (callback(options.isParent, currentId)) {
			var $parent = get();

			if ($parent.hasClass("aba-tree-children")) {
				if (!$parent.hasClass("aba-tree-children-open")) {
					treeToggleChildren.call($parent);
				}
				treeClickChild.call($parent.find(">ul>li:first"));
			}
		}
		return false;
	}

	export function goNext(): boolean {
		var next = getNextId() || getNextId(getParentId());

		if (next) {
			click(next);
		}
		return false;
	}

	export function goParent(): boolean {
		var parent = getParentId();

		if (parent) {
			click(parent);
		}
		return false;
	}

	export function goPrev(): boolean {
		var prev = getPrevId() || getParentId();

		if (prev) {
			click(prev);
		}
		return false;
	}

	export function goToggle(): boolean {
		if (isParent()) {
			treeToggleChildren.call(get());
		}
		return false;
	}

	export function index(id?: string) {
		return callback(options.getIndex, id === undefined ? currentId : id);
	};

	export function isParent(id?: string): boolean {
		return callback(options.isParent, id === undefined ? currentId : id)
	};

	export function init(opts: TreeOptions): void {
		if (opts) {
			var tree = opts.tree;
			$root = (tree instanceof jQuery ? tree : $(tree || ".aba-tree"))
				.addClass("aba-tree")
				.attr("tabindex", "0")
				.on("mousedown touchstart", ".aba-tree-children>span:first-child", treeToggleChildren)
				//					.on("click", "li", treeClickChild)
				.on("mousedown touchstart", "li", treeDragChild)
				.on("click", $.False)
				.on("keydown" as any, function(event: KeyboardEvent) {
					switch (event.which) {
						case 32: // space
							goToggle();
							break;

						case 37: // left
							goParent();
							break;

						case 38: // up
							goPrev();
							break;

						case 39: // right
							goChild();
							break;

						case 40: // down
							goNext();
							break;

						case 88: // x
							// TODO: Make use of the system clipboad event
							if (event.ctrlKey || event.metaKey) {
								doCut();
								break;
							}
							return;

						case 67: // c
							if (event.ctrlKey || event.metaKey) {
								doCopy();
								break;
							}
							return;

						case 86: // v
							if (event.ctrlKey || event.metaKey) {
								doPaste();
								break;
							}
							return;

						default:
							//console.log(event.which, event.ctrlKey)
							return;
					}
					event.preventDefault();
				});
			options = opts;
			empty();
		}
	};

	export function loadChildren(id?: string): void {
		id = id === undefined ? currentId : id;
		if (!nodes[id] || !nodes[id][1]) {
			nodes[id] = nodes[id] || [];
			nodes[id][1] = true;
			callback(options.getChildren, id);
		}
	};

	export function remove(id?: string): void {
		id = id === undefined ? currentId : id;
		if (id) {
			var $el = get(id);

			if (id === currentId) {
				click(getNextId() || getPrevId() || getParentId());
			}
			$el.remove();
			delete nodes[id];
		}
	};

};

interface JQueryStatic {
	tree: Tree;
}

$.tree = TreeModule;
