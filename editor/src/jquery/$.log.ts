///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQueryStatic {
    log(str: string): string;
}

/**
 * Always log this, even when uglify removes console stuff...
 * @param {String} text
 * @return {String}
 */
$.log = function(text) {
	var console = window["console"];
	if (text && console && console["log"]) {
		console["log"](text);
	}
	return text;
};
