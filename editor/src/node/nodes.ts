///<reference path="node.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ICNodes {
	interface ICNodeType {
		[id: number]: ICNode;
	};

	var activities: ICNodeType = {
		0: new ICNode({
			"_id": 0,
			"_type": ICNodeType.Dummy,
			"_children": []
		})
	};

	var loading: {[index: number]: boolean} = {},
		loadingList: number[] = [],
		loadingPromises: Function[] = [];

	export function forEach(callback: (activity: ICNode, id?: number, list?: {[id: number]: ICNode}) => void) {
		for (var id in activities) {
			callback(activities[id], parseInt(id, 10), activities);
		}
	}

	export function isGroup(id: number) {
		return activities[id] && activities[id].isGroup;
	}

	export function getParent(id: number): ICNode {
		return activities[id] && activities[id].getParent();
	}

	export function getParentId(id: number): number {
		return activities[id] && activities[id].getParentId();
	}

	export function getParents(id: number, andSelf?: boolean): ICNode[] {
		return (activities[id] && activities[id].getParents(andSelf)) || [];
	}

	export function getChildren(id: number): ICNode[] {
		return (activities[id] && activities[id].getChildren()) || [];
	}

	export function getIcon(id: number) {
		return activities[id] ? activities[id].icon : "fa-question-circle";
	}

	export function getIndex(id: number) {
		return activities[id] ? get(id).index : -1;
	}

	export function get(id: number): ICNode {
		return activities[id];
	}

	export function loaded(id: number): boolean {
		return !!activities[id];
	}

	export function has(id: number): boolean {
		return activities.hasOwnProperty("" + id);
	}

	export function set(data: Object | ICNode): ICNode {
		var id = (data as ICNode)._id;

		if (activities[id]) {
			activities[id].set(data);
		} else {
			activities[id] = new ICNode(data);
		}
		return activities[id];
	}

	export function getNamePrefix(id: number | ICNode, prefix: string): string {
		var i = 0,
			parent = isNumber(id) ? get(id as number) : id as ICNode;

		if (parent) {
			while (i === 1 || parent.getChildren().filter(function(activity) {
				return activity._name === prefix + (i ? " (" + i + ")" : "");
			}).length) {
				i++;
			}
		}
		return prefix = prefix + (i ? " (" + i + ")" : "");
	}

	/**
	 * Get all inherited data for an activity, _build and _children don't inherit, _files is corrected to include paths
	 * @param {(Object|Number)} data
	 * @return {Object}
	 */
	export function inherit(id: number | ICNode) {
		var copy = new ICNode(),
			activity = id instanceof ICNode ? id as ICNode : get(id as number),
			files: string[] = [];

		activity.getParents(true).reverse().forEach(function(parent) {
			parent.clone(copy as Object, true);
			if (parent._files) {
				parent._files.forEach(function(filename) {
					files.pushOnce(filename);
				});
			}
		});
		if (!activity.hasChildren) {
			delete copy._children;
		}
		if (!activity._build) {
			delete copy._build;
		}
		activity._files = files;
		return copy;
	};
	/**
	 * Remove parent inherited data from an activity
	 * @param {Object} data
	 * @return {Object}
	 */
	export function uninherit(activity: ICNode) {
		return (activity as Object).diff(inherit(activity._parent));
	};

	export function loadParents(id: number): void {
		Editor.ajax({
			type: "POST",
			url: "ajax.php",
			global: false,
			dataType: "json",
			data: {
				action: "get_parents",
				id: id
			},
			success: function(data: number[]) {
				if ((data as any).error) {
					return Editor.dialog("Error: " + (data as any).error, "Server Error");
				}
				// console.log("Load parents: ", data);
				for (var id: number, i = data.length - 1; i >= 0; i--) {
					id = data[i];
					if (ICNodes.has(id)) {
						// Don't re-load this node, but do grab any children
						data.splice(i, 1);
						[].push.apply(data, ICNodes.get(id).getChildrenId());
					}
				}
				ICNodes.load(loadingList = data);
			}.bind(ICNodes)
		});
	}

	export function onLoaded(callback: (activity: ICNode, index?: number) => void, data: ICNode | ICNode[], index?: number) {
		if ((data as any).error) {
			return Editor.dialog("Error: " + (data as any).error, "Server Error");
		} else if ((data as any).success) {
			if (callback) {
				callback(null, -1);
			}
			return Editor.log("Success: " + (data as any).success);
		}
		//		console.log("onLoaded", data)
		if (isArray(data)) {
			var data_array = data as ICNode[];

			for (var i = 0; i < data_array.length; i++) {
				onLoaded(callback, data_array[i], i);
			}
		} else if (isObject(data)) {
			var activity = set(data as ICNode),
				id = activity._id,
				old_files = (get((data as ICNode)._id) || {} as ICNode)._files || [],
				new_files = activity._files || [];

			cachedNodes = null;
			delete loading[0]; // make sure it can be loaded once
			delete loading[id];
			if (callback) {
				callback(activity, index);
			}
			$.tree.add(String(id));
			old_files.forEach(function(filename) {
				// remove old files that don't exist
				if (!new_files || !new_files.includes(filename)) {
					$.tree.remove(id + "_" + filename.replace(/^assets\/\d+\//, ""));
				}
			});
			new_files.forEach(function(filename) {
				// add new files that don't already exist
				if (!old_files || !old_files.includes(filename)) {
					$.tree.add(id + "_" + filename.replace(/^assets\/\d+\//, ""));
				}
			});
			if (!activity._parent) {
				activities[0]._children.pushOnce(id);
				activities[0]._children.sort(function(a, b) {
					var a_name = activities[a]._name || "",
						b_name = activities[b]._name || "";

					return a_name < b_name ? -1 : a_name > b_name ? 1 : 0;
				});
			}
			if (id === Editor.findActivityId) {
				$.tree.click(String(id));
				Editor.setActivity(activity);
				Editor.findActivityId = -1;
			}
			if (loadingList.includes(id)) {
				loadingList.splice(loadingList.indexOf(id), 1);
				if (activity.isGroup) {
					ICNodes.load(activity.getChildrenId());
				}
			}
			if (!Object.keys(loading).length) {
				loadingPromises.forEach(function(resolve) {
					resolve(activities);
				});
				loadingPromises = [];
			}
		} else {
			console.error("Activity.loaded() with invalid data", data);
		}
	}

	export function load(request: number | number[] | Object, callback?: (activity: ICNode, index?: number) => void, force?: boolean): Promise<ICNodeType> {
		if (request) {
			if (!isObject(request)) {
				var id_list: number[] = (isArray(request) ? request as number[] : [request as number]);

				if (!force) {
					var isWaiting = false;

					id_list = id_list.filter(function(id, index) {
						if (!loading[id]) {
							if (!ICNodes.has(id) || !id) {
								loading[id] = true;
								return true;
							}
						} else {
							isWaiting = true;
						}
						if (callback) {
							callback(ICNodes.get(id), index);
						}
					});
					if (!id_list.length) {
						// console.log("Saved re-loading activities")
						if (isWaiting) {
							return new Promise(function(resolve) {
								loadingPromises.push(resolve);
							});
						}
						return Promise.resolve(activities);
					}
				}
				// console.log(id_list.length === 1 && has(id_list[0]) ? "Updating" : "Loading", id_list);
				request = {
					"action": "get_node",
					"id": id_list.length === 1 ? id_list[0] : id_list
				}
			}
			return Editor.ajax({
				type: "POST",
				url: "ajax.php",
				global: !force, // Forced are quiet - poll etc
				dataType: "json",
				data: request,
				success: onLoaded.bind(ICNodes, callback)
			} as any);
		}
	}

	var cachedNodes: number[];
	var cachedSince: number;

	function getCachedNodes() {
		if (!cachedNodes) {
			var activity: ICNode,
				time: number = 0,
				nodes: number[] = [];

			for (var id in activities) {
				activity = activities[id];
				if (activities.hasOwnProperty(id) && activity._date) {
					time = Math.max(time, Date.parse(activity._date) || 0, Date.parse(activity._created) || 0);
					if (activity.isGroup && activity.hasChildren && loaded(activity.getChildrenId().first())) {
						// Only look for parents of children, otherwise we're already included by our own parents
						nodes.pushOnce(activity._id);
					}
				}
			}
			cachedNodes = nodes;
			cachedSince = time;
		}
		return cachedNodes;
	}

	function poll() {
		var nodes: number[] = getCachedNodes();

		if (Editor.online && nodes.length && cachedSince) {
			//console.log("Checking for updates", new Date(time).format(), nodes)
			ICNodes.load({
				"action": "poll_nodes",
				id: nodes,
				since: new Date(cachedSince).format() // Make sure we pass a date-agnostic date back
			}, null, true);
		}
	}

	/**
	* Poll the server for changes to visible / known about activities
	*/
	window.setInterval(poll, Config.pollInterval); // Poll every 10 seconds
};
