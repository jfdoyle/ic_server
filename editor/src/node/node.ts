///<reference path="../editor/editor.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

const enum ICNodeType {
	Dummy = -1,
	Organisation = 0,
	Templates = 1,
	Project = 2,
	Folder = 3,
	Group = 4,
	// everything above here is folders, everything below is screens
	Screen = 10,
	Theme = 11,
	Link = 12,
	Template = 13
}

const ICNodeTypeChildren: {[name: number]: ICNodeType[]} = {
	[ICNodeType.Dummy]: [ICNodeType.Organisation],
	[ICNodeType.Organisation]: [ICNodeType.Templates, ICNodeType.Project, ICNodeType.Folder],
	[ICNodeType.Templates]: [ICNodeType.Folder, ICNodeType.Theme, ICNodeType.Template],
	[ICNodeType.Project]: [ICNodeType.Group, ICNodeType.Screen, ICNodeType.Link],
	[ICNodeType.Folder]: [ICNodeType.Templates, ICNodeType.Project],
	[ICNodeType.Group]: [ICNodeType.Screen, ICNodeType.Link],
	[ICNodeType.Screen]: [],
	[ICNodeType.Theme]: [],
	[ICNodeType.Link]: [],
	[ICNodeType.Template]: []
}

const ICNodeTypeName: {[name: number]: string} = {
	[ICNodeType.Dummy]: "dummy",
	[ICNodeType.Organisation]: "organisation",
	[ICNodeType.Templates]: "templates",
	[ICNodeType.Project]: "project",
	[ICNodeType.Folder]: "folder",
	[ICNodeType.Group]: "group",
	[ICNodeType.Screen]: "screen",
	[ICNodeType.Theme]: "theme",
	[ICNodeType.Link]: "link",
	[ICNodeType.Template]: "template"
}

function ICNodeTypeHtml(type: ICNodeType): string {
	switch (type) {
		case ICNodeType.Dummy:
		case ICNodeType.Folder:
			return "%%content%%";
		case ICNodeType.Group:
			return "<div unwrap>%%content%%</div>";
		case ICNodeType.Organisation:
			return "<title>Infuze Creator</title>"
				+ "<link rel=\"stylesheet\" type=\"text/css\" href=\"activity/build/activity.css\"/>"
				+ "<link rel=\"stylesheet\" type=\"text/css\" href=\"../assets/3/theme.css\"/>" // TODO: Make the theme choice in the editor
				+ "<script type=\"text/javascript\" src=\"activity/build/activity.js\" defer></script>"
				+ "%%content%%";
		case ICNodeType.Project:
			return "<ic-screens><ic-screen>%%content%%</ic-screen></ic-screens>";
		// everything above here is folders, everything below is screens
		case ICNodeType.Screen:
			return "<ic-box></ic-box>";
		case ICNodeType.Templates:
		case ICNodeType.Theme:
		case ICNodeType.Link:
		case ICNodeType.Template:
			return "";
	}
	console.error("ICNodeTypeHtml() with invalid type:", this);
	return ""; // TODO: Throw error?
}


const enum ICNodeFlags {
	LOCK = (1 << 0), // Locked - No changes permitted
	DELETED = (1 << 1), // Needs Deletion (will not be in build)
	CHECK = (1 << 2), // Unsure - Check Comments
	IMPORTANT = (1 << 3), // Important
	URGENT = (1 << 4), // Very Important
	IGNORE = (1 << 5), // Don't Use (will not be in build)
	STATE_INCOMPLETE = (1 << 6), // Not Complete
	STATE_CHANGE = (1 << 7), // Changes Required
	STATE_CHECK = (1 << 8), // Needs Checking
	STATE_COMPLETE = (1 << 9), // Signed Off
	QA_BAD = (1 << 10), // QA Bad
	QA_UNSURE = (1 << 11), // QA Unsure
	QA_GOOD = (1 << 12), // QA Good
	STAR_NONE = (1 << 13), // Below Average
	STAR_HALF = (1 << 14), // Average
	STAR_FULL = (1 << 15), // Above Average
	ACCESSIBLE = (1 << 16), // Accessible
	IMAGE = (1 << 17), // Image
	SOUND = (1 << 18), // Sound
	TEXT = (1 << 19), // Text
	VIDEO = (1 << 20) // Video
}

class ICNode {
	static privateKeys = ["$changed", "$children", "$content", "$dirty", "$dom", "$html", "$pretty", "$save", "$screen", "$watchers"];

	// Anything starting with an underscore is pulled directly into the database
	public _id: number;
	public _parent: number;
	public _children: number[];
	public _name: string;
	public _flags: number;
	public _date: string;
	public _files: string[];
	public _build: Object;
	public _type: ICNodeType;

	public _created: string;
	public _creator: string;
	public _editor: string;

	private _html: string;

	// Starting with a $ are editor only, and hidden from hasOwnProperty

	private $changed: boolean;
	private $children: boolean; // clones to later remove
	private $content: HTMLElement; // to be able to replace it
	private $dirty: boolean; // We know it's dirty but don't want to replace the bad copy
	private $dom: DocumentFragment;
	public $html: string; // Should never be written to outside here - accessor?
	private $pretty: string;
	private $save: boolean; // Set when we're saving so accept next changes
	private $screen: HTMLElement;
	private $watchers: string[];

	// Anything else...
	//	public data: Object;
	//	public theme: Object;
	//	public modules: Object;
	//	public feedback: Object;

	constructor(data?: Object | ICNode) {
		if (data) {
			data.clone(this);
		}
		ICNode.privateKeys.forEach((key) => {
			Object.defineProperty(this, key, {
				"enumerable": false,
				"writable": true
			});
		});
	}

	toJSON(): Object {
		var ret: any = {};

		for (var property in this) {
			if (this.hasOwnProperty(property)) {
				ret[property] = this[property];
			}
		}
		return ret;
	};

	// Getters and setters

	get isHidden(): boolean {
		return !!(this._flags & ICNodeFlags.DELETED || this._flags & ICNodeFlags.IGNORE);
	}

	get changed(): boolean {
		return !!this.$changed;
	}

	set changed(val: boolean) {
		if (val === false) {
			this.$html = undefined;
		}
		this.$changed = val;
	}

	get content(): HTMLElement {
		if (this.$content) {
			return this.$content;
		}
		var walker = document.createNodeIterator(this.dom, NodeFilter.SHOW_TEXT, null, false);

		for (var node = walker.nextNode(); node; node = walker.nextNode()) {
			var text = node.textContent;

			if (text.includes("%%content%%")) {
				var split = text.regex(/^([\s\S]*?)(%%content%%)([\s\S]*?)/) as string[];

				if (split[0] || split[2]) {
					if (split[0]) {
						node.parentNode.insertBefore(document.createTextNode(split[0]), node);
					}
					if (split[2]) {
						node.parentNode.insertBefore(document.createTextNode(split[2]), node.nextSibling);
					}
					node.textContent = split[1];
				}
				return this.$content = node as HTMLElement;
			}
		}
	}

	get screen(): HTMLElement {
		if (this.$screen) {
			return this.$screen;
		}
		var content = this.content;

		if (content) {
			for (var parent = content.parentElement; parent; parent = parent.parentElement) {
				if (parent.tagName === "IC-SCREEN") {
					return this.$screen = parent;
				}
			}
		}
	}

	get dom(): DocumentFragment {
		if (!this.$dom) {
			var template = createElement("template") as HTMLTemplateElement;

			template.innerHTML = this.html;
			this.$dom = template.content;
			this.$content = this.$screen = undefined;
		}
		return this.$dom;
	}

	get pretty(): string {
		if (!this.$pretty) {
			this.formatHtml();
		}
		return this.$pretty;
	}

	get html(): string {
		return this._html || ICNodeTypeHtml(this._type);
	}

	set html(html: string) {
		if (html === ICNodeTypeHtml(this._type)) {
			html = "";
		}
		if (this._html !== html) {
			if (this.$html === undefined) {
				this.$html = this._html;
			}
			this._html = html;
			this.$dom = this.$pretty = undefined;
			for (var parent = this.getParent(); parent; parent = parent.getParent()) {
				if (parent.$dom) {
					parent.$dom = undefined;
				}
			}
			this.$changed = true;
		}
	}

	get icon(): string {
		switch (this._type) {
			case ICNodeType.Dummy:
				return "fa-stop";
			case ICNodeType.Organisation:
				return "fa-university";
			case ICNodeType.Templates:
				return "fa-cubes";
			case ICNodeType.Project:
				return "fa-database";
			case ICNodeType.Folder:
				return "fa-folder-open";
			case ICNodeType.Group:
				return "fa-object-ungroup";
			// everything above here is folders, everything below is screens
			case ICNodeType.Screen:
				return "fa-object-group";
			case ICNodeType.Theme:
				return "fa-paint-brush";
			case ICNodeType.Link:
				return "fa-link";
			case ICNodeType.Template:
				return "fa-cube";
		}
		console.error("Activity.getIcon() with invalid type:", this);
		return "fa-question-circle"; // TODO: Throw error?
	}

	get index(): number {
		return this._id ? ICNodes.get(this._parent)._children.indexOf(this._id) : -1;
	}

	get isGroup(): boolean {
		return this._type < ICNodeType.Screen;
	}

	get hasChildren(): boolean {
		return !!this._children;
	}

	get saving(): boolean {
		return !!this.$save;
	}

	set saving(state: boolean) {
		this.$save = state;
	}

	get dirty(): boolean {
		return !!this.$dirty;
	}

	set dirty(state: boolean) {
		this.$dirty = state;
	}

	get watchers(): string[] {
		return this.$watchers || [];
	}

	set watchers(watchers: string[]) {
		this.$watchers = watchers || [];
	}

	// Methods

	formatHtml(): string {
		if (this.$children) {
			this.$dom = this.$screen = this.$content = this.$children = undefined;
		}
		var dom = this.dom;

		if (!dom) {
			console.warn("WARNING: No DOM available", this);
			this.$pretty = undefined;
			return;
		}
		var output: string[] = [],
			pretty: string[] = [],
			selfClosing = /^(area|base|br|col|embed|hr|img|input|keygen|link|meta|param|source|track|wbr)$/i,
			selfClosingSVG = /^(path|circle|ellipse|line|rect|use|stop|polyline|polygon)$/i,
			index: {[widget: string]: number} = {
				"act": 1,
				"acts": 1,
				"drag": 1,
				"drop": 1,
				"option": 1,
				"select": 1,
				"text": 1,
				"toggle": 1,
				"p": 1
			},
			indexes: {[act: string]: {[widget: string]: number}} = {},
			removeClass = /\s*(?:ict-[a-z0-9_\-]+|mce-[a-z0-9_\-]+|ice-[a-z0-9_\-]+)/g,
			removeClassAll = /\s*(?:mce-[a-z0-9_\-]+|ice-[a-z0-9_\-]+)/g,
			multipleActivities = dom.querySelectorAll("ic-activity,ic-activities").length > 1,
			walker = document.createNodeIterator(dom, NodeFilter.SHOW_TEXT | NodeFilter.SHOW_ELEMENT | NodeFilter.SHOW_COMMENT, this.$children ? function(node: HTMLElement) {
				return node.nodeType === Node.ELEMENT_NODE && node.hasAttribute("ice") ? NodeFilter.FILTER_REJECT : NodeFilter.FILTER_ACCEPT;
			} as any : null, false),
			walk = function(node: Node, prefix: string) {
				for (; node; node = walker.nextNode() as Element) {
					switch (node.nodeType) {
						case Node.COMMENT_NODE:
							var commentNode = node as Node,
								text = "<!-- " + commentNode.textContent.trim() + "-->";

							output.push(text);
							pretty.push(prefix + text);
							break;

						case Node.TEXT_NODE:
							var textNode = node as Node,
								text = textNode.textContent.replace(/[\u00A0-\u9999<>\&]/gim, function(i) {
									return "&#" + i.charCodeAt(0) + ";";
								});

							output.push(text);
							pretty.push(prefix + text);
							break;

						case Node.ELEMENT_NODE:
							var elementNode = node as HTMLElement,
								nodeName = elementNode.nodeName,
								tagName = nodeName.toLowerCase(),
								isICTag = tagName.startsWith("ic-"),
								attributes = "",
								prettyAttributes = "",
								state: {[name: string]: boolean} = {},
								id: string,
								thisIndex = index,
								parentId = "",
								isActivity = false;

							switch (tagName) {
								case "ic-activity":
								case "ic-activities":
									isActivity = true;
									if (tagName === "ic-activity") {
										id = "act";
									} else {
										id = "acts";
									}
									indexes[id + index[id]] = {
										"act": 1,
										"drag": 1,
										"drop": 1,
										"option": 1,
										"select": 1,
										"text": 1,
										"toggle": 1,
									}
									break;

								case "ic-draggable":
									id = "drag";
									break;

								case "ic-droppable":
									id = "drop";
									// TODO: be a bit better at checking for empty state
									state["empty"] = !elementNode.firstElementChild;
									break;

								case "ic-option":
									id = "option";
									break;

								case "ic-select":
									id = "select";
									// TODO: check for empty state
									break;

								case "ic-text":
									id = "text";
									// TODO: check for empty state
									break;

								case "ic-toggle":
									id = "toggle";
									break;

								case "p":
									id = "p";
									break;

								default:
									id = null;
									break;
							}
							if (isICTag) {
								elementNode.fixState(state, index[id] || -1);
								if (elementNode.hasAttribute("ic-rounding")) {
									elementNode.style.width = elementNode.style.height = "";
								}
							}
							if (id) {
								if (id === "p") {
									id += index[id]++;
									if (elementNode.style.position === "relative") {
										elementNode.style.position = "";
									}
								} else {
									if (!isActivity) {
										for (var parent = elementNode.parentElement; parent; parent = parent.parentElement) {
											if (parent.nodeName.toLowerCase().startsWith("ic-") && /^acts?\d$/.test(parent.id)) {
												thisIndex = indexes[parentId = parent.id];
												break;
											}
										}
									}
									id += thisIndex[id]++;
									if (multipleActivities) {
										id = parentId + id;
									}
								}
								elementNode.setAttribute("id", id);
							} else if (isICTag) { // || (tagName === "p" && /^p\d+$/.test(elementNode.id))
								elementNode.removeAttribute("id");
							}
							for (var i = 0; i < elementNode.attributes.length; i++) {
								var attribute = elementNode.attributes[i],
									name = attribute.name,
									value = attribute.value;

								if (["ice", "contenteditable", "spellcheck", "readonly"].includes(name)
									|| /^(data-mce|mce-data)/.test(name) // used for the editor, may already be filtered out above
									|| (/^(data-json|style|class)/.test(name) && !value)) { // Don't care about these empty attributes
									continue;
								}
								if (name === "style") {
									value = value.replace(/([:;])\s+/g, "$1").replace(/&/g, "&amp;").replace(/"/g, "'").replace(/;?$/, ";");
									attributes += " " + (name + "=\"" + value + "\"");
									prettyAttributes += " " + (name + "=\"" + value + "\"");
								} else {
									if (name === "class") {
										value = value.replace(isICTag ? removeClass : removeClassAll, "").trim();
										if (!value) {
											continue;
										}
									}
									if (!isICTag || name !== "id") {
										attributes += " " + (name + (value ? "=\"" + value.replace(/&/g, "&amp;").replace(/\"/g, "&quot;") + "\"" : ""));
									}
									if (!isICTag || !["data-anchor", "data-mark", "data-percent", "data-points"].includes(name)) {
										prettyAttributes += " " + (name + (value ? "=\"" + value.replace(/&/g, "&amp;").replace(/\"/g, "&quot;") + "\"" : ""));
									}
								}
							}
							if (selfClosing.test(tagName) || (selfClosingSVG.test(tagName) && !elementNode.firstChild)) {
								output.push("<" + tagName + attributes + "/>");
								pretty.push(prefix + "<" + tagName + prettyAttributes + "/>");
							} else if (tagName === "template") {
								var oldWalker = walker;

								if (pretty.length && pretty.last()) {
									pretty.push("");
								}
								output.push("<" + tagName + attributes + ">");
								pretty.push(prefix + "<" + tagName + prettyAttributes + ">");
								walker = document.createNodeIterator((node as HTMLTemplateElement).content, NodeFilter.SHOW_TEXT | NodeFilter.SHOW_ELEMENT | NodeFilter.SHOW_COMMENT, null, false);
								walk(walker.nextNode() as Element, prefix + "\t");
								output.push("</" + tagName + ">");
								pretty.push(prefix + "</" + tagName + ">", "");
								walker = oldWalker;
							} else {
								var child = elementNode.firstChild;

								if (child && (child.nodeType === Node.TEXT_NODE || child.nodeType === Node.ELEMENT_NODE || child.nodeType === Node.COMMENT_NODE)) {
									output.push("<" + tagName + attributes + ">");
									pretty.push(prefix + "<" + tagName + prettyAttributes + ">");
									walk(walker.nextNode() as Element, prefix + "\t");
									output.push("</" + tagName + ">");
									pretty.push(prefix + "</" + tagName + ">");
								} else {
									output.push("<" + tagName + attributes + "></" + tagName + ">");
									pretty.push(prefix + "<" + tagName + prettyAttributes + "></" + tagName + ">");
								}
							}
							break;

						default:
							console.error("Error: Unknown TreeWalker node", node);
							break;
					}
					if (!node.nextSibling) {
						break;
					}
				}
			};

		walk(walker.nextNode() as Element, "");
		this.$pretty = pretty.join("\n") + "\n";
		return this.html = output.join("");
	}

	static requireList: number[] = [];

	/**
	 * Build the DOM tree for an activity. Wrap it in parents, and include any
	 * children.
	 * 
	 * @param skipParent
	 *    true means don't include parents (being called internally so we are
	 *       the parent calling).
	 *    false means build our parents and wrap us.
	 *    # means build as if we have a different node as our parent (used for
	 *       previews / templates).
	 * 
	 * @param child Only build this child, not any others that might exist.
	 */
	private internalBuild(skipParent?: boolean | number, child?: number): DocumentFragment {
		if (this.$children) {
			this.$dom = this.$screen = this.$content = this.$children = undefined;
		}
		var childList = !child && child !== 0 ? this.getChildrenId() : [child],
			dom = this.dom,
			content = this.content,
			screen = this.screen,
			screenOrContent = screen || content,
			parentNode = (screenOrContent ? screenOrContent.parentNode : null) || dom;

		if (!isBoolean(skipParent)) {
			ICNode.requireList = [];
		}
		for (var i = 0; i < dom.children.length; i++) {
			var el = dom.children[i];

			if (!el.hasAttribute("ice")) {
				el.setAttribute("ice", String(this._id));
			}
		}
		if (screenOrContent) {
			if (childList.length) {
				childList.forEach((id) => {
					if (skipParent === false || this._children.includes(id)) {
						var activity = ICNodes.get(id),
							clone: HTMLElement;
						if (!id
							|| (activity && skipParent !== false
								&& (activity.is(ICNodeType.Organisation)
									|| activity.is(ICNodeType.Templates)
									|| activity.is(ICNodeType.Project)
									|| !!(activity._flags & (ICNodeFlags.DELETED | ICNodeFlags.IGNORE))))) {
							return;
						}
						if (!activity) {
							console.error("Error: Empty child for building", this._id, id);
							ICNode.requireList.pushOnce(id);
							return;
						}
						if (activity.is(ICNodeType.Group) && skipParent !== false) {
							activity.internalBuild(true);
						}
						clone = activity.dom.cloneNode(true) as HTMLElement;

						for (var i = 0; i < clone.children.length; i++) {
							var child = clone.children[i];

							if (!child.hasAttribute("ice")) {
								child.setAttribute("ice", String(id));
							}
						}
						// check if there are multiple children
						if (clone.children.length > 1) {
							var div = createElement("div");

							div.setAttribute("unwrap", "");
							div.setAttribute("ice", String(id));
							div.appendChild(clone);
							clone = div;
						}
						// append to parent
						if (screen && (clone.children.length !== 1 || (clone.children[0] || {} as Element).nodeName !== "IC-SCREEN")) {
							var el = screen.cloneNode(true) as HTMLElement,
								walker = document.createNodeIterator(el, NodeFilter.SHOW_TEXT, null, false),
								node: Node;

							while ((node = walker.nextNode())) {
								if (node.textContent === "%%content%%") {
									node.parentNode.replaceChild(clone, node);
									break;
								}
							}
							parentNode.insertBefore(el, screen);
						} else {
							parentNode.insertBefore(clone, screenOrContent);
						}
					} else {
						console.error("Error: Trying to add a non child (" + id + ") to build.");
					}
				});
				querySelectorAll(dom, "div[unwrap][ice]").forEach((group: HTMLElement) => {
					var parentNode = group.parentNode,
						id = group.getAttribute("ice");

					while (group.hasChildNodes()) {
						if (group.firstChild.nodeType === Node.ELEMENT_NODE) {
							(group.firstChild as HTMLElement).setAttribute("ice", id);
						}
						parentNode.insertBefore(group.firstChild, group);
					}
					parentNode.removeChild(group);
				});
			}
			parentNode.removeChild(screenOrContent);
			this.$children = true;
		}
		if (isNumber(skipParent)) {
			dom = ICNodes.get(skipParent).internalBuild(false, this._id);
		} else if (!skipParent && this._parent && ICNodes.get(this._parent).html) {
			// console.info("Building parent", this._id, this._parent, dom)
			dom = this.getParent().internalBuild(false, this._id);
		}
		return dom;
	}

	/**
	 * Load all the children of this node - doesn't do anything beyond that
	 */
	loadChildren() {
		var want: number[] = [],
			resolver: (value?: {} | PromiseLike<{}>) => void,
			promise = new Promise((resolve) => resolver = resolve),
			checkNeeded = function(id: number) {
				if (id) {
					var activity = ICNodes.get(id);

					if (activity) {
						activity.getChildrenId().forEach(checkNeeded);
					} else {
						want.pushOnce(id);
					}
				}
			};

		this.getChildrenId().forEach(checkNeeded);
		if (!want.length) {
			resolver();
		} else {
			ICNodes.load(want)
				.then(() => {
					this.loadChildren()
						.then(() => {
							resolver();
						});
				});
		}
		return promise;
	}

	/**
	 * Builds the DocumentFragment for the selected node, simply prefix with <DOCTYPE>
	 */
	build(fakeParent?: number): Promise<DocumentFragment> {
		var need: number[] = [],
			resolver: (value?: {} | PromiseLike<{}>) => void,
			promise = new Promise((resolve) => resolver = resolve),
			loadChildren = () => {
				this.loadChildren().then(() => {
					resolver();
				});
			};

		this.getParentsId().forEach(function(id) {
			if (id && !ICNodes.has(id)) {
				need.push(id);
			}
		});
		if (need.length) {
			ICNodes.load(need).then(loadChildren);
		} else {
			loadChildren();
		}
		return promise.then((): DocumentFragment => {
			return this.internalBuild(fakeParent);
		});
	}

	getChildren(): ICNode[] {
		// console.log("getChildren", this)
		var i,
			childList = this.getChildrenId(),
			children = [];

		for (i = 0; childList && i < childList.length; i++) {
			children.push(ICNodes.get(childList[i]));
		}
		return children;
	}

	getChildrenId(): number[] {
		return this._children.slice(0) || [];
	}

	getParent(): ICNode {
		return ICNodes.get(this._parent);
	}

	getParentId(): number {
		return this._parent;
	}

	getSiblings(): ICNode[] {
		return this._id ? ICNodes.get(this._parent).getChildren() : [];
	}

	getSiblingsId(): number[] {
		return this._id ? ICNodes.get(this._parent).getChildrenId() : [];
	}

	/**
	 * Get all parents of an Activity, optionally including yourself, drilling up the tree
	 */
	getParents(andSelf?: boolean): ICNode[] {
		var parent = this.getParent(),
			parents = andSelf ? [this as ICNode] : [];

		while (parent) {
			parents.push(parent);
			parent = parent.getParent();
		}
		return parents;
	}

	/**
	 * Get all parents of an Activity, optionally including yourself, drilling up the tree
	 */
	getParentsId(andSelf?: boolean): number[] {
		var parent = this.getParent(),
			parents = andSelf ? [this._id] : [];

		while (parent) {
			parents.push(parent._id);
			parent = parent.getParent();
		}
		return parents;
	}

	/**
	 * Return a Promise, get all Template folders that this screen can access.
	 */
	getTemplateFolders(): Promise<ICNode[]> {
		var resolver = (resolve: (value?: ICNode[] | PromiseLike<ICNode[]>) => void) => {
			var templates: ICNode[] = [],
				load: number[] = [];

			this.getParents(true).forEach(function(parent) {
				parent.getSiblingsId().forEach(function(id) {
					var activity = ICNodes.get(id);

					if (!activity) {
						load.push(id);
					} else if (activity.is(ICNodeType.Templates)
						&& !(activity._flags & ICNodeFlags.DELETED ||
							(!Editor.permissions.isRoot && activity._flags & ICNodeFlags.IGNORE))) {
						templates.push(activity);
					}
				});
			});
			if (load.length) {
				ICNodes.load(load).then(resolver.bind(this, resolve));
			} else {
				resolve(templates);
			}
		};

		return new Promise(resolver);
	}

	/**
	 * Return a Promise, get all Template Screens that are descendants of
	 * Template folders this screen can access.
	 */
	getTemplates(): Promise<ICNode[]> {
		return this.getTemplateFolders().then((templates) => {
			var resolver = (resolve: (value?: ICNode[] | PromiseLike<ICNode[]>) => void) => {
				var themes: ICNode[] = [],
					load: number[] = [],
					checkChildren = function(parent: ICNode) {
						parent.getChildrenId().forEach(function(id) {
							var activity = ICNodes.get(id);

							if (!activity) {
								load.push(id);
							} else if (activity.is(ICNodeType.Folder)) {
								checkChildren(activity);
							} else if (activity.is(ICNodeType.Template)
								&& !(activity._flags & ICNodeFlags.DELETED ||
									(!Editor.permissions.isRoot && activity._flags & ICNodeFlags.IGNORE))) {
								themes.push(activity);
							}
						});
					};

				templates.forEach(function(parent) {
					checkChildren(parent);
				});
				if (load.length) {
					ICNodes.load(load).then(resolver.bind(this, resolve));
				} else {
					resolve(themes);
				}
			};

			return new Promise(resolver);
		});
	}

	/**
	 * Return a Promise, get all Themes that are direct descendants of Template
	 * folders this screen can access.
	 */
	getThemes(): Promise<ICNode[]> {
		return this.getTemplateFolders().then((templates) => {
			var resolver = (resolve: (value?: ICNode[] | PromiseLike<ICNode[]>) => void) => {
				var themes: ICNode[] = [],
					load: number[] = [];

				templates.forEach(function(parent) {
					parent.getChildrenId().forEach(function(id) {
						var activity = ICNodes.get(id);

						if (!activity) {
							load.push(id);
						} else if (activity.is(ICNodeType.Theme)) {
							themes.push(activity);
						}
					});
				});
				if (load.length) {
					ICNodes.load(load).then(resolver.bind(this, resolve));
				} else {
					resolve(themes);
				}
			};

			return new Promise(resolver);
		});
	}

	is(check: ICNodeType): boolean {
		return this._type === check;
	}

	set(data: Object | ICNode): ICNode {
		var isEditing = Editor.currentActivity && Editor.currentActivity._id === this._id,
			thisData = this as ICNode,
			newData = data as ICNode,
			checkValidKeys = function(key: string, target: ICNode) {
				if (key === "_html" && target.$html) {
					// Skip if the html is equal to our current "saved" html
					return !(target.$html === this[key]);
				}
				return !key.startsWith("$");
			},
			copyData = function() {
				var changeHtml = false,
					changeName = false;

				thisData._build = newData._build;
				thisData._children = newData._children;
				thisData._created = newData._created;
				thisData._creator = newData._creator;
				thisData._date = newData._date;
				thisData._editor = newData._editor;
				thisData._files = newData._files;
				thisData._flags = newData._flags;
				thisData._parent = newData._parent;
				thisData._type = newData._type;
				if (thisData._name !== newData._name) {
					thisData._name = newData._name;
					changeName = true;
				}
				if (thisData._html !== newData._html) {
					thisData.html = newData._html;
					changeHtml = true;
				}
				thisData.saving = thisData.dirty = thisData.changed = false;
				if (changeName) {
					$.tree.add(String(thisData._id));
				}
				if (changeHtml && isEditing) {
					Editor.setActivity(thisData);
				}
			};

		if (this.saving || !this.changed || !isEditing) {
			copyData();
		} else if (!this.dirty && !data.equals(this, checkValidKeys)) {
			if (confirm("This screen has been updated by " + ((data as ICNode)._editor || "someone") + ", do you wish to undo your changes?")) {
				copyData();
			} else {
				this.dirty = true;
			}
		}
		return this;
	}
};

fixPrototype(ICNode);
