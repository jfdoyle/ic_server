///<reference path="nodes.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace Templates {

	function clickType() {
		var $this = $(this);

		$("#ic_add_activity")
			.children("div")
			.children("div")
			.css("display", "")
			.eq($this.data("index"))
			.css("display", "block");
		$this.addClass("active").siblings().removeClass("active");
	}

	function addDefaultActivity(event) {
		var activity = new ICNode(event.data);

		activity._parent = parseInt($.tree.closest(), 10);
		console.log("Adding activity: ", activity);
		if ($("#ic_add_activity.ui-dialog-content").dialog("destroy").length) {
			$("#aba-button-save").button("enable");
			alert("Please remember that this activity is not saved until you hit the save button!");
		}
		Editor.setActivity(activity);
		$.tree.cancel();
		if (!Editor.change) {
			Editor.change = true;
			//			Editor.fixButtons();
		}
		return false;
	}

	function create() {
		var i: number,
			j: number,
			k: number,
			folder: ICNode,
			section: ICNode,
			$section: JQuery,
			$subsection: JQuery,
			template: ICNode,
			$type: JQuery,
			index: number,
			active: string,
			nodeId = parseInt($.tree.currentId, 10),
			parents = ICNodes.getParents(nodeId),
			sources: ICNode[] = [],
			folders = ICNodes.getChildren(3),
			$add = $("#ic_add_activity"),
			$list = $add.children("ul"),
			$divs = $add.children("div"),
			makeTemplate = function(activity: ICNode) {
				//					console.log("makeTemplate", activity)
				var i,
					data = {
						_id: 0,
						_name: activity._name,
						"type": activity._type
					};

				for (i in activity) {
					if (i[0] !== "_" && activity.hasOwnProperty(i)) {
						data[i] = activity[i];
					}
				}
				return $.make("div", "template",
					$.make("span", $.make("span", "=" + activity._name)),
					$.make("div", $.make("img", "src=assets/" + activity._id + "/_screenshot.png", "data-id=" + activity._id)))
					.on("click", null, data, addDefaultActivity);
			};

		active = $list.children(".active").text();
		$list.empty();
		$divs.empty();

		for (i = 0; i < parents.length; i++) {
			sources.push.apply(sources, parents[i].getChildren().filter(function(activity) {
				return activity.is(ICNodeType.Templates);
			}));
		}

		console.log("templates:", sources)

		for (i = 0; i < sources.length; i++) {
			folders = sources[i].getChildren();
			for (j = 0; j < folders.length; j++) {
				folder = folders[j];
				if (folder.is(ICNodeType.Folder)) {
					continue;
				}
				$section = $subsection = null;
				index = $list.children().length;
				$.make("li", "=" + folder._name, active === folder._name ? "active" : null).data("index", index).appendTo($list).on("click", clickType);
				$type = $.make("div").appendTo($divs);
				for (j = 0; folder._children && j < folder._children.length; j++) {
					section = ICNodes.get(folder._children[j]);
					if (section.is(ICNodeType.Folder)) {
						$subsection = null;
						for (k = 0; k < section._children.length; k++) {
							template = ICNodes.get(section._children[k]);
							if (template.is(ICNodeType.Screen) || template.is(ICNodeType.Link)) {
								if (!$subsection) {
									$subsection = $type.children().filter(function() {
										return $(">p", this).text() === section._name;
									});
									if (!$subsection.length) {
										$subsection = $.make("div", $.make("p", "=" + section._name), $.make("hr")).appendTo($type);
									}
								}
								makeTemplate(template).appendTo($subsection);
							}
						}
					} else {
						$section = $section || $.make("div", $.make("p", "=" + folder._name), $.make("hr")).prependTo($type);
						makeTemplate(section).appendTo($section);
					}
				}
			}
		}

		$list.children().sort(function(a, b) {
			var a_text = a.textContent,
				b_text = b.textContent;
			return a_text > b_text ? 1 : a_text < b_text ? -1 : 0;
		}).appendTo($list);
		clickType.call($list.children(active ? ".active" : undefined)[0]);
		$("#ic_add_activity").dialog({
			width: window.innerWidth * 0.75,
			height: window.innerHeight * 0.75,
			modal: true,
			close: function() {
				$("#ic_add_activity").dialog("destroy");
			},
			buttons: {
				"Cancel": function() {
					$("#ic_add_activity").dialog("destroy");
				}
			}
		});
	}
};
