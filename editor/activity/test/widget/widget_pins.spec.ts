///<reference path="../core.ts" />
/*
 * Copyright &copy; 2013 Infuze Ltd, All Rights Reserved.
 */

namespace test {
	type T = ic.PinsWidget;
	let selector = ic.PinsWidget.prototype.selector;

	let $test = $(`<ic-screen>
		<ic-activities>
			<ic-anchor></ic-anchor>

			<p>PinsWidget</p>

			<ic-activity>
				<ic-pins>Pins 0</ic-pins>

				<ic-pins>Pins 1</ic-pins>
			</ic-activity>
		</ic-activities>
	</ic-screen>`).appendTo("ic-screens");
};
