///<reference path="../core.ts" />
/*
 * Copyright &copy; 2013 Infuze Ltd, All Rights Reserved.
 */

namespace test {
	type T = ic.TimerWidget;
	let selector = ic.TimerWidget.prototype.selector;

	let $test = $(`<ic-screen>
		<ic-activities>
			<ic-anchor></ic-anchor>

			<p>TimerWidget</p>

			<ic-activity>
				<ic-timer>Timer 0</ic-timer>

				<ic-timer>Timer 1</ic-timer>
			</ic-activity>
		</ic-activities>
	</ic-screen>`).appendTo("ic-screens");
};
