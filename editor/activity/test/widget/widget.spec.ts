///<reference path="../core.ts" />
/* 
 * Copyright &copy; 2013 Infuze Ltd, All Rights Reserved.
 */

namespace test {
	type T = ic.Widget;

	describeQueue("Core Widget tests", function() {
		describe("test.html", function() {
			Object.keys(ic).forEach((className) => {
				let widgetClass: { new (...args: any[]): ic.Widget; } = ic[className];

				if (widgetClass.prototype instanceof ic.Widget) {
					let selector = (widgetClass as any).selector;

					if (selector) {
						let count = $(selector).filter(function() {
							return !(this as HTMLElement).closest("header");
						}).length;

						(count ? it : xit)(className + " has at least a single element", function() {
							expect(count).toBeGreaterThan(0);
						});
					}
				}
			});
		});

		describe("State is being set correctly", function() {
			let $screen = $("#ic-screens-1"),
				screen = $screen[0],
				widget = screen.icWidget,
				oldState = widget.state.clone();

			afterEach(function() {
				if (Object.keys(widget.state).length > 0) {
					widget.state = {};
					widget.fixState();
				}
			});

			it("starts with no state", function() {
				expect(oldState).toEqual({ active: true });
			});

			it("adding a state works", function() {
				widget.addState("test" as any);
				expect(widget.state["test"]).toEqual(true);
				widget.removeState("test" as any);
			});

			it("adding a state adds a class", function() {
				widget.addState("test" as any);
				expect($screen.hasClass("ict-state-test")).toEqual(true);
				widget.removeState("test" as any);
			});

			it("removing a state works", function() {
				widget.addState("test" as any);
				widget.removeState("test" as any);
				expect(widget.state["test"]).toEqual(false);
			});

			it("removing a state removes a class", function() {
				widget.addState("test" as any);
				widget.removeState("test" as any);
				expect($screen.hasClass("ict-state-test")).toEqual(false);
			});

			it("toggling a state works", function() {
				widget.addState("test" as any);
				widget.toggleState("test" as any);
				expect(widget.state["test"]).toEqual(false);
			});

			it("toggling a state toggles a class", function() {
				widget.addState("test" as any);
				widget.toggleState("test" as any);
				expect($screen.hasClass("ict-state-test")).toEqual(false);
			});

			it("not having an attribute doesn't have a class", function() {
				expect($screen.hasClass("ict-test")).toEqual(false);
			});

			it("having an normal attribute doesn't add a class", function() {
				let oldClass = $screen.attr("class");
				screen.setAttribute("test", "");
				widget.fixState();
				expect($screen.attr("class")).toEqual(oldClass);
				screen.removeAttribute("test");
				widget.fixState();
			});

			it("having an ic-prefixed attribute adds a class", function() {
				screen.setAttribute("ic-test", "");
				widget.fixState();
				expect($screen.hasClass("ict-test")).toEqual(true);
				screen.removeAttribute("ic-test");
				widget.fixState();
			});

			it("having an ic-prefixed attribute value adds a class", function() {
				screen.setAttribute("ic-test", "12345");
				widget.fixState();
				expect($screen.hasClass("ict-test-12345")).toEqual(true);
				screen.removeAttribute("ic-test");
				widget.fixState();
			});
		});
	});

	export function WidgetTests($widget: JQuery, widget: T[]) {
		function cleanup() {
			ic.Widget.forEach(function(widget) {
				if (Object.keys(widget.state).length > 0 && widget.element.closest("ic-activity")) {
					widget.state = {};
					widget.fixState();
				}
			});
			if ($widget.length > 0) {
				screensWidget["screenIndex"] = -1; // Fake it to make sure it's on the right screen
				screensWidget.screen = ($widget.closest("ic-screen")[0].icWidget as ic.ScreenWidget).index - 1;
			}
		}

		beforeEach(cleanup);
		afterAll(cleanup);

		WidgetNodeTests($widget, widget);

		describe("Widget tests", function() {
			it("Widget test requires one or more widgets", function() {
				expect($widget.length).toBeGreaterThan(0);
			});

			it("has the correct classname", function() {
				expect($widget.hasClass(widget[0].selector.replace(/^ic-([^\[,]+).*$/, "ict-$1"))).toEqual(true);
			});

			it("mouse move changes hover state", function() {
				$widget.eq(0).simulate("mouseenter");
				expect(widget[0].hasState("hover")).toEqual(true);
				$widget.eq(0).simulate("mouseleave");
				expect(widget[0].hasState("hover")).toEqual(false);
			});
		});
	}
};
