///<reference path="../core.ts" />
/*
 * Copyright &copy; 2013 Infuze Ltd, All Rights Reserved.
 */

namespace test {
	type T = ic.BoxWidget;
	let selector = ic.BoxWidget.prototype.selector;

	let $test = $(`<ic-screen>
		<ic-activities>
			<ic-anchor></ic-anchor>

			<p>BoxWidget</p>

			<ic-activity>
				<ic-box>Box 0</ic-box>

				<ic-box>Box 1</ic-box>
			</ic-activity>
		</ic-activities>
	</ic-screen>`).appendTo("ic-screens");

	describeQueue("BoxWidget", function() {
		let widget: T[] = [];
		let $widget = $test.find(selector).each(function(i, element: HTMLElement) {
			widget[i] = element.icWidget as T;
		});

		it("selector is \"ic-box\"", function() {
			expect(selector).toEqual("ic-box");
		});

		WidgetTests($widget, widget);
	});
};
