///<reference path="../core.ts" />
/*
 * Copyright &copy; 2013 Infuze Ltd, All Rights Reserved.
 */

namespace test {
	type T = ic.AudioWidget;
	let selector = ic.AudioWidget.prototype.selector;

	let $test = $(`<ic-screen>
		<ic-activities>
			<ic-anchor></ic-anchor>

			<p>AudioWidget</p>

			<ic-activity>
				<ic-audio>Audio 0</ic-audio>

				<ic-audio>Audio 1</ic-audio>
			</ic-activity>
		</ic-activities>
	</ic-screen>`).appendTo("ic-screens");
};
