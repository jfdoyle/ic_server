///<reference path="../core.ts" />
/* 
 * Copyright &copy; 2013 Infuze Ltd, All Rights Reserved.
 */

namespace test {
	type T = ic.ToggleWidget;
	let selector = ic.ToggleWidget.prototype.selector;

	let $test = $(`<ic-screen>
		<ic-activities>
			<ic-anchor></ic-anchor>

			<p>ToggleWidget</p>

			<ic-activity>
				<ic-toggle>Toggle 0</ic-toggle>

				<ic-toggle>Toggle 1</ic-toggle>

				<ic-toggleic-group="1">Toggle 2</ic-toggle>

				<ic-toggle ic-group="1">Toggle 3</ic-toggle>

				<ic-toggle ic-toggle="multiple">Toggle 4</ic-toggle>

				<ic-toggle ic-toggle="multiple">Toggle 5</ic-toggle>

				<ic-toggle ic-group="1" ic-toggle="multiple">Toggle 6</ic-toggle>

				<ic-toggle ic-group="1" ic-toggle="multiple">Toggle 7</ic-toggle>
			</ic-activity>

			<ic-activity>
				<ic-toggle id="ic-toggle-8">Toggle 8</ic-toggle>
			</ic-activity>
		</ic-activities>
	</ic-screen>`).appendTo("ic-screens");

	export function ToggleTests($widget: JQuery, widget: T[]) {
		ButtonTests($widget, widget);

		describe("ToggleWidget tests", function() {
			it("ToggleWidget test requires eight or more widgets", function() {
				expect($widget.length).toBeGreaterThan(7);
			});

			it("mouse click toggles checked state", function() {
				$widget.eq(0).simulate("mousedown").simulate("mouseup");
				expect(widget[0].hasState("checked")).toEqual(true);
				$widget.eq(0).simulate("mousedown").simulate("mouseup");
				expect(widget[0].hasState("checked")).toEqual(false);
			});

			it("mouse click elsewhere doesn't change checked state", function() {
				$widget.eq(1).simulate("mousedown");
				$widget.eq(0).simulate("mouseup").simulate("click");
				expect(widget[0].hasState("checked")).toEqual(false);
				expect(widget[1].hasState("checked")).toEqual(false);
				$widget.eq(0).simulate("mousedown");
				$widget.eq(1).simulate("mouseup").simulate("click");
				expect(widget[0].hasState("checked")).toEqual(false);
				expect(widget[1].hasState("checked")).toEqual(false);
			});

			describe("ic-toggle=single", function() {
				it("clicking doesn't check sibling", function() {
					$widget.eq(0).simulate("mousedown").simulate("mouseup");
					expect(widget[0].hasState("checked")).toEqual(true);
					expect(widget[1].hasState("checked")).toEqual(false);
				});

				it("clicking sibling unchecks self", function() {
					$widget.eq(0).simulate("mousedown").simulate("mouseup");
					$widget.eq(1).simulate("mousedown").simulate("mouseup");
					expect(widget[0].hasState("checked")).toEqual(false);
					expect(widget[1].hasState("checked")).toEqual(true);
				});

				it("clicking group 0 doesn't affect group 1", function() {
					$widget.eq(3).simulate("mousedown").simulate("mouseup");
					$widget.eq(0).simulate("mousedown").simulate("mouseup");
					expect(widget[0].hasState("checked")).toEqual(true);
					expect(widget[3].hasState("checked")).toEqual(true);
				});

				it("clicking group 1 doesn't affect group 0", function() {
					$widget.eq(0).simulate("mousedown").simulate("mouseup");
					$widget.eq(3).simulate("mousedown").simulate("mouseup");
					expect(widget[0].hasState("checked")).toEqual(true);
					expect(widget[3].hasState("checked")).toEqual(true);
				});

				it("clicking single doesn't affect multiple", function() {
					$widget.eq(4).simulate("mousedown").simulate("mouseup");
					$widget.eq(0).simulate("mousedown").simulate("mouseup");
					expect(widget[0].hasState("checked")).toEqual(true);
					expect(widget[4].hasState("checked")).toEqual(true);
				});

				it("clicking doesn't affect other activities", function() {
					$widget.eq(0).simulate("mousedown").simulate("mouseup");
					$widget.eq(7).simulate("mousedown").simulate("mouseup");
					expect(widget[0].hasState("checked")).toEqual(true);
					expect(widget[7].hasState("checked")).toEqual(true);
				});
			});

			describe("ic-toggle=multiple", function() {
				it("clicking doesn't check sibling", function() {
					$widget.eq(4).simulate("mousedown").simulate("mouseup");
					expect(widget[4].hasState("checked")).toEqual(true);
					expect(widget[5].hasState("checked")).toEqual(false);
				});

				it("clicking sibling doesn't uncheck self", function() {
					$widget.eq(4).simulate("mousedown").simulate("mouseup");
					$widget.eq(5).simulate("mousedown").simulate("mouseup");
					expect(widget[4].hasState("checked")).toEqual(true);
					expect(widget[5].hasState("checked")).toEqual(true);
				});

				it("clicking group 0 doesn't affect group 1", function() {
					$widget.eq(6).simulate("mousedown").simulate("mouseup");
					$widget.eq(4).simulate("mousedown").simulate("mouseup");
					expect(widget[4].hasState("checked")).toEqual(true);
					expect(widget[6].hasState("checked")).toEqual(true);
				});

				it("clicking group 1 doesn't affect group 0", function() {
					$widget.eq(4).simulate("mousedown").simulate("mouseup");
					$widget.eq(6).simulate("mousedown").simulate("mouseup");
					expect(widget[4].hasState("checked")).toEqual(true);
					expect(widget[6].hasState("checked")).toEqual(true);
				});

				it("clicking multiple doesn't affect single", function() {
					$widget.eq(0).simulate("mousedown").simulate("mouseup");
					$widget.eq(4).simulate("mousedown").simulate("mouseup");
					expect(widget[0].hasState("checked")).toEqual(true);
					expect(widget[4].hasState("checked")).toEqual(true);
				});
			});
		});
	}

	describeQueue("ToggleWidget", function() {
		let widget: T[] = [];
		let $widget = $test.find(selector).each(function(i, element: HTMLElement) {
			widget[i] = element.icWidget as T;
		});

		it("selector is \"ic-toggle\"", function() {
			expect(selector).toEqual("ic-toggle,ic-button[ic-button=toggle]");
		});

		ToggleTests($widget, widget);
	});
};
