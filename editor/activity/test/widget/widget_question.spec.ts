///<reference path="../core.ts" />
/*
 * Copyright &copy; 2013 Infuze Ltd, All Rights Reserved.
 */

namespace test {
	type T = ic.QuestionWidget;
	let selector = ic.QuestionWidget.prototype.selector;

	let $test = $(`<ic-screen>
		<ic-activities>
			<ic-anchor></ic-anchor>

			<p>QuestionWidget</p>

			<ic-activity>
				<ic-question>Question 0</ic-question>

				<ic-question>Question 1</ic-question>
			</ic-activity>
		</ic-activities>
	</ic-screen>`).appendTo("ic-screens");

	describeQueue("QuestionWidget", function() {
		let widget: T[] = [];
		let $widget = $test.find(selector).each(function(i, element: HTMLElement) {
			widget[i] = element.icWidget as T;
		});

		it("selector is \"ic-question\"", function() {
			expect(selector).toEqual("ic-question");
		});

		WidgetTests($widget, widget);
	});
};
