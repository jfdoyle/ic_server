///<reference path="../core.ts" />
/*
 * Copyright &copy; 2013 Infuze Ltd, All Rights Reserved.
 */

namespace test {
	type T = ic.NavWidget;
	let selector = ic.NavWidget.prototype.selector;

	let $test = $(`<ic-screen>
		<ic-activities>
			<ic-anchor></ic-anchor>

			<p>NavWidget</p>

			<ic-activity>
				<ic-nav></ic-nav>
			</ic-activity>
		</ic-activities>
	</ic-screen>`).appendTo("ic-screens");

	export function NavTests($widget: JQuery, widget: T[]) {
		WidgetTests($widget, widget);

		describe("NavWidget tests", function() {
			it("has the correct number of children", function() {
				let anchors = widget[0].get(ic.AnchorWidget);

				expect(widget[0].anchors.length - 1).toEqual(anchors.length);
			});

			xit("moves us to another activity", function() {
				let anchors = widget[0].get(ic.AnchorWidget);

				widget[0].anchors[1].icWidget.addState("active");
				console.log(anchors[0].hasState("active"), anchors[1].hasState("active"))
				expect(anchors[0].hasState("active")).toEqual(true);
				expect(anchors[1].hasState("active")).toEqual(false);
				$widget.eq(0).simulate("mousedown", { target: widget[0].anchors[2] }).simulate("mouseup", { target: widget[0].anchors[2] });
				console.log(anchors[0].hasState("active"), anchors[1].hasState("active"))
				expect(anchors[0].hasState("active")).toEqual(false);
				expect(anchors[1].hasState("active")).toEqual(true);
			});
		});
	}

	describeQueue("NavWidget", function() {
		let widget: T[] = [];
		let $widget = $test.find(selector).each(function(i, element: HTMLElement) {
			widget[i] = element.icWidget as T;
		});

		it("selector is \"ic-nav\"", function() {
			expect(selector).toEqual("ic-nav");
		});

		NavTests($widget, widget);
	});
};
