///<reference path="../core.ts" />
/*
 * Copyright &copy; 2013 Infuze Ltd, All Rights Reserved.
 */

namespace test {
	type T = ic.SelectWidget;
	let selector = ic.SelectWidget.prototype.selector;

	let $test = $(`<ic-screen>
		<ic-activities>
			<ic-anchor></ic-anchor>

			<p>SelectWidget</p>

			<ic-activity>
				<ic-select>Select 0</ic-select>

				<ic-select>Select 1</ic-select>
			</ic-activity>
		</ic-activities>
	</ic-screen>`).appendTo("ic-screens");
};
