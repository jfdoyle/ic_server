///<reference path="../core.ts" />
/*
 * Copyright &copy; 2013 Infuze Ltd, All Rights Reserved.
 */

namespace test {
	type T = ic.LastButtonWidget;
	let selector = ic.LastButtonWidget.prototype.selector;

	let $test = $(`<ic-screen>
		<ic-activities>
			<ic-anchor></ic-anchor>

			<p>LastButtonWidget</p>

			<ic-activity>
				<ic-button ic-button="last">Button 0</ic-button>

				<ic-button ic-button="last">Button 1</ic-button>

				<ic-button>Button</ic-button>
			</ic-activity>
		</ic-activities>
	</ic-screen>`).appendTo("ic-screens");

	export function LastButtonTests($widget: JQuery, widget: T[]) {
//		ButtonTests($widget, widget);

		describe("LastButtonWidget tests", function() {
			it("moves us to the last activity", function() {
				let anchors = widget[0].get(ic.AnchorWidget);

				anchors.first().addState("active");
				expect(anchors.first().hasState("active")).toEqual(true);
				expect(anchors.last().hasState("active")).toEqual(false);
				$widget.eq(0).simulate("mousedown").simulate("mouseup");
				expect(anchors.first().hasState("active")).toEqual(false);
				expect(anchors.last().hasState("active")).toEqual(true);
			});
		});
	}

	describeQueue("LastButtonWidget", function() {
		let widget: T[] = [];
		let $widget = $test.find(selector).each(function(i, element: HTMLElement) {
			widget[i] = element.icWidget as T;
		});

		it("selector is \"ic-button[ic-button=last]\"", function() {
			expect(selector).toEqual("ic-button[ic-button=last]");
		});

		LastButtonTests($widget, widget);
	});
};
