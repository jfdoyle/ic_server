///<reference path="../core.ts" />
/*
 * Copyright &copy; 2013 Infuze Ltd, All Rights Reserved.
 */

namespace test {
	type T = ic.NextButtonWidget;
	let selector = ic.NextButtonWidget.prototype.selector;

	let $test = $(`<ic-screen>
		<ic-activities>
			<ic-anchor></ic-anchor>

			<p>NextButtonWidget</p>

			<ic-activity>
				<ic-button ic-button="next">Next Button 0</ic-button>

				<ic-button ic-button="next">Next Button 1</ic-button>

				<ic-button>Button</ic-button>
			</ic-activity>
		</ic-activities>
	</ic-screen>`).appendTo("ic-screens");

	export function NextButtonTests($widget: JQuery, widget: T[]) {
//		ButtonTests($widget, widget);

		describe("NextButtonWidget tests", function() {
			it("moves us to the next activity", function() {
				let anchors = widget[0].get(ic.AnchorWidget);

				anchors[0].addState("active");
				expect(anchors[0].hasState("active")).toEqual(true);
				expect(anchors[1].hasState("active")).toEqual(false);
				$widget.eq(0).simulate("mousedown").simulate("mouseup");
				expect(anchors[0].hasState("active")).toEqual(false);
				expect(anchors[1].hasState("active")).toEqual(true);
			});
		});
	}

	describeQueue("NextButtonWidget", function() {
		let widget: T[] = [];
		let $widget = $test.find(selector).each(function(i, element: HTMLElement) {
			widget[i] = element.icWidget as T;
		});

		it("selector is \"ic-button[ic-button=next]\"", function() {
			expect(selector).toEqual("ic-button[ic-button=next]");
		});

		NextButtonTests($widget, widget);
	});
};
