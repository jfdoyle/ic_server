///<reference path="../core.ts" />
/*
 * Copyright &copy; 2013 Infuze Ltd, All Rights Reserved.
 */

namespace test {
	type T = ic.InputWidget;

	export function InputTests($widget: JQuery, widget: T[]) {
		WidgetTests($widget, widget);

		describe("InputWidget tests", function() {
			it("InputWidget test requires one or more widgets", function() {
				expect($widget.length).toBeGreaterThan(0);
			});

			it("has a numeric index", function() {
				expect(widget[0].index).toBeGreaterThan(-1);
			});
		});
	}
};
