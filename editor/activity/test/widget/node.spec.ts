///<reference path="../core.ts" />
/*
 * Copyright &copy; 2013 Infuze Ltd, All Rights Reserved.
 */

namespace test {
	type T = ic.WidgetNode;

	export function WidgetNodeTests($widget: JQuery, widget: T[]) {
		describe("WidgetNode tests", function() {
			if (widget[0] instanceof ic.ScreenWidget) {
				it("parent is a screens", function() {
					expect(widget[0].parentWidget).toBeDefined();
					expect(widget[0].parentWidget instanceof ic.ScreensWidget).toEqual(true);
				});
			} else if (widget[0] instanceof ic.ActivitiesWidget) {
				it("parent is a screen", function() {
					expect(widget[0].parentWidget).toBeDefined();
					expect(widget[0].parentWidget instanceof ic.ScreenWidget).toEqual(true);
				});
			} else if (widget[0] instanceof ic.ActivityWidget) {
				it("parent is an activities or screen", function() {
					expect(widget[0].parentWidget).toBeDefined();
					expect(widget[0].parentWidget instanceof ic.ActivitiesWidget || widget[0].parentWidget instanceof ic.ScreenWidget).toEqual(true);
				});
			} else if (!(widget[0] instanceof ic.ScreensWidget)) {
				it("parent is an activity", function() {
					expect(widget[0].parentWidget).toBeDefined();
					expect(widget[0].parentWidget instanceof ic.ActivityWidget).toEqual(true);
				});
			}

			if (!(widget[0] instanceof ic.ScreensWidget)) {
				it("root is a screen", function() {
					expect(widget[0].rootWidget).toBeDefined();
					expect(widget[0].rootWidget instanceof ic.ScreensWidget).toEqual(true);
				});
			}
		});
	}
};
