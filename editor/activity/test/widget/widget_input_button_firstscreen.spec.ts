///<reference path="../core.ts" />
/*
 * Copyright &copy; 2013 Infuze Ltd, All Rights Reserved.
 */

namespace test {
	type T = ic.FirstScreenButtonWidget;
	let selector = ic.FirstScreenButtonWidget.prototype.selector;

	let $test = $(`<ic-screen>
		<ic-activities>
			<ic-anchor></ic-anchor>

			<p>FirstScreenButtonWidget</p>

			<ic-activity>
				<ic-button ic-button="firstscreen">Button 0</ic-button>

				<ic-button ic-button="firstscreen">Button 1</ic-button>

				<ic-button>Button</ic-button>
			</ic-activity>
		</ic-activities>
	</ic-screen>`).appendTo("ic-screens");

	export function FirstScreenButtonTests($widget: JQuery, widget: T[]) {
//		ButtonTests($widget, widget);

		describe("FirstScreenButtonWidget tests", function() {
			it("moves us to the first screen", function() {
				screensWidget.screen = screensWidget.length - 1;
				expect(screensWidget.screen).toEqual(screensWidget.length - 1);
				$widget.eq(0).simulate("mousedown").simulate("mouseup");
				expect(screensWidget.screen).toEqual(0);
			});
		});
	}

	describeQueue("FirstScreenButtonWidget", function() {
		let widget: T[] = [];
		let $widget = $test.find(selector).each(function(i, element: HTMLElement) {
			widget[i] = element.icWidget as T;
		});

		it("selector is \"ic-button[ic-button=firstscreen]\"", function() {
			expect(selector).toEqual("ic-button[ic-button=firstscreen]");
		});

		FirstScreenButtonTests($widget, widget);
	});
};
