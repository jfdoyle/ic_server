///<reference path="../core.ts" />
/*
 * Copyright &copy; 2013 Infuze Ltd, All Rights Reserved.
 */

namespace test {
	type T = ic.FirstButtonWidget;
	let selector = ic.FirstButtonWidget.prototype.selector;

	let $test = $(`<ic-screen>
		<ic-activities>
			<ic-anchor></ic-anchor>

			<p>FirstButtonWidget</p>

			<ic-activity>
				<ic-button ic-button="first">Button 0</ic-button>

				<ic-button ic-button="first">Button 1</ic-button>

				<ic-button>Button</ic-button>
			</ic-activity>
		</ic-activities>
	</ic-screen>`).appendTo("ic-screens");

	export function FirstButtonTests($widget: JQuery, widget: T[]) {
//		ButtonTests($widget, widget);

		describe("FirstButtonWidget tests", function() {
			it("moves us to the first activity", function() {
				let anchors = widget[0].get(ic.AnchorWidget);

				anchors.last().addState("active");
				expect(anchors.first().hasState("active")).toEqual(false);
				expect(anchors.last().hasState("active")).toEqual(true);
				$widget.eq(0).simulate("mousedown").simulate("mouseup");
				expect(anchors.first().hasState("active")).toEqual(true);
				expect(anchors.last().hasState("active")).toEqual(false);
			});
		});
	}

	describeQueue("FirstButtonWidget", function() {
		let widget: T[] = [];
		let $widget = $test.find(selector).each(function(i, element: HTMLElement) {
			widget[i] = element.icWidget as T;
		});

		it("selector is \"ic-button[ic-button=first]\"", function() {
			expect(selector).toEqual("ic-button[ic-button=first]");
		});

		FirstButtonTests($widget, widget);
	});
};
