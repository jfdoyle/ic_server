///<reference path="../core.ts" />
/*
 * Copyright &copy; 2013 Infuze Ltd, All Rights Reserved.
 */

namespace test {
	type T = ic.LastScreenButtonWidget;
	let selector = ic.LastScreenButtonWidget.prototype.selector;

	let $test = $(`<ic-screen>
		<ic-activities>
			<ic-anchor></ic-anchor>

			<p>LastScreenButtonWidget</p>

			<ic-activity>
				<ic-button ic-button="lastscreen">Button 0</ic-button>

				<ic-button ic-button="lastscreen">Button 1</ic-button>

				<ic-button>Button</ic-button>
			</ic-activity>
		</ic-activities>
	</ic-screen>`).appendTo("ic-screens");

	export function LastScreenButtonTests($widget: JQuery, widget: T[]) {
//		ButtonTests($widget, widget);

		describe("LastScreenButtonWidget tests", function() {
			it("moves us to the last screen", function() {
				screensWidget.screen = 0;
				expect(screensWidget.screen).toEqual(0);
				$widget.eq(0).simulate("mousedown").simulate("mouseup");
				expect(screensWidget.screen).toEqual(screensWidget.length - 1);
			});
		});
	}

	describeQueue("LastScreenButtonWidget", function() {
		let widget: T[] = [];
		let $widget = $test.find(selector).each(function(i, element: HTMLElement) {
			widget[i] = element.icWidget as T;
		});

		it("selector is \"ic-button[ic-button=lastscreen]\"", function() {
			expect(selector).toEqual("ic-button[ic-button=lastscreen]");
		});

		LastScreenButtonTests($widget, widget);
	});
};
