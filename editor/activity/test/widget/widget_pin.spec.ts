///<reference path="../core.ts" />
/*
 * Copyright &copy; 2013 Infuze Ltd, All Rights Reserved.
 */

namespace test {
	type T = ic.PinWidget;
	let selector = ic.PinWidget.prototype.selector;

	let $test = $(`<ic-screen>
		<ic-activities>
			<ic-anchor></ic-anchor>

			<p>PinWidget</p>

			<ic-activity>
				<ic-pin>Pin 0</ic-pin>

				<ic-pin>Pin 1</ic-pin>
			</ic-activity>
		</ic-activities>
	</ic-screen>`).appendTo("ic-screens");
};
