///<reference path="../core.ts" />
/*
 * Copyright &copy; 2013 Infuze Ltd, All Rights Reserved.
 */

namespace test {
	type T = ic.MarkButtonWidget;
	let selector = ic.MarkButtonWidget.prototype.selector;

	let $test = $(`<ic-screen>
		<ic-activities>
			<ic-anchor></ic-anchor>

			<p>MarkButtonWidget</p>

			<ic-activity>
				<ic-button ic-button="mark">Mark Button 0</ic-button>

				<ic-button ic-button="mark">Mark Button 1</ic-button>
			</ic-activity>

			<ic-activity>
				<ic-button>Button</ic-button>
			</ic-activity>
		</ic-activities>
	</ic-screen>`).appendTo("ic-screens");

	export function MarkButtonTests($widget: JQuery, widget: T[]) {
//		ButtonTests($widget, widget);

		describe("MarkButtonWidget tests", function() {
			it("marks our activity", function() {
				widget[0].screenWidget.removeState("marked");
				expect(widget[0].screenWidget.hasState("marked")).toEqual(false);
				$widget.eq(0).simulate("mousedown").simulate("mouseup");
				expect(widget[0].screenWidget.hasState("marked")).toEqual(true);
			});

			xit("doesn't mark sibling activities", function() {
				widget[0].screenWidget.removeState("marked");
				//widget[0].screenWidget.nextWidgetSibling.removeState("marked");
				//expect(widget[0].screenWidget.nextWidgetSibling.hasState("marked")).toEqual(false);
				$widget.eq(0).simulate("mousedown").simulate("mouseup");
				//expect(widget[0].screenWidget.nextWidgetSibling.hasState("marked")).toEqual(false);
			});

			it("doesn't mark root", function() {
				widget[0].screenWidget.removeState("marked");
				widget[0].rootWidget.removeState("marked");
				expect(widget[0].rootWidget.hasState("marked")).toEqual(false);
				$widget.eq(0).simulate("mousedown").simulate("mouseup");
				expect(widget[0].rootWidget.hasState("marked")).toEqual(false);
			});
		});
	}

	describeQueue("MarkButtonWidget", function() {
		let widget: T[] = [];
		let $widget = $test.find(selector).each(function(i, element: HTMLElement) {
			widget[i] = element.icWidget as T;
		});

		it("selector is \"ic-button[ic-button=mark]\"", function() {
			expect(selector).toEqual("ic-button[ic-button=mark]");
		});

		MarkButtonTests($widget, widget);
	});
};
