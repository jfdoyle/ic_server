///<reference path="../core.ts" />
/*
 * Copyright &copy; 2013 Infuze Ltd, All Rights Reserved.
 */

namespace test {
	type T = ic.InputButtonWidget;
	let selector = ic.InputButtonWidget.prototype.selector;

	let $test = $(`<ic-screen>
		<ic-activities>
			<ic-anchor></ic-anchor>

			<p>InputButtonWidget</p>

			<ic-activity>
				<ic-button>Button 0</ic-button>

				<ic-button>Button 1</ic-button>
		</ic-activities>
	</ic-screen>`).appendTo("ic-screens");

	export function ButtonTests($widget: JQuery, widget: T[]) {
		InputTests($widget, widget);

		describe("InputButtonWidget tests", function() {
			it("InputButtonWidget test requires two or more widgets", function() {
				expect($widget.length).toBeGreaterThan(1);
			});

			it("mouse click sets click state", function() {
				$widget.eq(0).simulate("mousedown");
				expect(widget[0].hasState("click")).toEqual(true);
				$widget.eq(0).simulate("mouseup");
				expect(widget[0].hasState("click")).toEqual(false);
			});

			it("mouse click when disabled does nothing", function() {
				widget[0].addState("disabled");
				$widget.eq(0).simulate("mousedown");
				expect(widget[0].hasState("click")).toEqual(false);
				$widget.eq(0).simulate("mouseup");
				expect(widget[0].hasState("click")).toEqual(false);
			});

			it("mouse move after click changes click & hover state", function() {
				$widget.eq(0).simulate("mousedown").simulate("mouseenter");
				expect(widget[0].hasState("hover")).toEqual(true);
				expect(widget[0].hasState("click")).toEqual(true);
				$widget.eq(0).simulate("mouseleave");
				expect(widget[0].hasState("hover")).toEqual(false);
				expect(widget[0].hasState("click")).toEqual(false);
				$widget.eq(0).simulate("mouseenter");
				expect(widget[0].hasState("hover")).toEqual(true);
				expect(widget[0].hasState("click")).toEqual(true);
				$widget.eq(0).simulate("mouseup");
			});

			it("mouse move after click when disabled does nothing", function() {
				widget[0].addState("disabled");
				$widget.eq(0).simulate("mousedown").simulate("mouseenter");
				expect(widget[0].hasState("hover")).toEqual(false);
				expect(widget[0].hasState("click")).toEqual(false);
				$widget.eq(0).simulate("mouseleave");
				expect(widget[0].hasState("hover")).toEqual(false);
				expect(widget[0].hasState("click")).toEqual(false);
				$widget.eq(0).simulate("mouseenter");
				expect(widget[0].hasState("hover")).toEqual(false);
				expect(widget[0].hasState("click")).toEqual(false);
				$widget.eq(0).simulate("mouseup");
			});

			it("mouse move after click doesn't change sibling click & hover state", function() {
				$widget.eq(1).simulate("mousedown");
				$widget.eq(0).simulate("mouseenter");
				expect(widget[0].hasState("hover")).toEqual(false);
				expect(widget[0].hasState("click")).toEqual(false);
				$widget.eq(0).simulate("mouseleave");
				expect(widget[0].hasState("hover")).toEqual(false);
				expect(widget[0].hasState("click")).toEqual(false);
				$widget.eq(0).simulate("mouseenter");
				expect(widget[0].hasState("hover")).toEqual(false);
				expect(widget[0].hasState("click")).toEqual(false);
				$widget.eq(1).simulate("mouseup");
			});
		});
	}

	describeQueue("InputButtonWidget", function() {
		let widget: T[] = [];
		let $widget = $test.find(selector).each(function(i, element: HTMLElement) {
			widget[i] = element.icWidget as T;
		});

		it("selector is \"ic-button\"", function() {
			expect(selector).toEqual("ic-button");
		});

		ButtonTests($widget, widget);
	});
};
