///<reference path="../src/widget/_all.d.ts" />
///<reference path="jquery-simulate/jquery.simulate.js" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQueryStatic {
	simulate(el: HTMLElement, type: string, options?: any): void;
}

interface JQuery {
	simulate(type: string, options?: any): JQuery;
}

namespace test {
	export let screensWidget: ic.ScreensWidget;
	export let firstScreenIndex: number;
	export let lastScreenIndex: number;

	export let describeQueueData: { [description: string]: (() => void)[] };

	export function describeQueue(description: string, specDefinitions: () => void) {
		if (!describeQueueData) {
			describeQueueData = {};
		}
		if (!describeQueueData[description]) {
			describeQueueData[description] = [];
		}
		describeQueueData[description].push(specDefinitions);
	}

	export function startup(document: HTMLDocument) {
		ic.Widget.startup(document.body);

		screensWidget = $("ic-screens")[0].icWidget as ic.ScreensWidget;
		firstScreenIndex = screensWidget.get(ic.ActivityWidget).first().element.closest("ic-screen").icWidget.index - 1;
		lastScreenIndex = screensWidget.get(ic.ActivityWidget).last().element.closest("ic-screen").icWidget.index - 1;

		$("#content>p").click(function() {
			$(this).hide().nextAll().toggle();
		});

		Object.keys(describeQueueData).forEach(function(description) {
			describe(description, function() {
				describeQueueData[description].forEach(function(fn) {
					fn();
				});
			});
		});
	}
};
