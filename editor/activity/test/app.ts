///<reference types="jasmine" />
///<reference types="jquery" />
///<reference path="core.ts" />
///<reference path="widget/_all.d.ts" />
///<reference path="jquery-simulate/jquery.simulate.js" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface JQueryStatic {
	simulate(el: HTMLElement, type: string, options?: any): void;
}

interface JQuery {
	simulate(type: string, options?: any): JQuery;
}

namespace test {
	if (/complete|loaded|interactive/.test(document.readyState) && document.body) {
		startup(document)
	} else {
		document.addEventListener("DOMContentLoaded", startup.bind(test, document), false);
	}
};
