/* 
 * Copyright © 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

module.exports = function(grunt) {
	"use strict";

	require("load-grunt-tasks")(grunt);
};
