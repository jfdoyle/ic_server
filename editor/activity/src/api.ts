///<reference path="core.ts" />
///<reference path="startup.ts" />
///<reference path="api.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace IC {
	/**
	 * Reveal the correct answers
	 */
	export function reveal() {
		ic.Widget.trigger(".reveal");
		if (usePostMessage) {
			postMessage("reveal");
		}
	}

	/**
	 * Reset to the default state
	 */
	export function reset() {
		ic.Widget.trigger(".reset");
		if (usePostMessage) {
			postMessage("reset");
		}
	}

	/**
	 * Submit (check) the correct answers
	 */
	export function submit() {
		ic.Widget.trigger(".submit");
		if (usePostMessage) {
			let screen = ic.Widget.root.screenWidget,
				activities = screen.get(screen, ic.ActivitiesWidget),
				right = 0,
				total = 0;

			if (!activities.length) {
				activities = screen.get(screen, ic.ActivityWidget);
			}
			activities.forEach(function(activity) {
				let mark = activity.markable;

				if (mark.maxPoints !== -1 && mark.maxPoints !== undefined) {
					right += mark.score;
					total += mark.maxPoints;
				}
			});
			postMessage("submit", right, total);
		}
	}

	/**
	 * Go to a specific screen
	 */
	export function goto(screen: number) {
		if (isNumber(screen)) {
			ic.Widget.root.screen = screen;
		}
	}

	/**
	 * Get the elapsed time (in seconds) of this test
	 */
	export function timer(seconds?: number): number {
		if (ic.Widget.root) {
			let timer = ic.Widget.root.get(ic.TimerWidget).first();

			if (timer) {
				if (seconds) {
					timer.minutes = seconds * 60 * 1000;
				}
				return timer.trigger(".persist") / 1000;
			}
		}
		return 0;
	}

	/**
	 * Disable the timers
	 */
	export function pause(alsoButtons: boolean = true): number {
		if (ic.Widget.root) {
			ic.Widget.root.get<ic.Widget>(ic.TimerWidget, ic.InputWidget, ic.ButtonWidget, ic.NavWidget)
				.filter(function(widget: ic.Widget) {
					return alsoButtons
						|| widget instanceof ic.TimerWidget
						|| (widget instanceof ic.InputWidget && (widget.screenWidget || !widget.rootWidget));
				})
				.forEach(function(widget: ic.Widget) {
					if (!widget.hasState("disabled")) {
						widget.api_pause = true;
						widget.addState("disabled");
					}
				});
		}
		return 0;
	}

	/**
	 * Enable the timers
	 */
	export function resume(onlyButtons: boolean = false): number {
		if (ic.Widget.root) {
			ic.Widget.root.get<ic.Widget>(ic.TimerWidget, ic.InputWidget, ic.ButtonWidget, ic.NavWidget)
				.filter(function(widget: ic.Widget) {
					return !onlyButtons
						|| widget instanceof ic.ButtonWidget
						|| widget instanceof ic.NavWidget
						|| (widget instanceof ic.InputWidget && !(widget.screenWidget || !widget.rootWidget));
				})
				.forEach(function(widget: ic.Widget) {
					if (widget.api_pause) {
						delete widget.api_pause;
						widget.removeState("disabled");
					}
				});
		}
		return 0;
	}

	/**
	 * Get or set the state data for all screens and activities.
	 * This is a black-box data string that should not be manually edited,
	 * and is ideal for compression etc.
	 */
	export function state(data?: string): string {
		if (ic.Widget.root) {
			let json: string;

			if (isString(data)) {
				json = JSON.parse(data.decompress("uri"));
			}
			json = JSON.stringify(ic.Widget.root.trigger(".persist", json));
			if (!data) {
				data = json.compress("uri");
				//if (json && data) {
				//	console.log("Compress: src=" + json.length + " bytes, dest=" + data.length + " bytes (" + (data.length * 100 / json.length).toFixed(2) + "%)");
				//}
			}
			return data;
		}
		return "";
	};

	/**
	 * Get the current score for one or more activities.
	 * 
	 * @param screenIndex The index of the screen to list (starting at one).
	 * 
	 * @param activityIndex The index of the activity on the screen (starting
	 * at one).
	 * 
	 * @returns An array of <code>APIScore</code> objects listing every activity
	 * that has been requested.
	 */
	export function score(screenIndex?: number, activityIndex?: number): APIScore[] {
		let output: APIScore[] = [],
			isScreen = isNumber(screenIndex),
			isActivity = isScreen && isNumber(activityIndex);

		if (ic.Widget.root) {
			ic.Widget.root.get(ic.ScreenWidget).forEach(function(screen) {
				if (!isScreen || screen.realIndex === screenIndex) {
					let activities = screen.get(screen, ic.ActivityWidget).filter(function(activity) {
						return activity.markable && activity.markable.score >= 0;
					});

					activities.forEach(function(activity) {
						if (!isActivity || activity.realIndex === activityIndex) {
							let mark = activity.markable,
								answers: string[] = [],
								answered = activity.get(activity, ic.InputWidget).forEach(function(widget) {
									let mark = widget.markable;

									if (mark && mark.value) {
										if (isArray(mark.value)) {
											answers.push.apply(answers, mark.value);
										} else {
											answers.push(String(mark.value));
										}
									}
								}),
								screenWidget = activity.screenWidget,
								score: APIScore = {
									"id": (mark.id || "#@")
										.replace(/\#/g, String(screen.index))
										.replace(/\@/g, activities.length <= 1 ? "" : String.fromCharCode(96 + activity.index))
										.replace(/\%/g, activities.length <= 1 ? "" : activity.index.toRoman(true)),
									"screen": screen.realIndex,
									"activity": activity.realIndex,
									"answers": answers.length > 1 ? JSON.parse(JSON.stringify(answers)) : answers.length ? answers[0] : "",
									"score": mark.score,
									"points": mark.maxPoints,
									"min": mark.min || 0,
									"scaled": mark.scaled,
									"max": mark.max,
									"viewed": screenWidget && screenWidget.hasState("visited"),
									"attempted": activity.hasState("attempted"),
									"completed": (screenWidget || activity.rootWidget).hasState("marked")
								};

							activity.get(activity, ic.QuestionWidget).forEach(function(widget) {
								score["question"] = (score["question"] ? score["question"] + " " : "") + widget.element.textContent.trim();
							});
							output.push(score);
						}
					});
				}
			});
		}
		return output;
	}

	let usePostMessage: boolean,
		postMessageID = "[infuzeCreator]",
		joinChar = "~";

	function postMessage(...args: any[]) {
		window.parent.postMessage(postMessageID + joinChar + location.href + joinChar + args.join(joinChar), "*");
	}

	function attempt() {
		document.removeEventListener("mousedown", attempt, true);
		document.removeEventListener("touchstart", attempt, true);
		postMessage("attempt");
	}

	addEventListener("message", function(event: MessageEvent) {
		//console.log("postMessage", event)
		for (let win = window; win; win = win.parent) {
			if (event.source === win) {
				let message = (event.data as string).split(joinChar);

				//console.log("correct Window", message)
				if (message[0] === postMessageID && message[1] === location.href) {
					if (message[2] === "listen") {
						//console.log("listen")
						usePostMessage = true;
						document.addEventListener("mousedown", attempt, true);
						document.addEventListener("touchstart", attempt, true);
					} else if (usePostMessage) {
						let classList = document.body.classList;

						switch (message[2]) {
							case "ping":
								console.log("...pong");
								break;
							case "submit":
								submit();
								classList.add("mark");
								document.removeEventListener("mousedown", attempt, true);
								document.removeEventListener("touchstart", attempt, true);
								break;
							case "reset":
								reset();
								classList.remove("reveal", "mark");
								document.addEventListener("mousedown", attempt, true);
								document.addEventListener("touchstart", attempt, true);
								break;
							case "reveal":
								reveal();
								classList.remove("mark");
								classList.add("reveal");
								document.removeEventListener("mousedown", attempt, true);
								document.removeEventListener("touchstart", attempt, true);
								break;
						}
					}
				}
				return;
			}
		}
	}, false);

	if (IC !== window.IC) {
		let oldIC: IC = window.IC;

		window.IC = IC;
		if (oldIC) {
			for (let key in oldIC) {
				(window.IC as {[key: string]: any})[key] = (oldIC as {[key: string]: any})[key];
			}
		}
	}
};
