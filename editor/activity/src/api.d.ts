/*
 * Copyright (C) 2013-2015 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface APIScore {
	"id": string;
	"screen": number;
	"activity": number;
	"answers": string | string[];
	"score": number;
	"points": number;
	"min": number;
	"scaled": number;
	"max": number;
	"viewed": boolean;
	"attempted": boolean;
	"completed": boolean;
	"question"?: string;
}

interface IC {
	reveal(): void;
	reset(): void;
	submit(): void;
	goto(screen: number): void;
	timer(seconds?: number): number;
	pause(alsoButtons?: boolean): number;
	resume(onlyButtons?: boolean): number;
	state(data?: string): string;
	score(screenIndex?: number, activityIndex?: number): APIScore[];
}

interface Window {
	IC: IC;
}
