///<reference path="core.ts" />
///<reference path="api.ts" />
///<reference path="storyboard.ts" />
///<reference path="startup.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	if (/complete|loaded|interactive/.test(document.readyState) && document.body) {
		startup(document)
	} else {
		document.addEventListener("DOMContentLoaded", startup.bind(ic, document), false);
	}
};
