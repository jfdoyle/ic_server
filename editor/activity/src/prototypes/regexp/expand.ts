///<reference path="../defineProperty.ts" />
///<reference path="expand.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(RegExp.prototype, "expand", function(this: RegExp): string[] {
	function expandRegexp(regExp: string): string[] {
		if (regExp[0] === "^") {
			regExp = regExp.substr(1);
		} else {
			regExp = ".*" + regExp;
		}
		if (regExp[regExp.length - 1] === "^") {
			regExp = regExp.substr(0, regExp.length - 2);
		} else {
			regExp += ".*";
		}

		//	remove starting and trailing CRLF (maybe not useful?)
		let myRegExp = regExp.replace(/^[\n\r\^]+|[\n\r\$]+$/g, ""),
			// process parentheses without any pipe inside them (aaa)
			// transform (aaa)? into (aaa|)
			matches: RegExpMatchArray;

		// Replace (xyz)? with (xyz|)
		myRegExp = myRegExp.replace(/\(([^|]*?\))(\?)/g, function($0, $1, $2) {
			return $2 ? "(" + $1 + "|)?" : $1;
		});
		//console.log("expand2", myRegExp)

		// Replace "x?" with "(x|)"
		myRegExp = myRegExp.replace(/(\\?[^\\\)\*])\?/g, function($0, $1) {
			return "(" + $1 + "|)";
		});
		//console.log("expand3", myRegExp)

		// Expand [xyz] into [x|y|z]
		while ((matches = myRegExp.match(/\[.*?\]/g))) {
			let squareBrackets = matches,
				sb1 = squareBrackets[0],
				sb = sb1.substring(1, sb1.length - 1),
				regHyphen = /.?-.?/;

			while (myRegExp.match(regHyphen) != null) {
				let hyphen = myRegExp.match(regHyphen),
					stringLeft = myRegExp.split(hyphen[0])[0];

				if (stringLeft.charAt(stringLeft.length - 1) == "|") {
					stringLeft = stringLeft.substring(0, stringLeft.length - 1);
				}
				let stringRight = myRegExp.split(hyphen[0])[1];
				let hy1 = hyphen[0].charAt(0),
					hy2 = hyphen[0].charAt(2),
					hy = "",
					charList = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ%8A%8C%C0%C1%C2%C3%C4%C5%C6%C7%C8%C9%CA%CB%CC%CD%CE%CF%D0%D1%D2%D3%D4%D5%D6%D8%D9%DA%DB%DC%DD%DE%9Fabcdefghijklmnopqrstuvwxyz%9A%9C%E0%E1%E2%E3%E4%E5%E6%E7%E8%E9%EA%EB%EC%ED%EE%EF%F0%F1%F2%F3%F4%F5%F6%F8%F9%FA%FB%FC%FD%FE%FFéèÿ";

				for (let c = charList.indexOf(hy1); c < charList.indexOf(hy2) + 1; c++) {
					hy += charList.charAt(c);
				}
				myRegExp = stringLeft + hy + stringRight
			}
			let sb2 = "(";
			for (let i = 0; i < sb.length; i++) {
				sb2 += sb.charAt(i) + "|";
			}
			sb2 = sb2.substring(0, sb2.length - 1) + ")";
			myRegExp = replaceWord(myRegExp, sb1, sb2);
		}
		//console.log("expand3", myRegExp)

		// Replace "\x" with "%FF" encoded, first replace "%" to prevent issues
		myRegExp = myRegExp.replace(/\\?%/g, "%25").replace(/\\(.)/g, function($0, $1: string) {
			return "%" + $1.charCodeAt(0).toString(16);
		});
		//console.log("expand4", myRegExp)

		// Don"t process metacharacters not accepted in sentence generation
		if (/[^\\][\+\*\.\{\}]/.test(" " + myRegExp)) {
			let altExp = regExp.replace(/\.[\*\?]/g, "");

			return altExp === regExp ? [regExp] : [regExp, altExp];
		}
		myRegExp = replaceNestedOrs(myRegExp) || myRegExp;
		//console.log("expand5", myRegExp)
		let result = expandOrs(myRegExp); // expand parenthesis contents
		//console.log("expand6", result)
		for (let i = 0; i < result.length; i++) {
			result[i] = decodeURIComponent(result[i]);
		}
		return result;
	}

	// this function finds individual nestedOrs expressions in myRegExp
	function getNextNestedOr(str: string): string {
		let start = 0,
			isNested = false,
			depth = 0;

		for (let i = 0; i < str.length; i++) {
			switch (str[i]) {
				case "(":
					depth++;
					if (depth === 1) {
						start = i
					} else if (depth === 2) {
						isNested = true;
					}
					break;
				case ")":
					depth--;
					if (!depth && isNested) {
						return str.substring(start, i + 1);
					}
					break;
			}
		}
		return;
	}

	function getGroup(str: string): string {
		let matches = str.match(/[^(|)]*\(([^(|)]*\|[^(|)]*)+\)[^(|)]*/g);

		if (matches) {
			for (let i = 0; i < matches.length; i++) {
				let match = matches[i];

				if (match[0] !== "(" || match[match.length - 1] !== ")") {
					return match;
				}
			}
		}
	}

	function replaceNestedOrs(myRegExp: string): string {
		let regSingleOrs = /\([^()]*\)/,
			regSingleOrsTotal = /^\([^()]*\)$/;

		// find next nested parentheses in myRegExp
		while (getNextNestedOr(myRegExp)) {
			let nestedOrs = getNextNestedOr(myRegExp),
				nestedOrsOriginal = nestedOrs,
				matches: RegExpMatchArray;

			while (getGroup(nestedOrs)) {
				let myParent = getGroup(nestedOrs),
					leftChar = nestedOrs[nestedOrs.indexOf(myParent) - 1],
					rightChar = nestedOrs[nestedOrs.indexOf(myParent) + myParent.length],
					outerChars = leftChar + rightChar,
					leftPar = "", rightPar = "";

				if (outerChars == ")(") {break}
				switch (outerChars) {
					case "||": case "()":
						break;
					case "((": case "))": case "(|": case "|(": case ")|": case "|)":
						leftPar = "("; rightPar = ")";
						break;
					default:
						alert("ERROR: switch did not work ???\nouterChars = " + outerChars);
						break;
				}
				let myResult = leftPar + expandOrs(myParent).join("|") + rightPar;

				nestedOrs = replaceWord(nestedOrs, myParent, myResult);
			}
			//	detect sequence of ((*|*)|(*|*)) within parentheses or |) or (| and remove all INSIDE parentheses
			//	(\(|\|)\([^(|)]*\|[^(|)]*\)(\|\([^(|)]*\|[^(|)]*\))*(\)|\|)
			while ((matches = nestedOrs.match(/(\(|\|)\([^(|)]*\|[^(|)]*\)(\|\([^(|)]*\|[^(|)]*\))*(\)|\|)/))) {
				let plainOrs = matches[0];

				nestedOrs = replaceWord(nestedOrs, plainOrs, "(" + plainOrs.replace(/\(|\)/g, "") + ")");
				if (getGroup(nestedOrs)) {
					myRegExp = replaceWord(myRegExp, nestedOrsOriginal, nestedOrs);
					continue;
				}
			}
			//		any sequence of (|)(|) in nestedOrs? process them all
			while ((matches = nestedOrs.match(/(\([^(]*?\|*?\)){2,99}/))) {
				nestedOrs = nestedOrs.replace(matches[0], expandOrs(matches[0]).join("|"));
			}
			// test if we have reached the singleOrs stage
			if (getGroup(nestedOrs)) {
				myRegExp = replaceWord(myRegExp, nestedOrsOriginal, nestedOrs);
			} else {
				//	no parents left in nestedOrs, ...
				// find all single (*|*|*|*) and remove parentheses
				while (nestedOrs.match(regSingleOrs) != null) {
					if (nestedOrs.match(regSingleOrsTotal)[0] == nestedOrs) {
						break; // we have reached top of nestedOrs: keep ( )!
					}
					let singleParens = nestedOrs.match(regSingleOrs)[0],
						myResult = singleParens.substring(1, singleParens.length - 1);

					nestedOrs = replaceWord(nestedOrs, singleParens, myResult);
					if (getGroup(nestedOrs)) {
						myRegExp = replaceWord(myRegExp, nestedOrsOriginal, nestedOrs);
					}
				}
				myRegExp = replaceWord(myRegExp, nestedOrsOriginal, nestedOrs);
			}
		}
		return myRegExp;
	}

	function expandOrs(str: string): string[] {
		if (!str || !/\(.*\|.*\)/.test(str)) {
			return [str];
		}
		let parts: [number, (string | string[])][] = [],
			expanded: string[] = [""],
			lastIndex = 0;

		str.split(/(\(.*?\))/).forEach(part => {
			let data = part[0] === "(" && part[part.length - 1] === ")"
				? part.substr(1, part.length - 2).split("|")
				: part;

			parts.push([
				lastIndex = str.indexOf(part, lastIndex),
				data
			]);
		});
		parts.forEach(part => {
			for (let i = 0, length = expanded.length; i < length; i++) {
				if (isString(part[1])) {
					expanded[i] += part[1];
				} else {
					for (let j = 0; j < part[1].length; j++) {
						expanded.push(expanded[i] + part[1][j]);
					}
				}
			}
		});
		return expanded;
	}

	function replaceWord(str: string, substr: string, newSubstr: string) {
		return str.replace(new RegExp(substr.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1"), "g"), newSubstr);
	}

	return expandRegexp(this.source).sort();
});
