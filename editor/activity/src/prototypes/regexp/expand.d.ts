/*
 * Copyright &copy; 2017 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface RegExp {
	/**
	 * The <strong><code>RegExp.expand()</code></strong> method expands a
	 * regular expression into an array of matching strings.
	 * 
	 * Some tokens cannot be expanded yet, these include curly brackets and
	 * other multiple character symbols.
	 * 
	 * @returns An array of all strings that can be created from this regular
	 * expression.
	 */
	expand(): string[];
}
