/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Math {
	/**
	 * The <strong><code>Math.range()</code></strong> function returns a number
	 * clamped to a range.
	 * 
	 * @param min The lowest permitted number.
	 * 
	 * @param num The number to be clamped.
	 * 
	 * @param max The highest permitted number.
	 * 
	 * @returns A number between <code>min</code> and <code>max</code>
	 * inclusive.
	 */
	range(min: number, num: number, max: number): number;
}
