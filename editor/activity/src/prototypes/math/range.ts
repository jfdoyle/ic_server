///<reference path="../defineProperty.ts" />
///<reference path="range.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Math, "range", function(min: number, num: number, max: number) {
	return Math.max(min, Math.min(num, max));
});
