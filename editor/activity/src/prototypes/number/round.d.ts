/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Number {
	/**
	 * The <strong><code>round()</code></strong> method rounds a number to a set
	 * number of decimal places.
	 * 
	 * @param digits The number of decimal places to use.
	 * 
	 * @returns A Number rounded to a set number of places.
	 */
	round(digits: number): number;
}
