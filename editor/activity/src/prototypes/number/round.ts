///<reference path="../defineProperty.ts" />
///<reference path="round.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Number.prototype, "round", function(this: number, digits: number) {
	let num = +("1e" + Math.max(0, digits));

	return Math.round(this * num) / num;
});
