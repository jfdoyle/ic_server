///<reference path="../defineProperty.ts" />
///<reference path="range.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Number.prototype, "range", function(this: number, min: number, max: number) {
	return this >= Math.min(min, max) && this <= Math.max(min, max);
});
