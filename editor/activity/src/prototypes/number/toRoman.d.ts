/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Number {
	/**
	 * The <strong><code>toRoman()</code></strong> method formats a number using
	 * Roman numerals, optionally converting to lower case.
	 * 
	 * @param lower Return the string in lower case.
	 * 
	 * @returns A string representing the given number using Roman numerals.
	 */
	toRoman(lower?: boolean): string;
}
