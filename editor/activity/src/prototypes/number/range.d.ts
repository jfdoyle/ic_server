/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Number {
	/**
	 * The <strong><code>range()</code></strong> method tests if a number is
	 * within a range.
	 * 
	 * @param min The minimum permitted value.
	 * 
	 * @param max The maximum permitted value.
	 * 
	 * @returns A Boolean indicating whether or not the number is within the
	 * range.
	 */
	range(min: number, max: number): boolean;
}
