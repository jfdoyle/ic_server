///<reference path="../defineProperty.ts" />
///<reference path="toRoman.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Number.prototype, "toRoman", function(this: number, lower?: boolean) {
	let i: string,
		num = Math.floor(this),
		lookup: { [letter: string]: number } = {
			"M": 1000,
			"CM": 900,
			"D": 500,
			"CD": 400,
			"C": 100,
			"XC": 90,
			"L": 50,
			"XL": 40,
			"X": 10,
			"IX": 9,
			"V": 5,
			"IV": 4,
			"I": 1
		},
		roman = "";

	for (i in lookup) {
		while (num >= lookup[i]) {
			roman += i;
			num -= lookup[i];
		}
	}
	return lower ? roman.toLowerCase() : roman;
});
