///<reference path="../defineProperty.ts" />
///<reference path="endsWith.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(String.prototype, "endsWith", function(this: string, searchString: string, position?: number) {
	let subjectString = this.toString();

	if (typeof position !== "number" || !isFinite(position) || Math.floor(position) !== position || position > subjectString.length) {
		position = subjectString.length;
	}
	position -= searchString.length;
	let lastIndex = subjectString.indexOf(searchString, position);

	return lastIndex !== -1 && lastIndex === position;
});
