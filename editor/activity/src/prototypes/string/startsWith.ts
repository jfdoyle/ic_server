///<reference path="../defineProperty.ts" />
///<reference path="startsWith.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(String.prototype, "startsWith", function(this: string, searchString: string, position?: number) {
	return this.substr(position || 0, searchString.length) === searchString;
});
