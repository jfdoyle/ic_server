/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface String {
	/**
	 * The <strong><code>endsWith()</code></strong> method determines whether a
	 * string ends with the characters of another string, returning
	 * <code>true</code> or <code>false</code> as appropriate.
	 * 
	 * @param searchString The characters to be searched for at the end of this
	 * string.
	 * 
	 * @param position Optional. Search within this string as if this string
	 * were only this long; defaults to this string's actual length, clamped
	 * within the range established by this string's length.
	 * 
	 * @returns <code>true</code> if the string ends with the characters of the
	 * search string; otherwise, <code>false</code>.
	 */
	endsWith(searchString: string, position?: number): boolean;
}
