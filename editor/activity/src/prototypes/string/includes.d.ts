/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface String {
	/**
	 * The <strong><code>includes()</code></strong> method determines whether
	 * one string may be found within another string, returning
	 * <code>true</code> or <code>false</code> as appropriate.
	 * 
	 * The <code>includes()</code> method is case sensitive.
	 * 
	 * @param searchString A string to be searched for within this string.
	 * 
	 * @param position The position in this string at which to begin searching
	 * for <code>searchString</code>; defaults to 0.
	 * 
	 * @returns <strong><code>true</code></strong> if the string contains the
	 * search string; otherwise, <strong><code>false</code></strong>.
	 */
	includes(searchString: string, position?: number): boolean;
}
