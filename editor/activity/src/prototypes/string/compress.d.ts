/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Using code from https://github.com/pieroxy/lz-string/
 */

interface String {
	/**
	 * Compress a string using LZW. Optionally encode the output for storage and use.
	 */
	compress(encode?: string): string | Uint8Array;
	compress(): string;
	compress(encode: "base64"): string;
	compress(encode: "uri"): string;
	compress(encode: "uint"): Uint8Array;
	compress(encode: "utf-16"): string;
}
