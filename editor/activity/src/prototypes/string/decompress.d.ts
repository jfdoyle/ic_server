/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Using code from https://github.com/pieroxy/lz-string/
 */

interface String {
	/**
	 * Decompress a string using LZW. If encoded then must decode the same way (Uint8Array has it's own handler).
	 */
	decompress(encode?: string): string;
	decompress(): string;
	decompress(encode: "base64"): string;
	decompress(encode: "uri"): string;
	decompress(encode: "utf-16"): string;
}

interface Uint8Array {
	/**
	 * Decompress a Uint8Array using LZW.
	 */
	decompress(encode?: string): string;
	decompress(encode: "uint"): string;
}
