/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface String {
	/**
	 * The <strong><code>ucfirst()</code><strong> method returns the calling
	 * string value with the first character converted to upper case, according
	 * to any locale-specific case mappings.
	 * 
	 * @returns A new string representing the calling string with the first
	 * character converted to upper case, according to any locale-specific case
	 * mappings.
	 */
	ucfirst(): string;
}
