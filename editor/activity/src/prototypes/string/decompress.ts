///<reference path="../defineProperty.ts" />
///<reference path="decompress.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Using code from https://github.com/pieroxy/lz-string/
 */

(function() {

	let fromCharCode = String.fromCharCode,
		keyStrBase64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
		keyStrUriSafe = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+-$",
		baseReverseDic = {};

	function getBaseValue(alphabet: string, character: string) {
		if (!baseReverseDic[alphabet]) {
			baseReverseDic[alphabet] = {};
			for (let i = 0; i < alphabet.length; i++) {
				baseReverseDic[alphabet][alphabet.charAt(i)] = i;
			}
		}
		return baseReverseDic[alphabet][character];
	}

	function _decompress(length: number, resetValue: number, getNextValue: Function) {
		let dictionary = [],
			next,
			enlargeIn = 4,
			dictSize = 4,
			numBits = 3,
			entry = "",
			result: string[] = [],
			i: number,
			w: string,
			bits, resb, maxpower, power,
			c,
			data = {val: getNextValue(0), position: resetValue, index: 1};

		for (i = 0; i < 3; i += 1) {
			dictionary[i] = i;
		}

		bits = 0;
		maxpower = Math.pow(2, 2);
		power = 1;
		while (power != maxpower) {
			resb = data.val & data.position;
			data.position >>= 1;
			if (data.position == 0) {
				data.position = resetValue;
				data.val = getNextValue(data.index++);
			}
			bits |= (resb > 0 ? 1 : 0) * power;
			power <<= 1;
		}

		switch (next = bits) {
			case 0:
				bits = 0;
				maxpower = Math.pow(2, 8);
				power = 1;
				while (power != maxpower) {
					resb = data.val & data.position;
					data.position >>= 1;
					if (data.position == 0) {
						data.position = resetValue;
						data.val = getNextValue(data.index++);
					}
					bits |= (resb > 0 ? 1 : 0) * power;
					power <<= 1;
				}
				c = fromCharCode(bits);
				break;
			case 1:
				bits = 0;
				maxpower = Math.pow(2, 16);
				power = 1;
				while (power != maxpower) {
					resb = data.val & data.position;
					data.position >>= 1;
					if (data.position == 0) {
						data.position = resetValue;
						data.val = getNextValue(data.index++);
					}
					bits |= (resb > 0 ? 1 : 0) * power;
					power <<= 1;
				}
				c = fromCharCode(bits);
				break;
			case 2:
				return "";
		}
		dictionary[3] = c;
		w = c;
		result.push(c);
		while (true) {
			if (data.index > length) {
				return "";
			}

			bits = 0;
			maxpower = Math.pow(2, numBits);
			power = 1;
			while (power != maxpower) {
				resb = data.val & data.position;
				data.position >>= 1;
				if (data.position == 0) {
					data.position = resetValue;
					data.val = getNextValue(data.index++);
				}
				bits |= (resb > 0 ? 1 : 0) * power;
				power <<= 1;
			}

			switch (c = bits) {
				case 0:
					bits = 0;
					maxpower = Math.pow(2, 8);
					power = 1;
					while (power != maxpower) {
						resb = data.val & data.position;
						data.position >>= 1;
						if (data.position == 0) {
							data.position = resetValue;
							data.val = getNextValue(data.index++);
						}
						bits |= (resb > 0 ? 1 : 0) * power;
						power <<= 1;
					}

					dictionary[dictSize++] = fromCharCode(bits);
					c = dictSize - 1;
					enlargeIn--;
					break;
				case 1:
					bits = 0;
					maxpower = Math.pow(2, 16);
					power = 1;
					while (power != maxpower) {
						resb = data.val & data.position;
						data.position >>= 1;
						if (data.position == 0) {
							data.position = resetValue;
							data.val = getNextValue(data.index++);
						}
						bits |= (resb > 0 ? 1 : 0) * power;
						power <<= 1;
					}
					dictionary[dictSize++] = fromCharCode(bits);
					c = dictSize - 1;
					enlargeIn--;
					break;
				case 2:
					return result.join('');
			}

			if (enlargeIn == 0) {
				enlargeIn = Math.pow(2, numBits);
				numBits++;
			}

			if (dictionary[c]) {
				entry = dictionary[c] as string;
			} else {
				if (c === dictSize) {
					entry = w + w.charAt(0);
				} else {
					return null;
				}
			}
			result.push(entry);

			// Add w+entry[0] to the dictionary.
			dictionary[dictSize++] = w + entry.charAt(0);
			enlargeIn--;

			w = entry;

			if (enlargeIn == 0) {
				enlargeIn = Math.pow(2, numBits);
				numBits++;
			}

		}
	}

	defineProperty(String.prototype, "decompress", function(this: string, encode?: string): string | Uint8Array {
		let compressed = this;

		if (typeof encode === "string") {
			switch (encode.toLowerCase()) {
				case "base64":
					return _decompress(compressed.length, 32, function(index: number) {return getBaseValue(keyStrBase64, compressed.charAt(index));})

				case "uri":
					compressed = compressed.replace(/ /g, "+");
					return _decompress(compressed.length, 32, function(index: number) {return getBaseValue(keyStrUriSafe, compressed.charAt(index));});

				case "utf-16":
					return _decompress(compressed.length, 16384, function(index: number) {return compressed.charCodeAt(index) - 32;});

				case "uint":
					break;

				default:
					console.error("Error: Encode type must be one of: base64, uri, utf-16, uint");
			}
		}
		return _decompress(compressed.length, 32768, function(index: number) {return compressed.charCodeAt(index);})
	});

	defineProperty(Uint8Array.prototype, "decompress", function(this: Uint8Array, encode?: string): string | Uint8Array {
		let i = 0, length = this.length / 2, result: string[] = [];

		for (; i < length; i++) {
			result.push(fromCharCode(this[i * 2] * 256 + this[i * 2 + 1]));
		}
		return result.join("").decompress();
	});
})();
