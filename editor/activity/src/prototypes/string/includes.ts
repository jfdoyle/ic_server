///<reference path="../defineProperty.ts" />
///<reference path="includes.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(String.prototype, "includes", function(this: string, searchString: string, position?: number) {
	position = position || 0;
	if (position + searchString.length > this.length) {
		return false;
	}
	return this.indexOf(searchString, position) !== -1;
});
