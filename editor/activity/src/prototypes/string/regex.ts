///<reference path="../defineProperty.ts" />
///<reference path="regex.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(String.prototype, "regex", function(this: string, regexp: RegExp): string | number | (string | number)[] {
	let rx: RegExp,
		num: number,
		length: number,
		matches: (string | number)[] = this.match(regexp);

	if (matches) {
		if (regexp.global) {
			if (/(^|[^\\]|[^\\](\\\\)*)\([^?]/.test(regexp.source)) { // Try to match "(blah" but not "\(blah" or "(?:blah" - ignore invalid regexp
				rx = new RegExp(regexp.source, (regexp.ignoreCase ? "i" : "") + (regexp.multiline ? "m" : ""));
			}
		} else {
			matches.shift();
		}
		length = matches.length;
		while (length--) {
			if (matches[length]) {
				if (rx) {
					matches[length] = (matches[length] as string).regex(rx) as string | number;
				} else {
					num = parseFloat(matches[length] as string);
					if (!isNaN(num) && String(num) === matches[length]) {
						matches[length] = num;
					}
				}
			}
		}
		if (!rx && matches.length === 1) {
			return matches[0];
		}
	}
	return matches;
});
