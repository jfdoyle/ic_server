///<reference path="../defineProperty.ts" />
///<reference path="explode.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(String.prototype, "explode", function(this: string) {
	let arr: (string | number)[] = (this as string).split("."),
		length = arr.length;

	for (let i = 0; i < length; i++) {
		if (i < length - 1 && (arr[i] as string).substr(-1) === "\\") {
			arr[i] = (arr[i] as string).substr(0, (arr[i] as string).length - 1) + "." + arr.splice(i + 1, 1);
			i--;
		} else {
			let num = parseInt(arr[i] as string, 10);
			if (String(num) === arr[i]) {
				arr[i] = num;
			}
		}
	}
	return arr;
});
