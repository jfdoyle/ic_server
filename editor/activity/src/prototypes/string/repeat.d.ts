/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface String {
	/**
	 * The <strong><code>repeat()</code></strong> method constructs and returns
	 * a new string which contains the specified number of copies of the string
	 * on which it was called, concatenated together.
	 * 
	 * @param count An integer between 0 and +∞: [0, +∞), indicating the number
	 * of times to repeat the string in the newly-created string that is to be
	 * returned.
	 * 
	 * @returns A new string containing the specified number of copies of the
	 * given string.
	 */
	repeat(count: number): string;
}
