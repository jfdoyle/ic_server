/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface String {
	/**
	 * The <strong><code>startsWith()</code></strong> method determines whether
	 * a string begins with the characters of another string, returning
	 * <code>true</code> or <code>false</code> as appropriate.
	 * 
	 * @param searchString The characters to be searched for at the start of
	 * this string.
	 * 
	 * @param position The position in this string at which to begin searching
	 * for searchString; defaults to 0.
	 * 
	 * @returns <code>true</code> if the string begins with the characters of
	 * the search string; otherwise, <code>false</code>.
	 */
	startsWith(searchString: string, position?: number): boolean;
}
