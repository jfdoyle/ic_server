/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface String {
	/**
	 * The <strong><code>regex()</code></strong> method retrieves the matches
	 * when matching a <em>string</em> against a <em>regular expression</em>.
	 * Unlike <code>match()</code> this method will automatically convert any
	 * numbers to the correct type, and only return the actual matches. If there
	 * is only a single match then it will return that value instead of an
	 * array.
	 * 
	 * @param regexp A regular expression object. If a non-RegExp object obj is
	 * passed, it is implicitly converted to a RegExp by using
	 * <code>new RegExp(obj)</code>.
	 * 
	 * @returns An Array containing the entire match result and any
	 * parentheses-captured matched results; if there is only a single match
	 * then the match itself; null if there were no matches.
	 */
	regex(regexp: RegExp): string | number | (string | number)[] | (string | number)[][];
}
