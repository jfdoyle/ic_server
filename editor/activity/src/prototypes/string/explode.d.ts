/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface String {
	/**
	 * The <strong><code>explode()</code></strong> method splits a dot-separated
	 * string into an array of tokens. Prefix a dot with a slash if it is part
	 * of a token. Numbers are automatically parsed.
	 * 
	 * @returns An array of tokens with both <em>string</em> and <em>number</em>
	 * values.
	 */
	explode(): (string | number)[];
}
