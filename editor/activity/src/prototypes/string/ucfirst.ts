///<reference path="../defineProperty.ts" />
///<reference path="ucfirst.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(String.prototype, "ucfirst", function(this: string) {
	return this.charAt(0).toLocaleUpperCase() + this.slice(1).toLowerCase();
});
