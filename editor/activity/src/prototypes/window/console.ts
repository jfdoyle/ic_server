///<reference path="../defineProperty.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

/**
 * Fix for not having a fully working console available - mainly IE
 */
(function() {
	try {
		let console = window.console || ({} as any),
			bind = Function.prototype.bind;

		["log", "warn", "error", "debug", "info", "assert", "trace"].forEach(function(method) {
			console[method] = console[method] || function() { };
			if (bind && isObject(console[method])) { // IE fix
				console[method] = bind.call(console[method], console);
			}
		});
	} catch (e) {
	}
} ());
