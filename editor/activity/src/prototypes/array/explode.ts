///<reference path="../defineProperty.ts" />
///<reference path="explode.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Array.prototype, "explode", function(this: any[]) {
	let i: number,
		num: number,
		arr: any[] = this.slice(0);

	for (i = 0; i < arr.length; i++) {
		num = parseInt(arr[i] as string, 10);
		if (String(num) === arr[i]) {
			arr[i] = num;
		}
	}
	return arr;
});
