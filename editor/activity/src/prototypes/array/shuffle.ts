///<reference path="../defineProperty.ts" />
///<reference path="shuffle.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Array.prototype, "shuffle", function(this: any[]) {
	let current = this.length;

	while (current > 0) {
		let i = Math.floor(Math.random() * current--),
			tmp = this[current];

		this[current] = this[i];
		this[i] = tmp;
	}
	return this;
});
