/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Array<T> {
	/**
	 * The <strong><code>includes()</code></strong> method determines whether an
	 * array includes a certain element, returning <code>true</code> or
	 * <code>false</code> as appropriate. 
	 * 
	 * @param searchElement The element to search for.
	 * 
	 * @param fromIndex The position in this array at which to begin searching
	 * for <code>searchElement</code>. A negative value searches from the index
	 * of array.length + fromIndex by asc. Defaults to 0.
	 * 
	 * @returns A Boolean.
	 */
	includes(searchElement: T, fromIndex?: number): boolean;
}
