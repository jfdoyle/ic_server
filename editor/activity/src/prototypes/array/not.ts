///<reference path="../defineProperty.ts" />
///<reference path="not.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Array.prototype, "not", function(this: any[], searchArray: any | any[]) {
	if (!isArray(searchArray)) {
		searchArray = [].slice.call(arguments);
	}
	return (this as any[]).filter(function(value) {
		return !searchArray.includes(value);
	});
});
