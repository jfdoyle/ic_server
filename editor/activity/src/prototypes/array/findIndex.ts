///<reference path="../defineProperty.ts" />
///<reference path="findIndex.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Array.prototype, "findIndex", function(this: any[], predicate: (value?: any, i?: number, list?: any[]) => boolean) {
	if (this === null || this === undefined) {
		throw new TypeError('Array.prototype.findIndex called on null or undefined');
	}
	if (typeof predicate !== 'function') {
		throw new TypeError('predicate must be a function');
	}
	let list = Object(this) as any[],
		length = list.length >>> 0,
		thisArg = arguments[1],
		value: any;

	for (let i = 0; i < length; i++) {
		value = list[i];
		if (predicate.call(thisArg, value, i, list)) {
			return i;
		}
	}
	return -1;
});
