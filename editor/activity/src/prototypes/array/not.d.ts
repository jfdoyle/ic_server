/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Array<T> {
	/**
	 * The <strong><code>not()</code></strong> method removes entries from one
	 * array that appear in a second array. 
	 * 
	 * @param searchArray The second array to use.
	 * 
	 * @returns A new array with entries that are only in the source array.
	 */
	not(searchArray: T[]): T[];
	not(...searchArray: T[]): T[];
}
