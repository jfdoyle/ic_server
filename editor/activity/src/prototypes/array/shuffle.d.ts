/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Array<T> {
	/**
	 * Shuffle the order of an array
	 * 
	 * @return The same array
	 */
	shuffle(): Array<T>;
}
