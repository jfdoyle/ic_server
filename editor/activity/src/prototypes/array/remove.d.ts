/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Array<T> {
	/**
	 * Remove a case sensitive entry from an array
	 * 
	 * @param searchElement to remove.
	 */
	remove(searchElement: T): Array<T>;
}
