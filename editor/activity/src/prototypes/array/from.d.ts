/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface ArrayConstructor {
	/**
	 * The <strong><code>Array.from()</code></strong> method creates a new Array
	 * instance from an array-like or iterable object.
	 * 
	 * @param arrayLike An array-like or iterable object to convert to an array.
	 * 
	 * @param mapFn Optional. Map function to call on every element of the array.
	 * 
	 * @param thisArg Optional. Value to use as this when executing mapFn.
	 */
	from(arrayLike: ArrayLike<any> | Iterable<any>, mapfn?: (v: any, k: any) => any, thisArg?: any): any[];
}
