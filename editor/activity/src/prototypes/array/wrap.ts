///<reference path="../defineProperty.ts" />
///<reference path="wrap.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Array.prototype, "wrap", function(this: any[], between: string, before?: string, after?: string) {
	if (isUndefined(between)) {
		between = "";
	}
	if (isUndefined(before)) {
		before = "";
	}
	if (isUndefined(after)) {
		after = "";
	}
	return before + this.join(after + between + before) + after;
});
