///<reference path="../defineProperty.ts" />
///<reference path="remove.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Array.prototype, "remove", function(this: any[], searchElement: any): any[] {
	let i = this.indexOf(searchElement);

	return i >= 0 ? this.splice(i, 1) : [];
});
