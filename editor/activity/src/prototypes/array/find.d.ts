/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Array<T> {
	/**
	 * The find() method returns a value in the array, if an element in the array
	 * satisfies the provided testing function. Otherwise undefined is returned.
	 */
	find(callback: (value?: T, index?: number, array?: T[]) => boolean, thisArg?: any): T;
}
