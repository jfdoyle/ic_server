///<reference path="../defineProperty.ts" />
///<reference path="find.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Array.prototype, "find", function(this: any[], predicate: (value?: any, index?: number, array?: any[]) => boolean) {
	if (this === null || this === undefined) {
		throw new TypeError('Array.prototype.find called on null or undefined');
	}
	if (typeof predicate !== 'function') {
		throw new TypeError('predicate must be a function');
	}
	let list = Object(this) as any[],
		length = list.length >>> 0,
		thisArg = arguments[1],
		value: any;

	for (let i = 0; i < length; i++) {
		value = list[i];
		if (predicate.call(thisArg, value, i, list)) {
			return value;
		}
	}
	return undefined;
});
