/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Array<T> {
	/**
	 * The <strong><code>findIndex()</code></strong> method returns an
	 * <strong>index</strong> in the array, if an element in the array satisfies
	 * the provided testing function. Otherwise -1 is returned.
	 * See also the <code>find()</code> method, which returns the
	 * <strong>value</strong> of a found element in the array instead of its
	 * index.
	 * 
	 * @param callback Function to execute on each value in the array, taking
	 * three arguments:
	 * <strong><code>element</code><strong> The current element being processed
	 * in the array.
	 * <strong><code>index</code><strong> The index of the current element being
	 * processed in the array.
	 * <strong><code>array</code><strong> The array <code>findIndex</code> was
	 * called upon.
	 * 
	 * @param thisArg Object to use as <code>this</code> when executing
	 * <code>callback</code>.
	 * 
	 * @returns An index in the array if an element passes the test; otherwise,
	 * <code>-1</code>.
	 */
	findIndex(callback: (value: T, index: number, array: T[]) => boolean, thisArg?: any): number;
}
