///<reference path="../defineProperty.ts" />
///<reference path="intersect.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Array.prototype, "intersect", function(this: any[], searchArray: any | any[]) {
	if (!isArray(searchArray)) {
		searchArray = [].slice.call(arguments);
	}
	return this.filter(function(value) {
		return (searchArray as any[]).includes(value);
	});
});
