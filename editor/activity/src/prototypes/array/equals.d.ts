/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Array<T> {
	/**
	 * Object tree comparison
	 * 
	 * @returns True if they have the same content.
	 */
	equals(target: Array<T>): boolean;
}
