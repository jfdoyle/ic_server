///<reference path="../defineProperty.ts" />
///<reference path="equals.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Array.prototype, "equals", function(this: any[], target: any[]) {
	if (!target || this.length !== target.length) {
		return false;
	}
	for (let index = 0; index < this.length; index++) {
		if (typeof this[index] !== typeof target[index] || !this[index] !== !target[index]) {
			return false;
		}
		switch (typeof (this[index])) {
			case "object":
				if (this[index] !== null && target[index] !== null && (this[index].constructor.toString() !== target[index].constructor.toString() || !this[index].equals(target[index]))) {
					return false;
				}
				break;
			case "function":
				if (this[index].toString() !== target[index].toString()) {
					return false;
				}
				break;
			default:
				if (this[index] !== target[index]) {
					return false;
				}
		}
	}
	return true;
});
