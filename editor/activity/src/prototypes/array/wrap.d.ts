/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Array<T> {
	/**
	 * Join and wrap elements of an array
	 * 
	 * @param between Insert text between every item.
	 * 
	 * @param before Insert text before every item (and before "between" if
	 * necessary).
	 * 
	 * @param after Insert text after every item (and after "between" if
	 * necessary).
	 * 
	 * @return A string
	 */
	wrap(between: string, before?: string, after?: string): string;
}
