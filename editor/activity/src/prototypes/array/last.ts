///<reference path="../defineProperty.ts" />
///<reference path="last.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Array.prototype, "last", function(this: any[]) {
	return this[this.length - 1];
});
