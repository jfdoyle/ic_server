/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Array<T> {
	/**
	 * The <strong><code>intersect()</code></strong> method merges two arrays
	 * into a single one that only contains items in both arrays. 
	 * 
	 * @param searchArray The second array to use.
	 * 
	 * @returns A new array. If the new array has duplicate values then this
	 * would need to be chained to <code>unique()</code>.
	 */
	intersect(searchArray: T[]): T[];
	intersect(...searchArray: T[]): T[];
}
