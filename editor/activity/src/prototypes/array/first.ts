///<reference path="../defineProperty.ts" />
///<reference path="first.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Array.prototype, "first", function(this: any[]) {
	return this[0];
});
