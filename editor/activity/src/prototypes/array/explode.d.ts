/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Array<T> {
	/**
	 * Copy an array changing any "123" strings into 123 numbers (not floating
	 * point). Named "explode" to give same output as
	 * <code>String.explode()</code>.
	 */
	explode(): T[];
}
