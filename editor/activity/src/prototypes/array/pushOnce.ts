///<reference path="../defineProperty.ts" />
///<reference path="pushOnce.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Array.prototype, "pushOnce", function(this: any[]): number {
	let that = this, i = 0, elements = arguments;

	for (; i < elements.length; i++) {
		if (!that.includes(elements[i])) {
			that.push(elements[i]);
		}
	}
	return that.length;
});
