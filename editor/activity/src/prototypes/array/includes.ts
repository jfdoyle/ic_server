///<reference path="../defineProperty.ts" />
///<reference path="includes.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Array.prototype, "includes", function(this: any[], searchElement: any, fromIndex?: number) {
	let i: number,
		currentElement: any,
		isNaN = searchElement !== searchElement;

	fromIndex = fromIndex || 0;
	for (i = fromIndex >= 0 ? fromIndex : Math.max(0, length + fromIndex); i < this.length; i++) {
		currentElement = this[i];
		if (searchElement === currentElement ||
			(isNaN && currentElement !== currentElement)) { // NaN !== NaN
			return true;
		}
	}
	return false;
});
