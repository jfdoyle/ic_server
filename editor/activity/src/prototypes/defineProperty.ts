/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

/**
 * The <strong><code>defineProperty()</code></strong> function provides a
 * shortcut to defining a property that cannot be accidentally iterated across.
 * 
 * @param proto the Prototype to add the property to.
 * 
 * @param name of the property to add.
 * 
 * @param value of the property.
 */
function defineProperty(proto: any, name: string, value: Function | any) {
	if (proto && !proto[name]) {
		Object.defineProperty(proto, name, {
			"value": value
		});
	}
}
