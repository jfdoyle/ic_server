///<reference path="../defineProperty.ts" />
///<reference path="parentElements.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Node.prototype, "parentElements", function(this: Element, stop?: string | Element, andSelf?: boolean): Element[] {
	let element = this,
		isString = typeof stop === "string",
		stopSelector = isString ? stop as string : undefined,
		stopElement = isString ? undefined : stop as Element,
		result: Element[] = andSelf ? [element] : [];

	while (element && (!stopSelector || !element.matches(stopSelector)) && (!stopElement || element !== stopElement)) {
		result.push(element = element.parentElement);
	}
	return result;
});
