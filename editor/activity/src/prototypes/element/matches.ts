///<reference path="../defineProperty.ts" />
///<reference path="matches.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Element.prototype, "matches", Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector);

// TODO: Polyfill - https://developer.mozilla.org/en/docs/Web/API/Element/matches
