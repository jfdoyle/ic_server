/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Element {
	/**
	 * The <strong><code>Element.common()</code></strong> method returns the
	 * closest common ancestor between the element itself and the element
	 * passed to it.
	 * 
	 * @param element is the other element to fnid a common ancestor with.
	 * 
	 * @returns The Element which is the closest common ancestor of the selected
	 * elements. It may be undefined.
	 */
	common(element: Element): Element;
}

interface HTMLElement {
	/**
	 * The <strong><code>Element.common()</code></strong> method returns the
	 * closest common ancestor between the element itself and the element
	 * passed to it.
	 * 
	 * @param element is the other element to fnid a common ancestor with.
	 * 
	 * @returns The Element which is the closest common ancestor of the selected
	 * elements. It may be undefined.
	 */
	common(element: HTMLElement): HTMLElement;
}
