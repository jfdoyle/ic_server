/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Element {
	/**
	 * Check if element matches a selector - already part of the standard, so
	 * just a shim if needed
	 */
	matches(selector: string): boolean;
}

