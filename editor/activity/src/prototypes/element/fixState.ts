///<reference path="../defineProperty.ts" />
///<reference path="fixState.d.ts" />
///<reference path="../string/startsWith.d.ts" />
/* 
 * Copyright &copy; 2013 Infuze Ltd, All Rights Reserved.
 */

defineProperty(Element.prototype, "fixState", function(this: Element, currentState?: { [state: string]: boolean }, index?: number) {
	let element = this as HTMLElement,
		tagName = element.tagName;

	if (tagName.startsWith("IC-")) {
		let i: number,
			state: string,
			className: string,
			attributeNameClass: string,
			attribute: Attr,
			tagNameClass = tagName.toLowerCase().replace(/^ic-/, "ict-"),
			attributes = element.attributes,
			currentClass = element.classList,
			addClass: { [name: string]: boolean } = {};

		addClass[tagNameClass] = true;
		if (index !== undefined && index >= 0) {
			addClass[tagNameClass + "-" + index] = true;
		}
		for (i = 0; i < attributes.length; i++) {
			attribute = attributes[i];
			if (attribute.name.startsWith("ic-")) {
				attributeNameClass = attribute.name.replace(/^ic-/, "ict-");
				if (attributeNameClass === "state") {
					if (attribute.value.trim()) {
						attribute.value.split(/\s+/).forEach(function(state) {
							addClass["ict-state-" + state] = true;
						});
					}
				} else {
					addClass[attributeNameClass] = true;
					if (attribute.value && !/[^a-z0-9_-]/.test(attribute.value)) {
						addClass[attributeNameClass + "-" + attribute.value] = true;
					}
				}
			}
		}
		if (currentState) {
			for (state in currentState) {
				if (currentState[state] === true) {
					addClass["ict-state-" + state] = true;
				}
			}
		}
		for (i = currentClass.length - 1; i >= 0; i--) {
			className = currentClass[i];
			if (className.startsWith("ict-") && !addClass[className]) {
				currentClass.remove(className);
			}
		}
		Object.keys(addClass).forEach(function(className) {
			currentClass.add(className);
		});
	}
});
