///<reference path="../defineProperty.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

// Based on :scope polyfill - http://stackoverflow.com/a/17989803/3328079

(function(doc: Document, proto: any) {
	try { // check if browser supports :scope natively
		doc.querySelector(":scope body");
	} catch (e) { // polyfill native methods if it doesn"t
		["querySelector", "querySelectorAll"].forEach(function(method) {
			let native = proto[method];

			proto[method] = function(selectors: string) {
				if (/(^|,)\s*:scope/.test(selectors)) { // only if selectors contains :scope
					let el = this as HTMLElement,
						id = el.id; // remember current element id

					el.id = "SCOPE_" + Date.now(); // assign new unique id
					selectors = selectors.replace(/((^|,)\s*):scope/g, "$1#" + el.id); // replace :scope with #ID
					let result = (doc as any as {[key: string]: any})[method](selectors);

					el.id = id; // restore previous id
					return result;
				} else {
					return native.call(this, selectors); // use native code for other selectors
				}
			}
		});
	}
})(window.document, Element.prototype);
