///<reference path="../defineProperty.ts" />
///<reference path="closest.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Element.prototype, "closest", function(this: Element, selectors: string): Element {
	let element = this;

	while (element && !element.matches(selectors)) {
		element = element.parentElement;
	}
	return element;
});
