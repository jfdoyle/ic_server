/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Element {
	/**
	 * The <strong><code>Element.closestWidget()</code></strong> method returns
	 * the closest ancestor of the current element (or the current element
	 * itself) which is a Widget. If there isn't such an ancestor, it returns
	 * null.
	 * 
	 * @returns The Element which is the closest Widget ancestor of the selected
	 * elements. It may be null.
	 */
	closestWidget(selectors?: string): Element;
}

interface HTMLElement {
	/**
	 * The <strong><code>Element.closestWidget()</code></strong> method returns
	 * the closest ancestor of the current element (or the current element
	 * itself) which is a Widget. If there isn't such an ancestor, it returns
	 * null.
	 * 
	 * @returns The Element which is the closest Widget ancestor of the selected
	 * elements. It may be null.
	 */
	closestWidget(selectors?: string): HTMLElement;
}
