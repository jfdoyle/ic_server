///<reference path="../defineProperty.ts" />
///<reference path="common.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Element.prototype, "common", function(this: Element, element: Element): Element {
	let el_1 = element ? this : undefined,
		el_2 = element;

	while (el_1 && el_2 && el_1 !== el_2) {
		while (el_2 && el_1 !== el_2) {
			el_2 = el_2.parentElement;
		}
		if (el_2) {
			break;
		}
		el_1 = el_1.parentElement;
		el_2 = element;
	}
	return el_1;
});
