/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface HTMLElement {
	/**
	 * The <strong><code>Element.closest()</code></strong> method returns the
	 * closest ancestor of the current element (or the current element itself)
	 * which matches the selectors given in parameter. If there isn't such an
	 * ancestor, it returns null.
	 * 
	 * @param selectors is a DOMString containing a selector list like
	 * <code>"p:hover, .toto + q"</code>
	 * 
	 * @returns The Element which is the closest ancestor of the selected
	 * elements. It may be null.
	 */
	closest(selectors: string): HTMLElement;
}

interface Element {
	/**
	 * The <strong><code>Element.closest()</code></strong> method returns the
	 * closest ancestor of the current element (or the current element itself)
	 * which matches the selectors given in parameter. If there isn't such an
	 * ancestor, it returns null.
	 * 
	 * @param selectors is a DOMString containing a selector list like
	 * <code>"p:hover, .toto + q"</code>
	 * 
	 * @returns The Element which is the closest ancestor of the selected
	 * elements. It may be null.
	 */
	closest(selectors: string): Element;
}
