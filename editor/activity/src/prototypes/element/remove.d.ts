/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Element {
	/**
	 * The <strong><code>Element.remove()</code></strong> method removes the
	 * object from the tree it belongs to.
	 */
	remove(): void;
}

interface HTMLElement {
	/**
	 * The <strong><code>Element.remove()</code></strong> method removes the
	 * object from the tree it belongs to.
	 */
	remove(): void;
}
