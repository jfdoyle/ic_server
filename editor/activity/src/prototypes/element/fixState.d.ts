/* 
 * Copyright &copy; 2013 Infuze Ltd, All Rights Reserved.
 */

interface Element {
	/**
	 * The <strong><code>Element.fixState()</code></strong> method sets and
	 * removes classes based on the tagName, the attributes, and a supplied list
	 * of states. All "ic-" prefixes are replaces with "ict-". All states are
	 * prefixed with "ict-state-" when true.
	 * 
	 * @param currentState An Object with <code>state:boolean</code> pairs.
	 * 
	 * @param index The index number of this widget - gets an additional class
	 * added with ict-type-index format.
	 */
	fixState(currentState?: { [state: string]: boolean }, index?: number): Element;
}
