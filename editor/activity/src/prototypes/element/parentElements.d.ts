/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface HTMLElement {
	/**
	 * The <strong><code>Element.parentElements()</code></strong> method returns
	 * an Array of all parent elements of itself, optionally including itself,
	 * stopping when it matches a selector.
	 * 
	 * @param stop is a DOMString containing a selector list like
	 * <code>"p:hover, .toto + q"</code>, or an Element to stop before.
	 * 
	 * @param andSelf Includes itself in the Array.
	 * 
	 * @returns An Array of Elements, starting with the closest.
	 */
	parentElements(stop?: string | Element, andSelf?: boolean): HTMLElement[];
}

interface Element {
	/**
	 * The <strong><code>Element.parentElements()</code></strong> method returns
	 * an Array of all parent elements of itself, optionally including itself,
	 * stopping when it matches a selector.
	 * 
	 * @param stop is a DOMString containing a selector list like
	 * <code>"p:hover, .toto + q"</code>, or an Element to stop before.
	 * 
	 * @param andSelf Includes itself in the Array.
	 * 
	 * @returns An Array of Elements, starting with the closest.
	 */
	parentElements(stop?: string | Element, andSelf?: boolean): Element[];
}

interface Node {
	/**
	 * The <strong><code>Element.parentElements()</code></strong> method returns
	 * an Array of all parent elements of itself, optionally including itself,
	 * stopping when it matches a selector.
	 * 
	 * @param stop is a DOMString containing a selector list like
	 * <code>"p:hover, .toto + q"</code>, or an Element to stop before.
	 * 
	 * @param andSelf Includes itself in the Array.
	 * 
	 * @returns An Array of Elements, starting with the closest.
	 */
	parentElements(stop?: string | Element, andSelf?: boolean): Node[];
}
