///<reference path="../defineProperty.ts" />
///<reference path="closestWidget.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Element.prototype, "closestWidget", function(this: Element, selectors?: string): Element {
	let element = this;

	while (element && !element.tagName.startsWith("IC-") && (!selectors || !element.matches(selectors))) {
		element = element.parentElement;
	}
	return element;
});
