///<reference path="../defineProperty.ts" />
///<reference path="remove.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Element.prototype, "remove", function(this: Element): void {
	if (this.parentNode) {
		this.parentNode.removeChild(this);
	}
});
