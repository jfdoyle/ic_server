/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Date {
	/**
	 * Print a formatted date.
	 * 
	 * @param format The format to use.
	 * 
	 * @returns A string containing the formatted date.
	 */
	format(format?: string): string;
}
