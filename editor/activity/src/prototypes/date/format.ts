///<reference path="../defineProperty.ts" />
///<reference path="format.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

/**
 * Print a formatted date
 * @static
 * @method format
 * @for Date
 * @param {string} format
 * @return {string}
 */
defineProperty(Date, "format", function(format) {
	return (new Date()).format(format);
});

/**
 * Print a formatted date
 * @method format
 * @for Date
 * @param {string} format
 * @return {string}
 */
defineProperty(Date.prototype, "format", function(format?: string): string {
	let output = "";

	if (isNaN(this)) {
		return "Invalid Date";
	} else {
		let that = this as Date,
			date = that.getDate(),
			day = that.getDay(),
			month = that.getMonth(),
			year = that.getFullYear(),
			hours = that.getHours(),
			minutes = that.getMinutes(),
			seconds = that.getSeconds(),
			skip = false,
			monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
			dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
			pad = function(num: number): string {
				return num <= 9 ? "0" + num : "" + num;
			},
			plural = function(num: number, word: string): string {
				return num + " " + word + (num === 1 ? "" : "s");
			},
			replace = function(letter: string): string | number {
				if (skip) {
					skip = false;
					return letter;
				}
				switch (letter) {
					case "\\":
						skip = true;
						return "";
					case "d":
						return pad(date);
					case "D":
						return dayNames[day].substr(0, 3);
					case "j":
						return date;
					case "l":
						return dayNames[day];
					case "N":
						return day + 1;
					case "S":
						return (date % 10 === 1 && date !== 11 ? "st" : (date % 10 === 2 && date !== 12 ? "nd" : (date % 10 === 3 && date !== 13 ? "rd" : "th")));
					case "w":
						return day;
					case "R":
						let i = (new Date().getTime() - that.getTime()) / 1000;
						return i < 0
							? "In the Future"
							: i < 20
								? "Just Now"
								: i < 60
									? "Less than a Minute Ago"
									: i < 120
										? "About a Minute Ago"
										: i < (60 * 60)
											? plural(Math.floor(i / 60), "Minute") + " Ago"
											: i < (2 * 60 * 60)
												? "About an Hour Ago"
												: i < (12 * 60 * 60)
													? plural(Math.floor(i / (60 * 60)), "Hour") + " Ago"
													: i < (2 * 24 * 60 * 60) && day === (new Date).getDay() // i < (24 * 60 * 60)
														? "Today"
														: i < (3 * 24 * 60 * 60) && (day === 6 ? 0 : day + 1) === (new Date).getDay() // i < (2 * 24 * 60 * 60)
															? "Yesterday"
															: i < (7 * 24 * 60 * 60)
																? plural(Math.floor(i / (24 * 60 * 60)), "Day") + " Ago"
																: i < (31 * 24 * 60 * 60)
																	? plural(Math.floor(i / (7 * 24 * 60 * 60)), "Week") + " Ago"
																	: i < (365 * 24 * 60 * 60)
																		? plural(Math.floor(i / (31 * 24 * 60 * 60)), "Month") + " Ago"
																		: plural(Math.floor(i / (365 * 24 * 60 * 60)), "Year") + " Ago";
					case "F":
						return monthNames[month];
					case "m":
						return pad(month + 1);
					case "M":
						return monthNames[month].substr(0, 3);
					case "n":
						return month + 1;
					case "L":
						return (((year % 4 === 0) && (year % 100 !== 0)) || (year) % 400 === 0) ? "1" : "0";
					case "Y":
						return year;
					case "y":
						return String(year).substr(2);
					case "a":
						return hours < 12 ? "am" : "pm";
					case "A":
						return hours < 12 ? "AM" : "PM";
					case "g":
						return hours % 12 || 12;
					case "G":
						return hours;
					case "h":
						return pad(hours % 12 || 12);
					case "H":
						return pad(hours);
					case "i":
						return pad(minutes);
					case "s":
						return pad(seconds);
					case "u":
						return String(that.getMilliseconds()).substr(0, 3);
					case "O":
						{
							let offset = that.getTimezoneOffset();
							return (offset < 0 ? "+" : "-") + pad(Math.abs(offset / 60)) + "00";
						}
					case "P":
						{
							let offset = that.getTimezoneOffset();
							return (offset < 0 ? "+" : "-") + pad(Math.abs(offset / 60)) + ":" + pad(Math.abs(offset % 60));
						}
					case "T":
						that.setMonth(0);
						let result = that.toTimeString().replace(/^.+ \(?([^\)]+)\)?$/, "$1");

						that.setMonth(month);
						return result;
					case "Z":
						return -that.getTimezoneOffset() * 60;
					case "c":
						return that.format("Y-m-d\\TH:i:sP");
					case "r":
						return String(that);
					case "U":
						return that.getTime() / 1000;
					default:
						return letter;
				}
			};

		format = format || "c";
		for (let i = 0; i < format.length; i++) {
			output += replace(format[i]);
		}
	}
	return output;
});
