///<reference path="../defineProperty.ts" />
///<reference path="getTree.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Object.prototype, "getTree", function(this: any, path: string | string[], def: any) {
	let data = this,
		fixedPath: (string | number)[] = path.explode();

	while (fixedPath.length && data !== undefined) {
		data = data[fixedPath.shift()];
	}
	return data === undefined || data === null ? def : data;
});
