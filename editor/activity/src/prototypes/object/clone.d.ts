/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Array<T> {
	/**
	 * The <strong><code>clone()</code></strong> method makes a duplicate of an
	 * <code>Array</code>, with all members also being duplicated.
	 * 
	 * @param target Clone into an existing <code>Array</code> instead of
	 * creating a new one.
	 * 
	 * @param save If cloning into an existing <code>Array</code> then this
	 * prevents it from being cleared first.
	 * 
	 * @returns A new <code>Array</code> or <code>target</code> if supplied.
	 */
	clone(target?: Array<T>, save?: boolean): Array<T>;
}

interface Date {
	/**
	 * The <strong><code>clone()</code></strong> method makes a duplicate of an
	 * <code>Date</code>.
	 * 
	 * @param target Clone into an existing <code>Date</code> instead of
	 * creating a new one.
	 * 
	 * @param save If cloning into an existing <code>Date</code> then this
	 * prevents it from being cleared first.
	 * 
	 * @returns A new <code>Date</code> or <code>target</code> if supplied.
	 */
	clone(target?: Date, save?: boolean): Date;
}

interface Object {
	/**
	 * The <strong><code>clone()</code></strong> method makes a duplicate of an
	 * <code>Object</code>, with all members also being duplicated.
	 * 
	 * @param target Clone into an existing <code>Object</code> instead of
	 * creating a new one.
	 * 
	 * @param save If cloning into an existing <code>Object</code> then this
	 * prevents it from being cleared first.
	 * 
	 * @returns A new <code>Object</code> or <code>target</code> if supplied.
	 */
	clone(target?: Object, save?: boolean): Object;
}
