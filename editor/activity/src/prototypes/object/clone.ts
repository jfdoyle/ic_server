///<reference path="../defineProperty.ts" />
///<reference path="clone.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Object.prototype, "clone", function ObjectCopyCloneFn(this: any, target?: Object | any[] | Date, save?: boolean) {
	let copy, key, value;

	// Handle the 3 simple types, and null or undefined
	if (this === null || typeof this !== "object") {
		return this;
	}
	// Handle Date
	if (this instanceof Date) {
		copy = target || new Date();
		(copy as Date).setTime((this as Date).getTime());
		return copy;
	}
	if (this instanceof Function) {
		return this;
	}
	// Handle Array
	if (this instanceof Array) {
		if (target instanceof Array) {
			copy = target;
			if (save !== false) {
				(target as any[]).length = 0;
			}
		} else {
			copy = [];
		}
		for (key = 0; key < (this as string[]).length; key++) {
			value = this[key];
			if (isObject(value)) {
				copy[key] = ObjectCopyCloneFn.call(value);
			} else {
				copy[key] = value;
			}
		}
		return copy;
	}
	// Handle Object
	if (this instanceof Object) {
		if (target instanceof Object) {
			copy = target;
			if (save !== false) {
				for (key in target) {
					if (!(this as {}).hasOwnProperty(key) && target.hasOwnProperty(key)) {
						delete target[key];
					}
				}
			}
		} else {
			copy = {};
		}
		for (key in this) {
			if ((this as {}).hasOwnProperty(key)) {
				value = this[key];
				if (value instanceof Object) {
					copy[key] = ObjectCopyCloneFn.call(value);
				} else {
					copy[key] = value;
				}
			}
		}
		return copy;
	}
	throw new Error("Unable to copy obj! Its type isn't supported.");
});
