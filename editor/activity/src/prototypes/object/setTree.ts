///<reference path="../defineProperty.ts" />
///<reference path="setTree.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

defineProperty(Object.prototype, "setTree", function(this: any, path: string | string[] | { [path: string]: any }, value: any) {
	let _reduce = function(data: any[]) {
		if (isArray(data)) {
			// reduce length of array if possible
			while (data.length && data[data.length - 1] === undefined || data[data.length - 1] === null) {
				data.pop();
			}
		}
	},
		_set = function(data: any, fixedPath: (string | number)[], fixedValue: any, depth: number) {
			let i = fixedPath[depth];

			if (depth < fixedPath.length - 1) { // Can we go deeper?
				if (isString(data[i]) && isNumber(fixedPath[depth + 1])) {
					data[i] = [data[i]]; // Change into an array
				} else if (!isObject(data[i]) && !isArray(data[i])) {
					data[i] = isNumber(fixedPath[depth + 1]) ? [] : {};
				}
				if (!_set(data[i], fixedPath, fixedValue, depth + 1) && depth >= 1 && !Object.keys(data[i])) {// Can clear out empty trees completely...
					delete data[i];
					_reduce(data);
					return false; // let further up the tree know we've deleted
				}
			} else if (fixedValue !== data[i]) {
				if (fixedValue === undefined) {
					delete data[i];
					_reduce(data);
					return false; // let further up the tree know we've deleted
				}
				data[i] = fixedValue;
			}
			return true;
		};

	if (isObject(path)) {
		for (let i in path as { [path: string]: any }) {
			if (path.hasOwnProperty(i)) {
				_set(this, i.explode(), path[i], 0);
			}
		}
	} else {
		_set(this, (path as string).explode(), value, 0);
	}
	return this;
});
