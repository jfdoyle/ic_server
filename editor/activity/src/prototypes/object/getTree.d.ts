/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Object {
	/**
	 * The <strong><code>getTree()</code></strong> method gets data from a path
	 * within an Object or Array.
	 * 
	 * @param path The path to the data. If passed as a string then use
	 * "<code>.<code>" (dots) between segments (they can be escaped with aleading
	 * slash). Numbers correspond to Array keys, strings to Object keys.
	 * 
	 * @param def The default value if the specified path does not exist.
	 * 
	 * @returns The value at the path, or the def value if not found.
	 */
	getTree(path: string | (string | number)[], def?: any): any;
}
