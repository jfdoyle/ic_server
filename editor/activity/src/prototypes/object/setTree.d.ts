/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Object {
	/**
	 * Set a nested data value in an Object
	 * @method setTree
	 * @for Object
	 * @param {(String|Array|Object)} path to data, either dot separated or an array, alternatively an object with dot_separated:value mappings
	 * @param {*=} value or undefined to clear
	 * @return {Object} this
	 */
	setTree(path: string | string[] | Object, value: any): this;
}
