///<reference path="version.ts" />
///<reference path="_all.d.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

/**
 * Shortcut to Widget / element.getBoundingClientRect()
 */
function getBoundingClientRect(el: Element | {getBoundingClientRect: () => ClientRect}, scale?: boolean) {
	let box = el.getBoundingClientRect();

	if (scale) {
		box = {
			top: box.top / ic.scaleFactor,
			right: box.right / ic.scaleFactor,
			bottom: box.bottom / ic.scaleFactor,
			left: box.left / ic.scaleFactor,
			width: box.width / ic.scaleFactor,
			height: box.height / ic.scaleFactor,
		};
	}
	return box;
}

/**
 * Shortcut to element.appendChild(el)
 */
function appendChild(parent: Element, child: Element) {
	parent.appendChild(child);
}

/**
 * Decorator for persisting data, use as below:
 * To use simply annotate the field with: @persist
 * public name[: type][ = value];
 */
function persist(target: {"_persist": string[]} | any, propertyKey: string) {
	if (target) {
		if (!target["_persist"]) {
			defineProperty(target, "_persist", []);
		}
		target["_persist"].push(propertyKey);
	}
}

/**
* Decorator turns static and non-static fields into getter-only, and
* therefore renders them "final".
* To use simply annotate the static or non-static field with: @final
*/
function final(target: any, propertyKey: string) {
	const value: any = target[propertyKey],
		fn = function(target: string, propertyKey: string, value: PropertyDescriptor) {
			Object.defineProperty(target, propertyKey, {
				"value": value,
				"writable": false,
				"enumerable": true
			});
		};

	if (isUndefined(value)) {
		Object.defineProperty(target, propertyKey, {
			"set": function(value: any) {
				fn(this, propertyKey, value);
			},
			"configurable": true
		});
	} else {
		fn(target, propertyKey, value);
	}
}

function isArray(arr: any): arr is any[] {
	return Object.prototype.toString.call(arr) === "[object Array]";
}

function isBoolean(bool: any): bool is boolean {
	return bool === true || bool === false;
}

function isFunction(fn: any): fn is Function {
	return Object.prototype.toString.call(fn) === "[object Function]";
}

function isNumber(num: any): num is number {
	return typeof num === "number";
}

function isUndefined(val: any) {
	return val === undefined;
}

function isObject(obj: any): obj is Object {
	if (!obj || String(obj) !== "[object Object]") {
		return false;
	}
	let proto = Object.getPrototypeOf(obj),
		constructor = proto && proto.hasOwnProperty("constructor") && proto.constructor;

	return !proto || (typeof constructor === "function" && String(constructor) === String(Object));
}

function isString(str: any): str is string {
	return typeof str === "string";
}

function closestScroll(node: HTMLElement): HTMLElement {
	for (; node; node = node.parentElement) {
		if (/^(auto|scroll)$/i.test(getComputedStyle(node).overflowY)) {
			return node;
		}
	}
}

interface FrameRequestCallback {
	(time?: number): void;
}

namespace ic {
	let requestAnimationFrame = window.requestAnimationFrame,
		rAFname: {[name: string]: Function} = {},
		rAFwrapper = false,
		immediateIndex = 0,
		immediateName: {[name: string]: Function} = {},
		immediateCall = false,
		immediateSecret = "cb" + Math.random(),
		scaleTop: number = 0,
		scaleLeft: number = 0,
		userAgent = navigator.userAgent,
		iPad = userAgent.includes("Safari") && !userAgent.includes("Chrome") && !userAgent.includes("Edge") && window.orientation !== undefined;

	export let scaleFactor: number = 1;

	export function fixScaling() {
		let body = document.body,
			html = document.documentElement,
			style = html.style,
			zoom = iPad ? (html.clientWidth / window.innerWidth) : 1;

		style.transform = style.transformOrigin = "";
		let box = html.getBoundingClientRect(),
			scaleWidth = Math.min(1, innerWidth * zoom / body.clientWidth),
			scaleHeight = Math.min(1, innerHeight * zoom / body.clientHeight),
			isHorizontal = scaleWidth > scaleHeight;

		// alert("zoom: " + zoom + "\nhtml.width: " + html.clientWidth + "\nwindow.innerWidth: " + window.innerWidth + "\nbody.clientWidth: " + body.clientWidth)
		scaleFactor = Math.min(scaleWidth, scaleHeight);
		style.transform = scaleFactor >= 1 ? "" : "scale(" + scaleFactor + ")";
		style.transformOrigin = scaleFactor >= 1 ? "" : (isHorizontal ? (iPad && Math.abs(window.orientation as number) === 90 ? "25%" : "50%") : "0") + " 0";
		box = html.getBoundingClientRect();
		scaleTop = box.top;
		scaleLeft = box.left;
	};

	export function getCoords(event: MouseEvent | TouchEvent, scale?: boolean): [number, number] {
		if (event) {
			let isTouch = !!(event as TouchEvent).changedTouches,
				clientX = (isTouch ? (event as TouchEvent).changedTouches[0] : event as MouseEvent).clientX,
				clientY = (isTouch ? (event as TouchEvent).changedTouches[0] : event as MouseEvent).clientY;

			return [
				scale !== false ? (clientX - scaleLeft) / scaleFactor : clientX,
				scale !== false ? (clientY - scaleTop) / scaleFactor : clientY
			];
		}
		return [0, 0];
	}

	/**
	 * Iterate over a key:callback object and call all the callbacks
	 */
	function callbackAndDelete(obj: {[name: string]: Function}, args?: any) {
		Object.keys(obj).forEach(function(name) {
			let callback = obj[name];

			delete obj[name];
			callback(args);
		});
	}

	function rAFCallback(time: number) {
		rAFwrapper = false;
		callbackAndDelete(rAFname, time);
	}

	function immediateCallback() {
		immediateCall = false;
		immediateIndex = 0;
		callbackAndDelete(immediateName);
	}

	/**
	 * Request an animation frame, optionally give a name to only call once per frame
	 * @param {(string|function())} [name] Name of callback so it's only called once per frame
	 * @param {function()} callback Function to call on the next frame
	 */
	export function rAF(name: string | FrameRequestCallback, callback?: FrameRequestCallback) {
		let fn: FrameRequestCallback;

		if (isString(name)) {
			fn = callback;
			if (callback) {
				rAFname[name] = callback;
				if (rAFwrapper) {
					fn = undefined;
				} else {
					rAFwrapper = true;
					fn = rAFCallback
				}
			} else {
				delete rAFname[name];
			}
		} else {
			fn = name as FrameRequestCallback;
		}
		if (fn) {
			if (requestAnimationFrame && !document.hidden) {
				requestAnimationFrame(fn);
			} else {
				setTimeout(fn, 16);
			}
		}
	};

	addEventListener("message", function(event: MessageEvent) {
		if (event.source == window && event.data == immediateSecret) {
			event.stopPropagation();
			immediateCallback();
		}
	}, true);

	/**
	 * Request a callback after the main thread finishes, optionally give a name
	 * to allow it to be easily deleted.
	 * @param {(string|function())} [name] Name of callback so it can be removed
	 * @param {function()} callback Function to call after the main thread
	 */
	export function setImmediate(name: string | Function, callback?: Function) {
		if (isString(name)) {
			if (callback) {
				immediateName[name] = callback;
			} else {
				delete immediateName[name];
				return;
			}
		} else if (isFunction(name)) {
			for (let i in immediateName) {
				if (immediateName[i] === name) {
					return;
				}
			}
			immediateName[String(immediateIndex++)] = name;
		} else {
			return;
		}
		if (!immediateCall) {
			immediateCall = true;
			if (postMessage) {
				postMessage(immediateSecret, "*");
			} else {
				setTimeout(immediateCallback, 0);
			}
		}
	};
};

/******************************************************************************/
/*** Document shortcuts *******************************************************/
/******************************************************************************/

function createElement(tagName: "a"): HTMLAnchorElement;
function createElement(tagName: "applet"): HTMLAppletElement;
function createElement(tagName: "area"): HTMLAreaElement;
function createElement(tagName: "audio"): HTMLAudioElement;
function createElement(tagName: "base"): HTMLBaseElement;
function createElement(tagName: "basefont"): HTMLBaseFontElement;
function createElement(tagName: "blockquote"): HTMLQuoteElement;
function createElement(tagName: "body"): HTMLBodyElement;
function createElement(tagName: "br"): HTMLBRElement;
function createElement(tagName: "button"): HTMLButtonElement;
function createElement(tagName: "canvas"): HTMLCanvasElement;
function createElement(tagName: "caption"): HTMLTableCaptionElement;
function createElement(tagName: "col" | "colgroup"): HTMLTableColElement;
function createElement(tagName: "datalist"): HTMLDataListElement;
function createElement(tagName: "del"): HTMLModElement;
function createElement(tagName: "dir"): HTMLDirectoryElement;
function createElement(tagName: "div"): HTMLDivElement;
function createElement(tagName: "dl"): HTMLDListElement;
function createElement(tagName: "embed"): HTMLEmbedElement;
function createElement(tagName: "fieldset"): HTMLFieldSetElement;
function createElement(tagName: "font"): HTMLFontElement;
function createElement(tagName: "form"): HTMLFormElement;
function createElement(tagName: "frame"): HTMLFrameElement;
function createElement(tagName: "frameset"): HTMLFrameSetElement;
function createElement(tagName: "h1" | "h2" | "h3" | "h4" | "h5" | "h6"): HTMLHeadingElement;
function createElement(tagName: "head"): HTMLHeadElement;
function createElement(tagName: "hr"): HTMLHRElement;
function createElement(tagName: "html"): HTMLHtmlElement;
function createElement(tagName: "iframe"): HTMLIFrameElement;
function createElement(tagName: "img"): HTMLImageElement;
function createElement(tagName: "input"): HTMLInputElement;
function createElement(tagName: "ins"): HTMLModElement;
function createElement(tagName: "isindex"): HTMLUnknownElement;
function createElement(tagName: "label"): HTMLLabelElement;
function createElement(tagName: "legend"): HTMLLegendElement;
function createElement(tagName: "li"): HTMLLIElement;
function createElement(tagName: "link"): HTMLLinkElement;
function createElement(tagName: "listing"): HTMLPreElement;
function createElement(tagName: "map"): HTMLMapElement;
function createElement(tagName: "marquee"): HTMLMarqueeElement;
function createElement(tagName: "menu"): HTMLMenuElement;
function createElement(tagName: "meta"): HTMLMetaElement;
function createElement(tagName: "meter"): HTMLMeterElement;
function createElement(tagName: "nextid"): HTMLUnknownElement;
function createElement(tagName: "object"): HTMLObjectElement;
function createElement(tagName: "ol"): HTMLOListElement;
function createElement(tagName: "optgroup"): HTMLOptGroupElement;
function createElement(tagName: "option"): HTMLOptionElement;
function createElement(tagName: "p"): HTMLParagraphElement;
function createElement(tagName: "param"): HTMLParamElement;
function createElement(tagName: "picture"): HTMLPictureElement;
function createElement(tagName: "pre"): HTMLPreElement;
function createElement(tagName: "progress"): HTMLProgressElement;
function createElement(tagName: "q"): HTMLQuoteElement;
function createElement(tagName: "script"): HTMLScriptElement;
function createElement(tagName: "select"): HTMLSelectElement;
function createElement(tagName: "source"): HTMLSourceElement;
function createElement(tagName: "span"): HTMLSpanElement;
function createElement(tagName: "style"): HTMLStyleElement;
function createElement(tagName: "table"): HTMLTableElement;
function createElement(tagName: "tbody" | "tfoot" | "thead"): HTMLTableSectionElement;
function createElement(tagName: "td"): HTMLTableDataCellElement;
function createElement(tagName: "template"): HTMLTemplateElement;
function createElement(tagName: "textarea"): HTMLTextAreaElement;
function createElement(tagName: "th"): HTMLTableHeaderCellElement;
function createElement(tagName: "title"): HTMLTitleElement;
function createElement(tagName: "tr"): HTMLTableRowElement;
function createElement(tagName: "track"): HTMLTrackElement;
function createElement(tagName: "ul"): HTMLUListElement;
function createElement(tagName: "video"): HTMLVideoElement;
function createElement(tagName: "xmp"): HTMLPreElement;
function createElement(tagName: string): HTMLElement;
function createElement(tagName: string): HTMLElement {
	return document.createElement(tagName);
}

function getElementById(elementId: string): HTMLElement | null {
	return document.getElementById(elementId);
}

function getElementsByTagName(tagname: "a"): HTMLAnchorElement[];
function getElementsByTagName(tagname: "applet"): HTMLAppletElement[];
function getElementsByTagName(tagname: "area"): HTMLAreaElement[];
function getElementsByTagName(tagname: "audio"): HTMLAudioElement[];
function getElementsByTagName(tagname: "base"): HTMLBaseElement[];
function getElementsByTagName(tagname: "basefont"): HTMLBaseFontElement[];
function getElementsByTagName(tagname: "blockquote"): HTMLQuoteElement[];
function getElementsByTagName(tagname: "body"): HTMLBodyElement[];
function getElementsByTagName(tagname: "br"): HTMLBRElement[];
function getElementsByTagName(tagname: "button"): HTMLButtonElement[];
function getElementsByTagName(tagname: "canvas"): HTMLCanvasElement[];
function getElementsByTagName(tagname: "caption"): HTMLTableCaptionElement[];
function getElementsByTagName(tagname: "circle"): SVGCircleElement[];
function getElementsByTagName(tagname: "clippath"): SVGClipPathElement[];
function getElementsByTagName(tagname: "col"): HTMLTableColElement[];
function getElementsByTagName(tagname: "colgroup"): HTMLTableColElement[];
function getElementsByTagName(tagname: "datalist"): HTMLDataListElement[];
function getElementsByTagName(tagname: "defs"): SVGDefsElement[];
function getElementsByTagName(tagname: "del"): HTMLModElement[];
function getElementsByTagName(tagname: "desc"): SVGDescElement[];
function getElementsByTagName(tagname: "dir"): HTMLDirectoryElement[];
function getElementsByTagName(tagname: "div"): HTMLDivElement[];
function getElementsByTagName(tagname: "dl"): HTMLDListElement[];
function getElementsByTagName(tagname: "ellipse"): SVGEllipseElement[];
function getElementsByTagName(tagname: "embed"): HTMLEmbedElement[];
function getElementsByTagName(tagname: "feblend"): SVGFEBlendElement[];
function getElementsByTagName(tagname: "fecolormatrix"): SVGFEColorMatrixElement[];
function getElementsByTagName(tagname: "fecomponenttransfer"): SVGFEComponentTransferElement[];
function getElementsByTagName(tagname: "fecomposite"): SVGFECompositeElement[];
function getElementsByTagName(tagname: "feconvolvematrix"): SVGFEConvolveMatrixElement[];
function getElementsByTagName(tagname: "fediffuselighting"): SVGFEDiffuseLightingElement[];
function getElementsByTagName(tagname: "fedisplacementmap"): SVGFEDisplacementMapElement[];
function getElementsByTagName(tagname: "fedistantlight"): SVGFEDistantLightElement[];
function getElementsByTagName(tagname: "feflood"): SVGFEFloodElement[];
function getElementsByTagName(tagname: "fefunca"): SVGFEFuncAElement[];
function getElementsByTagName(tagname: "fefuncb"): SVGFEFuncBElement[];
function getElementsByTagName(tagname: "fefuncg"): SVGFEFuncGElement[];
function getElementsByTagName(tagname: "fefuncr"): SVGFEFuncRElement[];
function getElementsByTagName(tagname: "fegaussianblur"): SVGFEGaussianBlurElement[];
function getElementsByTagName(tagname: "feimage"): SVGFEImageElement[];
function getElementsByTagName(tagname: "femerge"): SVGFEMergeElement[];
function getElementsByTagName(tagname: "femergenode"): SVGFEMergeNodeElement[];
function getElementsByTagName(tagname: "femorphology"): SVGFEMorphologyElement[];
function getElementsByTagName(tagname: "feoffset"): SVGFEOffsetElement[];
function getElementsByTagName(tagname: "fepointlight"): SVGFEPointLightElement[];
function getElementsByTagName(tagname: "fespecularlighting"): SVGFESpecularLightingElement[];
function getElementsByTagName(tagname: "fespotlight"): SVGFESpotLightElement[];
function getElementsByTagName(tagname: "fetile"): SVGFETileElement[];
function getElementsByTagName(tagname: "feturbulence"): SVGFETurbulenceElement[];
function getElementsByTagName(tagname: "fieldset"): HTMLFieldSetElement[];
function getElementsByTagName(tagname: "filter"): SVGFilterElement[];
function getElementsByTagName(tagname: "font"): HTMLFontElement[];
function getElementsByTagName(tagname: "foreignobject"): SVGForeignObjectElement[];
function getElementsByTagName(tagname: "form"): HTMLFormElement[];
function getElementsByTagName(tagname: "frame"): HTMLFrameElement[];
function getElementsByTagName(tagname: "frameset"): HTMLFrameSetElement[];
function getElementsByTagName(tagname: "g"): SVGGElement[];
function getElementsByTagName(tagname: "h1" | "h2" | "h3" | "h4" | "h5" | "h6"): HTMLHeadingElement[];
function getElementsByTagName(tagname: "head"): HTMLHeadElement[];
function getElementsByTagName(tagname: "hr"): HTMLHRElement[];
function getElementsByTagName(tagname: "iframe"): HTMLIFrameElement[];
function getElementsByTagName(tagname: "image"): SVGImageElement[];
function getElementsByTagName(tagname: "img"): HTMLImageElement[];
function getElementsByTagName(tagname: "input"): HTMLInputElement[];
function getElementsByTagName(tagname: "ins"): HTMLModElement[];
function getElementsByTagName(tagname: "isindex"): HTMLUnknownElement[];
function getElementsByTagName(tagname: "label"): HTMLLabelElement[];
function getElementsByTagName(tagname: "legend"): HTMLLegendElement[];
function getElementsByTagName(tagname: "li"): HTMLLIElement[];
function getElementsByTagName(tagname: "line"): SVGLineElement[];
function getElementsByTagName(tagname: "lineargradient"): SVGLinearGradientElement[];
function getElementsByTagName(tagname: "link"): HTMLLinkElement[];
function getElementsByTagName(tagname: "listing"): HTMLPreElement[];
function getElementsByTagName(tagname: "map"): HTMLMapElement[];
function getElementsByTagName(tagname: "marker"): SVGMarkerElement[];
function getElementsByTagName(tagname: "marquee"): HTMLMarqueeElement[];
function getElementsByTagName(tagname: "mask"): SVGMaskElement[];
function getElementsByTagName(tagname: "menu"): HTMLMenuElement[];
function getElementsByTagName(tagname: "meta"): HTMLMetaElement[];
function getElementsByTagName(tagname: "metadata"): SVGMetadataElement[];
function getElementsByTagName(tagname: "meter"): HTMLMeterElement[];
function getElementsByTagName(tagname: "nextid"): HTMLUnknownElement[];
function getElementsByTagName(tagname: "object"): HTMLObjectElement[];
function getElementsByTagName(tagname: "ol"): HTMLOListElement[];
function getElementsByTagName(tagname: "optgroup"): HTMLOptGroupElement[];
function getElementsByTagName(tagname: "option"): HTMLOptionElement[];
function getElementsByTagName(tagname: "p"): HTMLParagraphElement[];
function getElementsByTagName(tagname: "param"): HTMLParamElement[];
function getElementsByTagName(tagname: "path"): SVGPathElement[];
function getElementsByTagName(tagname: "pattern"): SVGPatternElement[];
function getElementsByTagName(tagname: "picture"): HTMLPictureElement[];
function getElementsByTagName(tagname: "polygon"): SVGPolygonElement[];
function getElementsByTagName(tagname: "polyline"): SVGPolylineElement[];
function getElementsByTagName(tagname: "pre"): HTMLPreElement[];
function getElementsByTagName(tagname: "progress"): HTMLProgressElement[];
function getElementsByTagName(tagname: "q"): HTMLQuoteElement[];
function getElementsByTagName(tagname: "radialgradient"): SVGRadialGradientElement[];
function getElementsByTagName(tagname: "rect"): SVGRectElement[];
function getElementsByTagName(tagname: "script"): HTMLScriptElement[];
function getElementsByTagName(tagname: "select"): HTMLSelectElement[];
function getElementsByTagName(tagname: "source"): HTMLSourceElement[];
function getElementsByTagName(tagname: "span"): HTMLSpanElement[];
function getElementsByTagName(tagname: "stop"): SVGStopElement[];
function getElementsByTagName(tagname: "style"): HTMLStyleElement[];
function getElementsByTagName(tagname: "svg"): SVGSVGElement[];
function getElementsByTagName(tagname: "switch"): SVGSwitchElement[];
function getElementsByTagName(tagname: "symbol"): SVGSymbolElement[];
function getElementsByTagName(tagname: "table"): HTMLTableElement[];
function getElementsByTagName(tagname: "tbody" | "tfoot" | "thead"): HTMLTableSectionElement[];
function getElementsByTagName(tagname: "td"): HTMLTableDataCellElement[];
function getElementsByTagName(tagname: "template"): HTMLTemplateElement[];
function getElementsByTagName(tagname: "text"): SVGTextElement[];
function getElementsByTagName(tagname: "textpath"): SVGTextPathElement[];
function getElementsByTagName(tagname: "textarea"): HTMLTextAreaElement[];
function getElementsByTagName(tagname: "th"): HTMLTableHeaderCellElement[];
function getElementsByTagName(tagname: "title"): HTMLTitleElement[];
function getElementsByTagName(tagname: "tr"): HTMLTableRowElement[];
function getElementsByTagName(tagname: "track"): HTMLTrackElement[];
function getElementsByTagName(tagname: "tspan"): SVGTSpanElement[];
function getElementsByTagName(tagname: "ul"): HTMLUListElement[];
function getElementsByTagName(tagname: "use"): SVGUseElement[];
function getElementsByTagName(tagname: "video"): HTMLVideoElement[];
function getElementsByTagName(tagname: "view"): SVGViewElement[];
function getElementsByTagName(tagname: "x-ms-webview"): MSHTMLWebViewElement[];
function getElementsByTagName(tagname: "xmp"): HTMLPreElement[];
function getElementsByTagName(tagname: string): HTMLElement[];
function getElementsByTagName(tagname: string): Element[] {
	return Array.prototype.slice.call(document.getElementsByTagName(tagname));
}

function querySelector(selector: string): HTMLElement;
function querySelector(root: DocumentFragment | Document | Element, selector: string): HTMLElement;
function querySelector(root: DocumentFragment | Document | Element | string, selector?: string): HTMLElement {
	if (isString(root)) {
		selector = root;
		root = document;
	}
	return root.querySelector(selector) as HTMLElement;
}

function querySelectorSVG(selector: string): SVGElement;
function querySelectorSVG(root: DocumentFragment | Document | Element, selector: string): SVGElement;
function querySelectorSVG(root: DocumentFragment | Document | Element | string, selector?: string): SVGElement {
	return querySelector(root as Element, selector) as Element as SVGElement;
}

function querySelectorAll(selector: string): HTMLElement[];
function querySelectorAll(root: DocumentFragment | Document | Element, selector: string): HTMLElement[];
function querySelectorAll(root: DocumentFragment | Document | Element | string, selector?: string): HTMLElement[] {
	if (isString(root)) {
		selector = root;
		root = document;
	}
	return root ? Array.prototype.slice.call(root.querySelectorAll(selector)) : [];
}
