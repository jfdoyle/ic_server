/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

declare const VERSION: string;
declare const DATE: string;

namespace ic {
	try {
		window.console.info("IC v" + VERSION + " (" + DATE + ")");
	} catch (e) {}

	/**
	 * Support table for Infuze Creator Editor.
	 * All widgets must add their version in this table.
	 * Any "global" css can have versions added directly to this table.
	 */
	export let version: {[attribute: string]: number} = {
		// List all supported attributes here, {attribute: version} - where the
		// version is a simple incremental number as features are added to specific
		// attributes
		"css-columns": 1
	};
}