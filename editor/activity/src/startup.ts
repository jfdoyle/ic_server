///<reference path="widget/_all.d.ts" />
///<reference path="core.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	export function startup(root: Node) {
		let node: Node,
			filter = function(node: Element) {return node.nodeType === Node.TEXT_NODE ? NodeFilter.FILTER_ACCEPT : NodeFilter.FILTER_SKIP;},
			body = document.body,
			style = getComputedStyle(body),
			fixScalingCallback = function() {
				setImmediate("fixScaling", fixScaling);
			};

		addEventListener("options-test", null, Object.defineProperty({}, "capture", {
			get: function() {
				let patchTouchEventListener = function(prototype: {addEventListener: any}) {
					if (prototype && prototype.addEventListener && !prototype.addEventListener.isTouched) {
						let oldHandler = prototype.addEventListener,
							touchListener = function(eventType: string, listener: any, options: boolean | {[key: string]: any}, wantsUntrusted?: boolean) {
								switch (eventType) {
									case "touchstart":
									case "touchmove":
									case "touchend":
										if (options === true || options === false) {
											options = {
												capture: options
											}
										} else if (!options) {
											options = {};
										}
										if (!options.passive) {
											options.passive = false;
										}
										break;
								}
								// Just in case the spec changes to allow its return to be used and this patch is still in use...
								return oldHandler.call(this, eventType, listener, options, wantsUntrusted);
							};

						(touchListener as any).isTouched = true;
						Object.defineProperty(prototype, "addEventListener", {
							value: touchListener,
							configurable: true
						});
					}
				};

				patchTouchEventListener(document);
				patchTouchEventListener(window);
				patchTouchEventListener(Element && Element.prototype);
			}
		}));
		if (!inPreview) {
			body.addEventListener("contextmenu", function(event) {
				event.preventDefault();
			}, true);
		}
		if (parseFloat(style.minWidth) && parseFloat(style.minHeight)) {
			addEventListener("orientationchange", fixScalingCallback);
			addEventListener("resize", fixScalingCallback);
			fixScalingCallback();
			document.addEventListener("touchmove", function(event) {
				if ((event as any).scale && document.documentElement.clientWidth / window.innerWidth < 1) {
					event.preventDefault();
				}
			}, {
				passive: false,
				useCapture: true
			} as any);
		}
		(filter as any).acceptNode = filter;
		let walker = document.createTreeWalker(
			root,
			NodeFilter.SHOW_TEXT,
			filter as any,
			false
		);
		while (node = walker.nextNode()) {
			let changed = false,
				text = node.nodeValue,
				startPos = text.indexOf("[[");

			if (startPos >= 0 && startPos < text.indexOf("]]")) {
				text = text.replace(/\[\[([a-z]+)\]\]/g, function($0, $1) {
					switch ($1) {
						case "title":
							changed = true;
							$1 = "JSON Title";
							break;
					}
					return $1;
				});
				if (changed) {
					node.nodeValue = text;
				}
			}
		}

		// console.info("Activity startup")

		Widget.startup(root);

		setImmediate(function() {
			// Finally tell any waiting listeners that we've started
			let event = document.createEvent("Event");

			event.initEvent("ic-startup", false, false);
			body.classList.add("ict-started");
			body.dispatchEvent(event);
		});
	}
};
