///<reference path="core.ts" />
///<reference path="api.ts" />
///<reference path="startup.ts" />
/*
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Event {
	fakeStoryboardEvent: boolean;
}

namespace IC {
	const defaultKeyDelay = 0.5;
	const defaultMouseDelay = 2;
	const defaultClickDelay = 0.25;

	interface Storyboard {
		callback: (id?: string) => void;
		script: HTMLScriptElement;
		element: HTMLElement;
		started?: boolean;
	}

	export const enum StoryboardState {
		PAUSED = -1,
		PLAYING = 0,
		STEPPING = 1,
		SKIPPING = 2
	}

	export let hasStoryboards: boolean;
	export let storyboardState: StoryboardState;
	export let storyboardName: string = "";
	export let storyboardAction: string = "";

	let waitForTime: number = 0;

	let storyboardActions: string[] = [];

	storyboardState = storyboardState || StoryboardState.PLAYING;

	let lastLeft = -1,
		lastTop = -1,
		lastElement: HTMLElement, // Used for fake mouse events
		lastTarget: HTMLElement, // Used for cursor only
		lastDisplay = "none",
		lockSelector: string,
		lockElements: HTMLElement[],
		passiveInputEvent: number,
		activeInputEvent: boolean,
		isDragging: boolean,
		cursorLeft = 0,
		cursorTop = 0,
		overlay: HTMLDivElement,
		storyboards: Storyboard[] = [],
		currentState: StoryboardState,
		stepPromise: Function,
		createOverlay = function() {
			overlay = createElement("div");
			overlay.id = "ic_overlay";
			document.body.appendChild(overlay);
			let style = getComputedStyle(overlay),
				position = style.backgroundPosition.split(" ");

			cursorLeft = parseFloat(position[0]);
			cursorTop = parseFloat(position[1]);
			["mousedown", "touchstart", "mousemove", "touchmove", "mouseup", "touchend", "click", "keydown", "keypress", "keyup"].forEach(function(event) {
				document.addEventListener(event, cancelEvents, true);
			});
			createOverlay = function() {};
		};

	/**
	 * The <strong><code>addStoryboard()</code></strong> method adds a new
	 * storyboard function call to the list.
	 * 
	 * NOTE: When included via a script element there can only be a single
	 * <code>addStoryboard()</code> call in each file and the file itself must
	 * be named "<code>storyboard.js</code>". This is due to bugs within IE11,
	 * all other browsers behave properly in finding which file the function
	 * was called from.
	 */
	export function addStoryboard(callback: (id?: string) => void, closest: string = "ic-screen") {
		let script = document.currentScript as HTMLScriptElement;

		if (!script) {
			script = querySelectorAll("script[src*='storyboard']")[storyboards.length] as HTMLScriptElement;
		}
		if (script) {
			let el = script ? script.closest(closest) as HTMLElement : undefined;

			if (!storyboards.length) {
				ic.rAF(frameUpdate);
			}
			storyboards.push({
				callback: callback,
				script: script,
				element: el
			});
			if (el && !el.id) {
				el.id = "storyboard_" + storyboards.length;
			}
			hasStoryboards = true;
		}
	}

	/**
	 * Check if the last input event was a passive or active one
	 */
	function checkActiveEvent(event: MouseEvent | TouchEvent) {
		let firstLetter = event.type[0],
			isClick: boolean;

		switch (event.type) {
			case "mouseup":
			case "touchend":
				isDragging = false;
				isClick = true;
				break;

			case "mousedown":
			case "touchstart":
				isDragging = true;
				isClick = true;
				break;

			case "mousemove":
			case "touchmove":
				isClick = isDragging;
				break;
		}
		if (isClick || firstLetter === "k" || firstLetter === "c") {
			activeInputEvent = true;
		}
	}

	/**
	 * Cancel events while the overlay is visible with a mouse pointer
	 */
	function cancelEvents(event: MouseEvent | TouchEvent) {
		if (!event.fakeStoryboardEvent) {
			let isKeyEvent = event.type[0] === "k";

			passiveInputEvent = new Date().getTime();
			if (overlay.style.display !== "none") {
				if (lockSelector) {
					let found = false;

					if (!isKeyEvent) {
						overlay.style.pointerEvents = "none";
					}
					if (lockSelector === "*") {
						found = true;
					} else {
						let isTouch = !!(event as TouchEvent).changedTouches,
							clientX = (isTouch ? (event as TouchEvent).changedTouches[0] : event as MouseEvent).clientX,
							clientY = (isTouch ? (event as TouchEvent).changedTouches[0] : event as MouseEvent).clientY,
							el = isKeyEvent ?
								document.activeElement as HTMLElement :
								document.elementFromPoint(clientX, clientY) as HTMLElement;

						while (el) {
							if (lockElements.includes(el)) {
								found = true;
								break;
							}
							el = el.parentElement;
						}
					}
					if (found) {
						return checkActiveEvent(event);
					}
					overlay.style.pointerEvents = "";
				}
				event.preventDefault();
				event.stopImmediatePropagation();
			}
		}
	}

	/**
	 * Start the next storyboard act.
	 */
	function startNextStoryboard() {
		return storyboards.some(function(storyboard) {
			let element = storyboard.element;

			if (!storyboard.started && (!element || element.offsetParent !== null)) {
				waitForTime = 0;
				storyboard.callback(element ? "#" + element.id : "body");
				return storyboard.started = true;
			}
		});
	}

	/**
	 * When the overlay is visible this will update the position of the fake
	 * cursor's mouse events, such as hover and movement.
	 */
	function frameUpdate() {
		if (lockSelector === undefined) {
			startNextStoryboard();
		}
		if (currentState !== storyboardState) {
			currentState = storyboardState;
			if (currentState >= StoryboardState.PLAYING && stepPromise) {
				stepPromise();
			}
		}
		if (overlay) {
			if (lastDisplay !== overlay.style.display) {
				lastDisplay = overlay.style.display;

				if (lastDisplay === "none" && lastElement) {
					for (let element = lastElement; element; element = element.parentElement) {
						fake(element, "mouseleave");
					}
				}
				lastElement = undefined;
			}
			if (lastDisplay !== "none" && hasFakeCursor(true)) {
				let style = getComputedStyle(overlay), // Needs to be live, not attribute
					position = style.backgroundPosition.split(" "),
					left = parseFloat(position[0]) - cursorLeft,
					top = parseFloat(position[1]) - cursorTop;

				if (lastLeft !== left || lastTop !== top) {
					overlay.style.pointerEvents = "none";
					let el = document.elementFromPoint(left, top) as HTMLElement;

					lastLeft = left;
					lastTop = top;
					fake(el, "mousemove");
					if (el && lastElement !== el) {
						let common = el.common(lastElement);

						for (let element = lastElement; element && element !== common; element = element.parentElement) {
							fake(element, "mouseleave");
						}
						for (let element = lastElement = el; element && element !== common; element = element.parentElement) {
							fake(element, "mouseenter");
						}
					}
					overlay.style.pointerEvents = "";
				}
			}
		}
		ic.rAF(frameUpdate);
	}

	function addLog(log: string): string {
		if (log) {
			storyboardActions.push(log);
			storyboardAction = storyboardActions.join(" + ");
		}
		return log;
	}

	function removeLog(log: string): void {
		if (log) {
			storyboardActions.remove(log);
			storyboardAction = storyboardActions.join(" + ");
		}
	}

	/**
	 * The <strong><code>all()</code></strong> method wraps the standard
	 * <code>Promise.all([...])</code> method allowing it to be called with
	 * <code>...rest</code> arguments.
	 */
	export function all(...promises: Promise<any>[]) {
		return Promise.all(promises);
	}

	const enum Cursor {
		hide = -1,
		none = 0,
		pointer = 1,
		click = 2,
		drag = 3
	};

	type CursorActions = "show" | "hide" | "none" | "click" | "move" | "drag" | "drop";

	export interface CursorOptions {
		left?: number;
		top?: number;
		duration?: number;
		easing?: string;
	}

	let currentCursor = Cursor.hide;

	/**
	 * The <strong><code>setFakeCursor()</code></strong> method changes which
	 * cursor is being displayed when the fake overlay is visibile.
	 * 
	 * @param which The cursor to display
	 */
	function setFakeCursor(which: Cursor) {
		if (currentCursor !== which) {
			currentCursor = which;
			if (which === Cursor.hide) {
				overlay.removeAttribute("data-cursor");
			} else {
				overlay.setAttribute("data-cursor", ["none", "pointer", "click", "drag"][which]);
			}
		}
	}

	/**
	 * The <strong><code>hasFakeCursor()</code></strong> method determines if
	 * there is a currently displayed fake cursor. When the cursor is "none"
	 * then it is optionally classed as visible as the real cursor ir not
	 * visible.
	 * 
	 * @param visible If the cursor is there but not visible.
	 */
	function hasFakeCursor(visible?: boolean) {
		return currentCursor > (visible ? Cursor.none : Cursor.hide);
	}

	/*
	 * The <strong><code>then()</code></strong> method wraps a normal function
	 * in a thenable - it will pass on the function result, of the input result
	 * if the function does not return anything.
	 * 
	 * @param fn The function to call (without brackets).
	 * 
	 * @param ...args The arguments for the function.
	 */
	function then(fn: Function, ...args: any[]) {
		return function(data?: any) {
			let result = fn(...args);
			return isUndefined(result) ? data : result;
		};
	}

	const HIGHLIGHT_CLASSNAME = "ic-storyboard";

	function highlightRemove(this: HTMLElement, event: AnimationEvent) {
		//console.log("remove classname", this)
		this.removeEventListener("animationend", highlightRemove);
		this.classList.remove(HIGHLIGHT_CLASSNAME);
	}

	/**
	 * The <strong><code>highlight()</code></strong> method adds a class to an
	 * element that (by default) will run a "flashing outline" animation on that
	 * element. It will automatically remove the outline when finished.
	 * 
	 * If called on an element that is already highlighted then it will remove
	 * the highlight. This allows for custom highlight animations including
	 * infinite repeats.
	 * 
	 * @param selector The target to highlight.
	 */
	export function highlight(selector: string): () => Promise<any> {
		return function() {
			return highlight.promise(selector);
		}
	}

	export namespace highlight {
		export function promise(selector: string) {
			[].forEach.call(document.querySelectorAll(selector), function(element: HTMLElement) {
				if (element.classList.contains(HIGHLIGHT_CLASSNAME)) {
					highlightRemove.call(element);
				} else {
					//console.log("add classname", element)
					element.addEventListener("animationend", highlightRemove)
					element.classList.add(HIGHLIGHT_CLASSNAME);
				}
			});
			return Promise.resolve(true);
		}
	}

	/**
	 * The <strong><code>cursor()</code></strong> method provides fake cursor
	 * behaviour, including mouse events on elements. There is an extra delay
	 * before and after mouse clicks, with movement being taken care of via css
	 * transition.
	 * 
	 * @param action The action to be taken:<br>
	 * "show": Show the cursor as a pointer<br>
	 * "hide": Hide the cursor<br>
	 * "click": Click on a widget<br>
	 * "drag": Perform a mousedown (and optionally move)<br>
	 * "move": Move to a widget (can be called after a target-free drag)<br>
	 * "drop": Perform a mouseup (useful after drag + move)
	 * 
	 * @param selector The target to move to during the action.
	 * 
	 * @param duration The duration of the movement phase.
	 * 
	 * @param options The various options for movement.
	 * 
	 * @param options.left The left offset to apply when moving
	 * (in <code>rem</code> units).
	 * 
	 * @param options.top The top offset to apply when moving
	 *  (in <code>rem</code> units).
	 *  
	 * @param options.duration The duration of the movement phase.
	 */
	export function cursor(action: CursorActions, duration?: number): () => Promise<any>;
	export function cursor(action: CursorActions, options?: CursorOptions): () => Promise<any>;
	export function cursor(action: CursorActions, selector?: string, duration?: number): () => Promise<any>;
	export function cursor(action: CursorActions, selector?: string, options?: CursorOptions): () => Promise<any>;
	export function cursor(action: CursorActions, selector?: string | number | CursorOptions, options?: number | CursorOptions): () => Promise<any> {
		return function() {
			return cursor.promise(action, selector, options);
		}
	}

	export namespace cursor {
		export function promise(action: CursorActions, selector?: string | number | CursorOptions, options?: number | CursorOptions) {
			if (storyboardState === StoryboardState.SKIPPING) {
				return Promise.resolve(true);
			}
			return new Promise(function(resolve, reject) {
				createOverlay();
				if (!isUndefined(selector) && !isString(selector)) {
					options = selector;
					selector = undefined;
				}
				if (isNumber(options)) {
					options = {duration: options};
				}
				let log: string,
					target = isString(selector)
						? lastTarget = querySelector(selector)
						: options && (isNumber(options.left) || isNumber(options.top))
							? lastTarget
							: undefined,
					duration = options && isNumber(options.duration) ? options.duration : defaultMouseDelay,
					transition = "background-position " + duration + "s " + (options && options.easing ? options.easing : "ease-in-out"),
					style = overlay.style,
					promise: Promise<any>;

				function setPosition(data?: any) {
					if (target) {
						let targetBox = target.getBoundingClientRect(),
							left = options && isNumber((options as CursorOptions).left) ? (options as CursorOptions).left : 0,
							top = options && isNumber((options as CursorOptions).top) ? (options as CursorOptions).top : 0,
							rem = left || top ? parseFloat(getComputedStyle(document.body).fontSize) : 1;

						if (!log) {
							log = addLog("Move");
						}
						style.backgroundPositionX = (left * rem) + cursorLeft + targetBox.left + (targetBox.width / 2) + "px";
						style.backgroundPositionY = (top * rem) + cursorTop + targetBox.top + (targetBox.height / 2) + "px";
					}
					return data;
				}

				if (action === "show"
					|| action === "none") {
					if (target) {
						style.transition = "none";
						setPosition();
						removeLog(log);
						overlay.clientHeight; // Force recalc
					}
					style.transition = transition;
					setFakeCursor(action === "show" ? Cursor.pointer : Cursor.none);
					style.display = "block";
					resolve();
				} else if (action === "hide") {
					if (lockSelector !== undefined) {
						setFakeCursor(Cursor.hide);
					} else {
						style.display = "none";
					}
					resolve();
				} else if (action === "click"
					|| action === "drag"
					|| action === "drop"
					|| action === "move") {
					style.display = "block";
					style.transition = transition;
					promise = Promise.resolve();

					if ((action === "click" || action === "move") && !hasFakeCursor(true)) {
						promise = promise
							.then(then(setFakeCursor, Cursor.pointer));
					}
					if (action === "drag") {
						log = addLog("Drag");
						promise = promise
							.then(then(setFakeCursor, Cursor.drag))
							.then(fake.promise("mousedown"))
							.then(delay(defaultClickDelay));
					}
					if (target) {
						promise = promise
							.then(setPosition)
							.then(delay(duration));
					}
					if (action === "click") {
						log = addLog("Click");
						promise = promise
							.then(then(setFakeCursor, Cursor.click))
							.then(delay(defaultClickDelay))
							.then(then(setFakeCursor, Cursor.pointer))
							.then(delay(defaultClickDelay))
							.then(fake.promise("click"));
					} else if (action === "drop" || (action === "drag" && target)) {
						promise = promise
							.then(then(setFakeCursor, Cursor.pointer))
							.then(fake.promise("mouseup"))
							.then(fake.promise("mouseup"))
							.then(delay(defaultClickDelay));
					}
					promise.then(function(data: any) {
						removeLog(log);
						resolve(data);
					});
				} else {
					reject("Unknown cursor action: \"" + action + "\"");
				}
			});
		}
	}

	/**
	 * The <strong><code>delay()</code></strong> method waits for a number of
	 * seconds before resolving, which can be useful to sync the virtual cursor
	 * to audio.
	 * 
	 * @param seconds The number of seconds to delay.
	 */
	export function delay(seconds: number) {
		return function(data: any) {
			return delay.promise(seconds, data);
		}
	}

	export namespace delay {
		export function promise(seconds: number, data: any = true) {
			if (storyboardState === StoryboardState.SKIPPING) {
				return Promise.resolve(true);
			}
			return new Promise(function(resolve, reject) {
				let log = addLog("Delay " + seconds + "s");

				setTimeout(function() {
					removeLog(log);
					resolve(data);
				}, seconds * 1000);
			});
		}
	}

	type MouseEvents = "click" | "mousemove" | "mousedown" | "mouseup" | "mouseenter" | "mouseleave";

	/**
	 * The <strong><code>fake()</code></strong> method files a fake mouse event
	 * to elements underneath the virtual cursor.
	 */
	function fake(element: Element, eventType: MouseEvents, relatedTarget?: Element): Event {
		if (element) {
			let event: MouseEvent | TouchEvent;

			if (eventType === "click") {
				fake(element, "mousedown", relatedTarget);
				fake(element, "mouseup", relatedTarget);
			}
			event = document.createEvent("MouseEvent");
			event.initMouseEvent(eventType, true, eventType as any !== "mousemove", window, 0,
				document.body.scrollLeft + lastLeft, document.body.scrollTop + lastTop, lastLeft, lastTop,
				false, false, false, false,
				0, relatedTarget as Element || document.body.parentNode);
			event.fakeStoryboardEvent = true;
			(element || document.body).dispatchEvent(event);
			return event;
		}
	}

	namespace fake {
		export function promise(eventType: MouseEvents) {
			return function(data: any) {
				return new Promise(function(resolve, reject) {
					fake(lastElement, eventType);
					resolve(data);
				});
			}
		}
	}

	/**
	 * The <strong><code>idle()</code></strong> method waits until the user has
	 * been idle for a period of time. Normally this will have user input
	 * (typing or mouse click, not mouse movement) reject the
	 * <code>Promise</code> instead.
	 * 
	 * @param seconds The number of seconds of idle (no mouse movement or
	 * clicking) to wait.
	 * 
	 * @param ignoreClick Do not reject on user input. If passing a number then
	 * this is the number of seconds to wait after the last click before
	 * continuing
	 */
	export function idle(seconds: number, ignoreClick?: boolean | number) {
		return function(data: any) {
			return idle.promise(seconds, ignoreClick, data);
		}
	}

	export namespace idle {
		export function promise(seconds: number, ignoreClick?: boolean | number, data: any = true) {
			if (storyboardState === StoryboardState.SKIPPING) {
				return Promise.resolve(true);
			}
			activeInputEvent = false;
			passiveInputEvent = new Date().getTime();
			return new Promise(function(resolve, reject) {
				let log = addLog("Idle " + seconds + "s" + (isNumber(ignoreClick) ? " / " + ignoreClick + "s" : ""));

				let idle = setInterval(function() {
					if (new Date().getTime() >= passiveInputEvent + seconds * 1000) {
						removeLog(log);
						resolve(data);
						clearInterval(idle);
					} else if (activeInputEvent) {
						if (isNumber(ignoreClick)) {
							activeInputEvent = false;
							seconds = ignoreClick;
						} else if (isUndefined(ignoreClick) || ignoreClick === false) {
							removeLog(log);
							reject("click")
							clearInterval(idle);
						}
					}
				}, 500);
			});
		}
	}

	/**
	 * The <strong><code>lock()</code></strong> method must be called at the
	 * start of the storyboard. This locks the display preventing any user
	 * interraction, and prevens other storyboards from starting.
	 * 
	 * @param selector An element that allows user input, all attempts outside
	 * this (both keyboard and mouse) will silently be blocked. Pass "*" to
	 * allow it full access.
	 */
	export function lock(selector: string = "") {
		return function(data: any) {
			return lock.promise(selector, data);
		}
	}

	export namespace lock {
		export function promise(selector: string = "", data: any = true) {
			return new Promise(function(resolve, reject) {
				//storyboardAction = "Lock" + (selector ? " (" + selector + ")" : "");
				createOverlay();
				lockSelector = selector;
				lockElements = !lockSelector || lockSelector === "*" ?
					undefined :
					Array.prototype.slice.call(document.querySelectorAll(selector));
				overlay.style.display = "block";
				if (selector) {
					setFakeCursor(Cursor.hide);
				} else {
					overlay.style.pointerEvents = "";
					if (!hasFakeCursor()) {
						setFakeCursor(Cursor.none);
					}
				}
				resolve(data);
			});
		}
	}

	/**
	 * The <strong><code>play()</code></strong> method plays an audio file, and
	 * resolves the <code>Promise</code> when the file has finished playing.
	 * 
	 * @param url The path to the audio file to be played
	 */
	export function play(url: string) {
		return function(data: any) {
			return play.promise(url, data);
		}
	}

	interface promiseStorage {
		resolve: (value?: any) => void;
		reject: (reason?: any) => void;
		data?: any;
		log?: string;
	}

	let audioPromises: {[src: string]: promiseStorage[]} = {};

	function resolveOrReject(promises: promiseStorage[], action: "resolve" | "reject", reason?: any) {
		while (promises.length) {
			let promise = promises.pop();

			removeLog(promise.log);
			(promise as any)[action](reason || promise.data);
		}
	}

	function audioEnded(this: HTMLAudioElement) {
		this.pause(); // IE11 fix
		resolveOrReject(audioPromises[this.src], "resolve");
	}

	function audioError(this: HTMLAudioElement) {
		resolveOrReject(audioPromises[this.src], "reject", "Audio error");
	}

	export namespace play {
		export function promise(url: string, data: any = true) {
			if (storyboardState === StoryboardState.SKIPPING) {
				return Promise.resolve(true);
			}
			return new Promise(function(resolve, reject) {
				let audio = querySelector("audio[src='" + url + "']") as HTMLAudioElement;

				if (!audio) {
					let source = querySelector("audio>source[src='" + url + "']");

					if (source) {
						audio = source.parentElement as HTMLAudioElement;
					}
				}
				if (!audio) {
					audio = new Audio();

					createOverlay();
					audio.preload = "auto";
					audio.autoplay = true;
					audio.style.display = "none";
					audio.src = url;
					overlay.appendChild(audio);
				}
				let promiseList = audioPromises[audio.src] = audioPromises[audio.src] || [];

				querySelectorAll("audio").forEach((audio: HTMLAudioElement) => {audio.pause()})
				promiseList.push({
					resolve: resolve,
					reject: reject,
					data: data,
					log: addLog("Play " + url)
				});
				audio.addEventListener("error", audioError);
				audio.addEventListener("ended", audioEnded);
				audio.play();
			});
		}
	}

	/**
	 * The <strong><code>race()</code></strong> method wraps the standard
	 * <code>Promise.race([...])</code> method allowing it to be called with
	 * <code>...rest</code> arguments.
	 */
	export function race(...promises: Promise<any>[]) {
		return Promise.race(promises);
	}

	/**
	 * The <strong><code>restart()</code></strong> method restarts all
	 * storyboards. They will not trigger until the storyboard is unlocked
	 * however.
	 */
	export function restart() {
		storyboards.forEach(function(storyboard) {
			storyboard.started = false;
		});
	}

	/**
	 * The <strong><code>resolve()</code></strong> method is for use in a
	 * <code>Promise.catch()</code> block to return control to the normal
	 * <code>.then()</code> blocks.
	 */
	export function resolve(data: any = true) {
		return data;
	}

	/**
	 * The <strong><code>step()</code></strong> method adds a step to the
	 * storyboard. When in the editor this allows you to quickly jump through
	 * steps, or run one and pause again.
	 */
	export function step(name: string) {
		return function(data: any) {
			storyboardName = name;
			storyboardAction = "";
			if (storyboardState !== StoryboardState.PLAYING) {
				storyboardState = StoryboardState.PAUSED;
				return new Promise(function(resolve, reject) {
					stepPromise = resolve.bind(this, data);
				});
			}
			return data;
		}
	}

	/**
	* The <strong><code>type()</code></strong> method types a string into an
	* input box. If the current string is incorrect then it will delete
	* characters from the end until it can start typing. Due to browser
	* security it has to fake this typing, so it fires a "<code>change</code>"
	* event after every letter.
	*/
	export function type(text: string, seconds?: number) {
		return function(data: any) {
			return type.promise(text, seconds, data);
		}
	}

	export namespace type {
		export function promise(text: string, seconds: number = defaultKeyDelay, data: any = true) {
			if (storyboardState === StoryboardState.SKIPPING) {
				return Promise.resolve(true);
			}
			return new Promise(function(resolve, reject) {
				let element = !lastTarget
					? undefined
					: lastTarget.nodeName === "INPUT" && (lastTarget as HTMLInputElement)
						? lastTarget as HTMLInputElement
						: querySelector(lastTarget, "input[type=text]") as HTMLInputElement,
					typeSomething = function() {
						let value = element.value;

						if (text === value) {
							removeLog(log);
							resolve(data);
						} else {
							if (text.startsWith(value)) {
								element.value += (text as string)[value.length];
							} else {
								element.value = value.slice(0, -1);
							}
							let event = document.createEvent("Event");

							setTimeout(typeSomething, seconds * 1000);
							event.initEvent("change", true, false);
							event.fakeStoryboardEvent = true;
							element.dispatchEvent(event);
						}
					},
					log = addLog("Type \"" + text + "\"");

				if (element) {
					typeSomething();
				} else {
					removeLog(log);
					reject("Make sure to select a text input element first");
				}
			});
		}
	}

	/**
	 * The <strong><code>query()</code></strong> method checks if a selector
	 * exists, then rejects the promise if it is found.
	 * 
	 * @param invert Reject the promise if it's not found instead.
	 */
	export function query(selector: string, invert?: boolean) {
		return function(data: any) {
			return query.promise(selector, invert, data);
		}
	}

	export namespace query {
		export function promise(selector: string, invert?: boolean, data: any = true) {
			return new Promise(function(resolve, reject) {
				if (!querySelector(selector) === !!invert) {
					reject("Selector found")
				} else {
					resolve(data);
				}
			});
		}
	}

	/**
	 * The <strong><code>unlock()</code></strong> method unlocks the display
	 * and allows the next storyboard to start.
	 */
	export function unlock(data: any) {
		return unlock.promise(data);
	}

	export namespace unlock {
		export function promise(data: any = true) {
			return new Promise(function(resolve, reject) {
				storyboardName = storyboardAction = lockSelector = "";
				lockElements = undefined;
				if (!startNextStoryboard()) {
					lockSelector = undefined;
					overlay.style.display = "none";
				}
				setFakeCursor(Cursor.hide);
				resolve(data);
			});
		}
	}

	/**
	 * The <strong><code>waitFor()</code></strong> method waits for a specific
	 * number of seconds to have passed. If called with no arguments then the
	 * timer is reset to zero.
	 */
	export function waitFor(seconds: number) {
		return function(data: any) {
			return waitFor.promise(seconds, data);
		}
	}

	export namespace waitFor {
		export function promise(seconds: number, data: any = true) {
			return new Promise(function(resolve, reject) {
				let frame = function() {
					if (new Date().getTime() > waitForTime + (seconds * 1000)) {
						resolve(data);
					} else {
						requestAnimationFrame(frame);
					}
				};

				if (!seconds) {
					waitForTime = new Date().getTime();
					resolve(data);
				} else {
					requestAnimationFrame(frame);
				}
			});
		}
	}
};
