///<reference path="widget.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	let selector = "ic-question";

	/**
	 * Version of QuestionWidget
	 */
	version[selector] = 1;

	export class QuestionWidget extends Widget {
		static selector = selector;
	}
};
