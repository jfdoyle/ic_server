///<reference path="widget_button_state.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	export class SubmitButtonWidget extends StateButtonWidget {
		static selector = "ic-button[ic-button=submit]";

		/**
		 * Only need some widgets answered, otherwise require them all. Certain
		 * types (such as sortables or toggles) may be in the "correct" state to
		 * begin with, so the button needs to have sensible defaults.
		 */
		needSome: boolean;
		/**
		 * Only enable until this number of attempts, then permanently disable.
		 */
		maxAttempts: number;

		constructor(element: HTMLElement) {
			super(element);

			switch (element.getAttribute("ic-submit")) {
				case "any":
				case "some":
					this.needSome = true;
			}
			this.maxAttempts = parseInt(element.getAttribute("ic-attempts") || 0 as any, 10);
		}

		startup() {
			let closestWidget = this.closestWidget;

			// TODO: Need a better way to tell if something can be "empty"
			if (!this.needSome || this.get(closestWidget, DroppableWidget).find(drag => !drag.isSortable) || this.get(closestWidget, ToggleWidget).find(toggle => toggle.isScored)) {
				this.needSome = true;
			}
			super.startup();
		}

		onClick(event: MouseEvent | TouchEvent) {
			if (!this.isDisabled(event)) {
				let screen = this.screenWidget,
					root = screen || this.rootWidget;

				if (!root.hasState(["marked", "reveal"])) {
					let activity = this.activityWidget;

					if (activity) {
						activity.attempts = (activity.attempts || 0) + 1;
					}
					if (screen) {
						screen.attempts = (screen.attempts || 0) + 1;
					}
					if (screen !== root) {
						root.attempts = (root.attempts || 0) + 1;
					}
					root.addState("marked");
					Widget.trigger(".submit", screen);
				}
			}
			super.onClick(event);
		}

		checkState = () => {
			let disabled = true,
				closestWidget = this.closestWidget;

			if ((!this.maxAttempts || closestWidget.attempts < this.maxAttempts)
				&& (!closestWidget.hasState(["marked", "reveal"]))) {
				let widgets: (InputWidget | ActivityWidget)[] = this.get(closestWidget, InputWidget).filter(widget => widget.isScored);

				if (!widgets.length) {
					widgets = this.get(closestWidget, ActivityWidget).filter(widget => widget.isScored);
				}
				if (widgets.length) {
					if (this.needSome) {
						disabled = !widgets.some(widget => widget.hasAnswered);
					} else {
						disabled = !widgets.every(widget => widget.hasAnswered);
					}
				}
			}
			this.toggleState("disabled", disabled);
		}
	}
};
