///<reference path="widget.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	/**
	 * Version of InputWidget
	 */
	version["scoring"] = 1;

	function callInternalMark(widget: InputWidget | ActivityWidget) {
		if (widget.needMarking) {
			widget.needMarking = false;
			widget.internalMark();
		}
	}

	let scoreChanged: {[screen: number]: {[screen: number]: true}} = {},
		isScoring: boolean;

	function scoreEvent() {
		let event = document.createEvent("Event");

		event.initEvent("ic-score", false, false);
		(event as any).icData = scoreChanged;
		scoreChanged = {};
		document.body.dispatchEvent(event);
	}

	export class InputWidget extends Widget implements OnReset, OnSubmit { // NOTE: Cannot be abstract as TypeScript annotations don't allow us to search for it
		static isInputWidget = true;

		/**
		 * Set to prevent marking from working on this widget.
		 */
		public noMarking: boolean;
		/**
		 * Set to indicate that this widget needs marking in the next marking
		 * cycle.
		 */
		public needMarking: boolean;
		public markable: JsonData = {
			score: 0,
			min: 0,
			max: 0
		}

		public startState: {[state: string]: boolean};

		constructor(element: HTMLElement) {
			super(element);

			let mark = this.markable;

			// this gets either our own data object, or data from our activity's json object
			(this.data
				|| ((this.parentWidget ? this.parentWidget.data : {}) || {}).getTree([this.selector, String(this.realIndex)], {}) as Object)
				.clone(mark, true);
			if (isString(mark.points)) {
				let points = parseFloat(mark.points);

				if (!isNaN(points) && points == (mark.points as any)) {
					mark.points = points;
				}
			}
		}

		startup() {
			// Buttons are only widgets if they're custom widgets, otherwise leave well enough alone
			this.startState = {};
			Object.keys(this.state).forEach((state: WidgetState) => {
				if (!TemporaryState.includes(state)) {
					this.startState[state] = this.state[state];
				}
			});
			this.startState["disabled"] = this.state["disabled"] || false;
			this.startState["reveal"] = this.state["reveal"] || false;
			this.on([".submit", ".reset"]);
		}

		get hasAnswered(): boolean {
			return true;
		}

		/**
		 * Check if this element or one of it's ancestors can be scrolled
		 */
		public get isScrollable(): boolean {
			for (let element = this.element; element && element !== document.documentElement; element = element.parentElement) {
				let vertical = element.scrollTop || element.scrollHeight > element.clientHeight,
					horizontal = element.scrollLeft || element.scrollWidth > element.clientWidth;

				if (vertical || horizontal) {
					let style = getComputedStyle(element);

					if ((vertical && style.overflowY !== "hidden") || (horizontal && style.overflowX !== "hidden")) {
						return true;
					}
				}
			}
			return false;
		}

		onReset(screen: ScreenWidget) {
			if ((!screen || this.screenWidget === screen) && this.startState) {
				this.toggleState(this.startState);
			}
		}

		onSubmit(screen: ScreenWidget) {
			if ((!screen || this.screenWidget === screen) && this.startState) {
				this.addState("disabled");
			}
		}

		static eval(maths: string, fail?: boolean): number {
			let num = parseFloat(maths);

			if (!isNaN(num) && String(num) === maths) {
				return num;
			} else if (!/[a-z\[\],]/i.test(maths)) {
				let result: any;

				try {
					result = eval(maths);
					//console.log("Eval", maths, result)
					if (result === true) {
						return 1;
					}
					let val = parseFloat(result);
					return isNaN(val) ? 0 : parseFloat(val.toPrecision(12));
				} catch (error) {
					if (!fail && !(error instanceof ReferenceError)) {
						console.error("Error: Broken maths check:", maths, result, error);
					}
				}
			}
			return fail ? undefined : 0;
		}

		static methods: {[name: string]: (args: string, value: true | string, mark?: JsonData) => string} = {
			"correct": function(args, value, mark) {
				let opts = args.regex(/([0-9]+(?:\.[0-9]*)?)/g) as number[];

				if (!opts) {
					if (value === true) {
						return String(mark.total);
					}
					return String(mark.correct);
				}
				if (value === true) {
					return String(1);
				}
				if (opts.length === 1) {
					return String(mark.correct >= opts[0] ? 1 : 0);
				}
				return String(mark.correct.range(opts[0], opts[1]) ? 1 : 0);
			},
			"ceil": function(args) {
				return String(Math.ceil(InputWidget.eval(args)));
			},
			"fixed": function(args, value, mark) {
				let opts = args.split(",") as string[],
					val = opts.length ? InputWidget.eval(opts.pop()) : parseFloat((value === true ? "" : value).replace(/[^0-9\.]/g, ""));

				if (opts.length) {
					return String(parseFloat(val.toFixed(InputWidget.eval(opts[1]))));
				}
				return String(parseFloat(val.toFixed(12)));
			},
			"find": function(args, value, mark) {
				if (value === true) {
					return String(1);
				}
				let opts = args.split(",");

				if (opts.includes(opts[0], 1)) {
					return String(1);
				}
				return String(0);
			},
			"floor": function(args) {
				return String(Math.floor(InputWidget.eval(args)));
			},
			"get": function(args) {
				let name = args.trim();

				//console.log("getting", name, this[name])
				return this[name] || "";
			},
			"grouped": function(args, value) {
				let opts = args.regex(/(\[[^\]]+\]|\d+)/g) as any[],
					count: number = -1,
					correct = 0,
					total = 0;

				if (opts) {
					if (isNumber(opts[0])) {
						count = opts.shift();
					}
					opts.forEach(function(opt: string) {
						let values = opt.regex(/(\d+)/g) as number[];

						if (value || (values && !values.some(function(value) {
							return !value;
						}))) {
							total += values.length;
							correct++;
						}
					});
					if (value && count) {
						count = -1;
					}
				}
				return String(count < 0 ? total : !count ? correct : count === correct ? total : -Math.PI); // TODO: Use NaN as we need to return an irrational number as that's the only way to force it to be wrong
			},
			"if": function(args, value) {
				let opts = args.split(","),
					what = InputWidget.eval(opts.shift());

				if (value === true) {
					return String(Math.max(opts[0] ? parseFloat(opts[0]) : 1, opts[1] ? parseFloat(opts[1]) : 0));
				}
				return String(opts[what ? 0 : 1] || (what ? 1 : 0));
			},
			"map": function(args, value) {
				let opts = args.split(","),
					what = InputWidget.eval(opts.shift()),
					best = 0;

				opts.some(function(opt) {
					let map = opt.regex(/^\s*([0-9]+(?:\.[0-9]*)?)\s*:\s*([0-9]+(?:\.[0-9]*)?)\s*$/) as number[];

					if (value === true) {
						best = Math.max(best, map[1]);
					} else if (map[0] <= what) {
						best = map[1];
					} else {
						return true;
					}
				});
				return String(best);
			},
			"number": function(args) {
				return args.replace(/[^0-9\.]/g, "") || String(0);
			},
			"max": function(args, value, mark) {
				if (value === true) {
					return String(1);
				}
				let opts = args.regex(/([0-9]+(?:\.[0-9]*)?)/g) as number[];

				if (opts.length > 1) {
					return String(Math.max.apply(Math, opts));
				}
				return String(parseFloat(value.replace(/[^0-9\.]/g, "")) <= opts[0] ? 1 : 0);
			},
			"min": function(args, value, mark) {
				if (value === true) {
					return String(1);
				}
				let opts = args.regex(/([0-9]+(?:\.[0-9]*)?)/g) as number[];

				if (opts.length > 1) {
					return String(Math.min.apply(Math, opts));
				}
				return String(parseFloat(value.replace(/[^0-9\.]/g, "")) >= opts[0] ? 1 : 0);
			},
			"range": function(args, value, mark) {
				if (value === true) {
					return String(1);
				}
				let opts = args.regex(/([0-9]+(?:\.[0-9]*)?)/g) as number[];

				if (opts.length > 2) {
					return String(Math.range.apply(Math, opts));
				}
				return String(parseFloat(value.replace(/[^0-9\.]/g, "")).range(opts[0], opts[1]) ? 1 : 0);
			},
			"rx": function(args, value, mark) {
				if (value === true) {
					return String(1);
				}
				let opts = args.regex(/(.*?)(?:,?\s?\/(.*?)\/([ipP]*))/) as string[],
					match = opts[1],
					flags = opts[2],
					flagSmallP = flags.includes("p"),
					flagLargeP = flags.includes("P"),
					flagSmallI = flags.includes("i");

				if (flagLargeP || flagSmallP) {
					// Replace any anchors that shouldn't be in there
					match = match.replace(/(^\^?\s*|\s*\$?$)/g, "");
				}
				if (flagSmallP) {
					// replace any spaces or punctuation within the string with allowed characters - [ !\"\\#$%&'()*+,-\\.\\/:;<=>?@\\[]^_`{|}~]
					match = match.replace(/ +/g, "[ !\"\\#$%&'()*+,-\\.\\/:;<=>?@\\[\\]^_`{|}~]+");
				}
				if (flagLargeP || flagSmallP) {
					// Allow punctuation at the beginning and end of the line
					match = "^" + match.replace(/(^\^?\s*|\s*\$?$)/g, "").replace(/(^|$)/g, "[ !\"\\#$%&'()*+,-\\.\\/:;<=>?@\\[\\]^_`{|}~]*") + "$";
				}
				//console.log(opts[0] || value, opts[1], match, new RegExp(match, flagSmallI ? "i" : ""))
				return String(new RegExp(match, flagSmallI ? "i" : "").test(opts[0] || value) ? 1 : 0);
			},
			"set": function(args) {
				let name = "",
					value = args.replace(/[\n\r\t]/g, "").replace(/\s*(.*?)\s*,\s*/, function($0, $1) {
						name = $1;
						return "";
					});

				this[name] = value;
				//console.log("setting", name, this[name])
				return "";
			},
			"total": function(args, value, mark) {
				return String(mark.total);
			},
			"unique": function(args, value, mark) {
				if (value === true) {
					return String(1);
				}
				let opts = args.split(",");

				for (let i = 0; i < opts.length; i++) {
					if (opts.includes(opts[i], i + 1)) {
						return String(0);
					}
				}
				return String(1);
			},
			"": function(args) {
				let result = InputWidget.eval(args, true);

				if (result !== undefined) {
					return String(result)
				}
			}
		};

		static methodRx = new RegExp("(" + Object.keys(InputWidget.methods).join("|") + ")\\(([^\\(\\)]*)\\)");

		// Cache on first use
		static get types(): {[name: string]: {new(...args: any[]): Widget;}} {
			Object.defineProperty(this, "types", {
				value: {
					"act": ActivityWidget,
					"drag": DraggableWidget,
					"drop": DroppableWidget,
					"option": OptionWidget,
					"select": SelectWidget,
					"text": TextWidget,
					"toggle": ToggleWidget
				}
			});
			return this.types;
		};

		// Cache on first use
		static get typeRx(): RegExp {
			Object.defineProperty(this, "typeRx", {
				value: new RegExp("#(this|attempts|(?:act(s?)(\\d+))?(acts?|" + Object.keys(InputWidget.types).join("|") + ")([+-]?)(\\d+))(%|\\$|@|=|!)?#", "g")
			});
			return this.typeRx;
		}

		/**
		 * Expand any references in either answers or values
		 */
		expandReferences(str: string, cache: {[name: string]: string}, max?: boolean): string {
			return str.replace(InputWidget.typeRx, ($0: string, $match: string, $activities: string, $act: string, $widgettype: string, $relative, $index: string, $type: string) => { // First replace any references to other widgets
				if (cache[$0]) {
					return cache[$0];
				}
				let widget: InputWidget,
					noWidget = false,
					result = isBoolean(max) ? "0" : "";

				//console.log("options", $0, "match:", $match, "activities:", $activities, "act:", $act, "widgettype:", $widgettype, "index:", $index, "type:", $type)
				try {
					if ($match === "attempts") {
						result = String((this.activityWidget || this.screenWidget || this.rootWidget || {} as {attempts: number}).attempts || 0);
						noWidget = true;
					} else if ($match === "this") {
						widget = this;
					} else {
						let isActType = $widgettype === "act",
							isActsType = $widgettype === "acts",
							baseIndex = parseInt($index),
							index = $relative === "-" ? this.realIndex - baseIndex : $relative === "+" ? this.realIndex + baseIndex : baseIndex,
							act = $act
								? parseInt($act, 10)
								: isActType || isActsType
									? index
									: 0,
							actType = $activities || isActsType ? ActivitiesWidget : ActivityWidget,
							parent = $act === "0"
								? this.screenWidget
								: act
									? this.get(this.screenWidget || this.rootWidget, actType).find(function(activity) {
										return activity.index === act && activity.constructor === actType;
									})
									: this instanceof ActivityWidget
										? this
										: (this.activityWidget || this.parentWidget);

						if (isActType || isActsType) {
							widget = parent as ActivityWidget as any;
						} else {
							let widgetType = InputWidget.types[$widgettype];

							widget = this.get(parent, widgetType).find(function(widget: InputWidget) {
								if (widget.constructor === widgetType && widget.realIndex === index && (!$activities || widget.parentWidget === parent)) {
									return true;
								}
							}) as InputWidget;
						}
					}
					if (widget) {
						let widgetMark = widget.markable,
							getValue = function() {
								return isString(widgetMark.value) ? widgetMark.value as string : isArray(widgetMark.value) ? (widgetMark.value as string[]).join("|") : "";
							};

						if (widgetMark) {
							result = String($type
								? ($type === "@"
									? widgetMark.max || 0
									: $type === "$"
										? widgetMark.score || 0
										: $type === "%"
											? (widgetMark.scaled || 0) * 100
											: $type === "="
												? getValue() || 0
												: $type === "!"
													? widgetMark.raw || 0
													: getValue())
								: (max === true
									? widgetMark.max || 0
									: max === false || isScoring
										? widgetMark.score || 0
										: getValue()));
						}
						//console.log("Expand", $0, "=", result)
					} else if (!noWidget) {
						console.warn("Unable to find widget for", $0);
					}
				} catch (e) {
					console.error("Error: Unknown widget type:", $0, e);
				}
				return cache[$0] = result;
			});
		}

		parse(expr: string, max?: boolean | string): string {
			let mark = this.markable,
				found = true,
				cache: {[name: string]: string} = {},
				expression = this.expandReferences(expr, cache, isBoolean(max) ? max : undefined);

			//			if (isBoolean(max)) {
			expression = expression
				.replace(/@(?!#)/g, String(mark.max || 0))
				.replace(/\$(?![\/#])/g, String((max === true ? mark.max : mark.raw) || 0))
				.replace(/%(?!#)/g, String((max === true ? 100 : mark.scaled * 100) || 0));
			//.replace(/\?\?/g, (isArray(mark.value) ? (mark.value as string[]).join("|") : mark.value as string) || "")
			//			}
			cache = {};
			while (found) {
				found = false;
				//console.log("expression", expression)
				// TODO: Needs a tokenizer to fix matching brackets
				expression = expression.replace(InputWidget.methodRx, function($0, $fn: string, $args: string) {
					try {
						let result = InputWidget.methods[$fn].call(cache, $args.trim().replace(/\{\{/g, "(").replace(/\}\}/g, ")"), max, mark)

						//console.log("result", $0, result)
						if (result === undefined) {
							result = $0;
						} else {
							found = true;
						}
						//console.log("Calling", "\"" + $0 + "\" =", result)
						return result;
					} catch (e) {
						console.error("Error: Scripting error", $0, e);
						return "";
					}
				});
			}
			let final = InputWidget.eval(expression, true);
			//console.log("Final", expression, "=", final)
			return final === undefined ? expression : String(final);
		}

		getPoints(max?: boolean): number {
			let points = this.markable.points || 0;

			if (isNumber(points)) {
				return points as number;
			}
			isScoring = true;
			return parseFloat(this.parse(points as string, max === true || undefined));
		}

		get isScored(): boolean {
			return (this.markable.points || 0) >= 0;
		}

		_expandedAnswers: (string | RegExp)[];
		get expandedAnswers(): (string | RegExp)[] {
			let answers = this._expandedAnswers;

			if (!answers) {
				let mark = this.markable,
					answer = mark.answer,
					cache: {[name: string]: string} = {};

				answers = [];
				if (answer) {
					(isArray(answer) ? answer as string[] : [answer as string]).forEach(answer => {
						if (answer[0] === "\\" || answer[0] === "@") {
							answers.push(answer.substr(1));
							return;
						}
						if (answer[0] === "=") {
							answer = answer.substr(1);
							if (/^\/.*\/i?$/.test(answer)) {
								new RegExp(answer.substr(1, answer.length - 2)).expand().forEach(answer => answers.push(answer));
								return;
							}
						}
						answers.push(answer);
					});
					for (let i = 0; i < answers.length; i++) {
						answers[i] = this.expandReferences(answers[i] as string, cache);
						if ((answers[i] as string).indexOf(".") >= 0) {
							answers[i] = new RegExp(answers[i] as string);
						}
					}
				}
				this._expandedAnswers = answers;
			}
			return answers;
		}

		isCorrect(value: string, answer: string): boolean {
			let fixedAnswer: string = answer;

			//console.log("Checking", value, answer, fixedAnswer)
			if (fixedAnswer[0] === "\\") {
				fixedAnswer = fixedAnswer.substr(1);
			} else if (fixedAnswer[0] === "@") {
				fixedAnswer = fixedAnswer.substr(1);
				if (/^(-?[0-9]+|-?[0-9]*\.[0-9]+)$/.test(fixedAnswer)) {
					let first = true,
						parts = fixedAnswer.split(".");

					parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, function() {
						if (first) {
							first = false;
							return "([ ,]?)";
						}
						return "(?:\\1)";
					});
					if (!parts[0] || parts[0] === "0") {
						parts[0] = "0?";
					} else if (parts[0] === "-0") {
						parts[0] = "-0?";
					}
					//console.log("RegExp", new RegExp("^" + parts.join("\.") + "$"))
					return new RegExp("^" + parts.join("\.") + "$").test(value);
				} else {
					return value.toLocaleLowerCase() === fixedAnswer.toLocaleLowerCase();
				}
			} else if (fixedAnswer[0] === "=") {
				fixedAnswer = fixedAnswer.substr(1);
				let mark = this.markable

				if (fixedAnswer === "") {
					return !mark.value.length;
				}
				if (fixedAnswer === "*") {
					return !!mark.value.length;
				}
				if (/^\/.*\/[ipP]*$/.test(fixedAnswer)) {
					fixedAnswer = InputWidget.methods["rx"](fixedAnswer, value, mark);
				} else {
					isScoring = false;
					fixedAnswer = this.parse(fixedAnswer, value);
				}
				//console.log("Result", fixedAnswer)
				if (fixedAnswer === "1") {
					return true;
				} else if (fixedAnswer === "0") {
					return false;
				}
			}
			return value === fixedAnswer;
		}

		internalMark() {
			let isActivity = this instanceof ActivityWidget;

			if (isActivity || this.activityWidget) {
				let mark = this.markable,
					value = mark.value,
					answer = mark.answer,
					changed = false,
					cache: {[name: string]: string} = {};

				if (!isActivity) {
					mark.raw = mark.scaled = 0;
					if (value && answer) {
						let activity = this.activityWidget,
							usedAnswers: string[] = [],
							correctAnswers: string[] = mark.correctAnswers = !activity.allowDuplicates && !(this instanceof ToggleWidget) && [],
							expandRx = (answer: string): string => {
								return answer.replace(/rx\(.*?\)/g, match => this.expandReferences(match, cache));
							};

						if (correctAnswers) {
							this.get(activity, this.constructor as any, this.group, false).forEach(function(widget: InputWidget) {
								let answer = widget.markable.correctAnswers;

								if (answer && answer.length) {
									usedAnswers.pushOnce.apply(usedAnswers, answer);
								}
							});
						}
						if (isString(value)) {
							if (isString(answer)) {
								let expandedAnswer = expandRx(answer);

								if (!usedAnswers.includes(expandedAnswer) && this.isCorrect(value as string, expandedAnswer as string)) {
									if (correctAnswers) {
										correctAnswers.pushOnce(expandedAnswer);
									}
									mark.raw = 1;
								}
							} else if (isArray(answer)) {
								// When something can match multiple answers, it needs to mark them all as used
								(answer[correctAnswers ? "forEach" : "some"] as typeof answer.some)(answerString => {
									let expandedAnswer = expandRx(answerString);

									if (!usedAnswers.includes(expandedAnswer) && this.isCorrect(value as string, expandedAnswer)) {
										if (correctAnswers) {
											correctAnswers.pushOnce(expandedAnswer);
										}
										mark.raw = 1;
										return true;
									}
								});
							}
						} else if (isArray(value)) {
							if (isString(answer)) {
								let expandedAnswer = expandRx(answer);

								(value as string[]).forEach((value) => {
									if (this.isCorrect(value, expandedAnswer)) {
										mark.raw += 1;
									}
								});
								if (correctAnswers && mark.raw) {
									correctAnswers.pushOnce(expandedAnswer);
								}
							} else if (isArray(answer)) {
								if (mark.order) {
									value.forEach((value, index) => {
										let expandedAnswer = expandRx(answer[index]);

										if (this.isCorrect(value, expandedAnswer)) {
											if (correctAnswers) {
												correctAnswers.pushOnce(expandedAnswer);
											}
											mark.raw++;
										}
									});
								} else {
									for (let i = 0, found = false; i < value.length; i++) {
										// When something can match multiple answers, it needs to mark them all as used
										((answer as string[])[correctAnswers ? "forEach" : "some"] as typeof answer.some)(answerString => {
											let expandedAnswer = expandRx(answerString);

											if (!usedAnswers.includes(expandedAnswer) && this.isCorrect(value[i], expandedAnswer)) {
												usedAnswers.pushOnce(expandedAnswer);
												if (correctAnswers) {
													correctAnswers.pushOnce(expandedAnswer);
												}
												return found = true;
											}
										});
										if (found) {
											mark.raw++;
											found = false;
										}
									}
								}
							}
						}
						mark.scaled = Math.range(0, (mark.raw - (mark.min || 0)) / (mark.max || 1), 1);
					} else if (!value && !answer) {
						mark.scaled = 1;
					}
				} else {
					mark.scaled = Math.range(0, (mark.raw - (mark.min || 0)) / (mark.max || 1), 1);
				}
				switch (typeof mark.points) {
					case "number":
						mark.maxPoints = (mark.points as number);
						mark.score = mark.scaled * mark.maxPoints;
						break;
					case "string":
						mark.maxPoints = this.getPoints(true);
						mark.score = mark.scaled || isActivity ? this.getPoints() : 0;
						if (mark.maxPoints) {
							// Can be correct but have a zero max score
							mark.scaled = mark.score / mark.maxPoints;
						}
						break;
					default:
						mark.maxPoints = mark.max;
						mark.score = mark.raw;
						break;
				}
				if (mark.round) {
					mark.score = Math.floor(mark.score);
				}
				if (isNumber(mark.score) && !isNaN(mark.score) && mark.score >= 0 && mark.maxPoints !== -1 && mark.maxPoints !== undefined) {
					changed = this.setAttribute({
						"data-mark": String(mark.score),
						"data-percent": String(Math.floor(mark.scaled * 100)),
						"data-points": String(mark.maxPoints)
					});
				} else {
					changed = this.removeAttribute(["data-mark", "data-percent", "data-points"]);
				}
				if (changed) {
					let screenIndex = this.screenWidget.realIndex,
						activityIndex = (isActivity ? this : this.activityWidget).realIndex;

					Widget.trigger(".score", this);
					scoreChanged[screenIndex] = scoreChanged[screenIndex] || {};
					scoreChanged[screenIndex][activityIndex] = true;
					setImmediate("score", scoreEvent);
				}
			}
		}

		static internalMark() {
			let root = Widget.root;

			if (root) {
				root.get(InputWidget).forEach(callInternalMark);
				root.get(ActivityWidget).filter(activity => !(activity instanceof ActivitiesWidget)).forEach(callInternalMark);
				root.get(ActivitiesWidget).forEach(callInternalMark);
			}
		}

		mark() {
			if (!this.needMarking && !this.noMarking) {
				this.needMarking = true;
				this.markable.correctAnswers = null;
				// Need to mark all other Activities on screen in case of cross dependancies
				this.get(this.screenWidget || this.rootWidget, ActivityWidget, true).forEach(function(activity) {
					activity.mark();
				});
				setImmediate(InputWidget.internalMark);
			}
		}
	}
};
