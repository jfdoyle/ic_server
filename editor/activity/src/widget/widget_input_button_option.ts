///<reference path="widget_input_button.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	let selector = "ic-option";

	/**
	 * Version of OptionWidget
	 */
	version[selector] = 0;

	export class OptionWidget extends InputButtonWidget implements OnClick {
		static selector = selector;

		public noMarking = true;

		constructor(element: HTMLElement) {
			super(element);

			let mark = this.markable;

			if (!mark.value) {
				mark.value = element.textContent.replace(/[^\x20-\x7E]+/g, "").trim() || ("option" + this.index);
			}
			this.on("click");
		}

		get hasAnswered(): boolean {
			return false;
		}

		onClick(event: MouseEvent | TouchEvent) {
			if (!this.isDisabled(event)) {
				event.preventDefault();
				this.toggleState({
					"checked": true,
					"click": false,
					"hover": false
				}).get(this.parentWidget, OptionWidget, false).forEach(function(widget) {
					widget.removeState(["checked", "click", "hover"]);
				});
				if (event) {
					// TODO: only attempt on answer, or on any interraction?s
					if (this.parentWidget) {
						this.parentWidget.addState("attempted");
					}
					if (this.activityWidget) {
						this.activityWidget.addState("attempted");
					}
				}
			}
		}
	}
};
