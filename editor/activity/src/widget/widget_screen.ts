///<reference path="widget.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface HTMLImageElement {
	icOriginalImage: HTMLImageElement;
}

namespace ic {
	let selector = "ic-screen";

	/**
	 * Version of ScreenWidget
	 */
	version[selector] = 1;

	const enum SCREEN_MODE {
		DEFAULT = 0,
		ONCE,
		ALWAYS
	}

	export class ScreenWidget extends Widget implements OnPersist, OnScreen, OnState, OnTimeout {
		static selector = selector;
		static isInputWidget = true;
		static isTreeWidget = true;

		private mode: SCREEN_MODE;

		public detached: boolean;
		private reattach: boolean;
		public readonly dom: DocumentFragment = document.createDocumentFragment();

		/**
		 * Have all images loaded?
		 */
		private loaded: boolean;
		private loadCount: number;

		constructor(element: HTMLElement) {
			super(element);

			switch (element.getAttribute(selector)) {
				case "once":
					this.mode = SCREEN_MODE.ONCE;
					break;
				case "always":
					this.mode = SCREEN_MODE.ALWAYS;
					break;
			}
			if (!this.hasState("active")) {
				element.style.display = "none";
			}
			this.on([".persist", ".state", ".screen", ".timeout"]);
		}

		public seen: boolean;

		@persist
		public attempts: number = 0;

		public toggleRange: {[group: number]: ToggleWidget[]};

		private loadImage = (img: HTMLImageElement) => {
			let src = img.getAttribute("data-src");

			if (!img.icOriginalImage) {
				let image = img.icOriginalImage = new Image();

				image.icOriginalImage = img;
				image.onload = () => {
					img.src = image.src;
					img.removeAttribute("data-src");
					if (image.srcset || image.sizes) {
						img.srcset = image.srcset;
						img.sizes = image.sizes;
						img.removeAttribute("data-srcset");
						img.removeAttribute("data-sizes");
					}
					delete img.icOriginalImage;
					if (!--this.loadCount && this.reattach) {
						this.attach();
					}
				};
				image.onerror = function() {
					setTimeout(function() {
						image.src = "";
						image.src = src;
					}, ScreensWidget.trickle);
				};
				image.src = src;
				image.srcset = img.getAttribute("data-srcset") || "";
				image.sizes = img.getAttribute("data-sizes") || "";
			}
		}

		loadImages(force?: boolean) {
			if (!this.loaded) {
				let images = (this.element.firstChild ? this.element : this.dom).querySelectorAll("img[data-src]") as NodeListOf<HTMLImageElement>;

				this.loadCount = images.length;
				if (images[0]) {
					if (force) {
						[].forEach.call(images, this.loadImage);
					} else {
						this.loadImage(images[0]);
					}
					return false;
				}
				this.loaded = true;
			}
			return true;
		}

		detach = () => {
			if (Widget.started && !this.detached && this.mode !== SCREEN_MODE.ALWAYS) {
				let element = this.element;

				while (element.firstChild) {
					this.dom.appendChild(element.firstChild);
				}
				this.detached = true;
				this.reattach = false;
			}
		}

		attach = () => {
			if (Widget.started && this.detached && this.mode !== SCREEN_MODE.ALWAYS) {
				if (this.loadCount) {
					this.reattach = true;
					this.get<Widget>(ButtonWidget, InputWidget, TimerWidget).forEach(function(widget: Widget) {
						if (!widget.hasState("disabled")) {
							widget.lazy_pause = true;
							widget.addState("disabled");
						}
					});
				} else {
					let dom = this.dom;

					while (dom.firstChild) {
						this.element.appendChild(dom.firstChild);
					}
					// Unpause every time as anything might have asked to pause
					this.get<Widget>(ButtonWidget, InputWidget, TimerWidget, "disabled").forEach(function(widget: Widget) {
						if (widget.lazy_pause) {
							delete widget.lazy_pause;
							widget.removeState("disabled");
						}
					});
					this.reattach = this.detached = false;
				}
			}
		}

		onPersist(state?: any): any {
			return this.persistTree({
				"activity": ActivityWidget
			}, state);
		}

		onState(widget: Widget, stateList: WidgetState[]) {
			if (this === widget) {
				if (stateList.includes("active")) {
					if (this.hasState("active")) {
						this.element.style.display = "";
						this.rootWidget.screen = this.index - 1;
						if (!this.seen) {
							this.seen = true;
							let top = querySelector(this.element, "a[name='#top']"),
								scroll = closestScroll(top);

							if (top && scroll) {
								scroll.scrollTop = getBoundingClientRect(top).top - getBoundingClientRect(scroll).top;
							}
						}
						if (!this.get(this, ActivityWidget, "active", false).length) {
							this.get(this, ActivityWidget).some(function(widget) {
								if (widget.constructor === ActivityWidget) {
									widget.activate();
									return true;
								}
							})
						}
					} else {
						this.element.style.display = "none";
						this.get(this, "active").forEach(function(widget: Widget) {
							widget.removeState("active");
						});
						if (this.mode === SCREEN_MODE.ONCE) {
							this.addState("disabled");
						}
					}
				}
			} else if (widget.screenWidget === this && widget instanceof ActivityWidget) {
				stateList.intersect(["visible", "visited", "attempted"]).forEach((state) => {
					if (widget.hasState(state as any)) {
						this.addState(state as any);
					}
				});
			}
		}

		onScreen(screen: ScreenWidget) {
			//this.onState(this, ["active"]);
		}

		onTimeout() {
			console.log("timeout", this.get(ScreenWidget), this.get(this, ActivityWidget))
			if (this.index >= this.get(ScreenWidget).length && !this.get(this, ActivityWidget).length) {
				this.addState("active");
			}
		}
	}
};
