///<reference path="widget_input.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	let selector = "ic-droppable";

	/**
	 * Version of DroppableWidget
	 */
	version[selector] = 1;

	export class DroppableWidget extends InputWidget implements OnReset, OnReveal, OnPersist, OnMouseEnter, OnMouseLeave {
		static selector = selector;

		/**
		 * Tree Widgets help define the WidgetNode tree, if it's not a tree then
		 * it's a leaf.
		 */
		static isTreeWidget = true;

		isColour: boolean;
		isHorizontal: boolean;
		isVertical: boolean;
		isSortable: boolean;
		count: number;
		draggables: DraggableWidget[] = [];
		public startDraggables: DraggableWidget[] = [];
		public pins: PinWidget[];
		public dragPins: boolean;
		private lastPin: PinWidget;
		private preLoaded: DraggableWidget[] = [];

		public dropShapes: Element[];

		private icOnDropFn: string;

		constructor(element: HTMLElement) {
			super(element);

			let mark = this.markable,
				pinCount = element.querySelectorAll(":scope>ic-pins>ic-pin").length;

			switch (element.getAttribute(DroppableWidget.selector)) {
				case "colour":
					this.index = -1;
					this.isColour = true;
					break;

				case "horizontal":
					this.index = -1;
					this.isHorizontal = true;
					break;

				case "vertical":
					this.index = -1;
					this.isVertical = true;
					break;

				case "shape":
					let shapes = element.querySelectorAll(":scope>svg .drop");

					if (shapes.length) {
						this.dropShapes = Array.from(shapes);
					}
				// Deliberate fallthrough
				case "position":
					this.index = -1;
					this.isHorizontal = true;
					this.isVertical = true;
					break;

				case "sortable":
					this.isSortable = true;
					break;
			}
			this.count = this.isSortable ? 1 : pinCount || Math.max(parseInt(element.getAttribute(DroppableWidget.selector + "-count"), 10) || element.querySelectorAll(":scope>ic-draggable").length || 1, 1);
			let points = isArray(mark.answer) ? Math.min(this.count, mark.answer.length) : mark.answer || this.isSortable ? 1 : -1;

			if (!pinCount && element.querySelectorAll(":scope>ic-draggable>ic-pins>ic-pin").length) {
				this.dragPins = true;
			}
			if (mark.answer) {
				mark.max = points;
			}
			if (isUndefined(mark.points)) {
				mark.points = points;
			}
			this.icOnDropFn = element.getAttribute("onDrop");
			if (this.count > 1) {
				this.index = -1;
			}
		}

		startup() {
			if (!this.isSortable) {
				let pins = this.get(this, PinWidget).filter((pin) => {return pin.parent === this;});

				if (pins.length) {
					let value = this.markable.value,
						drags = isString(value) ? [value as string] : isArray(value) ? value as string[] : [];

					this.pins = pins;
					this.markable.value = drags;
					if (drags) {
						drags.forEach((target) => {
							let dragIndex = target.regex(/#drag([0-9]+)#/) as number,
								drag = this.get(this.activitiesWidget || this.activityWidget, DraggableWidget, this.group).find(function(drag) {
									return drag.index === dragIndex;
								});

							if (drag) {
								this.preLoaded.push(drag);
								drag.preLoaded.push(this);
								// TODO: delay this for race conditions?
								drag.getPin().linkPin(drag, this);
							}
						});
					}
					// This also gets cloned via any child draggable startup
					this.startDraggables = this.draggables.clone();
				}
			}
			this.on([".persist", ".reveal"])
				.update();

			super.startup();
		}

		rememberDrag(drag: DraggableWidget) {
			let drop = drag.droppables[0];

			if (drop) {
				drag.droppables.remove(drop);
				drop.draggables.remove(drag);
			}
			this.draggables.pushOnce(drag);
			drag.droppables.pushOnce(this);
		}

		get hasAnswered(): boolean {
			let mark = this.markable;

			return mark.points >= 0 && !this.hasState("empty") && !isUndefined(mark.value);
		}

		onPersist(state?: any[]): any[] {
			if (isUndefined(state)) {
				// Save state
				state = [];

				this.draggables.forEach((drag) => {
					// If we're positioned then [index, horiz%, vert%], otherwise just store index
					let value: number | number[] = drag.index;

					if (this.isHorizontal || this.isVertical) {
						// Get drag or pin position
						let el = (this.pins ? this.pins.find((pin) => {
							return pin.drag === drag;
						}) : drag).element;

						value = [value as number];
						if (this.isHorizontal) {
							(value as number[])[1] = parseFloat(el.style.left) || 0;
						}
						if (this.isVertical) {
							(value as number[])[2] = parseFloat(el.style.top) || 0;
						}
					}
					state.push(value);
				});
				return state;
			}
			if (isArray(state)) {
				// Load state
				let drags = this.get(this.activitiesWidget || this.activityWidget, DraggableWidget),
					pinElements: HTMLElement[] = [];

				state.forEach((value, index) => {
					// Make sure we're dealing with [index] or [index, horiz%, vert%]
					let values: number[] = isArray(value) ? value as number[] : [value as number],
						drag = drags.find(function(drag) {
							return drag.index === values[0];
						});

					if (drag) {
						let oldDrop = drag.droppableWidget;

						if (oldDrop !== this) {
							// Standard code to make sure drags<>drops are linked correctly
							if (oldDrop) {
								drag.droppables.remove(oldDrop);
								oldDrop.draggables.remove(drag);
							}
							// push or index?
							drag.droppables.pushOnce(this);
							this.draggables.pushOnce(drag);
							if (this.pins) {
								let pin = drag.pins[drag.droppables.indexOf(this)];

								pin.linkPin(drag, this, this.pins[index]);
								pinElements.push(pin.element);
							} else {
								appendChild(this.element, drag.originalElement);
							}
						}
						if (this.isHorizontal || this.isVertical) {
							// Set position if relevant
							let el = (this.pins ? this.pins.find((pin) => {
								return pin.drag === drag;
							}) : drag).element;

							if (this.isHorizontal) {
								el.style.left = values[1] + "%";
							}
							if (this.isVertical) {
								el.style.top = values[2] + "%";
							}
						}
					}
				});
				this.update();
			}
		}

		private removeUnlisted(arr: DraggableWidget[]) {
			let changed = false;

			if (this.pins) {
				this.pins.forEach((pin) => {
					if (!arr.includes(pin.drag)) {
						pin.unlinkPin();
						changed = true;
					}
				});
			} else if (!this.dragPins) {
				[].forEach.call(this.element.children, (child: HTMLElement) => {
					let drag = child.icWidget as DraggableWidget;

					if (drag && drag instanceof DraggableWidget && !arr.includes(drag)) {
						if (drag.isCloneable && child !== drag.originalElement) {
							drag.clones.remove(child as HTMLElement);
						}
						this.element.removeChild(child);
						this.draggables.remove(drag);
						changed = true;
					}
				});
			}
			return changed;
		}

		onReset(screen?: ScreenWidget) {
			if ((!screen || this.screenWidget === screen)) {
				let changed = false;

				this.startDraggables.forEach((drag, index) => {
					if (drag) {
						if (!drag.droppables.includes(this)) {
							if (this.pins) {
								this.pins[index].linkPin(drag, this, drag.pins[drag.startDroppables.indexOf(this)]);
							} else {
								let oldDrop = drag.droppableWidget;

								if (oldDrop) {
									drag.droppables.remove(oldDrop);
									oldDrop.draggables.remove(drag);
									if (oldDrop.isHorizontal || oldDrop.isVertical) {
										let style = drag.originalElement.style;

										style.left = style.top = "";
									}
									oldDrop.update();
								}
								drag.droppables.pushOnce(this);
								this.draggables.pushOnce(drag);
								appendChild(this.element, drag.originalElement);
							}
							changed = true;
						}
					} else if (this.pins) {
						this.pins[index].unlinkPin();
						changed = true;
					}
				});
				if (this.removeUnlisted(this.startDraggables) || changed) {
					this.update();
				}
				super.onReset(screen);
			}
		}

		onReveal(screen?: ScreenWidget) {
			if ((!screen || this.screenWidget === screen) && !this.hasState("reveal")) {
				let usedAnswers: DraggableWidget[] = this.activityWidget && this.activityWidget.allowDuplicates ? undefined : [];

				this.get(this.closestWidget, DroppableWidget, this.group, true).forEach(widget => {
					if (!widget.hasState("reveal")) {
						let mark = widget.markable,
							answer: DraggableWidget;

						if (!mark.answer && mark.points < 0) {
							widget.startDraggables.forEach(drag => {
								let oldDrop = drag.droppableWidget;

								//console.log("drop1", widget.element, drag.element)
								if ((!usedAnswers || !usedAnswers.includes(drag)) && oldDrop !== widget) {
									answer = drag; // Doesn't get used, just note us as not empty
									if (oldDrop) {
										drag.droppables.remove(oldDrop);
										oldDrop.draggables.remove(drag);
										oldDrop.update();
									}
									drag.droppables.pushOnce(widget);
									widget.draggables.pushOnce(drag);
									appendChild(widget.element, drag.originalElement);
									drag.invalidate();
								}
							});
						} else {
							let answers = widget.expandedAnswers,
								drags = widget.get(widget.screenWidget, DraggableWidget, widget.group).filter(drag => !drag.activityWidget || drag.activityWidget === this.activityWidget);

							answer = drags.not(usedAnswers).filter(function(widget) {
								let value = widget.markable.value;

								if (isString(value)) {
									return answers.some(answer => {return isString(answer) ? answer === value : answer.test(value as string)});
								}
								if (isArray(value)) {
									return value.some(val => answers.some(answer => {return isString(answer) ? answer === val : answer.test(val as string)}));
								}
							})[0];
							// console.log("drop2", widget && widget.element, answer && answer.element, answer && answer.droppableWidget && answer.droppableWidget.element, answers, drags)
							if (answer) {
								if (answer.isCloneable) {
									let clone = answer.originalElement.cloneNode(true) as HTMLElement;

									clone.icWidget = answer;
									answer.droppables.pushOnce(widget);
									widget.draggables.pushOnce(answer);
									appendChild(widget.element, clone);
								} else {
									let oldDrop = answer.droppableWidget;

									if (oldDrop !== widget) {
										if (oldDrop) {
											answer.droppables.remove(oldDrop);
											oldDrop.draggables.remove(answer);
											oldDrop.update();
										}
										answer.droppables.pushOnce(widget);
										widget.draggables.pushOnce(answer);
										if (widget.pins) {
											// TODO: multiple pins
											widget.pins.forEach(pin => {
												pin.unlinkPin();
												pin.update();
											});
											answer.pins[0].linkPin(answer, widget, widget.pins[0]);
											if (isString(answers[0]) && (widget.isHorizontal || widget.isVertical)) {
												let el = widget.pins[0].element,
													pos = (answers[0] as string).split(":");

												if (widget.isVertical) {
													el.style.top = pos.pop() + "%";
												}
												if (widget.isHorizontal) {
													el.style.left = pos.pop() + "%";
												}
											}
										} else {
											appendChild(widget.element, answer.originalElement);
											answer.invalidate();
										}
									}
								}
							}
							if (usedAnswers) {
								usedAnswers.pushOnce(answer);
							}
						}
						widget
							.toggleState({
								"empty": !answer,
								"disabled": true,
								"reveal": true
							})
							.update();
					}
				});
			}
		}

		includes(drag: DraggableWidget): boolean {
			return this.draggables.includes(drag);
		}

		onMouseEnter() {
			// Override so we don't get a plain hover effect from the mouse
		}

		onMouseLeave() {
			// Override so we don't get a plain hover effect from the mouse
		}

		onDragHover(drag: DraggableWidget, over: boolean, event: MouseEvent | TouchEvent) {
			if (!this.includes(drag) || this.isHorizontal || this.isVertical || this.isSortable) {
				this.toggleState("hover", over);
			}
			if (over && this.isSortable) {
				// Move all draggables except the current one to fill from the beginning
				let droppables = this.get(this.parentWidget, DroppableWidget, this.group, true).filter(droppable => droppable.isSortable),
					draggables = this.get(this.parentWidget, DraggableWidget, this.group).filter(draggable => draggable !== drag && draggable.droppableWidget.isSortable); // remove the active drag as we manually insert it

				droppables.filter((droppable) => {
					let draggable = droppable === this ? drag : draggables.shift();

					if (droppable.draggables[0] !== draggable) {
						let oldDrop = draggable.element.closest(DroppableWidget.selector).icWidget as DroppableWidget;

						draggable.droppables[0] = droppable;
						oldDrop.draggables[0] = draggable;
						appendChild(droppable.element, draggable.element);
						return true;
					}
				}).forEach(droppable => droppable.update());
			}
		}

		findPin(drag: DraggableWidget) {
			return this.pins.find((pin) => {return pin.target && pin.target.parent === drag;});
		}

		getPin(event?: MouseEvent | TouchEvent): PinWidget {
			let [clientX, clientY] = getCoords(event),
				available = this.pins.filter((pin) => {return !pin.target || (!this.preLoaded.includes(pin.drag) && !pin.drag.hasState("disabled"));}),
				underMouse = (event && available.filter(function(pin) {return pin.inRect(clientX, clientY) || (pin.target && pin.target.inRect(clientX, clientY));}));

			if (underMouse && underMouse.length) {
				available = underMouse;
			}
			let index = available.indexOf(this.lastPin) + 1;

			return this.lastPin = available.find((pin) => {return !pin.target;})
				|| available[index >= available.length ? 0 : index];
		}

		canAccept(drag: DraggableWidget) {
			// TODO: Check types
			return !this.hasState("disabled") && !this.pins === !drag.pins;
		}

		onDragDrop(drag: DraggableWidget, helper: DragHelper, event: MouseEvent | TouchEvent): boolean {
			if (this.includes(drag) && !this.isHorizontal && !this.isVertical) {
				return false; // Don't change anything
			}

			if (this.pins) { // only time when a drag can have multiple drops
				// Matching pairs - source pin to dest pin
				let pin = this.findPin(drag) || this.getPin();

				if (this.isHorizontal || this.isVertical) {
					let el = (helper as PinWidget).helperElement,
						style = pin.element.style,
						computedStyle = getComputedStyle(this.element, null),
						box = /^border-box$/i.test(computedStyle.boxSizing),
						rect = getBoundingClientRect(pin.element.parentElement),
						oldPos: ClientRect,
						width = rect.width - (box ? parseFloat(computedStyle.paddingLeft) + parseFloat(computedStyle.paddingRight) : 0),
						height = rect.height - (box ? parseFloat(computedStyle.paddingTop) + parseFloat(computedStyle.paddingBottom) : 0);

					// Force reflow then get screen position
					computedStyle = getComputedStyle(pin.element, null);
					style.position = "absolute";
					el.offsetHeight;
					oldPos = getBoundingClientRect(el);
					if (this.isHorizontal) {
						style.left = String(Math.range(0, (oldPos.left - rect.left - parseFloat(computedStyle.marginLeft)) * 100 / width, 100)) + "%";
					} else {
						style.left = "";
					}
					if (this.isVertical) {
						style.top = String(Math.range(0, (oldPos.top - rect.top - parseFloat(computedStyle.marginTop)) * 100 / height, 100)) + "%";
					} else {
						style.top = "";
					}
				}
				drag.getPin().unlinkPin().linkPin(drag, this);
				// TODO: reciprocal match?
			} else {
				if (this.isColour) {
					if (this.draggables[0] === drag) {
						this.draggables.pop();
					} else {
						this.draggables[0] = drag;
					}
				} else {
					let isCloneable = drag.isCloneable,
						el = isCloneable ? drag.cloneElement : drag.originalElement,
						oldPos: ClientRect = getBoundingClientRect(el),
						style = el.style,
						oldDrop = el.closest(DroppableWidget.selector).icWidget as DroppableWidget,
						oldDropIsOriginal = !isCloneable || drag.originalElement.parentElement === oldDrop.element,
						[clientX, clientY] = getCoords(event),
						children = this.get(this, DraggableWidget),
						swap = !this.isHorizontal && !this.isVertical
							? (this.count > 1
								? children.find(drag => drag.inRect(clientX, clientY))
								: children[0])
							: null;

					if (!isCloneable || !oldDropIsOriginal) {
						drag.droppables.remove(oldDrop);
						oldDrop.draggables.remove(drag);
					}
					drag.droppables.pushOnce(this);
					this.draggables.pushOnce(drag);
					style.top = style.left = "";
					// Physically move the element in the DOM
					if (swap) {
						let target: HTMLElement = swap.element;

						if (!isCloneable || (!oldDropIsOriginal && target !== swap.originalElement)) { // Swap if we're not cloneable or are both clones
							//console.log("Swap if we're not cloneable or are both clones")
							let nextSibling = el.nextSibling;

							this.element.insertBefore(el, target.nextSibling);
							oldDrop.element.insertBefore(target, nextSibling);
							// TODO: animate movement
							target.style.top = target.style.left = "";
							this.draggables.remove(swap);
							swap.droppables.remove(this);
							swap.droppables.pushOnce(oldDrop);
							oldDrop.draggables.pushOnce(swap);
						} else if (oldDropIsOriginal && target !== swap.originalElement) { // If we are an original dropping on a clone then delete them
							//console.log("If we are an original dropping on a clone then delete them")
							this.element.insertBefore(el, target);
							target.parentElement.removeChild(target);
							this.draggables.remove(swap);
							swap.droppables.remove(this);
						} else if (!oldDropIsOriginal && target === swap.originalElement) { // If we are a clone dropping on an original then delete us
							//console.log("If we are a clone dropping on an original then delete us")
							this.draggables.remove(drag);
							drag.droppables.remove(this);
							el.parentElement.removeChild(el);
							el = null;
						}
					} else {
						appendChild(this.element, el);
					}
					if (isCloneable && this.group) {
						this.get(this.parentWidget, DroppableWidget, this.group, false).forEach((droppable) => {
							if (droppable.includes(drag)) {
								[].forEach.call(droppable.element.children, function(el: HTMLElement) {
									if (el.icWidget === drag) {
										el.parentElement.removeChild(el);
									}
								});
								drag.droppables.remove(droppable);
								droppable.draggables.remove(drag);
								droppable.update();
							}
						});
					}
					if (el) {
						// Force reflow then get screen position
						el.offsetHeight;
						let newPos = getBoundingClientRect(el);

						if (this.isHorizontal || this.isVertical) {
							let percent: number,
								computedStyle = getComputedStyle(this.element, null),
								box = /^border-box$/i.test(computedStyle.boxSizing),
								rect = getBoundingClientRect(this),
								width = rect.width - (box ? parseFloat(computedStyle.paddingLeft) + parseFloat(computedStyle.paddingRight) : 0),
								height = rect.height - (box ? parseFloat(computedStyle.paddingTop) + parseFloat(computedStyle.paddingBottom) : 0);

							if (this.isHorizontal) {
								let left = Math.max(newPos.left, oldPos.left) - Math.min(newPos.left, oldPos.left);

								percent = Math.range(0, left * 100 / (width - newPos.width), 100);
								style.left = String(percent * (width - newPos.width) / width) + "%";
							} else {
								style.left = "";
							}
							if (this.isVertical) {
								let top = Math.max(newPos.top, oldPos.top) - Math.min(newPos.top, oldPos.top);

								percent = Math.range(0, top * 100 / (height - newPos.height), 100);
								style.top = String(percent * (height - newPos.height) / height) + "%";
							} else {
								style.top = "";
							}
						} else {
							style.top = style.left = "";
						}
						// console.log(oldPos.top, newPos.top, top, " : ", oldPos.left, newPos.left, left)
						// TODO: animate movement
						//style.top = Math.max(newPos.top, oldPos.top + top) - Math.min(newPos.top, oldPos.top + top) + "px";
						//style.left = Math.max(newPos.left, oldPos.left + left) - Math.min(newPos.left, oldPos.left + left) + "px";
					}
					oldDrop.update();
				}
			}
			if (event && this.parentWidget) {
				// TODO: only attempt on answer, or on any interraction?s
				this.parentWidget.addState("attempted");
			}
			this.update();
			this.callUserFunc(this.icOnDropFn, this.markable.value);
			return true;
		}

		onDragRemove(drag: DraggableWidget) {
			this.draggables.remove(drag);
			if (this.pins) {
				let pin = this.pins.find(pin => pin.drag === drag);

				if (pin) {
					pin.unlinkPin();
				}
			}
			this.update();
		}

		onDragEnd(drag: DraggableWidget, event: MouseEvent | TouchEvent, drop: DroppableWidget) {
			this.toggleState({
				"hover": false,
				"empty": !this.draggables.length
			});
			if (this.isSortable && !drop) {
				// put them back where they came from
				let draggables: DraggableWidget[] = [],
					droppables = this.get(this.parentWidget, DroppableWidget, this.group),
					index: number = 0,
					foundZero: boolean;

				droppables.forEach(function(droppable, index) {
					if (droppable.isSortable) {
						let drags = droppable.draggables;

						if (!drags.length) {
							foundZero = true;
						} else if (drags.length === 2 && foundZero) {
							drags.reverse();
						}
						draggables.pushOnce.apply(draggables, drags);
					}
				});
				droppables.forEach((droppable) => {
					if (droppable.isSortable) {
						let draggable = draggables[index++];

						if (!droppable.draggables.includes(draggable)) {
							let oldDrop = draggable.element.closest(DroppableWidget.selector).icWidget as DroppableWidget;

							oldDrop.draggables.remove(draggable);
							droppable.draggables.pushOnce(draggable);
							appendChild(droppable.element, draggable.element);
							droppable.update();
						}
					}
				});
			}
		}

		update() {
			let mark = this.markable;

			if (this.draggables.length) {
				let computedStyle: CSSStyleDeclaration,
					width: number,
					height: number;

				if (this.isHorizontal || this.isVertical) {
					let el = this.element,
						style = computedStyle = getComputedStyle(el, null),
						borderBox = /^border-box$/i.test(style.boxSizing);

					width = el.clientWidth - (borderBox ? parseFloat(style.paddingLeft) + parseFloat(style.paddingRight) : 0);
					height = el.clientHeight - (borderBox ? parseFloat(style.paddingTop) + parseFloat(style.paddingBottom) : 0);
				}
				mark.value = undefined;
				this.draggables.forEach((drag) => {
					if (!drag.hasState("disabled")) {
						let value: string = "",
							el = this.pins ? (this.findPin(drag) || {} as PinWidget).element : drag.element,
							rect = !this.pins && computedStyle ? getBoundingClientRect(el) : null;

						if (!mark.value) {
							mark.value = [];
						}
						(mark.value as string[]).push(drag.markable.value as string);
						if (el && (this.isHorizontal || this.isVertical)) {
							if (this.isHorizontal) {
								value += (value ? ":" : "") + (this.pins ? parseFloat(el.style.left) : String((parseFloat(el.style.left) || 0) * width / (width - rect.width)));
							}
							if (this.isVertical) {
								value += (value ? ":" : "") + (this.pins ? parseFloat(el.style.top) : String((parseFloat(el.style.top) || 0) * height / (height - rect.height)));
							}
							(mark.value as string[]).push(drag.markable.value as string + ":" + value, value);
						}
					}
				});
				if (this.isColour) {
					this.index = this.draggables.first().index;
				}
				this.removeState("empty");
			} else {
				mark.value = undefined;
				this.index = -1;
				this.addState("empty");
			}
			this.mark();
		}
	}
};
