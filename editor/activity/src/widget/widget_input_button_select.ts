///<reference path="widget_input_button.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	let selector = "ic-select";

	/**
	 * Version of SelectWidget
	 */
	version[selector] = 1;

	export class SelectWidget extends InputButtonWidget implements OnFrame, OnState, OnReset, OnReveal {
		static selector = selector;

		private posTop: number;
		private posLeft: number;

		/**
		 * Tree Widgets help define the WidgetNode tree, if it's not a tree then
		 * it's a leaf.
		 */
		static isTreeWidget = true;

		public startValue: string;

		private optionsElement: HTMLElement;

		constructor(element: HTMLElement) {
			super(element);

			this.optionsElement = querySelector(element, ":scope>ic-options");
			if (!this.optionsElement) {
				console.error("No <ic-options> element found!", element);
			}
			this.on(["click", ".state", ".persist", ".reset", ".reveal"]);
			document.addEventListener("mouseup", this.closePopup);
			document.addEventListener("touchend", this.closePopup);
		}

		startup() {
			super.startup();
			let options = this.get(this, OptionWidget),
				mark = this.markable,
				value = mark.value as string,
				index = value && value.regex(/#option([0-9]+)#/) as number;

			this.startValue = value || "";
			if (value) {
				this.get(this, OptionWidget, false).forEach(function(widget) {
					widget.toggleState("checked", widget.markable.value === value);
				});
			} else {
				this.addState("empty");
			}
			options.forEach((widget) => {
				mark.max = Math.max(mark.max || 0, widget.markable.max || 1);
				if (value) {
					widget.toggleState("checked", widget.index === index);
				}
			});
			if (mark.points === undefined) {
				mark.points = mark.max || 1;
			}
		}

		get hasAnswered(): boolean {
			let mark = this.markable;

			return mark.points >= 0 && !!mark.value;
		}

		onPersist(state?: number): number {
			let optionWidgets = this.get(this, OptionWidget);

			if (isUndefined(state)) {
				return optionWidgets.findIndex(function(widget) {
					return widget.hasState("checked");
				}) + 1;
			}
			optionWidgets.forEach(function(widget, index) {
				widget.toggleState("checked", index + 1 === state);
			});
			if (!state) {
				this.addState("empty");
			}
		}

		onClick(event: MouseEvent | TouchEvent) {
			if (!this.isDisabled(event)) {
				event.preventDefault();
				this.toggleState("open");
			}
		}

		onFrame() {
			let box = this.element.getBoundingClientRect();

			if (this.posTop !== box.top || this.posLeft !== box.left) {
				this.removeState("open");
			}
		}

		onReset(screen: ScreenWidget) {
			if (!screen || this.screenWidget === screen) {
				let mark = this.markable;

				mark.value = this.startValue;
				if (mark.value) {
					this.get(this, OptionWidget, false).forEach(function(widget) {
						widget.toggleState({
							"checked": widget.markable.value === mark.value,
							"click": false,
							"hover": false
						});
					});
				} else {
					Array.from(this.element.childNodes).not(this.optionsElement).forEach(function(el: HTMLElement) {
						el.parentElement.removeChild(el);
					});
				}
				this.toggleState({
					"open": false,
					"disabled": false,
					"empty": !mark.value
				});
			}
		}

		onReveal(screen: ScreenWidget) {
			if ((!screen || this.screenWidget === screen) && !this.hasState("reveal")) {
				this.get(this.closestWidget, SelectWidget, this.group, true).forEach(widget => {
					if (!widget.hasState("reveal")) {
						let element = widget.element,
							answers = widget.expandedAnswers,
							answer = widget.get(widget, OptionWidget).filter(function(option) {
								let value = option.markable.value;

								option.removeState("checked");
								return isString(value)
									? answers.includes(value)
									: isArray(value)
										? value.some(val => answers.includes(val))
										: false;
							})[0];

						Array.from(element.childNodes).not(widget.optionsElement).forEach(function(el) {
							element.removeChild(el);
						});
						if (answer) {
							answer.addState("checked");
						}
						widget
							.toggleState({
								"empty": !answer,
								"disabled": true,
								"reveal": true
							})
							.mark();
					}
				});
			}
		}

		closePopup = (event: MouseEvent | TouchEvent) => {
			if (this.hasState("open")) {
				for (let el = event.target as HTMLElement; el; el = el.parentElement) {
					if (el === this.element) {
						return;
					}
				}
				this.removeState("open");
			}
		}

		onState(widget: Widget, stateList: WidgetState[]) {
			if (this === widget && stateList.includes("open")) {
				if (this.hasState("open")) {
					// Make sure our options popup in the right place
					let box = this.element.getBoundingClientRect(),
						scrollBox = (closestScroll(this.element) || document.body).getBoundingClientRect(),
						optionsBox = this.optionsElement.getBoundingClientRect(),
						style = this.optionsElement.style;

					if (optionsBox.height + box.bottom >= scrollBox.bottom) {
						if (box.top - optionsBox.height >= scrollBox.top) {
							this.addState("alt");
							style.top = box.top - optionsBox.height + "px";
						} else {
							style.top = scrollBox.bottom - optionsBox.height + "px";
						}
					} else {
						style.top = box.bottom + "px";
					}
					style.left = Math.min(box.left, scrollBox.right - optionsBox.width) + "px";
					style.minWidth = box.width + "px";
					this.posTop = box.top;
					this.posLeft = box.left;
					this.on(".frame");
				} else {
					this
						.removeState("alt")
						.off(".frame");
				}
			} else if (widget instanceof OptionWidget
				&& widget.parentWidget === this
				&& stateList.includes("checked")
				&& widget.hasState("checked")) {
				// Replace our display elements with the correct ones
				let element = this.element,
					clone = widget.element.cloneNode(true),
					thisMark = this.markable,
					widgetMark = widget.markable;

				this.removeState("empty");
				Array.from(element.childNodes).not(this.optionsElement).forEach(function(el) {
					element.removeChild(el);
				});
				Array.from(clone.childNodes).forEach(function(el) {
					element.appendChild(el);
				});

				thisMark.points = widgetMark.points || 1;
				thisMark.value = widgetMark.value;
				this.mark();
			}
		}
	}
};
