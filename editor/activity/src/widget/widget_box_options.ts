///<reference path="widget.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	let selector = "ic-options";

	/**
	 * Version of OptionsWidget
	 */
	version[selector] = 1;

	export class OptionsWidget extends BoxWidget {
		static selector = selector;
	}
};
