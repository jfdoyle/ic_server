///<reference path="widget.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	let selector = "ic-box";

	/**
	 * Version of BoxWidget
	 */
	version[selector] = 1;

	export class BoxWidget extends Widget implements OnState {
		static selector = selector;

		public isStory: boolean;
		public isStoryStart: boolean;
		public storySpeed: number = 1000;
		public nextStory: BoxWidget;

		constructor(element: HTMLElement) {
			super(element);

			if (element.hasAttribute("ic-story")) {
				this.isStory = true;
				this.isStoryStart = element.getAttribute("ic-story") === "start";
				if (!this.isStoryStart) {
					this.addState("disabled");
				}
				(getComputedStyle(element.firstElementChild).transition).replace(/[^\d]+(\d+)(s|ms)(?:[^\d]+(\d+)(s|ms))?/g, ($0, duration, durationUnit, delay, delayUnit) => {
					if (durationUnit === "s") {
						duration *= 1000;
					}
					if (delayUnit === "s") {
						delay *= 1000;
					}
					this.storySpeed = Math.max(this.storySpeed, duration, delay);
					return $0;
				});
				this.on([".state"]);
			}
		}

		startup() {
			super.startup();

			if (this.isStory) {
				let foundThis = false,
					screenElement = this.screenWidget.element,
					thisElement = this.element;

				this.nextStory = this.get(this.screenWidget, BoxWidget).find((widget) => {
					if (widget === this) {
						foundThis = true;
					} else if (foundThis) {
						return widget.isStory && !widget.element.parentElements(screenElement).includes(thisElement);
					}
				});
				//console.log("story", this, this.nextStory)
				this.disappear();
				if (this.isStoryStart) {
					this.appear();
				}
			}
		}

		disappear = () => {
			let el = this.element.firstElementChild;

			while (el) {
				el.classList.add("hidden");
				el = el.nextElementSibling;
			}
		}

		appear = () => {
			let el = this.element.firstElementChild,
				appear = () => {
					if (el) {
						el.classList.remove("hidden");
						el = el.nextElementSibling;
						setTimeout(appear, this.storySpeed);
					}
				};

			//console.log("appear", this)
			appear()
		}

		onState(widget: Widget, stateList: WidgetState[]) {
			if (widget === this && stateList.includes("disabled")) {
				if (this.hasState("disabled")) {
					this.disappear();
					if (this.nextStory) {
						setTimeout(() => {this.nextStory.removeState("disabled")}, this.storySpeed);
					}
				} else {
					this.appear();
				}
			}
		}
	}
};
