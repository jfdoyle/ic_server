///<reference path="widget_button.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	export class LeftButtonWidget extends ButtonWidget implements OnScroll {
		static selector = "ic-button[ic-button=left]";

		nav: HTMLElement;
		tick: number;
		start: number;
		delta: number;

		constructor(element: HTMLElement) {
			super(element);
			let el = element.parentElement.firstElementChild;

			for (; el; el = el.nextElementSibling) {
				if (el.tagName === "IC-NAV") {
					break;
				}
			}
			if (el) {
				this.nav = el as HTMLElement;
				this.on(".scroll");
			} else {
				this.addState("disabled");
			}
		}

		startup() {
			super.startup();

		}

		scroll = () => {
			if (this.hasState("click")) {
				this.delta = Math.min(4, this.delta + 0.01);
				this.nav.scrollLeft = this.start + Math.floor(this.delta * this.tick++);
				Widget.trigger(".scroll", this.nav.icWidget);
				rAF("left", this.scroll);
			}
		}

		onMouseDown(event: MouseEvent | TouchEvent) {
			super.onMouseDown(event);
			this.tick = 0;
			this.start = this.nav.scrollLeft;
			this.delta = 1;
			rAF("left", this.scroll);
		}

		onScroll(widget: Widget) {
			if (widget === this.nav.icWidget) {
				this.toggleState("disabled", this.nav.scrollLeft + this.nav.clientWidth >= this.nav.scrollWidth);
			}
		}
	}
};
