///<reference path="widget.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	let selector = "ic-timer";

	/**
	 * Version of TimerWidget
	 */
	version[selector] = 1;

	export interface JsonData {
	}

	export class TimerWidget extends Widget implements OnPersist, OnState, OnTick {
		static selector = selector;

		public start: number;
		public format: string;

		public minutes: number;
		public used: number;
		public warning: number;

		constructor(element: HTMLElement) {
			super(element);

			this.format = element.getAttribute(this.selector) || "H:i:s";
			if (this.rootWidget.get(ActivityWidget).filter(function(activity) {return activity.hasState("active");}).length) {
				this.start = new Date().getTime();
			} else {
				this.on(".state");
			}
			this.minutes = (parseFloat(this.data.minutes as any) || 0) * 60000;
			this.warning = (parseFloat(this.data.warning as any) || 0) * 60000;
			this.onTick();
			this.on([".persist", inEditor ? null : ".tick"]);
		}

		startup() {
			super.startup();
			this.rootWidget.get(TimerWidget).some((timer, index) => {
				if (this === timer) {
					this._i = index;
					return true;
				}
			});
		}

		onPersist(state?: any): any {
			if (isUndefined(state)) {
				return this.start ? Math.range(0, new Date().getTime() - this.start, this.minutes) : this.used; // TODO: What if no limits?
			}
			if (this.rootWidget.get(ActivityWidget).find(function(activity) {return activity.hasState("active");})) {
				this.start = new Date().getTime() - state;
			} else {
				this.used = state;
				this.on(".state");
			}
		}

		onTick() {
			let elapsed = this.start ? new Date().getTime() - this.start : (this.used || 0),
				max = this.minutes;

			if (max) {
				if (this.start) {
					elapsed = Math.max(0, max - elapsed);
					if (this.warning >= elapsed) {
						this.addState("warning");
					}
					if (!elapsed && !this.hasState("disabled")) {
						this.addState("disabled");
						Widget.trigger(".timeout");
						let event = document.createEvent("Event");

						event.initEvent("ic-timeout", false, false);
						document.body.dispatchEvent(event);
					}
				} else {
					elapsed = max - (this.used || 0);
				}
			}
			this.element.textContent = new Date(Math.max(0, elapsed)).format(this.format);
		}

		onState(widget: Widget, stateList: WidgetState[]) {
			if (widget === this && stateList.includes("disabled")) {
				if (this.hasState("disabled")) {
					// turn off clock
					if (this.start) {
						this.used = new Date().getTime() - this.start;
					}
					this.start = 0;
				} else {
					// turn on clock
					this.start = new Date().getTime() - (this.used || 0);
				}
			} else if (!this.start && widget instanceof ActivityWidget && !this.hasState("disabled") && widget.hasState("active")) {
				// turn on clock when first activity goes active
				this.start = new Date().getTime() - (this.used || 0);
			}
		}
	}
};
