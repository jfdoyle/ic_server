///<reference path="widget_input.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	/**
	 * Version of InputButtonWidget
	 */
	version["button"] = 1;

	let fakeMouseEnter: Widget[],
		touchTimer: number,
		startX: number,
		startY: number,
		startEvent: TouchEvent,
		startWidget: InputButtonWidget;


	export abstract class InputButtonWidget extends InputWidget implements AllMouseMove, AllMouseUp, OnClick, OnMouseDown, OnMouseUp {
		static pressed: InputButtonWidget;

		private icOnClickFn: string;

		public isStory: boolean;

		constructor(element: HTMLElement) {
			super(element);

			this.markable.max = 0;

			if (element.getAttribute("icOnClick")) {
				this.icOnClickFn = element.getAttribute("icOnClick");
			}
			if (element.hasAttribute("ic-story")) {
				this.isStory = true;
			}
			this.on(["click", "mousedown", "touchstart", "mouseup", "touchend"]);
		}

		private _startTimer(event: TouchEvent) {
			startWidget = this;
			startEvent = event;
			[startX, startY] = getCoords(event, false);
			touchTimer = setTimeout(this._finishTimer, 500) as any;
		}

		private _checkTimer(event: TouchEvent) {
			let [clientX, clientY] = getCoords(event, false);

			if (Math.abs(clientX - startX) > 5 || Math.abs(clientY - startY) > 5) {
				clearTimeout(touchTimer);
				touchTimer = undefined;
				this.off("!mousemove", "!touchmove", "!mouseup", "!touchend");
			}
		}

		private _finishTimer = () => {
			clearTimeout(touchTimer);
			touchTimer = undefined;
			InputButtonWidget.pressed = startWidget;
		}

		onMouseDown(event: MouseEvent | TouchEvent) {
			if (!this.isDisabled(event) && !InputButtonWidget.pressed) {
				if (event instanceof MouseEvent || !this.isScrollable) {
					InputButtonWidget.pressed = this;
				} else {
					this._startTimer(event);
				}
				fakeMouseEnter = null;
				this.activate()
					.toggleState(["click", "focus"])
					.on(["!mousemove", "!touchmove", "!mouseup", "!touchend"]);
			}
		}

		onClick(event: MouseEvent | TouchEvent) {
			// Overload as needed
		}

		onMouseUp(event: MouseEvent | TouchEvent) {
			if (InputButtonWidget.pressed === this) {
				InputButtonWidget.pressed = undefined;
				this.removeState("click");
				this.callUserFunc(this.icOnClickFn);
				if (this.isStory) {
					let box = this.element,
						prev: BoxWidget,
						next: BoxWidget;

					while (box && !next) {
						box = box.parentElement.closest("ic-box[ic-story]") as HTMLElement;
						if (box) {
							prev = box.icWidget as BoxWidget;
							next = prev.nextStory;
						}
					}
					if (next) {
						prev.addState("disabled");
					}
				}
			}
		}

		onMouseEnter(event: MouseEvent | TouchEvent) {
			if (InputButtonWidget.pressed === this) {
				this.addState(["click", "hover"]);
			} else if (!InputButtonWidget.pressed) {
				super.onMouseEnter(event);
			}
		}

		onMouseLeave(event: MouseEvent | TouchEvent) {
			if (InputButtonWidget.pressed === this) {
				this.removeState(["click", "hover"]);
			} else if (!InputButtonWidget.pressed) {
				super.onMouseLeave(event);
			}
		}

		allMouseMove(event: MouseEvent | TouchEvent) {
			if (InputButtonWidget.pressed) {
				event.preventDefault();
				if ((event as TouchEvent).changedTouches) {
					let pos = (event as TouchEvent).changedTouches[0],
						element = document.elementFromPoint(pos.clientX, pos.clientY),
						widgetElement = element && element.closestWidget(),
						widget = widgetElement && widgetElement.icWidget,
						widgets = widget && widget.parentWidgets;

					if (widgets) {
						let common: Widget[];

						if (fakeMouseEnter) {
							common = widgets.intersect(fakeMouseEnter);
							fakeMouseEnter.not(common).forEach((widget) => {
								if (widget.events["mouseleave"]) {
									widget.onMouseLeave(event);
								}
							});
						}
						widgets.not(common || []).forEach((widget) => {
							if (widget.events["mouseenter"]) {
								widget.onMouseEnter(event);
							}
						});
						fakeMouseEnter = widgets;
					}
				}
			} else {
				this._checkTimer(event as TouchEvent);
			}
		}

		allMouseUp(event: MouseEvent | TouchEvent) {
			this.off("!mousemove", "!touchmove", "!mouseup", "!touchend");
			if (touchTimer) {
				this._finishTimer();
				this.onMouseUp(event);
			}
			if (fakeMouseEnter) {
				fakeMouseEnter.forEach((widget) => {
					if (widget.events["mouseleave"]) {
						widget.onMouseLeave(event);
					}
				});
			}
			InputButtonWidget.pressed = fakeMouseEnter = null;
		}
	}
};
