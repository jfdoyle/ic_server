///<reference path="widget.ts" />
///<reference path="widget_anchor.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface HTMLSpanElement {
	icAnchor: ic.AnchorWidget;
}

namespace ic {
	let selector = "ic-nav";

	/**
	 * Version of NavWidget
	 */
	version[selector] = 1;

	export class NavWidget extends Widget implements OnClick, OnResize, OnState {
		static selector = selector;

		anchors: HTMLSpanElement[] = [];

		startup() {
			super.startup();

			this.get(AnchorWidget).forEach((anchor: AnchorWidget) => {
				let span = this.anchors[anchor.index] = createElement("span");

				span.setAttribute(AnchorWidget.selector, anchor.element.getAttribute(AnchorWidget.selector) || String(anchor.index));
				span.icAnchor = anchor;
				appendChild(this.element, span);
				this.onState(anchor, []);
			});
			this.on(["click", ".resize", ".state"]);
			setImmediate(NavWidget.update);
		}

		onClick(event: MouseEvent | TouchEvent) {
			let target = event.target as HTMLSpanElement;

			if (!this.isDisabled(event) && target.icAnchor) {
				target.icAnchor.activate();
			}
		}

		onState(widget: Widget, stateList: WidgetState[]) {
			if (widget instanceof AnchorWidget && stateList.includes("active")) {
				setImmediate(NavWidget.update);
			}
		}

		onResize() {
			setImmediate(NavWidget.update);
		}

		static update = () => { // call once for all anchors
			Widget.root.get(NavWidget).forEach((nav) => {
				nav.anchors.forEach((span) => {
					let anchor = span.icAnchor;

					Object.keys(anchor.state).forEach(function(state) {
						if (anchor.state[state]) {
							span.classList.add(state);
							if (state === "active") {
								let navRect = getBoundingClientRect(nav),
									rect = getBoundingClientRect(span);

								if (rect.width) {
									nav.element.scrollLeft = Math.floor(nav.element.scrollLeft + rect.left - navRect.left - (navRect.width - rect.width) / 2);
								}
							}
						} else {
							span.classList.remove(state);
						}
					});
				});
				Widget.trigger(".scroll", nav);
			})
		};
	}
};
