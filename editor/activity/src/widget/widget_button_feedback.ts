///<reference path="widget_button.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	export class FeedbackButtonWidget extends ButtonWidget {
		static selector = "ic-button[ic-button=feedback]";

		onClick(event: MouseEvent | TouchEvent) {
			if (!this.isDisabled(event)) {
				let feedback = this.get(this.screenWidget, FeedbackWidget).first() || this.get(FeedbackWidget).first();

				if (feedback) {
					feedback.toggleFeedback();;
				}
			}
			super.onClick(event);
		}
	}
};
