///<reference path="widget_button.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	export class MarkButtonWidget extends ButtonWidget {
		static selector = "ic-button[ic-button=mark]";

		onClick(event: MouseEvent | TouchEvent) {
			if (!this.isDisabled(event)) {
				let root = this.screenWidget || this.rootWidget;

				if (root) {
					root.toggleState("marked");
				}
			}
			super.onClick(event);
		}
	}
};
