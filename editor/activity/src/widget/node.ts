///<reference path="../core.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

interface Element {
	icWidget: ic.Widget;
}

namespace ic {
	/**
	 * Version of WidgetNode
	 */
	version["node"] = 1;

	/**
	 * Used for optimising the access to widget.get()
	 */
	let isHas: boolean;

	/**
	 * A selector of elements that can have indexed widgets inside them.
	 * Cached on first access.
	 */
	let indexSelector: string;

	/**
	 * Useful shortcut type for Widgets
	 */
	export type WidgetConstructor<T> = {
		new(...args: any[]): T;
		selector?: string;
	};

	/**
	 * Cache for getOrHas. Is cleared if any widget is moved or removed from
	 * within an IC-SCREEN.
	 */
	let cache: WeakMap<Element, Map<WidgetConstructor<Widget> | WidgetConstructor<Widget>[], Widget[]>> = new WeakMap();

	/**
	 * To enable caching for more than a single index.
	 * This is an array of index counts, containing an array of arrays of
	 * WidgetContructors.
	 */
	let cacheIndex: WidgetConstructor<Widget>[][][] = [];

	/**
	 * Used to create the widget tree:
	 * true: anything that goes in the full tree
	 * false: default (non-tree widgets)
	 */
	interface IsInputWidget {
		isInputWidget?: boolean;
	}

	/**
	 * Used to create the widget tree:
	 * true: anything that can have childWidgets
	 * false: default (non-tree widgets)
	 */
	interface IsTreeWidget {
		isTreeWidget?: boolean;
	}

	/**
	 * Check that a node is a Widget
	 */
	function filterWidget(node: HTMLElement) {
		return node.icWidget ? NodeFilter.FILTER_ACCEPT : NodeFilter.FILTER_SKIP;
	}
	(filterWidget as any).acceptNode = filterWidget;

	/**
	 * Check that a node is an InputWidget
	 */
	function filterInputWidget(node: HTMLElement) {
		return node.icWidget && (node.icWidget as IsInputWidget).isInputWidget ? NodeFilter.FILTER_ACCEPT : NodeFilter.FILTER_SKIP;
	}
	(filterInputWidget as any).acceptNode = filterInputWidget;

	/**
	 * The <strong><code>WidgetNode</code></strong> class contains the root code
	 * for navigating the widgets.
	 */
	export abstract class WidgetNode extends WidgetEvent {
		protected _i: number;
		protected _index: number;

		private _rootWidget: ScreensWidget;
		/**
		 * Get the root ScreensWidget ancestor
		 */
		get rootWidget(): ScreensWidget {
			let widget = this._rootWidget;

			if (!widget) {
				let element = this.element.closest(ScreensWidget.selector) as HTMLElement;

				if (element) {
					this._rootWidget = widget = element.icWidget as ScreensWidget;
				}
			}
			return widget;
		};

		private _screenWidget: ScreenWidget;
		/**
		 * Get the closest ScreenWidget ancestor
		 */
		get screenWidget(): ScreenWidget {
			let widget = this._screenWidget;

			if (!widget) {
				let element = this.element.closest(ScreenWidget.selector) as HTMLElement;

				if (element) {
					this._screenWidget = widget = element.icWidget as ScreenWidget;
				}
			}
			return widget;
		};

		private _activitiesWidget: ActivitiesWidget;
		/**
		 * Get the closest ActivitiesWidget ancestor
		 */
		get activitiesWidget(): ActivitiesWidget {
			let widget = this._activitiesWidget;

			if (widget === undefined) {
				let element = this.element.closest(ActivitiesWidget.selector) as HTMLElement;

				this._activitiesWidget = widget = element ? element.icWidget as ActivitiesWidget : null;
			}
			return widget;
		};

		private _activityWidget: ActivityWidget;
		/**
		 * Get the closest ActivityWidget ancestor
		 */
		get activityWidget(): ActivityWidget {
			let widget = this._activityWidget;

			if (widget === undefined) {
				let element = this.element.closest(ActivityWidget.selector) as HTMLElement;

				this._activityWidget = widget = element ? element.icWidget as ActivityWidget : null;
			}
			return widget;
		}

		private _parentWidget: Widget;
		/**
		 * Get the closest TreeWidget ancestor
		 */
		get parentWidget(): Widget {
			let widget = this._parentWidget;

			if (widget === undefined) {
				let element = this.element.parentElement;

				while (element && (!element.icWidget || !(element.icWidget.constructor as IsTreeWidget).isTreeWidget)) {
					element = element.parentElement;
				}
				this._parentWidget = widget = element ? element.icWidget as Widget : null;
			}
			return widget;
		}

		private _closestWidget: ActivityWidget | ActivitiesWidget | ScreenWidget | ScreensWidget;
		/**
		 * Get the closest "important" widget ancestor.
		 */
		get closestWidget(): ActivityWidget | ActivitiesWidget | ScreenWidget | ScreensWidget {
			let widget = this._closestWidget;

			if (widget === undefined) {
				this._closestWidget = widget = this.activityWidget || this.activitiesWidget || this.screenWidget || this.rootWidget;
			}
			return widget;
		}

		invalidate() {
			delete this._parentWidget;
			delete this._closestWidget;
			delete this._activityWidget;
			delete this._activitiesWidget;
		}

		constructor(public element: HTMLElement) {
			super();

			let widgetType = this.constructor;

			element.icWidget = this as any; // this is an abstract class, so must be a Widget or greater
			if (this instanceof ScreenWidget) {
				new MutationObserver((mutations) => {
					mutations.forEach((mutation) => {
						Array.from(mutation.addedNodes).forEach((node: Element) => {
							if (node instanceof HTMLElement) {
								this.get(node, true).forEach(function(widget) {
									widget && widget.invalidate();
								});
							}
						});
					});
				}).observe(element, {childList: true, subtree: true});
			} else if (this instanceof ScreensWidget) {
				new MutationObserver(() => {
					// Clear the cache here, we don't care about outside stuff which might include other libraries
					cache = new WeakMap();
				}).observe(element, {childList: true, subtree: true});
			}
			if ((widgetType as IsInputWidget).isInputWidget) {
				if (!indexSelector) {
					indexSelector = [
						ActivityWidget.selector,
						ActivitiesWidget.selector,
						ScreenWidget.selector,
						ScreensWidget.selector,
						"body"
					].join(",");
				}
				let indexParent = element.parentElement.closest(indexSelector),
					indexCount = 0;

				if (!querySelectorAll(indexParent, element.tagName).some((el: HTMLElement) => {
					if (el.parentElement.closest(indexSelector) === indexParent) {
						indexCount++;
						if (el === element) {
							this._index = this._i = indexCount;
							return true;
						}
					}
				})) {
					console.error("Unable to find own element when indexing", element);
					this._index = this._i = 1;
				}
			}
		}

		public set index(index: number) {
			this._index = index;
		}

		public get index(): number {
			return this._index;
		}

		public get realIndex(): number {
			return this._i;
		}

		public get parentWidgets(): Widget[] {
			let nodes: Widget[] = [],
				node: WidgetNode = this;

			for (; node; node = node.parentWidget) {
				nodes.push(node as Widget);
			}
			return nodes;
		}

		/**
		 * Finding other widgets within the same activity, optionally by type and group
		 */
		has<T extends Widget>(...args: (Widget | Element | WidgetConstructor<T> | number | WidgetState | boolean)[]): boolean {
			isHas = true;
			return this.getOrHas.apply(this, arguments) as boolean;
		}

		/**
		 * Getting other widgets within the same activity, optionally by type and group
		 */
		get<T extends Widget>(...args: (Widget | Element | WidgetConstructor<T> | number | WidgetState | boolean)[]): T[] {
			isHas = false;
			return this.getOrHas.apply(this, arguments) as T[];
		}

		/**
		 * Find or Get other widgets within the same activity, optionally by type and group
		 */
		private getOrHas<T extends Widget>(...args: (Widget | Element | WidgetConstructor<T> | number | WidgetState | boolean)[]): T[] | boolean {
			let i: number = 0,
				arg: any,
				subClass: WidgetConstructor<T>[] = [],
				group: number,
				state: WidgetState[] = [],
				andSelf: boolean = true,
				element: Element,
				widgets: T[],
				pushElement: Element,
				filterNode = (node: T) => {
					return ((group === undefined || node.group === group)
						&& (!state.length || node.hasState(state))
						&& (andSelf || node !== this as WidgetNode));
				},
				returnWidgets = () => {
					if (isHas) {
						return !!widgets.find(filterNode);
					}
					return widgets.filter(filterNode);
				};

			for (; i < args.length; i++) {
				arg = args[i];
				switch (typeof arg) {
					case "object":
						if (arg) {
							if (arg instanceof Element) {
								element = arg as Element;
							} else if (arg instanceof WidgetNode) {
								element = arg.element;
								if (arg === this as WidgetNode) {
									// By default looking for our children won't include us
									andSelf = false;
								}
							}
						}
						break;

					case "function":
						subClass.push(arg as WidgetConstructor<T>);
						break;

					case "number":
						group = arg as number;
						break;

					case "string":
						state.push(arg as WidgetState);
						break;

					case "boolean":
						andSelf = arg as boolean;
						break;
				}
			}
			if (!element) {
				element = (this.rootWidget || (this as any) as Widget).element;
			}

			let subClassLength = subClass.length,
				index: WidgetConstructor<Widget> | WidgetConstructor<Widget>[];

			if (subClassLength === 1) {
				index = subClass[0];
			} else if (subClassLength > 1) {
				let cacheItems = cacheIndex[subClassLength];

				index = subClass;
				if (!cacheItems) {
					cacheIndex[subClassLength] = [index];
				} else if (!cacheItems.some(cacheItem => {
					if (cacheItem.every((item) => {
						return subClass.includes(item as any);
					})) {
						index = cacheItem;
						return true;
					}
				})) {
					cacheItems.push(index);
				}
			}
			if (subClassLength >= 1 && Widget.created) {
				let map = cache.get(element);

				if (!map) {
					cache.set(element, map = new Map());
				} else {
					widgets = map.get(index) as T[];
					if (widgets) {
						//console.log("load from cache", subClass[0].selector, returnWidgets())
						return returnWidgets();
					}
				}
				map.set(index, widgets = []);
			} else {
				widgets = [];
			}
			let filter = subClassLength && subClass.find(sc => (sc.prototype as IsInputWidget).isInputWidget)
				? filterInputWidget
				: filterWidget,
				walker = document.createTreeWalker(element, NodeFilter.SHOW_ELEMENT, filter as any, false),
				node: Widget;

			while (element) {
				node = (element as HTMLElement).icWidget;
				if (!index || subClass.find(sc => node instanceof sc)) {
					widgets.push(node as any);
				}
				if (node instanceof ScreenWidget && node.detached) {
					walker.currentNode = node.dom;
					pushElement = element;
				}
				element = walker.nextNode() as HTMLElement;
				if (!element && pushElement) {
					walker.currentNode = pushElement;
					pushElement = null;
					element = walker.nextNode() as HTMLElement;
				}
			}
			return returnWidgets();
		}
	}
};
