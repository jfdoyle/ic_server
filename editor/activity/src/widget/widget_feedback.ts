///<reference path="widget_activity.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	let selector = "ic-feedback";

	/**
	 * Version of FeedbackWidget
	 */
	version[selector] = 1;

	export class FeedbackWidget extends BoxWidget implements OnSubmit, OnReset, OnReveal {
		static selector = selector;

		public timeout: number;
		public timer: any;

		constructor(element: HTMLElement) {
			super(element);

			let timeout = this.timeout = (parseFloat(element.getAttribute(FeedbackWidget.selector)) || 0) * 1000;

			if (timeout) {
				element.addEventListener("click", this.hideFeedback);
			}
		}

		startup() {
			this.on([".reset", ".reveal", ".submit"]);
		}

		onReset(screen: ScreenWidget) {
			if ((!screen || this.screenWidget === screen)) {
				this.hideFeedback();
			}
		}

		onReveal(screen: ScreenWidget) {
			if ((!screen || this.screenWidget === screen)) {
				this.hideFeedback();
			}
		}

		onSubmit(screen: ScreenWidget) {
			if (!screen || this.screenWidget === screen) {
				let element = this.element,
					activity = this.get(screen, ActivityWidget).first();

				// get score, hide elements that don't want to be seen
				if (activity) {
					let percent = activity.markable.scaled * 100,
						attempts = activity.attempts || (this.screenWidget || this.rootWidget).attempts,
						rand: Element[] = [],
						possible: Element,
						best: Element;

					[].forEach.call(element.children, function(child: HTMLElement) {
						let i: number,
							wants = child.getAttribute("ic-feedback").split(/[,\s]+/),
							want: string;

						for (i = 0; i < wants.length; i++) {
							want = wants[i];
							if (want) {
								if (want[0] === "@" && want !== "@" + attempts) {
									return;
								}
								if (want.endsWith("%")) {
									if (parseInt(want, 10) > percent) {
										return;
									}
									best = child;
								}
								if (want === "*") {
									rand.push(child);
								} else if (!possible) {
									possible = child;
								}
							}
						}
					});
					if (!best && rand.length) {
						best = rand[Math.floor(Math.random() & rand.length)];
					}
					if (!best) {
						best = possible;
					}
					if (best) {
						[].forEach.call(element.children, function(child: HTMLElement) {
							child.style.display = best === child ? "" : "none";
						});
					}
					let random = querySelectorAll(best || element, "[ic-feedback='*']");

					if (random.length) {
						random.forEach(function(el: HTMLElement) {
							el.style.display = "none";
						});
						(random[Math.floor(Math.random() * random.length)] as HTMLElement).style.display = "";
					}
					this.showFeedback();
				}
			}
		}

		hideFeedback = () => {
			this.element.style.display = "";
			clearTimeout(this.timer);
			return this;
		}

		showFeedback() {
			this.element.style.display = "flex";
			if (this.timeout > 0) {
				this.timer = setTimeout(this.hideFeedback, this.timeout);
			}
			return this;
		}

		toggleFeedback() {
			if (this.element.style.display) {
				return this.hideFeedback();
			}
			return this.showFeedback();
		}
	}
};
