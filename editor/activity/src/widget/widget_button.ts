///<reference path="widget.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	let selector = "ic-button";

	/**
	 * Version of ButtonWidget
	 */
	version[selector] = 1;

	export class ButtonWidget extends Widget implements AllMouseMove, AllMouseUp, OnClick, OnMouseDown, OnMouseUp {
		static selector = selector;

		static pressed: ButtonWidget;

		private icOnClickFn: string;

		public isStory: boolean;

		constructor(element: HTMLElement) {
			super(element);

			if (element.getAttribute("icOnClick")) {
				this.icOnClickFn = element.getAttribute("icOnClick");
			}
			if (element.hasAttribute("ic-story")) {
				this.isStory = true;
			}
			this.on(["click", "mousedown", "touchstart", "mouseup", "touchend"]);
		}

		onMouseDown(event: MouseEvent | TouchEvent) {
			if (!this.isDisabled(event)) {
				ButtonWidget.pressed = this;
				this.activate()
					.toggleState(["click", "focus"])
					.on(["!mousemove", "!touchmove", "!mouseup", "!touchend"]);
			}
		}

		onClick(event: MouseEvent | TouchEvent) {
			// Overload as needed
		}

		onMouseUp(event: MouseEvent | TouchEvent) {
			if (ButtonWidget.pressed === this) {
				ButtonWidget.pressed = undefined;
				this.removeState("click");
				this.callUserFunc(this.icOnClickFn);
				if (this.isStory) {
					let box = this.element,
						prev: BoxWidget,
						next: BoxWidget;

					while (box && !next) {
						box = box.parentElement.closest("ic-box[ic-story]") as HTMLElement;
						if (box) {
							prev = box.icWidget as BoxWidget;
							next = prev.nextStory;
						}
					}
					if (next) {
						prev.addState("disabled");
					}
				}
			}
		}

		onMouseEnter(event: MouseEvent | TouchEvent) {
			if (ButtonWidget.pressed === this) {
				this.addState(["click", "hover"]);
			} else if (!ButtonWidget.pressed) {
				super.onMouseEnter(event);
			}
		}

		onMouseLeave(event: MouseEvent | TouchEvent) {
			if (ButtonWidget.pressed === this) {
				this.removeState(["click", "hover"]);
			} else if (!ButtonWidget.pressed) {
				super.onMouseLeave(event);
			}
		}

		allMouseMove(event: MouseEvent | TouchEvent) {
			if (ButtonWidget.pressed) {
				event.preventDefault();
			}
		}

		allMouseUp(event: MouseEvent | TouchEvent) {
			this.off("!mousemove", "!touchmove", "!mouseup", "!touchend");
			ButtonWidget.pressed = null;
		}
	}
};
