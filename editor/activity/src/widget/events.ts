///<reference path="../core.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	export type WidgetTargetEvents = "blur"
		| "focus"
		| "keypress"
		| "keyup"

	export type WidgetEvents = WidgetTargetEvents
		| "click"
		| "mouseenter"
		| "mousedown"
		| "mouseleave"
		| "mouseup"
		| "touchend"
		| "touchstart"
		| "!mousemove"
		| "!mouseup"
		| "!touchmove"
		| "!touchend"
		| ".css"
		| ".frame"
		| ".persist"
		| ".reset"
		| ".resize"
		| ".reveal"
		| ".score"
		| ".screen"
		| ".scroll"
		| ".state"
		| ".submit"
		| ".timeout"
		| ".tick"

	export interface OnBlur {
		onBlur(event?: MouseEvent | TouchEvent): void;
	}

	export interface OnCss {
		onCss(event?: UIEvent): void;
	}

	export interface OnClick {
		onClick(event?: MouseEvent | TouchEvent): void;
	}

	export interface OnFocus {
		onFocus(event?: MouseEvent | TouchEvent): void;
	}

	export interface OnKeyPress {
		onKeyPress(event?: KeyboardEvent): void;
	}

	export interface OnKeyUp {
		onKeyUp(event?: KeyboardEvent): void;
	}

	export interface OnMouseDown {
		onMouseDown(event?: MouseEvent | TouchEvent): void;
	}

	export interface OnMouseEnter {
		onMouseEnter(event?: MouseEvent | TouchEvent): void;
	}

	export interface OnMouseLeave {
		onMouseLeave(event?: MouseEvent | TouchEvent): void;
	}

	export interface OnMouseUp {
		onMouseUp(event?: MouseEvent | TouchEvent): void;
	}

	export interface OnResize {
		onResize(event?: UIEvent): void;
	}

	export interface OnState {
		onState(widget?: Widget, stateList?: WidgetState[]): void;
	}

	export interface OnScreen {
		onScreen(screen?: ScreenWidget): void;
	}

	export interface AllMouseMove {
		allMouseMove(event?: MouseEvent | TouchEvent): void;
	}

	export interface AllMouseUp {
		allMouseUp(event?: MouseEvent | TouchEvent): void;
	}

	export interface OnFrame {
		onFrame(): void;
	}

	export interface OnReset {
		onReset(screen: ScreenWidget): void;
	}

	export interface OnReveal {
		onReveal(screen: ScreenWidget): void;
	}

	export interface OnScore {
		onScore(widget: InputWidget): void;
	}

	export interface OnScroll {
		onScroll(widget: Widget): void;
	}

	export interface OnSubmit {
		onSubmit(screen: ScreenWidget): void;
	}

	export interface OnTick {
		onTick(): void;
	}

	export interface OnTimeout {
		onTimeout(): void;
	}

	export interface OnPersist {
		onPersist(state?: any): any;
	}

	export abstract class WidgetEvent {
		getEventHandler(event: WidgetEvents): any {
			switch (event) {
				case "blur":
					return (this as any as OnBlur).onBlur;

				case "click":
					return (this as any as OnClick).onClick;

				case ".css":
					return (this as any as OnCss).onCss;

				case "focus":
					return (this as any as OnFocus).onFocus;

				case ".frame":
					return (this as any as OnFrame).onFrame;

				case "keypress":
					return (this as any as OnKeyPress).onKeyPress;

				case "keyup":
					return (this as any as OnKeyUp).onKeyUp;

				case "mousedown":
				case "touchstart":
					return (this as any as OnMouseDown).onMouseDown;

				case "mouseenter":
					return (this as any as OnMouseEnter).onMouseEnter;

				case "mouseleave":
					return (this as any as OnMouseLeave).onMouseLeave;

				case "mouseup":
				case "touchend":
					return (this as any as OnMouseUp).onMouseUp;

				case "!mousemove":
				case "!touchmove":
					return (this as any as AllMouseMove).allMouseMove;

				case "!mouseup":
				case "!touchend":
					return (this as any as AllMouseUp).allMouseUp;

				case ".persist":
					return (this as any as OnPersist).onPersist;

				case ".reset":
					return (this as any as OnReset).onReset;

				case ".resize":
					return (this as any as OnResize).onResize;

				case ".reveal":
					return (this as any as OnReveal).onReveal;

				case ".score":
					return (this as any as OnScore).onScore;

				case ".screen":
					return (this as any as OnScreen).onScreen;

				case ".scroll":
					return (this as any as OnScroll).onScroll;

				case ".state":
					return (this as any as OnState).onState;

				case ".submit":
					return (this as any as OnSubmit).onSubmit;

				case ".timeout":
					return (this as any as OnTimeout).onTimeout;

				case ".tick":
					return (this as any as OnTick).onTick;

				default:
					console.error("Unknown event", event);
			}
		}
	}
};
