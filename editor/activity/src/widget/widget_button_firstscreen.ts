///<reference path="widget_button.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	export class FirstScreenButtonWidget extends ButtonWidget implements OnState {
		static selector = "ic-button[ic-button=firstscreen]";

		constructor(element: HTMLElement) {
			super(element);

			this.on(".state");
		}

		onClick(event: MouseEvent | TouchEvent) {
			if (!this.isDisabled(event)) {
				let screen = this.get(ScreenWidget).first();

				if (screen) {
					screen.addState("active");
				}
			}
			super.onClick(event);
		}

		onState(widget: Widget, stateList: WidgetState[]) {
			if (widget instanceof ScreenWidget && stateList.includes("active")) {
				this.toggleState("disabled", this.get(ScreenWidget).first().hasState("active"));
			}
		}
	}
};
