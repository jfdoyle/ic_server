///<reference path="widget_input.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	let selector = "ic-activity";

	/**
	 * Version of ActivityWidget
	 */
	version[selector] = 1;

	/**
	 * An <strong><code>ActivityWidget</code></strong> is responsible for all
	 * scoring. All Widgets inside an Activity get their scores merged into the
	 * Activity (perhaps via specific marking rules). If a Widget is not within
	 * an Activity then it cannot store it's score.
	 */
	export class ActivityWidget extends Widget implements OnPersist, OnState, OnScreen, OnTimeout, OnReset {

		/**
		 * Selector used to recognise an ActivityWidget.
		 */
		static selector = selector;

		/**
		 * Input Widgets get an index to help with identifying them.
		 */
		static isInputWidget = true;

		/**
		 * Tree Widgets help define the WidgetNode tree, if it's not a tree then
		 * it's a leaf.
		 */
		static isTreeWidget = true;

		/**
		 * Set when this needs marking (prevent wasting cycles).
		 */
		public needMarking: boolean;
		public markable: JsonData = {
			score: 0,
			min: 0
		}

		public allowDuplicates: boolean;

		public toggleRange: {[group: number]: ToggleWidget[]};

		@persist
		public attempts: number = 0;

		constructor(element: HTMLElement) {
			super(element);

			let data: JsonData = this.data
				|| ((this.parentWidget ? this.parentWidget.data : {}) || {}).getTree([this.selector, String(this.index)], {}) as Object;

			// this gets either our own data object, or data from our activity's json object
			if (data) {
				data.clone(this.markable, true);
				if (data.duplicates) {
					this.allowDuplicates = true;
				}
			}
			this.on([".persist", ".screen", ".state", ".timeout", ".reset"]);
			//			this.on(["mousedown", "touchstart", ".persist", ".screen", ".state", ".timeout"]);
		}

		get isScored(): boolean {
			return (this.markable.points || 0) >= 0;
		}

		get hasAnswered(): boolean {
			return this.hasState("attempted");
		}

		onPersist(state?: any): any {
			if (!isUndefined(state)) {
				this.get(this, DroppableWidget).forEach(function(drop) {
					drop.onReset();
				});
			}
			let result = this.persistTree({
				"drop": DroppableWidget,
				"select": SelectWidget,
				"text": TextWidget,
				"toggle": ToggleWidget
			}, state);

			if (!isUndefined(state)) {
				this.mark();
			}
			return result;
		}

		onReset(screen: ScreenWidget) {
			if ((!screen || this.screenWidget === screen)) {
				this.removeState("attempted");
			}
		}

		onScreen(screen: ScreenWidget) {
			if (this.screenWidget === screen && !this.get(screen, ActivityWidget, "active", false).length) {
				this.addState("active");
			}
		}

		onState(widget: Widget, stateList: WidgetState[]) {
			if (this === widget && this.parentWidget && this.parentWidget !== this.rootWidget) {
				let newStates: {[state: string]: boolean} = {},
					found: boolean;

				if (stateList.includes("attempted") && this.hasState("attempted")) {
					newStates["attempted"] = found = true;
				}
				if (stateList.includes("active") && this.hasState("active")) {
					newStates["active"] = found = true;
				}
				if (stateList.includes("answered")) {
					newStates["answered"] = this.hasState("answered");
					found = true;
				}
				if (found) {
					this.parentWidget.toggleState(newStates);
				}
			}
		}

		onTimeout() {
			this.get(this, InputWidget).forEach(function(widget) {
				widget.addState("disabled");
			});
		}

		expandReferences = InputWidget.prototype.expandReferences;
		parse = InputWidget.prototype.parse;
		getPoints = InputWidget.prototype.getPoints;

		internalMark() {
			let mark = this.markable;

			if (mark.points === -1) {
				let element = this.element;

				element.removeAttribute("data-mark");
				element.removeAttribute("data-percent");
				element.removeAttribute("data-points");
			} else {
				mark.raw = mark.max = mark.correct = mark.total = 0;
				this.get(this, this instanceof ActivitiesWidget ? ActivityWidget as any : InputWidget).forEach((widget: InputWidget) => {
					if (widget.activitiesWidget === this || widget.activityWidget === this && !widget.noMarking) {
						let points = widget.getPoints()

						if (points >= 0) {
							mark.raw += Math.max(0, widget.markable.score || 0);
							mark.max += Math.max(0, points);
							mark.total++;
							if (widget.markable.scaled > 0) {
								mark.correct++;
							}
						}
					}
				});
				InputWidget.prototype.internalMark.call(this);
			}
			this.toggleState("answered", this.get(this, InputWidget).some(widget => widget.isScored && widget.hasAnswered));
		}

		mark() {
			if (!this.needMarking) {
				this.needMarking = true;
				if (this.markable.points !== -1) {
					this.get(this, this instanceof ActivitiesWidget ? ActivityWidget as any : InputWidget).forEach((widget: InputWidget) => {
						if (widget.activitiesWidget === this || widget.activityWidget === this) {
							widget.mark();
						}
					});
				}
				setImmediate(InputWidget.internalMark);
			}
		}
	}
};
