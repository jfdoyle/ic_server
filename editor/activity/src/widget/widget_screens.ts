///<reference path="widget.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	let selector = "ic-screens";

	/**
	 * Version of ScreensWidget
	 */
	version[selector] = 1;

	function preventDefault(event: Event) {
		try {
			event.preventDefault();
			event.stopImmediatePropagation();
		} catch (e) {}
	}

	export class ScreensWidget extends Widget {
		static selector = selector;
		static isInputWidget = true;
		static isTreeWidget = true;

		private static screenIndex: number;
		private static screenLength: number = 0;
		private static screenWidget: ScreenWidget;

		/**
		 * Have all images loaded?
		 */
		private loaded: boolean;
		private loadTimer: any;

		@persist
		public attempts: number = 0;

		static get trickle(): number {
			return 1000 + Math.random() * 2000;
		}

		constructor(element: HTMLElement) {
			super(element);
			element.addEventListener("dragstart", preventDefault, true);
		}

		startup() {
			super.startup();
			let screens = this.get(ScreenWidget);

			Widget.root = this;
			ScreensWidget.screenLength = screens.length;
			// TODO: Navigate to first screen from query string
			this.screen = screens.length ? screens.first().index - 1 : 0;
			this.on(".persist");
		}

		onPersist(state?: any): any {
			let result = this.persistTree({
				"screen": ScreenWidget,
				"timer": TimerWidget
			}, state);

			if (state) {
				setImmediate(NavWidget.update);
			}
			if (isUndefined(state)) {
				return result;
			}
		}

		/**
		 * Load images from current screen to end, then work back from current screen to beginning...
		 */
		loadImages = () => {
			if (!this.loaded) {
				let screenIndex = this.screen,
					index = screenIndex,
					screenList = this.get(ScreenWidget),
					screenListLength = screenList.length;

				while (index >= 0) {
					if (!screenList[index].loadImages(index === screenIndex)) {
						this.loadTimer = setTimeout(this.loadImages, ScreensWidget.trickle);
						return;
					}
					if (index < screenIndex) {
						index--;
					} else if (++index >= screenListLength) {
						index = screenIndex - 1;
					}
				}
				this.loadTimer = null;
				this.loaded = true;
			}
		}

		@persist
		set screen(index: number) {
			if (index !== ScreensWidget.screenIndex && index >= 0 && index < ScreensWidget.screenLength) {
				let event = document.createEvent("Event"),
					oldScreen = ScreensWidget.screenIndex,
					oldElement = (ScreensWidget.screenWidget || {} as ScreenWidget).element,
					screenList = this.get(ScreenWidget),
					newScreen = ScreensWidget.screenWidget = screenList[ScreensWidget.screenIndex = index];

				if (!this.loaded) {
					if (this.loadTimer) {
						clearTimeout(this.loadTimer);
					}
					this.loadImages();
				}
				this.get(ScreenWidget, "active").not(newScreen).forEach(function(screen) {
					screen.removeState(["active", "hover"]);
					setImmediate(screen.detach);
				});
				newScreen.attach();
				Widget.trigger(".screen", newScreen.addState(["active", "visited"]));
				event.initEvent("ic-screen", false, false);
				(event as any).icData = {
					index: index,
					element: newScreen.element,
					oldIndex: oldScreen,
					oldElement: oldElement
				};
				document.body.dispatchEvent(event);
			}
		}

		get screen() {
			return ScreensWidget.screenIndex;
		}

		get screenWidget() {
			return ScreensWidget.screenWidget;
		}

		get length() {
			return ScreensWidget.screenLength;
		}
	}
};
