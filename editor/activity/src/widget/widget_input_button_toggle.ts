///<reference path="widget_input_button.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	let selector = "ic-toggle";

	/**
	 * Version of ToggleWidget
	 */
	version[selector] = 1;


	export const enum ToggleType {
		SINGLE = 0,
		MULTIPLE = 1,
		RANGE = 2,
		MULTIRANGE = 3
	}

	export class ToggleWidget extends InputButtonWidget implements OnPersist, OnState, OnReveal, OnReset {
		static selector = selector + ",ic-button[ic-button=toggle]";

		static range: boolean;

		public toggleType = ToggleType.SINGLE;

		public startChecked: boolean;
		public startDrag: boolean;

		count: number;

		private internalValue: string;

		constructor(element: HTMLElement) {
			super(element);

			let mark = this.markable,
				selector = ToggleWidget.selector.split(",").first();

			switch (element.getAttribute(selector)) {
				case "double":
					this.toggleType = ToggleType.MULTIPLE;
					this.count = 2;
					break;

				case "multiple":
					this.toggleType = ToggleType.MULTIPLE;
					this.count = parseInt(element.getAttribute(selector + "-count"), 10) || 0;
					break;

				case "range":
					this.toggleType = ToggleType.RANGE;
					break;

				case "multirange":
					this.toggleType = ToggleType.MULTIRANGE;
					break;
			}
			if (mark.points === undefined) {
				if (this.toggleType === ToggleType.RANGE || this.toggleType === ToggleType.MULTIRANGE) {
					let text = element.textContent;

					mark.points = text && /[^ .,:;'"]/.test(text) ? 1 : -1;
				} else {
					mark.points = 1;
				}
			}
			if (mark.value && mark.value !== "checked") {
				this.internalValue = mark.value as string;
				if (mark.answer === "checked") {
					mark.answer = this.internalValue;
					mark.value = "";
				}
			}
			this.toggleState("checked", mark.value === (this.internalValue || "checked"))
				.on(".state");
			if (element.tagName.toLowerCase() === selector) {
				this.on(".persist");
			} else {
				this.on(".timeout");
			}
		}

		startup() {
			super.startup();
			let closestWidget = this.activityWidget || this.screenWidget;

			if ((this.toggleType === ToggleType.RANGE || this.toggleType === ToggleType.MULTIRANGE)
				&& closestWidget) {
				if (!closestWidget.toggleRange) {
					closestWidget.toggleRange = {};
				}
				if (!closestWidget.toggleRange[this.group]) {
					let wanted = this.toggleType;

					closestWidget.toggleRange[this.group] = this.get(closestWidget, ToggleWidget, this.group, true).filter(function(toggle) {
						if (toggle.toggleType !== ToggleType.RANGE && toggle.toggleType !== ToggleType.MULTIRANGE) {
							return false;
						}
						if (toggle.toggleType !== wanted) {
							console.error("Error: Mismatched ic-toggle=\"" + (wanted === ToggleType.MULTIRANGE ? "multi" : "") + "range\" type:", toggle);
						}
						return true;
					});
				}
			}
			if (this.startState) {
				this.startState["checked"] = this.startState["checked"] || false;
				this.on(".reveal");
			}
		}

		get hasAnswered(): boolean {
			return this.hasState("checked");
		}

		onPersist(state?: number): number {
			if (isUndefined(state)) {
				return this.hasState("checked") ? 1 : 0;
			}
			this.toggleState("checked", !!state);
		}

		onReveal(screen: ScreenWidget) {
			if ((!screen || this.screenWidget === screen)) {
				this
					.toggleState({
						"disabled": true,
						"reveal": true,
						"checked": this.markable.answer === "checked"
					})
					.mark();
			}
		}

		startDragging(click?: boolean) {
			let range = (this.activityWidget || this.screenWidget).toggleRange[this.group];

			if (!click) {
				this.startDrag = true;
				this.toggleState("checked")
					.parentWidget.addState("attempted")
			}
			if (this.toggleType === ToggleType.RANGE) {
				let index = range.indexOf(this),
					isChecked = this.hasState("checked"),
					prevChecked = index === 0 ? isChecked : !range[index - 1].hasState("checked"),
					nextChecked = index === range.length - 1 ? isChecked : !range[index + 1].hasState("checked");

				if ((isChecked && prevChecked && nextChecked)
					|| (!isChecked && prevChecked === nextChecked)) {
					range.forEach((toggle) => {
						if (toggle !== this) {
							toggle.removeState("checked");
						}
					});
				}
				if (!isChecked && prevChecked === nextChecked) {
					this.toggleState("checked");
				}
			}
			range.forEach((toggle) => {
				toggle.startChecked = toggle.hasState("checked");
			});
		}

		finishDragging() {
			let checked: boolean = false,
				attempted: boolean = false,
				range = (this.activityWidget || this.screenWidget).toggleRange[this.group],
				fixDangling = function(toggle: ToggleWidget) {
					let isChecked = toggle.hasState("checked");

					attempted = attempted || isChecked;
					if (!checked && toggle.markable.points === -1 && isChecked) {
						toggle.removeState("checked");
					} else {
						checked = isChecked;
					}
				};

			range.forEach(fixDangling);
			checked = attempted = false;
			range.slice(0).reverse().forEach(fixDangling);
			if (attempted && this.parentWidget) {
				this.parentWidget.addState("attempted")
			}
		}

		onMouseDown(event: MouseEvent | TouchEvent) {
			this.startDrag = ToggleWidget.range = false;
			super.onMouseDown(event);
		};

		onMouseUp(event: MouseEvent | TouchEvent) {
			let pressed = InputButtonWidget.pressed;

			super.onMouseUp(event);
			if (pressed && !this.isDisabled(event) && !this.startDrag) {
				if (this.activityWidget && this.toggleType === ToggleType.MULTIPLE && this.count) {
					let count = 0;

					this.get(this.activityWidget, ToggleWidget, this.group, false).forEach(function(widget) {
						if (widget.hasState("checked")) {
							count++;
						}
					});
					if (count >= this.count) {
						return;
					}
				}
				this.toggleState("checked");
				if (this.activityWidget) {
					if (this.toggleType === ToggleType.SINGLE) {
						this.get(this.activityWidget, ToggleWidget, this.group, false).forEach(function(widget) {
							if (widget.toggleType === ToggleType.SINGLE) {
								widget.removeState("checked");
							}
						});
					} else if (this.toggleType === ToggleType.RANGE || this.toggleType === ToggleType.MULTIRANGE) {
						this.startDragging(!ToggleWidget.range);
						this.finishDragging();
					}
				}
				if (this.screenWidget) {
					this.parentWidget.addState("attempted")
				}
			}
		}

		onMouseEnter(event: MouseEvent | TouchEvent) {
			let parent = this.activityWidget || this.screenWidget;

			if (InputButtonWidget.pressed instanceof ToggleWidget
				&& (this.toggleType === ToggleType.RANGE || this.toggleType === ToggleType.MULTIRANGE)
				&& parent) {
				let range = parent.toggleRange[this.group],
					first = InputButtonWidget.pressed as ToggleWidget,
					start = range.indexOf(first),
					end = range.indexOf(this);

				if (this !== first) {
					ToggleWidget.range = true;
				}
				if (start !== -1) {
					if (!first.startDrag) {
						first.startDragging();
					}
					let min = Math.min(start, end),
						max = Math.max(start, end),
						checked = (InputButtonWidget.pressed as ToggleWidget).hasState("checked");

					range.forEach(function(toggle, index) {
						toggle
							.toggleState("checked", index >= min && index <= max ? checked : toggle.startChecked);
					});
					this.finishDragging();
				}
			}
			super.onMouseEnter(event);
		}

		onState(widget: Widget, stateList: WidgetState[]) {
			if (this === widget && stateList.includes("checked")) {
				let mark = this.markable;

				mark.value = this.hasState("checked") ? this.internalValue || "checked" : "";
				this.mark();
			}
		}

		onTimeout() {
			// Only used for the ic-button="toggle" type
			this.removeState("checked");
		}
	}
};
