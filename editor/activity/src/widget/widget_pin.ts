///<reference path="widget.ts" />
///<reference path="widget_input_button_draggable.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	let selector = "ic-pin";

	/**
	 * Version of PinWidget
	 */
	version[selector] = 1;

	export class PinWidget extends Widget implements DragHelper, OnCss, OnMouseDown, OnMouseUp, OnResize, OnScreen {
		static selector = selector;

		helperElement: HTMLElement;

		xmlns = "http://www.w3.org/2000/svg";
		rootsvg: SVGElement;
		svg: SVGElement;
		svgWidth: number;
		svgHeight: number;
		line: SVGLineElement;
		lineDisabled: boolean;

		// When linked
		drop: DroppableWidget;
		drag: DraggableWidget;
		target: PinWidget;
		isSource: boolean;

		parent: DraggableWidget | DroppableWidget;
		offsetTop: number;
		offsetLeft: number;

		constructor(element: HTMLElement) {
			super(element);

			for (let parent = element.parentElement; parent; parent = parent.parentElement) {
				let icWidget = parent.icWidget;

				if (icWidget instanceof DraggableWidget || icWidget instanceof DroppableWidget) {
					this.parent = icWidget as DraggableWidget;
					this.on(["mousedown", "mouseup", ".screen"])
					break;
				}
			}
			let screen = this.screenWidget.element,
				rootsvg = querySelectorSVG(this.rootWidget.element, ":scope>svg.ic-line"),
				svg = querySelectorSVG(screen, ":scope>svg.ic-line");

			if (rootsvg) {
				this.rootsvg = rootsvg;
			}
			if (!svg) {
				if (rootsvg) {
					svg = rootsvg.cloneNode(false) as SVGElement;
				} else {
					svg = document.createElementNS(this.xmlns, "svg") as SVGElement;
					svg.setAttribute("xmlns", this.xmlns);
					svg.setAttribute("ic-line", "");
					svg.setAttribute("class", "ic-line");
					//svg.setAttribute("viewBox", "0 0 1000 1000");
					//svg.setAttribute("preserveAspectRatio", "none");
				}
				screen.insertBefore(svg, screen.firstChild);
			}
			this.svg = svg;
			if (this.parent && /disabled/i.test(this.parent.element.getAttribute("ic-state"))) {
				this.lineDisabled = true;
			}
			if (!(this.parent instanceof DraggableWidget)) {
				this.addState("hidden")
			}
			this.on(".css");
		}

		private getTarget(event: MouseEvent | TouchEvent): DraggableWidget | PinWidget {
			if (this.parent instanceof DraggableWidget) {
				return this.parent as DraggableWidget;
			} else if (this.drag && !this.isSource) {
				return this.drag;
			} else if (/^absolute$/i.test(getComputedStyle(this.element).position)) {
				let i: number,
					el: HTMLElement,
					widget: PinWidget,
					children = this.element.parentElement.children,
					[clientX, clientY] = getCoords(event);

				for (i = 0; i < children.length; i++) {
					el = children[i] as HTMLElement;
					widget = el.icWidget as PinWidget;
					if (widget instanceof PinWidget && widget.inRect(clientX, clientY) && widget.drag && !widget.isSource) {
						return widget.drag;
					}
				}
			}
		}

		onMouseDown(event: MouseEvent | TouchEvent) {
			let target = this.getTarget(event) as OnMouseDown;

			if (target) {
				target.onMouseDown(event);
			}
		}

		onMouseUp(event: MouseEvent | TouchEvent) {
			let target = this.getTarget(event) as OnMouseUp;

			if (target) {
				target.onMouseUp(event);
			}
		}

		// TODO: Put drawing final lines onto ic-pins, that way it can handle multiple targets and making sure they take the shortest path?

		getHelper(event: MouseEvent | TouchEvent, data: DragData, within: ClientRect): HTMLElement {
			let helper = this.helperElement = createElement("ic-pin"),
				rect = getBoundingClientRect(this);

			helper.className = "drag-helper ict-state-dragging " + this.element.className;
			appendChild(this.element, helper);
			if (this.target) {
				this.target.addState("hidden");
			}
			data.offsetY = rect.height / 2;
			data.offsetX = rect.width / 2;
			data.height = rect.height;
			data.width = rect.width;
			this.update();
			return helper;
		}

		onHelperMove(event: MouseEvent | TouchEvent, data: DragData, within: ClientRect) {
			let style = this.helperElement.style,
				[clientX, clientY] = getCoords(event),
				left = Math.range(within.left - data.left, clientX - data.offsetX, (within.right - data.width - data.left) / scaleFactor),
				top = Math.range(within.top - data.top, clientY - data.offsetY, (within.bottom - data.height - data.top) / scaleFactor);

			style.top = top + "px";
			style.left = left + "px";
			this.update();
		}

		onScreen(screen: ScreenWidget) {
			if (this.screenWidget === screen && this.target) {
				this.update();
			}
		}

		freeHelper(event?: MouseEvent | TouchEvent, drag?: DraggableWidget, drop?: DroppableWidget) {
			let target = this.target;

			this.helperElement.remove();
			if (target) {
				target.removeState("hidden");
			}
			this.helperElement = undefined;
			this.update();
		}

		linkPin(drag: DraggableWidget, drop: DroppableWidget, pin?: PinWidget): this {
			// console.info("linking pins", this.element, drop && drop.element, drag && drag.element);
			if (drag === this.parent) {
				if (!pin) {
					pin = drop.getPin();
				}
				if (pin) {
					this.unlinkPin();
					pin.unlinkPin();
					drop.draggables.pushOnce(drag);
					drag.droppables.pushOnce(drop);
					this.drag = pin.drag = drag;
					this.drop = pin.drop = drop;
					pin.isSource = false;
					pin.target = this;
					this.isSource = true;
					this.target = pin;
					this.update();
					pin.update();
				}
			}
			return this;
		}

		unlinkPin(): this {
			// console.info("unlinking pins", this.element, this.drop.element, this.drag.element);
			if (this.target && (this.drag || this.drop)) {
				let pin = this.target;

				if (this.isSource) {
					if (this.drop) {
						this.drop.draggables.remove(this.drag);
						this.drag.droppables.remove(this.drop);
						this.drop.update();
					}
					this.drag = this.drop = this.target = pin.drag = pin.drop = pin.target = this.isSource = pin.isSource = null;
					this.update();
					pin.update();
				} else {
					pin.unlinkPin();
				}
			}
			return this;
		}

		onCss() {
			this.update();
		}

		onResize() {
			this.update();
		}

		internalUpdate = () => {
			let target = this.helperElement || (this.target && this.target.element);

			if (target) {
				if (this.isSource || this.helperElement) {
					let line = this.line,
						drag = this.drag,
						drop = this.drop;

					if (!line) {
						if (!this.helperElement && drag && drop) {
							// See if we've got a previous line we should be taking over - only useful for loading state
							line = querySelectorSVG(this.svg, "[ic-line='" + (drag && drag.activitiesWidget ? (drag.activityWidget || drag.activitiesWidget).realIndex + "_" : "") + drag.index + "_" + drop.realIndex + "']") as SVGLineElement;
						}
						if (!line) {
							if (this.rootsvg) {
								line = querySelectorSVG(this.rootsvg, "defs line") as SVGLineElement;
							}
							if (line) {
								line = line.cloneNode() as SVGLineElement;
							} else {
								line = document.createElementNS(this.xmlns, "line") as SVGLineElement;
							}
							appendChild(this.svg, this.line = line);
						}
					}
					let box = getBoundingClientRect(this.svg, true),
						src = getBoundingClientRect(this.element, true),
						dest = getBoundingClientRect(target, true),
						viewBox = (this.svg.getAttribute("viewBox") || "").regex(/(\d+)\s+(\d+)\s+(\d+)\s+(\d+)/) as number[],
						width = viewBox ? (viewBox[2] - viewBox[0]) / box.width : 1,
						height = viewBox ? (viewBox[3] - viewBox[1]) / box.height : 1;

					if (this.lineDisabled || (this.target && this.target.lineDisabled)) {
						line.setAttribute("ic-disabled", "");
					}
					if (!this.helperElement && drag && drop) {
						line.setAttribute("ic-line", (drag && drag.activitiesWidget ? (drag.activityWidget || drag.activitiesWidget).realIndex + "_" : "") + drag.index + "_" + drop.realIndex);
					}
					if (src.left && dest.left) {
						line.setAttribute("x1", String((src.width / 2 + src.left - box.left) * width));
						line.setAttribute("x2", String((dest.width / 2 + dest.left - box.left) * height));
						line.setAttribute("y1", String((src.height / 2 + src.top - box.top) * width));
						line.setAttribute("y2", String((dest.height / 2 + dest.top - box.top) * height));
					}
					if (!viewBox) {
						this.on(".resize");
					}
				}
				this.removeState("hidden");
			} else {
				if (!(this.parent instanceof DraggableWidget)) {
					this.addState("hidden");
				}
				if (this.line) {
					this.line.parentElement.removeChild(this.line);
					this.line = undefined;
					this.off(".resize");
				}
			}
		}

		update() {
			setImmediate(this.internalUpdate);
		}
	}
};
