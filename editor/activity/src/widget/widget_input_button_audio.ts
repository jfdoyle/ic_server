///<reference path="widget_input_button.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	let selector = "ic-audio";

	/**
	 * Version of AudioWidget
	 */
	version[selector] = 1;

	let pausedAudio: HTMLAudioElement[] = [];

	document.addEventListener("visibilitychange", function() {
		if (document.hidden) {
			getElementsByTagName("audio").forEach(function(audio: HTMLAudioElement) {
				if (!audio.paused) {
					audio.pause();
					pausedAudio.push(audio);
				}
			});
		} else {
			while (pausedAudio.length) {
				pausedAudio.pop().play();
			}
		}
	});

	const enum AudioType {
		AUTO,
		PLAY,
		RESET
	}

	export class AudioWidget extends InputButtonWidget implements OnScreen, OnTimeout {
		static selector = selector;

		audio: HTMLAudioElement;
		findAudio: boolean;
		audioType: AudioType;
		fill: string;

		constructor(element: HTMLElement) {
			super(element);
			let audio = element.getElementsByTagName("audio")[0],
				mark = this.markable;

			this.audioType = AudioType.AUTO;
			switch (element.getAttribute(this.selector)) {
				case "reset":
					this.audioType = AudioType.RESET;
					break;
				case "play":
					this.audioType = AudioType.PLAY;
				default:
					if (element.hasAttribute(this.selector + "-fill")) {
						this.fill = element.getAttribute(this.selector + "-fill")
					}
					break;
			}
			if (!audio) {
				this.findAudio = true;
				this.addState(["disabled", "empty"]);
			} else {
				this.audio = audio;
				this.setupListeners();
			}
			if (mark.points === undefined) {
				mark.points = -1;
			}
			this.on([".screen", ".timeout"]);
		}

		onPlaying = () => {
			querySelectorAll(this.rootWidget.element, "audio,video").forEach((element: HTMLMediaElement) => {
				if (element !== this.audio) {
					element.pause();
				}
			});
			this.addState("playing");
			if (this.audioType !== AudioType.AUTO && this.parentWidget) {
				this.parentWidget.addState("attempted")
			}
		}

		onPause = () => {
			this.removeState("playing");
		}

		onEnded = () => {
			this.audio.pause(); // IE11 fix
			this.removeState("playing");
			this.audio.currentTime = 0;
			this.onTimeupdate();
		}

		onTimeupdate = () => {
			let audio = this.audio,
				currentTime = audio.currentTime,
				duration = audio.duration,
				percent = Math.floor(currentTime * 100 / duration);

			if (this.audioType !== AudioType.AUTO) {
				this.markable.value = String(percent || 0);
				this.mark();
			}
			if (this.fill) {
				this.element.style.backgroundImage = "linear-gradient(90deg, " + this.fill + " " + percent + "%, transparent " + percent + "%)"
			}
		}

		setupListeners() {
			let audio = this.audio;

			if (audio && (this.audioType === AudioType.PLAY || this.audioType === AudioType.AUTO)) {
				audio.addEventListener("playing", this.onPlaying);
				audio.addEventListener("pause", this.onPause);
				audio.addEventListener("ended", this.onEnded);
				audio.addEventListener("timeupdate", this.onTimeupdate);
				this.onTimeupdate();
			}
		}

		onClick(event?: MouseEvent | TouchEvent) {
			if (((event as TouchEvent).changedTouches || (event as MouseEvent).which < 2) && !this.hasState("disabled")) {
				let audio = this.audio;

				if (audio) {
					if (this.audioType === AudioType.PLAY || this.audioType === AudioType.AUTO) {
						if (this.hasState("playing")) {
							audio.pause();
						} else {
							audio.play();
						}
					} else if (this.audioType === AudioType.RESET) {
						audio.pause();
						audio.currentTime = 0;
					}
				}
			}
			super.onClick(event);
		}

		onScreen(screen: ScreenWidget) {
			let audio = this.audio;

			if (audio && (this.audioType === AudioType.PLAY || this.audioType === AudioType.AUTO)) {
				audio.pause();
			}
			if (this.findAudio) {
				audio = screen.element.getElementsByTagName("audio")[0];
				this.toggleState(["disabled", "empty"], !audio);
				if (audio) {
					this.audio = audio;
					this.setupListeners();
				}
			}
		}

		onTimeout() {
			if (this.audio && (this.audioType === AudioType.PLAY || this.audioType === AudioType.AUTO)) {
				this.audio.pause();
			}
		}
	}
};
