///<reference path="widget.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	let selector = "ic-pins";

	/**
	 * Version of PinsWidget
	 */
	version[selector] = 1;

	export class PinsWidget extends Widget {
		static selector = selector;
	}
};
