///<reference path="widget.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	let selector = "ic-anchor";

	/**
	 * Version of AnchorWidget
	 */
	version[selector] = 1;

	export class AnchorWidget extends Widget implements OnState {
		static selector = selector;

		relativeWidget: ActivityWidget | ActivitiesWidget | ScreenWidget;

		constructor(element: HTMLElement) {
			super(element);

			if (this.screenWidget && this.get(this.screenWidget, AnchorWidget).length === 1) {
				this.relativeWidget = this.screenWidget;
			} else if (this.activitiesWidget && this.get(this.activitiesWidget, AnchorWidget).length === 1) {
				this.relativeWidget = this.activitiesWidget;
			} else if (this.activityWidget && this.get(this.activityWidget, AnchorWidget).length === 1) {
				this.relativeWidget = this.activityWidget;
			}
			if (this.relativeWidget) {
				this.index = this.get(AnchorWidget, true).indexOf(this) + 1;
				element.setAttribute("data-anchor", element.getAttribute(AnchorWidget.selector) || String(this.index));
				this.on(".state");
			} else {
				this.index = 0;
				element.removeAttribute("data-anchor");
			}
		}

		onState(widget: Widget, stateList: WidgetState[]) {
			if (widget === this) {
				if (stateList.includes("active") && this.hasState("active")) {
					this.relativeWidget.addState("active");
				}
			} else if (widget === this.relativeWidget) {
				this.toggleState({
					"active": this.relativeWidget.hasState("active"),
					"answered": this.relativeWidget.hasState("answered"),
					"attempted": this.relativeWidget.hasState("attempted")
				});
			}
		}
	}
};
