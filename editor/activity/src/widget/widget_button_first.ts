///<reference path="widget_button.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	export class FirstButtonWidget extends ButtonWidget implements OnState {
		static selector = "ic-button[ic-button=first]";

		constructor(element: HTMLElement) {
			super(element);

			this.on(".state");
		}

		onClick(event: MouseEvent | TouchEvent) {
			if (!this.isDisabled(event)) {
				let anchor = this.get(AnchorWidget).first();

				if (anchor) {
					anchor.parentWidget.addState("active");
				}
			}
			super.onClick(event);
		}

		onState(widget: Widget, stateList: WidgetState[]) {
			if (widget instanceof ActivityWidget && stateList.includes("active")) {
				let anchor = this.get(AnchorWidget).first();

				this.toggleState("disabled", anchor ? anchor.hasState("active") : false);
			}
		}
	}
};
