///<reference path="widget_input.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	let selector = "ic-text";

	/**
	 * Version of TextWidget
	 */
	version[selector] = 1;

	export class TextWidget extends InputWidget implements OnPersist, OnBlur, OnFocus, OnKeyPress, OnKeyUp, OnMouseDown, OnReset, OnReveal, OnState {
		static selector = selector;

		input: HTMLInputElement;
		textarea: HTMLTextAreaElement;
		rx: RegExp;
		accept: RegExp;
		startText: string;

		constructor(element: HTMLElement) {
			super(element);

			let i,
				child: HTMLElement,
				children = element.childNodes,
				mark = this.markable;

			mark.max = 1;
			if (isUndefined(mark.points)) {
				mark.points = 1;
			}
			for (i = 0; i < children.length && !this.input && !this.textarea; i++) {
				child = children[i] as HTMLElement;
				if (child.nodeType === Node.ELEMENT_NODE) {
					switch (child.tagName) {
						case "INPUT":
							this.input = child as HTMLInputElement;
							break;

						case "TEXTAREA":
							this.textarea = child as HTMLTextAreaElement;
							break;
					}
				}
			}
			if (!this.input && !this.textarea) {
				for (let parent = this.element.parentElement; parent && parent.icWidget !== this.parentWidget; parent = parent.parentElement) {
					if (parent.tagName === "P") {
						this.input = createElement("input") as HTMLInputElement;
						this.input.setAttribute("type", "text");
						appendChild(this.element, this.input);
						break;
					}
				}
				if (!this.input) {
					this.textarea = createElement("textarea") as HTMLTextAreaElement;
					appendChild(this.element, this.textarea);
				}
			}
			if (this.element.hasAttribute("data-accept")) {
				this.accept = new RegExp(this.element.getAttribute("data-accept"));
			}
			if (this.element.hasAttribute("data-rx")) {
				this.rx = new RegExp(this.element.getAttribute("data-rx"));
			}
			let input = this.input ? this.input : this.textarea;

			this.startText = mark.value = input.value;
			this.toggleState("empty", !mark.value);
			if (input.hasAttribute("disabled")) {
				this.addState("disabled");
			}
			// iOS
			if (!input.hasAttribute("autocapitalize")) {
				input.setAttribute("autocapitalize", this.input ? "none" : "sentences");
			}
			// iOS
			if (!input.hasAttribute("autocomplete")) {
				input.setAttribute("autocomplete", "off");
			}
			// iOS
			if (!input.hasAttribute("autocorrect")) {
				input.setAttribute("autocorrect", "off");
			}
			input.addEventListener("change", (event: KeyboardEvent) => {
				this.onKeyUp(event);
			});
			input.addEventListener("paste", function(event: KeyboardEvent) {
				event.preventDefault();
				return false;
			}, true);
			this.on(["mousedown", "touchstart", ".persist", ".state", ".reset", ".reveal"])
				.on(["focus", "blur", "keypress", "keyup"], input);
		}

		get hasAnswered(): boolean {
			return !!this.markable.value;
		}

		onPersist(state?: string): string {
			if (isUndefined(state)) {
				return this.markable.value as string || "";
			}
			(this.input || this.textarea).value = this.markable.value = state || "";
			this.toggleState("empty", !this.markable.value);
		}

		onReset(screen: ScreenWidget) {
			if ((!screen || this.screenWidget === screen)) {
				let input = this.input || this.textarea;

				super.onReset(screen);
				input.value = this.startText;
				this.mark();
			}
		}

		onReveal(screen: ScreenWidget) {
			if ((!screen || this.screenWidget === screen) && !this.hasState("reveal")) {
				let usedAnswers: (string | RegExp)[] = this.activityWidget.allowDuplicates ? undefined : [];

				this.get(this.closestWidget, TextWidget, this.group, true).forEach(widget => {
					if (!widget.hasState("reveal")) {
						let input = widget.input || widget.textarea,
							answers = widget.expandedAnswers.not(usedAnswers),
							answer = answers.find(answer => {return isString(answer);}) as string || answers[0] || "";

						widget.markable.value = input.value = isString(answer) ? answer : (answer as RegExp).source;
						usedAnswers.pushOnce(answer);
						widget.addState(["disabled", "reveal"])
							.mark();
					}
				});
			}
		}

		onMouseDown(event: MouseEvent | TouchEvent) {
			if (!this.isDisabled(event)
				&& document.activeElement !== (this.input || this.textarea)) {
				//console.log("focus", this.input || this.textarea);
				this.activate();
				(this.input || this.textarea).focus();
				event.preventDefault();
			}
		}

		onKeyPress(event: KeyboardEvent) {
			if (this.accept || this.rx) {
				let key = event.key,
					input = (event.target as HTMLInputElement),
					value = input.value;

				if (key[0].toLowerCase() === key[0]
					&& ((this.accept && !this.accept.test(key))
						|| (this.rx && !this.rx.test(value.substring(0, input.selectionStart) + key + value.substring(input.selectionEnd))))) {
					event.preventDefault();
					return false;
				}
			}
		}

		onKeyUp(event?: KeyboardEvent) {
			let value = (this.input || this.textarea).value.trim();

			if (this.markable.value !== value) {
				this.markable.value = value;
				this.toggleState("empty", !value);
				if (this.parentWidget) {
					this.parentWidget.addState("attempted");
				}
				this.mark();
			}
		}

		onBlur() {
			this.removeState("focus");
		}

		onFocus() {
			this.addState(["active", "focus"])
				.parentWidget.addState("active");
		}

		onState(widget: Widget, stateList: WidgetState[]) {
			if (widget === this && stateList.includes("disabled")) {
				(this.input || this.textarea).disabled = this.hasState("disabled");
			}
		}
	}
};
