///<reference path="widget_button_state.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	export class ResetButtonWidget extends StateButtonWidget {
		static selector = "ic-button[ic-button=reset]";

		/**
		 * Only enable after the submit button has been pressed, otherwise will
		 * enable when something has changed.
		 */
		afterSubmit: boolean;
		/**
		 * Only enable until this number of attempts, then permanently disable.
		 */
		maxAttempts: number;

		constructor(element: HTMLElement) {
			super(element);

			if (element.getAttribute("ic-reset") === "submit") {
				this.afterSubmit = true;
			}
			this.maxAttempts = parseInt(element.getAttribute("ic-attempts") || 0 as any, 10);
		}

		onClick(event: MouseEvent | TouchEvent) {
			if (!this.isDisabled(event)) {
				let screen = this.screenWidget;

				(screen || this.rootWidget).removeState(["marked", "reveal"]);
				Widget.trigger(".reset", screen);
			}
			super.onClick(event);
		}

		checkState = () => {
			let disabled = true,
				closestWidget = this.closestWidget;

			if ((!this.maxAttempts || closestWidget.attempts < this.maxAttempts)
				&& (!this.afterSubmit || closestWidget.hasState(["marked"]))) {
				let widgets = this.get(closestWidget, InputWidget).filter(widget => widget.isScored);

				if (widgets.length) {
					disabled = !widgets.some(widget => widget.hasAnswered);
				}
			}
			this.toggleState("disabled", disabled);
		}
	}
};
