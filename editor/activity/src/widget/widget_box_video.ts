///<reference path="widget.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	let selector = "ic-video";

	/**
	 * Version of VideoWidget
	 */
	version[selector] = 1;

	export class VideoWidget extends BoxWidget {
		static selector = selector;
	}
};
