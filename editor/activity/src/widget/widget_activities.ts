///<reference path="widget_activity.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	let selector = "ic-activities";

	/**
	 * Version of ActivitiesWidget
	 */
	version[selector] = 1;

	export class ActivitiesWidget extends ActivityWidget {
		/**
		 * Input Widgets get an index to help with identifying them.
		 */
		static isInputWidget = true;

		static selector = selector;
	}
};
