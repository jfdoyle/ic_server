///<reference path="widget_button_state.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	export class RevealButtonWidget extends StateButtonWidget {
		static selector = "ic-button[ic-button=reveal]";

		/**
		 * Only enable after the submit button has been pressed, otherwise will
		 * enable when something has changed.
		 */
		afterSubmit: boolean;
		/**
		 * Only enable after this number of attempts.
		 */
		maxAttempts: number;

		constructor(element: HTMLElement) {
			super(element);

			if (element.getAttribute("ic-reveal") === "submit") {
				this.afterSubmit = true;
			}
			this.maxAttempts = parseInt(element.getAttribute("ic-attempts") || 0 as any, 10);
		}

		onClick(event: MouseEvent | TouchEvent) {
			if (!this.isDisabled(event)) {
				let screen = this.screenWidget,
					root = screen || this.rootWidget;

				if (root && !root.hasState("reveal")) {
					root.toggleState({
						"marked": false,
						"reveal": true
					});
					Widget.trigger(".reveal", screen);
				}
			}
			super.onClick(event);
		}

		checkState = () => {
			let disabled = true,
				closestWidget = this.closestWidget;

			if ((!this.maxAttempts || closestWidget.attempts >= this.maxAttempts)
				&& (!this.afterSubmit || closestWidget.hasState(["marked"]))
				&& (!closestWidget.hasState(["reveal"]))) {
				disabled = false;
				//				let widgets = this.get(this.closestWidget, InputWidget).filter(widget => widget.isScored);
				//
				//				if (widgets.length) {
				//					disabled = !widgets.some(widget => widget.hasAnswered);
				//				}
			}
			this.toggleState("disabled", disabled);
		}
	}
};
