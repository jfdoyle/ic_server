///<reference path="widget_input_button.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	let selector = "ic-draggable";

	/**
	 * Version of DraggableWidget
	 */
	version[selector] = 1;

	export interface DragHelper {
		element: HTMLElement;
		getHelper(event?: MouseEvent | TouchEvent, data?: DragData, within?: ClientRect): HTMLElement;
		onHelperMove(event?: MouseEvent | TouchEvent, data?: DragData, within?: ClientRect): void;
		freeHelper(event?: MouseEvent | TouchEvent, drag?: DraggableWidget, drop?: DroppableWidget): void;
	}

	export interface DragData {
		drag: DraggableWidget,
		clientY: number;
		clientX: number;
		top: number; // set automatically
		left: number; // set automatically
		height: number; // set automatically
		width: number; // set automatically
		offsetY: number;
		offsetX: number;
		offsetTop?: number;
		offsetLeft?: number;
		target?: HTMLElement;
	}

	export interface eventPosition {
		clientX: number;
		clientY: number;
	}

	export class DraggableWidget extends InputButtonWidget implements DragHelper, OnMouseDown, AllMouseMove, AllMouseUp, OnReset, OnReveal {
		static selector = selector;

		// Dragging
		private targets: DroppableWidget[];
		private drop: DroppableWidget;
		// Autoscroll while dragging
		private scrollElement: HTMLElement;
		private autoLeftDelta: number;
		private autoLeftCount = 0;
		private autoTopDelta: number;
		private autoTopCount = 0;
		// Helper
		private dragData: DragData;
		protected helper: DragHelper;
		// Droppable we live in
		public droppables: DroppableWidget[] = [];
		public startDroppables: DroppableWidget[];

		public hasPins: boolean;
		public pins: PinWidget[];
		private usePin: PinWidget;
		private lastPin: PinWidget;

		public isCloneable: boolean;
		public cloneElement: HTMLElement;
		public clones: HTMLElement[] = [];
		public originalElement: HTMLElement;

		public isReset: boolean;

		public startLeft: string;
		public startTop: string;

		public preLoaded: DroppableWidget[] = [];

		private icOnMoveFn: string;
		private icOnEndFn: string;

		constructor(element: HTMLElement) {
			super(element);

			this.originalElement = element;
			if (querySelector(element, ":scope>ic-pins>ic-pin")) {
				// NOTE: We know that this is pins, but need to not set yet to force errors if accessing this.pins before startup()
				this.hasPins = true;
			} else {
				switch (element.getAttribute(DraggableWidget.selector)) {
					case "copy":
						this.isCloneable = true;
						break;
				}
			}
			this.isReset = !element.hasAttribute(selector + "-sticky");
			this.icOnMoveFn = element.getAttribute("onMove");
			this.icOnEndFn = element.getAttribute("onEnd");
		}

		startup() {
			super.startup();
			let mark = this.markable;

			if (!mark.value) {
				mark.value = this.element.textContent.replace(/[^\x20-\x7E]+/g, "").trim() || ("drag" + this.index);
			}
			if (this.hasPins) {
				this.getPins();
			} else {
				let dropElement = this.element.closest(DroppableWidget.selector);

				if (dropElement) {
					let drop = dropElement.icWidget as DroppableWidget;

					this.droppables.pushOnce(drop);
					drop.draggables.pushOnce(this);
					drop.startDraggables = drop.draggables.clone();
					drop.update();
				}
			}
			this.startDroppables = this.droppables.clone();
			this.on([".reset", ".reveal"]);
		}

		get isScored() {
			return false;
		}

		private _droppableWidget: DroppableWidget;
		/**
		 * Get the closest TreeWidget ancestor
		 */
		get droppableWidget(): DroppableWidget {
			let widget = this._droppableWidget;

			if (widget === undefined) {
				let element = this.element.closest(DroppableWidget.selector);

				this._droppableWidget = widget = element ? element.icWidget as DroppableWidget : null;
			}
			return widget;
		}

		invalidate() {
			delete this._droppableWidget;
			super.invalidate();
		}

		includes(drop: DroppableWidget): boolean {
			return this.droppables.includes(drop);
		}

		private fixElement(event: MouseEvent | TouchEvent) {
			this.element = (event.target as Element).closest(DraggableWidget.selector) as HTMLElement || this.originalElement;
		}

		onMouseEnter(event: MouseEvent | TouchEvent) {
			this.fixElement(event);
			super.onMouseEnter(event);
		}

		onMouseDown(event: MouseEvent | TouchEvent) {
			this.fixElement(event);
			super.onMouseDown(event);

			let [clientX, clientY] = getCoords(event);

			if (!this.isDisabled(event)) {
				let style = this.element.style;

				this.dragData = {
					drag: this,
					clientY: clientY,
					clientX: clientX,
					top: 0,
					left: 0,
					height: 0,
					width: 0,
					offsetY: 0,
					offsetX: 0,
					target: (event.target as Element).closest(DraggableWidget.selector) as HTMLElement
				};
				this.on(["!mousemove", "!touchmove", "!mouseup", "!touchend"]);
				this.startLeft = style.left;
				this.startTop = style.top;
			}
		}

		onReset(screen: ScreenWidget) {
			if ((!screen || this.screenWidget === screen)) {
				this.removeState(["disabled", "reveal"]);
			}
		}

		onReveal(screen: ScreenWidget) {
			if ((!screen || this.screenWidget === screen)) {
				this.addState(["disabled", "reveal"]);
			}
		}

		getHelper(event: MouseEvent | TouchEvent, data: DragData, within: ClientRect): HTMLElement {
			if (this.isCloneable) {
				let clone: HTMLElement,
					original = this.originalElement,
					target = data.target;

				if (!target || original === target) {
					clone = original.cloneNode(true) as HTMLElement;

					clone.icWidget = this;
					original.parentElement.insertBefore(clone, original);
					this.removeState(["click", "hover", "dragging"]);
					this.element = clone;
				} else {
					clone = target;
				}
				this.cloneElement = clone;
			}
			let el = this.cloneElement || this.originalElement,
				style = el.style,
				rect = getBoundingClientRect(el);

			el.classList.add("drag-helper");
			style.top = style.left = "0px";

			data.offsetY = data.clientY > rect.bottom ? rect.height / 2 : data.clientY - rect.top;
			data.offsetX = data.clientX > rect.right ? rect.width / 2 : data.clientX - rect.left;
			data.height = rect.height;
			data.width = rect.width;
			return el;
		}

		onHelperMove(event: MouseEvent | TouchEvent, data: DragData, within: ClientRect) {
			let el = this.cloneElement || this.originalElement,
				style = el.style,
				[clientX, clientY] = getCoords(event),
				left = (Math.range(within.left - data.left, clientX - data.offsetX, within.right - data.width - data.left) + (data.offsetLeft || 0)) / scaleFactor,
				top = (Math.range(within.top - data.top, clientY - data.offsetY, within.bottom - data.height - data.top) + (data.offsetTop || 0)) / scaleFactor;

			style.top = top + "px";
			style.left = left + "px";
			this.callUserFunc(this.icOnMoveFn, left, top);
		}

		freeHelper(event?: MouseEvent | TouchEvent, drag?: DraggableWidget, drop?: DroppableWidget) {
			let el = this.cloneElement || this.originalElement,
				style = el.style

			el.classList.remove("drag-helper");
			if (this.isCloneable) {
				this.cloneElement = null;
				if ((drop ? drop.element : el.parentElement) === this.originalElement.parentElement) {
					el.parentElement.removeChild(el);
				} else if (!drop) {
					// TODO: Animate back to 0
					style.top = style.left = "";
				} else {
					this.clones.pushOnce(el);
					this.on(["mouseenter", "mouseleave", "mousedown", "touchstart", "mouseup", "touchend"], el);
					style.position = "";
					this.fixState();
				}
				this.element = this.originalElement;
			} else if (!drop) {
				// TODO: Animate back to 0
				style.top = style.left = "";
			}
			let box = el.getBoundingClientRect();

			this.callUserFunc(this.icOnEndFn, box.left, box.top);
		}

		getParentRect(): ClientRect {
			// TODO: change where the dragging gets bound
			return getBoundingClientRect(this.screenWidget);
		}

		getPins() {
			if (this.hasPins && !this.pins) {
				this.pins = this.get(this, PinWidget).filter((pin) => {return pin.parent === this;});
			}
		}

		findPin(drop: DroppableWidget) {
			this.getPins();
			return this.pins.find((pin) => {return pin.target && pin.target.parent === drop;});
		}

		getPin(event?: MouseEvent | TouchEvent) {
			this.getPins();
			let [clientX, clientY] = getCoords(event),
				available = this.pins.filter((pin) => {return !pin.target || (!this.preLoaded.includes(pin.drop) && !pin.drop.hasState("disabled"));}),
				underMouse = (event && available.filter(function(pin) {return pin.inRect(clientX, clientY) || (pin.target && pin.target.inRect(clientX, clientY));}));

			if (underMouse && underMouse.length) {
				available = underMouse;
			}
			let index = available.indexOf(this.lastPin) + 1;

			return this.lastPin = this.usePin
				|| available.find((pin) => {return !pin.target;})
				|| available[index >= available.length ? 0 : index];
		}

		allMouseMove(event: MouseEvent | TouchEvent) {
			super.allMouseMove(event);

			let dragData = this.dragData,
				[clientX, clientY] = getCoords(event, false),
				targets = this.targets;

			if (!targets
				&& (Math.abs(dragData.clientY - clientY) > 5
					|| Math.abs(dragData.clientX - clientX) > 5)) {

				this.targets = targets = this.get(this.screenWidget, DroppableWidget, this.group || null).filter((drop) => {
					if (this.activitiesWidget === drop.activitiesWidget || this.activityWidget === drop.activityWidget) {
						return drop.canAccept(this);
					}
				});
				targets.forEach((widget) => {
					if (widget.dropShapes) {
						widget.dropShapes.forEach((el) => {
							el.classList.add("drop-helper");
						});
					}
				});
				this.addState("dragging");
				if (this.hasPins) {
					this.usePin = this.helper = this.getPin(event);
				}
				if (!this.helper) {
					this.helper = this;
				}
				this.helper.getHelper(event, dragData, this.getParentRect());
				// console.log("start dragging", event, this.helper, dragData, this.getParentRect())
			}
			if (targets) {
				let underMouse = document.elementFromPoint(clientX, clientY);

				if (!underMouse) {
					return
				}
				let closestDrop = underMouse.closest(".drop") as Element;

				this.helper.onHelperMove(event, dragData, this.getParentRect());
				this.drop = null;
				targets.forEach((widget) => {
					let over = widget.dropShapes
						? widget.dropShapes.includes(closestDrop)
						: widget.inRect(clientX, clientY);

					if (over) {
						this.drop = widget;
						//console.log("Over", event, widget)
					}
					widget.onDragHover(this, over, event);
				});

				// Autoscroll anything that can be scrolled by hovering near the edge of it
				let autoscroll = document.elementFromPoint(clientX, clientY) as HTMLElement;

				for (; autoscroll; autoscroll = autoscroll.parentElement) {
					let width = autoscroll.scrollWidth > autoscroll.clientWidth,
						height = autoscroll.scrollHeight > autoscroll.clientHeight;

					if (width || height) {
						let style = getComputedStyle(autoscroll),
							scrollX = width && /(auto|scroll)/i.test(style.overflowX),
							scrollY = height && /(auto|scroll)/i.test(style.overflowY);

						if (scrollX || scrollY) {
							let rect = getBoundingClientRect(autoscroll),
								fontSize = parseFloat(style.fontSize) / scaleFactor,
								left = !scrollX
									? 0
									: clientX < rect.left + fontSize && autoscroll.scrollLeft > 0
										? -1
										: clientX > rect.right - fontSize && autoscroll.scrollLeft < autoscroll.scrollWidth - autoscroll.clientWidth
											? 1
											: 0,
								top = !scrollY
									? 0
									: clientY < rect.top + fontSize && autoscroll.scrollTop > 0
										? -1
										: clientY > rect.bottom - fontSize && autoscroll.scrollTop < autoscroll.scrollHeight - autoscroll.clientHeight
											? 1
											: 0;

							if (left || top) {
								this.autoLeftDelta = left;
								this.autoTopDelta = top;
								rAF("autoscroll", this.autoscroll);
								break;
							}
						}
					}
				}
				if (this.scrollElement !== autoscroll) {
					this.scrollElement = autoscroll;
					this.autoLeftCount = this.autoTopCount = 0;
				}

				event.preventDefault();
				return false;
			}
		}

		autoscroll = () => {
			let el = this.scrollElement;

			if (el) {
				let oldLeft = el.scrollLeft,
					oldTop = el.scrollTop;

				el.scrollLeft += this.autoLeftDelta * (1 + Math.min(4, ++this.autoLeftCount * 0.1));
				el.scrollTop += this.autoTopDelta * (1 + Math.min(4, ++this.autoTopCount * 0.1));
				if (oldLeft !== el.scrollLeft || oldTop !== el.scrollTop) {
					rAF("autoscroll", this.autoscroll);
				}
			}
		}

		allMouseUp(event: MouseEvent | TouchEvent) {
			super.allMouseUp(event);
			this.scrollElement = null;
			this.off("!mousemove", "!touchmove", "!mouseup", "!touchend");
			if (this.targets) {
				let drop = this.drop,
					helper = this.helper;

				drop = drop && drop.onDragDrop(this, helper, event) ? drop : null;
				// Free the helper, pass the drop if it accepts the drag
				helper.freeHelper(event, this, drop);
				// Let all the drops know they won't be needed any more
				this.targets.forEach((widget) => {
					if (widget.dropShapes) {
						widget.dropShapes.forEach((el) => {
							el.classList.remove("drop-helper");
						});
					}
					widget.onDragEnd(this, event, widget);
				});
				// We're not dragging any more
				this.removeState("dragging");
				if (helper !== this) {
					this.removeState("hover");
				}
				// Free cache so the GC can clean up
				this.targets = this.helper = this.drop = this.usePin = null;
				// Handle resetting when dropping elsewhere or on source
				if (!drop && this.droppables.length === 1) {
					if (this.isReset || ((document.elementFromPoint.apply(document, getCoords(event, false)) as Element) || document.body).parentElements(this.originalElement, true).pop() === this.originalElement) {
						// TODO: Add more resets, including removing clones
						if (this.pins) {
							drop = this.droppables[0];
							this.droppables.remove(drop);
							drop.draggables.remove(this);
							(helper as PinWidget).unlinkPin();
						} else {
							let style = this.element.style;

							style.left = this.startLeft;
							style.top = this.startTop;
						}
					}
				}
			}
		}

		mark() {
			return;
		}
	}
};
