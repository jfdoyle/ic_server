/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	/**
	 * Data is stored either on the widget, or on it's parent activity widget.
	 * In build it should be stored on the activity widget due to obfuscation of larger data sets.
	 */
	export interface JsonData {
		// SCORING
		/**
		 * Number of correct answers in this widget
		 */
		raw?: number;
		/**
		 * Minimum number of answers this widget can be worth
		 */
		min?: number;
		/**
		 * Maximum number of answers this widget can be worth
		 */
		max?: number;
		/**
		 * Scaled score between min and max (0 <= ((score - min) / max) <= 1)
		 */
		scaled?: number;
		/**
		 * Number of points this widget can be worth, if not set then use the raw score
		 */
		points?: number | string;
		/**
		 * Number of points this widget is worth, if not set then use the raw score
		 */
		maxPoints?: number;
		/**
		 * Number of points this widget is actually worth
		 */
		score?: number;
		/**
		 * Round the points down (lose any trailing digits)
		 */
		round?: boolean;

		// MARKING
		/**
		 * To be used to identify this specific question
		 */
		id?: string;
		/**
		 * One or more answers to check for correctness
		 */
		value?: string | string[];
		/**
		 * One or more correct answers permissable
		 */
		answer?: string | string[];
		/**
		 * Correct answer string when answer is correct. Used for matching
		 * for duplicates
		 */
		correctAnswers?: string[];
		/**
		 * If the order of the answers -> correct answers is important
		 */
		order?: boolean;
		/**
		 * If the answers are case insensitive
		 */
		nocase?: boolean;
		/**
		 * Number of child widgets
		 */
		total?: number;
		/**
		 * Number of child widgets that have scaled > 0
		 */
		correct?: number;

		// TIMER
		/**
		 * Number of minutes as a countdown instead of simple timer
		 */
		minutes?: number;
		/**
		 * Number of minutes to give a warning at
		 */
		warning?: number;

		// DUPLICATE ANSWERS - ACTIVITY ONLY
		/**
		 * Allow identical duplicate answers
		 */
		duplicates?: boolean;
	};
};
