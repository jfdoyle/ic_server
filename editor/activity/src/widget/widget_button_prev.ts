///<reference path="widget_button.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	export class PrevButtonWidget extends ButtonWidget implements OnState {
		static selector = "ic-button[ic-button=prev]";

		isWait: boolean;
		needWidgetType: any;

		constructor(element: HTMLElement) {
			super(element);

			switch (element.getAttribute("ic-prev")) {
				case "anchor":
					this.needWidgetType = AnchorWidget;
					break;

				default:
				case "screen":
					this.needWidgetType = ScreenWidget;
					break;
			}
		}

		startup() {
			super.startup();
			if (this.hasAttribute("ic-wait") && this.get(this.closestWidget, InputWidget).find(widget => widget.isScored)) {
				this.isWait = true;
				this.addState("disabled")
					.on([".submit", ".reset"]);
			} else {
				let widget = this.get(this.needWidgetType).filter(widget => !widget.isDisabled()).first();

				this.toggleState("disabled", widget ? widget.hasState("active") : false)
					.on(".state");
			}
		}

		onClick(event: MouseEvent | TouchEvent) {
			if (!this.isDisabled(event)) {
				let widget = this.get(this.needWidgetType).filter(widget => !widget.isDisabled()),
					index = widget.findIndex(widget => widget.hasState("active"));

				if (index > 0) {
					widget[index - 1].addState("active");
				}
			}
			super.onClick(event);
		}

		onReset(screen: ScreenWidget) {
			if (!screen || this.screenWidget === screen) {
				this.addState("disabled");
			}
		}

		onSubmit(screen: ScreenWidget) {
			if (!screen || this.screenWidget === screen) {
				this.toggleState("disabled", this.get(this.needWidgetType).filter(widget => !widget.isDisabled()).first().hasState("active"));
			}
		}

		onState(widget: Widget, stateList: WidgetState[]) {
			if (widget instanceof ScreenWidget && stateList.includes("active")) {
				let widget = this.get(this.needWidgetType).filter(widget => !widget.isDisabled()).first();

				this.toggleState("disabled", widget ? widget.hasState("active") : false);
			}
		}
	}
};
