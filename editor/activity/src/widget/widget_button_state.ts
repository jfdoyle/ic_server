///<reference path="widget_button.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	export abstract class StateButtonWidget extends ButtonWidget implements OnScreen, OnState {
		static selector: string = undefined;

		startup() {
			super.startup();

			this.checkState();
			this.on([".screen", ".state"]);
		}

		abstract checkState: () => void;

		onScreen(screen: ScreenWidget) {
			if (screen === this.screenWidget) {
				this.checkState();
			}
		}

		onState(widget: Widget, stateList: WidgetState[]) {
			let screen = this.screenWidget;

			if ((!screen || widget.screenWidget === screen)
				&& ((widget instanceof InputWidget && widget.isScored)
					|| (widget === screen && (stateList.includes("marked") || stateList.includes("reveal"))))) {
				setImmediate(this.checkState);
			}
		}
	}
};
