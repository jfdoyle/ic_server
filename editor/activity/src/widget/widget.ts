///<reference path="json.ts" />
///<reference path="node.ts" />
///<reference path="../startup.ts" />
/**
 * Copyright &copy; 2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

namespace ic {
	/**
	 * Version of Widget
	 */
	version["widget"] = 1;

	/**
	 * Are we in the editor?
	 */
	export const inPreview = /^about:/.test(location as any);
	export const inEditor = inPreview && (window as any).ic_edit;

	// Gets started at the end of Widget.startup()
	function frameHandler() {
		Widget.trigger(".frame");
		rAF(frameHandler);
	}

	// Gets started at the end of Widget.startup()
	function cssLoadHandler(event: UIEvent) {
		Widget.trigger(".css", event);
	}

	export type WidgetState = "active"
		| "alt"
		| "answered"
		| "attempted"
		| "click"
		| "checked"
		| "disabled"
		| "dragging"
		| "empty"
		| "focus"
		| "hidden"
		| "hover"
		| "marked"
		| "open"
		| "playing"
		| "reveal"
		| "visible"
		| "visited"
		| "warning";

	/**
	 * These states are persisted when saving the blob.
	 */
	export const PersistState: WidgetState[] = ["attempted", "disabled", "marked", "reveal", "visited"];

	/**
	 * These states are cleared when resetting a screen.
	 */
	export const TemporaryState: WidgetState[] = ["active", "answered", "attempted", "click", "dragging", "focus", "hover", "playing", "visited"];

	/**
	 * This is a list of persist datatypes that need to be mapped to single
	 * characters when saving or loading state.
	 */
	function persistMap(name: string): string {
		switch (name) {
			case "activity": return "a";
			case "attempted": return "b";
			case "attempts": return "c";
			case "disabled": return "d";
			case "drop": return "e";
			case "marked": return "f";
			case "reveal": return "g";
			case "screen": return "h";
			case "select": return "m";
			case "text": return "i";
			case "timer": return "j";
			case "toggle": return "k";
			case "visited": return "l";
			// LAST: select - "m"
		}
		console.warn("Unknown map type:", name);
		return name;
	};

	export abstract class Widget extends WidgetNode implements OnMouseEnter, OnMouseLeave {
		// Globals reset on startup
		public static widgets: Widget[] = [];
		private static globalEvents: Widget[] = [];
		private static eventList: {[event: string]: number} = {};
		private static roundSize: Widget[] = [];

		static created: boolean;
		static started: boolean;

		@final
		public static root: ScreensWidget;

		public api_pause: boolean;
		public lazy_pause: boolean;

		// static selector: string; // set in all subclasses, not set in here

		private _persist: string[];

		public lockedState: {[state: string]: boolean};

		public group: number = 0;
		public data: JsonData = {};
		public state: {[state: string]: boolean} = {};

		public isRandom: boolean;

		public events: {[event: string]: Function} = {};

		constructor(element: HTMLElement) {
			super(element);

			Widget.widgets.pushOnce(this);
			// console.log("new widget", element)
			let data = element.getAttribute("data-json");

			if (data) {
				try {
					if (data[0] !== "{") {
						data = data.decompress("uri");
						element.removeAttribute("data-json");
					}
					this.data = JSON.parse(data);
				} catch (e) {
					console.warn("Error: Bad Json", element, element.getAttribute("data-json"));
				}
			} else if (this.realIndex) {
				for (let path = element.tagName.substring(3).toLowerCase() + "." + this.realIndex,
					parentWidget = this.parentWidget; parentWidget; parentWidget = parentWidget.parentWidget) {

					if (parentWidget.data) {
						data = (parentWidget.data as {}).getTree(path);
						if (data) {
							this.data = data as any;
							(parentWidget.data as {}).setTree(path, undefined);
						}
						break;
					}
					path = parentWidget.element.tagName.substring(3).toLowerCase() + "." + parentWidget.realIndex + "." + path;
				}
			}
			for (let parent = element; parent; parent = parent.parentElement) {
				if (parent.hasAttribute("ic-group")) {
					this.group = parseInt(parent.getAttribute("ic-group"));
					break;
				}
			}
			if (element.hasAttribute("ic-random")) {
				this.isRandom = true;
			}
			if (element.hasAttribute("ic-rounding")) {
				Widget.roundSize.push(this);
			}
			if (element.hasAttribute("ic-state")) {
				// TODO: Error checking for correct state strings?
				let states = element.getAttribute("ic-state").split(/[\s\+,]/);

				this.lockedState = {};
				states.forEach((state) => {
					this.addState(state as WidgetState);
					this.lockedState[state] = true;
				});
			}

			this.on(["mouseenter", "mouseleave"]);
			return this;
		}

		public get selector(): string {
			return (this.constructor as any).selector;
		}

		public set index(index: number) {
			if (this._index !== index) {
				this._index = index;
				this.fixState();
			}
		}

		public get index(): number { // Identical to node.ts, but can't inherit
			return this._index;
		}

		isDisabled(event?: MouseEvent | TouchEvent) {
			return this.hasState("disabled") || (event && ((event as MouseEvent).which || 0) > 1);
		}

		onMouseEnter(event: MouseEvent | TouchEvent) {
			if (!this.hasState("disabled")) {
				this.toggleState("hover", true);
			}
		}

		onMouseLeave(event: MouseEvent | TouchEvent) {
			this.toggleState("hover", false);
		}

		activate(): this {
			if (this.screenWidget) {
				let parents = this.parentWidgets;

				this.get(this.screenWidget, "active", false).not(parents).forEach(function(widget: Widget) {
					if (!(widget instanceof AnchorWidget)) {
						widget.removeState("active");
					}
				});
				parents.forEach(function(widget) {
					widget.addState("active");
				});
			}
			return this;
		}

		/**
		 * Get the bounding box of this element
		 */
		getBoundingClientRect(): ClientRect {
			return getBoundingClientRect(this.element);
		}

		/**
		 * Check if these coordinates are within the bounding box
		 */
		inRect(left: number, top: number): boolean {
			let rect = getBoundingClientRect(this);

			return rect.top <= top && rect.bottom >= top && rect.left <= left && rect.right >= left;
		}

		/**
		 * Check if this widget has a certain state
		 */
		hasState(state: WidgetState | WidgetState[]): boolean {
			if (isArray(state)) {
				return state.some((name) => {
					return !!this.state[name as string];
				});
			}
			return !!this.state[state as string];
		}

		/**
		 * Add state to this widget
		 */
		addState(state: WidgetState | WidgetState[]): this {
			return this.toggleState(state, true);
		}

		/**
		 * Remove state from this widget
		 */
		removeState(state: WidgetState | WidgetState[]): this {
			return this.toggleState(state, false);
		}

		/**
		 * Toggle state on this widget
		 */
		toggleState(state: WidgetState | WidgetState[] | {[state: string]: boolean}, add?: boolean): this {
			let name: string | WidgetState,
				newState: boolean,
				stateList = state as WidgetState[],
				stateObj = state as {[state: string]: boolean},
				changeState: WidgetState[] = [],
				currentState = this.state,
				lockedState = this.lockedState;

			if (isString(state)) {
				stateList = (state as WidgetState).split(/ +/) as WidgetState[];
				// Immediately fall through to array
			}
			if (isArray(stateList)) {
				stateObj = {};
				stateList.forEach(function(name) {
					stateObj[name as string] = add;
				});
			}
			for (name in stateObj) {
				add = stateObj[name];
				newState = add === true || add === false ? add : !currentState[name];
				if (currentState[name] !== newState && (!lockedState || name !== "disabled" || !lockedState.hasOwnProperty(name))) {
					if (name === "focus" && newState) {
						this.get(InputWidget, name, false).forEach(function(widget) {
							widget.removeState(name as WidgetState);
						});
					}
					currentState[name] = newState;
					changeState.push(name as WidgetState);
				}
			}
			if (changeState.length) {
				this.fixState();
				Widget.trigger(".state", this, changeState);
			}
			return this;
		}

		/**
		 * Fix classnames for this widget
		 * Due to the slow nature of changing classes, this is setting a flag
		 * which will be called via a callback as soon as the browser can handle
		 * it - typically less than a single frame later
		 */
		fixState(): this {
			if (Widget.started) {
				this.element.fixState(this.state, this.index);
			}
			return this;
		}

		/**
		 * Find an attribute on our element,
		 * returns whether the attribute exists.
		 */
		getAttribute(name: string): string;
		getAttribute(name: string[]): string[];
		getAttribute(name: string | string[]): string | string[] {
			if (isString(name)) {
				return this.element.getAttribute(name);
			}
			let result: string[] = [];

			(name as string[]).forEach((name) => {
				result.push(this.getAttribute(name));
			});
			return result;
		}

		/**
		 * Find an attribute on our element,
		 * returns whether the attribute exists.
		 */
		hasAttribute(name: string | string[]): boolean {
			if (isString(name)) {
				return this.element.hasAttribute(name);
			}
			return (name as string[]).some(this.hasAttribute, this);
		}

		/**
		 * Remove an attribute on our element,
		 * returns whether the attribute was removed.
		 */
		removeAttribute(name: string | string[]): boolean {
			if (isString(name)) {
				let element = this.element,
					change = element.hasAttribute(name);

				if (change) {
					element.removeAttribute(name);
				}
				return change;
			}
			return !!(name as string[]).filter(this.removeAttribute, this).length;
		}

		/**
		 * Set an attribute on our element,
		 * returns whether the attribute has been changed.
		 */
		setAttribute(name: {[name: string]: string}): boolean;
		setAttribute(name: string, value: string): boolean;
		setAttribute(name: string | {[name: string]: string}, value?: string): boolean {
			if (isString(name)) {
				let element = this.element,
					change = element.getAttribute(name) != value;

				if (change) {
					element.setAttribute(name, value);
				}
				return change;
			}
			return !!Object.keys(name as {[name: string]: string}).filter((key) => {
				return this.setAttribute(key, name[key]);
			}, this).length;
		}

		/**
		 * Shortcut for adding event listeners
		 */
		on(event: WidgetEvents | WidgetEvents[], el?: Element | Window): this {
			if (isArray(event)) {
				event.forEach((event) => {
					this.on(event, el);
				});
			} else if (event) {
				this.events[event as string] = this.getEventHandler(event as WidgetEvents);
				if (!this.events[event as string]) {
					console.error("Error: Event handler doesn't exist for " + event, this);
				}
				if (event[0] !== "." && event[0] !== "!") {
					(el || this.element).addEventListener(event as WidgetEvents, Widget.eventHandler);
				} else {
					Widget.globalEvents.pushOnce(this);
					if (event[0] === "!") {
						let realEvent = (event as string).substring(1);

						if (!Widget.eventList[realEvent]) {
							document.addEventListener(realEvent, Widget.eventHandler);
						}
						Widget.eventList[realEvent] = (Widget.eventList[realEvent] || 0) + 1;
					}
				}
			}
			return this;
		}

		/**
		 * Shortcut for removing event listeners
		 */
		off(...eventList: WidgetEvents[]): this {
			// console.log("off", eventList)
			eventList.forEach((event) => {
				this.events[event] = null;
				if (event[0] !== "!" && event[0] !== ".") {
					this.element.removeEventListener(event, Widget.eventHandler, false);
				}
			});
			return this;
		}

		/**
		 * Called on widget startup, shouldn't need to do anything except in rare cases
		 */
		startup() {}

		/**
		 * Used for individual widget events that return data
		 */
		public trigger(eventType: WidgetEvents, ...args: any[]): any {
			if (this.events[eventType]) {
				return this.events[eventType].apply(this, args);
			}
		}

		/**
		 * Used for widget events
		 */
		public static trigger(eventType: WidgetEvents, ...args: any[]) {
			Widget.globalEvents.forEach(function(widget: Widget) {
				if (widget.events[eventType]) {
					widget.events[eventType].apply(widget, args);
				}
			});
		}

		/**
		 * Floors the width and height of any widget element with an ic-rounding
		 * attribute
		 */
		private static fixRounding() {
			let fail = false;

			Widget.roundSize.forEach((widget) => {
				let element = widget.element,
					style = element.style;

				style.width = style.height = "";
				let computed = getComputedStyle(element),
					width = parseInt(computed.width),
					height = parseInt(computed.height);

				if (width || height) {
					style.width = width + "px";
					style.height = height + "px";
				} else {
					fail = true;
				}
			});
			if (fail) {
				rAF(".round", function() {
					Widget.fixRounding();
				});
			}
		}

		/**
		 * Called on Window resize to throttle stuff
		 */
		public static resizeHandler() {
			rAF(".resize", function() {
				Widget.fixRounding();
				Widget.trigger(".resize");
			});
		}

		private static lastEvent: Event;
		private static waitForMouseUp: boolean;

		/*
		 * Event handler - handles element / document based events and passes them
		 * to the relevant widgets
		 */
		private static eventHandler(event: MouseEvent | TouchEvent) {
			if (Widget.lastEvent !== event) {
				let i: number,
					widget: Widget,
					el = event.target as HTMLElement,
					eventType = event.type,
					globalEventType = "!" + eventType,
					events = Widget.globalEvents,
					cancel = false;

				Widget.lastEvent = event;
				if (Widget.waitForMouseUp) {
					let isMouseClickType = ["mousedown", "mouseup"].indexOf(eventType);

					if (isMouseClickType > 0) {
						Widget.waitForMouseUp = false;
					}
					if (isMouseClickType >= 0) {
						return;
					}
				}
				for (; !cancel && el && el !== document.body; el = el.parentNode as HTMLElement) {
					widget = el.icWidget;
					if (widget && widget.events[eventType]) {
						cancel = widget.events[eventType].call(widget, event);
						if (!event.bubbles) {
							break;
						}
					}
				}
				for (i = 0; event.bubbles && !cancel && i < events.length; i++) {
					widget = events[i];
					if (widget && widget.events[globalEventType]) {
						cancel = widget.events[globalEventType].call(widget, event);
					}
				}
				if (eventType === "touchstart") {
					Widget.waitForMouseUp = true;
				}
				if (cancel) {
					event.preventDefault();
					return false;
				}
			}
		}

		/**
		 * Persist the full tree of widgets. This will include only the types
		 * specified, so any child types need to persist their own child types.
		 * Be aware that this means that any widgets not inside a ScreenWidget
		 * (except a timer) will *not* be persisted.
		 */
		protected persistTree(persistTypes: {[name: string]: {new(...args: any[]): Widget;}}, state?: any): any {
			if (isUndefined(state)) {
				let typeName: string,
					firstIndex: number = 0,
					lastIndex: number = 0,
					firstValue: string = null,
					addOutput = function() {
						if (firstIndex) {
							let map = persistMap(typeName);

							state[map] = state[map] || {};
							state[map][String(firstIndex) + (lastIndex ? "-" + String(lastIndex) : "")] = firstValue;
						}
					};

				state = {};
				if (this._persist) {
					this._persist.forEach((key) => {
						if (!isUndefined((this as {[key: string]: any})[key])) {
							state["_" + persistMap(key)] = (this as {[key: string]: any})[key];
						}
					});
				}
				PersistState.forEach((key) => {
					if (this.hasState(key)) {
						state["@" + persistMap(key)] = true;
					}
				});
				for (typeName in persistTypes) {
					firstIndex = lastIndex = 0;
					firstValue = null;

					this.get(this, persistTypes[typeName]).forEach(function(widget) {
						let index = widget._i;

						if (index) {
							let value = (widget as Widget).trigger(".persist");

							if (firstValue === value || (isArray(firstValue) && !firstValue.length && isArray(value) && !value.length)) {
								lastIndex = index;
							} else {
								addOutput();
								firstIndex = index;
								lastIndex = 0;
								firstValue = value;
							}
						}
					});
					addOutput();
				}
				return state;
			}
			// Loading
			for (let typeName in persistTypes) {
				let typeData = state[persistMap(typeName)];

				if (typeData) {
					this.get(this, persistTypes[typeName]).forEach(function(widget) {
						let index = widget._i;

						if (index) {
							let state = typeData[index];

							if (isUndefined(state)) {
								for (let key in typeData) {
									let range = key.split("-");

									if (range.length === 2 && parseInt(range[0], 10) <= index && parseInt(range[1], 10) >= index) {
										state = typeData[key];
										break;
									}
								}
							}
							if (isUndefined(state)) {
								console.error("Error: Unable to load state for", widget);
							} else {
								(widget as Widget).trigger(".persist", state);
							}
						}
					});
				}
				if (this._persist) {
					this._persist.forEach((key) => {
						(this as {[key: string]: any})[key] = state["_" + persistMap(key)];
					});
				}
				let updateState: {[state: string]: boolean} = {};
				PersistState.forEach((key) => {
					updateState[key] = state["@" + persistMap(key)] ? true : false;
				});
				this.toggleState(updateState);
			}
		}

		/**
		 * Look through the page and find all widgets, create a widget instance of the correct type for each one
		 */
		static startup(root: Node) {
			if (!root) {
				console.error("Trying to run Widget.startup() without a valid root Node");
				return;
			}
			let classes: {[selector: string]: WidgetConstructor<Widget>} = {},
				styleScript = /^(style|script)$/i;

			Array.from(querySelectorAll("ic-screen[x]")).forEach((root) => {
				let walker = document.createNodeIterator(root, NodeFilter.SHOW_TEXT, null, false),
					node = walker.nextNode();

				while (node) {
					if (!styleScript.test(node.parentNode.nodeName)) {
						node.textContent = node.textContent.decompress("uri");
					}
					node = walker.nextNode();
				}
			});
			this.widgets = [];
			this.globalEvents = [];
			this.eventList = {};
			this.forEachClass((widgetClass) => {
				// figure out our widget subclasses
				let selector: string = widgetClass.selector;

				if (selector) {
					selector.split(",").forEach(function(selector) {
						if (classes[selector]) {
							console.error("Duplicate widget selector:", widgetClass.selector, classes[selector].selector);
						} else {
							classes[selector] = widgetClass;
						}
					});
				}
			});

			let onLoadCode: [Widget, string][] = [],
				selectorList = Object.keys(classes).sort().reverse(),
				findWidget = function(el: HTMLElement): WidgetConstructor<Widget> {
					return classes[selectorList.find(function(selector) {
						return el.matches(selector);
					})];
				};

			// For lazy-loaded images, give a "fake" svg of the correct
			// dimensions to make layout be consistant and give space for the
			// loading animation to play in.
			querySelectorAll("img[data-src][width][height]:not([src])").forEach(function(img: HTMLImageElement) {
				img.src = "data:image/svg+xml;base64," + btoa("<svg xmlns='http://www.w3.org/2000/svg' width='" + img.getAttribute("width") + "' height='" + img.getAttribute("height") + "'/>");
			});
			querySelectorAll(Object.keys(classes).join(",")).forEach(function(el) {
				let widget = new (findWidget(el))(el),
					onLoad = el.getAttribute("icOnLoad");

				if (onLoad) {
					onLoadCode.push([widget, onLoad]);
				}
			})
			this.created = true;
			if (!inEditor) {
				this.forEach(function(widget) {
					if (widget.isRandom) {
						let widgets = widget.get(widget.closestWidget, widget.constructor as WidgetConstructor<Widget>, widget.group).filter(widget => (widget as Widget).isRandom),
							original: WidgetNode[] = [],
							parent: Node[] = [],
							next: Node[] = [],
							isSameOrder = widgets.length;

						//console.log("random", widget, widgets)
						widgets.forEach((widget, index) => {
							let el = widget.element as Node;

							original[index] = widget;
							parent[index] = el.parentNode;
							while (el && (el as Element).icWidget && widgets.includes((el as Element).icWidget as any)) {
								el = el.nextSibling;
							}
							next[index] = el;
							(widget as Widget).isRandom = false;
						});
						//console.log("shuffle", widgets.map(widget => widget.realIndex), original.map(widget => widget.realIndex))
						while (isSameOrder > 1) {
							isSameOrder = 0;
							widgets.shuffle().forEach((widget, index) => {
								if (original[index] === widget) {
									isSameOrder++;
								}
							});
							//console.log("shuffle", widgets.map(widget => widget.realIndex), original.map(widget => widget.realIndex))
						}
						widgets.forEach((widget, index) => {
							//original[index].swapNodes(widget);
							parent[index].insertBefore(widget.element, next[index]);
						});
					}
				});
			}
			this.forEach(function(widget) {
				widget.startup();
			});
			this.started = true;
			this.forEach(function(widget) {
				widget.element.fixState(widget.state, widget.index);
				if (widget instanceof ActivityWidget) {
					widget.mark();
				}
			});
			onLoadCode.forEach(function(onLoad) {
				onLoad[0].callUserFunc(onLoad[1]);
			});
			querySelectorAll("link[rel='stylesheet']").forEach(function(el: HTMLLinkElement) {
				el.addEventListener("load", cssLoadHandler, false);
			});
			Widget.fixRounding();
			rAF(frameHandler);
			setInterval(function() {
				Widget.trigger(".tick");
			}, 1000);

			Widget.root.get(ScreenWidget).not(Widget.root.screenWidget).forEach(screen => screen.detach());

			/*
			 * Prevent major FOUC - visibility is hidden until this has set all
			 * the classNames etc - could conceivably wait for the stylesheets
			 * to load, however there may be errors or delays, and better to see
			 * something that looks almost right first
			 */
			let style = document.documentElement.style;

			if (style.visibility === "hidden") {
				style.visibility = "";
			}
		}

		/**
		 * A {source:Function} mapping to save the more expensive new Function(source)
		 * call and allow the browser to optimise it.
		 */
		static callFunctions: {[source: string]: Function} = {};

		/**
		 * Calls a user supplied function, similar to eval(), however allows the
		 * widget to be passed as 'this'.
		 */
		protected callUserFunc(source: string, ...args: any[]) {
			if (isString(source)) {
				// TODO: Wrap in a try/catch
				let fn = Widget.callFunctions[source];

				if (!fn) {
					fn = Widget.callFunctions[source] = new Function(source);
				}
				fn.apply(this.element, args);
			}
		}

		/**
		 * Perform a callback on every instanced widget
		 */
		static forEach(callback: (widget: Widget, i?: number, list?: Widget[]) => void, thisArg?: any) {
			this.widgets.forEach(callback, thisArg);
		}

		/**
		 * Perform a callback on every instanced widget, break on return false
		 */
		static some(callback: (widget: Widget, i?: number, list?: Widget[]) => boolean, thisArg?: any) {
			return this.widgets.every(callback, thisArg);
		}

		/**
		 * Perform a callback on every instanced widget, break on return true
		 */
		static every(callback: (widget: Widget, i?: number, list?: Widget[]) => boolean, thisArg?: any) {
			return this.widgets.some(callback, thisArg);
		}

		/**
		 * Perform a callback on every widget subclass
		 */
		static forEachClass(callback: (widgetClass: WidgetConstructor<Widget>, i?: number) => void) {
			Object.keys(ic).forEach((className, index) => {
				let widgetClass: WidgetConstructor<Widget> = (ic as {[key: string]: any})[className];

				if (widgetClass && widgetClass.prototype instanceof Widget) {
					callback(widgetClass, index);
				}
			});
		}
	}

	addEventListener("resize", Widget.resizeHandler);
};
