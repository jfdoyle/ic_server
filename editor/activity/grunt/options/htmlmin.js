module.exports = {
	main: {
		options: {
			removeComments: true,
			collapseWhitespace: true,
			collapseInlineTagWhitespace: true
		},
		files: {
			"build/test.html": "test/test.html"
		}
	}
};
