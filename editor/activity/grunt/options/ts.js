module.exports = {
	options: {
		"additionalFlags": "--experimentalDecorators --lib dom,es6,scripthost", // --strictNullChecks
		"fast": "never"
	},
	main: {
		"tsconfig": true
	},
	test: {
		options: {
			"additionalFlags": "--allowJs --experimentalDecorators --lib dom,es6,scripthost" // --strictNullChecks
		},
		files: {
			"build/activity.test.js": ["src/core.ts", "test/app.ts"]
		}
	}
};
