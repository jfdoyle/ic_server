module.exports = {
	default: {
		files: [{
				expand: true,
				cwd: "build/",
				src: ["**"],
				dest: "version/latest/"
			}]
	},
	major: {
		files: [{
				expand: true,
				cwd: "build/",
				src: ["**"],
				dest: "version/latest/"
			}, {
				expand: true,
				cwd: "build/",
				src: ["**"],
				dest: "version/v<%= pkg.version %>/",
				rename: function(dest, src) {
					return dest.replace(/v(\d+)\.(\d+)\.(\d+)\//, function($0, major, minor, patch) {
						return "v" + (parseInt(major) + 1) + ".0.0/";
					}) + src;
				}
			}, {
				expand: true,
				cwd: "build/",
				src: ["**"],
				dest: "version/v<%= pkg.version %>/",
				rename: function(dest, src) {
					return dest.replace(/v(\d+)\.(\d+)\.(\d+)\//, function($0, major, minor, patch) {
						return "v" + (parseInt(major) + 1) + ".0/";
					}) + src;
				}
			}, {
				expand: true,
				cwd: "build/",
				src: ["**"],
				dest: "version/v<%= pkg.version %>/",
				rename: function(dest, src) {
					return dest.replace(/v(\d+)\.(\d+)\.(\d+)\//, function($0, major, minor, patch) {
						return "v" + (parseInt(major) + 1) + "/";
					}) + src;
				}
			}]
	},
	minor: {
		files: [{
				expand: true,
				cwd: "build/",
				src: ["**"],
				dest: "version/latest/"
			}, {
				expand: true,
				cwd: "build/",
				src: ["**"],
				dest: "version/v<%= pkg.version %>/",
				rename: function(dest, src) {
					return dest.replace(/v(\d+)\.(\d+)\.(\d+)\//, function($0, major, minor, patch) {
						return "v" + major + "." + (parseInt(minor) + 1) + ".0/";
					}) + src;
				}
			}, {
				expand: true,
				cwd: "build/",
				src: ["**"],
				dest: "version/v<%= pkg.version %>/",
				rename: function(dest, src) {
					return dest.replace(/v(\d+)\.(\d+)\.(\d+)\//, function($0, major, minor, patch) {
						return "v" + major + "." + (parseInt(minor) + 1) + "/";
					}) + src;
				}
			}, {
				expand: true,
				cwd: "build/",
				src: ["**"],
				dest: "version/v<%= pkg.version %>/",
				rename: function(dest, src) {
					return dest.replace(/v(\d+)\.(\d+)\.(\d+)\//, function($0, major, minor, patch) {
						return "v" + major + "/";
					}) + src;
				}
			}]
	},
	patch: {
		files: [{
				expand: true,
				cwd: "build/",
				src: ["**"],
				dest: "version/latest/"
			}, {
				expand: true,
				cwd: "build/",
				src: ["**"],
				dest: "version/v<%= pkg.version %>/",
				rename: function(dest, src) {
					return dest.replace(/v(\d+)\.(\d+)\.(\d+)\//, function($0, major, minor, patch) {
						return "v" + major + "." + minor + "." + (parseInt(patch) + 1) + "/";
					}) + src;
				}
			}, {
				expand: true,
				cwd: "build/",
				src: ["**"],
				dest: "version/v<%= pkg.version %>/",
				rename: function(dest, src) {
					return dest.replace(/v(\d+)\.(\d+)\.(\d+)\//, function($0, major, minor, patch) {
						return "v" + major + "." + minor + "/";
					}) + src;
				}
			}, {
				expand: true,
				cwd: "build/",
				src: ["**"],
				dest: "version/v<%= pkg.version %>/",
				rename: function(dest, src) {
					return dest.replace(/v(\d+)\.(\d+)\.(\d+)\//, function($0, major, minor, patch) {
						return "v" + major + "/";
					}) + src;
				}
			}]
	}
};
