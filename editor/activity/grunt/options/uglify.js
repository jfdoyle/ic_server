module.exports = {
	options: {
		banner: '/*! <%= pkg.name %> v<%= pkg.version %> (<%= grunt.template.today("dddd dS mmmm yyyy, h:MM:ss TT") %>) */',
		screwIE8: true,
		toplevel: true,
		mangle: {
			toplevel: true
//			properties: "domprops"
		},
		compress: {
			dead_code: true,
			drop_console: true,
			drop_debugger: true,
			pure_getters: true,
			unsafe: true,
			global_defs: {
				"VERSION": '<%= pkg.version %>',
				"DATE": '<%= grunt.template.today("yyyy-mm-dd HH:MM:ss") %>'
			}
		}
	},
	main: {
		files: {
			"build/activity.min.js": ["build/activity.js"]
		}
	},
	test: {
		options: {
			banner: '/*! TEST-<%= pkg.name %> v<%= pkg.version %> (<%= grunt.template.today("dddd dS mmmm yyyy, h:MM:ss TT") %>) */'
		},
		files: {
			"build/activity.test.min.js": ["build/activity.test.js"]
		}
	}
};
