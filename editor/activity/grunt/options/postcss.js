module.exports = {
	main: {
		options: {
			processors: [
				require('postcss-assets')({loadPaths: ["src/"]}),
				require("autoprefixer")({browsers: ["last 2 versions", "Safari 8"], remove: false})
			]
		},
		files: {
			"build/activity.css": ["build/activity.css"]
		}
	}
};
