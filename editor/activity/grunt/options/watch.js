module.exports = {
	scripts: {
		files: "src/**/*.ts",
		tasks: ["ts:main", "changed:uglify:main"]
	},
	tests: {
		files: ["src/**/*.ts", "test/**/*.ts"],
		tasks: ["ts:test", "changed:uglify:test"]
	},
	css: {
		files: "src/**/*.scss",
		tasks: ["sass", "postcss"]
	},
	html: {
		files: "test/test.html",
		tasks: ["htmlmin"]
	},
	json: {
		files: "src/rules.json",
		tasks: ["jsonmin"]
	}
};
