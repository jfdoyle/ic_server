module.exports = {
	main: {
		options: {
			useStrict: true,
			args: ["document", "window"],
			params: ["document", "window"]
		},
		files: {
			"build/activity.js": ["build/activity.js"]
		}
	}
};
