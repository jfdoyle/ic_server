module.exports = {
	main: {
		options: {
			module: "system",
			target: "es6",
			out: "build/docs/",
			mode: "file",
			name: "Infuze Creator",
			hideGenerator: true
		},
		src: ["src/**/*.ts", "src/**/*.tsx"]
	}
};
