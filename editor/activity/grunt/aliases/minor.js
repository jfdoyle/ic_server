module.exports = function(grunt) {
	grunt.registerTask("minor", [
		"bump-only:minor",
		"default",
		"typedoc",
		"newer:copy:minor",
		"bump-commit"
	]);
};
