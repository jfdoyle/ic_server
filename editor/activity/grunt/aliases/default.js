module.exports = function(grunt) {
	grunt.registerTask("default", [
		"ts",
		"newer:iife",
		"changed:uglify",
		"newer:sass",
		"newer:postcss",
		"newer:jsonmin"
	]);
};
