module.exports = function(grunt) {
	grunt.registerTask("major", [
		"bump-only:major",
		"default",
		"typedoc",
		"newer:copy:major",
		"bump-commit"
	]);
};
