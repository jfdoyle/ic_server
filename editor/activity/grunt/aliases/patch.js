module.exports = function(grunt) {
	grunt.registerTask("patch", [
		"bump-only:patch",
		"default",
		"typedoc",
		"newer:copy:patch",
		"bump-commit"
	]);
};
