# Infuze Creator

An activity creation system using HTML5.

## Activities

This is the Activities section, responsible for running activities that are displayed as plain HTML. The display, almost static html, is mostly unchanged by this excepting behaviours (such as dragging).

--

*See roadmap document for details.*

Copyright &copy; 2016 Infuze, All Rights Reserved.
