var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var defaultJson = {
    "classNames": {
        "*": {
            "tick": "ict-state-tick",
            "cross": "ict-state-cross",
            "reveal": "ict-state-show",
            "active": "ict-state-active",
            "click": "ict-state-click",
            "disabled": "ict-state-disabled",
            "hover": "ict-state-hover"
        },
        "ic-activity": {
            "*": "ict-activity"
        },
        "ic-anchor": {
            "*": "ict-anchor"
        },
        "ic-box": {
            "*": "ict-box"
        },
        "ic-button": {
            "*": "ict-button",
            "ic-button=submit": "ict-button-submit",
            "ic-button=reset": "ict-button-reset",
            "ic-button=reveal": "ict-button-reveal",
            "ic-button=first": "ict-button-first",
            "ic-button=previous": "ict-button-previous",
            "ic-button=navigate": "ict-button-navigate",
            "ic-button=next": "ict-button-next",
            "ic-button=last": "ict-button-last",
        },
        "ic-draggable": {
            "*": "ict-draggable",
            "ic-draggable=*move": "ict-draggable-move",
            "ic-draggable=copy": "ict-draggable-copy",
            "dragging": "ict-state-dragging"
        },
        "ic-dropdown": {
            "*": "ict-dropdown",
            "empty": "ict-state-empty",
            "open": "ict-state-open"
        },
        "ic-droppable": {
            "*": "ict-droppable",
            "ic-droppable=*none": "",
            "ic-droppable=color": "ict-droppable-color",
            "empty": "ict-state-empty",
        },
        "ic-line": {
            "*": "ict-line"
        },
        "ic-pin": {
            "*": "ict-pin",
            "empty": "ict-state-empty",
        },
        "ic-pins": {
            "*": "ict-pins"
        },
        "ic-question": {
            "*": "ict-question"
        },
        "ic-text": {
            "*": "ict-text",
            "empty": "ict-state-empty"
        },
        "ic-timer": {
            "*": "ict-timer"
        },
        "ic-toggle": {
            "*": "ict-toggle",
            "checked": "ict-state-checked",
            "ic-toggle=single": "ict-toggle-single",
            "ic-toggle=*multiple": "ict-toggle-multiple"
        },
        "img": {
            "*": "ict-image"
        }
    },
    "support": {
        "columns": 1,
        "ic-activity": 1,
        "ic-anchor": 1,
        "ic-box": 1,
        "ic-button": 1,
        "ic-draggable": 1,
        "ic-dropdown": 1,
        "ic-droppable": 1,
        "ic-pin": 1,
        "ic-pins": 1,
        "ic-question": 1,
        "ic-text": 1,
        "ic-timer": 1,
        "ic-toggle": 1,
    }
};
function defineProperty(proto, fName, fn) {
    if (!proto[fName]) {
        proto[fName] = fn;
        Object.defineProperty(proto, fName, { "enumerable": false });
    }
}
defineProperty(Array.prototype, "clean", function () {
    for (var i = 0, arr = []; i < this.length; i++) {
        if (this[i] || this[i] === 0 || this[i] === false) {
            arr.push(this[i]);
        }
    }
    return arr;
});
defineProperty(Array.prototype, "equals", function (target) {
    var index;
    if (!target || this.length !== target.length) {
        return false;
    }
    for (index = 0; index < this.length; index++) {
        if (typeof this[index] !== typeof target[index] || !this[index] !== !target[index]) {
            return false;
        }
        switch (typeof (this[index])) {
            case "object":
                if (this[index] !== null && target[index] !== null && (this[index].constructor.toString() !== target[index].constructor.toString() || !this[index].equals(target[index]))) {
                    return false;
                }
                break;
            case "function":
                if (this[index].toString() !== target[index].toString()) {
                    return false;
                }
                break;
            default:
                if (this[index] !== target[index]) {
                    return false;
                }
        }
    }
    return true;
});
defineProperty(Array.prototype, "explode", function () {
    var i, num, arr = this.slice(0);
    for (i = 0; i < arr.length; i++) {
        num = parseInt(arr[i], 10);
        if (String(num === arr[i])) {
            arr[i] = num;
        }
    }
    return arr;
});
defineProperty(Array.prototype, "find", function (predicate) {
    if (this === null) {
        throw new TypeError('Array.prototype.find called on null or undefined');
    }
    if (typeof predicate !== 'function') {
        throw new TypeError('predicate must be a function');
    }
    var list = Object(this);
    var length = list.length >>> 0;
    var thisArg = arguments[1];
    var value;
    for (var i = 0; i < length; i++) {
        value = list[i];
        if (predicate.call(thisArg, value, i, list)) {
            return value;
        }
    }
    return undefined;
});
defineProperty(Array.prototype, "findIndex", function (predicate) {
    if (this === null) {
        throw new TypeError('Array.prototype.findIndex called on null or undefined');
    }
    if (typeof predicate !== 'function') {
        throw new TypeError('predicate must be a function');
    }
    var list = Object(this);
    var length = list.length >>> 0;
    var thisArg = arguments[1];
    var value;
    for (var i = 0; i < length; i++) {
        value = list[i];
        if (predicate.call(thisArg, value, i, list)) {
            return i;
        }
    }
    return -1;
});
defineProperty(Array.prototype, "first", function () {
    return this[0];
});
defineProperty(Array.prototype, "grow", function (length) {
    var add = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        add[_i - 1] = arguments[_i];
    }
    var i = 0, len = Math.abs(length), args = [].slice.call(arguments, 1);
    if (this.length < len) {
        args = args.length ? args : [0];
        while (this.length < len) {
            if (i >= args.length) {
                i = 0;
            }
            this.push(args[i++]);
        }
    }
    else if (length > 0 && this.length > length) {
        this.splice(length, this.length - length);
    }
    return this;
});
defineProperty(Array.prototype, "includes", function (searchElement, fromIndex) {
    var i, currentElement;
    fromIndex = fromIndex || 0;
    for (i = fromIndex >= 0 ? fromIndex : Math.max(0, length + fromIndex); i < this.length; i++) {
        currentElement = this[i];
        if (searchElement === currentElement ||
            (searchElement !== searchElement && currentElement !== currentElement)) {
            return true;
        }
    }
    return false;
});
defineProperty(Array.prototype, "indexOfCase", function (searchElement, fromIndex) {
    var i, length = this.length >>> 0;
    fromIndex = fromIndex || 0;
    searchElement = searchElement.toUpperCase();
    for (i = Math.max(fromIndex >= 0 ? fromIndex : length - Math.abs(fromIndex), 0); i < length; i++) {
        if (i in this && this[i].toUpperCase() === searchElement) {
            return i;
        }
    }
    return -1;
});
defineProperty(Array.prototype, "last", function () {
    return this[this.length - 1];
});
defineProperty(Array.prototype, "natcasesort", function () {
    return this.sort(function (str1, str2) {
        str1 = (str1 + "").toLowerCase();
        str2 = (str2 + "").toLowerCase();
        var ca, cb, ia, ib, anum, alen, bnum, blen, a_length = str1.length, b_length = str2.length, isDigitChar = function (a) {
            var charCode = a.charCodeAt(0);
            return (charCode >= 48 && charCode <= 57);
        };
        for (ia = ib = 0; ia < a_length && ib < b_length; ia++, ib++) {
            ca = str1.charAt(ia);
            cb = str2.charAt(ib);
            if (isDigitChar(ca) && isDigitChar(cb)) {
                for (anum = alen = 0; isDigitChar(ca); ca = str1.charAt(++ia)) {
                    alen++;
                    anum = (anum * 10) + parseInt(ca, 10);
                }
                for (bnum = blen = 0; isDigitChar(cb); cb = str2.charAt(++ib)) {
                    blen++;
                    bnum = (bnum * 10) + parseInt(cb, 10);
                }
                ca = anum === bnum ? alen : anum;
                cb = anum === bnum ? blen : bnum;
                ia--;
                ib--;
            }
            if (ca < cb) {
                return -1;
            }
            else if (ca > cb) {
                return 1;
            }
        }
        return a_length === b_length ? 0 : a_length > b_length ? 1 : -1;
    });
});
defineProperty(Array.prototype, "pushOnce", function () {
    for (var thisArg = this, i = 0, elements = arguments; i < elements.length; i++) {
        if (!thisArg.includes(elements[i])) {
            thisArg.push(elements[i]);
        }
    }
    return thisArg.length;
});
defineProperty(Array.prototype, "remove", function (searchElement) {
    var i = this.indexOf(searchElement);
    return i >= 0 ? this.splice(i, 1) : [];
});
defineProperty(Array.prototype, "removeCase", function (searchElement) {
    var i = this.indexOfCase(searchElement);
    return i >= 0 ? this.splice(i, 1) : [];
});
defineProperty(Array.prototype, "shuffle", function () {
    var tmp, current, i = this.length;
    while (--i > 0) {
        current = Math.floor(Math.random() * (i + 1));
        tmp = this[current];
        this[current] = this[i];
        this[i] = tmp;
    }
    return this;
});
defineProperty(Array.prototype, "toObject", function () {
    var i, obj = {};
    for (i = 0; i < this.length; i++) {
        obj[this[i]] = true;
    }
    return obj;
});
defineProperty(Array.prototype, "unique", function () {
    var i = this.length;
    while (i-- > 0) {
        if (i > this.indexOf(this[i])) {
            this.splice(i, 1);
        }
    }
    return this;
});
defineProperty(Array.prototype, "wrap", function (between, before, after) {
    between = between === undefined ? "" : between;
    before = before === undefined ? "" : before;
    after = after === undefined ? "" : after;
    return before + this.join(after + between + before) + after;
});
defineProperty(Date, "format", function (format) {
    return (new Date()).format(format);
});
defineProperty(Date.prototype, "format", function (format) {
    if (isNaN(this)) {
        return "Invalid Date";
    }
    else {
        var that = this, date = that.getDate(), day = that.getDay(), month = that.getMonth(), year = that.getFullYear(), hours = that.getHours(), minutes = that.getMinutes(), seconds = that.getSeconds(), ms = String(that.getMilliseconds()), monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"], dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"], pad = function (num) {
            return num <= 9 ? "0" + num : "" + num;
        }, skip = false, output = [], plural = function (num, word) {
            return num + " " + word + (num === 1 ? "" : "s");
        }, replace = function (letter) {
            if (skip) {
                skip = false;
                return letter;
            }
            switch (letter) {
                case "\\":
                    skip = true;
                    return "";
                case "d":
                    return pad(date);
                case "D":
                    return dayNames[day].substr(0, 3);
                case "j":
                    return date;
                case "l":
                    return dayNames[day];
                case "N":
                    return day + 1;
                case "S":
                    return (date % 10 === 1 && date !== 11 ? "st" : (date % 10 === 2 && date !== 12 ? "nd" : (date % 10 === 3 && date !== 13 ? "rd" : "th")));
                case "w":
                    return day;
                case "R":
                    var i = (new Date().getTime() - that.getTime()) / 1000;
                    return i < 0
                        ? "In the Future"
                        : i < 20
                            ? "Just Now"
                            : i < 60
                                ? "Less than a Minute Ago"
                                : i < 120
                                    ? "About a Minute Ago"
                                    : i < (60 * 60)
                                        ? plural(Math.floor(i / 60), "Minute") + " Ago"
                                        : i < (2 * 60 * 60)
                                            ? "About an Hour Ago"
                                            : i < (12 * 60 * 60)
                                                ? plural(Math.floor(i / (60 * 60)), "Hour") + " Ago"
                                                : i < (2 * 24 * 60 * 60) && day === (new Date).getDay()
                                                    ? "Today"
                                                    : i < (3 * 24 * 60 * 60) && (day === 6 ? 0 : day + 1) === (new Date).getDay()
                                                        ? "Yesterday"
                                                        : i < (7 * 24 * 60 * 60)
                                                            ? plural(Math.floor(i / (24 * 60 * 60)), "Day") + " Ago"
                                                            : i < (31 * 24 * 60 * 60)
                                                                ? plural(Math.floor(i / (7 * 24 * 60 * 60)), "Week") + " Ago"
                                                                : i < (365 * 24 * 60 * 60)
                                                                    ? plural(Math.floor(i / (31 * 24 * 60 * 60)), "Month") + " Ago"
                                                                    : plural(Math.floor(i / (365 * 24 * 60 * 60)), "Year") + " Ago";
                case "F":
                    return monthNames[month];
                case "m":
                    return pad(month + 1);
                case "M":
                    return monthNames[month].substr(0, 3);
                case "n":
                    return month + 1;
                case "L":
                    return (((year % 4 === 0) && (year % 100 !== 0)) || (year) % 400 === 0) ? "1" : "0";
                case "Y":
                    return year;
                case "y":
                    return year.toString().substr(2);
                case "a":
                    return hours < 12 ? "am" : "pm";
                case "A":
                    return hours < 12 ? "AM" : "PM";
                case "g":
                    return hours % 12 || 12;
                case "G":
                    return hours;
                case "h":
                    return pad(hours % 12 || 12);
                case "H":
                    return pad(hours);
                case "i":
                    return pad(minutes);
                case "s":
                    return pad(seconds);
                case "u":
                    return ms.substr(0, 3);
                case "O":
                    var offset = that.getTimezoneOffset();
                    return (offset < 0 ? "+" : "-") + pad(Math.abs(offset / 60)) + "00";
                case "P":
                    var offset = that.getTimezoneOffset();
                    return (offset < 0 ? "+" : "-") + pad(Math.abs(offset / 60)) + ":" + pad(Math.abs(offset % 60));
                case "T":
                    var result;
                    that.setMonth(0);
                    result = that.toTimeString().replace(/^.+ \(?([^\)]+)\)?$/, "$1");
                    that.setMonth(month);
                    return result;
                case "Z":
                    return -that.getTimezoneOffset() * 60;
                case "c":
                    return that.format("Y-m-d\\TH:i:sP");
                case "r":
                    return that.toString();
                case "U":
                    return that.getTime() / 1000;
                default:
                    return letter;
            }
        };
        format = format || "c";
        for (var i = 0; i < format.length; i++) {
            output.push(replace(format[i]));
        }
    }
    return output.join("");
});
defineProperty(Element.prototype, "closest", function (selector) {
    var element = this;
    while (element && !element.matches(selector)) {
        element = element.parentElement;
    }
    return element;
});
defineProperty(Element.prototype, "closestWidget", function () {
    var element = this;
    while (element && !element.tagName.startsWith("IC-")) {
        element = element.parentElement;
    }
    return element;
});
defineProperty(Element.prototype, "common", function (element2) {
    var element = this, other = element2;
    while (element && other && element !== other) {
        while (other && element !== other) {
            other = other.parentElement;
        }
        if (other) {
            break;
        }
        element = element.parentElement;
        other = element2;
    }
    return element;
});
defineProperty(String.prototype, "startsWith", function (searchString, position) {
    return this.substr(position || 0, searchString.length) === searchString;
});
defineProperty(Element.prototype, "fixState", function (currentState, index) {
    var i, state, className, attributeNameClass, attribute, tagName = this.tagName.toLowerCase(), tagNameClass = tagName.replace(/^ic-/, "ict-"), attributes = this.attributes, currentClass = this.classList, addClass = {};
    addClass[tagNameClass] = true;
    if (index !== undefined && index >= 0) {
        addClass[tagNameClass + "-" + index] = true;
    }
    for (i = 0; i < attributes.length; i++) {
        attribute = attributes[i];
        if (attribute.name.startsWith("ic-")) {
            attributeNameClass = attribute.name.replace(/^ic-/, "ict-");
            if (attributeNameClass === "state") {
                if (attribute.value.trim()) {
                    attribute.value.split(/\s+/).forEach(function (state) {
                        addClass["ict-state-" + state] = true;
                    });
                }
            }
            else {
                addClass[attributeNameClass] = true;
                if (attribute.value && !/[^a-z0-9_-]/.test(attribute.value)) {
                    addClass[attributeNameClass + "-" + attribute.value] = true;
                }
            }
        }
    }
    if (currentState) {
        for (state in currentState) {
            if (currentState[state] === true) {
                addClass["ict-state-" + state] = true;
            }
        }
    }
    for (i = currentClass.length - 1; i >= 0; i--) {
        className = currentClass[i];
        if (className.startsWith("ict-") && !addClass[className]) {
            currentClass.remove(className);
        }
    }
    Object.keys(addClass).forEach(function (className) {
        currentClass.add(className);
    });
});
defineProperty(Element.prototype, "matches", Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector);
(function (doc, proto) {
    try {
        doc.querySelector(":scope body");
    }
    catch (e) {
        ["querySelector", "querySelectorAll"].forEach(function (method) {
            var native = proto[method];
            proto[method] = function (selectors) {
                if (/(^|,)\s*:scope/.test(selectors)) {
                    var el = this, id = el.id;
                    el.id = "SCOPE_" + Date.now();
                    selectors = selectors.replace(/((^|,)\s*):scope/g, "$1#" + el.id);
                    var result = doc[method](selectors);
                    el.id = id;
                    return result;
                }
                else {
                    return native.call(this, selectors);
                }
            };
        });
    }
})(window.document, Element.prototype);
defineProperty(Function.prototype, "bind", function (thisArg, argArray) {
    argArray = [].slice.call(arguments);
    thisArg = argArray.shift();
    var original = this, func = function () {
        var that = thisArg;
        return original.apply(that, argArray.concat([].slice.call(arguments, argArray.length)).slice(0));
    };
    func.bind = function () {
        return Function.prototype.bind.apply(original, [].slice.call(arguments));
    };
    return func;
});
defineProperty(JSON, "shallow", function (obj, depth, replacer, space) {
    var walk = function (o, d) {
        var i, out;
        if (o && ic.isObject(typeof o)) {
            if (ic.isArray(o)) {
                if (d > 0) {
                    out = [];
                    for (i = 0; i < o.length; i++) {
                        out[i] = walk(o[i], d - 1);
                    }
                }
                else {
                    out = "[object Array]";
                }
            }
            else if (d > 0) {
                out = {};
                for (i in o) {
                    if (o.hasOwnProperty(i)) {
                        out[i] = walk(o[i], d - 1);
                    }
                }
            }
            else {
                out = "[object Object]";
            }
        }
        else {
            out = o;
        }
        return out;
    };
    return JSON.stringify(walk(obj, depth || 1), replacer, space);
});
defineProperty(Math, "range", function (min, num, max) {
    return Math.max(min, Math.min(num, max));
});
defineProperty(Number.prototype, "addCommas", function (digits) {
    var num = String(ic.isNumber(digits) ? this.toFixed(digits) : this), rx = /^(.*\s)?(\d+)(\d{3}\b)/;
    while (rx.test(num)) {
        num = num.replace(rx, "$1$2,$3");
    }
    return num;
});
defineProperty(Number.prototype, "range", function (min, max) {
    return this >= Math.min(min, max) && this <= Math.max(min, max);
});
defineProperty(Number.prototype, "toRoman", function () {
    var i, num = this, lookup = {
        "M": 1000,
        "CM": 900,
        "D": 500,
        "CD": 400,
        "C": 100,
        "XC": 90,
        "L": 50,
        "XL": 40,
        "X": 10,
        "IX": 9,
        "V": 5,
        "IV": 4,
        "I": 1
    }, roman = "";
    for (i in lookup) {
        while (num >= lookup[i]) {
            roman += i;
            num -= lookup[i];
        }
    }
    return roman;
});
defineProperty(Object.prototype, "bestMatch", function (percent) {
    var i, num, best = -1;
    for (i in this) {
        num = parseInt(i);
        if (this.hasOwnProperty(i) && num > best && num <= percent) {
            best = num;
        }
    }
    return best > -1 ? this[best] : undefined;
});
defineProperty(Object.prototype, "clear", function () {
    for (var prop in this) {
        if (this.hasOwnProperty(prop)) {
            delete this[prop];
        }
    }
    return this;
});
defineProperty(Object.prototype, "count", function () {
    if (ic.isArray(this)) {
        return this.length;
    }
    return Object.keys(this).length;
});
defineProperty(Object.prototype, "diff", function diff(original) {
    for (var i in this) {
        if (this.hasOwnProperty(i) && original.hasOwnProperty(i)) {
            if ((ic.isArray(this[i]) && ic.isArray(original[i])) || (ic.isObject(this[i]) && ic.isObject(original[i]))) {
                this[i] = diff.call(this[i], original[i], true);
                if (!this[i].count()) {
                    delete this[i];
                }
            }
            else if (this[i] === original[i]) {
                delete this[i];
            }
        }
    }
    return this;
});
defineProperty(Object.prototype, "clone", function ObjectCopyCloneFn(target, save) {
    var copy, key, value;
    if (this === null || typeof this !== "object") {
        return this;
    }
    if (this instanceof Date) {
        copy = target || new Date();
        copy.setTime(this.getTime());
        return copy;
    }
    if (this instanceof Function) {
        return this;
    }
    if (this instanceof Array) {
        if (target instanceof Array) {
            copy = target;
            if (save !== false) {
                target.length = 0;
            }
        }
        else {
            copy = [];
        }
        for (key = 0; key < this.length; key++) {
            value = this[key];
            if (ic.isObject(value)) {
                copy[key] = ObjectCopyCloneFn.call(value);
            }
            else {
                copy[key] = value;
            }
        }
        return copy;
    }
    if (this instanceof Object) {
        if (target instanceof Object) {
            copy = target;
            if (save !== false) {
                for (key in target) {
                    if (!this.hasOwnProperty(key) && target.hasOwnProperty(key)) {
                        delete target[key];
                    }
                }
            }
        }
        else {
            copy = {};
        }
        for (key in this) {
            if (this.hasOwnProperty(key)) {
                value = this[key];
                if (value instanceof Object) {
                    copy[key] = ObjectCopyCloneFn.call(value);
                }
                else {
                    copy[key] = value;
                }
            }
        }
        return copy;
    }
    throw new Error("Unable to copy obj! Its type isn't supported.");
});
defineProperty(Object.prototype, "equals", function (target, callback) {
    var key;
    if (!target) {
        return false;
    }
    for (key in this) {
        if (this.hasOwnProperty(key) && (!callback || callback.call(this, key, target) !== false)) {
            if (!target.hasOwnProperty(key) || typeof this[key] !== typeof target[key] || !this[key] !== !target[key]) {
                return false;
            }
            switch (typeof (this[key])) {
                case "object":
                    if (this[key] !== null && target[key] !== null && (this[key].constructor.toString() !== target[key].constructor.toString() || !this[key].equals(target[key]))) {
                        return false;
                    }
                    break;
                case "function":
                    if (this[key].toString() !== target[key].toString()) {
                        return false;
                    }
                    break;
                default:
                    if (this[key] !== target[key]) {
                        return false;
                    }
            }
        }
    }
    for (key in target) {
        if (!this.hasOwnProperty(key) && (!callback || callback.call(this, key, target) !== false)) {
            return false;
        }
    }
    return true;
});
defineProperty(Object.prototype, "getTree", function (path, def) {
    var data = this, fixedPath = path.explode();
    while (fixedPath.length && data !== undefined) {
        data = data[fixedPath.shift()];
    }
    return data === undefined || data === null ? def : data;
});
defineProperty(Object.prototype, "hasTree", function (path) {
    var data = this, fixedPath = path.explode();
    while (fixedPath.length && data !== undefined) {
        data = data[fixedPath.shift()];
    }
    return !(data === undefined || data === null);
});
defineProperty(Object.prototype, "indexOfX", function (value) {
    var key;
    for (key in this) {
        if (this.hasOwnProperty(key) && this[key] === value) {
            return key;
        }
    }
    return null;
});
defineProperty(Object.prototype, "setTree", function (path, value) {
    var _reduce = function (data) {
        if (ic.isArray(data)) {
            while (data.length && data[data.length - 1] === undefined || data[data.length - 1] === null) {
                data.pop();
            }
        }
    }, _set = function (data, fixedPath, fixedValue, depth) {
        var i = fixedPath[depth];
        if (depth < fixedPath.length - 1) {
            if (ic.isString(data[i]) && ic.isNumber(fixedPath[depth + 1])) {
                data[i] = [data[i]];
            }
            else if (!ic.isObject(data[i]) && !ic.isArray(data[i])) {
                data[i] = ic.isNumber(fixedPath[depth + 1]) ? [] : {};
            }
            if (!_set(data[i], fixedPath, fixedValue, depth + 1) && depth >= 1 && !data[i].count()) {
                delete data[i];
                _reduce(data);
                return false;
            }
        }
        else if (fixedValue !== data[i]) {
            if (fixedValue === undefined) {
                delete data[i];
                _reduce(data);
                return false;
            }
            data[i] = fixedValue;
        }
        return true;
    };
    if (ic.isObject(path)) {
        for (var i in path) {
            if (path.hasOwnProperty(i)) {
                _set(this, i.explode(), path[i], 0);
            }
        }
    }
    else {
        _set(this, path.explode(), value, 0);
    }
    return this;
});
defineProperty(String.prototype, "endsWith", function (searchString, position) {
    var subjectString = this.toString();
    if (typeof position !== "number" || !isFinite(position) || Math.floor(position) !== position || position > subjectString.length) {
        position = subjectString.length;
    }
    position -= searchString.length;
    var lastIndex = subjectString.indexOf(searchString, position);
    return lastIndex !== -1 && lastIndex === position;
});
defineProperty(String.prototype, "explode", function () {
    var i, num, arr = this.split(".");
    for (i = 0; i < arr.length - 1; i++) {
        if (arr[i].substr(-1) === "\\") {
            arr[i] = arr[i].substr(0, arr[i].length - 1) + "." + arr.splice(i + 1, 1);
            i--;
        }
        else {
            num = parseInt(arr[i], 10);
            if (String(num) === arr[i]) {
                arr[i] = num;
            }
        }
    }
    return arr;
});
defineProperty(String.prototype, "regex", function (r) {
    var rx, a = this.match(r);
    if (a) {
        if (r.global) {
            if (/(^|[^\\]|[^\\](\\\\)*)\([^?]/.test(r.source)) {
                rx = new RegExp(r.source, (r.ignoreCase ? "i" : "") + (r.multiline ? "m" : ""));
            }
        }
        else {
            a.shift();
        }
        var i = a.length;
        while (i--) {
            if (a[i]) {
                if (rx) {
                    a[i] = a[i].regex(rx);
                }
                else {
                    if (a[i].search(/^[\-+]?\d*\.?\d+(?:e[\-+]?\d+)?$/i) >= 0) {
                        a[i] = parseFloat(a[i]);
                    }
                }
            }
        }
        if (!rx && a.length === 1) {
            return a[0];
        }
    }
    return a;
});
defineProperty(String.prototype, "regexIndexOf", function (rx, startPos) {
    var index = this.substring(startPos || 0).search(rx);
    return index >= 0 ? index + (startPos || 0) : index;
});
defineProperty(String.prototype, "regexLastIndexOf", function (rx, startPos) {
    rx = rx.global ? rx : new RegExp(rx.source, "g" + (rx.ignoreCase ? "i" : "") + (rx.multiLine ? "m" : ""));
    if (startPos === undefined) {
        startPos = this.length;
    }
    else if (startPos < 0) {
        startPos = 0;
    }
    var result, stringToWorkWith = this.substring(0, startPos + 1), index = -1;
    while ((result = rx.exec(stringToWorkWith)) != null) {
        index = result.index;
        rx.lastIndex = index + 1;
    }
    return index;
});
defineProperty(String.prototype, "repeat", function (count) {
    if (this == null) {
        throw new TypeError('can\'t convert ' + this + ' to object');
    }
    var str = '' + this;
    count = +count;
    if (count != count) {
        count = 0;
    }
    if (count < 0) {
        throw new RangeError('repeat count must be non-negative');
    }
    if (count == Infinity) {
        throw new RangeError('repeat count must be less than infinity');
    }
    count = Math.floor(count);
    if (str.length == 0 || count == 0) {
        return '';
    }
    if (str.length * count >= 1 << 28) {
        throw new RangeError('repeat count must not overflow maximum string size');
    }
    var rpt = '';
    for (;;) {
        if ((count & 1) == 1) {
            rpt += str;
        }
        count >>>= 1;
        if (count == 0) {
            break;
        }
        str += str;
    }
    return rpt;
});
defineProperty(String.prototype, "reverse", function () {
    return this.split("").reverse().join("");
});
defineProperty(String.prototype, "text", function () {
    return this.replace(/<\/?(img|li|ul|ol|span|div)[^>]*>/gi, "");
});
defineProperty(String.prototype, "ucfirst", function () {
    return this.charAt(0).toLocaleUpperCase() + this.slice(1).toLowerCase();
});
defineProperty(String.prototype, "unicode", function () {
    return this.replace(/[\u0100-\uffff]/g, function (char) {
        return "\\u" + ("0000" + char.charCodeAt(0).toString(16)).slice(-4);
    });
});
defineProperty(window, "atob", function (input) {
    var output = "", chr1, chr2, chr3, enc1, enc2, enc3, enc4, i = 0, _keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
    while (i < input.length) {
        enc1 = _keyStr.indexOf(input.charAt(i++));
        enc2 = _keyStr.indexOf(input.charAt(i++));
        enc3 = _keyStr.indexOf(input.charAt(i++));
        enc4 = _keyStr.indexOf(input.charAt(i++));
        chr1 = (enc1 << 2) | (enc2 >> 4);
        chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
        chr3 = ((enc3 & 3) << 6) | enc4;
        output = output + String.fromCharCode(chr1);
        if (enc3 !== 64) {
            output = output + String.fromCharCode(chr2);
        }
        if (enc4 !== 64) {
            output = output + String.fromCharCode(chr3);
        }
    }
    return output;
});
defineProperty(window, "btoa", function (str) {
    if (arguments.length !== 1) {
        throw new SyntaxError("Not enough arguments");
    }
    str = "" + str;
    var i, b10, padchar = "=", alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", getbyte = function (s, i) {
        var x = s.charCodeAt(i);
        if (x > 255) {
            throw new DOMException(DOMException.INVALID_CHARACTER_ERR);
        }
        return x;
    }, x = [], imax = str.length - str.length % 3;
    if (str.length === 0) {
        return str;
    }
    for (i = 0; i < imax; i += 3) {
        b10 = (getbyte(str, i) << 16) | (getbyte(str, i + 1) << 8) | getbyte(str, i + 2);
        x.push(alpha.charAt(b10 >> 18), alpha.charAt((b10 >> 12) & 0x3F), alpha.charAt((b10 >> 6) & 0x3f), alpha.charAt(b10 & 0x3F));
    }
    switch (str.length - imax) {
        case 1:
            b10 = getbyte(str, i) << 16;
            x.push(alpha.charAt(b10 >> 18) + alpha.charAt((b10 >> 12) & 0x3F) + padchar + padchar);
            break;
        case 2:
            b10 = (getbyte(str, i) << 16) | (getbyte(str, i + 1) << 8);
            x.push(alpha.charAt(b10 >> 18) + alpha.charAt((b10 >> 12) & 0x3F) + alpha.charAt((b10 >> 6) & 0x3F) + padchar);
            break;
    }
    return x.join("");
});
(function () {
    try {
        var console = window.console || {};
        var i, method, bind = Function.prototype.bind, methods = ["log", "warn", "error", "debug", "info", "assert", "trace"];
        for (i = 0; i < methods.length; i++) {
            method = methods[i];
            console[method] = console[method] || function () { };
            if (bind && ic.isObject(console[method])) {
                console[method] = bind.call(console[method], console);
            }
        }
    }
    catch (e) {
    }
}());
var ic;
(function (ic) {
    ic.drawLineOptions = {
        source: null,
        corners: true,
        from: "center",
        to: "center",
        replace: true
    };
    function drawLine(from, target, options) {
        var fromList = from.length ? from : [from], get_relative = function (from_pos, from_size, to_pos, to_size, small, big, corners) {
            if (from_pos + (corners ? from_size / 4 : 0) > to_pos + to_size) {
                return small;
            }
            if (from_pos + (corners ? from_size / 4 * 3 : from_size) < to_pos) {
                return big;
            }
            return "center";
        }, get_pos = function (from, to, relative, corners, offset) {
            var rect_from = from.getBoundingClientRect(), rect_to = to.getBoundingClientRect(), rel = relative.split(" ");
            rel[1] = rel[1] || rel[0];
            if (rel[0] === "auto") {
                rel[0] = get_relative(rect_from.left, rect_from.width, rect_to.left, rect_to.width, "left", "right", corners);
            }
            if (rel[1] === "auto") {
                rel[1] = get_relative(rect_from.top, rect_from.height, rect_to.top, rect_to.height, "top", "bottom", corners);
            }
            if (!corners && rel.indexOf("center") === -1) {
                rel[rect_from.height > rect_from.width ? 0 : 1] = "center";
            }
            var pos = {
                left: (rel[0] === "left" ? rect_from.left : rel[0] === "right" ? rect_from.right : (rect_from.width / 2 + rect_from.left)) - offset.left,
                top: (rel[1] === "top" ? rect_from.top : rel[1] === "bottom" ? rect_from.bottom : (rect_from.height / 2 + rect_from.top)) - offset.top
            };
            return pos;
        }, update_line = function (line) {
            if (!line.offsetParent) {
                return;
            }
            var opts = line.uiLineOptions, parent_rect = line.offsetParent.getBoundingClientRect(), from = get_pos(opts.parent, opts.target, opts.from, opts.corners, parent_rect), to = get_pos(opts.target, opts.parent, opts.to, opts.corners, parent_rect), css = getComputedStyle(line), style = line.style;
            style.top = (from.top - parseFloat(css.paddingTop)) + "px";
            style.left = (from.left - parseFloat(css.paddingLeft)) + "px";
            style.width = Math.sqrt(Math.pow(from.left - to.left, 2) + Math.pow(from.top - to.top, 2)) + "px";
            style.transform = "rotate(" + Math.atan2(to.top - from.top, to.left - from.left) + "rad)";
        };
        if (target === "destroy") {
            return fromList.forEach(function (el) {
                var opts = el.uiLineOptions;
                if (opts) {
                }
                else if (el.uiLines) {
                    el.uiLines.forEach(function (line) {
                        line.parentElement.removeChild(line);
                    });
                    delete el.uiLines;
                }
            });
        }
        if (!target || target === "update") {
            return fromList.forEach(function (el) {
                var opts = el.uiLineOptions;
                if (opts) {
                    update_line(el);
                }
                else if (el.uiLines) {
                    drawLine(el.uiLines, "update");
                }
            });
        }
        if (target instanceof HTMLElement || target.length) {
            var targetList = target.length ? target : [target], opts = ic.drawLineOptions.clone((options || {}).clone(), true);
            return fromList.forEach(function (el) {
                var i, j, dest, line, source = opts.source || el, lines = el.uiLines || [];
                if (opts.replace && lines.length) {
                    lines.forEach(function (line) {
                        line.parentElement.removeChild(line);
                    });
                    lines = [];
                }
                for (i = 0; i < targetList.length; i++) {
                    dest = targetList[i];
                    for (j = 0; j < lines.length; j++) {
                        line = lines[j];
                        if (line.uiLineOptions.target === dest) {
                            break;
                        }
                    }
                    if (j === lines.length) {
                        line = document.createElement("IC-LINE");
                        line.className = "ict-line";
                        line.uiLineOptions = {
                            target: dest,
                            from: opts.from,
                            parent: source,
                            to: opts.to,
                            corners: opts.corners
                        };
                        lines.push(line);
                        el.uiLines = lines;
                        el.appendChild(line);
                    }
                    update_line(line);
                }
            });
        }
        console.error("drawLine called with bad options!", arguments);
        return this;
    }
    ic.drawLine = drawLine;
    ;
    window.addEventListener("resize", function () {
        [].forEach.call(document.getElementsByTagName("ic-line"), function (el) {
            drawLine(el, "update");
        });
    });
})(ic || (ic = {}));
var ic;
(function (ic) {
    var requestAnimationFrame = window.requestAnimationFrame, rAFname = {}, rAFwrapper = false, immediateIndex = 0, immediateName = {}, immediateCall = false, immediateSecret = "cb" + Math.random();
    function persist(target, propertyKey) {
        if (target) {
            defineProperty(target, "_persist", []);
            target._persist.push(propertyKey);
        }
    }
    ic.persist = persist;
    function defineProperty(proto, name, val) {
        if (!proto[name]) {
            Object.defineProperty(proto, name, {
                "enumerable": false,
                "value": val
            });
        }
    }
    ic.defineProperty = defineProperty;
    function callbackAndDelete(obj) {
        Object.keys(obj).forEach(function (name) {
            var callback = obj[name];
            delete obj[name];
            callback();
        });
    }
    function rAFCallback() {
        rAFwrapper = false;
        callbackAndDelete(rAFname);
    }
    ;
    function immediateCallback() {
        immediateCall = false;
        immediateIndex = 0;
        callbackAndDelete(immediateName);
    }
    function rAF(name, callback) {
        var fn;
        if (isString(name)) {
            fn = callback;
            if (callback) {
                rAFname[name] = callback;
                if (rAFwrapper) {
                    fn = false;
                }
                else {
                    rAFwrapper = true;
                    fn = rAFCallback;
                }
            }
            else {
                delete rAFname[name];
            }
        }
        else {
            fn = name;
        }
        if (fn) {
            if (requestAnimationFrame && !document.hidden) {
                requestAnimationFrame(fn);
            }
            else {
                window.setTimeout(fn, 16);
            }
        }
    }
    ic.rAF = rAF;
    ;
    window.addEventListener("message", function (event) {
        if (event.source == window && event.data == immediateSecret) {
            event.stopPropagation();
            immediateCallback();
        }
    }, true);
    function setImmediate(name, callback) {
        if (isString(name)) {
            if (callback) {
                immediateName[name] = callback;
            }
            else {
                delete immediateName[name];
                return;
            }
        }
        else if (isFunction(name)) {
            immediateName[String(immediateIndex++)] = name;
        }
        else {
            return;
        }
        if (!immediateCall) {
            immediateCall = true;
            if (postMessage) {
                postMessage(immediateSecret, "*");
            }
            else {
                setTimeout(immediateCallback, 0);
            }
        }
    }
    ic.setImmediate = setImmediate;
    ;
    function toString(obj) {
        return Object.prototype.toString.call(obj);
    }
    function isArray(arr) {
        return toString(arr) === "[object Array]";
    }
    ic.isArray = isArray;
    function isBoolean(bool) {
        return bool === true || bool === false;
    }
    ic.isBoolean = isBoolean;
    ;
    function isFunction(fn) {
        return toString(fn) === "[object Function]";
    }
    ic.isFunction = isFunction;
    function isNumber(num) {
        return typeof num === "number";
    }
    ic.isNumber = isNumber;
    ;
    function isUndefined(val) {
        return val === undefined;
    }
    ic.isUndefined = isUndefined;
    ;
    function isObject(obj) {
        if (!obj || toString(obj) !== "[object Object]") {
            return false;
        }
        return !Object.getPrototypeOf(obj);
    }
    ic.isObject = isObject;
    function isString(str) {
        return typeof str === "string";
    }
    ic.isString = isString;
    ;
    function closestScroll(node) {
        if (node) {
            if (node.scrollHeight > node.clientHeight) {
                var style = getComputedStyle(node);
                if (/^(auto|scroll)$/i.test(style.overflowY)) {
                    return node;
                }
            }
            return closestScroll(node.parentElement);
        }
    }
    ic.closestScroll = closestScroll;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var WidgetEvent = (function () {
        function WidgetEvent() {
        }
        WidgetEvent.prototype.getEventHandler = function (event) {
            switch (event) {
                case "blur":
                    return this.onBlur;
                case "click":
                    return this.onClick;
                case "focus":
                    return this.onFocus;
                case "keypress":
                    return this.onKeyPress;
                case "keyup":
                    return this.onKeyUp;
                case "mousedown":
                case "touchstart":
                    return this.onMouseDown;
                case "mouseenter":
                    return this.onMouseEnter;
                case "mouseleave":
                    return this.onMouseLeave;
                case "mouseup":
                case "touchend":
                    return this.onMouseUp;
                case "!mousemove":
                case "!touchmove":
                    return this.allMouseMove;
                case "!mouseup":
                case "!touchend":
                    return this.allMouseUp;
                case ".persist":
                    return this.onPersist;
                case ".reset":
                    return this.onReset;
                case ".reveal":
                    return this.onReveal;
                case ".score":
                    return this.onScore;
                case ".screen":
                    return this.onScreen;
                case ".scroll":
                    return this.onScroll;
                case ".state":
                    return this.onState;
                case ".submit":
                    return this.onSubmit;
                case ".timeout":
                    return this.onTimeout;
                case ".tick":
                    return this.onTick;
                default:
                    console.error("Unknown event", event);
            }
        };
        return WidgetEvent;
    }());
    ic.WidgetEvent = WidgetEvent;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var WidgetNode = (function (_super) {
        __extends(WidgetNode, _super);
        function WidgetNode(element) {
            _super.call(this);
            this.element = element;
            element.icWidget = this;
            this.fixTree();
        }
        WidgetNode.prototype.fixTree = function () {
            var widgetType = this.constructor, isInputWidget = widgetType.isInputWidget, hasParent = false;
            delete this.rootWidget;
            delete this.screenWidget;
            delete this.activityWidget;
            delete this.parentWidget;
            delete this.previousWidgetSibling;
            delete this.nextWidgetSibling;
            delete this.firstWidgetChild;
            delete this.lastWidgetChild;
            delete this.previousSibling;
            delete this.nextSibling;
            delete this.firstChild;
            delete this.lastChild;
            if (isInputWidget) {
                this._index = this._i = 1;
            }
            for (var parent = this.element.parentElement; parent; parent = parent.parentElement) {
                var parentWidget = parent.icWidget;
                if (parentWidget) {
                    if (!hasParent) {
                        hasParent = true;
                        if (parentWidget.lastChild) {
                            this.previousSibling = parentWidget.lastChild;
                            parentWidget.lastChild = parentWidget.lastChild.nextSibling = this;
                        }
                        else {
                            parentWidget.lastChild = parentWidget.firstChild = this;
                        }
                    }
                    if (parentWidget.constructor.isTreeWidget) {
                        this.parentWidget = parentWidget;
                        this.rootWidget = parentWidget.rootWidget || parentWidget;
                        if (isInputWidget) {
                            if (parentWidget.lastWidgetChild) {
                                this.previousWidgetSibling = parentWidget.lastWidgetChild;
                                parentWidget.lastWidgetChild = parentWidget.lastWidgetChild.nextWidgetSibling = this;
                            }
                            else {
                                parentWidget.lastWidgetChild = parentWidget.firstWidgetChild = this;
                            }
                            for (var sibling = this.previousWidgetSibling; sibling; sibling = sibling.previousWidgetSibling) {
                                if (sibling instanceof widgetType) {
                                    this.index++;
                                    this._i++;
                                }
                            }
                        }
                        break;
                    }
                }
            }
            for (var widget = this; widget; widget = widget.parentWidget) {
                if (widget instanceof ic.ActivityWidget) {
                    if (!this.activityWidget) {
                        this.activityWidget = widget;
                    }
                }
                else if (widget instanceof ic.ScreenWidget) {
                    this.screenWidget = widget;
                    break;
                }
            }
        };
        Object.defineProperty(WidgetNode.prototype, "index", {
            get: function () {
                return this._index;
            },
            set: function (index) {
                this._index = index;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(WidgetNode.prototype, "parentWidgets", {
            get: function () {
                var nodes = [], node = this.parentWidget;
                while (node) {
                    nodes.push(node);
                    node = node.parentWidget;
                }
                return nodes;
            },
            enumerable: true,
            configurable: true
        });
        WidgetNode.prototype.get = function (_root, _subClass, _group, _state, _andSelf) {
            var _this = this;
            var widgets = [], root, subClass, group = undefined, state = undefined, andSelf = true, fn = function (node, isInput) {
                for (; node; node = isInput ? node.nextWidgetSibling : node.nextSibling) {
                    if ((!subClass || node instanceof subClass)
                        && (group === undefined || node.group === group)
                        && (!state || node.hasState(state))
                        && (andSelf || node !== _this)) {
                        widgets.push(node);
                    }
                    fn(isInput ? node.firstWidgetChild : node.firstChild, isInput);
                    if (node === root) {
                        break;
                    }
                }
            };
            for (var i = 0, args = arguments; i < args.length; i++) {
                switch (typeof args[i]) {
                    case "object":
                        root = args[i];
                        if (root === this) {
                            andSelf = false;
                        }
                        break;
                    case "function":
                        subClass = args[i];
                        break;
                    case "number":
                        group = args[i];
                        break;
                    case "string":
                        state = args[i];
                        break;
                    case "boolean":
                        andSelf = args[i];
                        break;
                }
            }
            if (!root) {
                root = this.rootWidget || this;
            }
            fn(root, subClass && subClass.prototype.isInputWidget);
            return widgets;
        };
        return WidgetNode;
    }(ic.WidgetEvent));
    ic.WidgetNode = WidgetNode;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    ;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    ic.PersistState = ["attempted", "disabled", "marked", "reveal", "visited"];
    function persistMap(name) {
        switch (name) {
            case "activity": return "a";
            case "attempted": return "b";
            case "attempts": return "c";
            case "disabled": return "d";
            case "drop": return "e";
            case "marked": return "f";
            case "reveal": return "g";
            case "screen": return "h";
            case "text": return "i";
            case "timer": return "j";
            case "toggle": return "k";
            case "visited": return "l";
        }
        console.warn("Unknown map type:", name);
        return name;
    }
    ;
    var Widget = (function (_super) {
        __extends(Widget, _super);
        function Widget(element) {
            _super.call(this, element);
            this.group = 0;
            this.data = {};
            this.state = {};
            this.events = {};
            Widget.widgets.pushOnce(this);
            if (element.hasAttribute("data-json")) {
                try {
                    this.data = JSON.parse(element.getAttribute("data-json"));
                }
                catch (e) {
                    console.warn("Error: Bad Json", element, element.getAttribute("data-json"));
                }
            }
            for (var parent = element; parent; parent = parent.parentElement) {
                if (parent.hasAttribute("ic-group")) {
                    this.group = parseInt(parent.getAttribute("ic-group"));
                    break;
                }
            }
            this
                .on("mouseenter")
                .on("mouseleave");
            return this;
        }
        Object.defineProperty(Widget.prototype, "selector", {
            get: function () {
                return this.constructor.selector;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Widget.prototype, "index", {
            get: function () {
                return this._index;
            },
            set: function (index) {
                if (this._index !== index) {
                    this._index = index;
                    this.fixState();
                }
            },
            enumerable: true,
            configurable: true
        });
        Widget.prototype.onMouseEnter = function (event) {
            if (!this.hasState("disabled")) {
                this.toggleState("hover", true);
            }
        };
        Widget.prototype.onMouseLeave = function (event) {
            this.toggleState("hover", false);
        };
        Widget.prototype.getBoundingClientRect = function () {
            return this.element.getBoundingClientRect();
        };
        Widget.prototype.inRect = function (top, left) {
            var rect = this.getBoundingClientRect();
            return rect.top <= top && rect.bottom >= top && rect.left <= left && rect.right >= left;
        };
        Widget.prototype.hasState = function (state) {
            var _this = this;
            if (ic.isArray(state)) {
                return state.some(function (state) {
                    return !!_this.state[state];
                });
            }
            return !!this.state[state];
        };
        Widget.prototype.addState = function (state) {
            return this.toggleState(state, true);
        };
        Widget.prototype.removeState = function (state) {
            return this.toggleState(state, false);
        };
        Widget.prototype.toggleState = function (state, add) {
            var name, newState, stateList = state, stateObj = state, changeState = [], currentState = this.state;
            if (ic.isString(state)) {
                stateList = state.split(/ +/);
            }
            if (ic.isArray(stateList)) {
                stateObj = {};
                stateList.forEach(function (state) {
                    stateObj[state] = add;
                });
            }
            for (name in stateObj) {
                add = stateObj[name];
                newState = add === true || add === false ? add : !currentState[name];
                if (currentState[name] !== newState) {
                    if (name === "focus" && newState) {
                        this.get(ic.InputWidget, name, false).forEach(function (widget) {
                            widget.removeState(name);
                        });
                    }
                    currentState[name] = newState;
                    changeState.push(name);
                }
            }
            if (changeState.length) {
                this.fixState();
                Widget.trigger(".state", this, changeState);
            }
            return this;
        };
        Widget.prototype.fixState = function () {
            if (Widget.started) {
                this.element.fixState(this.state, this.index);
            }
            else {
                this.needsFixState = true;
                if (!Widget.needsFixState) {
                    Widget.needsFixState = true;
                    ic.setImmediate("fixState", Widget.internalFixState);
                }
            }
            return this;
        };
        Widget.internalFixState = function () {
            Widget.needsFixState = false;
            Widget.widgets.forEach(function (widget) {
                if (widget.needsFixState) {
                    widget.needsFixState = false;
                    widget.element.fixState(widget.state, widget.index);
                }
            });
        };
        Widget.prototype.getAttribute = function (name) {
            var _this = this;
            if (ic.isString(name)) {
                return this.element.getAttribute(name);
            }
            var result = [];
            name.forEach(function (name) {
                result.push(_this.getAttribute(name));
            });
            return result;
        };
        Widget.prototype.hasAttribute = function (name) {
            if (ic.isString(name)) {
                return this.element.hasAttribute(name);
            }
            return name.some(this.hasAttribute, this);
        };
        Widget.prototype.removeAttribute = function (name) {
            if (ic.isString(name)) {
                var element = this.element, change = element.hasAttribute(name);
                if (change) {
                    element.removeAttribute(name);
                }
                return change;
            }
            return !!name.filter(this.removeAttribute, this).length;
        };
        Widget.prototype.setAttribute = function (name, value) {
            var _this = this;
            if (ic.isString(name)) {
                var element = this.element, change = element.getAttribute(name) != value;
                if (change) {
                    element.setAttribute(name, value);
                }
                return change;
            }
            return !!Object.keys(name).filter(function (key) {
                return _this.setAttribute(key, name[key]);
            }, this).length;
        };
        Widget.prototype.on = function (event, el) {
            this.events[event] = this.getEventHandler(event);
            if (!this.events[event]) {
                console.error("Error: Event handler doesn't exist for " + event, this);
            }
            if (event[0] !== "." && event[0] !== "!") {
                (el || this.element).addEventListener(event, Widget.eventHandler, false);
            }
            else {
                Widget.globalEvents.pushOnce(this);
                if (event[0] === "!") {
                    var realEvent = event.substring(1);
                    if (!Widget.eventList[realEvent]) {
                        document.addEventListener(realEvent, Widget.eventHandler, false);
                    }
                    Widget.eventList[realEvent] = (Widget.eventList[realEvent] || 0) + 1;
                }
            }
            return this;
        };
        Widget.prototype.off = function () {
            var _this = this;
            var eventList = [];
            for (var _a = 0; _a < arguments.length; _a++) {
                eventList[_a - 0] = arguments[_a];
            }
            eventList.forEach(function (event) {
                _this.events[event] = null;
                if (event[0] !== "!" && event[0] !== ".") {
                    _this.element.removeEventListener(event, Widget.eventHandler, false);
                }
            });
            return this;
        };
        Widget.prototype.startup = function () { };
        Widget.prototype.trigger = function (eventType) {
            var args = [];
            for (var _a = 1; _a < arguments.length; _a++) {
                args[_a - 1] = arguments[_a];
            }
            if (this.events[eventType]) {
                return this.events[eventType].apply(this, args);
            }
        };
        Widget.trigger = function (eventType) {
            var args = [];
            for (var _a = 1; _a < arguments.length; _a++) {
                args[_a - 1] = arguments[_a];
            }
            Widget.globalEvents.forEach(function (widget) {
                if (widget.events[eventType]) {
                    widget.events[eventType].apply(widget, args);
                }
            });
        };
        Widget.tickHandler = function () {
            Widget.trigger(".tick");
        };
        Widget.eventHandler = function (event) {
            var i, widget, el = event.target, eventType = event.type, events = Widget.globalEvents, cancel = false;
            for (; !cancel && el; el = el.parentElement) {
                widget = el.icWidget;
                if (widget && widget.events[eventType]) {
                    cancel = widget.events[eventType].call(widget, event);
                    if (!event.bubbles) {
                        break;
                    }
                }
            }
            eventType = "!" + eventType;
            for (i = 0; event.bubbles && !cancel && i < events.length; i++) {
                widget = events[i];
                if (widget && widget.events[eventType]) {
                    cancel = widget.events[eventType].call(widget, event);
                }
            }
            if (cancel) {
                event.preventDefault();
                return false;
            }
        };
        Widget.prototype.persistTree = function (persistTypes, state) {
            var _this = this;
            if (ic.isUndefined(state)) {
                var firstIndex = 0, lastIndex = 0, firstValue = null, addOutput = function () {
                    if (firstIndex) {
                        var map = persistMap(typeName);
                        state[map] = state[map] || {};
                        state[map][String(firstIndex) + (lastIndex ? "-" + String(lastIndex) : "")] = firstValue;
                    }
                };
                state = {};
                if (this._persist) {
                    this._persist.forEach(function (key) {
                        if (!ic.isUndefined(_this[key])) {
                            state["_" + persistMap(key)] = _this[key];
                        }
                    });
                }
                ic.PersistState.forEach(function (key) {
                    if (_this.hasState(key)) {
                        state["@" + persistMap(key)] = true;
                    }
                });
                for (var typeName in persistTypes) {
                    firstIndex = lastIndex = 0;
                    firstValue = null;
                    this.get(this, persistTypes[typeName]).forEach(function (widget) {
                        var index = widget._i;
                        if (index) {
                            var value = widget.trigger(".persist");
                            if (firstValue === value || (ic.isArray(firstValue) && !firstValue.length && ic.isArray(value) && !value.length)) {
                                lastIndex = index;
                            }
                            else {
                                addOutput();
                                firstIndex = index;
                                lastIndex = 0;
                                firstValue = value;
                            }
                        }
                    });
                    addOutput();
                }
                return state;
            }
            for (var typeName in persistTypes) {
                var typeData = state[persistMap(typeName)];
                if (typeData) {
                    this.get(this, persistTypes[typeName]).forEach(function (widget) {
                        var index = widget._i;
                        if (index) {
                            var state = typeData[index];
                            if (ic.isUndefined(state)) {
                                for (var key in typeData) {
                                    var range = key.split("-");
                                    if (range.length === 2 && parseInt(range[0], 10) <= index && parseInt(range[1], 10) >= index) {
                                        state = typeData[key];
                                        break;
                                    }
                                }
                            }
                            if (ic.isUndefined(state)) {
                                console.error("Error: Unable to load state for", widget);
                            }
                            else {
                                widget.trigger(".persist", state);
                            }
                        }
                    });
                }
                if (this._persist) {
                    this._persist.forEach(function (key) {
                        _this[key] = state["_" + persistMap(key)];
                    });
                }
                var updateState = {};
                ic.PersistState.forEach(function (key) {
                    updateState[key] = state["@" + persistMap(key)] ? true : false;
                });
                this.toggleState(updateState);
            }
        };
        Widget.startup = function (root) {
            var _this = this;
            if (!root) {
                console.error("Trying to run Widget.startup() without a valid root Node");
                return;
            }
            this.widgets = [];
            this.globalEvents = [];
            this.eventList = {};
            this.classes = {};
            this.tagNames = [];
            this.forEachClass(function (widgetClass) {
                var selector = widgetClass.selector;
                if (selector) {
                    selector.split(",").forEach(function (selector) {
                        if (_this.classes[selector]) {
                            console.error("Trying to create multiple widgets with the same selector: \"" + selector + "\")", widgetClass);
                        }
                        else {
                            _this.classes[selector] = widgetClass;
                            _this.tagNames.pushOnce(selector.replace(/[\[\.\#].*$/, "").toUpperCase());
                        }
                    });
                }
            });
            var selectorList = Object.keys(this.classes).sort().reverse(), findWidget = function (el) {
                return Widget.classes[selectorList.find(function (selector) {
                    return el.matches(selector);
                })];
            }, widgetFilter = function (node) {
                return node && node.tagName && Widget.tagNames.indexOf(node.tagName) >= 0 ? NodeFilter.FILTER_ACCEPT : NodeFilter.FILTER_SKIP;
            }, walker = document.createNodeIterator(root, NodeFilter.SHOW_ELEMENT, widgetFilter, false), el;
            while ((el = walker.nextNode())) {
                if (el.icWidget) {
                    el.icWidget.constructor.call(el.icWidget, el);
                }
                else {
                    var widgetType = findWidget(el);
                    if (!widgetType) {
                        console.error("Unknown widget type for", el, el.className);
                        continue;
                    }
                    var widget = new widgetType(el);
                    if (el.hasAttribute("ic-state")) {
                        widget.addState(el.getAttribute("ic-state"));
                    }
                }
            }
            var onLoadCode = [];
            this.forEach(function (widget) {
                widget.startup();
                widget.fixState();
                var onLoad = widget.element.getAttribute("icOnLoad");
                if (onLoad) {
                    onLoadCode.push([widget, onLoad]);
                }
            });
            this.forEach(function (widget) {
                if (widget instanceof ic.ActivityWidget) {
                    widget.mark();
                }
            });
            onLoadCode.forEach(function (onLoad) {
                onLoad[0].callUserFunc(onLoad[1]);
            });
            this.started = true;
            if (this.needsFixState) {
                this.internalFixState();
            }
        };
        Widget.prototype.callUserFunc = function (source, _args) {
            if (ic.isString(source)) {
                var args = [].slice.call(arguments, 1), fn = Widget.callFunctions[source];
                if (!fn) {
                    fn = Widget.callFunctions[source] = new Function(source);
                }
                fn.call(this, args);
            }
        };
        Widget.forEach = function (callback, thisArg) {
            this.widgets.forEach(callback, thisArg);
        };
        Widget.some = function (callback, thisArg) {
            return this.widgets.every(callback, thisArg);
        };
        Widget.every = function (callback, thisArg) {
            return this.widgets.some(callback, thisArg);
        };
        Widget.forEachClass = function (callback) {
            Object.keys(ic).forEach(function (className, index) {
                var widgetClass = ic[className];
                if (widgetClass.prototype instanceof Widget) {
                    callback(widgetClass, index);
                }
            });
        };
        Widget.classes = {};
        Widget.tagNames = [];
        Widget.timerInterval = window.setInterval(Widget.tickHandler, 1000);
        Widget.widgets = [];
        Widget.globalEvents = [];
        Widget.eventList = {};
        Widget.callFunctions = {};
        return Widget;
    }(ic.WidgetNode));
    ic.Widget = Widget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    function callInternalMark(widget) {
        if (widget.needMarking) {
            widget.needMarking = false;
            widget.internalMark();
        }
    }
    var InputWidget = (function (_super) {
        __extends(InputWidget, _super);
        function InputWidget(element) {
            _super.call(this, element);
            this.markable = {
                score: 0,
                min: 0,
                max: 0
            };
            (this.data
                || ((this.parentWidget ? this.parentWidget.data : {}) || {}).getTree([this.selector, String(this.index)], {}))
                .clone(this.markable, true);
            if (ic.isString(this.markable.points)) {
                this.markable.points = parseFloat(this.markable.points);
            }
        }
        InputWidget.prototype.startup = function () {
            var _this = this;
            if (this.element.tagName !== "IC-BUTTON") {
                var skip = ["attempted", "click", "focus", "hover", "visited"];
                this.startState = {};
                Object.keys(this.state).forEach(function (state) {
                    if (!skip.includes(state)) {
                        _this.startState[state] = _this.state[state];
                    }
                });
                this.startState["disabled"] = this.state["disabled"] || false;
                this
                    .on(".submit")
                    .on(".reset");
            }
        };
        InputWidget.prototype.hasAnswered = function () {
            return true;
        };
        InputWidget.prototype.onReset = function (screen) {
            if ((!screen || this.screenWidget === screen) && this.startState) {
                this.toggleState(this.startState);
            }
        };
        InputWidget.prototype.onSubmit = function (screen) {
            if ((!screen || this.screenWidget === screen) && this.startState && this.element.tagName !== "IC-BUTTON") {
                this.addState("disabled");
            }
        };
        InputWidget.eval = function (maths, fail) {
            var num = parseFloat(maths);
            if (!isNaN(num) && String(num) === maths) {
                return num;
            }
            else if (!/[a-z\[\],]/i.test(maths)) {
                try {
                    var result = eval(maths);
                    if (result === true) {
                        return 1;
                    }
                    var val = parseFloat(result);
                    return isNaN(val) ? 0 : parseFloat(val.toPrecision(12));
                }
                catch (error) {
                    if (!fail && !(error instanceof ReferenceError)) {
                        console.error("Error: Broken maths check:", maths, result, error);
                    }
                }
            }
            return fail ? undefined : 0;
        };
        InputWidget.prototype.parse = function (expr, max) {
            var _this = this;
            if (!InputWidget.types) {
                InputWidget.types = {
                    "act": ic.ActivityWidget,
                    "text": ic.TextWidget,
                    "toggle": ic.ToggleWidget,
                    "drag": ic.DraggableWidget,
                    "drop": ic.DroppableWidget
                };
                InputWidget.typeRx = new RegExp("#(?:act(\\d+))?(this|attempts|(?:" + Object.keys(InputWidget.types).join("|") + ")(\\d+))(%|\\$|@|=|!)?#", "g");
            }
            var mark = this.markable, found = true, cache = {}, expression = expr
                .replace(InputWidget.typeRx, function ($0, $act, $widgettype, $index, $type) {
                if (cache[$0]) {
                    return cache[$0];
                }
                var widget, noWidget = false, result = ic.isBoolean(max) ? "0" : "";
                try {
                    if ($widgettype === "attempts") {
                        result = String(_this.activityWidget.attempts || _this.screenWidget.attempts || _this.rootWidget.attempts || 0);
                        noWidget = true;
                    }
                    else if ($widgettype === "this") {
                        widget = _this;
                    }
                    else {
                        var widgetType = InputWidget.types[$widgettype.replace(/\d+/, "")], index = parseInt($index), act = $act ? parseInt($act, 10) : 0, parent = $act === "0"
                            ? _this.screenWidget
                            : $act
                                ? _this.get(_this.screenWidget || _this.rootWidget, ic.ActivityWidget).find(function (activity) {
                                    return activity.index === act && activity.constructor === ic.ActivityWidget;
                                })
                                : _this instanceof ic.ActivityWidget
                                    ? _this
                                    : (_this.activityWidget || _this.parentWidget);
                        widget = _this.get(parent, widgetType).find(function (widget) {
                            if (widget.index === index) {
                                return true;
                            }
                        });
                    }
                    if (widget) {
                        var widgetMark = widget.markable, getValue = function () {
                            return ic.isString(widgetMark.value) ? widgetMark.value : ic.isArray(widgetMark.value) ? widgetMark.value.join("|") : "";
                        };
                        if (widgetMark) {
                            result = String($type
                                ? ($type === "@"
                                    ? widgetMark.max || 0
                                    : $type === "$"
                                        ? widgetMark.score || 0
                                        : $type === "%"
                                            ? (widgetMark.scaled || 0) * 100
                                            : $type === "="
                                                ? getValue() || 0
                                                : getValue())
                                : (max === true
                                    ? widgetMark.max || 0
                                    : max === false
                                        ? widgetMark.score || 0
                                        : getValue()));
                        }
                    }
                    else if (!noWidget) {
                        console.warn("Unable to find widget for", $0);
                    }
                }
                catch (e) {
                    console.error("Error: Unknown widget type:", $0, e);
                }
                return cache[$0] = result;
            });
            if (ic.isBoolean(max)) {
                expression = expression
                    .replace(/@/g, String(mark.max || 0))
                    .replace(/\$/g, String((max ? mark.max : mark.raw) || 0))
                    .replace(/%/g, String((max ? 100 : mark.scaled * 100) || 0));
            }
            cache = {};
            while (found) {
                found = false;
                expression = expression.replace(InputWidget.methodRx, function ($0, $fn, $args) {
                    try {
                        var result = InputWidget.methods[$fn].call(cache, $args.trim(), max, mark);
                        if (result === undefined) {
                            result = $0;
                        }
                        else {
                            found = true;
                        }
                        return result;
                    }
                    catch (e) {
                        console.error("Error: Scripting error", $0, e);
                        return "";
                    }
                });
            }
            var final = InputWidget.eval(expression, true);
            return final === undefined ? expression : String(final);
        };
        InputWidget.prototype.getPoints = function (max) {
            var points = this.markable.points || 0;
            if (ic.isNumber(points)) {
                return points;
            }
            return parseFloat(this.parse(points, max || false));
        };
        InputWidget.prototype.isCorrect = function (value, answer) {
            var fixedAnswer = answer;
            if (fixedAnswer[0] === "\\") {
                fixedAnswer = fixedAnswer.substring(1);
            }
            else if (fixedAnswer[0] === "@") {
                fixedAnswer = fixedAnswer.substring(1);
                if (/^(-?[0-9]+|-?[0-9]*\.[0-9]+)$/.test(fixedAnswer)) {
                    var first = true, parts = fixedAnswer.split(".");
                    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, function () {
                        if (first) {
                            first = false;
                            return "([ ,]?)";
                        }
                        return "(?:\\1)";
                    });
                    if (!parts[0] || parts[0] === "0") {
                        parts[0] = "0?";
                    }
                    else if (parts[0] === "-0") {
                        parts[0] = "-0?";
                    }
                    return new RegExp("^" + parts.join("\.") + "$").test(value);
                }
                else {
                    return value.toLocaleLowerCase() === fixedAnswer.toLocaleLowerCase();
                }
            }
            else if (fixedAnswer[0] === "=") {
                fixedAnswer = fixedAnswer.substring(1);
                var mark = this.markable;
                if (fixedAnswer === "") {
                    return !mark.value.length;
                }
                if (fixedAnswer === "*") {
                    return !!mark.value.length;
                }
                if (/^\/.*\/i?$/.test(fixedAnswer)) {
                    fixedAnswer = InputWidget.methods["rx"](fixedAnswer, false, mark);
                }
                else {
                    fixedAnswer = this.parse(fixedAnswer);
                }
                if (fixedAnswer === "1") {
                    return true;
                }
                else if (fixedAnswer === "0") {
                    return false;
                }
            }
            return value === fixedAnswer;
        };
        InputWidget.prototype.internalMark = function () {
            var _this = this;
            var isActivity = this instanceof ic.ActivityWidget;
            if (isActivity || this.activityWidget) {
                var mark = this.markable, value = mark.value, answer = mark.answer, correctPoints = 1, wrongPoints = 0;
                if (!isActivity) {
                    mark.raw = mark.scaled = 0;
                    if (value && answer) {
                        if (ic.isString(value)) {
                            var activity = this.activityWidget;
                            if (this instanceof ic.TextWidget && activity && !activity.allowDuplicates && this.get(activity, this.constructor, this.group, false).some(function (widget) {
                                return widget.markable.score && widget.markable.value === value;
                            })) {
                                mark.raw = 0;
                            }
                            else {
                                if (!answer && !value) {
                                    mark.raw = correctPoints;
                                }
                                else if (ic.isString(answer)) {
                                    mark.raw = this.isCorrect(value, answer)
                                        ? correctPoints : wrongPoints;
                                }
                                else {
                                    mark.raw = answer.some(function (answer) {
                                        return _this.isCorrect(value, answer);
                                    }) ? correctPoints : wrongPoints;
                                }
                            }
                        }
                        else if (ic.isArray(value)) {
                            if (ic.isString(answer)) {
                                value.forEach(function (value) {
                                    mark.raw += _this.isCorrect(value, answer)
                                        ? correctPoints : wrongPoints;
                                });
                            }
                            else if (ic.isArray(answer)) {
                                value.forEach(function (value, index) {
                                    if (mark.order) {
                                        mark.raw += _this.isCorrect(value, answer[index])
                                            ? correctPoints : wrongPoints;
                                    }
                                    else {
                                        mark.raw += answer.some(function (answer) {
                                            return _this.isCorrect(value, answer);
                                        }) ? correctPoints : wrongPoints;
                                    }
                                });
                            }
                        }
                        mark.scaled = Math.range(0, (mark.raw - (mark.min || 0)) / (mark.max || 1), 1);
                    }
                    else if (!value && !answer) {
                        mark.scaled = 1;
                    }
                }
                else {
                    mark.scaled = Math.range(0, (mark.raw - (mark.min || 0)) / (mark.max || 1), 1);
                }
                switch (typeof mark.points) {
                    case "number":
                        mark.maxPoints = mark.points;
                        mark.score = mark.scaled * mark.maxPoints;
                        break;
                    case "string":
                        mark.maxPoints = this.getPoints(true);
                        mark.score = mark.scaled || isActivity ? this.getPoints() : 0;
                        if (mark.maxPoints) {
                            mark.scaled = mark.score / mark.maxPoints;
                        }
                        break;
                    default:
                        mark.maxPoints = mark.max;
                        mark.score = mark.raw;
                        break;
                }
                if (mark.round) {
                    mark.score = Math.floor(mark.score);
                }
                if (ic.isNumber(mark.score) && !isNaN(mark.score) && mark.score >= 0 && mark.maxPoints !== -1 && mark.maxPoints !== undefined) {
                    if (this.setAttribute({
                        "data-mark": String(mark.score),
                        "data-percent": String(Math.floor(mark.scaled * 100)),
                        "data-points": String(mark.maxPoints)
                    })) {
                        ic.Widget.trigger(".score", this);
                    }
                }
                else {
                    if (this.removeAttribute(["data-mark", "data-percent", "data-points"])) {
                        ic.Widget.trigger(".score", this);
                    }
                }
            }
        };
        InputWidget.internalMark = function () {
            var root = ic.Widget.root;
            if (root) {
                root.get(InputWidget).forEach(callInternalMark);
                root.get(ic.ActivityWidget).forEach(callInternalMark);
            }
        };
        InputWidget.prototype.mark = function () {
            if (!this.needMarking) {
                this.needMarking = true;
                if (this.screenWidget) {
                    this.get(this.screenWidget, ic.ActivityWidget).forEach(function (activity) {
                        activity.mark();
                    });
                }
                else if (this.activityWidget) {
                    this.activityWidget.mark();
                }
                ic.setImmediate("mark", InputWidget.internalMark);
            }
        };
        InputWidget.isInputWidget = true;
        InputWidget.methods = {
            "correct": function (args, max, mark) {
                var opts = args.regex(/([0-9]+(?:\.[0-9]*)?)/g);
                if (!opts) {
                    if (max === true) {
                        return String(mark.total);
                    }
                    return String(mark.correct);
                }
                if (max === true) {
                    return String(1);
                }
                if (opts.length === 1) {
                    return String(mark.correct >= opts[0] ? 1 : 0);
                }
                return String(mark.correct.range(opts[0], opts[1]) ? 1 : 0);
            },
            "ceil": function (args) {
                return String(Math.ceil(InputWidget.eval(args)));
            },
            "fixed": function (args, max, mark) {
                var opts = args.split(","), value = opts.length ? InputWidget.eval(opts.pop()) : parseFloat(mark.value.replace(/[^0-9\.]/g, ""));
                if (opts.length) {
                    return String(parseFloat(value.toFixed(InputWidget.eval(opts[1]))));
                }
                return String(parseFloat(value.toFixed(12)));
            },
            "find": function (args, max, mark) {
                if (max === true) {
                    return String(1);
                }
                var opts = args.split(",");
                if (opts.indexOf(opts[0], 1) >= 0) {
                    return String(1);
                }
                return String(0);
            },
            "floor": function (args) {
                return String(Math.floor(InputWidget.eval(args)));
            },
            "get": function (args) {
                var name = args.trim();
                return this[name] || "";
            },
            "grouped": function (args, max) {
                var opts = args.regex(/(\[[^\]]+\]|\d+)/g), count = -1, correct = 0, total = 0;
                if (opts) {
                    if (ic.isNumber(opts[0])) {
                        count = opts.shift();
                    }
                    opts.forEach(function (opt) {
                        var values = opt.regex(/(\d+)/g);
                        if (max || (values && !values.some(function (value) {
                            return !value;
                        }))) {
                            total += values.length;
                            correct++;
                        }
                    });
                    if (max && count) {
                        count = -1;
                    }
                }
                return String(count < 0 ? total : !count ? correct : count === correct ? total : -Math.PI);
            },
            "if": function (args, max) {
                var opts = args.split(","), what = InputWidget.eval(opts.shift());
                if (max === true) {
                    return String(Math.max(opts[0] ? parseFloat(opts[0]) : 1, opts[1] ? parseFloat(opts[1]) : 0));
                }
                return String(opts[what ? 0 : 1] || (what ? 1 : 0));
            },
            "map": function (args, max) {
                var opts = args.split(","), what = InputWidget.eval(opts.shift()), best = 0;
                opts.some(function (opt) {
                    var map = opt.regex(/^\s*([0-9]+(?:\.[0-9]*)?)\s*:\s*([0-9]+(?:\.[0-9]*)?)\s*$/);
                    if (max === true) {
                        best = Math.max(best, map[1]);
                    }
                    else if (map[0] <= what) {
                        best = map[1];
                    }
                    else {
                        return true;
                    }
                });
                return String(best);
            },
            "number": function (args) {
                return args.replace(/[^0-9\.]/g, "") || String(0);
            },
            "max": function (args, max, mark) {
                if (max === true) {
                    return String(1);
                }
                var opts = args.regex(/([0-9]+(?:\.[0-9]*)?)/g);
                if (opts.length > 1) {
                    return String(Math.max.apply(Math, opts));
                }
                var value = ic.isArray(mark.value)
                    ? mark.value
                    : [mark.value];
                return String(value.every(function (val) {
                    return parseFloat(val.replace(/[^0-9\.]/g, "")) <= opts[0];
                }) ? 1 : 0);
            },
            "min": function (args, max, mark) {
                if (max === true) {
                    return String(1);
                }
                var opts = args.regex(/([0-9]+(?:\.[0-9]*)?)/g);
                if (opts.length > 1) {
                    return String(Math.min.apply(Math, opts));
                }
                var value = ic.isArray(mark.value)
                    ? mark.value
                    : [mark.value];
                return String(value.every(function (val) {
                    return parseFloat(val.replace(/[^0-9\.]/g, "")) >= opts[0];
                }) ? 1 : 0);
            },
            "range": function (args, max, mark) {
                if (max === true) {
                    return String(1);
                }
                var opts = args.regex(/([0-9]+(?:\.[0-9]*)?)/g);
                if (opts.length > 2) {
                    return String(Math.range.apply(Math, opts));
                }
                var value = ic.isArray(mark.value)
                    ? mark.value
                    : [mark.value];
                return String(value.every(function (val) {
                    return parseFloat(val.replace(/[^0-9\.]/g, "")).range(opts[0], opts[1]);
                }) ? 1 : 0);
            },
            "rx": function (args, max, mark) {
                if (max === true) {
                    return String(1);
                }
                var opts = args.regex(/(.*?)(?:,?\s?\/(.*?)\/(i?))/), rx = new RegExp(opts[1], opts[2]), value = opts[0]
                    ? [opts[0]]
                    : ic.isArray(mark.value)
                        ? mark.value
                        : [mark.value];
                return String(value.every(function (val) {
                    return rx.test(val);
                }) ? 1 : 0);
            },
            "set": function (args) {
                var name = "", value = args.replace(/[\n\r\t]/g, "").replace(/\s*(.*?)\s*,\s*/, function ($0, $1) {
                    name = $1;
                    return "";
                });
                this[name] = value;
                return "";
            },
            "total": function (args, max, mark) {
                return String(mark.total);
            },
            "unique": function (args, max, mark) {
                if (max === true) {
                    return String(1);
                }
                var opts = args.split(",");
                for (var i = 0; i < opts.length; i++) {
                    if (opts.indexOf(opts[i], i + 1) >= 0) {
                        return String(0);
                    }
                }
                return String(1);
            },
            "": function (args) {
                var result = InputWidget.eval(args, true);
                if (result !== undefined) {
                    return String(result);
                }
            }
        };
        InputWidget.methodRx = new RegExp("(" + Object.keys(InputWidget.methods).join("|") + ")\\(([^\\(\\)]*)\\)");
        return InputWidget;
    }(ic.Widget));
    ic.InputWidget = InputWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var ActivityWidget = (function (_super) {
        __extends(ActivityWidget, _super);
        function ActivityWidget(element) {
            _super.call(this, element);
            this.markable = {
                score: 0,
                min: 0
            };
            this.parse = ic.InputWidget.prototype.parse;
            this.getPoints = ic.InputWidget.prototype.getPoints;
            var data = this.data
                || ((this.parentWidget ? this.parentWidget.data : {}) || {}).getTree([this.selector, String(this.index)], {});
            if (data) {
                data.clone(this.markable, true);
                if (data.duplicates) {
                    this.allowDuplicates = true;
                }
            }
            this
                .on("mousedown")
                .on("touchstart")
                .on(".persist")
                .on(".screen")
                .on(".state")
                .on(".timeout");
        }
        ActivityWidget.prototype.onMouseDown = function (event) {
            if (event.which === 1) {
                this.off("mousedown", "touchstart");
            }
        };
        ActivityWidget.prototype.onPersist = function (state) {
            if (!ic.isUndefined(state)) {
                this.get(this, ic.DroppableWidget).forEach(function (drop) {
                    drop.onReset();
                });
            }
            var result = this.persistTree({
                "drop": ic.DroppableWidget,
                "text": ic.TextWidget,
                "toggle": ic.ToggleWidget
            }, state);
            if (!ic.isUndefined(state)) {
                this.mark();
            }
            return result;
        };
        ActivityWidget.prototype.onScreen = function (screen) {
            if (this.screenWidget === screen && !this.get(screen, ActivityWidget, "active", false).length) {
                this.addState("active");
            }
        };
        ActivityWidget.prototype.onState = function (widget, stateList) {
            var _this = this;
            if (this === widget) {
                if (stateList.includes("attempted") && this.hasState("attempted") && this.parentWidget && !(this.parentWidget instanceof ic.ScreensWidget)) {
                    this.parentWidget.addState("attempted");
                }
                if (stateList.includes("active") && this.hasState("active")) {
                    if (this.parentWidget && !(this.parentWidget instanceof ic.ScreensWidget)) {
                        this.parentWidget.addState("active");
                    }
                    this.get(ActivityWidget, "active", false).forEach(function (activity) {
                        if (activity !== _this.parentWidget) {
                            activity
                                .removeState("active")
                                .get(activity, ic.InputWidget, "active").forEach(function (widget) {
                                widget.removeState("active");
                            });
                        }
                    });
                }
            }
            else if (this.parentWidget === widget && stateList.includes("active") && widget.hasState("active")) {
                if (!this.get(widget, ActivityWidget, "active").length) {
                    this.addState("active");
                }
            }
        };
        ActivityWidget.prototype.onTimeout = function () {
            this.get(this, ic.InputWidget).forEach(function (widget) {
                widget.addState("disabled");
            });
        };
        ActivityWidget.prototype.internalMark = function () {
            var _this = this;
            var mark = this.markable;
            if (mark.points === -1) {
                var element = this.element;
                element.removeAttribute("data-mark");
                element.removeAttribute("data-percent");
                element.removeAttribute("data-points");
            }
            else {
                mark.raw = mark.max = mark.correct = 0;
                this.get(this, ic.InputWidget).forEach(function (widget) {
                    if (widget.activityWidget === _this) {
                        mark.raw += Math.max(0, parseFloat(widget.markable.score || 0));
                        mark.max += Math.max(0, widget.getPoints());
                        mark.total++;
                        if (widget.markable.scaled > 0) {
                            mark.correct++;
                        }
                    }
                });
                ic.InputWidget.prototype.internalMark.call(this);
            }
        };
        ActivityWidget.prototype.mark = function () {
            var _this = this;
            if (!this.needMarking) {
                this.needMarking = true;
                if (this.markable.points !== -1) {
                    this.get(this, ic.InputWidget).forEach(function (widget) {
                        if (widget.activityWidget === _this) {
                            widget.mark();
                        }
                    });
                }
                ic.setImmediate("mark", ic.InputWidget.internalMark);
            }
        };
        ActivityWidget.selector = "ic-activity";
        ActivityWidget.isInputWidget = true;
        ActivityWidget.isTreeWidget = true;
        __decorate([
            ic.persist
        ], ActivityWidget.prototype, "attempts", void 0);
        return ActivityWidget;
    }(ic.Widget));
    ic.ActivityWidget = ActivityWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var ActivitiesWidget = (function (_super) {
        __extends(ActivitiesWidget, _super);
        function ActivitiesWidget() {
            _super.apply(this, arguments);
        }
        ActivitiesWidget.selector = "ic-activities";
        return ActivitiesWidget;
    }(ic.ActivityWidget));
    ic.ActivitiesWidget = ActivitiesWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var AnchorWidget = (function (_super) {
        __extends(AnchorWidget, _super);
        function AnchorWidget(element) {
            var _this = this;
            _super.call(this, element);
            if (this.screenWidget) {
                this.get(AnchorWidget, true).some(function (anchor, index) {
                    if (anchor === _this) {
                        anchor.index = index + 1;
                        return true;
                    }
                });
                element.setAttribute(AnchorWidget.selector, String(this.index));
                this.state = this.screenWidget.state;
                this.on(".state");
            }
            else {
                element.removeAttribute(AnchorWidget.selector);
                this.state = {};
                this.index = 0;
            }
        }
        AnchorWidget.prototype.onState = function (widget, stateList) {
            if (widget === this || widget === this.screenWidget) {
                var target = widget === this ? this.screenWidget : this;
                this.off(".state");
                target.fixState();
                ic.Widget.trigger(".state", target, stateList);
                this.on(".state");
            }
        };
        AnchorWidget.selector = "ic-anchor";
        return AnchorWidget;
    }(ic.Widget));
    ic.AnchorWidget = AnchorWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var BoxWidget = (function (_super) {
        __extends(BoxWidget, _super);
        function BoxWidget() {
            _super.apply(this, arguments);
        }
        BoxWidget.selector = "ic-box";
        return BoxWidget;
    }(ic.Widget));
    ic.BoxWidget = BoxWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var FeedbackWidget = (function (_super) {
        __extends(FeedbackWidget, _super);
        function FeedbackWidget() {
            _super.apply(this, arguments);
        }
        FeedbackWidget.prototype.startup = function () {
            this
                .on(".reset")
                .on(".reveal")
                .on(".submit");
        };
        FeedbackWidget.prototype.onReset = function (screen) {
            if ((!screen || this.screenWidget === screen)) {
                this.hideFeedback();
            }
        };
        FeedbackWidget.prototype.onReveal = function (screen) {
            if ((!screen || this.screenWidget === screen)) {
                this.hideFeedback();
            }
        };
        FeedbackWidget.prototype.onSubmit = function (screen) {
            if (!screen || this.screenWidget === screen) {
                var element = this.element, activity = this.get(screen, ic.ActivityWidget).first();
                if (activity) {
                    var percent = activity.markable.scaled * 100, attempts = activity.attempts || (this.screenWidget || this.rootWidget).attempts, rand = [], possible, best;
                    [].forEach.call(element.children, function (child) {
                        var i, wants = child.getAttribute("ic-feedback").split(/[,\s]+/), want;
                        for (i = 0; i < wants.length; i++) {
                            want = wants[i];
                            if (want) {
                                if (want[0] === "@" && want !== "@" + attempts) {
                                    return;
                                }
                                if (want.endsWith("%")) {
                                    if (parseInt(want, 10) > percent) {
                                        return;
                                    }
                                    best = child;
                                }
                                if (want === "*") {
                                    rand.push(child);
                                }
                                else if (!possible) {
                                    possible = child;
                                }
                            }
                        }
                    });
                    if (!best && rand.length) {
                        best = rand[Math.floor(Math.random() & rand.length)];
                    }
                    if (!best) {
                        best = possible;
                    }
                    if (best) {
                        [].forEach.call(element.children, function (child) {
                            child.style.display = best === child ? "" : "none";
                        });
                    }
                    var random = (best || element).querySelectorAll("[ic-feedback='*']");
                    if (random.length) {
                        [].forEach.call(random, function (el) {
                            el.style.display = "none";
                        });
                        random[Math.floor(Math.random() * random.length)].style.display = "";
                    }
                    this.showFeedback();
                }
            }
        };
        FeedbackWidget.prototype.hideFeedback = function () {
            this.element.style.display = "";
            return this;
        };
        FeedbackWidget.prototype.showFeedback = function () {
            this.element.style.display = "flex";
            return this;
        };
        FeedbackWidget.prototype.toggleFeedback = function () {
            if (this.element.style.display) {
                return this.hideFeedback();
            }
            return this.showFeedback();
        };
        FeedbackWidget.selector = "ic-feedback";
        return FeedbackWidget;
    }(ic.BoxWidget));
    ic.FeedbackWidget = FeedbackWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var ButtonWidget = (function (_super) {
        __extends(ButtonWidget, _super);
        function ButtonWidget(element) {
            _super.call(this, element);
            this.markable.max = 0;
            if (element.getAttribute("icOnClick")) {
                this.icOnClickFn = element.getAttribute("icOnClick");
            }
            this
                .on("mousedown")
                .on("touchstart")
                .on("mouseup")
                .on("touchend");
        }
        ButtonWidget.prototype.onMouseDown = function (event) {
            if (event.which < 2 && !this.hasState("disabled")) {
                ButtonWidget.pressed = this;
                this.toggleState(["click", "focus"])
                    .on("!mousemove")
                    .on("!touchmove")
                    .on("!mouseup")
                    .on("!touchend");
                event.preventDefault();
            }
        };
        ButtonWidget.prototype.onClick = function (event) {
        };
        ButtonWidget.prototype.onMouseUp = function (event) {
            if (ButtonWidget.pressed === this) {
                event.preventDefault();
                this.removeState("click");
                this.onClick(event);
                this.callUserFunc(this.icOnClickFn);
            }
        };
        ButtonWidget.prototype.onMouseEnter = function (event) {
            if (ButtonWidget.pressed === this) {
                this.addState(["click", "hover"]);
            }
            else if (!ButtonWidget.pressed) {
                _super.prototype.onMouseEnter.call(this, event);
            }
        };
        ButtonWidget.prototype.onMouseLeave = function (event) {
            if (ButtonWidget.pressed === this) {
                this.removeState(["click", "hover"]);
            }
            else if (!ButtonWidget.pressed) {
                _super.prototype.onMouseLeave.call(this, event);
            }
        };
        ButtonWidget.prototype.allMouseMove = function (event) {
            if (ButtonWidget.pressed) {
                event.preventDefault();
            }
        };
        ButtonWidget.prototype.allMouseUp = function (event) {
            this.off("!mousemove", "!touchmove", "!mouseup", "!touchend");
            ButtonWidget.pressed = null;
        };
        ButtonWidget.selector = "ic-button";
        return ButtonWidget;
    }(ic.InputWidget));
    ic.ButtonWidget = ButtonWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var AudioType;
    (function (AudioType) {
        AudioType[AudioType["PLAY"] = 0] = "PLAY";
        AudioType[AudioType["RESET"] = 1] = "RESET";
    })(AudioType || (AudioType = {}));
    var AudioWidget = (function (_super) {
        __extends(AudioWidget, _super);
        function AudioWidget(element) {
            var _this = this;
            _super.call(this, element);
            this.onPlaying = function () {
                _this.rootWidget.element.querySelectorAll("audio,video");
                _this.addState("playing");
                if (_this.parentWidget) {
                    _this.parentWidget.addState("attempted");
                }
            };
            this.onPause = function () {
                _this.removeState("playing");
            };
            this.onEnded = function () {
                _this.removeState("playing");
                _this.audio.currentTime = 0;
                _this.onTimeupdate();
            };
            this.onTimeupdate = function () {
                var audio = _this.audio, currentTime = audio.currentTime, duration = audio.duration, percent = Math.floor(currentTime * 100 / duration);
                _this.markable.value = String(percent);
                _this.mark();
                if (_this.fill) {
                    _this.element.style.backgroundImage = "linear-gradient(90deg, " + _this.fill + " " + percent + "%, transparent " + percent + "%)";
                }
            };
            var audio = element.querySelector("audio"), mark = this.markable;
            switch (element.getAttribute(this.selector)) {
                case "reset":
                    this.audioType = 1;
                    break;
                case "play":
                default:
                    this.audioType = 0;
                    if (element.hasAttribute(this.selector + "-fill")) {
                        this.fill = element.getAttribute(this.selector + "-fill");
                    }
                    break;
            }
            if (!audio) {
                this.findAudio = true;
                this.addState(["disabled", "empty"]);
            }
            else {
                this.audio = audio;
                this.setupListeners();
            }
            if (mark.points === undefined) {
                mark.points = -1;
            }
            this.on(".screen")
                .on(".timeout");
        }
        AudioWidget.prototype.setupListeners = function () {
            var audio = this.audio;
            if (audio && this.audioType === 0) {
                audio.addEventListener("playing", this.onPlaying);
                audio.addEventListener("pause", this.onPause);
                audio.addEventListener("ended", this.onEnded);
                audio.addEventListener("timeupdate", this.onTimeupdate);
                this.onTimeupdate();
            }
        };
        AudioWidget.prototype.onClick = function () {
            var audio = this.audio;
            if (audio) {
                if (this.audioType === 0) {
                    if (this.hasState("playing")) {
                        audio.pause();
                    }
                    else {
                        audio.play();
                    }
                }
                else if (this.audioType === 1) {
                    audio.pause();
                    audio.currentTime = 0;
                }
            }
            _super.prototype.onClick.call(this);
        };
        AudioWidget.prototype.onScreen = function (screen) {
            var audio = this.audio;
            if (audio && this.audioType === 0) {
                audio.pause();
            }
            if (this.findAudio) {
                audio = screen.element.querySelector("audio");
                this.toggleState(["disabled", "empty"], !audio);
                if (audio) {
                    this.audio = audio;
                    this.setupListeners();
                }
            }
        };
        AudioWidget.prototype.onTimeout = function () {
            if (this.audio && this.audioType === 0) {
                this.audio.pause();
            }
        };
        AudioWidget.selector = "ic-audio";
        return AudioWidget;
    }(ic.ButtonWidget));
    ic.AudioWidget = AudioWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var DraggableWidget = (function (_super) {
        __extends(DraggableWidget, _super);
        function DraggableWidget(element) {
            var _this = this;
            _super.call(this, element);
            this.autoLeftCount = 0;
            this.autoTopCount = 0;
            this.droppables = [];
            this.clones = [];
            this.preLoaded = [];
            this.autoscroll = function () {
                var el = _this.scrollElement;
                if (el) {
                    var oldLeft = el.scrollLeft, oldTop = el.scrollTop;
                    el.scrollLeft += _this.autoLeftDelta * (1 + Math.min(4, ++_this.autoLeftCount * 0.1));
                    el.scrollTop += _this.autoTopDelta * (1 + Math.min(4, ++_this.autoTopCount * 0.1));
                    if (oldLeft !== el.scrollLeft || oldTop !== el.scrollTop) {
                        ic.rAF("autoscroll", _this.autoscroll);
                    }
                }
            };
            var mark = this.markable;
            this.originalElement = element;
            if (!mark.value) {
                mark.value = element.innerText.replace(/[^\x20-\x7E]+/g, "").trim() || ("drag" + this.index);
            }
            if (element.querySelector(":scope>ic-pins>ic-pin")) {
                this.hasPins = true;
            }
            else {
                var dropElement = element.closest(ic.DroppableWidget.selector);
                if (dropElement) {
                    var drop = dropElement.icWidget;
                    this.droppables.push(drop);
                    drop.draggables.push(this);
                    drop.update();
                }
                switch (element.getAttribute(DraggableWidget.selector)) {
                    case "copy":
                        this.isCloneable = true;
                        break;
                }
            }
        }
        DraggableWidget.prototype.startup = function () {
            _super.prototype.startup.call(this);
            this.getPins();
            this.startDroppables = this.droppables.clone();
        };
        DraggableWidget.prototype.includes = function (drop) {
            return this.droppables.includes(drop);
        };
        DraggableWidget.prototype.fixElement = function (event) {
            this.element = event.target.closest(DraggableWidget.selector) || this.originalElement;
        };
        DraggableWidget.prototype.onMouseEnter = function (event) {
            this.fixElement(event);
            _super.prototype.onMouseEnter.call(this, event);
        };
        DraggableWidget.prototype.onMouseDown = function (event) {
            this.fixElement(event);
            _super.prototype.onMouseDown.call(this, event);
            if (event.which === 1 && !this.hasState("disabled")) {
                this.dragData = {
                    drag: this,
                    clientY: event.clientY,
                    clientX: event.clientX,
                    top: 0,
                    left: 0,
                    height: 0,
                    width: 0,
                    offsetY: 0,
                    offsetX: 0,
                    target: event.target.closest(DraggableWidget.selector)
                };
                this.on("!mousemove")
                    .on("!touchmove")
                    .on("!mouseup")
                    .on("!touchend");
            }
        };
        DraggableWidget.prototype.getHelper = function (event, data, within) {
            if (this.isCloneable) {
                var clone, original = this.originalElement, target = data.target;
                if (!target || original === target) {
                    clone = original.cloneNode(true);
                    clone.icWidget = this;
                    original.parentElement.insertBefore(clone, original);
                    this.removeState(["click", "hover", "dragging"]);
                    this.element = clone;
                }
                else {
                    clone = target;
                }
                this.cloneElement = clone;
            }
            var el = this.cloneElement || this.originalElement, style = el.style, rect = el.getBoundingClientRect();
            el.classList.add("drag-helper");
            style.top = style.left = "0px";
            var fixedRect = el.getBoundingClientRect();
            data.offsetY = fixedRect.top - rect.top;
            data.offsetX = fixedRect.left - rect.left;
            data.height = fixedRect.height;
            data.width = fixedRect.width;
            if (data.clientX > rect.left + fixedRect.width) {
                data.offsetX -= event.clientX - (rect.left + (fixedRect.width / 2));
            }
            if (data.clientY > rect.top + fixedRect.height) {
                data.offsetY -= event.clientY - (rect.top + (fixedRect.height / 2));
            }
            return el;
        };
        DraggableWidget.prototype.onHelperMove = function (event, data, within) {
            var el = this.cloneElement || this.originalElement, style = el.style;
            style.top = Math.range(within.top - data.top, event.clientY - data.clientY - data.offsetY, within.bottom - data.height - data.top) + (data.offsetTop || 0) + "px";
            style.left = Math.range(within.left - data.left, event.clientX - data.clientX - data.offsetX, within.right - data.width - data.left) + (data.offsetLeft || 0) + "px";
        };
        DraggableWidget.prototype.freeHelper = function (event, drag, drop) {
            var el = this.cloneElement || this.originalElement, style = el.style;
            el.classList.remove("drag-helper");
            if (this.isCloneable) {
                this.cloneElement = null;
                if (!drop || el.parentElement === this.originalElement.parentElement) {
                    el.parentElement.removeChild(el);
                }
                else {
                    this.clones.pushOnce(el);
                    this.on("mouseenter", el)
                        .on("mouseleave", el)
                        .on("mousedown", el)
                        .on("touchstart", el)
                        .on("mouseup", el)
                        .on("touchend", el);
                    style.position = "";
                    this.fixState();
                }
                this.element = this.originalElement;
            }
            else if (!drop) {
                style.top = style.left = "";
            }
        };
        DraggableWidget.prototype.getParentRect = function () {
            return this.screenWidget.getBoundingClientRect();
        };
        DraggableWidget.prototype.getPins = function () {
            var _this = this;
            if (this.hasPins && !this.pins) {
                this.pins = this.get(this, ic.PinWidget).filter(function (pin) { return pin.parent === _this; });
            }
        };
        DraggableWidget.prototype.getPin = function (event) {
            var _this = this;
            this.getPins();
            var available = this.pins.filter(function (pin) { return !pin.target || (!_this.preLoaded.includes(pin.drop) && !pin.drop.hasState("disabled")); }), underMouse = (event && available.filter(function (pin) { return pin.inRect(event.clientY, event.clientX) || (pin.target && pin.target.inRect(event.clientY, event.clientX)); }));
            if (underMouse && underMouse.length) {
                available = underMouse;
            }
            var index = available.indexOf(this.lastPin) + 1;
            return this.lastPin = this.usePin
                || available.find(function (pin) { return !pin.target; })
                || available[index >= available.length ? 0 : index];
        };
        DraggableWidget.prototype.allMouseMove = function (event) {
            var _this = this;
            _super.prototype.allMouseMove.call(this, event);
            var dragData = this.dragData;
            if (!this.targets
                && (Math.abs(dragData.clientY - event.clientY) > 5
                    || Math.abs(dragData.clientX - event.clientX) > 5)) {
                this.targets = this.get(this.parentWidget, ic.DroppableWidget, this.group || null).filter(function (drop) {
                    return drop.canAccept(_this);
                });
                this.addState("dragging");
                if (this.hasPins) {
                    this.usePin = this.helper = this.getPin(event);
                }
                if (!this.helper) {
                    this.helper = this;
                }
                this.helper.getHelper(event, dragData, this.getParentRect());
            }
            if (this.targets) {
                this.helper.onHelperMove(event, dragData, this.getParentRect());
                this.drop = null;
                this.targets.forEach(function (widget) {
                    var over = widget.inRect(event.clientY, event.clientX);
                    if (over) {
                        _this.drop = widget;
                    }
                    widget.onDragHover(_this, over, event);
                });
                for (var autoscroll = document.elementFromPoint(event.clientX, event.clientY); autoscroll; autoscroll = autoscroll.parentElement) {
                    var width = autoscroll.scrollWidth > autoscroll.clientWidth, height = autoscroll.scrollHeight > autoscroll.clientHeight;
                    if (width || height) {
                        var style = getComputedStyle(autoscroll), scrollX = width && /(auto|scroll)/i.test(style.overflowX), scrollY = height && /(auto|scroll)/i.test(style.overflowY);
                        if (scrollX || scrollY) {
                            var rect = autoscroll.getBoundingClientRect(), fontSize = parseFloat(style.fontSize), left = !scrollX
                                ? 0
                                : event.clientX < rect.left + fontSize && autoscroll.scrollLeft > 0
                                    ? -1
                                    : event.clientX > rect.right - fontSize && autoscroll.scrollLeft < autoscroll.scrollWidth - autoscroll.clientWidth
                                        ? 1
                                        : 0, top = !scrollY
                                ? 0
                                : event.clientY < rect.top + fontSize && autoscroll.scrollTop > 0
                                    ? -1
                                    : event.clientY > rect.bottom - fontSize && autoscroll.scrollTop < autoscroll.scrollHeight - autoscroll.clientHeight
                                        ? 1
                                        : 0;
                            if (left || top) {
                                this.autoLeftDelta = left;
                                this.autoTopDelta = top;
                                ic.rAF("autoscroll", this.autoscroll);
                                break;
                            }
                        }
                    }
                }
                if (this.scrollElement !== autoscroll) {
                    this.scrollElement = autoscroll;
                    this.autoLeftCount = this.autoTopCount = 0;
                }
                event.preventDefault();
                return false;
            }
        };
        DraggableWidget.prototype.allMouseUp = function (event) {
            var _this = this;
            _super.prototype.allMouseUp.call(this, event);
            this.scrollElement = null;
            if (this.targets) {
                var drop = this.drop, helper = this.helper;
                drop = drop && drop.onDragDrop(this, helper, event) ? drop : null;
                helper.freeHelper(event, this, drop);
                this.targets.forEach(function (widget) {
                    widget.onDragEnd(_this, event, drop);
                });
                this.removeState("dragging");
                if (helper !== this) {
                    this.removeState("hover");
                }
                this.targets = this.helper = this.drop = this.usePin = null;
            }
            this.off("!mousemove", "!touchmove", "!mouseup", "!touchend");
        };
        DraggableWidget.prototype.mark = function () {
            return;
        };
        DraggableWidget.selector = "ic-draggable";
        return DraggableWidget;
    }(ic.ButtonWidget));
    ic.DraggableWidget = DraggableWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var DropdownWidget = (function (_super) {
        __extends(DropdownWidget, _super);
        function DropdownWidget() {
            _super.apply(this, arguments);
        }
        DropdownWidget.selector = "ic-dropdown";
        return DropdownWidget;
    }(ic.ButtonWidget));
    ic.DropdownWidget = DropdownWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var FeedbackButtonWidget = (function (_super) {
        __extends(FeedbackButtonWidget, _super);
        function FeedbackButtonWidget() {
            _super.apply(this, arguments);
        }
        FeedbackButtonWidget.prototype.onClick = function (event) {
            var feedback = this.get(this.screenWidget, ic.FeedbackWidget).first() || this.get(ic.FeedbackWidget).first();
            if (feedback) {
                feedback.toggleFeedback();
                ;
            }
            _super.prototype.onClick.call(this, event);
        };
        FeedbackButtonWidget.selector = "ic-button[ic-button=feedback]";
        return FeedbackButtonWidget;
    }(ic.ButtonWidget));
    ic.FeedbackButtonWidget = FeedbackButtonWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var FirstButtonWidget = (function (_super) {
        __extends(FirstButtonWidget, _super);
        function FirstButtonWidget(element) {
            _super.call(this, element);
            this.on(".state");
        }
        FirstButtonWidget.prototype.onClick = function (event) {
            var anchor = this.get(ic.AnchorWidget).first();
            if (anchor) {
                anchor.parentWidget.addState("active");
            }
            _super.prototype.onClick.call(this, event);
        };
        FirstButtonWidget.prototype.onState = function (widget, stateList) {
            if (widget instanceof ic.ActivityWidget && stateList.includes("active")) {
                var anchor = this.get(ic.AnchorWidget).first();
                this.toggleState("disabled", anchor ? anchor.hasState("active") : false);
            }
        };
        FirstButtonWidget.selector = "ic-button[ic-button=first]";
        return FirstButtonWidget;
    }(ic.ButtonWidget));
    ic.FirstButtonWidget = FirstButtonWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var FirstScreenButtonWidget = (function (_super) {
        __extends(FirstScreenButtonWidget, _super);
        function FirstScreenButtonWidget(element) {
            _super.call(this, element);
            this.on(".state");
        }
        FirstScreenButtonWidget.prototype.onClick = function () {
            var screen = this.get(ic.ScreenWidget).first();
            if (screen) {
                screen.addState("active");
            }
            _super.prototype.onClick.call(this);
        };
        FirstScreenButtonWidget.prototype.onState = function (widget, stateList) {
            if (widget instanceof ic.ScreenWidget && stateList.includes("active")) {
                this.toggleState("disabled", this.get(ic.ScreenWidget).first().hasState("active"));
            }
        };
        FirstScreenButtonWidget.selector = "ic-button[ic-button=firstscreen]";
        return FirstScreenButtonWidget;
    }(ic.ButtonWidget));
    ic.FirstScreenButtonWidget = FirstScreenButtonWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var LastButtonWidget = (function (_super) {
        __extends(LastButtonWidget, _super);
        function LastButtonWidget(element) {
            _super.call(this, element);
            this.on(".state");
        }
        LastButtonWidget.prototype.onClick = function () {
            var anchor = this.get(ic.AnchorWidget).last();
            if (anchor) {
                anchor.parentWidget.addState("active");
            }
            _super.prototype.onClick.call(this);
        };
        LastButtonWidget.prototype.onState = function (widget, stateList) {
            if (widget instanceof ic.ActivityWidget && stateList.includes("active")) {
                var anchor = this.get(ic.AnchorWidget).last();
                this.toggleState("disabled", anchor ? anchor.hasState("active") : false);
            }
        };
        LastButtonWidget.selector = "ic-button[ic-button=last]";
        return LastButtonWidget;
    }(ic.ButtonWidget));
    ic.LastButtonWidget = LastButtonWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var LastScreenButtonWidget = (function (_super) {
        __extends(LastScreenButtonWidget, _super);
        function LastScreenButtonWidget(element) {
            _super.call(this, element);
            this.on(".state");
        }
        LastScreenButtonWidget.prototype.onClick = function () {
            var screen = this.get(ic.ScreenWidget).last();
            if (screen) {
                screen.addState("active");
            }
            _super.prototype.onClick.call(this);
        };
        LastScreenButtonWidget.prototype.onState = function (widget, stateList) {
            if (widget instanceof ic.ScreenWidget && stateList.includes("active")) {
                this.toggleState("disabled", this.get(ic.ScreenWidget).last().hasState("active"));
            }
        };
        LastScreenButtonWidget.selector = "ic-button[ic-button=lastscreen]";
        return LastScreenButtonWidget;
    }(ic.ButtonWidget));
    ic.LastScreenButtonWidget = LastScreenButtonWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var MarkButtonWidget = (function (_super) {
        __extends(MarkButtonWidget, _super);
        function MarkButtonWidget() {
            _super.apply(this, arguments);
        }
        MarkButtonWidget.prototype.onClick = function () {
            var root = this.screenWidget || this.rootWidget;
            if (root) {
                root.toggleState("marked");
            }
            _super.prototype.onClick.call(this);
        };
        MarkButtonWidget.selector = "ic-button[ic-button=mark]";
        return MarkButtonWidget;
    }(ic.ButtonWidget));
    ic.MarkButtonWidget = MarkButtonWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var LeftButtonWidget = (function (_super) {
        __extends(LeftButtonWidget, _super);
        function LeftButtonWidget(element) {
            var _this = this;
            _super.call(this, element);
            this.scroll = function () {
                if (_this.hasState("click")) {
                    _this.delta = Math.min(4, _this.delta + 0.01);
                    _this.nav.scrollLeft = _this.start + Math.floor(_this.delta * _this.tick++);
                    ic.Widget.trigger(".scroll", _this.nav.icWidget);
                    ic.rAF("left", _this.scroll);
                }
            };
            for (var el = element.parentElement.firstElementChild; el; el = el.nextElementSibling) {
                if (el.tagName === "IC-NAV") {
                    break;
                }
            }
            if (el) {
                this.nav = el;
                this.on(".scroll");
            }
            else {
                this.addState("disabled");
            }
        }
        LeftButtonWidget.prototype.startup = function () {
            _super.prototype.startup.call(this);
        };
        LeftButtonWidget.prototype.onMouseDown = function (event) {
            _super.prototype.onMouseDown.call(this, event);
            this.tick = 0;
            this.start = this.nav.scrollLeft;
            this.delta = 1;
            ic.rAF("left", this.scroll);
        };
        LeftButtonWidget.prototype.onScroll = function (widget) {
            if (widget === this.nav.icWidget) {
                this.toggleState("disabled", this.nav.scrollLeft + this.nav.clientWidth >= this.nav.scrollWidth);
            }
        };
        LeftButtonWidget.selector = "ic-button[ic-button=left]";
        return LeftButtonWidget;
    }(ic.ButtonWidget));
    ic.LeftButtonWidget = LeftButtonWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var RightButtonWidget = (function (_super) {
        __extends(RightButtonWidget, _super);
        function RightButtonWidget(element) {
            var _this = this;
            _super.call(this, element);
            this.scroll = function () {
                if (_this.hasState("click")) {
                    _this.delta = Math.min(4, _this.delta + 0.01);
                    _this.nav.scrollLeft = _this.start - Math.floor(_this.delta * _this.tick++);
                    ic.Widget.trigger(".scroll", _this.nav.icWidget);
                    ic.rAF("right", _this.scroll);
                }
            };
            for (var el = element.parentElement.firstElementChild; el; el = el.nextElementSibling) {
                if (el.tagName === "IC-NAV") {
                    break;
                }
            }
            if (el) {
                this.nav = el;
                this.on(".scroll");
            }
            else {
                this.addState("disabled");
            }
        }
        RightButtonWidget.prototype.startup = function () {
            _super.prototype.startup.call(this);
        };
        RightButtonWidget.prototype.onMouseDown = function (event) {
            _super.prototype.onMouseDown.call(this, event);
            this.tick = 0;
            this.start = this.nav.scrollLeft;
            this.delta = 1;
            ic.rAF("right", this.scroll);
        };
        RightButtonWidget.prototype.onScroll = function (widget) {
            if (widget === this.nav.icWidget) {
                this.toggleState("disabled", this.nav.scrollLeft <= 0);
            }
        };
        RightButtonWidget.selector = "ic-button[ic-button=right]";
        return RightButtonWidget;
    }(ic.ButtonWidget));
    ic.RightButtonWidget = RightButtonWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var NextButtonWidget = (function (_super) {
        __extends(NextButtonWidget, _super);
        function NextButtonWidget(element) {
            _super.call(this, element);
            this.on(".state");
        }
        NextButtonWidget.prototype.onClick = function () {
            var anchors = this.get(ic.AnchorWidget), i = anchors.length - 2;
            while (i >= 0 && !anchors[i].hasState("active")) {
                i--;
            }
            if (anchors.length) {
                anchors[i + 1].parentWidget.addState("active");
            }
            _super.prototype.onClick.call(this);
        };
        NextButtonWidget.prototype.onState = function (widget, stateList) {
            if (widget instanceof ic.ActivityWidget && stateList.includes("active")) {
                var anchor = this.get(ic.AnchorWidget).last();
                this.toggleState("disabled", anchor ? anchor.hasState("active") : false);
            }
        };
        NextButtonWidget.selector = "ic-button[ic-button=next]";
        return NextButtonWidget;
    }(ic.ButtonWidget));
    ic.NextButtonWidget = NextButtonWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var NextScreenButtonWidget = (function (_super) {
        __extends(NextScreenButtonWidget, _super);
        function NextScreenButtonWidget() {
            _super.apply(this, arguments);
        }
        NextScreenButtonWidget.prototype.startup = function () {
            var parent = this.activityWidget || this.screenWidget;
            _super.prototype.startup.call(this);
            if (this.hasAttribute("ic-wait") && this.get(parent, ic.InputWidget).find(function (widget) {
                return widget.element.tagName !== "IC-BUTTON" && widget.markable && widget.markable.points >= 0;
            })) {
                this.isWait = true;
                this
                    .on(".submit")
                    .on(".reset")
                    .addState("disabled");
            }
            else {
                this
                    .toggleState("disabled", this.get(ic.ScreenWidget).last().hasState("active"))
                    .on(".state");
            }
        };
        NextScreenButtonWidget.prototype.onClick = function () {
            var i, screen = this.get(ic.ScreenWidget);
            for (i = 0; i < screen.length - 1; i++) {
                if (screen[i].hasState("active")) {
                    screen[i + 1].addState("active");
                    return;
                }
            }
            if (screen.length) {
                screen.first().addState("active");
            }
            _super.prototype.onClick.call(this);
        };
        NextScreenButtonWidget.prototype.onReset = function (screen) {
            if (!screen || this.screenWidget === screen) {
                this.addState("disabled");
            }
        };
        NextScreenButtonWidget.prototype.onSubmit = function (screen) {
            if (!screen || this.screenWidget === screen) {
                this.toggleState("disabled", this.get(ic.ScreenWidget).last().hasState("active"));
            }
        };
        NextScreenButtonWidget.prototype.onState = function (widget, stateList) {
            if (widget instanceof ic.ScreenWidget && stateList.includes("active")) {
                this.toggleState("disabled", this.get(ic.ScreenWidget).last().hasState("active"));
            }
        };
        NextScreenButtonWidget.selector = "ic-button[ic-button=nextscreen]";
        return NextScreenButtonWidget;
    }(ic.ButtonWidget));
    ic.NextScreenButtonWidget = NextScreenButtonWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var PrevButtonWidget = (function (_super) {
        __extends(PrevButtonWidget, _super);
        function PrevButtonWidget(element) {
            _super.call(this, element);
            this.on(".state");
        }
        PrevButtonWidget.prototype.onClick = function () {
            var i, anchors = this.get(ic.AnchorWidget);
            for (i = 1; i < anchors.length; i++) {
                if (anchors[i].hasState("active")) {
                    anchors[i - 1].parentWidget.addState("active");
                    return;
                }
            }
            _super.prototype.onClick.call(this);
        };
        PrevButtonWidget.prototype.onState = function (widget, stateList) {
            if (widget instanceof ic.ActivityWidget && stateList.includes("active")) {
                var anchor = this.get(ic.AnchorWidget).first();
                this.toggleState("disabled", anchor ? anchor.hasState("active") : false);
            }
        };
        PrevButtonWidget.selector = "ic-button[ic-button=prev]";
        return PrevButtonWidget;
    }(ic.ButtonWidget));
    ic.PrevButtonWidget = PrevButtonWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var PrevScreenButtonWidget = (function (_super) {
        __extends(PrevScreenButtonWidget, _super);
        function PrevScreenButtonWidget(element) {
            _super.call(this, element);
            this.on(".state");
        }
        PrevScreenButtonWidget.prototype.onClick = function () {
            var i, screen = this.get(ic.ScreenWidget);
            for (i = 1; i < screen.length; i++) {
                if (screen[i].hasState("active")) {
                    screen[i - 1].addState("active");
                    return;
                }
            }
            _super.prototype.onClick.call(this);
        };
        PrevScreenButtonWidget.prototype.onState = function (widget, stateList) {
            if (widget instanceof ic.ScreenWidget && stateList.includes("active")) {
                this.toggleState("disabled", this.get(ic.ScreenWidget).first().hasState("active"));
            }
        };
        PrevScreenButtonWidget.selector = "ic-button[ic-button=prevscreen]";
        return PrevScreenButtonWidget;
    }(ic.ButtonWidget));
    ic.PrevScreenButtonWidget = PrevScreenButtonWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var StateButtonWidget = (function (_super) {
        __extends(StateButtonWidget, _super);
        function StateButtonWidget() {
            _super.apply(this, arguments);
        }
        StateButtonWidget.prototype.startup = function () {
            _super.prototype.startup.call(this);
            var parent = this.activityWidget || this.screenWidget, disabled = false;
            if (parent) {
                if (!this.get(parent, ic.InputWidget).find(function (widget) {
                    return widget.element.tagName !== "IC-BUTTON";
                })) {
                    this.addState("disabled");
                    disabled = true;
                }
            }
            else {
                this.on(".screen");
            }
            this.startState = {};
            this.startState["disabled"] = disabled;
        };
        StateButtonWidget.prototype.onScreen = function (screen) {
            this.toggleState("disabled", !this.get(screen, ic.InputWidget).filter(function (widget) {
                return widget.element.tagName !== "IC-BUTTON";
            }).length);
        };
        StateButtonWidget.selector = undefined;
        return StateButtonWidget;
    }(ic.ButtonWidget));
    ic.StateButtonWidget = StateButtonWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var ResetButtonWidget = (function (_super) {
        __extends(ResetButtonWidget, _super);
        function ResetButtonWidget() {
            _super.apply(this, arguments);
        }
        ResetButtonWidget.prototype.startup = function () {
            _super.prototype.startup.call(this);
            var parent = this.activityWidget || this.screenWidget;
            if (parent && !this.get(parent, ic.InputWidget).filter(function (widget) {
                return widget.element.tagName !== "IC-BUTTON";
            }).length) {
                this.addState("disabled");
            }
            else {
                this.on(".screen");
            }
        };
        ResetButtonWidget.prototype.onClick = function () {
            var screen = this.screenWidget;
            (screen || this.rootWidget).removeState(["marked", "reveal"]);
            ic.Widget.trigger(".reset", screen);
            _super.prototype.onClick.call(this);
        };
        ResetButtonWidget.prototype.onScreen = function (screen) {
            this.toggleState("disabled", !this.get(screen, ic.InputWidget).filter(function (widget) {
                return widget.element.tagName !== "IC-BUTTON";
            }).length);
        };
        ResetButtonWidget.selector = "ic-button[ic-button=reset]";
        return ResetButtonWidget;
    }(ic.ButtonWidget));
    ic.ResetButtonWidget = ResetButtonWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var RevealButtonWidget = (function (_super) {
        __extends(RevealButtonWidget, _super);
        function RevealButtonWidget() {
            _super.apply(this, arguments);
        }
        RevealButtonWidget.prototype.startup = function () {
            _super.prototype.startup.call(this);
            if (!this.hasState("disabled")) {
                this
                    .on(".submit")
                    .on(".reset")
                    .on(".reveal");
            }
        };
        RevealButtonWidget.prototype.onClick = function () {
            var screen = this.screenWidget, root = screen || this.rootWidget;
            if (root && !root.hasState("reveal")) {
                root.toggleState({
                    "marked": false,
                    "reveal": true
                });
                ic.Widget.trigger(".reveal", screen);
            }
            _super.prototype.onClick.call(this);
        };
        RevealButtonWidget.prototype.onSubmit = function (screen) {
            if (!screen || this.screenWidget === screen) {
                this.removeState("disabled");
            }
        };
        RevealButtonWidget.prototype.onReset = function (screen) {
            if (!screen || this.screenWidget === screen) {
                this.addState("disabled");
            }
        };
        RevealButtonWidget.prototype.onReveal = function (screen) {
            if (!screen || this.screenWidget === screen) {
                this.addState("disabled");
            }
        };
        RevealButtonWidget.selector = "ic-button[ic-button=reveal]";
        return RevealButtonWidget;
    }(ic.StateButtonWidget));
    ic.RevealButtonWidget = RevealButtonWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var SubmitButtonWidget = (function (_super) {
        __extends(SubmitButtonWidget, _super);
        function SubmitButtonWidget() {
            var _this = this;
            _super.apply(this, arguments);
            this.checkSubmit = function () {
                var state = (_this.screenWidget || _this.rootWidget).hasState(["marked", "reveal"]), disabled = _this.hasState("disabled");
                if (!state) {
                    var widgets = _this.get(_this.screenWidget, ic.InputWidget);
                    if (_this.needSome) {
                        state = !widgets.some(function (widget) {
                            var points = widget.markable.points;
                            return widget.element.tagName !== "IC-BUTTON" && !ic.isUndefined(points) && points >= 0 && widget.hasAnswered();
                        });
                    }
                    else {
                        state = !widgets.every(function (widget) {
                            var points = widget.markable.points;
                            return widget.element.tagName === "IC-BUTTON" || ic.isUndefined(points) || points < 0 || widget.hasAnswered();
                        });
                    }
                }
                else {
                    state = disabled;
                }
                _this.toggleState("disabled", state);
            };
        }
        SubmitButtonWidget.prototype.startup = function () {
            _super.prototype.startup.call(this);
            if (!this.hasState("disabled")) {
                var attr = this.element.getAttribute("ic-submit");
                if (attr === "any" || attr === "some" || this.get(this.activityWidget || this.screenWidget, ic.DroppableWidget).length) {
                    this.needSome = true;
                }
                this
                    .on(".state")
                    .on(".submit")
                    .on(".reset")
                    .on(".reveal")
                    .addState("disabled");
            }
        };
        SubmitButtonWidget.prototype.onClick = function () {
            var screen = this.screenWidget, root = screen || this.rootWidget;
            if (!root.hasState(["marked", "reveal"])) {
                var activity = this.activityWidget;
                if (activity) {
                    activity.attempts = (activity.attempts || 0) + 1;
                }
                if (screen) {
                    screen.attempts = (screen.attempts || 0) + 1;
                }
                if (screen !== root) {
                    root.attempts = (root.attempts || 0) + 1;
                }
                root.addState("marked");
                ic.Widget.trigger(".submit", screen);
            }
            _super.prototype.onClick.call(this);
        };
        SubmitButtonWidget.prototype.onSubmit = function (screen) {
            if (!screen || this.screenWidget === screen) {
                this.addState("disabled");
            }
        };
        SubmitButtonWidget.prototype.onReset = function (screen) {
            if (!screen || this.screenWidget === screen) {
                this.addState("disabled");
            }
        };
        SubmitButtonWidget.prototype.onReveal = function (screen) {
            if (!screen || this.screenWidget === screen) {
                this.addState("disabled");
            }
        };
        SubmitButtonWidget.prototype.onState = function (widget, stateList) {
            var screen = this.screenWidget;
            if ((!screen
                || widget.screenWidget === screen
                || (widget === screen && (stateList.includes("marked") || stateList.includes("reveal"))))
                && widget instanceof ic.InputWidget) {
                ic.setImmediate("submit", this.checkSubmit);
            }
        };
        SubmitButtonWidget.selector = "ic-button[ic-button=submit]";
        return SubmitButtonWidget;
    }(ic.StateButtonWidget));
    ic.SubmitButtonWidget = SubmitButtonWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    (function (ToggleType) {
        ToggleType[ToggleType["SINGLE"] = 0] = "SINGLE";
        ToggleType[ToggleType["MULTIPLE"] = 1] = "MULTIPLE";
        ToggleType[ToggleType["RANGE"] = 2] = "RANGE";
        ToggleType[ToggleType["MULTIRANGE"] = 3] = "MULTIRANGE";
    })(ic.ToggleType || (ic.ToggleType = {}));
    var ToggleType = ic.ToggleType;
    var ToggleWidget = (function (_super) {
        __extends(ToggleWidget, _super);
        function ToggleWidget(element) {
            _super.call(this, element);
            this.toggleType = 0;
            var mark = this.markable, selector = ToggleWidget.selector.split(",").first();
            switch (element.getAttribute(selector)) {
                case "double":
                    this.toggleType = 1;
                    this.count = 2;
                    break;
                case "multiple":
                    this.toggleType = 1;
                    this.count = parseInt(element.getAttribute(selector + "-count"), 10) || 0;
                    break;
                case "range":
                    this.toggleType = 2;
                    break;
                case "multirange":
                    this.toggleType = 3;
                    break;
            }
            if (mark.points === undefined) {
                if (this.toggleType === 2 || this.toggleType === 3) {
                    var text = element.innerText;
                    mark.points = text && /[^ .,:;'"]/.test(text) ? 1 : -1;
                }
                else {
                    mark.points = 1;
                }
            }
            this
                .toggleState("checked", mark.value === "checked")
                .on(".state");
            if (element.tagName.toLowerCase() === selector) {
                this.on(".persist");
            }
            else {
                this.on(".timeout");
            }
        }
        ToggleWidget.prototype.startup = function () {
            _super.prototype.startup.call(this);
            var parent = this.activityWidget || this.screenWidget;
            if ((this.toggleType === 2 || this.toggleType === 3)
                && parent) {
                if (!parent.toggleRange) {
                    parent.toggleRange = {};
                }
                if (!parent.toggleRange[this.group]) {
                    var wanted = this.toggleType;
                    parent.toggleRange[this.group] = this.get(parent, ToggleWidget, this.group, true).filter(function (toggle) {
                        if (toggle.toggleType !== 2 && toggle.toggleType !== 3) {
                            return false;
                        }
                        if (toggle.toggleType !== wanted) {
                            console.error("Error: Mismatched ic-toggle=\"" + (wanted === 3 ? "multi" : "") + "range\" type:", toggle);
                        }
                        return true;
                    });
                }
            }
            if (this.startState) {
                this.startState["checked"] = this.startState["checked"] || false;
                this.on(".reveal");
            }
        };
        ToggleWidget.prototype.hasAnswered = function () {
            return !!this.answered;
        };
        ToggleWidget.prototype.onPersist = function (state) {
            if (ic.isUndefined(state)) {
                return this.hasState("checked") ? 1 : 0;
            }
            this.toggleState("checked", !!state);
        };
        ToggleWidget.prototype.onReveal = function (screen) {
            if ((!screen || this.screenWidget === screen)) {
                this
                    .toggleState("checked", this.markable.answer === "checked")
                    .mark();
            }
        };
        ToggleWidget.prototype.onReset = function (screen) {
            _super.prototype.onReset.call(this, screen);
            if ((!screen || this.screenWidget === screen)) {
                this.answered = false;
            }
        };
        ToggleWidget.prototype.startDragging = function (click) {
            var _this = this;
            var range = (this.activityWidget || this.screenWidget).toggleRange[this.group];
            if (!click) {
                this.startDrag = true;
                this.toggleState("checked")
                    .parentWidget.addState("attempted");
            }
            if (this.toggleType === 2) {
                var index = range.indexOf(this), isChecked = this.hasState("checked"), prevChecked = index === 0 ? isChecked : !range[index - 1].hasState("checked"), nextChecked = index === range.length - 1 ? isChecked : !range[index + 1].hasState("checked");
                if ((isChecked && prevChecked && nextChecked)
                    || (!isChecked && prevChecked === nextChecked)) {
                    range.forEach(function (toggle) {
                        if (toggle !== _this) {
                            toggle.removeState("checked");
                        }
                    });
                }
                if (!isChecked && prevChecked === nextChecked) {
                    this.toggleState("checked");
                }
            }
            range.forEach(function (toggle) {
                toggle.startChecked = toggle.hasState("checked");
            });
        };
        ToggleWidget.prototype.finishDragging = function () {
            var checked = false, attempted = false, range = (this.activityWidget || this.screenWidget).toggleRange[this.group], fixDangling = function (toggle) {
                var isChecked = toggle.hasState("checked");
                attempted = attempted || isChecked;
                if (!checked && toggle.markable.points === -1 && isChecked) {
                    toggle.removeState("checked");
                }
                else {
                    checked = isChecked;
                }
            };
            range.forEach(fixDangling);
            checked = attempted = false;
            range.reverse().forEach(fixDangling);
            if (attempted && this.parentWidget) {
                this.parentWidget.addState("attempted");
            }
        };
        ToggleWidget.prototype.onMouseDown = function (event) {
            this.startDrag = false;
            _super.prototype.onMouseDown.call(this, event);
        };
        ;
        ToggleWidget.prototype.onClick = function () {
            if (!this.startDrag) {
                if (this.toggleType === 1 && this.count) {
                    var count = 0;
                    this.get(this.activityWidget, ToggleWidget, this.group, false).forEach(function (widget) {
                        if (widget.hasState("checked")) {
                            count++;
                        }
                    });
                    if (count >= this.count) {
                        return;
                    }
                }
                var toggles = this.get(this.activityWidget, ToggleWidget, this.group, false), answered = this.toggleState("checked").hasState("checked");
                if (this.toggleType === 0) {
                    this.answered = answered;
                    toggles.forEach(function (widget) {
                        if (widget.toggleType === 0) {
                            widget.answered = answered;
                            widget.removeState("checked");
                        }
                    });
                }
                else {
                    if (this.toggleType === 2 || this.toggleType === 3) {
                        this.startDragging(true);
                        this.finishDragging();
                    }
                    answered = this.answered = this.hasState("checked") || toggles.some(function (widget) {
                        return widget.answered;
                    });
                    toggles.forEach(function (widget) {
                        widget.answered = answered;
                    });
                    this.answered = answered;
                }
                if (this.parentWidget) {
                    this.parentWidget.addState("attempted");
                }
                _super.prototype.onClick.call(this);
            }
        };
        ToggleWidget.prototype.onMouseEnter = function (event) {
            var parent = this.activityWidget || this.screenWidget;
            if (ic.ButtonWidget.pressed instanceof ToggleWidget
                && (this.toggleType === 2 || this.toggleType === 3)
                && parent) {
                var range = parent.toggleRange[this.group], first = ic.ButtonWidget.pressed, start = range.indexOf(first), end = range.indexOf(this);
                if (start !== -1) {
                    if (!first.startDrag) {
                        first.startDragging();
                    }
                    if (start !== end) {
                        var min = Math.min(start, end), max = Math.max(start, end), checked = ic.ButtonWidget.pressed.hasState("checked");
                        range.forEach(function (toggle, index) {
                            toggle.toggleState("checked", index >= min && index <= max ? checked : toggle.startChecked);
                        });
                        this.finishDragging();
                    }
                }
            }
            _super.prototype.onMouseEnter.call(this, event);
        };
        ToggleWidget.prototype.onState = function (widget, stateList) {
            if (this === widget && stateList.includes("checked")) {
                this.markable.value = this.hasState("checked") ? "checked" : "";
                this.mark();
            }
        };
        ToggleWidget.prototype.onTimeout = function () {
            this.removeState("checked");
        };
        ToggleWidget.selector = "ic-toggle,ic-button[ic-button=toggle]";
        return ToggleWidget;
    }(ic.ButtonWidget));
    ic.ToggleWidget = ToggleWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var lastSortable;
    var DroppableWidget = (function (_super) {
        __extends(DroppableWidget, _super);
        function DroppableWidget(element) {
            _super.call(this, element);
            this.draggables = [];
            this.preLoaded = [];
            var mark = this.markable, pinCount = element.querySelectorAll(":scope>ic-pins>ic-pin").length;
            switch (element.getAttribute(DroppableWidget.selector)) {
                case "colour":
                    this.index = -1;
                    this.isColour = true;
                    break;
                case "horizontal":
                    this.index = -1;
                    this.isHorizontal = true;
                    break;
                case "vertical":
                    this.index = -1;
                    this.isVertical = true;
                    break;
                case "position":
                    this.index = -1;
                    this.isHorizontal = true;
                    this.isVertical = true;
                    break;
                case "sortable":
                    this.isSortable = true;
                    break;
            }
            this.count = this.isSortable ? 1 : pinCount || Math.max(parseInt(element.getAttribute(DroppableWidget.selector + "-count"), 10) || element.querySelectorAll(":scope>ic-draggable").length || 1, 1);
            var points = ic.isArray(mark.answer) ? Math.min(this.count, mark.answer.length) : mark.answer || this.isSortable ? 1 : -1;
            if (!pinCount && element.querySelectorAll(":scope>ic-draggable>ic-pins>ic-pin").length) {
                this.dragPins = true;
            }
            if (mark.answer) {
                mark.max = points;
            }
            if (ic.isUndefined(mark.points)) {
                mark.points = points;
            }
            if (this.count > 1) {
                this.index = -1;
            }
        }
        DroppableWidget.prototype.startup = function () {
            var _this = this;
            if (!this.isSortable) {
                var pins = this.get(this, ic.PinWidget).filter(function (pin) { return pin.parent === _this; });
                if (pins.length) {
                    var value = this.markable.value, drags = ic.isString(value) ? [value] : ic.isArray(value) ? value : [];
                    this.pins = pins;
                    this.markable.value = drags;
                    if (drags) {
                        drags.forEach(function (target) {
                            var dragIndex = target.regex(/#drag([0-9]+)#/), drag = _this.get(_this.parentWidget, ic.DraggableWidget, _this.group).find(function (drag) {
                                return drag.index === dragIndex;
                            });
                            if (drag) {
                                _this.preLoaded.push(drag);
                                drag.preLoaded.push(_this);
                                drag.getPin().linkPin(drag, _this);
                            }
                        });
                    }
                }
            }
            this.startDraggables = this.draggables.clone();
            this
                .on(".persist")
                .on(".reveal")
                .update();
            _super.prototype.startup.call(this);
        };
        DroppableWidget.prototype.hasAnswered = function () {
            var mark = this.markable;
            return mark.points < 0 || !this.hasState("empty");
        };
        DroppableWidget.prototype.onPersist = function (state) {
            var _this = this;
            if (ic.isUndefined(state)) {
                state = [];
                this.draggables.forEach(function (drag) {
                    var value = drag.index;
                    if (_this.isHorizontal || _this.isVertical) {
                        var el = (_this.pins ? _this.pins.find(function (pin) {
                            return pin.drag === drag;
                        }) : drag).element;
                        value = [value];
                        if (_this.isHorizontal) {
                            value[1] = parseFloat(el.style.left) || 0;
                        }
                        if (_this.isVertical) {
                            value[2] = parseFloat(el.style.top) || 0;
                        }
                    }
                    state.push(value);
                });
                return state;
            }
            if (ic.isArray(state)) {
                var drags = this.get(this.activityWidget, ic.DraggableWidget), pinElements = [];
                state.forEach(function (value, index) {
                    var values = ic.isArray(value) ? value : [value], drag = drags.find(function (drag) {
                        return drag.index === values[0];
                    });
                    if (drag) {
                        var oldDrop = (drag.element.closest(DroppableWidget.selector) || {}).icWidget;
                        if (oldDrop !== _this) {
                            if (oldDrop) {
                                drag.droppables.remove(oldDrop);
                                oldDrop.draggables.remove(drag);
                            }
                            drag.droppables.pushOnce(_this);
                            _this.draggables.pushOnce(drag);
                            if (_this.pins) {
                                var pin = drag.pins[drag.droppables.indexOf(_this)];
                                pin.linkPin(drag, _this, _this.pins[index]);
                                pinElements.push(pin.element);
                            }
                            else {
                                _this.element.appendChild(drag.originalElement);
                            }
                        }
                        if (_this.isHorizontal || _this.isVertical) {
                            var el = (_this.pins ? _this.pins.find(function (pin) {
                                return pin.drag === drag;
                            }) : drag).element;
                            if (_this.isHorizontal) {
                                el.style.left = values[1] + "%";
                            }
                            if (_this.isVertical) {
                                el.style.top = values[2] + "%";
                            }
                        }
                    }
                });
                this.update();
                ic.drawLine(pinElements, "update");
            }
        };
        DroppableWidget.prototype.removeUnlisted = function (arr) {
            var _this = this;
            var changed = false;
            if (this.pins) {
                this.pins.forEach(function (pin) {
                    if (!arr.includes(pin.drag)) {
                        pin.unlinkPin();
                        changed = true;
                    }
                });
            }
            else if (!this.dragPins) {
                [].forEach.call(this.element.children, function (child) {
                    var drag = child.icWidget;
                    if (drag && drag instanceof ic.DraggableWidget && !arr.includes(drag)) {
                        if (drag.isCloneable && child !== drag.originalElement) {
                            drag.clones.remove(child);
                        }
                        _this.element.removeChild(child);
                        _this.draggables.remove(drag);
                        changed = true;
                    }
                });
            }
            return changed;
        };
        DroppableWidget.prototype.onReset = function (screen) {
            var _this = this;
            if ((!screen || this.screenWidget === screen)) {
                var changed = false;
                this.startDraggables.forEach(function (drag, index) {
                    if (drag) {
                        var oldDrop = (drag.element.closest(DroppableWidget.selector) || {}).icWidget;
                        if (oldDrop !== _this) {
                            if (_this.pins) {
                                _this.pins[index].linkPin(drag, _this, drag.pins[drag.startDroppables.indexOf(_this)]);
                            }
                            else {
                                if (oldDrop) {
                                    drag.droppables.remove(oldDrop);
                                    oldDrop.draggables.remove(drag);
                                    oldDrop.update();
                                }
                                drag.droppables.pushOnce(_this);
                                _this.draggables.pushOnce(drag);
                                _this.element.appendChild(drag.originalElement);
                            }
                            changed = true;
                        }
                    }
                    else if (_this.pins) {
                        _this.pins[index].unlinkPin();
                        changed = true;
                    }
                });
                if (this.removeUnlisted(this.startDraggables) || changed) {
                    this.update();
                }
                _super.prototype.onReset.call(this, screen);
            }
        };
        DroppableWidget.prototype.onReveal = function (screen) {
            var _this = this;
            if ((!screen || this.screenWidget === screen)) {
                var index = 0, changed = false, mark = this.markable, answers = ic.isArray(mark.answer) ? mark.answer : [mark.answer], correct = [];
                if (!mark.answer && mark.points < 0) {
                    correct = this.startDraggables;
                }
                else {
                    answers.forEach(function (answer) {
                        var offset = (answer || "").regex(/#(?:act([1-9][0-9]*))?drag(\d+)#$/);
                        if (index < _this.count && offset) {
                            var parent = offset[0]
                                ? _this.get(_this.screenWidget || _this.rootWidget, ic.ActivityWidget).find(function (activity) {
                                    return activity.index === offset[0] && activity.constructor === ic.ActivityWidget;
                                })
                                : _this.parentWidget, drag = _this.get(parent, ic.DraggableWidget).find(function (drag) {
                                return drag.index === offset[1];
                            });
                            if (drag) {
                                correct.pushOnce(drag);
                                if (!_this.draggables.includes(drag)) {
                                    if (drag.isCloneable) {
                                        var clone = drag.originalElement.cloneNode(true);
                                        clone.icWidget = drag;
                                        drag.droppables.pushOnce(_this);
                                        _this.draggables.pushOnce(drag);
                                        _this.element.appendChild(clone);
                                    }
                                    else {
                                        var oldDrop = (drag.element.closest(DroppableWidget.selector) || {}).icWidget;
                                        if (oldDrop !== _this) {
                                            if (_this.pins) {
                                                _this.pins[index].linkPin(drag, _this, drag.pins[drag.startDroppables.indexOf(_this)]);
                                            }
                                            else {
                                                if (oldDrop) {
                                                    drag.droppables.remove(oldDrop);
                                                    oldDrop.draggables.remove(drag);
                                                    oldDrop.update();
                                                }
                                                drag.droppables.pushOnce(_this);
                                                _this.draggables.pushOnce(drag);
                                                _this.element.appendChild(drag.originalElement);
                                            }
                                            changed = true;
                                        }
                                    }
                                }
                                index++;
                            }
                        }
                    });
                }
                if (this.removeUnlisted(correct) || changed) {
                    this.update();
                }
            }
        };
        DroppableWidget.prototype.includes = function (drag) {
            return this.draggables.includes(drag);
        };
        DroppableWidget.prototype.onMouseEnter = function () {
        };
        DroppableWidget.prototype.onMouseLeave = function () {
        };
        DroppableWidget.prototype.onDragHover = function (drag, over, event) {
            var _this = this;
            if (!this.includes(drag) || this.isHorizontal || this.isVertical || this.isSortable) {
                this.toggleState("hover", over);
            }
            if (over && this.isSortable && lastSortable !== this) {
                lastSortable = this;
                var draggables = [], droppables = this.get(this.parentWidget, DroppableWidget, this.group), index = 0, changed = false;
                droppables.forEach(function (droppable) {
                    if (droppable.isSortable) {
                        droppable.draggables.forEach(function (draggable) {
                            if (draggable !== drag) {
                                draggables.pushOnce(draggable);
                            }
                        });
                    }
                });
                droppables.forEach(function (droppable) {
                    if (droppable !== _this && droppable.isSortable) {
                        var draggable = draggables[index++];
                        if (!droppable.draggables.includes(draggable)) {
                            var oldDrop = draggable.element.closest(DroppableWidget.selector).icWidget;
                            draggable.droppables.remove(oldDrop);
                            draggable.droppables.pushOnce(_this);
                            oldDrop.draggables.remove(draggable);
                            droppable.draggables.pushOnce(draggable);
                            droppable.element.appendChild(draggable.element);
                            droppable.update();
                            changed = true;
                        }
                    }
                });
                if (changed) {
                    this.update();
                }
            }
        };
        DroppableWidget.prototype.getPin = function (event) {
            var _this = this;
            var available = this.pins.filter(function (pin) { return !pin.target || (!_this.preLoaded.includes(pin.drag) && !pin.drag.hasState("disabled")); }), underMouse = (event && available.filter(function (pin) { return pin.inRect(event.clientY, event.clientX) || (pin.target && pin.target.inRect(event.clientY, event.clientX)); }));
            if (underMouse && underMouse.length) {
                available = underMouse;
            }
            var index = available.indexOf(this.lastPin) + 1;
            return this.lastPin = available.find(function (pin) { return !pin.target; })
                || available[index >= available.length ? 0 : index];
        };
        DroppableWidget.prototype.canAccept = function (drag) {
            return !this.hasState("disabled") && !this.pins === !drag.pins;
        };
        DroppableWidget.prototype.onDragDrop = function (drag, helper, event) {
            if (this.includes(drag) && !this.isHorizontal && !this.isVertical) {
                return false;
            }
            if (this.pins) {
                var pin = this.getPin();
                if (this.isHorizontal || this.isVertical) {
                    var percent, top, left, el = helper.helperElement, style = pin.element.style, computedStyle = getComputedStyle(this.element, null), box = /^border-box$/i.test(computedStyle.boxSizing), rect = pin.element.parentElement.getBoundingClientRect(), width = rect.width - (box ? parseFloat(computedStyle.paddingLeft) + parseFloat(computedStyle.paddingRight) : 0), height = rect.height - (box ? parseFloat(computedStyle.paddingTop) + parseFloat(computedStyle.paddingBottom) : 0);
                    computedStyle = getComputedStyle(pin.element, null);
                    style.position = "absolute";
                    el.offsetHeight;
                    oldPos = el.getBoundingClientRect();
                    if (this.isHorizontal) {
                        style.left = String(Math.range(0, (oldPos.left - rect.left - parseFloat(computedStyle.marginLeft)) * 100 / width, 100)) + "%";
                    }
                    else {
                        style.left = "";
                    }
                    if (this.isVertical) {
                        style.top = String(Math.range(0, (oldPos.top - rect.top - parseFloat(computedStyle.marginTop)) * 100 / height, 100)) + "%";
                    }
                    else {
                        style.top = "";
                    }
                }
                drag.getPin().unlinkPin().linkPin(drag, this);
            }
            else {
                if (this.isColour) {
                    if (this.draggables[0] === drag) {
                        this.draggables.pop();
                    }
                    else {
                        this.draggables[0] = drag;
                    }
                }
                else {
                    var newPos, oldPos, isCloneable = drag.isCloneable, el = isCloneable ? drag.cloneElement : drag.originalElement, style = el.style, top = parseFloat(style.top) || 0, left = parseFloat(style.left) || 0, oldDrop = el.closest(DroppableWidget.selector).icWidget, oldDropIsOriginal = !isCloneable || drag.originalElement.parentElement === oldDrop.element, target = [].find.call(this.element.children, function (element) {
                        var rect = element.getBoundingClientRect();
                        return element.icWidget instanceof ic.DraggableWidget && rect.top <= event.clientY && rect.bottom >= event.clientY && rect.left <= event.clientX && rect.right >= event.clientX;
                    }), swap = target ? target.icWidget : null;
                    drag.droppables.pushOnce(this);
                    this.draggables.pushOnce(drag);
                    if (!isCloneable || !oldDropIsOriginal) {
                        drag.droppables.remove(oldDrop);
                        oldDrop.draggables.remove(drag);
                    }
                    style.top = style.left = "";
                    el.offsetHeight;
                    oldPos = el.getBoundingClientRect();
                    if (swap) {
                        if (!isCloneable || (!oldDropIsOriginal && target !== swap.originalElement)) {
                            var nextSibling = el.nextSibling;
                            this.element.insertBefore(el, target.nextSibling);
                            oldDrop.element.insertBefore(target, nextSibling);
                            target.style.top = target.style.left = "";
                            this.draggables.remove(swap);
                            swap.droppables.remove(this);
                            swap.droppables.pushOnce(oldDrop);
                            oldDrop.draggables.pushOnce(swap);
                        }
                        else if (oldDropIsOriginal && target !== swap.originalElement) {
                            this.element.insertBefore(el, target);
                            target.parentElement.removeChild(target);
                            this.draggables.remove(swap);
                            swap.droppables.remove(this);
                        }
                        else if (!oldDropIsOriginal && target === swap.originalElement) {
                            this.draggables.remove(drag);
                            drag.droppables.remove(this);
                            el.parentElement.removeChild(el);
                            el = null;
                        }
                    }
                    else {
                        this.element.appendChild(el);
                    }
                    if (isCloneable && this.group) {
                        this.get(this.parentWidget, DroppableWidget, this.group, false).forEach(function (droppable) {
                            if (droppable.includes(drag)) {
                                [].forEach.call(droppable.element.children, function (el) {
                                    if (el.icWidget === drag) {
                                        el.parentElement.removeChild(el);
                                    }
                                });
                                drag.droppables.remove(droppable);
                                droppable.draggables.remove(drag);
                                droppable.update();
                            }
                        });
                    }
                    if (el) {
                        el.offsetHeight;
                        newPos = el.getBoundingClientRect();
                        if (this.isHorizontal || this.isVertical) {
                            var percent, computedStyle = getComputedStyle(this.element, null), box = /^border-box$/i.test(computedStyle.boxSizing), rect = this.getBoundingClientRect(), width = rect.width - (box ? parseFloat(computedStyle.paddingLeft) + parseFloat(computedStyle.paddingRight) : 0), height = rect.height - (box ? parseFloat(computedStyle.paddingTop) + parseFloat(computedStyle.paddingBottom) : 0);
                            if (this.isHorizontal) {
                                left = Math.max(newPos.left, oldPos.left + left) - Math.min(newPos.left, oldPos.left + left);
                                percent = Math.range(0, left * 100 / (width - newPos.width), 100);
                                style.left = String(percent * (width - newPos.width) / width) + "%";
                            }
                            else {
                                style.left = "";
                            }
                            if (this.isVertical) {
                                top = Math.max(newPos.top, oldPos.top + top) - Math.min(newPos.top, oldPos.top + top);
                                percent = Math.range(0, top * 100 / (height - newPos.height), 100);
                                style.top = String(percent * (height - newPos.height) / height) + "%";
                            }
                            else {
                                style.top = "";
                            }
                        }
                        else {
                            style.top = style.left = "";
                        }
                    }
                    oldDrop.update();
                }
            }
            if (event && this.parentWidget) {
                this.parentWidget.addState("attempted");
            }
            this.update();
            return true;
        };
        DroppableWidget.prototype.onDragRemove = function (drag) {
            var _this = this;
            this.draggables.remove(drag);
            if (this.pins) {
                var pin = this.get(this, ic.PinWidget).find(function (pin) { return pin.drop === _this && pin.drag === drag; });
                if (pin) {
                    pin.unlinkPin(drag, this);
                }
            }
            this.update();
        };
        DroppableWidget.prototype.onDragEnd = function (drag, event, drop) {
            this.toggleState({
                "hover": false,
                "empty": !this.draggables.length
            });
            if (this.isSortable && lastSortable && !drop) {
                var draggables = [], droppables = this.get(this.parentWidget, DroppableWidget, this.group), index = 0, foundZero;
                lastSortable = null;
                droppables.forEach(function (droppable, index) {
                    if (droppable.isSortable) {
                        var drags = droppable.draggables;
                        if (!drags.length) {
                            foundZero = true;
                        }
                        else if (drags.length === 2 && foundZero) {
                            drags = drags.reverse();
                        }
                        draggables.pushOnce.apply(draggables, drags);
                    }
                });
                droppables.forEach(function (droppable) {
                    if (droppable.isSortable) {
                        var draggable = draggables[index++];
                        if (!droppable.draggables.includes(draggable)) {
                            var oldDrop = draggable.element.closest(DroppableWidget.selector).icWidget;
                            oldDrop.draggables.remove(draggable);
                            droppable.draggables.pushOnce(draggable);
                            droppable.element.appendChild(draggable.element);
                            droppable.update();
                        }
                    }
                });
            }
        };
        DroppableWidget.prototype.update = function () {
            var _this = this;
            var mark = this.markable;
            if (this.draggables.length) {
                if (this.isHorizontal || this.isVertical) {
                    var el = this.element, computedStyle = getComputedStyle(el, null), borderBox = /^border-box$/i.test(computedStyle.boxSizing), width = el.clientWidth - (borderBox ? parseFloat(computedStyle.paddingLeft) + parseFloat(computedStyle.paddingRight) : 0), height = el.clientHeight - (borderBox ? parseFloat(computedStyle.paddingTop) + parseFloat(computedStyle.paddingBottom) : 0);
                }
                mark.value = [];
                this.draggables.forEach(function (drag) {
                    var value, el = drag.element, rect = computedStyle ? el.getBoundingClientRect() : null;
                    if ((_this.isHorizontal || _this.isVertical) && !_this.pins) {
                        if (_this.isHorizontal) {
                            var left = value = String((parseFloat(el.style.left) || 0) * width / (width - rect.width));
                        }
                        if (_this.isVertical) {
                            var top = value = String((parseFloat(el.style.top) || 0) * height / (height - rect.height));
                        }
                        if (_this.isHorizontal && _this.isVertical) {
                            value = left + ":" + top;
                        }
                    }
                    else {
                        value = drag.markable.value;
                    }
                    mark.value.push(value);
                });
                if (this.isColour) {
                    this.index = this.draggables.first().index;
                }
                this.removeState("empty");
            }
            else {
                mark.value = undefined;
                this.index = -1;
                this.addState("empty");
            }
            this.mark();
        };
        DroppableWidget.selector = "ic-droppable";
        return DroppableWidget;
    }(ic.InputWidget));
    ic.DroppableWidget = DroppableWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var TextWidget = (function (_super) {
        __extends(TextWidget, _super);
        function TextWidget(element) {
            _super.call(this, element);
            var i, child, children = element.childNodes, mark = this.markable;
            mark.max = 1;
            if (ic.isUndefined(mark.points)) {
                mark.points = 1;
            }
            for (i = 0; i < children.length; i++) {
                child = children[i];
                if (child.nodeType === Node.ELEMENT_NODE) {
                    if (child.tagName === "INPUT") {
                        this.input = child;
                        break;
                    }
                    if (child.tagName === "TEXTAREA") {
                        this.textarea = child;
                        break;
                    }
                }
            }
            if (!this.input && !this.textarea) {
                for (var parent = this.element.parentElement; parent && parent.icWidget !== this.parentWidget; parent = parent.parentElement) {
                    if (parent.tagName === "P") {
                        this.input = document.createElement("INPUT");
                        this.input.setAttribute("type", "text");
                        this.element.appendChild(this.input);
                        break;
                    }
                }
                if (!this.input) {
                    this.textarea = document.createElement("TEXTAREA");
                    this.element.appendChild(this.textarea);
                }
            }
            if (this.element.hasAttribute("data-accept")) {
                this.accept = new RegExp(this.element.getAttribute("data-accept"));
            }
            if (this.element.hasAttribute("data-rx")) {
                this.rx = new RegExp(this.element.getAttribute("data-rx"));
            }
            var input = this.input ? this.input : this.textarea;
            this.startText = mark.value = input.value;
            if (input.hasAttribute("disabled")) {
                this.addState("disabled");
            }
            input.addEventListener("paste", function (event) {
                event.preventDefault();
                return false;
            }, true);
            this.on("mousedown")
                .on("touchstart")
                .on("focus", input)
                .on("blur", input)
                .on("keypress", input)
                .on("keyup", input)
                .on(".persist")
                .on(".state");
        }
        TextWidget.prototype.hasAnswered = function () {
            return !!this.markable.value;
        };
        TextWidget.prototype.onPersist = function (state) {
            if (ic.isUndefined(state)) {
                return this.markable.value || "";
            }
            (this.input || this.textarea).value = this.markable.value = state || "";
        };
        TextWidget.prototype.onReset = function (screen) {
            if ((!screen || this.screenWidget === screen)) {
                var input = this.input || this.textarea;
                _super.prototype.onReset.call(this, screen);
                input.value = this.startText;
                this.mark();
            }
        };
        TextWidget.prototype.onReveal = function (screen) {
            if ((!screen || this.screenWidget === screen)) {
                var input = this.input || this.textarea, answer = this.markable.answer;
                this.markable.value = input.value = (ic.isArray(answer) ? answer[0] : answer) || "";
                this.mark();
            }
        };
        TextWidget.prototype.onMouseDown = function (event) {
            if (!this.hasState("disabled") && document.activeElement !== (this.input || this.textarea)) {
                console.log("focus", this.input || this.textarea);
                (this.input || this.textarea).focus();
                event.preventDefault();
            }
        };
        TextWidget.prototype.onKeyPress = function (event) {
            if (this.accept || this.rx) {
                var key = String.fromCharCode(event.which || event.keyCode || event.charCode), input = event.target, value = input.value;
                if ((this.accept && !this.accept.test(key))
                    || (this.rx && !this.rx.test(value.substring(0, input.selectionStart) + key + value.substring(input.selectionEnd)))) {
                    event.preventDefault();
                    return false;
                }
            }
        };
        TextWidget.prototype.onKeyUp = function () {
            var value = (this.input || this.textarea).value.trim();
            if (this.markable.value !== value) {
                this.markable.value = value;
                if (this.parentWidget) {
                    this.parentWidget.addState("attempted");
                }
                this.mark();
            }
        };
        TextWidget.prototype.onBlur = function () {
            this.removeState("focus");
        };
        TextWidget.prototype.onFocus = function () {
            this.addState(["active", "focus"])
                .parentWidget.addState("active");
        };
        TextWidget.prototype.onState = function (widget, stateList) {
            if (widget === this && stateList.includes("disabled")) {
                var disabled = this.hasState("disabled");
                if (this.input) {
                    this.input.disabled = disabled;
                }
                else {
                    this.textarea.disabled = disabled;
                }
            }
        };
        TextWidget.selector = "ic-text";
        return TextWidget;
    }(ic.InputWidget));
    ic.TextWidget = TextWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var NavWidget = (function (_super) {
        __extends(NavWidget, _super);
        function NavWidget() {
            _super.apply(this, arguments);
            this.anchors = [];
        }
        NavWidget.prototype.startup = function () {
            var _this = this;
            _super.prototype.startup.call(this);
            this.get(ic.AnchorWidget).forEach(function (anchor) {
                var span = _this.anchors[anchor.index] = document.createElement("span");
                span.setAttribute(ic.AnchorWidget.selector, String(anchor.index));
                span.icWidget = anchor;
                _this.element.appendChild(span);
                _this.onState(anchor, []);
            });
            this
                .on("click")
                .on(".state");
            ic.Widget.trigger(".scroll", this);
        };
        NavWidget.prototype.onClick = function (event) {
            var anchor = event.target.icWidget;
            if (anchor.screenWidget) {
                anchor.screenWidget.addState("active");
            }
        };
        NavWidget.prototype.onState = function (widget, stateList) {
            var _this = this;
            if (widget instanceof ic.AnchorWidget && stateList.includes("active")) {
                var span = this.anchors[widget.index];
                Object.keys(widget.state).forEach(function (state) {
                    span.classList.toggle(state, widget.state[state]);
                });
                ic.setImmediate("nav", function () {
                    _this.get(NavWidget).forEach(function (nav) {
                        nav.anchors.some(function (span) {
                            if (span && span.classList.contains("active")) {
                                var rect = span.getBoundingClientRect(), navRect = nav.getBoundingClientRect();
                                nav.element.scrollLeft += rect.left - navRect.left - Math.floor((navRect.width - rect.width) / 2);
                                return true;
                            }
                        });
                        ic.Widget.trigger(".scroll", nav);
                    });
                });
            }
        };
        NavWidget.selector = "ic-nav";
        return NavWidget;
    }(ic.Widget));
    ic.NavWidget = NavWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var PinWidget = (function (_super) {
        __extends(PinWidget, _super);
        function PinWidget(element) {
            _super.call(this, element);
            for (var parent = element.parentElement; parent; parent = parent.parentElement) {
                if (parent.icWidget instanceof ic.DraggableWidget || parent.icWidget instanceof ic.DroppableWidget) {
                    this.parent = parent.icWidget;
                    this.on("mousedown")
                        .on("mouseup")
                        .on(".screen");
                    break;
                }
            }
        }
        PinWidget.prototype.getTarget = function (event) {
            if (this.parent instanceof ic.DraggableWidget) {
                return this.parent;
            }
            else if (this.drag && !this.isSource) {
                return this.drag;
            }
            else if (/^absolute$/i.test(getComputedStyle(this.element).position)) {
                var i, el, widget, children = this.element.parentElement.children;
                for (i = 0; i < children.length; i++) {
                    el = children[i];
                    widget = el.icWidget;
                    if (widget instanceof PinWidget && widget.inRect(event.clientY, event.clientX) && widget.drag && !widget.isSource) {
                        return widget.drag;
                    }
                }
            }
        };
        PinWidget.prototype.onMouseDown = function (event) {
            var target = this.getTarget(event);
            if (target) {
                target.onMouseDown(event);
            }
        };
        PinWidget.prototype.onMouseUp = function (event) {
            var target = this.getTarget(event);
            if (target) {
                target.onMouseUp(event);
            }
        };
        PinWidget.prototype.getHelper = function (event, data, within) {
            var helper = document.createElement("IC-PIN"), rect = this.getBoundingClientRect(), style = helper.style;
            if (/^absolute$/i.test(getComputedStyle(this.element).position)) {
                this.offsetTop = this.element.offsetTop || 0;
                this.offsetLeft = this.element.offsetLeft || 0;
            }
            else {
                this.offsetTop = this.offsetLeft = 0;
            }
            this.helperElement = helper;
            helper.className = "ict-state-dragging " + this.element.className;
            data.offsetY = rect.height / 2 + rect.top - data.clientY;
            data.offsetX = rect.width / 2 + rect.left - data.clientX;
            style.position = "absolute";
            style.zIndex = "100";
            this.element.appendChild(helper);
            ic.drawLine(this.element, this.helperElement);
            if (this.target) {
                this.target.addState("hidden");
            }
            data.top = rect.top;
            data.left = rect.left;
            data.height = rect.height;
            data.width = rect.width;
            return this.element;
        };
        PinWidget.prototype.onHelperMove = function (event, data, within) {
            var style = this.helperElement.style;
            style.top = Math.range(within.top - data.top, event.clientY - data.clientY - data.offsetY - this.offsetTop, within.bottom - data.height - data.top) + "px";
            style.left = Math.range(within.left - data.left, event.clientX - data.clientX - data.offsetX - this.offsetLeft, within.right - data.width - data.left) + "px";
            ic.drawLine(this.element, "update");
        };
        PinWidget.prototype.onScreen = function (screen) {
            if (this.screenWidget === screen && this.target) {
                ic.setImmediate(ic.drawLine.bind(ic, this.element, "update"));
            }
        };
        PinWidget.prototype.freeHelper = function (event, drag, drop) {
            var target = this.target;
            this.helperElement.remove();
            if (target) {
                target.removeState("hidden");
                ic.drawLine(this.element, target.element);
            }
            else {
                ic.drawLine(this.element, "destroy");
            }
        };
        PinWidget.prototype.linkPin = function (drag, drop, pin) {
            if (drag === this.parent) {
                pin = pin || drop.getPin();
                if (pin) {
                    this.unlinkPin();
                    pin.unlinkPin();
                    drop.draggables.pushOnce(drag);
                    drag.droppables.pushOnce(drop);
                    this.drag = pin.drag = drag;
                    this.drop = pin.drop = drop;
                    pin.isSource = false;
                    pin.target = this;
                    this.isSource = true;
                    this.target = pin;
                }
            }
            ic.drawLine(this.element, this.target ? this.target.element : "destroy");
            return this;
        };
        PinWidget.prototype.unlinkPin = function (drag, drop) {
            if (this.target && (this.drag || this.drop) && (!drop || this.drop === drop) && (!drag || this.drag === drag)) {
                var pin = this.target;
                if (this.isSource) {
                    this.drop.draggables.remove(this.drag);
                    this.drop.update();
                    this.drag.droppables.remove(this.drop);
                    ic.drawLine(this.element, "destroy");
                    this.drag = this.drop = this.target = pin.drag = pin.drop = pin.target = null;
                    this.isSource = pin.isSource = false;
                }
                else {
                    pin.unlinkPin(this.drag, this.drop);
                }
            }
            return this;
        };
        PinWidget.selector = "ic-pin";
        return PinWidget;
    }(ic.Widget));
    ic.PinWidget = PinWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var PinsWidget = (function (_super) {
        __extends(PinsWidget, _super);
        function PinsWidget() {
            _super.apply(this, arguments);
        }
        PinsWidget.selector = "ic-pins";
        return PinsWidget;
    }(ic.Widget));
    ic.PinsWidget = PinsWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var QuestionWidget = (function (_super) {
        __extends(QuestionWidget, _super);
        function QuestionWidget() {
            _super.apply(this, arguments);
        }
        QuestionWidget.selector = "ic-question";
        return QuestionWidget;
    }(ic.Widget));
    ic.QuestionWidget = QuestionWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var ScreenWidget = (function (_super) {
        __extends(ScreenWidget, _super);
        function ScreenWidget(element) {
            _super.call(this, element);
            if (!this.hasState("active")) {
                this.element.style.display = "none";
            }
            this
                .on(".persist")
                .on(".state")
                .on(".screen")
                .on(".timeout");
        }
        ScreenWidget.prototype.onPersist = function (state) {
            return this.persistTree({
                "activity": ic.ActivityWidget
            }, state);
        };
        ScreenWidget.prototype.onState = function (widget, stateList) {
            var _this = this;
            if (this === widget) {
                if (stateList.includes("active")) {
                    if (this.hasState("active")) {
                        this.element.style.display = "";
                        this.rootWidget.screen = this.index - 1;
                        if (!this.seen) {
                            this.seen = true;
                            var top = this.element.querySelector("a[name='#top']"), scroll = ic.closestScroll(top);
                            if (top && scroll) {
                                scroll.scrollTop = top.getBoundingClientRect().top - scroll.getBoundingClientRect().top;
                            }
                        }
                    }
                    else {
                        this.element.style.display = "none";
                        this.get("active").forEach(function (widget) {
                            widget.removeState("active");
                        });
                    }
                }
            }
            else if (widget.parentWidget === this && widget instanceof ic.ActivityWidget) {
                ["active", "visible", "visited"].forEach(function (state) {
                    if (widget.hasState(state)) {
                        _this.addState(state);
                    }
                });
            }
        };
        ScreenWidget.prototype.onScreen = function (screen) {
        };
        ScreenWidget.prototype.onTimeout = function () {
            console.log("timeout", this.nextWidgetSibling, this.get(this, ic.ActivityWidget));
            if (!this.nextWidgetSibling && !this.get(this, ic.ActivityWidget).length) {
                this.addState("active");
            }
        };
        ScreenWidget.selector = "ic-screen";
        ScreenWidget.isInputWidget = true;
        ScreenWidget.isTreeWidget = true;
        __decorate([
            ic.persist
        ], ScreenWidget.prototype, "attempts", void 0);
        return ScreenWidget;
    }(ic.Widget));
    ic.ScreenWidget = ScreenWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var ScreensWidget = (function (_super) {
        __extends(ScreensWidget, _super);
        function ScreensWidget() {
            _super.apply(this, arguments);
        }
        ScreensWidget.prototype.startup = function () {
            _super.prototype.startup.call(this);
            var screens = this.get(ic.ScreenWidget);
            ic.Widget.root = this;
            ScreensWidget.screenLength = screens.length;
            this.screen = screens.length ? screens.first().index - 1 : 0;
            this.on(".persist");
        };
        ScreensWidget.prototype.onPersist = function (state) {
            if (state === true) {
                state = JSON.parse(localStorage["state"]);
            }
            var result = this.persistTree({
                "screen": ic.ScreenWidget,
                "timer": ic.TimerWidget
            }, state);
            if (ic.isUndefined(state)) {
                localStorage["state"] = JSON.stringify(result);
                return result;
            }
        };
        Object.defineProperty(ScreensWidget.prototype, "screen", {
            get: function () {
                return ScreensWidget.screenIndex;
            },
            set: function (index) {
                if (index !== ScreensWidget.screenIndex && index >= 0 && index < ScreensWidget.screenLength) {
                    this.get(ic.ScreenWidget, "active").forEach(function (screen) {
                        screen.removeState("active");
                    });
                    ScreensWidget.screenIndex = index;
                    ic.Widget.trigger(".screen", ScreensWidget.screenWidget = this.get(ic.ScreenWidget)[index].addState("active"));
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ScreensWidget.prototype, "screenWidget", {
            get: function () {
                return ScreensWidget.screenWidget;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ScreensWidget.prototype, "length", {
            get: function () {
                return ScreensWidget.screenLength;
            },
            enumerable: true,
            configurable: true
        });
        ScreensWidget.selector = "ic-screens";
        ScreensWidget.isInputWidget = true;
        ScreensWidget.isTreeWidget = true;
        ScreensWidget.screenLength = 0;
        __decorate([
            ic.persist
        ], ScreensWidget.prototype, "attempts", void 0);
        __decorate([
            ic.persist
        ], ScreensWidget.prototype, "screen", null);
        return ScreensWidget;
    }(ic.Widget));
    ic.ScreensWidget = ScreensWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var TimerWidget = (function (_super) {
        __extends(TimerWidget, _super);
        function TimerWidget(element) {
            _super.call(this, element);
            this.format = element.getAttribute(this.selector) || "H:i:s";
            if (this.rootWidget.get(ic.ActivityWidget).filter(function (activity) { return activity.hasState("active"); }).length) {
                this.start = new Date().getTime();
            }
            else {
                this.on(".state");
            }
            this.minutes = (parseFloat(this.data.minutes) || 0) * 60000;
            this.warning = (parseFloat(this.data.warning) || 0) * 60000;
            this.onTick();
            this
                .on(".persist")
                .on(".tick");
        }
        TimerWidget.prototype.startup = function () {
            var _this = this;
            _super.prototype.startup.call(this);
            this.rootWidget.get(TimerWidget).some(function (timer, index) {
                if (_this === timer) {
                    _this._i = index;
                    return true;
                }
            });
        };
        TimerWidget.prototype.onPersist = function (state) {
            if (ic.isUndefined(state)) {
                return this.start ? Math.range(0, new Date().getTime() - this.start, this.minutes) : this.used;
            }
            if (this.rootWidget.get(ic.ActivityWidget).find(function (activity) { return activity.hasState("active"); })) {
                this.start = new Date().getTime() - state;
            }
            else {
                this.used = state;
                this.on(".state");
            }
        };
        TimerWidget.prototype.onTick = function () {
            var elapsed = this.start ? new Date().getTime() - this.start : (this.used || 0), max = this.minutes;
            if (max) {
                if (this.start) {
                    elapsed = Math.max(0, max - elapsed);
                    if (this.warning >= elapsed) {
                        this.addState("warning");
                    }
                    if (!elapsed && !this.hasState("disabled")) {
                        this.addState("disabled");
                        ic.Widget.trigger(".timeout");
                    }
                }
                else {
                    elapsed = max - (this.used || 0);
                }
            }
            this.element.innerText = new Date(elapsed).format(this.format);
        };
        TimerWidget.prototype.onState = function (widget, stateList) {
            if (widget === this && stateList.includes("disabled")) {
                if (this.hasState("disabled")) {
                    if (this.start) {
                        this.used = new Date().getTime() - this.start;
                    }
                    this.start = 0;
                }
                else {
                    this.start = new Date().getTime() - (this.used || 0);
                }
            }
            else if (!this.start && widget instanceof ic.ActivityWidget && !this.hasState("disabled") && widget.hasState("active")) {
                this.start = new Date().getTime() - (this.used || 0);
            }
        };
        TimerWidget.selector = "ic-timer";
        return TimerWidget;
    }(ic.Widget));
    ic.TimerWidget = TimerWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    function startup(root) {
        var text, changed, node, walker = document.createTreeWalker(root, NodeFilter.SHOW_TEXT, null, false);
        while (node = walker.nextNode()) {
            changed = false;
            text = node.nodeValue.replace(/\[\[([a-z]+)\]\]/g, function ($0, $1) {
                switch ($1) {
                    case "title":
                        changed = true;
                        $1 = "JSON Title";
                        break;
                }
                return $1;
            });
            if (changed) {
                node.nodeValue = text;
            }
        }
        ic.Widget.startup(root);
    }
    ic.startup = startup;
})(ic || (ic = {}));
;
var api;
(function (api) {
    window.IC = api;
    function timer(seconds) {
        if (ic.Widget.root) {
            var timer = ic.Widget.root.get(ic.TimerWidget).first();
            if (timer) {
                if (seconds) {
                    timer.minutes = seconds * 60 * 1000;
                }
                return timer.trigger(".persist") / 1000;
            }
        }
        return 0;
    }
    api.timer = timer;
    function pause() {
        if (ic.Widget.root) {
            ic.Widget.root.get(ic.TimerWidget).forEach(function (timer) {
                if (!timer.hasState("disabled")) {
                    timer.api_pause = true;
                    timer.addState("disabled");
                }
            });
            ic.Widget.root.get(ic.InputWidget).forEach(function (widget) {
                if (widget.element.tagName !== "IC-BUTTON") {
                    if (!widget.hasState("disabled")) {
                        widget.api_pause = true;
                        widget.addState("disabled");
                    }
                }
            });
        }
        return 0;
    }
    api.pause = pause;
    function resume() {
        if (ic.Widget.root) {
            ic.Widget.root.get(ic.InputWidget).forEach(function (widget) {
                if (widget.element.tagName !== "IC-BUTTON") {
                    if (widget.api_pause) {
                        delete widget.api_pause;
                        widget.removeState("disabled");
                    }
                }
            });
            ic.Widget.root.get(ic.TimerWidget).forEach(function (timer) {
                if (timer.api_pause) {
                    delete timer.api_pause;
                    timer.removeState("disabled");
                }
            });
        }
        return 0;
    }
    api.resume = resume;
    function state(data) {
        if (ic.Widget.root) {
            var json;
            if (ic.isString(data)) {
                json = JSON.parse(api.LZString.decompressFromEncodedURIComponent(data));
            }
            json = JSON.stringify(ic.Widget.root.trigger(".persist", json));
            data = api.LZString.compressToEncodedURIComponent(json);
            if (json && data) {
                console.log("Compress: src=" + json.length + " bytes, dest=" + data.length + " bytes (" + (data.length * 100 / json.length).toFixed(2) + "%)");
            }
            return data;
        }
        return "";
    }
    api.state = state;
    ;
    function score(screenIndex, activityIndex) {
        var output = [], isScreen = ic.isNumber(screenIndex), isActivity = isScreen && ic.isNumber(activityIndex);
        if (ic.Widget.root) {
            ic.Widget.root.get(ic.ScreenWidget).forEach(function (screen) {
                if (!isScreen || screen.index === screenIndex) {
                    var activities = screen.get(screen, ic.ActivityWidget).filter(function (activity) {
                        return activity.markable && activity.markable.score >= 0;
                    });
                    activities.forEach(function (activity) {
                        if (!isActivity || activity.index === activityIndex) {
                            var mark = activity.markable, score = {
                                "id": (mark.id || "#@")
                                    .replace(/\#/g, String(screen.index))
                                    .replace(/\@/g, activities.length <= 1 ? "" : String.fromCharCode(96 + activity.index))
                                    .replace(/\%/g, activities.length <= 1 ? "" : activity.index.toRoman().toLowerCase()),
                                "score": mark.score,
                                "points": mark.maxPoints,
                                "min": mark.min || 0,
                                "scaled": mark.scaled,
                                "max": mark.max,
                                "attempted": activity.hasState("attempted")
                            };
                            activity.get(activity, ic.QuestionWidget).forEach(function (widget) {
                                score["question"] = (score["question"] ? " " : "") + widget.element.textContent;
                            });
                            output.push(score);
                        }
                    });
                }
            });
        }
        return output;
    }
    api.score = score;
    var fromCharCode = String.fromCharCode;
    var keyStrBase64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    var keyStrUriSafe = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+-$";
    var baseReverseDic = {};
    function getBaseValue(alphabet, character) {
        if (!baseReverseDic[alphabet]) {
            baseReverseDic[alphabet] = {};
            for (var i = 0; i < alphabet.length; i++) {
                baseReverseDic[alphabet][alphabet.charAt(i)] = i;
            }
        }
        return baseReverseDic[alphabet][character];
    }
    api.LZString = {
        compressToEncodedURIComponent: function (input) {
            if (input == null)
                return "";
            return api.LZString._compress(input, 6, function (a) { return keyStrUriSafe.charAt(a); });
        },
        decompressFromEncodedURIComponent: function (input) {
            if (input == null)
                return "";
            if (input == "")
                return null;
            input = input.replace(/ /g, "+");
            return api.LZString._decompress(input.length, 32, function (index) { return getBaseValue(keyStrUriSafe, input.charAt(index)); });
        },
        compress: function (uncompressed) {
            return api.LZString._compress(uncompressed, 16, function (a) { return fromCharCode(a); });
        },
        _compress: function (uncompressed, bitsPerChar, getCharFromInt) {
            if (uncompressed == null)
                return "";
            var i, value, context_dictionary = {}, context_dictionaryToCreate = {}, context_c = "", context_wc = "", context_w = "", context_enlargeIn = 2, context_dictSize = 3, context_numBits = 2, context_data = [], context_data_val = 0, context_data_position = 0, ii;
            for (ii = 0; ii < uncompressed.length; ii += 1) {
                context_c = uncompressed.charAt(ii);
                if (!Object.prototype.hasOwnProperty.call(context_dictionary, context_c)) {
                    context_dictionary[context_c] = context_dictSize++;
                    context_dictionaryToCreate[context_c] = true;
                }
                context_wc = context_w + context_c;
                if (Object.prototype.hasOwnProperty.call(context_dictionary, context_wc)) {
                    context_w = context_wc;
                }
                else {
                    if (Object.prototype.hasOwnProperty.call(context_dictionaryToCreate, context_w)) {
                        if (context_w.charCodeAt(0) < 256) {
                            for (i = 0; i < context_numBits; i++) {
                                context_data_val = (context_data_val << 1);
                                if (context_data_position == bitsPerChar - 1) {
                                    context_data_position = 0;
                                    context_data.push(getCharFromInt(context_data_val));
                                    context_data_val = 0;
                                }
                                else {
                                    context_data_position++;
                                }
                            }
                            value = context_w.charCodeAt(0);
                            for (i = 0; i < 8; i++) {
                                context_data_val = (context_data_val << 1) | (value & 1);
                                if (context_data_position == bitsPerChar - 1) {
                                    context_data_position = 0;
                                    context_data.push(getCharFromInt(context_data_val));
                                    context_data_val = 0;
                                }
                                else {
                                    context_data_position++;
                                }
                                value = value >> 1;
                            }
                        }
                        else {
                            value = 1;
                            for (i = 0; i < context_numBits; i++) {
                                context_data_val = (context_data_val << 1) | value;
                                if (context_data_position == bitsPerChar - 1) {
                                    context_data_position = 0;
                                    context_data.push(getCharFromInt(context_data_val));
                                    context_data_val = 0;
                                }
                                else {
                                    context_data_position++;
                                }
                                value = 0;
                            }
                            value = context_w.charCodeAt(0);
                            for (i = 0; i < 16; i++) {
                                context_data_val = (context_data_val << 1) | (value & 1);
                                if (context_data_position == bitsPerChar - 1) {
                                    context_data_position = 0;
                                    context_data.push(getCharFromInt(context_data_val));
                                    context_data_val = 0;
                                }
                                else {
                                    context_data_position++;
                                }
                                value = value >> 1;
                            }
                        }
                        context_enlargeIn--;
                        if (context_enlargeIn == 0) {
                            context_enlargeIn = Math.pow(2, context_numBits);
                            context_numBits++;
                        }
                        delete context_dictionaryToCreate[context_w];
                    }
                    else {
                        value = context_dictionary[context_w];
                        for (i = 0; i < context_numBits; i++) {
                            context_data_val = (context_data_val << 1) | (value & 1);
                            if (context_data_position == bitsPerChar - 1) {
                                context_data_position = 0;
                                context_data.push(getCharFromInt(context_data_val));
                                context_data_val = 0;
                            }
                            else {
                                context_data_position++;
                            }
                            value = value >> 1;
                        }
                    }
                    context_enlargeIn--;
                    if (context_enlargeIn == 0) {
                        context_enlargeIn = Math.pow(2, context_numBits);
                        context_numBits++;
                    }
                    context_dictionary[context_wc] = context_dictSize++;
                    context_w = String(context_c);
                }
            }
            if (context_w !== "") {
                if (Object.prototype.hasOwnProperty.call(context_dictionaryToCreate, context_w)) {
                    if (context_w.charCodeAt(0) < 256) {
                        for (i = 0; i < context_numBits; i++) {
                            context_data_val = (context_data_val << 1);
                            if (context_data_position == bitsPerChar - 1) {
                                context_data_position = 0;
                                context_data.push(getCharFromInt(context_data_val));
                                context_data_val = 0;
                            }
                            else {
                                context_data_position++;
                            }
                        }
                        value = context_w.charCodeAt(0);
                        for (i = 0; i < 8; i++) {
                            context_data_val = (context_data_val << 1) | (value & 1);
                            if (context_data_position == bitsPerChar - 1) {
                                context_data_position = 0;
                                context_data.push(getCharFromInt(context_data_val));
                                context_data_val = 0;
                            }
                            else {
                                context_data_position++;
                            }
                            value = value >> 1;
                        }
                    }
                    else {
                        value = 1;
                        for (i = 0; i < context_numBits; i++) {
                            context_data_val = (context_data_val << 1) | value;
                            if (context_data_position == bitsPerChar - 1) {
                                context_data_position = 0;
                                context_data.push(getCharFromInt(context_data_val));
                                context_data_val = 0;
                            }
                            else {
                                context_data_position++;
                            }
                            value = 0;
                        }
                        value = context_w.charCodeAt(0);
                        for (i = 0; i < 16; i++) {
                            context_data_val = (context_data_val << 1) | (value & 1);
                            if (context_data_position == bitsPerChar - 1) {
                                context_data_position = 0;
                                context_data.push(getCharFromInt(context_data_val));
                                context_data_val = 0;
                            }
                            else {
                                context_data_position++;
                            }
                            value = value >> 1;
                        }
                    }
                    context_enlargeIn--;
                    if (context_enlargeIn == 0) {
                        context_enlargeIn = Math.pow(2, context_numBits);
                        context_numBits++;
                    }
                    delete context_dictionaryToCreate[context_w];
                }
                else {
                    value = context_dictionary[context_w];
                    for (i = 0; i < context_numBits; i++) {
                        context_data_val = (context_data_val << 1) | (value & 1);
                        if (context_data_position == bitsPerChar - 1) {
                            context_data_position = 0;
                            context_data.push(getCharFromInt(context_data_val));
                            context_data_val = 0;
                        }
                        else {
                            context_data_position++;
                        }
                        value = value >> 1;
                    }
                }
                context_enlargeIn--;
                if (context_enlargeIn == 0) {
                    context_enlargeIn = Math.pow(2, context_numBits);
                    context_numBits++;
                }
            }
            value = 2;
            for (i = 0; i < context_numBits; i++) {
                context_data_val = (context_data_val << 1) | (value & 1);
                if (context_data_position == bitsPerChar - 1) {
                    context_data_position = 0;
                    context_data.push(getCharFromInt(context_data_val));
                    context_data_val = 0;
                }
                else {
                    context_data_position++;
                }
                value = value >> 1;
            }
            while (true) {
                context_data_val = (context_data_val << 1);
                if (context_data_position == bitsPerChar - 1) {
                    context_data.push(getCharFromInt(context_data_val));
                    break;
                }
                else
                    context_data_position++;
            }
            return context_data.join('');
        },
        decompress: function (compressed) {
            if (compressed == null)
                return "";
            if (compressed == "")
                return null;
            return api.LZString._decompress(compressed.length, 32768, function (index) { return compressed.charCodeAt(index); });
        },
        _decompress: function (length, resetValue, getNextValue) {
            var dictionary = [], next, enlargeIn = 4, dictSize = 4, numBits = 3, entry = "", result = [], i, w, bits, resb, maxpower, power, c, data = { val: getNextValue(0), position: resetValue, index: 1 };
            for (i = 0; i < 3; i += 1) {
                dictionary[i] = i;
            }
            bits = 0;
            maxpower = Math.pow(2, 2);
            power = 1;
            while (power != maxpower) {
                resb = data.val & data.position;
                data.position >>= 1;
                if (data.position == 0) {
                    data.position = resetValue;
                    data.val = getNextValue(data.index++);
                }
                bits |= (resb > 0 ? 1 : 0) * power;
                power <<= 1;
            }
            switch (next = bits) {
                case 0:
                    bits = 0;
                    maxpower = Math.pow(2, 8);
                    power = 1;
                    while (power != maxpower) {
                        resb = data.val & data.position;
                        data.position >>= 1;
                        if (data.position == 0) {
                            data.position = resetValue;
                            data.val = getNextValue(data.index++);
                        }
                        bits |= (resb > 0 ? 1 : 0) * power;
                        power <<= 1;
                    }
                    c = fromCharCode(bits);
                    break;
                case 1:
                    bits = 0;
                    maxpower = Math.pow(2, 16);
                    power = 1;
                    while (power != maxpower) {
                        resb = data.val & data.position;
                        data.position >>= 1;
                        if (data.position == 0) {
                            data.position = resetValue;
                            data.val = getNextValue(data.index++);
                        }
                        bits |= (resb > 0 ? 1 : 0) * power;
                        power <<= 1;
                    }
                    c = fromCharCode(bits);
                    break;
                case 2:
                    return "";
            }
            dictionary[3] = c;
            w = c;
            result.push(c);
            while (true) {
                if (data.index > length) {
                    return "";
                }
                bits = 0;
                maxpower = Math.pow(2, numBits);
                power = 1;
                while (power != maxpower) {
                    resb = data.val & data.position;
                    data.position >>= 1;
                    if (data.position == 0) {
                        data.position = resetValue;
                        data.val = getNextValue(data.index++);
                    }
                    bits |= (resb > 0 ? 1 : 0) * power;
                    power <<= 1;
                }
                switch (c = bits) {
                    case 0:
                        bits = 0;
                        maxpower = Math.pow(2, 8);
                        power = 1;
                        while (power != maxpower) {
                            resb = data.val & data.position;
                            data.position >>= 1;
                            if (data.position == 0) {
                                data.position = resetValue;
                                data.val = getNextValue(data.index++);
                            }
                            bits |= (resb > 0 ? 1 : 0) * power;
                            power <<= 1;
                        }
                        dictionary[dictSize++] = fromCharCode(bits);
                        c = dictSize - 1;
                        enlargeIn--;
                        break;
                    case 1:
                        bits = 0;
                        maxpower = Math.pow(2, 16);
                        power = 1;
                        while (power != maxpower) {
                            resb = data.val & data.position;
                            data.position >>= 1;
                            if (data.position == 0) {
                                data.position = resetValue;
                                data.val = getNextValue(data.index++);
                            }
                            bits |= (resb > 0 ? 1 : 0) * power;
                            power <<= 1;
                        }
                        dictionary[dictSize++] = fromCharCode(bits);
                        c = dictSize - 1;
                        enlargeIn--;
                        break;
                    case 2:
                        return result.join('');
                }
                if (enlargeIn == 0) {
                    enlargeIn = Math.pow(2, numBits);
                    numBits++;
                }
                if (dictionary[c]) {
                    entry = dictionary[c];
                }
                else {
                    if (c === dictSize) {
                        entry = w + w.charAt(0);
                    }
                    else {
                        return null;
                    }
                }
                result.push(entry);
                dictionary[dictSize++] = w + entry.charAt(0);
                enlargeIn--;
                w = entry;
                if (enlargeIn == 0) {
                    enlargeIn = Math.pow(2, numBits);
                    numBits++;
                }
            }
        }
    };
})(api || (api = {}));
;
var ic;
(function (ic) {
    if (/complete|loaded|interactive/.test(document.readyState) && document.body) {
        ic.startup(document);
    }
    else {
        document.addEventListener("DOMContentLoaded", ic.startup.bind(ic, document), false);
    }
})(ic || (ic = {}));
;
//# sourceMappingURL=activity.js.map