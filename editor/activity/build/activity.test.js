var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var ic;
(function (ic) {
    try {
        window.console.info("IC v" + VERSION + " (" + DATE + ")");
    }
    catch (e) { }
    ic.version = {
        "css-columns": 1
    };
})(ic || (ic = {}));
function defineProperty(proto, name, value) {
    if (proto && !proto[name]) {
        Object.defineProperty(proto, name, {
            "value": value
        });
    }
}
defineProperty(Array.prototype, "equals", function (target) {
    if (!target || this.length !== target.length) {
        return false;
    }
    for (var index = 0; index < this.length; index++) {
        if (typeof this[index] !== typeof target[index] || !this[index] !== !target[index]) {
            return false;
        }
        switch (typeof (this[index])) {
            case "object":
                if (this[index] !== null && target[index] !== null && (this[index].constructor.toString() !== target[index].constructor.toString() || !this[index].equals(target[index]))) {
                    return false;
                }
                break;
            case "function":
                if (this[index].toString() !== target[index].toString()) {
                    return false;
                }
                break;
            default:
                if (this[index] !== target[index]) {
                    return false;
                }
        }
    }
    return true;
});
defineProperty(Array.prototype, "explode", function () {
    var i, num, arr = this.slice(0);
    for (i = 0; i < arr.length; i++) {
        num = parseInt(arr[i], 10);
        if (String(num) === arr[i]) {
            arr[i] = num;
        }
    }
    return arr;
});
defineProperty(Array.prototype, "find", function (predicate) {
    if (this === null || this === undefined) {
        throw new TypeError('Array.prototype.find called on null or undefined');
    }
    if (typeof predicate !== 'function') {
        throw new TypeError('predicate must be a function');
    }
    var list = Object(this), length = list.length >>> 0, thisArg = arguments[1], value;
    for (var i = 0; i < length; i++) {
        value = list[i];
        if (predicate.call(thisArg, value, i, list)) {
            return value;
        }
    }
    return undefined;
});
defineProperty(Array.prototype, "findIndex", function (predicate) {
    if (this === null || this === undefined) {
        throw new TypeError('Array.prototype.findIndex called on null or undefined');
    }
    if (typeof predicate !== 'function') {
        throw new TypeError('predicate must be a function');
    }
    var list = Object(this), length = list.length >>> 0, thisArg = arguments[1], value;
    for (var i = 0; i < length; i++) {
        value = list[i];
        if (predicate.call(thisArg, value, i, list)) {
            return i;
        }
    }
    return -1;
});
defineProperty(Array.prototype, "first", function () {
    return this[0];
});
defineProperty(Array, "from", function (arrayLike, _mapFn, thisArg) {
    var toStr = Object.prototype.toString, isCallable = function (fn) {
        return typeof fn === 'function' || toStr.call(fn) === '[object Function]';
    }, toInteger = function (value) {
        var number = Number(value);
        if (isNaN(number)) {
            return 0;
        }
        if (number === 0 || !isFinite(number)) {
            return number;
        }
        return (number > 0 ? 1 : -1) * Math.floor(Math.abs(number));
    }, maxSafeInteger = Math.pow(2, 53) - 1, toLength = function (value) {
        var len = toInteger(value);
        return Math.min(Math.max(len, 0), maxSafeInteger);
    }, C = this, items = Object(arrayLike);
    if (arrayLike == null) {
        throw new TypeError('Array.from requires an array-like object - not null or undefined');
    }
    var mapFn = arguments.length > 1 ? arguments[1] : void undefined, T;
    if (typeof mapFn !== 'undefined') {
        if (!isCallable(mapFn)) {
            throw new TypeError('Array.from: when provided, the second argument must be a function');
        }
        if (arguments.length > 2) {
            T = arguments[2];
        }
    }
    var len = toLength(items.length), A = isCallable(C) ? Object(new C(len)) : new Array(len), k = 0, kValue;
    while (k < len) {
        kValue = items[k];
        if (mapFn) {
            A[k] = T === undefined ? mapFn(kValue, k) : mapFn.call(T, kValue, k);
        }
        else {
            A[k] = kValue;
        }
        k += 1;
    }
    A.length = len;
    return A;
});
defineProperty(Array.prototype, "includes", function (searchElement, fromIndex) {
    var i, currentElement, isNaN = searchElement !== searchElement;
    fromIndex = fromIndex || 0;
    for (i = fromIndex >= 0 ? fromIndex : Math.max(0, length + fromIndex); i < this.length; i++) {
        currentElement = this[i];
        if (searchElement === currentElement ||
            (isNaN && currentElement !== currentElement)) {
            return true;
        }
    }
    return false;
});
defineProperty(Array.prototype, "intersect", function (searchArray) {
    if (!isArray(searchArray)) {
        searchArray = [].slice.call(arguments);
    }
    return this.filter(function (value) {
        return searchArray.includes(value);
    });
});
defineProperty(Array.prototype, "last", function () {
    return this[this.length - 1];
});
defineProperty(Array.prototype, "not", function (searchArray) {
    if (!isArray(searchArray)) {
        searchArray = [].slice.call(arguments);
    }
    return this.filter(function (value) {
        return !searchArray.includes(value);
    });
});
defineProperty(Array.prototype, "pushOnce", function () {
    var that = this, i = 0, elements = arguments;
    for (; i < elements.length; i++) {
        if (!that.includes(elements[i])) {
            that.push(elements[i]);
        }
    }
    return that.length;
});
defineProperty(Array.prototype, "remove", function (searchElement) {
    var i = this.indexOf(searchElement);
    return i >= 0 ? this.splice(i, 1) : [];
});
defineProperty(Array.prototype, "shuffle", function () {
    var current = this.length;
    while (current > 0) {
        var i = Math.floor(Math.random() * current--), tmp = this[current];
        this[current] = this[i];
        this[i] = tmp;
    }
    return this;
});
defineProperty(Array.prototype, "wrap", function (between, before, after) {
    if (isUndefined(between)) {
        between = "";
    }
    if (isUndefined(before)) {
        before = "";
    }
    if (isUndefined(after)) {
        after = "";
    }
    return before + this.join(after + between + before) + after;
});
defineProperty(Date, "format", function (format) {
    return (new Date()).format(format);
});
defineProperty(Date.prototype, "format", function (format) {
    var output = "";
    if (isNaN(this)) {
        return "Invalid Date";
    }
    else {
        var that_1 = this, date_1 = that_1.getDate(), day_1 = that_1.getDay(), month_1 = that_1.getMonth(), year_1 = that_1.getFullYear(), hours_1 = that_1.getHours(), minutes_1 = that_1.getMinutes(), seconds_1 = that_1.getSeconds(), skip_1 = false, monthNames_1 = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"], dayNames_1 = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"], pad_1 = function (num) {
            return num <= 9 ? "0" + num : "" + num;
        }, plural_1 = function (num, word) {
            return num + " " + word + (num === 1 ? "" : "s");
        }, replace = function (letter) {
            if (skip_1) {
                skip_1 = false;
                return letter;
            }
            switch (letter) {
                case "\\":
                    skip_1 = true;
                    return "";
                case "d":
                    return pad_1(date_1);
                case "D":
                    return dayNames_1[day_1].substr(0, 3);
                case "j":
                    return date_1;
                case "l":
                    return dayNames_1[day_1];
                case "N":
                    return day_1 + 1;
                case "S":
                    return (date_1 % 10 === 1 && date_1 !== 11 ? "st" : (date_1 % 10 === 2 && date_1 !== 12 ? "nd" : (date_1 % 10 === 3 && date_1 !== 13 ? "rd" : "th")));
                case "w":
                    return day_1;
                case "R":
                    var i = (new Date().getTime() - that_1.getTime()) / 1000;
                    return i < 0
                        ? "In the Future"
                        : i < 20
                            ? "Just Now"
                            : i < 60
                                ? "Less than a Minute Ago"
                                : i < 120
                                    ? "About a Minute Ago"
                                    : i < (60 * 60)
                                        ? plural_1(Math.floor(i / 60), "Minute") + " Ago"
                                        : i < (2 * 60 * 60)
                                            ? "About an Hour Ago"
                                            : i < (12 * 60 * 60)
                                                ? plural_1(Math.floor(i / (60 * 60)), "Hour") + " Ago"
                                                : i < (2 * 24 * 60 * 60) && day_1 === (new Date).getDay()
                                                    ? "Today"
                                                    : i < (3 * 24 * 60 * 60) && (day_1 === 6 ? 0 : day_1 + 1) === (new Date).getDay()
                                                        ? "Yesterday"
                                                        : i < (7 * 24 * 60 * 60)
                                                            ? plural_1(Math.floor(i / (24 * 60 * 60)), "Day") + " Ago"
                                                            : i < (31 * 24 * 60 * 60)
                                                                ? plural_1(Math.floor(i / (7 * 24 * 60 * 60)), "Week") + " Ago"
                                                                : i < (365 * 24 * 60 * 60)
                                                                    ? plural_1(Math.floor(i / (31 * 24 * 60 * 60)), "Month") + " Ago"
                                                                    : plural_1(Math.floor(i / (365 * 24 * 60 * 60)), "Year") + " Ago";
                case "F":
                    return monthNames_1[month_1];
                case "m":
                    return pad_1(month_1 + 1);
                case "M":
                    return monthNames_1[month_1].substr(0, 3);
                case "n":
                    return month_1 + 1;
                case "L":
                    return (((year_1 % 4 === 0) && (year_1 % 100 !== 0)) || (year_1) % 400 === 0) ? "1" : "0";
                case "Y":
                    return year_1;
                case "y":
                    return String(year_1).substr(2);
                case "a":
                    return hours_1 < 12 ? "am" : "pm";
                case "A":
                    return hours_1 < 12 ? "AM" : "PM";
                case "g":
                    return hours_1 % 12 || 12;
                case "G":
                    return hours_1;
                case "h":
                    return pad_1(hours_1 % 12 || 12);
                case "H":
                    return pad_1(hours_1);
                case "i":
                    return pad_1(minutes_1);
                case "s":
                    return pad_1(seconds_1);
                case "u":
                    return String(that_1.getMilliseconds()).substr(0, 3);
                case "O":
                    {
                        var offset = that_1.getTimezoneOffset();
                        return (offset < 0 ? "+" : "-") + pad_1(Math.abs(offset / 60)) + "00";
                    }
                case "P":
                    {
                        var offset = that_1.getTimezoneOffset();
                        return (offset < 0 ? "+" : "-") + pad_1(Math.abs(offset / 60)) + ":" + pad_1(Math.abs(offset % 60));
                    }
                case "T":
                    that_1.setMonth(0);
                    var result = that_1.toTimeString().replace(/^.+ \(?([^\)]+)\)?$/, "$1");
                    that_1.setMonth(month_1);
                    return result;
                case "Z":
                    return -that_1.getTimezoneOffset() * 60;
                case "c":
                    return that_1.format("Y-m-d\\TH:i:sP");
                case "r":
                    return String(that_1);
                case "U":
                    return that_1.getTime() / 1000;
                default:
                    return letter;
            }
        };
        format = format || "c";
        for (var i = 0; i < format.length; i++) {
            output += replace(format[i]);
        }
    }
    return output;
});
defineProperty(Element.prototype, "closest", function (selectors) {
    var element = this;
    while (element && !element.matches(selectors)) {
        element = element.parentElement;
    }
    return element;
});
defineProperty(Element.prototype, "closestWidget", function (selectors) {
    var element = this;
    while (element && !element.tagName.startsWith("IC-") && (!selectors || !element.matches(selectors))) {
        element = element.parentElement;
    }
    return element;
});
defineProperty(Element.prototype, "common", function (element) {
    var el_1 = element ? this : undefined, el_2 = element;
    while (el_1 && el_2 && el_1 !== el_2) {
        while (el_2 && el_1 !== el_2) {
            el_2 = el_2.parentElement;
        }
        if (el_2) {
            break;
        }
        el_1 = el_1.parentElement;
        el_2 = element;
    }
    return el_1;
});
defineProperty(Element.prototype, "fixState", function (currentState, index) {
    var element = this, tagName = element.tagName;
    if (tagName.startsWith("IC-")) {
        var i = void 0, state = void 0, className = void 0, attributeNameClass = void 0, attribute = void 0, tagNameClass = tagName.toLowerCase().replace(/^ic-/, "ict-"), attributes = element.attributes, currentClass_1 = element.classList, addClass_1 = {};
        addClass_1[tagNameClass] = true;
        if (index !== undefined && index >= 0) {
            addClass_1[tagNameClass + "-" + index] = true;
        }
        for (i = 0; i < attributes.length; i++) {
            attribute = attributes[i];
            if (attribute.name.startsWith("ic-")) {
                attributeNameClass = attribute.name.replace(/^ic-/, "ict-");
                if (attributeNameClass === "state") {
                    if (attribute.value.trim()) {
                        attribute.value.split(/\s+/).forEach(function (state) {
                            addClass_1["ict-state-" + state] = true;
                        });
                    }
                }
                else {
                    addClass_1[attributeNameClass] = true;
                    if (attribute.value && !/[^a-z0-9_-]/.test(attribute.value)) {
                        addClass_1[attributeNameClass + "-" + attribute.value] = true;
                    }
                }
            }
        }
        if (currentState) {
            for (state in currentState) {
                if (currentState[state] === true) {
                    addClass_1["ict-state-" + state] = true;
                }
            }
        }
        for (i = currentClass_1.length - 1; i >= 0; i--) {
            className = currentClass_1[i];
            if (className.startsWith("ict-") && !addClass_1[className]) {
                currentClass_1.remove(className);
            }
        }
        Object.keys(addClass_1).forEach(function (className) {
            currentClass_1.add(className);
        });
    }
});
defineProperty(Element.prototype, "matches", Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector);
defineProperty(Node.prototype, "parentElements", function (stop, andSelf) {
    var element = this, isString = typeof stop === "string", stopSelector = isString ? stop : undefined, stopElement = isString ? undefined : stop, result = andSelf ? [element] : [];
    while (element && (!stopSelector || !element.matches(stopSelector)) && (!stopElement || element !== stopElement)) {
        result.push(element = element.parentElement);
    }
    return result;
});
defineProperty(Element.prototype, "remove", function () {
    if (this.parentNode) {
        this.parentNode.removeChild(this);
    }
});
(function (doc, proto) {
    try {
        doc.querySelector(":scope body");
    }
    catch (e) {
        ["querySelector", "querySelectorAll"].forEach(function (method) {
            var native = proto[method];
            proto[method] = function (selectors) {
                if (/(^|,)\s*:scope/.test(selectors)) {
                    var el = this, id = el.id;
                    el.id = "SCOPE_" + Date.now();
                    selectors = selectors.replace(/((^|,)\s*):scope/g, "$1#" + el.id);
                    var result = doc[method](selectors);
                    el.id = id;
                    return result;
                }
                else {
                    return native.call(this, selectors);
                }
            };
        });
    }
})(window.document, Element.prototype);
defineProperty(Math, "range", function (min, num, max) {
    return Math.max(min, Math.min(num, max));
});
defineProperty(Number.prototype, "range", function (min, max) {
    return this >= Math.min(min, max) && this <= Math.max(min, max);
});
defineProperty(Number.prototype, "round", function (digits) {
    var num = +("1e" + Math.max(0, digits));
    return Math.round(this * num) / num;
});
defineProperty(Number.prototype, "toRoman", function (lower) {
    var i, num = Math.floor(this), lookup = {
        "M": 1000,
        "CM": 900,
        "D": 500,
        "CD": 400,
        "C": 100,
        "XC": 90,
        "L": 50,
        "XL": 40,
        "X": 10,
        "IX": 9,
        "V": 5,
        "IV": 4,
        "I": 1
    }, roman = "";
    for (i in lookup) {
        while (num >= lookup[i]) {
            roman += i;
            num -= lookup[i];
        }
    }
    return lower ? roman.toLowerCase() : roman;
});
defineProperty(Object.prototype, "clone", function ObjectCopyCloneFn(target, save) {
    var copy, key, value;
    if (this === null || typeof this !== "object") {
        return this;
    }
    if (this instanceof Date) {
        copy = target || new Date();
        copy.setTime(this.getTime());
        return copy;
    }
    if (this instanceof Function) {
        return this;
    }
    if (this instanceof Array) {
        if (target instanceof Array) {
            copy = target;
            if (save !== false) {
                target.length = 0;
            }
        }
        else {
            copy = [];
        }
        for (key = 0; key < this.length; key++) {
            value = this[key];
            if (isObject(value)) {
                copy[key] = ObjectCopyCloneFn.call(value);
            }
            else {
                copy[key] = value;
            }
        }
        return copy;
    }
    if (this instanceof Object) {
        if (target instanceof Object) {
            copy = target;
            if (save !== false) {
                for (key in target) {
                    if (!this.hasOwnProperty(key) && target.hasOwnProperty(key)) {
                        delete target[key];
                    }
                }
            }
        }
        else {
            copy = {};
        }
        for (key in this) {
            if (this.hasOwnProperty(key)) {
                value = this[key];
                if (value instanceof Object) {
                    copy[key] = ObjectCopyCloneFn.call(value);
                }
                else {
                    copy[key] = value;
                }
            }
        }
        return copy;
    }
    throw new Error("Unable to copy obj! Its type isn't supported.");
});
defineProperty(Object.prototype, "getTree", function (path, def) {
    var data = this, fixedPath = path.explode();
    while (fixedPath.length && data !== undefined) {
        data = data[fixedPath.shift()];
    }
    return data === undefined || data === null ? def : data;
});
defineProperty(Object.prototype, "setTree", function (path, value) {
    var _reduce = function (data) {
        if (isArray(data)) {
            while (data.length && data[data.length - 1] === undefined || data[data.length - 1] === null) {
                data.pop();
            }
        }
    }, _set = function (data, fixedPath, fixedValue, depth) {
        var i = fixedPath[depth];
        if (depth < fixedPath.length - 1) {
            if (isString(data[i]) && isNumber(fixedPath[depth + 1])) {
                data[i] = [data[i]];
            }
            else if (!isObject(data[i]) && !isArray(data[i])) {
                data[i] = isNumber(fixedPath[depth + 1]) ? [] : {};
            }
            if (!_set(data[i], fixedPath, fixedValue, depth + 1) && depth >= 1 && !Object.keys(data[i])) {
                delete data[i];
                _reduce(data);
                return false;
            }
        }
        else if (fixedValue !== data[i]) {
            if (fixedValue === undefined) {
                delete data[i];
                _reduce(data);
                return false;
            }
            data[i] = fixedValue;
        }
        return true;
    };
    if (isObject(path)) {
        for (var i in path) {
            if (path.hasOwnProperty(i)) {
                _set(this, i.explode(), path[i], 0);
            }
        }
    }
    else {
        _set(this, path.explode(), value, 0);
    }
    return this;
});
(function () {
    'use strict';
    function objectOrFunction(x) {
        return typeof x === 'function' || typeof x === 'object' && x !== null;
    }
    function isFunction(x) {
        return typeof x === 'function';
    }
    var _isArray = undefined;
    if (!Array.isArray) {
        _isArray = function (x) {
            return Object.prototype.toString.call(x) === '[object Array]';
        };
    }
    else {
        _isArray = Array.isArray;
    }
    var isArray = _isArray;
    var len = 0;
    var vertxNext = undefined;
    var customSchedulerFn = undefined;
    var asap = function asap(callback, arg) {
        queue[len] = callback;
        queue[len + 1] = arg;
        len += 2;
        if (len === 2) {
            if (customSchedulerFn) {
                customSchedulerFn(flush);
            }
            else {
                scheduleFlush();
            }
        }
    };
    function setScheduler(scheduleFn) {
        customSchedulerFn = scheduleFn;
    }
    function setAsap(asapFn) {
        asap = asapFn;
    }
    function useMutationObserver() {
        var iterations = 0;
        var observer = new MutationObserver(flush);
        var node = document.createTextNode('');
        observer.observe(node, { characterData: true });
        return function () {
            node.data = String(iterations = ++iterations % 2);
        };
    }
    function useSetTimeout() {
        var globalSetTimeout = setTimeout;
        return function () {
            return globalSetTimeout(flush, 1);
        };
    }
    var queue = new Array(1000);
    function flush() {
        for (var i = 0; i < len; i += 2) {
            var callback = queue[i];
            var arg = queue[i + 1];
            callback(arg);
            queue[i] = undefined;
            queue[i + 1] = undefined;
        }
        len = 0;
    }
    var scheduleFlush = undefined;
    if (MutationObserver) {
        scheduleFlush = useMutationObserver();
    }
    else {
        scheduleFlush = useSetTimeout();
    }
    function then(onFulfillment, onRejection) {
        var _arguments = arguments;
        var parent = this;
        var child = new this.constructor(noop);
        if (child[PROMISE_ID] === undefined) {
            makePromise(child);
        }
        var _state = parent._state;
        if (_state) {
            (function () {
                var callback = _arguments[_state - 1];
                asap(function () {
                    return invokeCallback(_state, child, callback, parent._result);
                });
            })();
        }
        else {
            subscribe(parent, child, onFulfillment, onRejection);
        }
        return child;
    }
    function resolve(object) {
        var Constructor = this;
        if (object && typeof object === 'object' && object.constructor === Constructor) {
            return object;
        }
        var promise = new Constructor(noop);
        _resolve(promise, object);
        return promise;
    }
    var PROMISE_ID = Math.random().toString(36).substring(16);
    function noop() { }
    var PENDING = void 0;
    var FULFILLED = 1;
    var REJECTED = 2;
    var GET_THEN_ERROR = new ErrorObject();
    function selfFulfillment() {
        return new TypeError("You cannot resolve a promise with itself");
    }
    function cannotReturnOwn() {
        return new TypeError('A promises callback cannot return that same promise.');
    }
    function getThen(promise) {
        try {
            return promise.then;
        }
        catch (error) {
            GET_THEN_ERROR.error = error;
            return GET_THEN_ERROR;
        }
    }
    function tryThen(then, value, fulfillmentHandler, rejectionHandler, arg) {
        try {
            then.call(value, fulfillmentHandler, rejectionHandler);
        }
        catch (e) {
            return e;
        }
    }
    function handleForeignThenable(promise, thenable, then) {
        asap(function (promise) {
            var sealed = false;
            var error = tryThen(then, thenable, function (value) {
                if (sealed) {
                    return;
                }
                sealed = true;
                if (thenable !== value) {
                    _resolve(promise, value);
                }
                else {
                    fulfill(promise, value);
                }
            }, function (reason) {
                if (sealed) {
                    return;
                }
                sealed = true;
                _reject(promise, reason);
            }, 'Settle: ' + (promise._label || ' unknown promise'));
            if (!sealed && error) {
                sealed = true;
                _reject(promise, error);
            }
        }, promise);
    }
    function handleOwnThenable(promise, thenable) {
        if (thenable._state === FULFILLED) {
            fulfill(promise, thenable._result);
        }
        else if (thenable._state === REJECTED) {
            _reject(promise, thenable._result);
        }
        else {
            subscribe(thenable, undefined, function (value) {
                return _resolve(promise, value);
            }, function (reason) {
                return _reject(promise, reason);
            });
        }
    }
    function handleMaybeThenable(promise, maybeThenable, then$$) {
        if (maybeThenable.constructor === promise.constructor && then$$ === then && maybeThenable.constructor.resolve === resolve) {
            handleOwnThenable(promise, maybeThenable);
        }
        else {
            if (then$$ === GET_THEN_ERROR) {
                _reject(promise, GET_THEN_ERROR.error);
            }
            else if (then$$ === undefined) {
                fulfill(promise, maybeThenable);
            }
            else if (isFunction(then$$)) {
                handleForeignThenable(promise, maybeThenable, then$$);
            }
            else {
                fulfill(promise, maybeThenable);
            }
        }
    }
    function _resolve(promise, value) {
        if (promise === value) {
            _reject(promise, selfFulfillment());
        }
        else if (objectOrFunction(value)) {
            handleMaybeThenable(promise, value, getThen(value));
        }
        else {
            fulfill(promise, value);
        }
    }
    function publishRejection(promise) {
        if (promise._onerror) {
            promise._onerror(promise._result);
        }
        publish(promise);
    }
    function fulfill(promise, value) {
        if (promise._state !== PENDING) {
            return;
        }
        promise._result = value;
        promise._state = FULFILLED;
        if (promise._subscribers.length !== 0) {
            asap(publish, promise);
        }
    }
    function _reject(promise, reason) {
        if (promise._state !== PENDING) {
            return;
        }
        promise._state = REJECTED;
        promise._result = reason;
        asap(publishRejection, promise);
    }
    function subscribe(parent, child, onFulfillment, onRejection) {
        var _subscribers = parent._subscribers;
        var length = _subscribers.length;
        parent._onerror = null;
        _subscribers[length] = child;
        _subscribers[length + FULFILLED] = onFulfillment;
        _subscribers[length + REJECTED] = onRejection;
        if (length === 0 && parent._state) {
            asap(publish, parent);
        }
    }
    function publish(promise) {
        var subscribers = promise._subscribers;
        var settled = promise._state;
        if (subscribers.length === 0) {
            return;
        }
        var child = undefined, callback = undefined, detail = promise._result;
        for (var i = 0; i < subscribers.length; i += 3) {
            child = subscribers[i];
            callback = subscribers[i + settled];
            if (child) {
                invokeCallback(settled, child, callback, detail);
            }
            else {
                callback(detail);
            }
        }
        promise._subscribers.length = 0;
    }
    function ErrorObject() {
        this.error = null;
    }
    var TRY_CATCH_ERROR = new ErrorObject();
    function tryCatch(callback, detail) {
        try {
            return callback(detail);
        }
        catch (e) {
            TRY_CATCH_ERROR.error = e;
            return TRY_CATCH_ERROR;
        }
    }
    function invokeCallback(settled, promise, callback, detail) {
        var hasCallback = isFunction(callback), value = undefined, error = undefined, succeeded = undefined, failed = undefined;
        if (hasCallback) {
            value = tryCatch(callback, detail);
            if (value === TRY_CATCH_ERROR) {
                failed = true;
                error = value.error;
                value = null;
            }
            else {
                succeeded = true;
            }
            if (promise === value) {
                _reject(promise, cannotReturnOwn());
                return;
            }
        }
        else {
            value = detail;
            succeeded = true;
        }
        if (promise._state !== PENDING) {
        }
        else if (hasCallback && succeeded) {
            _resolve(promise, value);
        }
        else if (failed) {
            _reject(promise, error);
        }
        else if (settled === FULFILLED) {
            fulfill(promise, value);
        }
        else if (settled === REJECTED) {
            _reject(promise, value);
        }
    }
    function initializePromise(promise, resolver) {
        try {
            resolver(function resolvePromise(value) {
                _resolve(promise, value);
            }, function rejectPromise(reason) {
                _reject(promise, reason);
            });
        }
        catch (e) {
            _reject(promise, e);
        }
    }
    var id = 0;
    function nextId() {
        return id++;
    }
    function makePromise(promise) {
        promise[PROMISE_ID] = id++;
        promise._state = undefined;
        promise._result = undefined;
        promise._subscribers = [];
    }
    function Enumerator(Constructor, input) {
        this._instanceConstructor = Constructor;
        this.promise = new Constructor(noop);
        if (!this.promise[PROMISE_ID]) {
            makePromise(this.promise);
        }
        if (isArray(input)) {
            this._input = input;
            this.length = input.length;
            this._remaining = input.length;
            this._result = new Array(this.length);
            if (this.length === 0) {
                fulfill(this.promise, this._result);
            }
            else {
                this.length = this.length || 0;
                this._enumerate();
                if (this._remaining === 0) {
                    fulfill(this.promise, this._result);
                }
            }
        }
        else {
            _reject(this.promise, validationError());
        }
    }
    function validationError() {
        return new Error('Array Methods must be provided an Array');
    }
    ;
    Enumerator.prototype._enumerate = function () {
        var length = this.length;
        var _input = this._input;
        for (var i = 0; this._state === PENDING && i < length; i++) {
            this._eachEntry(_input[i], i);
        }
    };
    Enumerator.prototype._eachEntry = function (entry, i) {
        var c = this._instanceConstructor;
        var resolve$$ = c.resolve;
        if (resolve$$ === resolve) {
            var _then = getThen(entry);
            if (_then === then && entry._state !== PENDING) {
                this._settledAt(entry._state, i, entry._result);
            }
            else if (typeof _then !== 'function') {
                this._remaining--;
                this._result[i] = entry;
            }
            else if (c === Promise) {
                var promise = new c(noop);
                handleMaybeThenable(promise, entry, _then);
                this._willSettleAt(promise, i);
            }
            else {
                this._willSettleAt(new c(function (resolve$$) {
                    return resolve$$(entry);
                }), i);
            }
        }
        else {
            this._willSettleAt(resolve$$(entry), i);
        }
    };
    Enumerator.prototype._settledAt = function (state, i, value) {
        var promise = this.promise;
        if (promise._state === PENDING) {
            this._remaining--;
            if (state === REJECTED) {
                _reject(promise, value);
            }
            else {
                this._result[i] = value;
            }
        }
        if (this._remaining === 0) {
            fulfill(promise, this._result);
        }
    };
    Enumerator.prototype._willSettleAt = function (promise, i) {
        var enumerator = this;
        subscribe(promise, undefined, function (value) {
            return enumerator._settledAt(FULFILLED, i, value);
        }, function (reason) {
            return enumerator._settledAt(REJECTED, i, reason);
        });
    };
    function all(entries) {
        return new Enumerator(this, entries).promise;
    }
    function race(entries) {
        var Constructor = this;
        if (!isArray(entries)) {
            return new Constructor(function (_, reject) {
                return reject(new TypeError('You must pass an array to race.'));
            });
        }
        else {
            return new Constructor(function (resolve, reject) {
                var length = entries.length;
                for (var i = 0; i < length; i++) {
                    Constructor.resolve(entries[i]).then(resolve, reject);
                }
            });
        }
    }
    function reject(reason) {
        var Constructor = this;
        var promise = new Constructor(noop);
        _reject(promise, reason);
        return promise;
    }
    function needsResolver() {
        throw new TypeError('You must pass a resolver function as the first argument to the promise constructor');
    }
    function needsNew() {
        throw new TypeError("Failed to construct 'Promise': Please use the 'new' operator, this object constructor cannot be called as a function.");
    }
    function Promise(resolver) {
        this[PROMISE_ID] = nextId();
        this._result = this._state = undefined;
        this._subscribers = [];
        if (noop !== resolver) {
            typeof resolver !== 'function' && needsResolver();
            this instanceof Promise ? initializePromise(this, resolver) : needsNew();
        }
    }
    Promise.all = all;
    Promise.race = race;
    Promise.resolve = resolve;
    Promise.reject = reject;
    Promise._setScheduler = setScheduler;
    Promise._setAsap = setAsap;
    Promise._asap = asap;
    Promise.prototype = {
        constructor: Promise,
        then: then,
        'catch': function _catch(onRejection) {
            return this.then(null, onRejection);
        }
    };
    function polyfill() {
        var local = window;
        var P = local.Promise;
        if (P) {
            var promiseToString = null;
            try {
                promiseToString = Object.prototype.toString.call(P.resolve());
            }
            catch (e) {
            }
            if (promiseToString === '[object Promise]' && !P.cast) {
                return;
            }
        }
        local.Promise = Promise;
    }
    Promise.polyfill = polyfill;
    Promise.Promise = Promise;
    Promise.polyfill();
})();
defineProperty(RegExp.prototype, "expand", function () {
    function expandRegexp(regExp) {
        if (regExp[0] === "^") {
            regExp = regExp.substr(1);
        }
        else {
            regExp = ".*" + regExp;
        }
        if (regExp[regExp.length - 1] === "^") {
            regExp = regExp.substr(0, regExp.length - 2);
        }
        else {
            regExp += ".*";
        }
        var myRegExp = regExp.replace(/^[\n\r\^]+|[\n\r\$]+$/g, ""), matches;
        myRegExp = myRegExp.replace(/\(([^|]*?\))(\?)/g, function ($0, $1, $2) {
            return $2 ? "(" + $1 + "|)?" : $1;
        });
        myRegExp = myRegExp.replace(/(\\?[^\\\)\*])\?/g, function ($0, $1) {
            return "(" + $1 + "|)";
        });
        while ((matches = myRegExp.match(/\[.*?\]/g))) {
            var squareBrackets = matches, sb1 = squareBrackets[0], sb = sb1.substring(1, sb1.length - 1), regHyphen = /.?-.?/;
            while (myRegExp.match(regHyphen) != null) {
                var hyphen = myRegExp.match(regHyphen), stringLeft = myRegExp.split(hyphen[0])[0];
                if (stringLeft.charAt(stringLeft.length - 1) == "|") {
                    stringLeft = stringLeft.substring(0, stringLeft.length - 1);
                }
                var stringRight = myRegExp.split(hyphen[0])[1];
                var hy1 = hyphen[0].charAt(0), hy2 = hyphen[0].charAt(2), hy = "", charList = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ%8A%8C%C0%C1%C2%C3%C4%C5%C6%C7%C8%C9%CA%CB%CC%CD%CE%CF%D0%D1%D2%D3%D4%D5%D6%D8%D9%DA%DB%DC%DD%DE%9Fabcdefghijklmnopqrstuvwxyz%9A%9C%E0%E1%E2%E3%E4%E5%E6%E7%E8%E9%EA%EB%EC%ED%EE%EF%F0%F1%F2%F3%F4%F5%F6%F8%F9%FA%FB%FC%FD%FE%FFéèÿ";
                for (var c = charList.indexOf(hy1); c < charList.indexOf(hy2) + 1; c++) {
                    hy += charList.charAt(c);
                }
                myRegExp = stringLeft + hy + stringRight;
            }
            var sb2 = "(";
            for (var i = 0; i < sb.length; i++) {
                sb2 += sb.charAt(i) + "|";
            }
            sb2 = sb2.substring(0, sb2.length - 1) + ")";
            myRegExp = replaceWord(myRegExp, sb1, sb2);
        }
        myRegExp = myRegExp.replace(/\\?%/g, "%25").replace(/\\(.)/g, function ($0, $1) {
            return "%" + $1.charCodeAt(0).toString(16);
        });
        if (/[^\\][\+\*\.\{\}]/.test(" " + myRegExp)) {
            var altExp = regExp.replace(/\.[\*\?]/g, "");
            return altExp === regExp ? [regExp] : [regExp, altExp];
        }
        myRegExp = replaceNestedOrs(myRegExp) || myRegExp;
        var result = expandOrs(myRegExp);
        for (var i = 0; i < result.length; i++) {
            result[i] = decodeURIComponent(result[i]);
        }
        return result;
    }
    function getNextNestedOr(str) {
        var start = 0, isNested = false, depth = 0;
        for (var i = 0; i < str.length; i++) {
            switch (str[i]) {
                case "(":
                    depth++;
                    if (depth === 1) {
                        start = i;
                    }
                    else if (depth === 2) {
                        isNested = true;
                    }
                    break;
                case ")":
                    depth--;
                    if (!depth && isNested) {
                        return str.substring(start, i + 1);
                    }
                    break;
            }
        }
        return;
    }
    function getGroup(str) {
        var matches = str.match(/[^(|)]*\(([^(|)]*\|[^(|)]*)+\)[^(|)]*/g);
        if (matches) {
            for (var i = 0; i < matches.length; i++) {
                var match = matches[i];
                if (match[0] !== "(" || match[match.length - 1] !== ")") {
                    return match;
                }
            }
        }
    }
    function replaceNestedOrs(myRegExp) {
        var regSingleOrs = /\([^()]*\)/, regSingleOrsTotal = /^\([^()]*\)$/;
        while (getNextNestedOr(myRegExp)) {
            var nestedOrs = getNextNestedOr(myRegExp), nestedOrsOriginal = nestedOrs, matches = void 0;
            while (getGroup(nestedOrs)) {
                var myParent = getGroup(nestedOrs), leftChar = nestedOrs[nestedOrs.indexOf(myParent) - 1], rightChar = nestedOrs[nestedOrs.indexOf(myParent) + myParent.length], outerChars = leftChar + rightChar, leftPar = "", rightPar = "";
                if (outerChars == ")(") {
                    break;
                }
                switch (outerChars) {
                    case "||":
                    case "()":
                        break;
                    case "((":
                    case "))":
                    case "(|":
                    case "|(":
                    case ")|":
                    case "|)":
                        leftPar = "(";
                        rightPar = ")";
                        break;
                    default:
                        alert("ERROR: switch did not work ???\nouterChars = " + outerChars);
                        break;
                }
                var myResult = leftPar + expandOrs(myParent).join("|") + rightPar;
                nestedOrs = replaceWord(nestedOrs, myParent, myResult);
            }
            while ((matches = nestedOrs.match(/(\(|\|)\([^(|)]*\|[^(|)]*\)(\|\([^(|)]*\|[^(|)]*\))*(\)|\|)/))) {
                var plainOrs = matches[0];
                nestedOrs = replaceWord(nestedOrs, plainOrs, "(" + plainOrs.replace(/\(|\)/g, "") + ")");
                if (getGroup(nestedOrs)) {
                    myRegExp = replaceWord(myRegExp, nestedOrsOriginal, nestedOrs);
                    continue;
                }
            }
            while ((matches = nestedOrs.match(/(\([^(]*?\|*?\)){2,99}/))) {
                nestedOrs = nestedOrs.replace(matches[0], expandOrs(matches[0]).join("|"));
            }
            if (getGroup(nestedOrs)) {
                myRegExp = replaceWord(myRegExp, nestedOrsOriginal, nestedOrs);
            }
            else {
                while (nestedOrs.match(regSingleOrs) != null) {
                    if (nestedOrs.match(regSingleOrsTotal)[0] == nestedOrs) {
                        break;
                    }
                    var singleParens = nestedOrs.match(regSingleOrs)[0], myResult = singleParens.substring(1, singleParens.length - 1);
                    nestedOrs = replaceWord(nestedOrs, singleParens, myResult);
                    if (getGroup(nestedOrs)) {
                        myRegExp = replaceWord(myRegExp, nestedOrsOriginal, nestedOrs);
                    }
                }
                myRegExp = replaceWord(myRegExp, nestedOrsOriginal, nestedOrs);
            }
        }
        return myRegExp;
    }
    function expandOrs(str) {
        if (!str || !/\(.*\|.*\)/.test(str)) {
            return [str];
        }
        var parts = [], expanded = [""], lastIndex = 0;
        str.split(/(\(.*?\))/).forEach(function (part) {
            var data = part[0] === "(" && part[part.length - 1] === ")"
                ? part.substr(1, part.length - 2).split("|")
                : part;
            parts.push([
                lastIndex = str.indexOf(part, lastIndex),
                data
            ]);
        });
        parts.forEach(function (part) {
            for (var i = 0, length_1 = expanded.length; i < length_1; i++) {
                if (isString(part[1])) {
                    expanded[i] += part[1];
                }
                else {
                    for (var j = 0; j < part[1].length; j++) {
                        expanded.push(expanded[i] + part[1][j]);
                    }
                }
            }
        });
        return expanded;
    }
    function replaceWord(str, substr, newSubstr) {
        return str.replace(new RegExp(substr.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1"), "g"), newSubstr);
    }
    return expandRegexp(this.source).sort();
});
defineProperty(String.prototype, "endsWith", function (searchString, position) {
    var subjectString = this.toString();
    if (typeof position !== "number" || !isFinite(position) || Math.floor(position) !== position || position > subjectString.length) {
        position = subjectString.length;
    }
    position -= searchString.length;
    var lastIndex = subjectString.indexOf(searchString, position);
    return lastIndex !== -1 && lastIndex === position;
});
defineProperty(String.prototype, "repeat", function (count) {
    if (this == null) {
        throw new TypeError('can\'t convert ' + this + ' to object');
    }
    var str = '' + this;
    count = +count;
    if (count != count) {
        count = 0;
    }
    if (count < 0) {
        throw new RangeError('repeat count must be non-negative');
    }
    if (count == Infinity) {
        throw new RangeError('repeat count must be less than infinity');
    }
    count = Math.floor(count);
    if (str.length == 0 || count == 0) {
        return '';
    }
    if (str.length * count >= 1 << 28) {
        throw new RangeError('repeat count must not overflow maximum string size');
    }
    var rpt = '';
    for (;;) {
        if ((count & 1) == 1) {
            rpt += str;
        }
        count >>>= 1;
        if (count == 0) {
            break;
        }
        str += str;
    }
    return rpt;
});
(function () {
    var fromCharCode = String.fromCharCode, keyStrBase64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", keyStrUriSafe = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+-$";
    function _compress(uncompressed, bitsPerChar, getCharFromInt) {
        if (!uncompressed)
            return "";
        var i, value, context_dictionary = {}, context_dictionaryToCreate = {}, context_c = "", context_wc = "", context_w = "", context_enlargeIn = 2, context_dictSize = 3, context_numBits = 2, context_data = [], context_data_val = 0, context_data_position = 0, ii;
        for (ii = 0; ii < uncompressed.length; ii += 1) {
            context_c = uncompressed.charAt(ii);
            if (!Object.prototype.hasOwnProperty.call(context_dictionary, context_c)) {
                context_dictionary[context_c] = context_dictSize++;
                context_dictionaryToCreate[context_c] = true;
            }
            context_wc = context_w + context_c;
            if (Object.prototype.hasOwnProperty.call(context_dictionary, context_wc)) {
                context_w = context_wc;
            }
            else {
                if (Object.prototype.hasOwnProperty.call(context_dictionaryToCreate, context_w)) {
                    if (context_w.charCodeAt(0) < 256) {
                        for (i = 0; i < context_numBits; i++) {
                            context_data_val = (context_data_val << 1);
                            if (context_data_position == bitsPerChar - 1) {
                                context_data_position = 0;
                                context_data.push(getCharFromInt(context_data_val));
                                context_data_val = 0;
                            }
                            else {
                                context_data_position++;
                            }
                        }
                        value = context_w.charCodeAt(0);
                        for (i = 0; i < 8; i++) {
                            context_data_val = (context_data_val << 1) | (value & 1);
                            if (context_data_position == bitsPerChar - 1) {
                                context_data_position = 0;
                                context_data.push(getCharFromInt(context_data_val));
                                context_data_val = 0;
                            }
                            else {
                                context_data_position++;
                            }
                            value = value >> 1;
                        }
                    }
                    else {
                        value = 1;
                        for (i = 0; i < context_numBits; i++) {
                            context_data_val = (context_data_val << 1) | value;
                            if (context_data_position == bitsPerChar - 1) {
                                context_data_position = 0;
                                context_data.push(getCharFromInt(context_data_val));
                                context_data_val = 0;
                            }
                            else {
                                context_data_position++;
                            }
                            value = 0;
                        }
                        value = context_w.charCodeAt(0);
                        for (i = 0; i < 16; i++) {
                            context_data_val = (context_data_val << 1) | (value & 1);
                            if (context_data_position == bitsPerChar - 1) {
                                context_data_position = 0;
                                context_data.push(getCharFromInt(context_data_val));
                                context_data_val = 0;
                            }
                            else {
                                context_data_position++;
                            }
                            value = value >> 1;
                        }
                    }
                    context_enlargeIn--;
                    if (context_enlargeIn == 0) {
                        context_enlargeIn = Math.pow(2, context_numBits);
                        context_numBits++;
                    }
                    delete context_dictionaryToCreate[context_w];
                }
                else {
                    value = context_dictionary[context_w];
                    for (i = 0; i < context_numBits; i++) {
                        context_data_val = (context_data_val << 1) | (value & 1);
                        if (context_data_position == bitsPerChar - 1) {
                            context_data_position = 0;
                            context_data.push(getCharFromInt(context_data_val));
                            context_data_val = 0;
                        }
                        else {
                            context_data_position++;
                        }
                        value = value >> 1;
                    }
                }
                context_enlargeIn--;
                if (context_enlargeIn == 0) {
                    context_enlargeIn = Math.pow(2, context_numBits);
                    context_numBits++;
                }
                context_dictionary[context_wc] = context_dictSize++;
                context_w = String(context_c);
            }
        }
        if (context_w !== "") {
            if (Object.prototype.hasOwnProperty.call(context_dictionaryToCreate, context_w)) {
                if (context_w.charCodeAt(0) < 256) {
                    for (i = 0; i < context_numBits; i++) {
                        context_data_val = (context_data_val << 1);
                        if (context_data_position == bitsPerChar - 1) {
                            context_data_position = 0;
                            context_data.push(getCharFromInt(context_data_val));
                            context_data_val = 0;
                        }
                        else {
                            context_data_position++;
                        }
                    }
                    value = context_w.charCodeAt(0);
                    for (i = 0; i < 8; i++) {
                        context_data_val = (context_data_val << 1) | (value & 1);
                        if (context_data_position == bitsPerChar - 1) {
                            context_data_position = 0;
                            context_data.push(getCharFromInt(context_data_val));
                            context_data_val = 0;
                        }
                        else {
                            context_data_position++;
                        }
                        value = value >> 1;
                    }
                }
                else {
                    value = 1;
                    for (i = 0; i < context_numBits; i++) {
                        context_data_val = (context_data_val << 1) | value;
                        if (context_data_position == bitsPerChar - 1) {
                            context_data_position = 0;
                            context_data.push(getCharFromInt(context_data_val));
                            context_data_val = 0;
                        }
                        else {
                            context_data_position++;
                        }
                        value = 0;
                    }
                    value = context_w.charCodeAt(0);
                    for (i = 0; i < 16; i++) {
                        context_data_val = (context_data_val << 1) | (value & 1);
                        if (context_data_position == bitsPerChar - 1) {
                            context_data_position = 0;
                            context_data.push(getCharFromInt(context_data_val));
                            context_data_val = 0;
                        }
                        else {
                            context_data_position++;
                        }
                        value = value >> 1;
                    }
                }
                context_enlargeIn--;
                if (context_enlargeIn == 0) {
                    context_enlargeIn = Math.pow(2, context_numBits);
                    context_numBits++;
                }
                delete context_dictionaryToCreate[context_w];
            }
            else {
                value = context_dictionary[context_w];
                for (i = 0; i < context_numBits; i++) {
                    context_data_val = (context_data_val << 1) | (value & 1);
                    if (context_data_position == bitsPerChar - 1) {
                        context_data_position = 0;
                        context_data.push(getCharFromInt(context_data_val));
                        context_data_val = 0;
                    }
                    else {
                        context_data_position++;
                    }
                    value = value >> 1;
                }
            }
            context_enlargeIn--;
            if (context_enlargeIn == 0) {
                context_enlargeIn = Math.pow(2, context_numBits);
                context_numBits++;
            }
        }
        value = 2;
        for (i = 0; i < context_numBits; i++) {
            context_data_val = (context_data_val << 1) | (value & 1);
            if (context_data_position == bitsPerChar - 1) {
                context_data_position = 0;
                context_data.push(getCharFromInt(context_data_val));
                context_data_val = 0;
            }
            else {
                context_data_position++;
            }
            value = value >> 1;
        }
        while (true) {
            context_data_val = (context_data_val << 1);
            if (context_data_position == bitsPerChar - 1) {
                context_data.push(getCharFromInt(context_data_val));
                break;
            }
            else
                context_data_position++;
        }
        return context_data.join('');
    }
    defineProperty(String.prototype, "compress", function (encode) {
        if (typeof encode === "string") {
            switch (encode.toLowerCase()) {
                case "base64":
                    {
                        var compressed = _compress(this, 6, function (a) { return keyStrBase64.charAt(a); });
                        return compressed + "=".repeat(compressed.length % 4);
                    }
                case "uri":
                    return _compress(this, 6, function (a) { return keyStrUriSafe.charAt(a); });
                case "utf-16":
                    return _compress(this, 15, function (a) { return fromCharCode(a + 32); }) + " ";
                case "uint":
                    {
                        var compressed = _compress(this, 16, function (a) { return fromCharCode(a); }), buf = new Uint8Array(compressed.length * 2);
                        for (var i = 0, TotalLen = compressed.length; i < TotalLen; i++) {
                            var current_value = compressed.charCodeAt(i);
                            buf[i * 2] = current_value >>> 8;
                            buf[i * 2 + 1] = current_value % 256;
                        }
                        return buf;
                    }
                default:
                    console.error("Error: Encode type must be one of: base64, uri, utf-16, uint");
            }
        }
        return _compress(this, 16, function (a) { return fromCharCode(a); });
    });
})();
(function () {
    var fromCharCode = String.fromCharCode, keyStrBase64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", keyStrUriSafe = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+-$", baseReverseDic = {};
    function getBaseValue(alphabet, character) {
        if (!baseReverseDic[alphabet]) {
            baseReverseDic[alphabet] = {};
            for (var i = 0; i < alphabet.length; i++) {
                baseReverseDic[alphabet][alphabet.charAt(i)] = i;
            }
        }
        return baseReverseDic[alphabet][character];
    }
    function _decompress(length, resetValue, getNextValue) {
        var dictionary = [], next, enlargeIn = 4, dictSize = 4, numBits = 3, entry = "", result = [], i, w, bits, resb, maxpower, power, c, data = { val: getNextValue(0), position: resetValue, index: 1 };
        for (i = 0; i < 3; i += 1) {
            dictionary[i] = i;
        }
        bits = 0;
        maxpower = Math.pow(2, 2);
        power = 1;
        while (power != maxpower) {
            resb = data.val & data.position;
            data.position >>= 1;
            if (data.position == 0) {
                data.position = resetValue;
                data.val = getNextValue(data.index++);
            }
            bits |= (resb > 0 ? 1 : 0) * power;
            power <<= 1;
        }
        switch (next = bits) {
            case 0:
                bits = 0;
                maxpower = Math.pow(2, 8);
                power = 1;
                while (power != maxpower) {
                    resb = data.val & data.position;
                    data.position >>= 1;
                    if (data.position == 0) {
                        data.position = resetValue;
                        data.val = getNextValue(data.index++);
                    }
                    bits |= (resb > 0 ? 1 : 0) * power;
                    power <<= 1;
                }
                c = fromCharCode(bits);
                break;
            case 1:
                bits = 0;
                maxpower = Math.pow(2, 16);
                power = 1;
                while (power != maxpower) {
                    resb = data.val & data.position;
                    data.position >>= 1;
                    if (data.position == 0) {
                        data.position = resetValue;
                        data.val = getNextValue(data.index++);
                    }
                    bits |= (resb > 0 ? 1 : 0) * power;
                    power <<= 1;
                }
                c = fromCharCode(bits);
                break;
            case 2:
                return "";
        }
        dictionary[3] = c;
        w = c;
        result.push(c);
        while (true) {
            if (data.index > length) {
                return "";
            }
            bits = 0;
            maxpower = Math.pow(2, numBits);
            power = 1;
            while (power != maxpower) {
                resb = data.val & data.position;
                data.position >>= 1;
                if (data.position == 0) {
                    data.position = resetValue;
                    data.val = getNextValue(data.index++);
                }
                bits |= (resb > 0 ? 1 : 0) * power;
                power <<= 1;
            }
            switch (c = bits) {
                case 0:
                    bits = 0;
                    maxpower = Math.pow(2, 8);
                    power = 1;
                    while (power != maxpower) {
                        resb = data.val & data.position;
                        data.position >>= 1;
                        if (data.position == 0) {
                            data.position = resetValue;
                            data.val = getNextValue(data.index++);
                        }
                        bits |= (resb > 0 ? 1 : 0) * power;
                        power <<= 1;
                    }
                    dictionary[dictSize++] = fromCharCode(bits);
                    c = dictSize - 1;
                    enlargeIn--;
                    break;
                case 1:
                    bits = 0;
                    maxpower = Math.pow(2, 16);
                    power = 1;
                    while (power != maxpower) {
                        resb = data.val & data.position;
                        data.position >>= 1;
                        if (data.position == 0) {
                            data.position = resetValue;
                            data.val = getNextValue(data.index++);
                        }
                        bits |= (resb > 0 ? 1 : 0) * power;
                        power <<= 1;
                    }
                    dictionary[dictSize++] = fromCharCode(bits);
                    c = dictSize - 1;
                    enlargeIn--;
                    break;
                case 2:
                    return result.join('');
            }
            if (enlargeIn == 0) {
                enlargeIn = Math.pow(2, numBits);
                numBits++;
            }
            if (dictionary[c]) {
                entry = dictionary[c];
            }
            else {
                if (c === dictSize) {
                    entry = w + w.charAt(0);
                }
                else {
                    return null;
                }
            }
            result.push(entry);
            dictionary[dictSize++] = w + entry.charAt(0);
            enlargeIn--;
            w = entry;
            if (enlargeIn == 0) {
                enlargeIn = Math.pow(2, numBits);
                numBits++;
            }
        }
    }
    defineProperty(String.prototype, "decompress", function (encode) {
        var compressed = this;
        if (typeof encode === "string") {
            switch (encode.toLowerCase()) {
                case "base64":
                    return _decompress(compressed.length, 32, function (index) { return getBaseValue(keyStrBase64, compressed.charAt(index)); });
                case "uri":
                    compressed = compressed.replace(/ /g, "+");
                    return _decompress(compressed.length, 32, function (index) { return getBaseValue(keyStrUriSafe, compressed.charAt(index)); });
                case "utf-16":
                    return _decompress(compressed.length, 16384, function (index) { return compressed.charCodeAt(index) - 32; });
                case "uint":
                    break;
                default:
                    console.error("Error: Encode type must be one of: base64, uri, utf-16, uint");
            }
        }
        return _decompress(compressed.length, 32768, function (index) { return compressed.charCodeAt(index); });
    });
    defineProperty(Uint8Array.prototype, "decompress", function (encode) {
        var i = 0, length = this.length / 2, result = [];
        for (; i < length; i++) {
            result.push(fromCharCode(this[i * 2] * 256 + this[i * 2 + 1]));
        }
        return result.join("").decompress();
    });
})();
defineProperty(String.prototype, "explode", function () {
    var arr = this.split("."), length = arr.length;
    for (var i = 0; i < length; i++) {
        if (i < length - 1 && arr[i].substr(-1) === "\\") {
            arr[i] = arr[i].substr(0, arr[i].length - 1) + "." + arr.splice(i + 1, 1);
            i--;
        }
        else {
            var num = parseInt(arr[i], 10);
            if (String(num) === arr[i]) {
                arr[i] = num;
            }
        }
    }
    return arr;
});
defineProperty(String.prototype, "includes", function (searchString, position) {
    position = position || 0;
    if (position + searchString.length > this.length) {
        return false;
    }
    return this.indexOf(searchString, position) !== -1;
});
defineProperty(String.prototype, "regex", function (regexp) {
    var rx, num, length, matches = this.match(regexp);
    if (matches) {
        if (regexp.global) {
            if (/(^|[^\\]|[^\\](\\\\)*)\([^?]/.test(regexp.source)) {
                rx = new RegExp(regexp.source, (regexp.ignoreCase ? "i" : "") + (regexp.multiline ? "m" : ""));
            }
        }
        else {
            matches.shift();
        }
        length = matches.length;
        while (length--) {
            if (matches[length]) {
                if (rx) {
                    matches[length] = matches[length].regex(rx);
                }
                else {
                    num = parseFloat(matches[length]);
                    if (!isNaN(num) && String(num) === matches[length]) {
                        matches[length] = num;
                    }
                }
            }
        }
        if (!rx && matches.length === 1) {
            return matches[0];
        }
    }
    return matches;
});
defineProperty(String.prototype, "startsWith", function (searchString, position) {
    return this.substr(position || 0, searchString.length) === searchString;
});
defineProperty(String.prototype, "ucfirst", function () {
    return this.charAt(0).toLocaleUpperCase() + this.slice(1).toLowerCase();
});
(function () {
    try {
        var console_1 = window.console || {}, bind_1 = Function.prototype.bind;
        ["log", "warn", "error", "debug", "info", "assert", "trace"].forEach(function (method) {
            console_1[method] = console_1[method] || function () { };
            if (bind_1 && isObject(console_1[method])) {
                console_1[method] = bind_1.call(console_1[method], console_1);
            }
        });
    }
    catch (e) {
    }
}());
function getBoundingClientRect(el, scale) {
    var box = el.getBoundingClientRect();
    if (scale) {
        box = {
            top: box.top / ic.scaleFactor,
            right: box.right / ic.scaleFactor,
            bottom: box.bottom / ic.scaleFactor,
            left: box.left / ic.scaleFactor,
            width: box.width / ic.scaleFactor,
            height: box.height / ic.scaleFactor,
        };
    }
    return box;
}
function appendChild(parent, child) {
    parent.appendChild(child);
}
function persist(target, propertyKey) {
    if (target) {
        if (!target["_persist"]) {
            defineProperty(target, "_persist", []);
        }
        target["_persist"].push(propertyKey);
    }
}
function final(target, propertyKey) {
    var value = target[propertyKey], fn = function (target, propertyKey, value) {
        Object.defineProperty(target, propertyKey, {
            "value": value,
            "writable": false,
            "enumerable": true
        });
    };
    if (isUndefined(value)) {
        Object.defineProperty(target, propertyKey, {
            "set": function (value) {
                fn(this, propertyKey, value);
            },
            "configurable": true
        });
    }
    else {
        fn(target, propertyKey, value);
    }
}
function isArray(arr) {
    return Object.prototype.toString.call(arr) === "[object Array]";
}
function isBoolean(bool) {
    return bool === true || bool === false;
}
function isFunction(fn) {
    return Object.prototype.toString.call(fn) === "[object Function]";
}
function isNumber(num) {
    return typeof num === "number";
}
function isUndefined(val) {
    return val === undefined;
}
function isObject(obj) {
    if (!obj || String(obj) !== "[object Object]") {
        return false;
    }
    var proto = Object.getPrototypeOf(obj), constructor = proto && proto.hasOwnProperty("constructor") && proto.constructor;
    return !proto || (typeof constructor === "function" && String(constructor) === String(Object));
}
function isString(str) {
    return typeof str === "string";
}
function closestScroll(node) {
    for (; node; node = node.parentElement) {
        if (/^(auto|scroll)$/i.test(getComputedStyle(node).overflowY)) {
            return node;
        }
    }
}
var ic;
(function (ic) {
    var requestAnimationFrame = window.requestAnimationFrame, rAFname = {}, rAFwrapper = false, immediateIndex = 0, immediateName = {}, immediateCall = false, immediateSecret = "cb" + Math.random(), scaleTop = 0, scaleLeft = 0, userAgent = navigator.userAgent, iPad = userAgent.includes("Safari") && !userAgent.includes("Chrome") && !userAgent.includes("Edge") && window.orientation !== undefined;
    ic.scaleFactor = 1;
    function fixScaling() {
        var body = document.body, html = document.documentElement, style = html.style, zoom = iPad ? (html.clientWidth / window.innerWidth) : 1;
        style.transform = style.transformOrigin = "";
        var box = html.getBoundingClientRect(), scaleWidth = Math.min(1, innerWidth * zoom / body.clientWidth), scaleHeight = Math.min(1, innerHeight * zoom / body.clientHeight), isHorizontal = scaleWidth > scaleHeight;
        ic.scaleFactor = Math.min(scaleWidth, scaleHeight);
        style.transform = ic.scaleFactor >= 1 ? "" : "scale(" + ic.scaleFactor + ")";
        style.transformOrigin = ic.scaleFactor >= 1 ? "" : (isHorizontal ? (iPad && Math.abs(window.orientation) === 90 ? "25%" : "50%") : "0") + " 0";
        box = html.getBoundingClientRect();
        scaleTop = box.top;
        scaleLeft = box.left;
    }
    ic.fixScaling = fixScaling;
    ;
    function getCoords(event, scale) {
        if (event) {
            var isTouch = !!event.changedTouches, clientX = (isTouch ? event.changedTouches[0] : event).clientX, clientY = (isTouch ? event.changedTouches[0] : event).clientY;
            return [
                scale !== false ? (clientX - scaleLeft) / ic.scaleFactor : clientX,
                scale !== false ? (clientY - scaleTop) / ic.scaleFactor : clientY
            ];
        }
        return [0, 0];
    }
    ic.getCoords = getCoords;
    function callbackAndDelete(obj, args) {
        Object.keys(obj).forEach(function (name) {
            var callback = obj[name];
            delete obj[name];
            callback(args);
        });
    }
    function rAFCallback(time) {
        rAFwrapper = false;
        callbackAndDelete(rAFname, time);
    }
    function immediateCallback() {
        immediateCall = false;
        immediateIndex = 0;
        callbackAndDelete(immediateName);
    }
    function rAF(name, callback) {
        var fn;
        if (isString(name)) {
            fn = callback;
            if (callback) {
                rAFname[name] = callback;
                if (rAFwrapper) {
                    fn = undefined;
                }
                else {
                    rAFwrapper = true;
                    fn = rAFCallback;
                }
            }
            else {
                delete rAFname[name];
            }
        }
        else {
            fn = name;
        }
        if (fn) {
            if (requestAnimationFrame && !document.hidden) {
                requestAnimationFrame(fn);
            }
            else {
                setTimeout(fn, 16);
            }
        }
    }
    ic.rAF = rAF;
    ;
    addEventListener("message", function (event) {
        if (event.source == window && event.data == immediateSecret) {
            event.stopPropagation();
            immediateCallback();
        }
    }, true);
    function setImmediate(name, callback) {
        if (isString(name)) {
            if (callback) {
                immediateName[name] = callback;
            }
            else {
                delete immediateName[name];
                return;
            }
        }
        else if (isFunction(name)) {
            for (var i in immediateName) {
                if (immediateName[i] === name) {
                    return;
                }
            }
            immediateName[String(immediateIndex++)] = name;
        }
        else {
            return;
        }
        if (!immediateCall) {
            immediateCall = true;
            if (postMessage) {
                postMessage(immediateSecret, "*");
            }
            else {
                setTimeout(immediateCallback, 0);
            }
        }
    }
    ic.setImmediate = setImmediate;
    ;
})(ic || (ic = {}));
;
function createElement(tagName) {
    return document.createElement(tagName);
}
function getElementById(elementId) {
    return document.getElementById(elementId);
}
function getElementsByTagName(tagname) {
    return Array.prototype.slice.call(document.getElementsByTagName(tagname));
}
function querySelector(root, selector) {
    if (isString(root)) {
        selector = root;
        root = document;
    }
    return root.querySelector(selector);
}
function querySelectorSVG(root, selector) {
    return querySelector(root, selector);
}
function querySelectorAll(root, selector) {
    if (isString(root)) {
        selector = root;
        root = document;
    }
    return root ? Array.prototype.slice.call(root.querySelectorAll(selector)) : [];
}
var ic;
(function (ic) {
    var WidgetEvent = (function () {
        function WidgetEvent() {
        }
        WidgetEvent.prototype.getEventHandler = function (event) {
            switch (event) {
                case "blur":
                    return this.onBlur;
                case "click":
                    return this.onClick;
                case ".css":
                    return this.onCss;
                case "focus":
                    return this.onFocus;
                case ".frame":
                    return this.onFrame;
                case "keypress":
                    return this.onKeyPress;
                case "keyup":
                    return this.onKeyUp;
                case "mousedown":
                case "touchstart":
                    return this.onMouseDown;
                case "mouseenter":
                    return this.onMouseEnter;
                case "mouseleave":
                    return this.onMouseLeave;
                case "mouseup":
                case "touchend":
                    return this.onMouseUp;
                case "!mousemove":
                case "!touchmove":
                    return this.allMouseMove;
                case "!mouseup":
                case "!touchend":
                    return this.allMouseUp;
                case ".persist":
                    return this.onPersist;
                case ".reset":
                    return this.onReset;
                case ".resize":
                    return this.onResize;
                case ".reveal":
                    return this.onReveal;
                case ".score":
                    return this.onScore;
                case ".screen":
                    return this.onScreen;
                case ".scroll":
                    return this.onScroll;
                case ".state":
                    return this.onState;
                case ".submit":
                    return this.onSubmit;
                case ".timeout":
                    return this.onTimeout;
                case ".tick":
                    return this.onTick;
                default:
                    console.error("Unknown event", event);
            }
        };
        return WidgetEvent;
    }());
    ic.WidgetEvent = WidgetEvent;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    ic.version["node"] = 1;
    var isHas;
    var indexSelector;
    var cache = new WeakMap();
    var cacheIndex = [];
    function filterWidget(node) {
        return node.icWidget ? NodeFilter.FILTER_ACCEPT : NodeFilter.FILTER_SKIP;
    }
    filterWidget.acceptNode = filterWidget;
    function filterInputWidget(node) {
        return node.icWidget && node.icWidget.isInputWidget ? NodeFilter.FILTER_ACCEPT : NodeFilter.FILTER_SKIP;
    }
    filterInputWidget.acceptNode = filterInputWidget;
    var WidgetNode = (function (_super) {
        __extends(WidgetNode, _super);
        function WidgetNode(element) {
            var _this = _super.call(this) || this;
            _this.element = element;
            var widgetType = _this.constructor;
            element.icWidget = _this;
            if (_this instanceof ic.ScreenWidget) {
                new MutationObserver(function (mutations) {
                    mutations.forEach(function (mutation) {
                        Array.from(mutation.addedNodes).forEach(function (node) {
                            if (node instanceof HTMLElement) {
                                _this.get(node, true).forEach(function (widget) {
                                    widget && widget.invalidate();
                                });
                            }
                        });
                    });
                }).observe(element, { childList: true, subtree: true });
            }
            else if (_this instanceof ic.ScreensWidget) {
                new MutationObserver(function () {
                    cache = new WeakMap();
                }).observe(element, { childList: true, subtree: true });
            }
            if (widgetType.isInputWidget) {
                if (!indexSelector) {
                    indexSelector = [
                        ic.ActivityWidget.selector,
                        ic.ActivitiesWidget.selector,
                        ic.ScreenWidget.selector,
                        ic.ScreensWidget.selector,
                        "body"
                    ].join(",");
                }
                var indexParent_1 = element.parentElement.closest(indexSelector), indexCount_1 = 0;
                if (!querySelectorAll(indexParent_1, element.tagName).some(function (el) {
                    if (el.parentElement.closest(indexSelector) === indexParent_1) {
                        indexCount_1++;
                        if (el === element) {
                            _this._index = _this._i = indexCount_1;
                            return true;
                        }
                    }
                })) {
                    console.error("Unable to find own element when indexing", element);
                    _this._index = _this._i = 1;
                }
            }
            return _this;
        }
        Object.defineProperty(WidgetNode.prototype, "rootWidget", {
            get: function () {
                var widget = this._rootWidget;
                if (!widget) {
                    var element = this.element.closest(ic.ScreensWidget.selector);
                    if (element) {
                        this._rootWidget = widget = element.icWidget;
                    }
                }
                return widget;
            },
            enumerable: true,
            configurable: true
        });
        ;
        Object.defineProperty(WidgetNode.prototype, "screenWidget", {
            get: function () {
                var widget = this._screenWidget;
                if (!widget) {
                    var element = this.element.closest(ic.ScreenWidget.selector);
                    if (element) {
                        this._screenWidget = widget = element.icWidget;
                    }
                }
                return widget;
            },
            enumerable: true,
            configurable: true
        });
        ;
        Object.defineProperty(WidgetNode.prototype, "activitiesWidget", {
            get: function () {
                var widget = this._activitiesWidget;
                if (widget === undefined) {
                    var element = this.element.closest(ic.ActivitiesWidget.selector);
                    this._activitiesWidget = widget = element ? element.icWidget : null;
                }
                return widget;
            },
            enumerable: true,
            configurable: true
        });
        ;
        Object.defineProperty(WidgetNode.prototype, "activityWidget", {
            get: function () {
                var widget = this._activityWidget;
                if (widget === undefined) {
                    var element = this.element.closest(ic.ActivityWidget.selector);
                    this._activityWidget = widget = element ? element.icWidget : null;
                }
                return widget;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(WidgetNode.prototype, "parentWidget", {
            get: function () {
                var widget = this._parentWidget;
                if (widget === undefined) {
                    var element = this.element.parentElement;
                    while (element && (!element.icWidget || !element.icWidget.constructor.isTreeWidget)) {
                        element = element.parentElement;
                    }
                    this._parentWidget = widget = element ? element.icWidget : null;
                }
                return widget;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(WidgetNode.prototype, "closestWidget", {
            get: function () {
                var widget = this._closestWidget;
                if (widget === undefined) {
                    this._closestWidget = widget = this.activityWidget || this.activitiesWidget || this.screenWidget || this.rootWidget;
                }
                return widget;
            },
            enumerable: true,
            configurable: true
        });
        WidgetNode.prototype.invalidate = function () {
            delete this._parentWidget;
            delete this._closestWidget;
            delete this._activityWidget;
            delete this._activitiesWidget;
        };
        Object.defineProperty(WidgetNode.prototype, "index", {
            get: function () {
                return this._index;
            },
            set: function (index) {
                this._index = index;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(WidgetNode.prototype, "realIndex", {
            get: function () {
                return this._i;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(WidgetNode.prototype, "parentWidgets", {
            get: function () {
                var nodes = [], node = this;
                for (; node; node = node.parentWidget) {
                    nodes.push(node);
                }
                return nodes;
            },
            enumerable: true,
            configurable: true
        });
        WidgetNode.prototype.has = function () {
            var args = [];
            for (var _a = 0; _a < arguments.length; _a++) {
                args[_a] = arguments[_a];
            }
            isHas = true;
            return this.getOrHas.apply(this, arguments);
        };
        WidgetNode.prototype.get = function () {
            var args = [];
            for (var _a = 0; _a < arguments.length; _a++) {
                args[_a] = arguments[_a];
            }
            isHas = false;
            return this.getOrHas.apply(this, arguments);
        };
        WidgetNode.prototype.getOrHas = function () {
            var _this = this;
            var args = [];
            for (var _a = 0; _a < arguments.length; _a++) {
                args[_a] = arguments[_a];
            }
            var i = 0, arg, subClass = [], group, state = [], andSelf = true, element, widgets, pushElement, filterNode = function (node) {
                return ((group === undefined || node.group === group)
                    && (!state.length || node.hasState(state))
                    && (andSelf || node !== _this));
            }, returnWidgets = function () {
                if (isHas) {
                    return !!widgets.find(filterNode);
                }
                return widgets.filter(filterNode);
            };
            for (; i < args.length; i++) {
                arg = args[i];
                switch (typeof arg) {
                    case "object":
                        if (arg) {
                            if (arg instanceof Element) {
                                element = arg;
                            }
                            else if (arg instanceof WidgetNode) {
                                element = arg.element;
                                if (arg === this) {
                                    andSelf = false;
                                }
                            }
                        }
                        break;
                    case "function":
                        subClass.push(arg);
                        break;
                    case "number":
                        group = arg;
                        break;
                    case "string":
                        state.push(arg);
                        break;
                    case "boolean":
                        andSelf = arg;
                        break;
                }
            }
            if (!element) {
                element = (this.rootWidget || this).element;
            }
            var subClassLength = subClass.length, index;
            if (subClassLength === 1) {
                index = subClass[0];
            }
            else if (subClassLength > 1) {
                var cacheItems = cacheIndex[subClassLength];
                index = subClass;
                if (!cacheItems) {
                    cacheIndex[subClassLength] = [index];
                }
                else if (!cacheItems.some(function (cacheItem) {
                    if (cacheItem.every(function (item) {
                        return subClass.includes(item);
                    })) {
                        index = cacheItem;
                        return true;
                    }
                })) {
                    cacheItems.push(index);
                }
            }
            if (subClassLength >= 1 && ic.Widget.created) {
                var map = cache.get(element);
                if (!map) {
                    cache.set(element, map = new Map());
                }
                else {
                    widgets = map.get(index);
                    if (widgets) {
                        return returnWidgets();
                    }
                }
                map.set(index, widgets = []);
            }
            else {
                widgets = [];
            }
            var filter = subClassLength && subClass.find(function (sc) { return sc.prototype.isInputWidget; })
                ? filterInputWidget
                : filterWidget, walker = document.createTreeWalker(element, NodeFilter.SHOW_ELEMENT, filter, false), node;
            while (element) {
                node = element.icWidget;
                if (!index || subClass.find(function (sc) { return node instanceof sc; })) {
                    widgets.push(node);
                }
                if (node instanceof ic.ScreenWidget && node.detached) {
                    walker.currentNode = node.dom;
                    pushElement = element;
                }
                element = walker.nextNode();
                if (!element && pushElement) {
                    walker.currentNode = pushElement;
                    pushElement = null;
                    element = walker.nextNode();
                }
            }
            return returnWidgets();
        };
        return WidgetNode;
    }(ic.WidgetEvent));
    ic.WidgetNode = WidgetNode;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    ;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    function startup(root) {
        var node, filter = function (node) { return node.nodeType === Node.TEXT_NODE ? NodeFilter.FILTER_ACCEPT : NodeFilter.FILTER_SKIP; }, body = document.body, style = getComputedStyle(body), fixScalingCallback = function () {
            ic.setImmediate("fixScaling", ic.fixScaling);
        };
        addEventListener("options-test", null, Object.defineProperty({}, "capture", {
            get: function () {
                var patchTouchEventListener = function (prototype) {
                    if (prototype && prototype.addEventListener && !prototype.addEventListener.isTouched) {
                        var oldHandler_1 = prototype.addEventListener, touchListener = function (eventType, listener, options, wantsUntrusted) {
                            switch (eventType) {
                                case "touchstart":
                                case "touchmove":
                                case "touchend":
                                    if (options === true || options === false) {
                                        options = {
                                            capture: options
                                        };
                                    }
                                    else if (!options) {
                                        options = {};
                                    }
                                    if (!options.passive) {
                                        options.passive = false;
                                    }
                                    break;
                            }
                            return oldHandler_1.call(this, eventType, listener, options, wantsUntrusted);
                        };
                        touchListener.isTouched = true;
                        Object.defineProperty(prototype, "addEventListener", {
                            value: touchListener,
                            configurable: true
                        });
                    }
                };
                patchTouchEventListener(document);
                patchTouchEventListener(window);
                patchTouchEventListener(Element && Element.prototype);
            }
        }));
        if (!ic.inPreview) {
            body.addEventListener("contextmenu", function (event) {
                event.preventDefault();
            }, true);
        }
        if (parseFloat(style.minWidth) && parseFloat(style.minHeight)) {
            addEventListener("orientationchange", fixScalingCallback);
            addEventListener("resize", fixScalingCallback);
            fixScalingCallback();
            document.addEventListener("touchmove", function (event) {
                if (event.scale && document.documentElement.clientWidth / window.innerWidth < 1) {
                    event.preventDefault();
                }
            }, {
                passive: false,
                useCapture: true
            });
        }
        filter.acceptNode = filter;
        var walker = document.createTreeWalker(root, NodeFilter.SHOW_TEXT, filter, false);
        var _loop_1 = function () {
            var changed = false, text = node.nodeValue, startPos = text.indexOf("[[");
            if (startPos >= 0 && startPos < text.indexOf("]]")) {
                text = text.replace(/\[\[([a-z]+)\]\]/g, function ($0, $1) {
                    switch ($1) {
                        case "title":
                            changed = true;
                            $1 = "JSON Title";
                            break;
                    }
                    return $1;
                });
                if (changed) {
                    node.nodeValue = text;
                }
            }
        };
        while (node = walker.nextNode()) {
            _loop_1();
        }
        ic.Widget.startup(root);
        ic.setImmediate(function () {
            var event = document.createEvent("Event");
            event.initEvent("ic-startup", false, false);
            body.classList.add("ict-started");
            body.dispatchEvent(event);
        });
    }
    ic.startup = startup;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    ic.version["widget"] = 1;
    ic.inPreview = /^about:/.test(location);
    ic.inEditor = ic.inPreview && window.ic_edit;
    function frameHandler() {
        Widget.trigger(".frame");
        ic.rAF(frameHandler);
    }
    function cssLoadHandler(event) {
        Widget.trigger(".css", event);
    }
    ic.PersistState = ["attempted", "disabled", "marked", "reveal", "visited"];
    ic.TemporaryState = ["active", "answered", "attempted", "click", "dragging", "focus", "hover", "playing", "visited"];
    function persistMap(name) {
        switch (name) {
            case "activity": return "a";
            case "attempted": return "b";
            case "attempts": return "c";
            case "disabled": return "d";
            case "drop": return "e";
            case "marked": return "f";
            case "reveal": return "g";
            case "screen": return "h";
            case "select": return "m";
            case "text": return "i";
            case "timer": return "j";
            case "toggle": return "k";
            case "visited": return "l";
        }
        console.warn("Unknown map type:", name);
        return name;
    }
    ;
    var Widget = (function (_super) {
        __extends(Widget, _super);
        function Widget(element) {
            var _this = _super.call(this, element) || this;
            _this.group = 0;
            _this.data = {};
            _this.state = {};
            _this.events = {};
            Widget.widgets.pushOnce(_this);
            var data = element.getAttribute("data-json");
            if (data) {
                try {
                    if (data[0] !== "{") {
                        data = data.decompress("uri");
                        element.removeAttribute("data-json");
                    }
                    _this.data = JSON.parse(data);
                }
                catch (e) {
                    console.warn("Error: Bad Json", element, element.getAttribute("data-json"));
                }
            }
            else if (_this.realIndex) {
                for (var path = element.tagName.substring(3).toLowerCase() + "." + _this.realIndex, parentWidget = _this.parentWidget; parentWidget; parentWidget = parentWidget.parentWidget) {
                    if (parentWidget.data) {
                        data = parentWidget.data.getTree(path);
                        if (data) {
                            _this.data = data;
                            parentWidget.data.setTree(path, undefined);
                        }
                        break;
                    }
                    path = parentWidget.element.tagName.substring(3).toLowerCase() + "." + parentWidget.realIndex + "." + path;
                }
            }
            for (var parent_1 = element; parent_1; parent_1 = parent_1.parentElement) {
                if (parent_1.hasAttribute("ic-group")) {
                    _this.group = parseInt(parent_1.getAttribute("ic-group"));
                    break;
                }
            }
            if (element.hasAttribute("ic-random")) {
                _this.isRandom = true;
            }
            if (element.hasAttribute("ic-rounding")) {
                Widget.roundSize.push(_this);
            }
            if (element.hasAttribute("ic-state")) {
                var states = element.getAttribute("ic-state").split(/[\s\+,]/);
                _this.lockedState = {};
                states.forEach(function (state) {
                    _this.addState(state);
                    _this.lockedState[state] = true;
                });
            }
            _this.on(["mouseenter", "mouseleave"]);
            return _this;
        }
        Object.defineProperty(Widget.prototype, "selector", {
            get: function () {
                return this.constructor.selector;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(Widget.prototype, "index", {
            get: function () {
                return this._index;
            },
            set: function (index) {
                if (this._index !== index) {
                    this._index = index;
                    this.fixState();
                }
            },
            enumerable: true,
            configurable: true
        });
        Widget.prototype.isDisabled = function (event) {
            return this.hasState("disabled") || (event && (event.which || 0) > 1);
        };
        Widget.prototype.onMouseEnter = function (event) {
            if (!this.hasState("disabled")) {
                this.toggleState("hover", true);
            }
        };
        Widget.prototype.onMouseLeave = function (event) {
            this.toggleState("hover", false);
        };
        Widget.prototype.activate = function () {
            if (this.screenWidget) {
                var parents = this.parentWidgets;
                this.get(this.screenWidget, "active", false).not(parents).forEach(function (widget) {
                    if (!(widget instanceof ic.AnchorWidget)) {
                        widget.removeState("active");
                    }
                });
                parents.forEach(function (widget) {
                    widget.addState("active");
                });
            }
            return this;
        };
        Widget.prototype.getBoundingClientRect = function () {
            return getBoundingClientRect(this.element);
        };
        Widget.prototype.inRect = function (left, top) {
            var rect = getBoundingClientRect(this);
            return rect.top <= top && rect.bottom >= top && rect.left <= left && rect.right >= left;
        };
        Widget.prototype.hasState = function (state) {
            var _this = this;
            if (isArray(state)) {
                return state.some(function (name) {
                    return !!_this.state[name];
                });
            }
            return !!this.state[state];
        };
        Widget.prototype.addState = function (state) {
            return this.toggleState(state, true);
        };
        Widget.prototype.removeState = function (state) {
            return this.toggleState(state, false);
        };
        Widget.prototype.toggleState = function (state, add) {
            var name, newState, stateList = state, stateObj = state, changeState = [], currentState = this.state, lockedState = this.lockedState;
            if (isString(state)) {
                stateList = state.split(/ +/);
            }
            if (isArray(stateList)) {
                stateObj = {};
                stateList.forEach(function (name) {
                    stateObj[name] = add;
                });
            }
            for (name in stateObj) {
                add = stateObj[name];
                newState = add === true || add === false ? add : !currentState[name];
                if (currentState[name] !== newState && (!lockedState || name !== "disabled" || !lockedState.hasOwnProperty(name))) {
                    if (name === "focus" && newState) {
                        this.get(ic.InputWidget, name, false).forEach(function (widget) {
                            widget.removeState(name);
                        });
                    }
                    currentState[name] = newState;
                    changeState.push(name);
                }
            }
            if (changeState.length) {
                this.fixState();
                Widget.trigger(".state", this, changeState);
            }
            return this;
        };
        Widget.prototype.fixState = function () {
            if (Widget.started) {
                this.element.fixState(this.state, this.index);
            }
            return this;
        };
        Widget.prototype.getAttribute = function (name) {
            var _this = this;
            if (isString(name)) {
                return this.element.getAttribute(name);
            }
            var result = [];
            name.forEach(function (name) {
                result.push(_this.getAttribute(name));
            });
            return result;
        };
        Widget.prototype.hasAttribute = function (name) {
            if (isString(name)) {
                return this.element.hasAttribute(name);
            }
            return name.some(this.hasAttribute, this);
        };
        Widget.prototype.removeAttribute = function (name) {
            if (isString(name)) {
                var element = this.element, change = element.hasAttribute(name);
                if (change) {
                    element.removeAttribute(name);
                }
                return change;
            }
            return !!name.filter(this.removeAttribute, this).length;
        };
        Widget.prototype.setAttribute = function (name, value) {
            var _this = this;
            if (isString(name)) {
                var element = this.element, change = element.getAttribute(name) != value;
                if (change) {
                    element.setAttribute(name, value);
                }
                return change;
            }
            return !!Object.keys(name).filter(function (key) {
                return _this.setAttribute(key, name[key]);
            }, this).length;
        };
        Widget.prototype.on = function (event, el) {
            var _this = this;
            if (isArray(event)) {
                event.forEach(function (event) {
                    _this.on(event, el);
                });
            }
            else if (event) {
                this.events[event] = this.getEventHandler(event);
                if (!this.events[event]) {
                    console.error("Error: Event handler doesn't exist for " + event, this);
                }
                if (event[0] !== "." && event[0] !== "!") {
                    (el || this.element).addEventListener(event, Widget.eventHandler);
                }
                else {
                    Widget.globalEvents.pushOnce(this);
                    if (event[0] === "!") {
                        var realEvent = event.substring(1);
                        if (!Widget.eventList[realEvent]) {
                            document.addEventListener(realEvent, Widget.eventHandler);
                        }
                        Widget.eventList[realEvent] = (Widget.eventList[realEvent] || 0) + 1;
                    }
                }
            }
            return this;
        };
        Widget.prototype.off = function () {
            var _this = this;
            var eventList = [];
            for (var _a = 0; _a < arguments.length; _a++) {
                eventList[_a] = arguments[_a];
            }
            eventList.forEach(function (event) {
                _this.events[event] = null;
                if (event[0] !== "!" && event[0] !== ".") {
                    _this.element.removeEventListener(event, Widget.eventHandler, false);
                }
            });
            return this;
        };
        Widget.prototype.startup = function () { };
        Widget.prototype.trigger = function (eventType) {
            var args = [];
            for (var _a = 1; _a < arguments.length; _a++) {
                args[_a - 1] = arguments[_a];
            }
            if (this.events[eventType]) {
                return this.events[eventType].apply(this, args);
            }
        };
        Widget.trigger = function (eventType) {
            var args = [];
            for (var _a = 1; _a < arguments.length; _a++) {
                args[_a - 1] = arguments[_a];
            }
            Widget.globalEvents.forEach(function (widget) {
                if (widget.events[eventType]) {
                    widget.events[eventType].apply(widget, args);
                }
            });
        };
        Widget.fixRounding = function () {
            var fail = false;
            Widget.roundSize.forEach(function (widget) {
                var element = widget.element, style = element.style;
                style.width = style.height = "";
                var computed = getComputedStyle(element), width = parseInt(computed.width), height = parseInt(computed.height);
                if (width || height) {
                    style.width = width + "px";
                    style.height = height + "px";
                }
                else {
                    fail = true;
                }
            });
            if (fail) {
                ic.rAF(".round", function () {
                    Widget.fixRounding();
                });
            }
        };
        Widget.resizeHandler = function () {
            ic.rAF(".resize", function () {
                Widget.fixRounding();
                Widget.trigger(".resize");
            });
        };
        Widget.eventHandler = function (event) {
            if (Widget.lastEvent !== event) {
                var i = void 0, widget = void 0, el = event.target, eventType = event.type, globalEventType = "!" + eventType, events = Widget.globalEvents, cancel = false;
                Widget.lastEvent = event;
                if (Widget.waitForMouseUp) {
                    var isMouseClickType = ["mousedown", "mouseup"].indexOf(eventType);
                    if (isMouseClickType > 0) {
                        Widget.waitForMouseUp = false;
                    }
                    if (isMouseClickType >= 0) {
                        return;
                    }
                }
                for (; !cancel && el && el !== document.body; el = el.parentNode) {
                    widget = el.icWidget;
                    if (widget && widget.events[eventType]) {
                        cancel = widget.events[eventType].call(widget, event);
                        if (!event.bubbles) {
                            break;
                        }
                    }
                }
                for (i = 0; event.bubbles && !cancel && i < events.length; i++) {
                    widget = events[i];
                    if (widget && widget.events[globalEventType]) {
                        cancel = widget.events[globalEventType].call(widget, event);
                    }
                }
                if (eventType === "touchstart") {
                    Widget.waitForMouseUp = true;
                }
                if (cancel) {
                    event.preventDefault();
                    return false;
                }
            }
        };
        Widget.prototype.persistTree = function (persistTypes, state) {
            var _this = this;
            if (isUndefined(state)) {
                var typeName_1, firstIndex_1 = 0, lastIndex_1 = 0, firstValue_1 = null, addOutput_1 = function () {
                    if (firstIndex_1) {
                        var map = persistMap(typeName_1);
                        state[map] = state[map] || {};
                        state[map][String(firstIndex_1) + (lastIndex_1 ? "-" + String(lastIndex_1) : "")] = firstValue_1;
                    }
                };
                state = {};
                if (this._persist) {
                    this._persist.forEach(function (key) {
                        if (!isUndefined(_this[key])) {
                            state["_" + persistMap(key)] = _this[key];
                        }
                    });
                }
                ic.PersistState.forEach(function (key) {
                    if (_this.hasState(key)) {
                        state["@" + persistMap(key)] = true;
                    }
                });
                for (typeName_1 in persistTypes) {
                    firstIndex_1 = lastIndex_1 = 0;
                    firstValue_1 = null;
                    this.get(this, persistTypes[typeName_1]).forEach(function (widget) {
                        var index = widget._i;
                        if (index) {
                            var value = widget.trigger(".persist");
                            if (firstValue_1 === value || (isArray(firstValue_1) && !firstValue_1.length && isArray(value) && !value.length)) {
                                lastIndex_1 = index;
                            }
                            else {
                                addOutput_1();
                                firstIndex_1 = index;
                                lastIndex_1 = 0;
                                firstValue_1 = value;
                            }
                        }
                    });
                    addOutput_1();
                }
                return state;
            }
            var _loop_2 = function (typeName) {
                var typeData = state[persistMap(typeName)];
                if (typeData) {
                    this_1.get(this_1, persistTypes[typeName]).forEach(function (widget) {
                        var index = widget._i;
                        if (index) {
                            var state_1 = typeData[index];
                            if (isUndefined(state_1)) {
                                for (var key in typeData) {
                                    var range = key.split("-");
                                    if (range.length === 2 && parseInt(range[0], 10) <= index && parseInt(range[1], 10) >= index) {
                                        state_1 = typeData[key];
                                        break;
                                    }
                                }
                            }
                            if (isUndefined(state_1)) {
                                console.error("Error: Unable to load state for", widget);
                            }
                            else {
                                widget.trigger(".persist", state_1);
                            }
                        }
                    });
                }
                if (this_1._persist) {
                    this_1._persist.forEach(function (key) {
                        _this[key] = state["_" + persistMap(key)];
                    });
                }
                var updateState = {};
                ic.PersistState.forEach(function (key) {
                    updateState[key] = state["@" + persistMap(key)] ? true : false;
                });
                this_1.toggleState(updateState);
            };
            var this_1 = this;
            for (var typeName in persistTypes) {
                _loop_2(typeName);
            }
        };
        Widget.startup = function (root) {
            if (!root) {
                console.error("Trying to run Widget.startup() without a valid root Node");
                return;
            }
            var classes = {}, styleScript = /^(style|script)$/i;
            Array.from(querySelectorAll("ic-screen[x]")).forEach(function (root) {
                var walker = document.createNodeIterator(root, NodeFilter.SHOW_TEXT, null, false), node = walker.nextNode();
                while (node) {
                    if (!styleScript.test(node.parentNode.nodeName)) {
                        node.textContent = node.textContent.decompress("uri");
                    }
                    node = walker.nextNode();
                }
            });
            this.widgets = [];
            this.globalEvents = [];
            this.eventList = {};
            this.forEachClass(function (widgetClass) {
                var selector = widgetClass.selector;
                if (selector) {
                    selector.split(",").forEach(function (selector) {
                        if (classes[selector]) {
                            console.error("Duplicate widget selector:", widgetClass.selector, classes[selector].selector);
                        }
                        else {
                            classes[selector] = widgetClass;
                        }
                    });
                }
            });
            var onLoadCode = [], selectorList = Object.keys(classes).sort().reverse(), findWidget = function (el) {
                return classes[selectorList.find(function (selector) {
                    return el.matches(selector);
                })];
            };
            querySelectorAll("img[data-src][width][height]:not([src])").forEach(function (img) {
                img.src = "data:image/svg+xml;base64," + btoa("<svg xmlns='http://www.w3.org/2000/svg' width='" + img.getAttribute("width") + "' height='" + img.getAttribute("height") + "'/>");
            });
            querySelectorAll(Object.keys(classes).join(",")).forEach(function (el) {
                var widget = new (findWidget(el))(el), onLoad = el.getAttribute("icOnLoad");
                if (onLoad) {
                    onLoadCode.push([widget, onLoad]);
                }
            });
            this.created = true;
            if (!ic.inEditor) {
                this.forEach(function (widget) {
                    if (widget.isRandom) {
                        var widgets_1 = widget.get(widget.closestWidget, widget.constructor, widget.group).filter(function (widget) { return widget.isRandom; }), original_1 = [], parent_2 = [], next_1 = [], isSameOrder_1 = widgets_1.length;
                        widgets_1.forEach(function (widget, index) {
                            var el = widget.element;
                            original_1[index] = widget;
                            parent_2[index] = el.parentNode;
                            while (el && el.icWidget && widgets_1.includes(el.icWidget)) {
                                el = el.nextSibling;
                            }
                            next_1[index] = el;
                            widget.isRandom = false;
                        });
                        while (isSameOrder_1 > 1) {
                            isSameOrder_1 = 0;
                            widgets_1.shuffle().forEach(function (widget, index) {
                                if (original_1[index] === widget) {
                                    isSameOrder_1++;
                                }
                            });
                        }
                        widgets_1.forEach(function (widget, index) {
                            parent_2[index].insertBefore(widget.element, next_1[index]);
                        });
                    }
                });
            }
            this.forEach(function (widget) {
                widget.startup();
            });
            this.started = true;
            this.forEach(function (widget) {
                widget.element.fixState(widget.state, widget.index);
                if (widget instanceof ic.ActivityWidget) {
                    widget.mark();
                }
            });
            onLoadCode.forEach(function (onLoad) {
                onLoad[0].callUserFunc(onLoad[1]);
            });
            querySelectorAll("link[rel='stylesheet']").forEach(function (el) {
                el.addEventListener("load", cssLoadHandler, false);
            });
            Widget.fixRounding();
            ic.rAF(frameHandler);
            setInterval(function () {
                Widget.trigger(".tick");
            }, 1000);
            Widget.root.get(ic.ScreenWidget).not(Widget.root.screenWidget).forEach(function (screen) { return screen.detach(); });
            var style = document.documentElement.style;
            if (style.visibility === "hidden") {
                style.visibility = "";
            }
        };
        Widget.prototype.callUserFunc = function (source) {
            var args = [];
            for (var _a = 1; _a < arguments.length; _a++) {
                args[_a - 1] = arguments[_a];
            }
            if (isString(source)) {
                var fn = Widget.callFunctions[source];
                if (!fn) {
                    fn = Widget.callFunctions[source] = new Function(source);
                }
                fn.apply(this.element, args);
            }
        };
        Widget.forEach = function (callback, thisArg) {
            this.widgets.forEach(callback, thisArg);
        };
        Widget.some = function (callback, thisArg) {
            return this.widgets.every(callback, thisArg);
        };
        Widget.every = function (callback, thisArg) {
            return this.widgets.some(callback, thisArg);
        };
        Widget.forEachClass = function (callback) {
            Object.keys(ic).forEach(function (className, index) {
                var widgetClass = ic[className];
                if (widgetClass && widgetClass.prototype instanceof Widget) {
                    callback(widgetClass, index);
                }
            });
        };
        Widget.widgets = [];
        Widget.globalEvents = [];
        Widget.eventList = {};
        Widget.roundSize = [];
        Widget.callFunctions = {};
        __decorate([
            final
        ], Widget, "root", void 0);
        return Widget;
    }(ic.WidgetNode));
    ic.Widget = Widget;
    addEventListener("resize", Widget.resizeHandler);
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    ic.version["scoring"] = 1;
    function callInternalMark(widget) {
        if (widget.needMarking) {
            widget.needMarking = false;
            widget.internalMark();
        }
    }
    var scoreChanged = {}, isScoring;
    function scoreEvent() {
        var event = document.createEvent("Event");
        event.initEvent("ic-score", false, false);
        event.icData = scoreChanged;
        scoreChanged = {};
        document.body.dispatchEvent(event);
    }
    var InputWidget = (function (_super) {
        __extends(InputWidget, _super);
        function InputWidget(element) {
            var _this = _super.call(this, element) || this;
            _this.markable = {
                score: 0,
                min: 0,
                max: 0
            };
            var mark = _this.markable;
            (_this.data
                || ((_this.parentWidget ? _this.parentWidget.data : {}) || {}).getTree([_this.selector, String(_this.realIndex)], {}))
                .clone(mark, true);
            if (isString(mark.points)) {
                var points = parseFloat(mark.points);
                if (!isNaN(points) && points == mark.points) {
                    mark.points = points;
                }
            }
            return _this;
        }
        InputWidget.prototype.startup = function () {
            var _this = this;
            this.startState = {};
            Object.keys(this.state).forEach(function (state) {
                if (!ic.TemporaryState.includes(state)) {
                    _this.startState[state] = _this.state[state];
                }
            });
            this.startState["disabled"] = this.state["disabled"] || false;
            this.startState["reveal"] = this.state["reveal"] || false;
            this.on([".submit", ".reset"]);
        };
        Object.defineProperty(InputWidget.prototype, "hasAnswered", {
            get: function () {
                return true;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(InputWidget.prototype, "isScrollable", {
            get: function () {
                for (var element = this.element; element && element !== document.documentElement; element = element.parentElement) {
                    var vertical = element.scrollTop || element.scrollHeight > element.clientHeight, horizontal = element.scrollLeft || element.scrollWidth > element.clientWidth;
                    if (vertical || horizontal) {
                        var style = getComputedStyle(element);
                        if ((vertical && style.overflowY !== "hidden") || (horizontal && style.overflowX !== "hidden")) {
                            return true;
                        }
                    }
                }
                return false;
            },
            enumerable: true,
            configurable: true
        });
        InputWidget.prototype.onReset = function (screen) {
            if ((!screen || this.screenWidget === screen) && this.startState) {
                this.toggleState(this.startState);
            }
        };
        InputWidget.prototype.onSubmit = function (screen) {
            if ((!screen || this.screenWidget === screen) && this.startState) {
                this.addState("disabled");
            }
        };
        InputWidget.eval = function (maths, fail) {
            var num = parseFloat(maths);
            if (!isNaN(num) && String(num) === maths) {
                return num;
            }
            else if (!/[a-z\[\],]/i.test(maths)) {
                var result = void 0;
                try {
                    result = eval(maths);
                    if (result === true) {
                        return 1;
                    }
                    var val = parseFloat(result);
                    return isNaN(val) ? 0 : parseFloat(val.toPrecision(12));
                }
                catch (error) {
                    if (!fail && !(error instanceof ReferenceError)) {
                        console.error("Error: Broken maths check:", maths, result, error);
                    }
                }
            }
            return fail ? undefined : 0;
        };
        Object.defineProperty(InputWidget, "types", {
            get: function () {
                Object.defineProperty(this, "types", {
                    value: {
                        "act": ic.ActivityWidget,
                        "drag": ic.DraggableWidget,
                        "drop": ic.DroppableWidget,
                        "option": ic.OptionWidget,
                        "select": ic.SelectWidget,
                        "text": ic.TextWidget,
                        "toggle": ic.ToggleWidget
                    }
                });
                return this.types;
            },
            enumerable: true,
            configurable: true
        });
        ;
        Object.defineProperty(InputWidget, "typeRx", {
            get: function () {
                Object.defineProperty(this, "typeRx", {
                    value: new RegExp("#(this|attempts|(?:act(s?)(\\d+))?(acts?|" + Object.keys(InputWidget.types).join("|") + ")([+-]?)(\\d+))(%|\\$|@|=|!)?#", "g")
                });
                return this.typeRx;
            },
            enumerable: true,
            configurable: true
        });
        InputWidget.prototype.expandReferences = function (str, cache, max) {
            var _this = this;
            return str.replace(InputWidget.typeRx, function ($0, $match, $activities, $act, $widgettype, $relative, $index, $type) {
                if (cache[$0]) {
                    return cache[$0];
                }
                var widget, noWidget = false, result = isBoolean(max) ? "0" : "";
                try {
                    if ($match === "attempts") {
                        result = String((_this.activityWidget || _this.screenWidget || _this.rootWidget || {}).attempts || 0);
                        noWidget = true;
                    }
                    else if ($match === "this") {
                        widget = _this;
                    }
                    else {
                        var isActType = $widgettype === "act", isActsType = $widgettype === "acts", baseIndex = parseInt($index), index_1 = $relative === "-" ? _this.realIndex - baseIndex : $relative === "+" ? _this.realIndex + baseIndex : baseIndex, act_1 = $act
                            ? parseInt($act, 10)
                            : isActType || isActsType
                                ? index_1
                                : 0, actType_1 = $activities || isActsType ? ic.ActivitiesWidget : ic.ActivityWidget, parent_3 = $act === "0"
                            ? _this.screenWidget
                            : act_1
                                ? _this.get(_this.screenWidget || _this.rootWidget, actType_1).find(function (activity) {
                                    return activity.index === act_1 && activity.constructor === actType_1;
                                })
                                : _this instanceof ic.ActivityWidget
                                    ? _this
                                    : (_this.activityWidget || _this.parentWidget);
                        if (isActType || isActsType) {
                            widget = parent_3;
                        }
                        else {
                            var widgetType_1 = InputWidget.types[$widgettype];
                            widget = _this.get(parent_3, widgetType_1).find(function (widget) {
                                if (widget.constructor === widgetType_1 && widget.realIndex === index_1 && (!$activities || widget.parentWidget === parent_3)) {
                                    return true;
                                }
                            });
                        }
                    }
                    if (widget) {
                        var widgetMark_1 = widget.markable, getValue = function () {
                            return isString(widgetMark_1.value) ? widgetMark_1.value : isArray(widgetMark_1.value) ? widgetMark_1.value.join("|") : "";
                        };
                        if (widgetMark_1) {
                            result = String($type
                                ? ($type === "@"
                                    ? widgetMark_1.max || 0
                                    : $type === "$"
                                        ? widgetMark_1.score || 0
                                        : $type === "%"
                                            ? (widgetMark_1.scaled || 0) * 100
                                            : $type === "="
                                                ? getValue() || 0
                                                : $type === "!"
                                                    ? widgetMark_1.raw || 0
                                                    : getValue())
                                : (max === true
                                    ? widgetMark_1.max || 0
                                    : max === false || isScoring
                                        ? widgetMark_1.score || 0
                                        : getValue()));
                        }
                    }
                    else if (!noWidget) {
                        console.warn("Unable to find widget for", $0);
                    }
                }
                catch (e) {
                    console.error("Error: Unknown widget type:", $0, e);
                }
                return cache[$0] = result;
            });
        };
        InputWidget.prototype.parse = function (expr, max) {
            var mark = this.markable, found = true, cache = {}, expression = this.expandReferences(expr, cache, isBoolean(max) ? max : undefined);
            expression = expression
                .replace(/@(?!#)/g, String(mark.max || 0))
                .replace(/\$(?![\/#])/g, String((max === true ? mark.max : mark.raw) || 0))
                .replace(/%(?!#)/g, String((max === true ? 100 : mark.scaled * 100) || 0));
            cache = {};
            while (found) {
                found = false;
                expression = expression.replace(InputWidget.methodRx, function ($0, $fn, $args) {
                    try {
                        var result = InputWidget.methods[$fn].call(cache, $args.trim().replace(/\{\{/g, "(").replace(/\}\}/g, ")"), max, mark);
                        if (result === undefined) {
                            result = $0;
                        }
                        else {
                            found = true;
                        }
                        return result;
                    }
                    catch (e) {
                        console.error("Error: Scripting error", $0, e);
                        return "";
                    }
                });
            }
            var final = InputWidget.eval(expression, true);
            return final === undefined ? expression : String(final);
        };
        InputWidget.prototype.getPoints = function (max) {
            var points = this.markable.points || 0;
            if (isNumber(points)) {
                return points;
            }
            isScoring = true;
            return parseFloat(this.parse(points, max === true || undefined));
        };
        Object.defineProperty(InputWidget.prototype, "isScored", {
            get: function () {
                return (this.markable.points || 0) >= 0;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(InputWidget.prototype, "expandedAnswers", {
            get: function () {
                var answers = this._expandedAnswers;
                if (!answers) {
                    var mark = this.markable, answer = mark.answer, cache = {};
                    answers = [];
                    if (answer) {
                        (isArray(answer) ? answer : [answer]).forEach(function (answer) {
                            if (answer[0] === "\\" || answer[0] === "@") {
                                answers.push(answer.substr(1));
                                return;
                            }
                            if (answer[0] === "=") {
                                answer = answer.substr(1);
                                if (/^\/.*\/i?$/.test(answer)) {
                                    new RegExp(answer.substr(1, answer.length - 2)).expand().forEach(function (answer) { return answers.push(answer); });
                                    return;
                                }
                            }
                            answers.push(answer);
                        });
                        for (var i = 0; i < answers.length; i++) {
                            answers[i] = this.expandReferences(answers[i], cache);
                            if (answers[i].indexOf(".") >= 0) {
                                answers[i] = new RegExp(answers[i]);
                            }
                        }
                    }
                    this._expandedAnswers = answers;
                }
                return answers;
            },
            enumerable: true,
            configurable: true
        });
        InputWidget.prototype.isCorrect = function (value, answer) {
            var fixedAnswer = answer;
            if (fixedAnswer[0] === "\\") {
                fixedAnswer = fixedAnswer.substr(1);
            }
            else if (fixedAnswer[0] === "@") {
                fixedAnswer = fixedAnswer.substr(1);
                if (/^(-?[0-9]+|-?[0-9]*\.[0-9]+)$/.test(fixedAnswer)) {
                    var first_1 = true, parts = fixedAnswer.split(".");
                    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, function () {
                        if (first_1) {
                            first_1 = false;
                            return "([ ,]?)";
                        }
                        return "(?:\\1)";
                    });
                    if (!parts[0] || parts[0] === "0") {
                        parts[0] = "0?";
                    }
                    else if (parts[0] === "-0") {
                        parts[0] = "-0?";
                    }
                    return new RegExp("^" + parts.join("\.") + "$").test(value);
                }
                else {
                    return value.toLocaleLowerCase() === fixedAnswer.toLocaleLowerCase();
                }
            }
            else if (fixedAnswer[0] === "=") {
                fixedAnswer = fixedAnswer.substr(1);
                var mark = this.markable;
                if (fixedAnswer === "") {
                    return !mark.value.length;
                }
                if (fixedAnswer === "*") {
                    return !!mark.value.length;
                }
                if (/^\/.*\/[ipP]*$/.test(fixedAnswer)) {
                    fixedAnswer = InputWidget.methods["rx"](fixedAnswer, value, mark);
                }
                else {
                    isScoring = false;
                    fixedAnswer = this.parse(fixedAnswer, value);
                }
                if (fixedAnswer === "1") {
                    return true;
                }
                else if (fixedAnswer === "0") {
                    return false;
                }
            }
            return value === fixedAnswer;
        };
        InputWidget.prototype.internalMark = function () {
            var _this = this;
            var isActivity = this instanceof ic.ActivityWidget;
            if (isActivity || this.activityWidget) {
                var mark_1 = this.markable, value_1 = mark_1.value, answer_1 = mark_1.answer, changed = false, cache_1 = {};
                if (!isActivity) {
                    mark_1.raw = mark_1.scaled = 0;
                    if (value_1 && answer_1) {
                        var activity = this.activityWidget, usedAnswers_1 = [], correctAnswers_1 = mark_1.correctAnswers = !activity.allowDuplicates && !(this instanceof ic.ToggleWidget) && [], expandRx_1 = function (answer) {
                            return answer.replace(/rx\(.*?\)/g, function (match) { return _this.expandReferences(match, cache_1); });
                        };
                        if (correctAnswers_1) {
                            this.get(activity, this.constructor, this.group, false).forEach(function (widget) {
                                var answer = widget.markable.correctAnswers;
                                if (answer && answer.length) {
                                    usedAnswers_1.pushOnce.apply(usedAnswers_1, answer);
                                }
                            });
                        }
                        if (isString(value_1)) {
                            if (isString(answer_1)) {
                                var expandedAnswer = expandRx_1(answer_1);
                                if (!usedAnswers_1.includes(expandedAnswer) && this.isCorrect(value_1, expandedAnswer)) {
                                    if (correctAnswers_1) {
                                        correctAnswers_1.pushOnce(expandedAnswer);
                                    }
                                    mark_1.raw = 1;
                                }
                            }
                            else if (isArray(answer_1)) {
                                answer_1[correctAnswers_1 ? "forEach" : "some"](function (answerString) {
                                    var expandedAnswer = expandRx_1(answerString);
                                    if (!usedAnswers_1.includes(expandedAnswer) && _this.isCorrect(value_1, expandedAnswer)) {
                                        if (correctAnswers_1) {
                                            correctAnswers_1.pushOnce(expandedAnswer);
                                        }
                                        mark_1.raw = 1;
                                        return true;
                                    }
                                });
                            }
                        }
                        else if (isArray(value_1)) {
                            if (isString(answer_1)) {
                                var expandedAnswer_1 = expandRx_1(answer_1);
                                value_1.forEach(function (value) {
                                    if (_this.isCorrect(value, expandedAnswer_1)) {
                                        mark_1.raw += 1;
                                    }
                                });
                                if (correctAnswers_1 && mark_1.raw) {
                                    correctAnswers_1.pushOnce(expandedAnswer_1);
                                }
                            }
                            else if (isArray(answer_1)) {
                                if (mark_1.order) {
                                    value_1.forEach(function (value, index) {
                                        var expandedAnswer = expandRx_1(answer_1[index]);
                                        if (_this.isCorrect(value, expandedAnswer)) {
                                            if (correctAnswers_1) {
                                                correctAnswers_1.pushOnce(expandedAnswer);
                                            }
                                            mark_1.raw++;
                                        }
                                    });
                                }
                                else {
                                    var _loop_3 = function (i, found) {
                                        answer_1[correctAnswers_1 ? "forEach" : "some"](function (answerString) {
                                            var expandedAnswer = expandRx_1(answerString);
                                            if (!usedAnswers_1.includes(expandedAnswer) && _this.isCorrect(value_1[i], expandedAnswer)) {
                                                usedAnswers_1.pushOnce(expandedAnswer);
                                                if (correctAnswers_1) {
                                                    correctAnswers_1.pushOnce(expandedAnswer);
                                                }
                                                return found = true;
                                            }
                                        });
                                        if (found) {
                                            mark_1.raw++;
                                            found = false;
                                        }
                                        out_found_1 = found;
                                    };
                                    var out_found_1;
                                    for (var i = 0, found = false; i < value_1.length; i++) {
                                        _loop_3(i, found);
                                        found = out_found_1;
                                    }
                                }
                            }
                        }
                        mark_1.scaled = Math.range(0, (mark_1.raw - (mark_1.min || 0)) / (mark_1.max || 1), 1);
                    }
                    else if (!value_1 && !answer_1) {
                        mark_1.scaled = 1;
                    }
                }
                else {
                    mark_1.scaled = Math.range(0, (mark_1.raw - (mark_1.min || 0)) / (mark_1.max || 1), 1);
                }
                switch (typeof mark_1.points) {
                    case "number":
                        mark_1.maxPoints = mark_1.points;
                        mark_1.score = mark_1.scaled * mark_1.maxPoints;
                        break;
                    case "string":
                        mark_1.maxPoints = this.getPoints(true);
                        mark_1.score = mark_1.scaled || isActivity ? this.getPoints() : 0;
                        if (mark_1.maxPoints) {
                            mark_1.scaled = mark_1.score / mark_1.maxPoints;
                        }
                        break;
                    default:
                        mark_1.maxPoints = mark_1.max;
                        mark_1.score = mark_1.raw;
                        break;
                }
                if (mark_1.round) {
                    mark_1.score = Math.floor(mark_1.score);
                }
                if (isNumber(mark_1.score) && !isNaN(mark_1.score) && mark_1.score >= 0 && mark_1.maxPoints !== -1 && mark_1.maxPoints !== undefined) {
                    changed = this.setAttribute({
                        "data-mark": String(mark_1.score),
                        "data-percent": String(Math.floor(mark_1.scaled * 100)),
                        "data-points": String(mark_1.maxPoints)
                    });
                }
                else {
                    changed = this.removeAttribute(["data-mark", "data-percent", "data-points"]);
                }
                if (changed) {
                    var screenIndex = this.screenWidget.realIndex, activityIndex = (isActivity ? this : this.activityWidget).realIndex;
                    ic.Widget.trigger(".score", this);
                    scoreChanged[screenIndex] = scoreChanged[screenIndex] || {};
                    scoreChanged[screenIndex][activityIndex] = true;
                    ic.setImmediate("score", scoreEvent);
                }
            }
        };
        InputWidget.internalMark = function () {
            var root = ic.Widget.root;
            if (root) {
                root.get(InputWidget).forEach(callInternalMark);
                root.get(ic.ActivityWidget).filter(function (activity) { return !(activity instanceof ic.ActivitiesWidget); }).forEach(callInternalMark);
                root.get(ic.ActivitiesWidget).forEach(callInternalMark);
            }
        };
        InputWidget.prototype.mark = function () {
            if (!this.needMarking && !this.noMarking) {
                this.needMarking = true;
                this.markable.correctAnswers = null;
                this.get(this.screenWidget || this.rootWidget, ic.ActivityWidget, true).forEach(function (activity) {
                    activity.mark();
                });
                ic.setImmediate(InputWidget.internalMark);
            }
        };
        InputWidget.isInputWidget = true;
        InputWidget.methods = {
            "correct": function (args, value, mark) {
                var opts = args.regex(/([0-9]+(?:\.[0-9]*)?)/g);
                if (!opts) {
                    if (value === true) {
                        return String(mark.total);
                    }
                    return String(mark.correct);
                }
                if (value === true) {
                    return String(1);
                }
                if (opts.length === 1) {
                    return String(mark.correct >= opts[0] ? 1 : 0);
                }
                return String(mark.correct.range(opts[0], opts[1]) ? 1 : 0);
            },
            "ceil": function (args) {
                return String(Math.ceil(InputWidget.eval(args)));
            },
            "fixed": function (args, value, mark) {
                var opts = args.split(","), val = opts.length ? InputWidget.eval(opts.pop()) : parseFloat((value === true ? "" : value).replace(/[^0-9\.]/g, ""));
                if (opts.length) {
                    return String(parseFloat(val.toFixed(InputWidget.eval(opts[1]))));
                }
                return String(parseFloat(val.toFixed(12)));
            },
            "find": function (args, value, mark) {
                if (value === true) {
                    return String(1);
                }
                var opts = args.split(",");
                if (opts.includes(opts[0], 1)) {
                    return String(1);
                }
                return String(0);
            },
            "floor": function (args) {
                return String(Math.floor(InputWidget.eval(args)));
            },
            "get": function (args) {
                var name = args.trim();
                return this[name] || "";
            },
            "grouped": function (args, value) {
                var opts = args.regex(/(\[[^\]]+\]|\d+)/g), count = -1, correct = 0, total = 0;
                if (opts) {
                    if (isNumber(opts[0])) {
                        count = opts.shift();
                    }
                    opts.forEach(function (opt) {
                        var values = opt.regex(/(\d+)/g);
                        if (value || (values && !values.some(function (value) {
                            return !value;
                        }))) {
                            total += values.length;
                            correct++;
                        }
                    });
                    if (value && count) {
                        count = -1;
                    }
                }
                return String(count < 0 ? total : !count ? correct : count === correct ? total : -Math.PI);
            },
            "if": function (args, value) {
                var opts = args.split(","), what = InputWidget.eval(opts.shift());
                if (value === true) {
                    return String(Math.max(opts[0] ? parseFloat(opts[0]) : 1, opts[1] ? parseFloat(opts[1]) : 0));
                }
                return String(opts[what ? 0 : 1] || (what ? 1 : 0));
            },
            "map": function (args, value) {
                var opts = args.split(","), what = InputWidget.eval(opts.shift()), best = 0;
                opts.some(function (opt) {
                    var map = opt.regex(/^\s*([0-9]+(?:\.[0-9]*)?)\s*:\s*([0-9]+(?:\.[0-9]*)?)\s*$/);
                    if (value === true) {
                        best = Math.max(best, map[1]);
                    }
                    else if (map[0] <= what) {
                        best = map[1];
                    }
                    else {
                        return true;
                    }
                });
                return String(best);
            },
            "number": function (args) {
                return args.replace(/[^0-9\.]/g, "") || String(0);
            },
            "max": function (args, value, mark) {
                if (value === true) {
                    return String(1);
                }
                var opts = args.regex(/([0-9]+(?:\.[0-9]*)?)/g);
                if (opts.length > 1) {
                    return String(Math.max.apply(Math, opts));
                }
                return String(parseFloat(value.replace(/[^0-9\.]/g, "")) <= opts[0] ? 1 : 0);
            },
            "min": function (args, value, mark) {
                if (value === true) {
                    return String(1);
                }
                var opts = args.regex(/([0-9]+(?:\.[0-9]*)?)/g);
                if (opts.length > 1) {
                    return String(Math.min.apply(Math, opts));
                }
                return String(parseFloat(value.replace(/[^0-9\.]/g, "")) >= opts[0] ? 1 : 0);
            },
            "range": function (args, value, mark) {
                if (value === true) {
                    return String(1);
                }
                var opts = args.regex(/([0-9]+(?:\.[0-9]*)?)/g);
                if (opts.length > 2) {
                    return String(Math.range.apply(Math, opts));
                }
                return String(parseFloat(value.replace(/[^0-9\.]/g, "")).range(opts[0], opts[1]) ? 1 : 0);
            },
            "rx": function (args, value, mark) {
                if (value === true) {
                    return String(1);
                }
                var opts = args.regex(/(.*?)(?:,?\s?\/(.*?)\/([ipP]*))/), match = opts[1], flags = opts[2], flagSmallP = flags.includes("p"), flagLargeP = flags.includes("P"), flagSmallI = flags.includes("i");
                if (flagLargeP || flagSmallP) {
                    match = match.replace(/(^\^?\s*|\s*\$?$)/g, "");
                }
                if (flagSmallP) {
                    match = match.replace(/ +/g, "[ !\"\\#$%&'()*+,-\\.\\/:;<=>?@\\[\\]^_`{|}~]+");
                }
                if (flagLargeP || flagSmallP) {
                    match = "^" + match.replace(/(^\^?\s*|\s*\$?$)/g, "").replace(/(^|$)/g, "[ !\"\\#$%&'()*+,-\\.\\/:;<=>?@\\[\\]^_`{|}~]*") + "$";
                }
                return String(new RegExp(match, flagSmallI ? "i" : "").test(opts[0] || value) ? 1 : 0);
            },
            "set": function (args) {
                var name = "", value = args.replace(/[\n\r\t]/g, "").replace(/\s*(.*?)\s*,\s*/, function ($0, $1) {
                    name = $1;
                    return "";
                });
                this[name] = value;
                return "";
            },
            "total": function (args, value, mark) {
                return String(mark.total);
            },
            "unique": function (args, value, mark) {
                if (value === true) {
                    return String(1);
                }
                var opts = args.split(",");
                for (var i = 0; i < opts.length; i++) {
                    if (opts.includes(opts[i], i + 1)) {
                        return String(0);
                    }
                }
                return String(1);
            },
            "": function (args) {
                var result = InputWidget.eval(args, true);
                if (result !== undefined) {
                    return String(result);
                }
            }
        };
        InputWidget.methodRx = new RegExp("(" + Object.keys(InputWidget.methods).join("|") + ")\\(([^\\(\\)]*)\\)");
        return InputWidget;
    }(ic.Widget));
    ic.InputWidget = InputWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var selector = "ic-activity";
    ic.version[selector] = 1;
    var ActivityWidget = (function (_super) {
        __extends(ActivityWidget, _super);
        function ActivityWidget(element) {
            var _this = _super.call(this, element) || this;
            _this.markable = {
                score: 0,
                min: 0
            };
            _this.attempts = 0;
            _this.expandReferences = ic.InputWidget.prototype.expandReferences;
            _this.parse = ic.InputWidget.prototype.parse;
            _this.getPoints = ic.InputWidget.prototype.getPoints;
            var data = _this.data
                || ((_this.parentWidget ? _this.parentWidget.data : {}) || {}).getTree([_this.selector, String(_this.index)], {});
            if (data) {
                data.clone(_this.markable, true);
                if (data.duplicates) {
                    _this.allowDuplicates = true;
                }
            }
            _this.on([".persist", ".screen", ".state", ".timeout", ".reset"]);
            return _this;
        }
        Object.defineProperty(ActivityWidget.prototype, "isScored", {
            get: function () {
                return (this.markable.points || 0) >= 0;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ActivityWidget.prototype, "hasAnswered", {
            get: function () {
                return this.hasState("attempted");
            },
            enumerable: true,
            configurable: true
        });
        ActivityWidget.prototype.onPersist = function (state) {
            if (!isUndefined(state)) {
                this.get(this, ic.DroppableWidget).forEach(function (drop) {
                    drop.onReset();
                });
            }
            var result = this.persistTree({
                "drop": ic.DroppableWidget,
                "select": ic.SelectWidget,
                "text": ic.TextWidget,
                "toggle": ic.ToggleWidget
            }, state);
            if (!isUndefined(state)) {
                this.mark();
            }
            return result;
        };
        ActivityWidget.prototype.onReset = function (screen) {
            if ((!screen || this.screenWidget === screen)) {
                this.removeState("attempted");
            }
        };
        ActivityWidget.prototype.onScreen = function (screen) {
            if (this.screenWidget === screen && !this.get(screen, ActivityWidget, "active", false).length) {
                this.addState("active");
            }
        };
        ActivityWidget.prototype.onState = function (widget, stateList) {
            if (this === widget && this.parentWidget && this.parentWidget !== this.rootWidget) {
                var newStates = {}, found = void 0;
                if (stateList.includes("attempted") && this.hasState("attempted")) {
                    newStates["attempted"] = found = true;
                }
                if (stateList.includes("active") && this.hasState("active")) {
                    newStates["active"] = found = true;
                }
                if (stateList.includes("answered")) {
                    newStates["answered"] = this.hasState("answered");
                    found = true;
                }
                if (found) {
                    this.parentWidget.toggleState(newStates);
                }
            }
        };
        ActivityWidget.prototype.onTimeout = function () {
            this.get(this, ic.InputWidget).forEach(function (widget) {
                widget.addState("disabled");
            });
        };
        ActivityWidget.prototype.internalMark = function () {
            var _this = this;
            var mark = this.markable;
            if (mark.points === -1) {
                var element = this.element;
                element.removeAttribute("data-mark");
                element.removeAttribute("data-percent");
                element.removeAttribute("data-points");
            }
            else {
                mark.raw = mark.max = mark.correct = mark.total = 0;
                this.get(this, this instanceof ic.ActivitiesWidget ? ActivityWidget : ic.InputWidget).forEach(function (widget) {
                    if (widget.activitiesWidget === _this || widget.activityWidget === _this && !widget.noMarking) {
                        var points = widget.getPoints();
                        if (points >= 0) {
                            mark.raw += Math.max(0, widget.markable.score || 0);
                            mark.max += Math.max(0, points);
                            mark.total++;
                            if (widget.markable.scaled > 0) {
                                mark.correct++;
                            }
                        }
                    }
                });
                ic.InputWidget.prototype.internalMark.call(this);
            }
            this.toggleState("answered", this.get(this, ic.InputWidget).some(function (widget) { return widget.isScored && widget.hasAnswered; }));
        };
        ActivityWidget.prototype.mark = function () {
            var _this = this;
            if (!this.needMarking) {
                this.needMarking = true;
                if (this.markable.points !== -1) {
                    this.get(this, this instanceof ic.ActivitiesWidget ? ActivityWidget : ic.InputWidget).forEach(function (widget) {
                        if (widget.activitiesWidget === _this || widget.activityWidget === _this) {
                            widget.mark();
                        }
                    });
                }
                ic.setImmediate(ic.InputWidget.internalMark);
            }
        };
        ActivityWidget.selector = selector;
        ActivityWidget.isInputWidget = true;
        ActivityWidget.isTreeWidget = true;
        __decorate([
            persist
        ], ActivityWidget.prototype, "attempts", void 0);
        return ActivityWidget;
    }(ic.Widget));
    ic.ActivityWidget = ActivityWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var selector = "ic-activities";
    ic.version[selector] = 1;
    var ActivitiesWidget = (function (_super) {
        __extends(ActivitiesWidget, _super);
        function ActivitiesWidget() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        ActivitiesWidget.isInputWidget = true;
        ActivitiesWidget.selector = selector;
        return ActivitiesWidget;
    }(ic.ActivityWidget));
    ic.ActivitiesWidget = ActivitiesWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var selector = "ic-anchor";
    ic.version[selector] = 1;
    var AnchorWidget = (function (_super) {
        __extends(AnchorWidget, _super);
        function AnchorWidget(element) {
            var _this = _super.call(this, element) || this;
            if (_this.screenWidget && _this.get(_this.screenWidget, AnchorWidget).length === 1) {
                _this.relativeWidget = _this.screenWidget;
            }
            else if (_this.activitiesWidget && _this.get(_this.activitiesWidget, AnchorWidget).length === 1) {
                _this.relativeWidget = _this.activitiesWidget;
            }
            else if (_this.activityWidget && _this.get(_this.activityWidget, AnchorWidget).length === 1) {
                _this.relativeWidget = _this.activityWidget;
            }
            if (_this.relativeWidget) {
                _this.index = _this.get(AnchorWidget, true).indexOf(_this) + 1;
                element.setAttribute("data-anchor", element.getAttribute(AnchorWidget.selector) || String(_this.index));
                _this.on(".state");
            }
            else {
                _this.index = 0;
                element.removeAttribute("data-anchor");
            }
            return _this;
        }
        AnchorWidget.prototype.onState = function (widget, stateList) {
            if (widget === this) {
                if (stateList.includes("active") && this.hasState("active")) {
                    this.relativeWidget.addState("active");
                }
            }
            else if (widget === this.relativeWidget) {
                this.toggleState({
                    "active": this.relativeWidget.hasState("active"),
                    "answered": this.relativeWidget.hasState("answered"),
                    "attempted": this.relativeWidget.hasState("attempted")
                });
            }
        };
        AnchorWidget.selector = selector;
        return AnchorWidget;
    }(ic.Widget));
    ic.AnchorWidget = AnchorWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var selector = "ic-box";
    ic.version[selector] = 1;
    var BoxWidget = (function (_super) {
        __extends(BoxWidget, _super);
        function BoxWidget(element) {
            var _this = _super.call(this, element) || this;
            _this.storySpeed = 1000;
            _this.disappear = function () {
                var el = _this.element.firstElementChild;
                while (el) {
                    el.classList.add("hidden");
                    el = el.nextElementSibling;
                }
            };
            _this.appear = function () {
                var el = _this.element.firstElementChild, appear = function () {
                    if (el) {
                        el.classList.remove("hidden");
                        el = el.nextElementSibling;
                        setTimeout(appear, _this.storySpeed);
                    }
                };
                appear();
            };
            if (element.hasAttribute("ic-story")) {
                _this.isStory = true;
                _this.isStoryStart = element.getAttribute("ic-story") === "start";
                if (!_this.isStoryStart) {
                    _this.addState("disabled");
                }
                (getComputedStyle(element.firstElementChild).transition).replace(/[^\d]+(\d+)(s|ms)(?:[^\d]+(\d+)(s|ms))?/g, function ($0, duration, durationUnit, delay, delayUnit) {
                    if (durationUnit === "s") {
                        duration *= 1000;
                    }
                    if (delayUnit === "s") {
                        delay *= 1000;
                    }
                    _this.storySpeed = Math.max(_this.storySpeed, duration, delay);
                    return $0;
                });
                _this.on([".state"]);
            }
            return _this;
        }
        BoxWidget.prototype.startup = function () {
            var _this = this;
            _super.prototype.startup.call(this);
            if (this.isStory) {
                var foundThis_1 = false, screenElement_1 = this.screenWidget.element, thisElement_1 = this.element;
                this.nextStory = this.get(this.screenWidget, BoxWidget).find(function (widget) {
                    if (widget === _this) {
                        foundThis_1 = true;
                    }
                    else if (foundThis_1) {
                        return widget.isStory && !widget.element.parentElements(screenElement_1).includes(thisElement_1);
                    }
                });
                this.disappear();
                if (this.isStoryStart) {
                    this.appear();
                }
            }
        };
        BoxWidget.prototype.onState = function (widget, stateList) {
            var _this = this;
            if (widget === this && stateList.includes("disabled")) {
                if (this.hasState("disabled")) {
                    this.disappear();
                    if (this.nextStory) {
                        setTimeout(function () { _this.nextStory.removeState("disabled"); }, this.storySpeed);
                    }
                }
                else {
                    this.appear();
                }
            }
        };
        BoxWidget.selector = selector;
        return BoxWidget;
    }(ic.Widget));
    ic.BoxWidget = BoxWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var selector = "ic-options";
    ic.version[selector] = 1;
    var OptionsWidget = (function (_super) {
        __extends(OptionsWidget, _super);
        function OptionsWidget() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        OptionsWidget.selector = selector;
        return OptionsWidget;
    }(ic.BoxWidget));
    ic.OptionsWidget = OptionsWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var selector = "ic-video";
    ic.version[selector] = 1;
    var VideoWidget = (function (_super) {
        __extends(VideoWidget, _super);
        function VideoWidget() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        VideoWidget.selector = selector;
        return VideoWidget;
    }(ic.BoxWidget));
    ic.VideoWidget = VideoWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var selector = "ic-button";
    ic.version[selector] = 1;
    var ButtonWidget = (function (_super) {
        __extends(ButtonWidget, _super);
        function ButtonWidget(element) {
            var _this = _super.call(this, element) || this;
            if (element.getAttribute("icOnClick")) {
                _this.icOnClickFn = element.getAttribute("icOnClick");
            }
            if (element.hasAttribute("ic-story")) {
                _this.isStory = true;
            }
            _this.on(["click", "mousedown", "touchstart", "mouseup", "touchend"]);
            return _this;
        }
        ButtonWidget.prototype.onMouseDown = function (event) {
            if (!this.isDisabled(event)) {
                ButtonWidget.pressed = this;
                this.activate()
                    .toggleState(["click", "focus"])
                    .on(["!mousemove", "!touchmove", "!mouseup", "!touchend"]);
            }
        };
        ButtonWidget.prototype.onClick = function (event) {
        };
        ButtonWidget.prototype.onMouseUp = function (event) {
            if (ButtonWidget.pressed === this) {
                ButtonWidget.pressed = undefined;
                this.removeState("click");
                this.callUserFunc(this.icOnClickFn);
                if (this.isStory) {
                    var box = this.element, prev = void 0, next = void 0;
                    while (box && !next) {
                        box = box.parentElement.closest("ic-box[ic-story]");
                        if (box) {
                            prev = box.icWidget;
                            next = prev.nextStory;
                        }
                    }
                    if (next) {
                        prev.addState("disabled");
                    }
                }
            }
        };
        ButtonWidget.prototype.onMouseEnter = function (event) {
            if (ButtonWidget.pressed === this) {
                this.addState(["click", "hover"]);
            }
            else if (!ButtonWidget.pressed) {
                _super.prototype.onMouseEnter.call(this, event);
            }
        };
        ButtonWidget.prototype.onMouseLeave = function (event) {
            if (ButtonWidget.pressed === this) {
                this.removeState(["click", "hover"]);
            }
            else if (!ButtonWidget.pressed) {
                _super.prototype.onMouseLeave.call(this, event);
            }
        };
        ButtonWidget.prototype.allMouseMove = function (event) {
            if (ButtonWidget.pressed) {
                event.preventDefault();
            }
        };
        ButtonWidget.prototype.allMouseUp = function (event) {
            this.off("!mousemove", "!touchmove", "!mouseup", "!touchend");
            ButtonWidget.pressed = null;
        };
        ButtonWidget.selector = selector;
        return ButtonWidget;
    }(ic.Widget));
    ic.ButtonWidget = ButtonWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var FeedbackButtonWidget = (function (_super) {
        __extends(FeedbackButtonWidget, _super);
        function FeedbackButtonWidget() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        FeedbackButtonWidget.prototype.onClick = function (event) {
            if (!this.isDisabled(event)) {
                var feedback = this.get(this.screenWidget, ic.FeedbackWidget).first() || this.get(ic.FeedbackWidget).first();
                if (feedback) {
                    feedback.toggleFeedback();
                    ;
                }
            }
            _super.prototype.onClick.call(this, event);
        };
        FeedbackButtonWidget.selector = "ic-button[ic-button=feedback]";
        return FeedbackButtonWidget;
    }(ic.ButtonWidget));
    ic.FeedbackButtonWidget = FeedbackButtonWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var FirstButtonWidget = (function (_super) {
        __extends(FirstButtonWidget, _super);
        function FirstButtonWidget(element) {
            var _this = _super.call(this, element) || this;
            _this.on(".state");
            return _this;
        }
        FirstButtonWidget.prototype.onClick = function (event) {
            if (!this.isDisabled(event)) {
                var anchor = this.get(ic.AnchorWidget).first();
                if (anchor) {
                    anchor.parentWidget.addState("active");
                }
            }
            _super.prototype.onClick.call(this, event);
        };
        FirstButtonWidget.prototype.onState = function (widget, stateList) {
            if (widget instanceof ic.ActivityWidget && stateList.includes("active")) {
                var anchor = this.get(ic.AnchorWidget).first();
                this.toggleState("disabled", anchor ? anchor.hasState("active") : false);
            }
        };
        FirstButtonWidget.selector = "ic-button[ic-button=first]";
        return FirstButtonWidget;
    }(ic.ButtonWidget));
    ic.FirstButtonWidget = FirstButtonWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var FirstScreenButtonWidget = (function (_super) {
        __extends(FirstScreenButtonWidget, _super);
        function FirstScreenButtonWidget(element) {
            var _this = _super.call(this, element) || this;
            _this.on(".state");
            return _this;
        }
        FirstScreenButtonWidget.prototype.onClick = function (event) {
            if (!this.isDisabled(event)) {
                var screen_1 = this.get(ic.ScreenWidget).first();
                if (screen_1) {
                    screen_1.addState("active");
                }
            }
            _super.prototype.onClick.call(this, event);
        };
        FirstScreenButtonWidget.prototype.onState = function (widget, stateList) {
            if (widget instanceof ic.ScreenWidget && stateList.includes("active")) {
                this.toggleState("disabled", this.get(ic.ScreenWidget).first().hasState("active"));
            }
        };
        FirstScreenButtonWidget.selector = "ic-button[ic-button=firstscreen]";
        return FirstScreenButtonWidget;
    }(ic.ButtonWidget));
    ic.FirstScreenButtonWidget = FirstScreenButtonWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var LastButtonWidget = (function (_super) {
        __extends(LastButtonWidget, _super);
        function LastButtonWidget(element) {
            var _this = _super.call(this, element) || this;
            _this.on(".state");
            return _this;
        }
        LastButtonWidget.prototype.onClick = function (event) {
            if (!this.isDisabled(event)) {
                var anchor = this.get(ic.AnchorWidget).last();
                if (anchor) {
                    anchor.parentWidget.addState("active");
                }
            }
            _super.prototype.onClick.call(this, event);
        };
        LastButtonWidget.prototype.onState = function (widget, stateList) {
            if (widget instanceof ic.ActivityWidget && stateList.includes("active")) {
                var anchor = this.get(ic.AnchorWidget).last();
                this.toggleState("disabled", anchor ? anchor.hasState("active") : false);
            }
        };
        LastButtonWidget.selector = "ic-button[ic-button=last]";
        return LastButtonWidget;
    }(ic.ButtonWidget));
    ic.LastButtonWidget = LastButtonWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var LastScreenButtonWidget = (function (_super) {
        __extends(LastScreenButtonWidget, _super);
        function LastScreenButtonWidget(element) {
            var _this = _super.call(this, element) || this;
            _this.on(".state");
            return _this;
        }
        LastScreenButtonWidget.prototype.onClick = function (event) {
            if (!this.isDisabled(event)) {
                var screen_2 = this.get(ic.ScreenWidget).last();
                if (screen_2) {
                    screen_2.addState("active");
                }
            }
            _super.prototype.onClick.call(this, event);
        };
        LastScreenButtonWidget.prototype.onState = function (widget, stateList) {
            if (widget instanceof ic.ScreenWidget && stateList.includes("active")) {
                this.toggleState("disabled", this.get(ic.ScreenWidget).last().hasState("active"));
            }
        };
        LastScreenButtonWidget.selector = "ic-button[ic-button=lastscreen]";
        return LastScreenButtonWidget;
    }(ic.ButtonWidget));
    ic.LastScreenButtonWidget = LastScreenButtonWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var MarkButtonWidget = (function (_super) {
        __extends(MarkButtonWidget, _super);
        function MarkButtonWidget() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        MarkButtonWidget.prototype.onClick = function (event) {
            if (!this.isDisabled(event)) {
                var root = this.screenWidget || this.rootWidget;
                if (root) {
                    root.toggleState("marked");
                }
            }
            _super.prototype.onClick.call(this, event);
        };
        MarkButtonWidget.selector = "ic-button[ic-button=mark]";
        return MarkButtonWidget;
    }(ic.ButtonWidget));
    ic.MarkButtonWidget = MarkButtonWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var LeftButtonWidget = (function (_super) {
        __extends(LeftButtonWidget, _super);
        function LeftButtonWidget(element) {
            var _this = _super.call(this, element) || this;
            _this.scroll = function () {
                if (_this.hasState("click")) {
                    _this.delta = Math.min(4, _this.delta + 0.01);
                    _this.nav.scrollLeft = _this.start + Math.floor(_this.delta * _this.tick++);
                    ic.Widget.trigger(".scroll", _this.nav.icWidget);
                    ic.rAF("left", _this.scroll);
                }
            };
            var el = element.parentElement.firstElementChild;
            for (; el; el = el.nextElementSibling) {
                if (el.tagName === "IC-NAV") {
                    break;
                }
            }
            if (el) {
                _this.nav = el;
                _this.on(".scroll");
            }
            else {
                _this.addState("disabled");
            }
            return _this;
        }
        LeftButtonWidget.prototype.startup = function () {
            _super.prototype.startup.call(this);
        };
        LeftButtonWidget.prototype.onMouseDown = function (event) {
            _super.prototype.onMouseDown.call(this, event);
            this.tick = 0;
            this.start = this.nav.scrollLeft;
            this.delta = 1;
            ic.rAF("left", this.scroll);
        };
        LeftButtonWidget.prototype.onScroll = function (widget) {
            if (widget === this.nav.icWidget) {
                this.toggleState("disabled", this.nav.scrollLeft + this.nav.clientWidth >= this.nav.scrollWidth);
            }
        };
        LeftButtonWidget.selector = "ic-button[ic-button=left]";
        return LeftButtonWidget;
    }(ic.ButtonWidget));
    ic.LeftButtonWidget = LeftButtonWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var RightButtonWidget = (function (_super) {
        __extends(RightButtonWidget, _super);
        function RightButtonWidget(element) {
            var _this = _super.call(this, element) || this;
            _this.scroll = function () {
                if (_this.hasState("click")) {
                    _this.delta = Math.min(4, _this.delta + 0.01);
                    _this.nav.scrollLeft = _this.start - Math.floor(_this.delta * _this.tick++);
                    ic.Widget.trigger(".scroll", _this.nav.icWidget);
                    ic.rAF("right", _this.scroll);
                }
            };
            var el = element.parentElement.firstElementChild;
            for (; el; el = el.nextElementSibling) {
                if (el.tagName === "IC-NAV") {
                    break;
                }
            }
            if (el) {
                _this.nav = el;
                _this.on(".scroll");
            }
            else {
                _this.addState("disabled");
            }
            return _this;
        }
        RightButtonWidget.prototype.startup = function () {
            _super.prototype.startup.call(this);
        };
        RightButtonWidget.prototype.onMouseDown = function (event) {
            _super.prototype.onMouseDown.call(this, event);
            this.tick = 0;
            this.start = this.nav.scrollLeft;
            this.delta = 1;
            ic.rAF("right", this.scroll);
        };
        RightButtonWidget.prototype.onScroll = function (widget) {
            if (widget === this.nav.icWidget) {
                this.toggleState("disabled", this.nav.scrollLeft <= 0);
            }
        };
        RightButtonWidget.selector = "ic-button[ic-button=right]";
        return RightButtonWidget;
    }(ic.ButtonWidget));
    ic.RightButtonWidget = RightButtonWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var NextButtonWidget = (function (_super) {
        __extends(NextButtonWidget, _super);
        function NextButtonWidget(element) {
            var _this = _super.call(this, element) || this;
            switch (element.getAttribute("ic-next")) {
                case "anchor":
                    _this.needWidgetType = ic.AnchorWidget;
                    break;
                default:
                case "screen":
                    _this.needWidgetType = ic.ScreenWidget;
                    break;
            }
            return _this;
        }
        NextButtonWidget.prototype.startup = function () {
            _super.prototype.startup.call(this);
            if (this.hasAttribute("ic-wait") && this.get(this.closestWidget, ic.InputWidget).find(function (widget) { return widget.isScored; })) {
                this.isWait = true;
                this.addState("disabled")
                    .on([".submit", ".reset"]);
            }
            else {
                var widget = this.get(this.needWidgetType).filter(function (widget) { return !widget.isDisabled(); }).last();
                this.toggleState("disabled", widget ? widget.hasState("active") : false)
                    .on(".state");
            }
        };
        NextButtonWidget.prototype.onClick = function (event) {
            if (!this.isDisabled(event)) {
                var widget = this.get(this.needWidgetType).filter(function (widget) { return !widget.isDisabled(); }), index = widget.findIndex(function (widget) { return widget.hasState("active"); });
                if (index < widget.length - 1) {
                    widget[index + 1].addState("active");
                }
            }
            _super.prototype.onClick.call(this, event);
        };
        NextButtonWidget.prototype.onReset = function (screen) {
            if (!screen || this.screenWidget === screen) {
                this.addState("disabled");
            }
        };
        NextButtonWidget.prototype.onSubmit = function (screen) {
            if (!screen || this.screenWidget === screen) {
                this.toggleState("disabled", this.get(this.needWidgetType).filter(function (widget) { return !widget.isDisabled(); }).last().hasState("active"));
            }
        };
        NextButtonWidget.prototype.onState = function (widget, stateList) {
            if (widget instanceof ic.ScreenWidget && stateList.includes("active")) {
                var widget_1 = this.get(this.needWidgetType).filter(function (widget) { return !widget.isDisabled(); }).last();
                this.toggleState("disabled", widget_1 ? widget_1.hasState("active") : false);
            }
        };
        NextButtonWidget.selector = "ic-button[ic-button=next]";
        return NextButtonWidget;
    }(ic.ButtonWidget));
    ic.NextButtonWidget = NextButtonWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var PrevButtonWidget = (function (_super) {
        __extends(PrevButtonWidget, _super);
        function PrevButtonWidget(element) {
            var _this = _super.call(this, element) || this;
            switch (element.getAttribute("ic-prev")) {
                case "anchor":
                    _this.needWidgetType = ic.AnchorWidget;
                    break;
                default:
                case "screen":
                    _this.needWidgetType = ic.ScreenWidget;
                    break;
            }
            return _this;
        }
        PrevButtonWidget.prototype.startup = function () {
            _super.prototype.startup.call(this);
            if (this.hasAttribute("ic-wait") && this.get(this.closestWidget, ic.InputWidget).find(function (widget) { return widget.isScored; })) {
                this.isWait = true;
                this.addState("disabled")
                    .on([".submit", ".reset"]);
            }
            else {
                var widget = this.get(this.needWidgetType).filter(function (widget) { return !widget.isDisabled(); }).first();
                this.toggleState("disabled", widget ? widget.hasState("active") : false)
                    .on(".state");
            }
        };
        PrevButtonWidget.prototype.onClick = function (event) {
            if (!this.isDisabled(event)) {
                var widget = this.get(this.needWidgetType).filter(function (widget) { return !widget.isDisabled(); }), index = widget.findIndex(function (widget) { return widget.hasState("active"); });
                if (index > 0) {
                    widget[index - 1].addState("active");
                }
            }
            _super.prototype.onClick.call(this, event);
        };
        PrevButtonWidget.prototype.onReset = function (screen) {
            if (!screen || this.screenWidget === screen) {
                this.addState("disabled");
            }
        };
        PrevButtonWidget.prototype.onSubmit = function (screen) {
            if (!screen || this.screenWidget === screen) {
                this.toggleState("disabled", this.get(this.needWidgetType).filter(function (widget) { return !widget.isDisabled(); }).first().hasState("active"));
            }
        };
        PrevButtonWidget.prototype.onState = function (widget, stateList) {
            if (widget instanceof ic.ScreenWidget && stateList.includes("active")) {
                var widget_2 = this.get(this.needWidgetType).filter(function (widget) { return !widget.isDisabled(); }).first();
                this.toggleState("disabled", widget_2 ? widget_2.hasState("active") : false);
            }
        };
        PrevButtonWidget.selector = "ic-button[ic-button=prev]";
        return PrevButtonWidget;
    }(ic.ButtonWidget));
    ic.PrevButtonWidget = PrevButtonWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var StateButtonWidget = (function (_super) {
        __extends(StateButtonWidget, _super);
        function StateButtonWidget() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        StateButtonWidget.prototype.startup = function () {
            _super.prototype.startup.call(this);
            this.checkState();
            this.on([".screen", ".state"]);
        };
        StateButtonWidget.prototype.onScreen = function (screen) {
            if (screen === this.screenWidget) {
                this.checkState();
            }
        };
        StateButtonWidget.prototype.onState = function (widget, stateList) {
            var screen = this.screenWidget;
            if ((!screen || widget.screenWidget === screen)
                && ((widget instanceof ic.InputWidget && widget.isScored)
                    || (widget === screen && (stateList.includes("marked") || stateList.includes("reveal"))))) {
                ic.setImmediate(this.checkState);
            }
        };
        StateButtonWidget.selector = undefined;
        return StateButtonWidget;
    }(ic.ButtonWidget));
    ic.StateButtonWidget = StateButtonWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var ResetButtonWidget = (function (_super) {
        __extends(ResetButtonWidget, _super);
        function ResetButtonWidget(element) {
            var _this = _super.call(this, element) || this;
            _this.checkState = function () {
                var disabled = true, closestWidget = _this.closestWidget;
                if ((!_this.maxAttempts || closestWidget.attempts < _this.maxAttempts)
                    && (!_this.afterSubmit || closestWidget.hasState(["marked"]))) {
                    var widgets = _this.get(closestWidget, ic.InputWidget).filter(function (widget) { return widget.isScored; });
                    if (widgets.length) {
                        disabled = !widgets.some(function (widget) { return widget.hasAnswered; });
                    }
                }
                _this.toggleState("disabled", disabled);
            };
            if (element.getAttribute("ic-reset") === "submit") {
                _this.afterSubmit = true;
            }
            _this.maxAttempts = parseInt(element.getAttribute("ic-attempts") || 0, 10);
            return _this;
        }
        ResetButtonWidget.prototype.onClick = function (event) {
            if (!this.isDisabled(event)) {
                var screen_3 = this.screenWidget;
                (screen_3 || this.rootWidget).removeState(["marked", "reveal"]);
                ic.Widget.trigger(".reset", screen_3);
            }
            _super.prototype.onClick.call(this, event);
        };
        ResetButtonWidget.selector = "ic-button[ic-button=reset]";
        return ResetButtonWidget;
    }(ic.StateButtonWidget));
    ic.ResetButtonWidget = ResetButtonWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var RevealButtonWidget = (function (_super) {
        __extends(RevealButtonWidget, _super);
        function RevealButtonWidget(element) {
            var _this = _super.call(this, element) || this;
            _this.checkState = function () {
                var disabled = true, closestWidget = _this.closestWidget;
                if ((!_this.maxAttempts || closestWidget.attempts >= _this.maxAttempts)
                    && (!_this.afterSubmit || closestWidget.hasState(["marked"]))
                    && (!closestWidget.hasState(["reveal"]))) {
                    disabled = false;
                }
                _this.toggleState("disabled", disabled);
            };
            if (element.getAttribute("ic-reveal") === "submit") {
                _this.afterSubmit = true;
            }
            _this.maxAttempts = parseInt(element.getAttribute("ic-attempts") || 0, 10);
            return _this;
        }
        RevealButtonWidget.prototype.onClick = function (event) {
            if (!this.isDisabled(event)) {
                var screen_4 = this.screenWidget, root = screen_4 || this.rootWidget;
                if (root && !root.hasState("reveal")) {
                    root.toggleState({
                        "marked": false,
                        "reveal": true
                    });
                    ic.Widget.trigger(".reveal", screen_4);
                }
            }
            _super.prototype.onClick.call(this, event);
        };
        RevealButtonWidget.selector = "ic-button[ic-button=reveal]";
        return RevealButtonWidget;
    }(ic.StateButtonWidget));
    ic.RevealButtonWidget = RevealButtonWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var SubmitButtonWidget = (function (_super) {
        __extends(SubmitButtonWidget, _super);
        function SubmitButtonWidget(element) {
            var _this = _super.call(this, element) || this;
            _this.checkState = function () {
                var disabled = true, closestWidget = _this.closestWidget;
                if ((!_this.maxAttempts || closestWidget.attempts < _this.maxAttempts)
                    && (!closestWidget.hasState(["marked", "reveal"]))) {
                    var widgets = _this.get(closestWidget, ic.InputWidget).filter(function (widget) { return widget.isScored; });
                    if (!widgets.length) {
                        widgets = _this.get(closestWidget, ic.ActivityWidget).filter(function (widget) { return widget.isScored; });
                    }
                    if (widgets.length) {
                        if (_this.needSome) {
                            disabled = !widgets.some(function (widget) { return widget.hasAnswered; });
                        }
                        else {
                            disabled = !widgets.every(function (widget) { return widget.hasAnswered; });
                        }
                    }
                }
                _this.toggleState("disabled", disabled);
            };
            switch (element.getAttribute("ic-submit")) {
                case "any":
                case "some":
                    _this.needSome = true;
            }
            _this.maxAttempts = parseInt(element.getAttribute("ic-attempts") || 0, 10);
            return _this;
        }
        SubmitButtonWidget.prototype.startup = function () {
            var closestWidget = this.closestWidget;
            if (!this.needSome || this.get(closestWidget, ic.DroppableWidget).find(function (drag) { return !drag.isSortable; }) || this.get(closestWidget, ic.ToggleWidget).find(function (toggle) { return toggle.isScored; })) {
                this.needSome = true;
            }
            _super.prototype.startup.call(this);
        };
        SubmitButtonWidget.prototype.onClick = function (event) {
            if (!this.isDisabled(event)) {
                var screen_5 = this.screenWidget, root = screen_5 || this.rootWidget;
                if (!root.hasState(["marked", "reveal"])) {
                    var activity = this.activityWidget;
                    if (activity) {
                        activity.attempts = (activity.attempts || 0) + 1;
                    }
                    if (screen_5) {
                        screen_5.attempts = (screen_5.attempts || 0) + 1;
                    }
                    if (screen_5 !== root) {
                        root.attempts = (root.attempts || 0) + 1;
                    }
                    root.addState("marked");
                    ic.Widget.trigger(".submit", screen_5);
                }
            }
            _super.prototype.onClick.call(this, event);
        };
        SubmitButtonWidget.selector = "ic-button[ic-button=submit]";
        return SubmitButtonWidget;
    }(ic.StateButtonWidget));
    ic.SubmitButtonWidget = SubmitButtonWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var selector = "ic-feedback";
    ic.version[selector] = 1;
    var FeedbackWidget = (function (_super) {
        __extends(FeedbackWidget, _super);
        function FeedbackWidget(element) {
            var _this = _super.call(this, element) || this;
            _this.hideFeedback = function () {
                _this.element.style.display = "";
                clearTimeout(_this.timer);
                return _this;
            };
            var timeout = _this.timeout = (parseFloat(element.getAttribute(FeedbackWidget.selector)) || 0) * 1000;
            if (timeout) {
                element.addEventListener("click", _this.hideFeedback);
            }
            return _this;
        }
        FeedbackWidget.prototype.startup = function () {
            this.on([".reset", ".reveal", ".submit"]);
        };
        FeedbackWidget.prototype.onReset = function (screen) {
            if ((!screen || this.screenWidget === screen)) {
                this.hideFeedback();
            }
        };
        FeedbackWidget.prototype.onReveal = function (screen) {
            if ((!screen || this.screenWidget === screen)) {
                this.hideFeedback();
            }
        };
        FeedbackWidget.prototype.onSubmit = function (screen) {
            if (!screen || this.screenWidget === screen) {
                var element = this.element, activity = this.get(screen, ic.ActivityWidget).first();
                if (activity) {
                    var percent_1 = activity.markable.scaled * 100, attempts_1 = activity.attempts || (this.screenWidget || this.rootWidget).attempts, rand_1 = [], possible_1, best_1;
                    [].forEach.call(element.children, function (child) {
                        var i, wants = child.getAttribute("ic-feedback").split(/[,\s]+/), want;
                        for (i = 0; i < wants.length; i++) {
                            want = wants[i];
                            if (want) {
                                if (want[0] === "@" && want !== "@" + attempts_1) {
                                    return;
                                }
                                if (want.endsWith("%")) {
                                    if (parseInt(want, 10) > percent_1) {
                                        return;
                                    }
                                    best_1 = child;
                                }
                                if (want === "*") {
                                    rand_1.push(child);
                                }
                                else if (!possible_1) {
                                    possible_1 = child;
                                }
                            }
                        }
                    });
                    if (!best_1 && rand_1.length) {
                        best_1 = rand_1[Math.floor(Math.random() & rand_1.length)];
                    }
                    if (!best_1) {
                        best_1 = possible_1;
                    }
                    if (best_1) {
                        [].forEach.call(element.children, function (child) {
                            child.style.display = best_1 === child ? "" : "none";
                        });
                    }
                    var random = querySelectorAll(best_1 || element, "[ic-feedback='*']");
                    if (random.length) {
                        random.forEach(function (el) {
                            el.style.display = "none";
                        });
                        random[Math.floor(Math.random() * random.length)].style.display = "";
                    }
                    this.showFeedback();
                }
            }
        };
        FeedbackWidget.prototype.showFeedback = function () {
            this.element.style.display = "flex";
            if (this.timeout > 0) {
                this.timer = setTimeout(this.hideFeedback, this.timeout);
            }
            return this;
        };
        FeedbackWidget.prototype.toggleFeedback = function () {
            if (this.element.style.display) {
                return this.hideFeedback();
            }
            return this.showFeedback();
        };
        FeedbackWidget.selector = selector;
        return FeedbackWidget;
    }(ic.BoxWidget));
    ic.FeedbackWidget = FeedbackWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    ic.version["button"] = 1;
    var fakeMouseEnter, touchTimer, startX, startY, startEvent, startWidget;
    var InputButtonWidget = (function (_super) {
        __extends(InputButtonWidget, _super);
        function InputButtonWidget(element) {
            var _this = _super.call(this, element) || this;
            _this._finishTimer = function () {
                clearTimeout(touchTimer);
                touchTimer = undefined;
                InputButtonWidget.pressed = startWidget;
            };
            _this.markable.max = 0;
            if (element.getAttribute("icOnClick")) {
                _this.icOnClickFn = element.getAttribute("icOnClick");
            }
            if (element.hasAttribute("ic-story")) {
                _this.isStory = true;
            }
            _this.on(["click", "mousedown", "touchstart", "mouseup", "touchend"]);
            return _this;
        }
        InputButtonWidget.prototype._startTimer = function (event) {
            startWidget = this;
            startEvent = event;
            _a = ic.getCoords(event, false), startX = _a[0], startY = _a[1];
            touchTimer = setTimeout(this._finishTimer, 500);
            var _a;
        };
        InputButtonWidget.prototype._checkTimer = function (event) {
            var _a = ic.getCoords(event, false), clientX = _a[0], clientY = _a[1];
            if (Math.abs(clientX - startX) > 5 || Math.abs(clientY - startY) > 5) {
                clearTimeout(touchTimer);
                touchTimer = undefined;
                this.off("!mousemove", "!touchmove", "!mouseup", "!touchend");
            }
        };
        InputButtonWidget.prototype.onMouseDown = function (event) {
            if (!this.isDisabled(event) && !InputButtonWidget.pressed) {
                if (event instanceof MouseEvent || !this.isScrollable) {
                    InputButtonWidget.pressed = this;
                }
                else {
                    this._startTimer(event);
                }
                fakeMouseEnter = null;
                this.activate()
                    .toggleState(["click", "focus"])
                    .on(["!mousemove", "!touchmove", "!mouseup", "!touchend"]);
            }
        };
        InputButtonWidget.prototype.onClick = function (event) {
        };
        InputButtonWidget.prototype.onMouseUp = function (event) {
            if (InputButtonWidget.pressed === this) {
                InputButtonWidget.pressed = undefined;
                this.removeState("click");
                this.callUserFunc(this.icOnClickFn);
                if (this.isStory) {
                    var box = this.element, prev = void 0, next = void 0;
                    while (box && !next) {
                        box = box.parentElement.closest("ic-box[ic-story]");
                        if (box) {
                            prev = box.icWidget;
                            next = prev.nextStory;
                        }
                    }
                    if (next) {
                        prev.addState("disabled");
                    }
                }
            }
        };
        InputButtonWidget.prototype.onMouseEnter = function (event) {
            if (InputButtonWidget.pressed === this) {
                this.addState(["click", "hover"]);
            }
            else if (!InputButtonWidget.pressed) {
                _super.prototype.onMouseEnter.call(this, event);
            }
        };
        InputButtonWidget.prototype.onMouseLeave = function (event) {
            if (InputButtonWidget.pressed === this) {
                this.removeState(["click", "hover"]);
            }
            else if (!InputButtonWidget.pressed) {
                _super.prototype.onMouseLeave.call(this, event);
            }
        };
        InputButtonWidget.prototype.allMouseMove = function (event) {
            if (InputButtonWidget.pressed) {
                event.preventDefault();
                if (event.changedTouches) {
                    var pos = event.changedTouches[0], element = document.elementFromPoint(pos.clientX, pos.clientY), widgetElement = element && element.closestWidget(), widget = widgetElement && widgetElement.icWidget, widgets = widget && widget.parentWidgets;
                    if (widgets) {
                        var common = void 0;
                        if (fakeMouseEnter) {
                            common = widgets.intersect(fakeMouseEnter);
                            fakeMouseEnter.not(common).forEach(function (widget) {
                                if (widget.events["mouseleave"]) {
                                    widget.onMouseLeave(event);
                                }
                            });
                        }
                        widgets.not(common || []).forEach(function (widget) {
                            if (widget.events["mouseenter"]) {
                                widget.onMouseEnter(event);
                            }
                        });
                        fakeMouseEnter = widgets;
                    }
                }
            }
            else {
                this._checkTimer(event);
            }
        };
        InputButtonWidget.prototype.allMouseUp = function (event) {
            this.off("!mousemove", "!touchmove", "!mouseup", "!touchend");
            if (touchTimer) {
                this._finishTimer();
                this.onMouseUp(event);
            }
            if (fakeMouseEnter) {
                fakeMouseEnter.forEach(function (widget) {
                    if (widget.events["mouseleave"]) {
                        widget.onMouseLeave(event);
                    }
                });
            }
            InputButtonWidget.pressed = fakeMouseEnter = null;
        };
        return InputButtonWidget;
    }(ic.InputWidget));
    ic.InputButtonWidget = InputButtonWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var selector = "ic-audio";
    ic.version[selector] = 1;
    var pausedAudio = [];
    document.addEventListener("visibilitychange", function () {
        if (document.hidden) {
            getElementsByTagName("audio").forEach(function (audio) {
                if (!audio.paused) {
                    audio.pause();
                    pausedAudio.push(audio);
                }
            });
        }
        else {
            while (pausedAudio.length) {
                pausedAudio.pop().play();
            }
        }
    });
    var AudioWidget = (function (_super) {
        __extends(AudioWidget, _super);
        function AudioWidget(element) {
            var _this = _super.call(this, element) || this;
            _this.onPlaying = function () {
                querySelectorAll(_this.rootWidget.element, "audio,video").forEach(function (element) {
                    if (element !== _this.audio) {
                        element.pause();
                    }
                });
                _this.addState("playing");
                if (_this.audioType !== 0 && _this.parentWidget) {
                    _this.parentWidget.addState("attempted");
                }
            };
            _this.onPause = function () {
                _this.removeState("playing");
            };
            _this.onEnded = function () {
                _this.audio.pause();
                _this.removeState("playing");
                _this.audio.currentTime = 0;
                _this.onTimeupdate();
            };
            _this.onTimeupdate = function () {
                var audio = _this.audio, currentTime = audio.currentTime, duration = audio.duration, percent = Math.floor(currentTime * 100 / duration);
                if (_this.audioType !== 0) {
                    _this.markable.value = String(percent || 0);
                    _this.mark();
                }
                if (_this.fill) {
                    _this.element.style.backgroundImage = "linear-gradient(90deg, " + _this.fill + " " + percent + "%, transparent " + percent + "%)";
                }
            };
            var audio = element.getElementsByTagName("audio")[0], mark = _this.markable;
            _this.audioType = 0;
            switch (element.getAttribute(_this.selector)) {
                case "reset":
                    _this.audioType = 2;
                    break;
                case "play":
                    _this.audioType = 1;
                default:
                    if (element.hasAttribute(_this.selector + "-fill")) {
                        _this.fill = element.getAttribute(_this.selector + "-fill");
                    }
                    break;
            }
            if (!audio) {
                _this.findAudio = true;
                _this.addState(["disabled", "empty"]);
            }
            else {
                _this.audio = audio;
                _this.setupListeners();
            }
            if (mark.points === undefined) {
                mark.points = -1;
            }
            _this.on([".screen", ".timeout"]);
            return _this;
        }
        AudioWidget.prototype.setupListeners = function () {
            var audio = this.audio;
            if (audio && (this.audioType === 1 || this.audioType === 0)) {
                audio.addEventListener("playing", this.onPlaying);
                audio.addEventListener("pause", this.onPause);
                audio.addEventListener("ended", this.onEnded);
                audio.addEventListener("timeupdate", this.onTimeupdate);
                this.onTimeupdate();
            }
        };
        AudioWidget.prototype.onClick = function (event) {
            if ((event.changedTouches || event.which < 2) && !this.hasState("disabled")) {
                var audio = this.audio;
                if (audio) {
                    if (this.audioType === 1 || this.audioType === 0) {
                        if (this.hasState("playing")) {
                            audio.pause();
                        }
                        else {
                            audio.play();
                        }
                    }
                    else if (this.audioType === 2) {
                        audio.pause();
                        audio.currentTime = 0;
                    }
                }
            }
            _super.prototype.onClick.call(this, event);
        };
        AudioWidget.prototype.onScreen = function (screen) {
            var audio = this.audio;
            if (audio && (this.audioType === 1 || this.audioType === 0)) {
                audio.pause();
            }
            if (this.findAudio) {
                audio = screen.element.getElementsByTagName("audio")[0];
                this.toggleState(["disabled", "empty"], !audio);
                if (audio) {
                    this.audio = audio;
                    this.setupListeners();
                }
            }
        };
        AudioWidget.prototype.onTimeout = function () {
            if (this.audio && (this.audioType === 1 || this.audioType === 0)) {
                this.audio.pause();
            }
        };
        AudioWidget.selector = selector;
        return AudioWidget;
    }(ic.InputButtonWidget));
    ic.AudioWidget = AudioWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var selector = "ic-draggable";
    ic.version[selector] = 1;
    var DraggableWidget = (function (_super) {
        __extends(DraggableWidget, _super);
        function DraggableWidget(element) {
            var _this = _super.call(this, element) || this;
            _this.autoLeftCount = 0;
            _this.autoTopCount = 0;
            _this.droppables = [];
            _this.clones = [];
            _this.preLoaded = [];
            _this.autoscroll = function () {
                var el = _this.scrollElement;
                if (el) {
                    var oldLeft = el.scrollLeft, oldTop = el.scrollTop;
                    el.scrollLeft += _this.autoLeftDelta * (1 + Math.min(4, ++_this.autoLeftCount * 0.1));
                    el.scrollTop += _this.autoTopDelta * (1 + Math.min(4, ++_this.autoTopCount * 0.1));
                    if (oldLeft !== el.scrollLeft || oldTop !== el.scrollTop) {
                        ic.rAF("autoscroll", _this.autoscroll);
                    }
                }
            };
            _this.originalElement = element;
            if (querySelector(element, ":scope>ic-pins>ic-pin")) {
                _this.hasPins = true;
            }
            else {
                switch (element.getAttribute(DraggableWidget.selector)) {
                    case "copy":
                        _this.isCloneable = true;
                        break;
                }
            }
            _this.isReset = !element.hasAttribute(selector + "-sticky");
            _this.icOnMoveFn = element.getAttribute("onMove");
            _this.icOnEndFn = element.getAttribute("onEnd");
            return _this;
        }
        DraggableWidget.prototype.startup = function () {
            _super.prototype.startup.call(this);
            var mark = this.markable;
            if (!mark.value) {
                mark.value = this.element.textContent.replace(/[^\x20-\x7E]+/g, "").trim() || ("drag" + this.index);
            }
            if (this.hasPins) {
                this.getPins();
            }
            else {
                var dropElement = this.element.closest(ic.DroppableWidget.selector);
                if (dropElement) {
                    var drop = dropElement.icWidget;
                    this.droppables.pushOnce(drop);
                    drop.draggables.pushOnce(this);
                    drop.startDraggables = drop.draggables.clone();
                    drop.update();
                }
            }
            this.startDroppables = this.droppables.clone();
            this.on([".reset", ".reveal"]);
        };
        Object.defineProperty(DraggableWidget.prototype, "isScored", {
            get: function () {
                return false;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(DraggableWidget.prototype, "droppableWidget", {
            get: function () {
                var widget = this._droppableWidget;
                if (widget === undefined) {
                    var element = this.element.closest(ic.DroppableWidget.selector);
                    this._droppableWidget = widget = element ? element.icWidget : null;
                }
                return widget;
            },
            enumerable: true,
            configurable: true
        });
        DraggableWidget.prototype.invalidate = function () {
            delete this._droppableWidget;
            _super.prototype.invalidate.call(this);
        };
        DraggableWidget.prototype.includes = function (drop) {
            return this.droppables.includes(drop);
        };
        DraggableWidget.prototype.fixElement = function (event) {
            this.element = event.target.closest(DraggableWidget.selector) || this.originalElement;
        };
        DraggableWidget.prototype.onMouseEnter = function (event) {
            this.fixElement(event);
            _super.prototype.onMouseEnter.call(this, event);
        };
        DraggableWidget.prototype.onMouseDown = function (event) {
            this.fixElement(event);
            _super.prototype.onMouseDown.call(this, event);
            var _a = ic.getCoords(event), clientX = _a[0], clientY = _a[1];
            if (!this.isDisabled(event)) {
                var style = this.element.style;
                this.dragData = {
                    drag: this,
                    clientY: clientY,
                    clientX: clientX,
                    top: 0,
                    left: 0,
                    height: 0,
                    width: 0,
                    offsetY: 0,
                    offsetX: 0,
                    target: event.target.closest(DraggableWidget.selector)
                };
                this.on(["!mousemove", "!touchmove", "!mouseup", "!touchend"]);
                this.startLeft = style.left;
                this.startTop = style.top;
            }
        };
        DraggableWidget.prototype.onReset = function (screen) {
            if ((!screen || this.screenWidget === screen)) {
                this.removeState(["disabled", "reveal"]);
            }
        };
        DraggableWidget.prototype.onReveal = function (screen) {
            if ((!screen || this.screenWidget === screen)) {
                this.addState(["disabled", "reveal"]);
            }
        };
        DraggableWidget.prototype.getHelper = function (event, data, within) {
            if (this.isCloneable) {
                var clone = void 0, original = this.originalElement, target = data.target;
                if (!target || original === target) {
                    clone = original.cloneNode(true);
                    clone.icWidget = this;
                    original.parentElement.insertBefore(clone, original);
                    this.removeState(["click", "hover", "dragging"]);
                    this.element = clone;
                }
                else {
                    clone = target;
                }
                this.cloneElement = clone;
            }
            var el = this.cloneElement || this.originalElement, style = el.style, rect = getBoundingClientRect(el);
            el.classList.add("drag-helper");
            style.top = style.left = "0px";
            data.offsetY = data.clientY > rect.bottom ? rect.height / 2 : data.clientY - rect.top;
            data.offsetX = data.clientX > rect.right ? rect.width / 2 : data.clientX - rect.left;
            data.height = rect.height;
            data.width = rect.width;
            return el;
        };
        DraggableWidget.prototype.onHelperMove = function (event, data, within) {
            var el = this.cloneElement || this.originalElement, style = el.style, _a = ic.getCoords(event), clientX = _a[0], clientY = _a[1], left = (Math.range(within.left - data.left, clientX - data.offsetX, within.right - data.width - data.left) + (data.offsetLeft || 0)) / ic.scaleFactor, top = (Math.range(within.top - data.top, clientY - data.offsetY, within.bottom - data.height - data.top) + (data.offsetTop || 0)) / ic.scaleFactor;
            style.top = top + "px";
            style.left = left + "px";
            this.callUserFunc(this.icOnMoveFn, left, top);
        };
        DraggableWidget.prototype.freeHelper = function (event, drag, drop) {
            var el = this.cloneElement || this.originalElement, style = el.style;
            el.classList.remove("drag-helper");
            if (this.isCloneable) {
                this.cloneElement = null;
                if ((drop ? drop.element : el.parentElement) === this.originalElement.parentElement) {
                    el.parentElement.removeChild(el);
                }
                else if (!drop) {
                    style.top = style.left = "";
                }
                else {
                    this.clones.pushOnce(el);
                    this.on(["mouseenter", "mouseleave", "mousedown", "touchstart", "mouseup", "touchend"], el);
                    style.position = "";
                    this.fixState();
                }
                this.element = this.originalElement;
            }
            else if (!drop) {
                style.top = style.left = "";
            }
            var box = el.getBoundingClientRect();
            this.callUserFunc(this.icOnEndFn, box.left, box.top);
        };
        DraggableWidget.prototype.getParentRect = function () {
            return getBoundingClientRect(this.screenWidget);
        };
        DraggableWidget.prototype.getPins = function () {
            var _this = this;
            if (this.hasPins && !this.pins) {
                this.pins = this.get(this, ic.PinWidget).filter(function (pin) { return pin.parent === _this; });
            }
        };
        DraggableWidget.prototype.findPin = function (drop) {
            this.getPins();
            return this.pins.find(function (pin) { return pin.target && pin.target.parent === drop; });
        };
        DraggableWidget.prototype.getPin = function (event) {
            var _this = this;
            this.getPins();
            var _a = ic.getCoords(event), clientX = _a[0], clientY = _a[1], available = this.pins.filter(function (pin) { return !pin.target || (!_this.preLoaded.includes(pin.drop) && !pin.drop.hasState("disabled")); }), underMouse = (event && available.filter(function (pin) { return pin.inRect(clientX, clientY) || (pin.target && pin.target.inRect(clientX, clientY)); }));
            if (underMouse && underMouse.length) {
                available = underMouse;
            }
            var index = available.indexOf(this.lastPin) + 1;
            return this.lastPin = this.usePin
                || available.find(function (pin) { return !pin.target; })
                || available[index >= available.length ? 0 : index];
        };
        DraggableWidget.prototype.allMouseMove = function (event) {
            var _this = this;
            _super.prototype.allMouseMove.call(this, event);
            var dragData = this.dragData, _a = ic.getCoords(event, false), clientX = _a[0], clientY = _a[1], targets = this.targets;
            if (!targets
                && (Math.abs(dragData.clientY - clientY) > 5
                    || Math.abs(dragData.clientX - clientX) > 5)) {
                this.targets = targets = this.get(this.screenWidget, ic.DroppableWidget, this.group || null).filter(function (drop) {
                    if (_this.activitiesWidget === drop.activitiesWidget || _this.activityWidget === drop.activityWidget) {
                        return drop.canAccept(_this);
                    }
                });
                targets.forEach(function (widget) {
                    if (widget.dropShapes) {
                        widget.dropShapes.forEach(function (el) {
                            el.classList.add("drop-helper");
                        });
                    }
                });
                this.addState("dragging");
                if (this.hasPins) {
                    this.usePin = this.helper = this.getPin(event);
                }
                if (!this.helper) {
                    this.helper = this;
                }
                this.helper.getHelper(event, dragData, this.getParentRect());
            }
            if (targets) {
                var underMouse = document.elementFromPoint(clientX, clientY);
                if (!underMouse) {
                    return;
                }
                var closestDrop_1 = underMouse.closest(".drop");
                this.helper.onHelperMove(event, dragData, this.getParentRect());
                this.drop = null;
                targets.forEach(function (widget) {
                    var over = widget.dropShapes
                        ? widget.dropShapes.includes(closestDrop_1)
                        : widget.inRect(clientX, clientY);
                    if (over) {
                        _this.drop = widget;
                    }
                    widget.onDragHover(_this, over, event);
                });
                var autoscroll = document.elementFromPoint(clientX, clientY);
                for (; autoscroll; autoscroll = autoscroll.parentElement) {
                    var width = autoscroll.scrollWidth > autoscroll.clientWidth, height = autoscroll.scrollHeight > autoscroll.clientHeight;
                    if (width || height) {
                        var style = getComputedStyle(autoscroll), scrollX_1 = width && /(auto|scroll)/i.test(style.overflowX), scrollY_1 = height && /(auto|scroll)/i.test(style.overflowY);
                        if (scrollX_1 || scrollY_1) {
                            var rect = getBoundingClientRect(autoscroll), fontSize = parseFloat(style.fontSize) / ic.scaleFactor, left = !scrollX_1
                                ? 0
                                : clientX < rect.left + fontSize && autoscroll.scrollLeft > 0
                                    ? -1
                                    : clientX > rect.right - fontSize && autoscroll.scrollLeft < autoscroll.scrollWidth - autoscroll.clientWidth
                                        ? 1
                                        : 0, top_1 = !scrollY_1
                                ? 0
                                : clientY < rect.top + fontSize && autoscroll.scrollTop > 0
                                    ? -1
                                    : clientY > rect.bottom - fontSize && autoscroll.scrollTop < autoscroll.scrollHeight - autoscroll.clientHeight
                                        ? 1
                                        : 0;
                            if (left || top_1) {
                                this.autoLeftDelta = left;
                                this.autoTopDelta = top_1;
                                ic.rAF("autoscroll", this.autoscroll);
                                break;
                            }
                        }
                    }
                }
                if (this.scrollElement !== autoscroll) {
                    this.scrollElement = autoscroll;
                    this.autoLeftCount = this.autoTopCount = 0;
                }
                event.preventDefault();
                return false;
            }
        };
        DraggableWidget.prototype.allMouseUp = function (event) {
            var _this = this;
            _super.prototype.allMouseUp.call(this, event);
            this.scrollElement = null;
            this.off("!mousemove", "!touchmove", "!mouseup", "!touchend");
            if (this.targets) {
                var drop = this.drop, helper = this.helper;
                drop = drop && drop.onDragDrop(this, helper, event) ? drop : null;
                helper.freeHelper(event, this, drop);
                this.targets.forEach(function (widget) {
                    if (widget.dropShapes) {
                        widget.dropShapes.forEach(function (el) {
                            el.classList.remove("drop-helper");
                        });
                    }
                    widget.onDragEnd(_this, event, widget);
                });
                this.removeState("dragging");
                if (helper !== this) {
                    this.removeState("hover");
                }
                this.targets = this.helper = this.drop = this.usePin = null;
                if (!drop && this.droppables.length === 1) {
                    if (this.isReset || (document.elementFromPoint.apply(document, ic.getCoords(event, false)) || document.body).parentElements(this.originalElement, true).pop() === this.originalElement) {
                        if (this.pins) {
                            drop = this.droppables[0];
                            this.droppables.remove(drop);
                            drop.draggables.remove(this);
                            helper.unlinkPin();
                        }
                        else {
                            var style = this.element.style;
                            style.left = this.startLeft;
                            style.top = this.startTop;
                        }
                    }
                }
            }
        };
        DraggableWidget.prototype.mark = function () {
            return;
        };
        DraggableWidget.selector = selector;
        return DraggableWidget;
    }(ic.InputButtonWidget));
    ic.DraggableWidget = DraggableWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var selector = "ic-option";
    ic.version[selector] = 0;
    var OptionWidget = (function (_super) {
        __extends(OptionWidget, _super);
        function OptionWidget(element) {
            var _this = _super.call(this, element) || this;
            _this.noMarking = true;
            var mark = _this.markable;
            if (!mark.value) {
                mark.value = element.textContent.replace(/[^\x20-\x7E]+/g, "").trim() || ("option" + _this.index);
            }
            _this.on("click");
            return _this;
        }
        Object.defineProperty(OptionWidget.prototype, "hasAnswered", {
            get: function () {
                return false;
            },
            enumerable: true,
            configurable: true
        });
        OptionWidget.prototype.onClick = function (event) {
            if (!this.isDisabled(event)) {
                event.preventDefault();
                this.toggleState({
                    "checked": true,
                    "click": false,
                    "hover": false
                }).get(this.parentWidget, OptionWidget, false).forEach(function (widget) {
                    widget.removeState(["checked", "click", "hover"]);
                });
                if (event) {
                    if (this.parentWidget) {
                        this.parentWidget.addState("attempted");
                    }
                    if (this.activityWidget) {
                        this.activityWidget.addState("attempted");
                    }
                }
            }
        };
        OptionWidget.selector = selector;
        return OptionWidget;
    }(ic.InputButtonWidget));
    ic.OptionWidget = OptionWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var selector = "ic-select";
    ic.version[selector] = 1;
    var SelectWidget = (function (_super) {
        __extends(SelectWidget, _super);
        function SelectWidget(element) {
            var _this = _super.call(this, element) || this;
            _this.closePopup = function (event) {
                if (_this.hasState("open")) {
                    for (var el = event.target; el; el = el.parentElement) {
                        if (el === _this.element) {
                            return;
                        }
                    }
                    _this.removeState("open");
                }
            };
            _this.optionsElement = querySelector(element, ":scope>ic-options");
            if (!_this.optionsElement) {
                console.error("No <ic-options> element found!", element);
            }
            _this.on(["click", ".state", ".persist", ".reset", ".reveal"]);
            document.addEventListener("mouseup", _this.closePopup);
            document.addEventListener("touchend", _this.closePopup);
            return _this;
        }
        SelectWidget.prototype.startup = function () {
            _super.prototype.startup.call(this);
            var options = this.get(this, ic.OptionWidget), mark = this.markable, value = mark.value, index = value && value.regex(/#option([0-9]+)#/);
            this.startValue = value || "";
            if (value) {
                this.get(this, ic.OptionWidget, false).forEach(function (widget) {
                    widget.toggleState("checked", widget.markable.value === value);
                });
            }
            else {
                this.addState("empty");
            }
            options.forEach(function (widget) {
                mark.max = Math.max(mark.max || 0, widget.markable.max || 1);
                if (value) {
                    widget.toggleState("checked", widget.index === index);
                }
            });
            if (mark.points === undefined) {
                mark.points = mark.max || 1;
            }
        };
        Object.defineProperty(SelectWidget.prototype, "hasAnswered", {
            get: function () {
                var mark = this.markable;
                return mark.points >= 0 && !!mark.value;
            },
            enumerable: true,
            configurable: true
        });
        SelectWidget.prototype.onPersist = function (state) {
            var optionWidgets = this.get(this, ic.OptionWidget);
            if (isUndefined(state)) {
                return optionWidgets.findIndex(function (widget) {
                    return widget.hasState("checked");
                }) + 1;
            }
            optionWidgets.forEach(function (widget, index) {
                widget.toggleState("checked", index + 1 === state);
            });
            if (!state) {
                this.addState("empty");
            }
        };
        SelectWidget.prototype.onClick = function (event) {
            if (!this.isDisabled(event)) {
                event.preventDefault();
                this.toggleState("open");
            }
        };
        SelectWidget.prototype.onFrame = function () {
            var box = this.element.getBoundingClientRect();
            if (this.posTop !== box.top || this.posLeft !== box.left) {
                this.removeState("open");
            }
        };
        SelectWidget.prototype.onReset = function (screen) {
            if (!screen || this.screenWidget === screen) {
                var mark_2 = this.markable;
                mark_2.value = this.startValue;
                if (mark_2.value) {
                    this.get(this, ic.OptionWidget, false).forEach(function (widget) {
                        widget.toggleState({
                            "checked": widget.markable.value === mark_2.value,
                            "click": false,
                            "hover": false
                        });
                    });
                }
                else {
                    Array.from(this.element.childNodes).not(this.optionsElement).forEach(function (el) {
                        el.parentElement.removeChild(el);
                    });
                }
                this.toggleState({
                    "open": false,
                    "disabled": false,
                    "empty": !mark_2.value
                });
            }
        };
        SelectWidget.prototype.onReveal = function (screen) {
            if ((!screen || this.screenWidget === screen) && !this.hasState("reveal")) {
                this.get(this.closestWidget, SelectWidget, this.group, true).forEach(function (widget) {
                    if (!widget.hasState("reveal")) {
                        var element_1 = widget.element, answers_1 = widget.expandedAnswers, answer = widget.get(widget, ic.OptionWidget).filter(function (option) {
                            var value = option.markable.value;
                            option.removeState("checked");
                            return isString(value)
                                ? answers_1.includes(value)
                                : isArray(value)
                                    ? value.some(function (val) { return answers_1.includes(val); })
                                    : false;
                        })[0];
                        Array.from(element_1.childNodes).not(widget.optionsElement).forEach(function (el) {
                            element_1.removeChild(el);
                        });
                        if (answer) {
                            answer.addState("checked");
                        }
                        widget
                            .toggleState({
                            "empty": !answer,
                            "disabled": true,
                            "reveal": true
                        })
                            .mark();
                    }
                });
            }
        };
        SelectWidget.prototype.onState = function (widget, stateList) {
            if (this === widget && stateList.includes("open")) {
                if (this.hasState("open")) {
                    var box = this.element.getBoundingClientRect(), scrollBox = (closestScroll(this.element) || document.body).getBoundingClientRect(), optionsBox = this.optionsElement.getBoundingClientRect(), style = this.optionsElement.style;
                    if (optionsBox.height + box.bottom >= scrollBox.bottom) {
                        if (box.top - optionsBox.height >= scrollBox.top) {
                            this.addState("alt");
                            style.top = box.top - optionsBox.height + "px";
                        }
                        else {
                            style.top = scrollBox.bottom - optionsBox.height + "px";
                        }
                    }
                    else {
                        style.top = box.bottom + "px";
                    }
                    style.left = Math.min(box.left, scrollBox.right - optionsBox.width) + "px";
                    style.minWidth = box.width + "px";
                    this.posTop = box.top;
                    this.posLeft = box.left;
                    this.on(".frame");
                }
                else {
                    this
                        .removeState("alt")
                        .off(".frame");
                }
            }
            else if (widget instanceof ic.OptionWidget
                && widget.parentWidget === this
                && stateList.includes("checked")
                && widget.hasState("checked")) {
                var element_2 = this.element, clone = widget.element.cloneNode(true), thisMark = this.markable, widgetMark = widget.markable;
                this.removeState("empty");
                Array.from(element_2.childNodes).not(this.optionsElement).forEach(function (el) {
                    element_2.removeChild(el);
                });
                Array.from(clone.childNodes).forEach(function (el) {
                    element_2.appendChild(el);
                });
                thisMark.points = widgetMark.points || 1;
                thisMark.value = widgetMark.value;
                this.mark();
            }
        };
        SelectWidget.selector = selector;
        SelectWidget.isTreeWidget = true;
        return SelectWidget;
    }(ic.InputButtonWidget));
    ic.SelectWidget = SelectWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var selector = "ic-toggle";
    ic.version[selector] = 1;
    var ToggleWidget = (function (_super) {
        __extends(ToggleWidget, _super);
        function ToggleWidget(element) {
            var _this = _super.call(this, element) || this;
            _this.toggleType = 0;
            var mark = _this.markable, selector = ToggleWidget.selector.split(",").first();
            switch (element.getAttribute(selector)) {
                case "double":
                    _this.toggleType = 1;
                    _this.count = 2;
                    break;
                case "multiple":
                    _this.toggleType = 1;
                    _this.count = parseInt(element.getAttribute(selector + "-count"), 10) || 0;
                    break;
                case "range":
                    _this.toggleType = 2;
                    break;
                case "multirange":
                    _this.toggleType = 3;
                    break;
            }
            if (mark.points === undefined) {
                if (_this.toggleType === 2 || _this.toggleType === 3) {
                    var text = element.textContent;
                    mark.points = text && /[^ .,:;'"]/.test(text) ? 1 : -1;
                }
                else {
                    mark.points = 1;
                }
            }
            if (mark.value && mark.value !== "checked") {
                _this.internalValue = mark.value;
                if (mark.answer === "checked") {
                    mark.answer = _this.internalValue;
                    mark.value = "";
                }
            }
            _this.toggleState("checked", mark.value === (_this.internalValue || "checked"))
                .on(".state");
            if (element.tagName.toLowerCase() === selector) {
                _this.on(".persist");
            }
            else {
                _this.on(".timeout");
            }
            return _this;
        }
        ToggleWidget.prototype.startup = function () {
            _super.prototype.startup.call(this);
            var closestWidget = this.activityWidget || this.screenWidget;
            if ((this.toggleType === 2 || this.toggleType === 3)
                && closestWidget) {
                if (!closestWidget.toggleRange) {
                    closestWidget.toggleRange = {};
                }
                if (!closestWidget.toggleRange[this.group]) {
                    var wanted_1 = this.toggleType;
                    closestWidget.toggleRange[this.group] = this.get(closestWidget, ToggleWidget, this.group, true).filter(function (toggle) {
                        if (toggle.toggleType !== 2 && toggle.toggleType !== 3) {
                            return false;
                        }
                        if (toggle.toggleType !== wanted_1) {
                            console.error("Error: Mismatched ic-toggle=\"" + (wanted_1 === 3 ? "multi" : "") + "range\" type:", toggle);
                        }
                        return true;
                    });
                }
            }
            if (this.startState) {
                this.startState["checked"] = this.startState["checked"] || false;
                this.on(".reveal");
            }
        };
        Object.defineProperty(ToggleWidget.prototype, "hasAnswered", {
            get: function () {
                return this.hasState("checked");
            },
            enumerable: true,
            configurable: true
        });
        ToggleWidget.prototype.onPersist = function (state) {
            if (isUndefined(state)) {
                return this.hasState("checked") ? 1 : 0;
            }
            this.toggleState("checked", !!state);
        };
        ToggleWidget.prototype.onReveal = function (screen) {
            if ((!screen || this.screenWidget === screen)) {
                this
                    .toggleState({
                    "disabled": true,
                    "reveal": true,
                    "checked": this.markable.answer === "checked"
                })
                    .mark();
            }
        };
        ToggleWidget.prototype.startDragging = function (click) {
            var _this = this;
            var range = (this.activityWidget || this.screenWidget).toggleRange[this.group];
            if (!click) {
                this.startDrag = true;
                this.toggleState("checked")
                    .parentWidget.addState("attempted");
            }
            if (this.toggleType === 2) {
                var index = range.indexOf(this), isChecked = this.hasState("checked"), prevChecked = index === 0 ? isChecked : !range[index - 1].hasState("checked"), nextChecked = index === range.length - 1 ? isChecked : !range[index + 1].hasState("checked");
                if ((isChecked && prevChecked && nextChecked)
                    || (!isChecked && prevChecked === nextChecked)) {
                    range.forEach(function (toggle) {
                        if (toggle !== _this) {
                            toggle.removeState("checked");
                        }
                    });
                }
                if (!isChecked && prevChecked === nextChecked) {
                    this.toggleState("checked");
                }
            }
            range.forEach(function (toggle) {
                toggle.startChecked = toggle.hasState("checked");
            });
        };
        ToggleWidget.prototype.finishDragging = function () {
            var checked = false, attempted = false, range = (this.activityWidget || this.screenWidget).toggleRange[this.group], fixDangling = function (toggle) {
                var isChecked = toggle.hasState("checked");
                attempted = attempted || isChecked;
                if (!checked && toggle.markable.points === -1 && isChecked) {
                    toggle.removeState("checked");
                }
                else {
                    checked = isChecked;
                }
            };
            range.forEach(fixDangling);
            checked = attempted = false;
            range.slice(0).reverse().forEach(fixDangling);
            if (attempted && this.parentWidget) {
                this.parentWidget.addState("attempted");
            }
        };
        ToggleWidget.prototype.onMouseDown = function (event) {
            this.startDrag = ToggleWidget.range = false;
            _super.prototype.onMouseDown.call(this, event);
        };
        ;
        ToggleWidget.prototype.onMouseUp = function (event) {
            var pressed = ic.InputButtonWidget.pressed;
            _super.prototype.onMouseUp.call(this, event);
            if (pressed && !this.isDisabled(event) && !this.startDrag) {
                if (this.activityWidget && this.toggleType === 1 && this.count) {
                    var count_1 = 0;
                    this.get(this.activityWidget, ToggleWidget, this.group, false).forEach(function (widget) {
                        if (widget.hasState("checked")) {
                            count_1++;
                        }
                    });
                    if (count_1 >= this.count) {
                        return;
                    }
                }
                this.toggleState("checked");
                if (this.activityWidget) {
                    if (this.toggleType === 0) {
                        this.get(this.activityWidget, ToggleWidget, this.group, false).forEach(function (widget) {
                            if (widget.toggleType === 0) {
                                widget.removeState("checked");
                            }
                        });
                    }
                    else if (this.toggleType === 2 || this.toggleType === 3) {
                        this.startDragging(!ToggleWidget.range);
                        this.finishDragging();
                    }
                }
                if (this.screenWidget) {
                    this.parentWidget.addState("attempted");
                }
            }
        };
        ToggleWidget.prototype.onMouseEnter = function (event) {
            var parent = this.activityWidget || this.screenWidget;
            if (ic.InputButtonWidget.pressed instanceof ToggleWidget
                && (this.toggleType === 2 || this.toggleType === 3)
                && parent) {
                var range = parent.toggleRange[this.group], first = ic.InputButtonWidget.pressed, start = range.indexOf(first), end = range.indexOf(this);
                if (this !== first) {
                    ToggleWidget.range = true;
                }
                if (start !== -1) {
                    if (!first.startDrag) {
                        first.startDragging();
                    }
                    var min_1 = Math.min(start, end), max_1 = Math.max(start, end), checked_1 = ic.InputButtonWidget.pressed.hasState("checked");
                    range.forEach(function (toggle, index) {
                        toggle
                            .toggleState("checked", index >= min_1 && index <= max_1 ? checked_1 : toggle.startChecked);
                    });
                    this.finishDragging();
                }
            }
            _super.prototype.onMouseEnter.call(this, event);
        };
        ToggleWidget.prototype.onState = function (widget, stateList) {
            if (this === widget && stateList.includes("checked")) {
                var mark = this.markable;
                mark.value = this.hasState("checked") ? this.internalValue || "checked" : "";
                this.mark();
            }
        };
        ToggleWidget.prototype.onTimeout = function () {
            this.removeState("checked");
        };
        ToggleWidget.selector = selector + ",ic-button[ic-button=toggle]";
        return ToggleWidget;
    }(ic.InputButtonWidget));
    ic.ToggleWidget = ToggleWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var selector = "ic-droppable";
    ic.version[selector] = 1;
    var DroppableWidget = (function (_super) {
        __extends(DroppableWidget, _super);
        function DroppableWidget(element) {
            var _this = _super.call(this, element) || this;
            _this.draggables = [];
            _this.startDraggables = [];
            _this.preLoaded = [];
            var mark = _this.markable, pinCount = element.querySelectorAll(":scope>ic-pins>ic-pin").length;
            switch (element.getAttribute(DroppableWidget.selector)) {
                case "colour":
                    _this.index = -1;
                    _this.isColour = true;
                    break;
                case "horizontal":
                    _this.index = -1;
                    _this.isHorizontal = true;
                    break;
                case "vertical":
                    _this.index = -1;
                    _this.isVertical = true;
                    break;
                case "shape":
                    var shapes = element.querySelectorAll(":scope>svg .drop");
                    if (shapes.length) {
                        _this.dropShapes = Array.from(shapes);
                    }
                case "position":
                    _this.index = -1;
                    _this.isHorizontal = true;
                    _this.isVertical = true;
                    break;
                case "sortable":
                    _this.isSortable = true;
                    break;
            }
            _this.count = _this.isSortable ? 1 : pinCount || Math.max(parseInt(element.getAttribute(DroppableWidget.selector + "-count"), 10) || element.querySelectorAll(":scope>ic-draggable").length || 1, 1);
            var points = isArray(mark.answer) ? Math.min(_this.count, mark.answer.length) : mark.answer || _this.isSortable ? 1 : -1;
            if (!pinCount && element.querySelectorAll(":scope>ic-draggable>ic-pins>ic-pin").length) {
                _this.dragPins = true;
            }
            if (mark.answer) {
                mark.max = points;
            }
            if (isUndefined(mark.points)) {
                mark.points = points;
            }
            _this.icOnDropFn = element.getAttribute("onDrop");
            if (_this.count > 1) {
                _this.index = -1;
            }
            return _this;
        }
        DroppableWidget.prototype.startup = function () {
            var _this = this;
            if (!this.isSortable) {
                var pins = this.get(this, ic.PinWidget).filter(function (pin) { return pin.parent === _this; });
                if (pins.length) {
                    var value = this.markable.value, drags = isString(value) ? [value] : isArray(value) ? value : [];
                    this.pins = pins;
                    this.markable.value = drags;
                    if (drags) {
                        drags.forEach(function (target) {
                            var dragIndex = target.regex(/#drag([0-9]+)#/), drag = _this.get(_this.activitiesWidget || _this.activityWidget, ic.DraggableWidget, _this.group).find(function (drag) {
                                return drag.index === dragIndex;
                            });
                            if (drag) {
                                _this.preLoaded.push(drag);
                                drag.preLoaded.push(_this);
                                drag.getPin().linkPin(drag, _this);
                            }
                        });
                    }
                    this.startDraggables = this.draggables.clone();
                }
            }
            this.on([".persist", ".reveal"])
                .update();
            _super.prototype.startup.call(this);
        };
        DroppableWidget.prototype.rememberDrag = function (drag) {
            var drop = drag.droppables[0];
            if (drop) {
                drag.droppables.remove(drop);
                drop.draggables.remove(drag);
            }
            this.draggables.pushOnce(drag);
            drag.droppables.pushOnce(this);
        };
        Object.defineProperty(DroppableWidget.prototype, "hasAnswered", {
            get: function () {
                var mark = this.markable;
                return mark.points >= 0 && !this.hasState("empty") && !isUndefined(mark.value);
            },
            enumerable: true,
            configurable: true
        });
        DroppableWidget.prototype.onPersist = function (state) {
            var _this = this;
            if (isUndefined(state)) {
                state = [];
                this.draggables.forEach(function (drag) {
                    var value = drag.index;
                    if (_this.isHorizontal || _this.isVertical) {
                        var el = (_this.pins ? _this.pins.find(function (pin) {
                            return pin.drag === drag;
                        }) : drag).element;
                        value = [value];
                        if (_this.isHorizontal) {
                            value[1] = parseFloat(el.style.left) || 0;
                        }
                        if (_this.isVertical) {
                            value[2] = parseFloat(el.style.top) || 0;
                        }
                    }
                    state.push(value);
                });
                return state;
            }
            if (isArray(state)) {
                var drags_1 = this.get(this.activitiesWidget || this.activityWidget, ic.DraggableWidget), pinElements_1 = [];
                state.forEach(function (value, index) {
                    var values = isArray(value) ? value : [value], drag = drags_1.find(function (drag) {
                        return drag.index === values[0];
                    });
                    if (drag) {
                        var oldDrop = drag.droppableWidget;
                        if (oldDrop !== _this) {
                            if (oldDrop) {
                                drag.droppables.remove(oldDrop);
                                oldDrop.draggables.remove(drag);
                            }
                            drag.droppables.pushOnce(_this);
                            _this.draggables.pushOnce(drag);
                            if (_this.pins) {
                                var pin = drag.pins[drag.droppables.indexOf(_this)];
                                pin.linkPin(drag, _this, _this.pins[index]);
                                pinElements_1.push(pin.element);
                            }
                            else {
                                appendChild(_this.element, drag.originalElement);
                            }
                        }
                        if (_this.isHorizontal || _this.isVertical) {
                            var el = (_this.pins ? _this.pins.find(function (pin) {
                                return pin.drag === drag;
                            }) : drag).element;
                            if (_this.isHorizontal) {
                                el.style.left = values[1] + "%";
                            }
                            if (_this.isVertical) {
                                el.style.top = values[2] + "%";
                            }
                        }
                    }
                });
                this.update();
            }
        };
        DroppableWidget.prototype.removeUnlisted = function (arr) {
            var _this = this;
            var changed = false;
            if (this.pins) {
                this.pins.forEach(function (pin) {
                    if (!arr.includes(pin.drag)) {
                        pin.unlinkPin();
                        changed = true;
                    }
                });
            }
            else if (!this.dragPins) {
                [].forEach.call(this.element.children, function (child) {
                    var drag = child.icWidget;
                    if (drag && drag instanceof ic.DraggableWidget && !arr.includes(drag)) {
                        if (drag.isCloneable && child !== drag.originalElement) {
                            drag.clones.remove(child);
                        }
                        _this.element.removeChild(child);
                        _this.draggables.remove(drag);
                        changed = true;
                    }
                });
            }
            return changed;
        };
        DroppableWidget.prototype.onReset = function (screen) {
            var _this = this;
            if ((!screen || this.screenWidget === screen)) {
                var changed_1 = false;
                this.startDraggables.forEach(function (drag, index) {
                    if (drag) {
                        if (!drag.droppables.includes(_this)) {
                            if (_this.pins) {
                                _this.pins[index].linkPin(drag, _this, drag.pins[drag.startDroppables.indexOf(_this)]);
                            }
                            else {
                                var oldDrop = drag.droppableWidget;
                                if (oldDrop) {
                                    drag.droppables.remove(oldDrop);
                                    oldDrop.draggables.remove(drag);
                                    if (oldDrop.isHorizontal || oldDrop.isVertical) {
                                        var style = drag.originalElement.style;
                                        style.left = style.top = "";
                                    }
                                    oldDrop.update();
                                }
                                drag.droppables.pushOnce(_this);
                                _this.draggables.pushOnce(drag);
                                appendChild(_this.element, drag.originalElement);
                            }
                            changed_1 = true;
                        }
                    }
                    else if (_this.pins) {
                        _this.pins[index].unlinkPin();
                        changed_1 = true;
                    }
                });
                if (this.removeUnlisted(this.startDraggables) || changed_1) {
                    this.update();
                }
                _super.prototype.onReset.call(this, screen);
            }
        };
        DroppableWidget.prototype.onReveal = function (screen) {
            var _this = this;
            if ((!screen || this.screenWidget === screen) && !this.hasState("reveal")) {
                var usedAnswers_2 = this.activityWidget && this.activityWidget.allowDuplicates ? undefined : [];
                this.get(this.closestWidget, DroppableWidget, this.group, true).forEach(function (widget) {
                    if (!widget.hasState("reveal")) {
                        var mark = widget.markable, answer_2;
                        if (!mark.answer && mark.points < 0) {
                            widget.startDraggables.forEach(function (drag) {
                                var oldDrop = drag.droppableWidget;
                                if ((!usedAnswers_2 || !usedAnswers_2.includes(drag)) && oldDrop !== widget) {
                                    answer_2 = drag;
                                    if (oldDrop) {
                                        drag.droppables.remove(oldDrop);
                                        oldDrop.draggables.remove(drag);
                                        oldDrop.update();
                                    }
                                    drag.droppables.pushOnce(widget);
                                    widget.draggables.pushOnce(drag);
                                    appendChild(widget.element, drag.originalElement);
                                    drag.invalidate();
                                }
                            });
                        }
                        else {
                            var answers_2 = widget.expandedAnswers, drags = widget.get(widget.screenWidget, ic.DraggableWidget, widget.group).filter(function (drag) { return !drag.activityWidget || drag.activityWidget === _this.activityWidget; });
                            answer_2 = drags.not(usedAnswers_2).filter(function (widget) {
                                var value = widget.markable.value;
                                if (isString(value)) {
                                    return answers_2.some(function (answer) { return isString(answer) ? answer === value : answer.test(value); });
                                }
                                if (isArray(value)) {
                                    return value.some(function (val) { return answers_2.some(function (answer) { return isString(answer) ? answer === val : answer.test(val); }); });
                                }
                            })[0];
                            if (answer_2) {
                                if (answer_2.isCloneable) {
                                    var clone = answer_2.originalElement.cloneNode(true);
                                    clone.icWidget = answer_2;
                                    answer_2.droppables.pushOnce(widget);
                                    widget.draggables.pushOnce(answer_2);
                                    appendChild(widget.element, clone);
                                }
                                else {
                                    var oldDrop = answer_2.droppableWidget;
                                    if (oldDrop !== widget) {
                                        if (oldDrop) {
                                            answer_2.droppables.remove(oldDrop);
                                            oldDrop.draggables.remove(answer_2);
                                            oldDrop.update();
                                        }
                                        answer_2.droppables.pushOnce(widget);
                                        widget.draggables.pushOnce(answer_2);
                                        if (widget.pins) {
                                            widget.pins.forEach(function (pin) {
                                                pin.unlinkPin();
                                                pin.update();
                                            });
                                            answer_2.pins[0].linkPin(answer_2, widget, widget.pins[0]);
                                            if (isString(answers_2[0]) && (widget.isHorizontal || widget.isVertical)) {
                                                var el = widget.pins[0].element, pos = answers_2[0].split(":");
                                                if (widget.isVertical) {
                                                    el.style.top = pos.pop() + "%";
                                                }
                                                if (widget.isHorizontal) {
                                                    el.style.left = pos.pop() + "%";
                                                }
                                            }
                                        }
                                        else {
                                            appendChild(widget.element, answer_2.originalElement);
                                            answer_2.invalidate();
                                        }
                                    }
                                }
                            }
                            if (usedAnswers_2) {
                                usedAnswers_2.pushOnce(answer_2);
                            }
                        }
                        widget
                            .toggleState({
                            "empty": !answer_2,
                            "disabled": true,
                            "reveal": true
                        })
                            .update();
                    }
                });
            }
        };
        DroppableWidget.prototype.includes = function (drag) {
            return this.draggables.includes(drag);
        };
        DroppableWidget.prototype.onMouseEnter = function () {
        };
        DroppableWidget.prototype.onMouseLeave = function () {
        };
        DroppableWidget.prototype.onDragHover = function (drag, over, event) {
            var _this = this;
            if (!this.includes(drag) || this.isHorizontal || this.isVertical || this.isSortable) {
                this.toggleState("hover", over);
            }
            if (over && this.isSortable) {
                var droppables = this.get(this.parentWidget, DroppableWidget, this.group, true).filter(function (droppable) { return droppable.isSortable; }), draggables_1 = this.get(this.parentWidget, ic.DraggableWidget, this.group).filter(function (draggable) { return draggable !== drag && draggable.droppableWidget.isSortable; });
                droppables.filter(function (droppable) {
                    var draggable = droppable === _this ? drag : draggables_1.shift();
                    if (droppable.draggables[0] !== draggable) {
                        var oldDrop = draggable.element.closest(DroppableWidget.selector).icWidget;
                        draggable.droppables[0] = droppable;
                        oldDrop.draggables[0] = draggable;
                        appendChild(droppable.element, draggable.element);
                        return true;
                    }
                }).forEach(function (droppable) { return droppable.update(); });
            }
        };
        DroppableWidget.prototype.findPin = function (drag) {
            return this.pins.find(function (pin) { return pin.target && pin.target.parent === drag; });
        };
        DroppableWidget.prototype.getPin = function (event) {
            var _this = this;
            var _a = ic.getCoords(event), clientX = _a[0], clientY = _a[1], available = this.pins.filter(function (pin) { return !pin.target || (!_this.preLoaded.includes(pin.drag) && !pin.drag.hasState("disabled")); }), underMouse = (event && available.filter(function (pin) { return pin.inRect(clientX, clientY) || (pin.target && pin.target.inRect(clientX, clientY)); }));
            if (underMouse && underMouse.length) {
                available = underMouse;
            }
            var index = available.indexOf(this.lastPin) + 1;
            return this.lastPin = available.find(function (pin) { return !pin.target; })
                || available[index >= available.length ? 0 : index];
        };
        DroppableWidget.prototype.canAccept = function (drag) {
            return !this.hasState("disabled") && !this.pins === !drag.pins;
        };
        DroppableWidget.prototype.onDragDrop = function (drag, helper, event) {
            if (this.includes(drag) && !this.isHorizontal && !this.isVertical) {
                return false;
            }
            if (this.pins) {
                var pin = this.findPin(drag) || this.getPin();
                if (this.isHorizontal || this.isVertical) {
                    var el = helper.helperElement, style = pin.element.style, computedStyle = getComputedStyle(this.element, null), box = /^border-box$/i.test(computedStyle.boxSizing), rect = getBoundingClientRect(pin.element.parentElement), oldPos = void 0, width = rect.width - (box ? parseFloat(computedStyle.paddingLeft) + parseFloat(computedStyle.paddingRight) : 0), height = rect.height - (box ? parseFloat(computedStyle.paddingTop) + parseFloat(computedStyle.paddingBottom) : 0);
                    computedStyle = getComputedStyle(pin.element, null);
                    style.position = "absolute";
                    el.offsetHeight;
                    oldPos = getBoundingClientRect(el);
                    if (this.isHorizontal) {
                        style.left = String(Math.range(0, (oldPos.left - rect.left - parseFloat(computedStyle.marginLeft)) * 100 / width, 100)) + "%";
                    }
                    else {
                        style.left = "";
                    }
                    if (this.isVertical) {
                        style.top = String(Math.range(0, (oldPos.top - rect.top - parseFloat(computedStyle.marginTop)) * 100 / height, 100)) + "%";
                    }
                    else {
                        style.top = "";
                    }
                }
                drag.getPin().unlinkPin().linkPin(drag, this);
            }
            else {
                if (this.isColour) {
                    if (this.draggables[0] === drag) {
                        this.draggables.pop();
                    }
                    else {
                        this.draggables[0] = drag;
                    }
                }
                else {
                    var isCloneable = drag.isCloneable, el = isCloneable ? drag.cloneElement : drag.originalElement, oldPos = getBoundingClientRect(el), style = el.style, oldDrop = el.closest(DroppableWidget.selector).icWidget, oldDropIsOriginal = !isCloneable || drag.originalElement.parentElement === oldDrop.element, _a = ic.getCoords(event), clientX_1 = _a[0], clientY_1 = _a[1], children = this.get(this, ic.DraggableWidget), swap = !this.isHorizontal && !this.isVertical
                        ? (this.count > 1
                            ? children.find(function (drag) { return drag.inRect(clientX_1, clientY_1); })
                            : children[0])
                        : null;
                    if (!isCloneable || !oldDropIsOriginal) {
                        drag.droppables.remove(oldDrop);
                        oldDrop.draggables.remove(drag);
                    }
                    drag.droppables.pushOnce(this);
                    this.draggables.pushOnce(drag);
                    style.top = style.left = "";
                    if (swap) {
                        var target = swap.element;
                        if (!isCloneable || (!oldDropIsOriginal && target !== swap.originalElement)) {
                            var nextSibling = el.nextSibling;
                            this.element.insertBefore(el, target.nextSibling);
                            oldDrop.element.insertBefore(target, nextSibling);
                            target.style.top = target.style.left = "";
                            this.draggables.remove(swap);
                            swap.droppables.remove(this);
                            swap.droppables.pushOnce(oldDrop);
                            oldDrop.draggables.pushOnce(swap);
                        }
                        else if (oldDropIsOriginal && target !== swap.originalElement) {
                            this.element.insertBefore(el, target);
                            target.parentElement.removeChild(target);
                            this.draggables.remove(swap);
                            swap.droppables.remove(this);
                        }
                        else if (!oldDropIsOriginal && target === swap.originalElement) {
                            this.draggables.remove(drag);
                            drag.droppables.remove(this);
                            el.parentElement.removeChild(el);
                            el = null;
                        }
                    }
                    else {
                        appendChild(this.element, el);
                    }
                    if (isCloneable && this.group) {
                        this.get(this.parentWidget, DroppableWidget, this.group, false).forEach(function (droppable) {
                            if (droppable.includes(drag)) {
                                [].forEach.call(droppable.element.children, function (el) {
                                    if (el.icWidget === drag) {
                                        el.parentElement.removeChild(el);
                                    }
                                });
                                drag.droppables.remove(droppable);
                                droppable.draggables.remove(drag);
                                droppable.update();
                            }
                        });
                    }
                    if (el) {
                        el.offsetHeight;
                        var newPos = getBoundingClientRect(el);
                        if (this.isHorizontal || this.isVertical) {
                            var percent = void 0, computedStyle = getComputedStyle(this.element, null), box = /^border-box$/i.test(computedStyle.boxSizing), rect = getBoundingClientRect(this), width = rect.width - (box ? parseFloat(computedStyle.paddingLeft) + parseFloat(computedStyle.paddingRight) : 0), height = rect.height - (box ? parseFloat(computedStyle.paddingTop) + parseFloat(computedStyle.paddingBottom) : 0);
                            if (this.isHorizontal) {
                                var left = Math.max(newPos.left, oldPos.left) - Math.min(newPos.left, oldPos.left);
                                percent = Math.range(0, left * 100 / (width - newPos.width), 100);
                                style.left = String(percent * (width - newPos.width) / width) + "%";
                            }
                            else {
                                style.left = "";
                            }
                            if (this.isVertical) {
                                var top_2 = Math.max(newPos.top, oldPos.top) - Math.min(newPos.top, oldPos.top);
                                percent = Math.range(0, top_2 * 100 / (height - newPos.height), 100);
                                style.top = String(percent * (height - newPos.height) / height) + "%";
                            }
                            else {
                                style.top = "";
                            }
                        }
                        else {
                            style.top = style.left = "";
                        }
                    }
                    oldDrop.update();
                }
            }
            if (event && this.parentWidget) {
                this.parentWidget.addState("attempted");
            }
            this.update();
            this.callUserFunc(this.icOnDropFn, this.markable.value);
            return true;
        };
        DroppableWidget.prototype.onDragRemove = function (drag) {
            this.draggables.remove(drag);
            if (this.pins) {
                var pin = this.pins.find(function (pin) { return pin.drag === drag; });
                if (pin) {
                    pin.unlinkPin();
                }
            }
            this.update();
        };
        DroppableWidget.prototype.onDragEnd = function (drag, event, drop) {
            this.toggleState({
                "hover": false,
                "empty": !this.draggables.length
            });
            if (this.isSortable && !drop) {
                var draggables_2 = [], droppables = this.get(this.parentWidget, DroppableWidget, this.group), index_2 = 0, foundZero_1;
                droppables.forEach(function (droppable, index) {
                    if (droppable.isSortable) {
                        var drags = droppable.draggables;
                        if (!drags.length) {
                            foundZero_1 = true;
                        }
                        else if (drags.length === 2 && foundZero_1) {
                            drags.reverse();
                        }
                        draggables_2.pushOnce.apply(draggables_2, drags);
                    }
                });
                droppables.forEach(function (droppable) {
                    if (droppable.isSortable) {
                        var draggable = draggables_2[index_2++];
                        if (!droppable.draggables.includes(draggable)) {
                            var oldDrop = draggable.element.closest(DroppableWidget.selector).icWidget;
                            oldDrop.draggables.remove(draggable);
                            droppable.draggables.pushOnce(draggable);
                            appendChild(droppable.element, draggable.element);
                            droppable.update();
                        }
                    }
                });
            }
        };
        DroppableWidget.prototype.update = function () {
            var _this = this;
            var mark = this.markable;
            if (this.draggables.length) {
                var computedStyle_1, width_1, height_1;
                if (this.isHorizontal || this.isVertical) {
                    var el = this.element, style = computedStyle_1 = getComputedStyle(el, null), borderBox = /^border-box$/i.test(style.boxSizing);
                    width_1 = el.clientWidth - (borderBox ? parseFloat(style.paddingLeft) + parseFloat(style.paddingRight) : 0);
                    height_1 = el.clientHeight - (borderBox ? parseFloat(style.paddingTop) + parseFloat(style.paddingBottom) : 0);
                }
                mark.value = undefined;
                this.draggables.forEach(function (drag) {
                    if (!drag.hasState("disabled")) {
                        var value = "", el = _this.pins ? (_this.findPin(drag) || {}).element : drag.element, rect = !_this.pins && computedStyle_1 ? getBoundingClientRect(el) : null;
                        if (!mark.value) {
                            mark.value = [];
                        }
                        mark.value.push(drag.markable.value);
                        if (el && (_this.isHorizontal || _this.isVertical)) {
                            if (_this.isHorizontal) {
                                value += (value ? ":" : "") + (_this.pins ? parseFloat(el.style.left) : String((parseFloat(el.style.left) || 0) * width_1 / (width_1 - rect.width)));
                            }
                            if (_this.isVertical) {
                                value += (value ? ":" : "") + (_this.pins ? parseFloat(el.style.top) : String((parseFloat(el.style.top) || 0) * height_1 / (height_1 - rect.height)));
                            }
                            mark.value.push(drag.markable.value + ":" + value, value);
                        }
                    }
                });
                if (this.isColour) {
                    this.index = this.draggables.first().index;
                }
                this.removeState("empty");
            }
            else {
                mark.value = undefined;
                this.index = -1;
                this.addState("empty");
            }
            this.mark();
        };
        DroppableWidget.selector = selector;
        DroppableWidget.isTreeWidget = true;
        return DroppableWidget;
    }(ic.InputWidget));
    ic.DroppableWidget = DroppableWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var selector = "ic-text";
    ic.version[selector] = 1;
    var TextWidget = (function (_super) {
        __extends(TextWidget, _super);
        function TextWidget(element) {
            var _this = _super.call(this, element) || this;
            var i, child, children = element.childNodes, mark = _this.markable;
            mark.max = 1;
            if (isUndefined(mark.points)) {
                mark.points = 1;
            }
            for (i = 0; i < children.length && !_this.input && !_this.textarea; i++) {
                child = children[i];
                if (child.nodeType === Node.ELEMENT_NODE) {
                    switch (child.tagName) {
                        case "INPUT":
                            _this.input = child;
                            break;
                        case "TEXTAREA":
                            _this.textarea = child;
                            break;
                    }
                }
            }
            if (!_this.input && !_this.textarea) {
                for (var parent_4 = _this.element.parentElement; parent_4 && parent_4.icWidget !== _this.parentWidget; parent_4 = parent_4.parentElement) {
                    if (parent_4.tagName === "P") {
                        _this.input = createElement("input");
                        _this.input.setAttribute("type", "text");
                        appendChild(_this.element, _this.input);
                        break;
                    }
                }
                if (!_this.input) {
                    _this.textarea = createElement("textarea");
                    appendChild(_this.element, _this.textarea);
                }
            }
            if (_this.element.hasAttribute("data-accept")) {
                _this.accept = new RegExp(_this.element.getAttribute("data-accept"));
            }
            if (_this.element.hasAttribute("data-rx")) {
                _this.rx = new RegExp(_this.element.getAttribute("data-rx"));
            }
            var input = _this.input ? _this.input : _this.textarea;
            _this.startText = mark.value = input.value;
            _this.toggleState("empty", !mark.value);
            if (input.hasAttribute("disabled")) {
                _this.addState("disabled");
            }
            if (!input.hasAttribute("autocapitalize")) {
                input.setAttribute("autocapitalize", _this.input ? "none" : "sentences");
            }
            if (!input.hasAttribute("autocomplete")) {
                input.setAttribute("autocomplete", "off");
            }
            if (!input.hasAttribute("autocorrect")) {
                input.setAttribute("autocorrect", "off");
            }
            input.addEventListener("change", function (event) {
                _this.onKeyUp(event);
            });
            input.addEventListener("paste", function (event) {
                event.preventDefault();
                return false;
            }, true);
            _this.on(["mousedown", "touchstart", ".persist", ".state", ".reset", ".reveal"])
                .on(["focus", "blur", "keypress", "keyup"], input);
            return _this;
        }
        Object.defineProperty(TextWidget.prototype, "hasAnswered", {
            get: function () {
                return !!this.markable.value;
            },
            enumerable: true,
            configurable: true
        });
        TextWidget.prototype.onPersist = function (state) {
            if (isUndefined(state)) {
                return this.markable.value || "";
            }
            (this.input || this.textarea).value = this.markable.value = state || "";
            this.toggleState("empty", !this.markable.value);
        };
        TextWidget.prototype.onReset = function (screen) {
            if ((!screen || this.screenWidget === screen)) {
                var input = this.input || this.textarea;
                _super.prototype.onReset.call(this, screen);
                input.value = this.startText;
                this.mark();
            }
        };
        TextWidget.prototype.onReveal = function (screen) {
            if ((!screen || this.screenWidget === screen) && !this.hasState("reveal")) {
                var usedAnswers_3 = this.activityWidget.allowDuplicates ? undefined : [];
                this.get(this.closestWidget, TextWidget, this.group, true).forEach(function (widget) {
                    if (!widget.hasState("reveal")) {
                        var input = widget.input || widget.textarea, answers = widget.expandedAnswers.not(usedAnswers_3), answer = answers.find(function (answer) { return isString(answer); }) || answers[0] || "";
                        widget.markable.value = input.value = isString(answer) ? answer : answer.source;
                        usedAnswers_3.pushOnce(answer);
                        widget.addState(["disabled", "reveal"])
                            .mark();
                    }
                });
            }
        };
        TextWidget.prototype.onMouseDown = function (event) {
            if (!this.isDisabled(event)
                && document.activeElement !== (this.input || this.textarea)) {
                this.activate();
                (this.input || this.textarea).focus();
                event.preventDefault();
            }
        };
        TextWidget.prototype.onKeyPress = function (event) {
            if (this.accept || this.rx) {
                var key = event.key, input = event.target, value = input.value;
                if (key[0].toLowerCase() === key[0]
                    && ((this.accept && !this.accept.test(key))
                        || (this.rx && !this.rx.test(value.substring(0, input.selectionStart) + key + value.substring(input.selectionEnd))))) {
                    event.preventDefault();
                    return false;
                }
            }
        };
        TextWidget.prototype.onKeyUp = function (event) {
            var value = (this.input || this.textarea).value.trim();
            if (this.markable.value !== value) {
                this.markable.value = value;
                this.toggleState("empty", !value);
                if (this.parentWidget) {
                    this.parentWidget.addState("attempted");
                }
                this.mark();
            }
        };
        TextWidget.prototype.onBlur = function () {
            this.removeState("focus");
        };
        TextWidget.prototype.onFocus = function () {
            this.addState(["active", "focus"])
                .parentWidget.addState("active");
        };
        TextWidget.prototype.onState = function (widget, stateList) {
            if (widget === this && stateList.includes("disabled")) {
                (this.input || this.textarea).disabled = this.hasState("disabled");
            }
        };
        TextWidget.selector = selector;
        return TextWidget;
    }(ic.InputWidget));
    ic.TextWidget = TextWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var selector = "ic-nav";
    ic.version[selector] = 1;
    var NavWidget = (function (_super) {
        __extends(NavWidget, _super);
        function NavWidget() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.anchors = [];
            return _this;
        }
        NavWidget.prototype.startup = function () {
            var _this = this;
            _super.prototype.startup.call(this);
            this.get(ic.AnchorWidget).forEach(function (anchor) {
                var span = _this.anchors[anchor.index] = createElement("span");
                span.setAttribute(ic.AnchorWidget.selector, anchor.element.getAttribute(ic.AnchorWidget.selector) || String(anchor.index));
                span.icAnchor = anchor;
                appendChild(_this.element, span);
                _this.onState(anchor, []);
            });
            this.on(["click", ".resize", ".state"]);
            ic.setImmediate(NavWidget.update);
        };
        NavWidget.prototype.onClick = function (event) {
            var target = event.target;
            if (!this.isDisabled(event) && target.icAnchor) {
                target.icAnchor.activate();
            }
        };
        NavWidget.prototype.onState = function (widget, stateList) {
            if (widget instanceof ic.AnchorWidget && stateList.includes("active")) {
                ic.setImmediate(NavWidget.update);
            }
        };
        NavWidget.prototype.onResize = function () {
            ic.setImmediate(NavWidget.update);
        };
        NavWidget.selector = selector;
        NavWidget.update = function () {
            ic.Widget.root.get(NavWidget).forEach(function (nav) {
                nav.anchors.forEach(function (span) {
                    var anchor = span.icAnchor;
                    Object.keys(anchor.state).forEach(function (state) {
                        if (anchor.state[state]) {
                            span.classList.add(state);
                            if (state === "active") {
                                var navRect = getBoundingClientRect(nav), rect = getBoundingClientRect(span);
                                if (rect.width) {
                                    nav.element.scrollLeft = Math.floor(nav.element.scrollLeft + rect.left - navRect.left - (navRect.width - rect.width) / 2);
                                }
                            }
                        }
                        else {
                            span.classList.remove(state);
                        }
                    });
                });
                ic.Widget.trigger(".scroll", nav);
            });
        };
        return NavWidget;
    }(ic.Widget));
    ic.NavWidget = NavWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var selector = "ic-pin";
    ic.version[selector] = 1;
    var PinWidget = (function (_super) {
        __extends(PinWidget, _super);
        function PinWidget(element) {
            var _this = _super.call(this, element) || this;
            _this.xmlns = "http://www.w3.org/2000/svg";
            _this.internalUpdate = function () {
                var target = _this.helperElement || (_this.target && _this.target.element);
                if (target) {
                    if (_this.isSource || _this.helperElement) {
                        var line = _this.line, drag = _this.drag, drop = _this.drop;
                        if (!line) {
                            if (!_this.helperElement && drag && drop) {
                                line = querySelectorSVG(_this.svg, "[ic-line='" + (drag && drag.activitiesWidget ? (drag.activityWidget || drag.activitiesWidget).realIndex + "_" : "") + drag.index + "_" + drop.realIndex + "']");
                            }
                            if (!line) {
                                if (_this.rootsvg) {
                                    line = querySelectorSVG(_this.rootsvg, "defs line");
                                }
                                if (line) {
                                    line = line.cloneNode();
                                }
                                else {
                                    line = document.createElementNS(_this.xmlns, "line");
                                }
                                appendChild(_this.svg, _this.line = line);
                            }
                        }
                        var box = getBoundingClientRect(_this.svg, true), src = getBoundingClientRect(_this.element, true), dest = getBoundingClientRect(target, true), viewBox = (_this.svg.getAttribute("viewBox") || "").regex(/(\d+)\s+(\d+)\s+(\d+)\s+(\d+)/), width = viewBox ? (viewBox[2] - viewBox[0]) / box.width : 1, height = viewBox ? (viewBox[3] - viewBox[1]) / box.height : 1;
                        if (_this.lineDisabled || (_this.target && _this.target.lineDisabled)) {
                            line.setAttribute("ic-disabled", "");
                        }
                        if (!_this.helperElement && drag && drop) {
                            line.setAttribute("ic-line", (drag && drag.activitiesWidget ? (drag.activityWidget || drag.activitiesWidget).realIndex + "_" : "") + drag.index + "_" + drop.realIndex);
                        }
                        if (src.left && dest.left) {
                            line.setAttribute("x1", String((src.width / 2 + src.left - box.left) * width));
                            line.setAttribute("x2", String((dest.width / 2 + dest.left - box.left) * height));
                            line.setAttribute("y1", String((src.height / 2 + src.top - box.top) * width));
                            line.setAttribute("y2", String((dest.height / 2 + dest.top - box.top) * height));
                        }
                        if (!viewBox) {
                            _this.on(".resize");
                        }
                    }
                    _this.removeState("hidden");
                }
                else {
                    if (!(_this.parent instanceof ic.DraggableWidget)) {
                        _this.addState("hidden");
                    }
                    if (_this.line) {
                        _this.line.parentElement.removeChild(_this.line);
                        _this.line = undefined;
                        _this.off(".resize");
                    }
                }
            };
            for (var parent_5 = element.parentElement; parent_5; parent_5 = parent_5.parentElement) {
                var icWidget = parent_5.icWidget;
                if (icWidget instanceof ic.DraggableWidget || icWidget instanceof ic.DroppableWidget) {
                    _this.parent = icWidget;
                    _this.on(["mousedown", "mouseup", ".screen"]);
                    break;
                }
            }
            var screen = _this.screenWidget.element, rootsvg = querySelectorSVG(_this.rootWidget.element, ":scope>svg.ic-line"), svg = querySelectorSVG(screen, ":scope>svg.ic-line");
            if (rootsvg) {
                _this.rootsvg = rootsvg;
            }
            if (!svg) {
                if (rootsvg) {
                    svg = rootsvg.cloneNode(false);
                }
                else {
                    svg = document.createElementNS(_this.xmlns, "svg");
                    svg.setAttribute("xmlns", _this.xmlns);
                    svg.setAttribute("ic-line", "");
                    svg.setAttribute("class", "ic-line");
                }
                screen.insertBefore(svg, screen.firstChild);
            }
            _this.svg = svg;
            if (_this.parent && /disabled/i.test(_this.parent.element.getAttribute("ic-state"))) {
                _this.lineDisabled = true;
            }
            if (!(_this.parent instanceof ic.DraggableWidget)) {
                _this.addState("hidden");
            }
            _this.on(".css");
            return _this;
        }
        PinWidget.prototype.getTarget = function (event) {
            if (this.parent instanceof ic.DraggableWidget) {
                return this.parent;
            }
            else if (this.drag && !this.isSource) {
                return this.drag;
            }
            else if (/^absolute$/i.test(getComputedStyle(this.element).position)) {
                var i = void 0, el = void 0, widget = void 0, children = this.element.parentElement.children, _a = ic.getCoords(event), clientX = _a[0], clientY = _a[1];
                for (i = 0; i < children.length; i++) {
                    el = children[i];
                    widget = el.icWidget;
                    if (widget instanceof PinWidget && widget.inRect(clientX, clientY) && widget.drag && !widget.isSource) {
                        return widget.drag;
                    }
                }
            }
        };
        PinWidget.prototype.onMouseDown = function (event) {
            var target = this.getTarget(event);
            if (target) {
                target.onMouseDown(event);
            }
        };
        PinWidget.prototype.onMouseUp = function (event) {
            var target = this.getTarget(event);
            if (target) {
                target.onMouseUp(event);
            }
        };
        PinWidget.prototype.getHelper = function (event, data, within) {
            var helper = this.helperElement = createElement("ic-pin"), rect = getBoundingClientRect(this);
            helper.className = "drag-helper ict-state-dragging " + this.element.className;
            appendChild(this.element, helper);
            if (this.target) {
                this.target.addState("hidden");
            }
            data.offsetY = rect.height / 2;
            data.offsetX = rect.width / 2;
            data.height = rect.height;
            data.width = rect.width;
            this.update();
            return helper;
        };
        PinWidget.prototype.onHelperMove = function (event, data, within) {
            var style = this.helperElement.style, _a = ic.getCoords(event), clientX = _a[0], clientY = _a[1], left = Math.range(within.left - data.left, clientX - data.offsetX, (within.right - data.width - data.left) / ic.scaleFactor), top = Math.range(within.top - data.top, clientY - data.offsetY, (within.bottom - data.height - data.top) / ic.scaleFactor);
            style.top = top + "px";
            style.left = left + "px";
            this.update();
        };
        PinWidget.prototype.onScreen = function (screen) {
            if (this.screenWidget === screen && this.target) {
                this.update();
            }
        };
        PinWidget.prototype.freeHelper = function (event, drag, drop) {
            var target = this.target;
            this.helperElement.remove();
            if (target) {
                target.removeState("hidden");
            }
            this.helperElement = undefined;
            this.update();
        };
        PinWidget.prototype.linkPin = function (drag, drop, pin) {
            if (drag === this.parent) {
                if (!pin) {
                    pin = drop.getPin();
                }
                if (pin) {
                    this.unlinkPin();
                    pin.unlinkPin();
                    drop.draggables.pushOnce(drag);
                    drag.droppables.pushOnce(drop);
                    this.drag = pin.drag = drag;
                    this.drop = pin.drop = drop;
                    pin.isSource = false;
                    pin.target = this;
                    this.isSource = true;
                    this.target = pin;
                    this.update();
                    pin.update();
                }
            }
            return this;
        };
        PinWidget.prototype.unlinkPin = function () {
            if (this.target && (this.drag || this.drop)) {
                var pin = this.target;
                if (this.isSource) {
                    if (this.drop) {
                        this.drop.draggables.remove(this.drag);
                        this.drag.droppables.remove(this.drop);
                        this.drop.update();
                    }
                    this.drag = this.drop = this.target = pin.drag = pin.drop = pin.target = this.isSource = pin.isSource = null;
                    this.update();
                    pin.update();
                }
                else {
                    pin.unlinkPin();
                }
            }
            return this;
        };
        PinWidget.prototype.onCss = function () {
            this.update();
        };
        PinWidget.prototype.onResize = function () {
            this.update();
        };
        PinWidget.prototype.update = function () {
            ic.setImmediate(this.internalUpdate);
        };
        PinWidget.selector = selector;
        return PinWidget;
    }(ic.Widget));
    ic.PinWidget = PinWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var selector = "ic-pins";
    ic.version[selector] = 1;
    var PinsWidget = (function (_super) {
        __extends(PinsWidget, _super);
        function PinsWidget() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        PinsWidget.selector = selector;
        return PinsWidget;
    }(ic.Widget));
    ic.PinsWidget = PinsWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var selector = "ic-question";
    ic.version[selector] = 1;
    var QuestionWidget = (function (_super) {
        __extends(QuestionWidget, _super);
        function QuestionWidget() {
            return _super !== null && _super.apply(this, arguments) || this;
        }
        QuestionWidget.selector = selector;
        return QuestionWidget;
    }(ic.Widget));
    ic.QuestionWidget = QuestionWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var selector = "ic-screen";
    ic.version[selector] = 1;
    var ScreenWidget = (function (_super) {
        __extends(ScreenWidget, _super);
        function ScreenWidget(element) {
            var _this = _super.call(this, element) || this;
            _this.dom = document.createDocumentFragment();
            _this.attempts = 0;
            _this.loadImage = function (img) {
                var src = img.getAttribute("data-src");
                if (!img.icOriginalImage) {
                    var image_1 = img.icOriginalImage = new Image();
                    image_1.icOriginalImage = img;
                    image_1.onload = function () {
                        img.src = image_1.src;
                        img.removeAttribute("data-src");
                        if (image_1.srcset || image_1.sizes) {
                            img.srcset = image_1.srcset;
                            img.sizes = image_1.sizes;
                            img.removeAttribute("data-srcset");
                            img.removeAttribute("data-sizes");
                        }
                        delete img.icOriginalImage;
                        if (!--_this.loadCount && _this.reattach) {
                            _this.attach();
                        }
                    };
                    image_1.onerror = function () {
                        setTimeout(function () {
                            image_1.src = "";
                            image_1.src = src;
                        }, ic.ScreensWidget.trickle);
                    };
                    image_1.src = src;
                    image_1.srcset = img.getAttribute("data-srcset") || "";
                    image_1.sizes = img.getAttribute("data-sizes") || "";
                }
            };
            _this.detach = function () {
                if (ic.Widget.started && !_this.detached && _this.mode !== 2) {
                    var element = _this.element;
                    while (element.firstChild) {
                        _this.dom.appendChild(element.firstChild);
                    }
                    _this.detached = true;
                    _this.reattach = false;
                }
            };
            _this.attach = function () {
                if (ic.Widget.started && _this.detached && _this.mode !== 2) {
                    if (_this.loadCount) {
                        _this.reattach = true;
                        _this.get(ic.ButtonWidget, ic.InputWidget, ic.TimerWidget).forEach(function (widget) {
                            if (!widget.hasState("disabled")) {
                                widget.lazy_pause = true;
                                widget.addState("disabled");
                            }
                        });
                    }
                    else {
                        var dom = _this.dom;
                        while (dom.firstChild) {
                            _this.element.appendChild(dom.firstChild);
                        }
                        _this.get(ic.ButtonWidget, ic.InputWidget, ic.TimerWidget, "disabled").forEach(function (widget) {
                            if (widget.lazy_pause) {
                                delete widget.lazy_pause;
                                widget.removeState("disabled");
                            }
                        });
                        _this.reattach = _this.detached = false;
                    }
                }
            };
            switch (element.getAttribute(selector)) {
                case "once":
                    _this.mode = 1;
                    break;
                case "always":
                    _this.mode = 2;
                    break;
            }
            if (!_this.hasState("active")) {
                element.style.display = "none";
            }
            _this.on([".persist", ".state", ".screen", ".timeout"]);
            return _this;
        }
        ScreenWidget.prototype.loadImages = function (force) {
            if (!this.loaded) {
                var images = (this.element.firstChild ? this.element : this.dom).querySelectorAll("img[data-src]");
                this.loadCount = images.length;
                if (images[0]) {
                    if (force) {
                        [].forEach.call(images, this.loadImage);
                    }
                    else {
                        this.loadImage(images[0]);
                    }
                    return false;
                }
                this.loaded = true;
            }
            return true;
        };
        ScreenWidget.prototype.onPersist = function (state) {
            return this.persistTree({
                "activity": ic.ActivityWidget
            }, state);
        };
        ScreenWidget.prototype.onState = function (widget, stateList) {
            var _this = this;
            if (this === widget) {
                if (stateList.includes("active")) {
                    if (this.hasState("active")) {
                        this.element.style.display = "";
                        this.rootWidget.screen = this.index - 1;
                        if (!this.seen) {
                            this.seen = true;
                            var top_3 = querySelector(this.element, "a[name='#top']"), scroll_1 = closestScroll(top_3);
                            if (top_3 && scroll_1) {
                                scroll_1.scrollTop = getBoundingClientRect(top_3).top - getBoundingClientRect(scroll_1).top;
                            }
                        }
                        if (!this.get(this, ic.ActivityWidget, "active", false).length) {
                            this.get(this, ic.ActivityWidget).some(function (widget) {
                                if (widget.constructor === ic.ActivityWidget) {
                                    widget.activate();
                                    return true;
                                }
                            });
                        }
                    }
                    else {
                        this.element.style.display = "none";
                        this.get(this, "active").forEach(function (widget) {
                            widget.removeState("active");
                        });
                        if (this.mode === 1) {
                            this.addState("disabled");
                        }
                    }
                }
            }
            else if (widget.screenWidget === this && widget instanceof ic.ActivityWidget) {
                stateList.intersect(["visible", "visited", "attempted"]).forEach(function (state) {
                    if (widget.hasState(state)) {
                        _this.addState(state);
                    }
                });
            }
        };
        ScreenWidget.prototype.onScreen = function (screen) {
        };
        ScreenWidget.prototype.onTimeout = function () {
            console.log("timeout", this.get(ScreenWidget), this.get(this, ic.ActivityWidget));
            if (this.index >= this.get(ScreenWidget).length && !this.get(this, ic.ActivityWidget).length) {
                this.addState("active");
            }
        };
        ScreenWidget.selector = selector;
        ScreenWidget.isInputWidget = true;
        ScreenWidget.isTreeWidget = true;
        __decorate([
            persist
        ], ScreenWidget.prototype, "attempts", void 0);
        return ScreenWidget;
    }(ic.Widget));
    ic.ScreenWidget = ScreenWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var selector = "ic-screens";
    ic.version[selector] = 1;
    function preventDefault(event) {
        try {
            event.preventDefault();
            event.stopImmediatePropagation();
        }
        catch (e) { }
    }
    var ScreensWidget = (function (_super) {
        __extends(ScreensWidget, _super);
        function ScreensWidget(element) {
            var _this = _super.call(this, element) || this;
            _this.attempts = 0;
            _this.loadImages = function () {
                if (!_this.loaded) {
                    var screenIndex = _this.screen, index = screenIndex, screenList = _this.get(ic.ScreenWidget), screenListLength = screenList.length;
                    while (index >= 0) {
                        if (!screenList[index].loadImages(index === screenIndex)) {
                            _this.loadTimer = setTimeout(_this.loadImages, ScreensWidget.trickle);
                            return;
                        }
                        if (index < screenIndex) {
                            index--;
                        }
                        else if (++index >= screenListLength) {
                            index = screenIndex - 1;
                        }
                    }
                    _this.loadTimer = null;
                    _this.loaded = true;
                }
            };
            element.addEventListener("dragstart", preventDefault, true);
            return _this;
        }
        Object.defineProperty(ScreensWidget, "trickle", {
            get: function () {
                return 1000 + Math.random() * 2000;
            },
            enumerable: true,
            configurable: true
        });
        ScreensWidget.prototype.startup = function () {
            _super.prototype.startup.call(this);
            var screens = this.get(ic.ScreenWidget);
            ic.Widget.root = this;
            ScreensWidget.screenLength = screens.length;
            this.screen = screens.length ? screens.first().index - 1 : 0;
            this.on(".persist");
        };
        ScreensWidget.prototype.onPersist = function (state) {
            var result = this.persistTree({
                "screen": ic.ScreenWidget,
                "timer": ic.TimerWidget
            }, state);
            if (state) {
                ic.setImmediate(ic.NavWidget.update);
            }
            if (isUndefined(state)) {
                return result;
            }
        };
        Object.defineProperty(ScreensWidget.prototype, "screen", {
            get: function () {
                return ScreensWidget.screenIndex;
            },
            set: function (index) {
                if (index !== ScreensWidget.screenIndex && index >= 0 && index < ScreensWidget.screenLength) {
                    var event_1 = document.createEvent("Event"), oldScreen = ScreensWidget.screenIndex, oldElement = (ScreensWidget.screenWidget || {}).element, screenList = this.get(ic.ScreenWidget), newScreen = ScreensWidget.screenWidget = screenList[ScreensWidget.screenIndex = index];
                    if (!this.loaded) {
                        if (this.loadTimer) {
                            clearTimeout(this.loadTimer);
                        }
                        this.loadImages();
                    }
                    this.get(ic.ScreenWidget, "active").not(newScreen).forEach(function (screen) {
                        screen.removeState(["active", "hover"]);
                        ic.setImmediate(screen.detach);
                    });
                    newScreen.attach();
                    ic.Widget.trigger(".screen", newScreen.addState(["active", "visited"]));
                    event_1.initEvent("ic-screen", false, false);
                    event_1.icData = {
                        index: index,
                        element: newScreen.element,
                        oldIndex: oldScreen,
                        oldElement: oldElement
                    };
                    document.body.dispatchEvent(event_1);
                }
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ScreensWidget.prototype, "screenWidget", {
            get: function () {
                return ScreensWidget.screenWidget;
            },
            enumerable: true,
            configurable: true
        });
        Object.defineProperty(ScreensWidget.prototype, "length", {
            get: function () {
                return ScreensWidget.screenLength;
            },
            enumerable: true,
            configurable: true
        });
        ScreensWidget.selector = selector;
        ScreensWidget.isInputWidget = true;
        ScreensWidget.isTreeWidget = true;
        ScreensWidget.screenLength = 0;
        __decorate([
            persist
        ], ScreensWidget.prototype, "attempts", void 0);
        __decorate([
            persist
        ], ScreensWidget.prototype, "screen", null);
        return ScreensWidget;
    }(ic.Widget));
    ic.ScreensWidget = ScreensWidget;
})(ic || (ic = {}));
;
var ic;
(function (ic) {
    var selector = "ic-timer";
    ic.version[selector] = 1;
    var TimerWidget = (function (_super) {
        __extends(TimerWidget, _super);
        function TimerWidget(element) {
            var _this = _super.call(this, element) || this;
            _this.format = element.getAttribute(_this.selector) || "H:i:s";
            if (_this.rootWidget.get(ic.ActivityWidget).filter(function (activity) { return activity.hasState("active"); }).length) {
                _this.start = new Date().getTime();
            }
            else {
                _this.on(".state");
            }
            _this.minutes = (parseFloat(_this.data.minutes) || 0) * 60000;
            _this.warning = (parseFloat(_this.data.warning) || 0) * 60000;
            _this.onTick();
            _this.on([".persist", ic.inEditor ? null : ".tick"]);
            return _this;
        }
        TimerWidget.prototype.startup = function () {
            var _this = this;
            _super.prototype.startup.call(this);
            this.rootWidget.get(TimerWidget).some(function (timer, index) {
                if (_this === timer) {
                    _this._i = index;
                    return true;
                }
            });
        };
        TimerWidget.prototype.onPersist = function (state) {
            if (isUndefined(state)) {
                return this.start ? Math.range(0, new Date().getTime() - this.start, this.minutes) : this.used;
            }
            if (this.rootWidget.get(ic.ActivityWidget).find(function (activity) { return activity.hasState("active"); })) {
                this.start = new Date().getTime() - state;
            }
            else {
                this.used = state;
                this.on(".state");
            }
        };
        TimerWidget.prototype.onTick = function () {
            var elapsed = this.start ? new Date().getTime() - this.start : (this.used || 0), max = this.minutes;
            if (max) {
                if (this.start) {
                    elapsed = Math.max(0, max - elapsed);
                    if (this.warning >= elapsed) {
                        this.addState("warning");
                    }
                    if (!elapsed && !this.hasState("disabled")) {
                        this.addState("disabled");
                        ic.Widget.trigger(".timeout");
                        var event_2 = document.createEvent("Event");
                        event_2.initEvent("ic-timeout", false, false);
                        document.body.dispatchEvent(event_2);
                    }
                }
                else {
                    elapsed = max - (this.used || 0);
                }
            }
            this.element.textContent = new Date(Math.max(0, elapsed)).format(this.format);
        };
        TimerWidget.prototype.onState = function (widget, stateList) {
            if (widget === this && stateList.includes("disabled")) {
                if (this.hasState("disabled")) {
                    if (this.start) {
                        this.used = new Date().getTime() - this.start;
                    }
                    this.start = 0;
                }
                else {
                    this.start = new Date().getTime() - (this.used || 0);
                }
            }
            else if (!this.start && widget instanceof ic.ActivityWidget && !this.hasState("disabled") && widget.hasState("active")) {
                this.start = new Date().getTime() - (this.used || 0);
            }
        };
        TimerWidget.selector = selector;
        return TimerWidget;
    }(ic.Widget));
    ic.TimerWidget = TimerWidget;
})(ic || (ic = {}));
;
;
(function ($) {
    $.fn.extend({
        simulate: function (type, options) {
            return this.each(function () {
                var opt = $.extend({}, $.simulate.defaults, options || {});
                new $.simulate(this, type, opt);
            });
        }
    });
    $.simulate = function (el, type, options) {
        this.target = el;
        this.options = options;
        if (/^drag$/.test(type)) {
            this[type].apply(this, [this.target, options]);
        }
        else {
            this.simulateEvent(el, type, options);
        }
    };
    $.extend($.simulate.prototype, {
        simulateEvent: function (el, type, options) {
            var evt = this.createEvent(type, options);
            this.dispatchEvent(el, type, evt, options);
            return evt;
        },
        createEvent: function (type, options) {
            if (/^mouse(over|out|down|up|move|enter|leave)|(dbl)?click$/.test(type)) {
                return this.mouseEvent(type, options);
            }
            else if (/^key(up|down|press)$/.test(type)) {
                return this.keyboardEvent(type, options);
            }
        },
        mouseEvent: function (type, options) {
            var evt;
            var e = $.extend({
                bubbles: true, cancelable: (type != "mousemove"), view: window, detail: 0,
                screenX: 0, screenY: 0, clientX: 0, clientY: 0,
                ctrlKey: false, altKey: false, shiftKey: false, metaKey: false,
                button: 0, relatedTarget: undefined
            }, options);
            var relatedTarget = $(e.relatedTarget)[0];
            if ($.isFunction(document.createEvent)) {
                evt = document.createEvent("MouseEvents");
                evt.initMouseEvent(type, e.bubbles, e.cancelable, e.view, e.detail, e.screenX, e.screenY, e.clientX, e.clientY, e.ctrlKey, e.altKey, e.shiftKey, e.metaKey, e.button, (e.relatedTarget === void 0) ? document.body.parentNode : e.relatedTarget);
            }
            else if (document.createEventObject) {
                evt = document.createEventObject();
                $.extend(evt, e);
                evt.button = { 0: 1, 1: 4, 2: 2 }[evt.button] || evt.button;
            }
            return evt;
        },
        keyboardEvent: function (type, options) {
            var evt;
            var e = $.extend({ bubbles: true, cancelable: true, view: window,
                ctrlKey: false, altKey: false, shiftKey: false, metaKey: false, altGraphKey: false,
                keyCode: 0, charCode: 0, keyIdentifier: "", keyLocation: 0
            }, options);
            if ($.isFunction(document.createEvent)) {
                try {
                    try {
                        evt = document.createEvent("KeyEvents");
                        evt.initKeyEvent(type, e.bubbles, e.cancelable, e.view, e.ctrlKey, e.altKey, e.shiftKey, e.metaKey, e.keyCode, e.charCode);
                    }
                    catch (err) {
                        evt = document.createEvent("KeyboardEvent");
                        evt.initKeyboardEvent(type, e.bubbles, e.cancelable, e.view, e.keyIdentifier, e.keyLocation, e.ctrlKey, e.altKey, e.shiftKey, e.metaKey, e.altGraphKey);
                        $.extend(evt, { keyCode: e.keyCode, charCode: e.charCode });
                    }
                }
                catch (err) {
                    evt = document.createEvent("Events");
                    evt.initEvent(type, e.bubbles, e.cancelable);
                    $.extend(evt, { view: e.view,
                        ctrlKey: e.ctrlKey, altKey: e.altKey, shiftKey: e.shiftKey, metaKey: e.metaKey,
                        keyCode: e.keyCode, charCode: e.charCode
                    });
                }
            }
            else if (document.createEventObject) {
                evt = document.createEventObject();
                $.extend(evt, e);
            }
            if (($.browser !== undefined) && ($.browser.msie || $.browser.opera)) {
                evt.keyCode = (e.charCode > 0) ? e.charCode : e.keyCode;
                evt.charCode = undefined;
            }
            return evt;
        },
        dispatchEvent: function (el, type, evt) {
            if (el.dispatchEvent) {
                el.dispatchEvent(evt);
            }
            else if (el.fireEvent) {
                el.fireEvent('on' + type, evt);
            }
            return evt;
        },
        drag: function (el) {
            var self = this, center = this.findCenter(this.target), options = this.options, x = Math.floor(center.x), y = Math.floor(center.y), dx = options.dx || 0, dy = options.dy || 0, target = this.target;
            var coord = { clientX: x, clientY: y };
            this.simulateEvent(target, "mousedown", coord);
            coord = { clientX: x + 1, clientY: y + 1 };
            this.simulateEvent(document, "mousemove", coord);
            coord = { clientX: x + dx, clientY: y + dy };
            this.simulateEvent(document, "mousemove", coord);
            this.simulateEvent(document, "mousemove", coord);
            this.simulateEvent(target, "mouseup", coord);
        },
        findCenter: function (el) {
            var el = $(this.target), o = el.offset();
            return {
                x: o.left + el.outerWidth() / 2,
                y: o.top + el.outerHeight() / 2
            };
        }
    });
    $.extend($.simulate, {
        defaults: {
            speed: 'sync'
        },
        VK_TAB: 9,
        VK_ENTER: 13,
        VK_ESC: 27,
        VK_PGUP: 33,
        VK_PGDN: 34,
        VK_END: 35,
        VK_HOME: 36,
        VK_LEFT: 37,
        VK_UP: 38,
        VK_RIGHT: 39,
        VK_DOWN: 40
    });
})(jQuery);
var test;
(function (test) {
    function describeQueue(description, specDefinitions) {
        if (!test.describeQueueData) {
            test.describeQueueData = {};
        }
        if (!test.describeQueueData[description]) {
            test.describeQueueData[description] = [];
        }
        test.describeQueueData[description].push(specDefinitions);
    }
    test.describeQueue = describeQueue;
    function startup(document) {
        ic.Widget.startup(document.body);
        test.screensWidget = $("ic-screens")[0].icWidget;
        test.firstScreenIndex = test.screensWidget.get(ic.ActivityWidget).first().element.closest("ic-screen").icWidget.index - 1;
        test.lastScreenIndex = test.screensWidget.get(ic.ActivityWidget).last().element.closest("ic-screen").icWidget.index - 1;
        $("#content>p").click(function () {
            $(this).hide().nextAll().toggle();
        });
        Object.keys(test.describeQueueData).forEach(function (description) {
            describe(description, function () {
                test.describeQueueData[description].forEach(function (fn) {
                    fn();
                });
            });
        });
    }
    test.startup = startup;
})(test || (test = {}));
;
var test;
(function (test) {
    function WidgetNodeTests($widget, widget) {
        describe("WidgetNode tests", function () {
            if (widget[0] instanceof ic.ScreenWidget) {
                it("parent is a screens", function () {
                    expect(widget[0].parentWidget).toBeDefined();
                    expect(widget[0].parentWidget instanceof ic.ScreensWidget).toEqual(true);
                });
            }
            else if (widget[0] instanceof ic.ActivitiesWidget) {
                it("parent is a screen", function () {
                    expect(widget[0].parentWidget).toBeDefined();
                    expect(widget[0].parentWidget instanceof ic.ScreenWidget).toEqual(true);
                });
            }
            else if (widget[0] instanceof ic.ActivityWidget) {
                it("parent is an activities or screen", function () {
                    expect(widget[0].parentWidget).toBeDefined();
                    expect(widget[0].parentWidget instanceof ic.ActivitiesWidget || widget[0].parentWidget instanceof ic.ScreenWidget).toEqual(true);
                });
            }
            else if (!(widget[0] instanceof ic.ScreensWidget)) {
                it("parent is an activity", function () {
                    expect(widget[0].parentWidget).toBeDefined();
                    expect(widget[0].parentWidget instanceof ic.ActivityWidget).toEqual(true);
                });
            }
            if (!(widget[0] instanceof ic.ScreensWidget)) {
                it("root is a screen", function () {
                    expect(widget[0].rootWidget).toBeDefined();
                    expect(widget[0].rootWidget instanceof ic.ScreensWidget).toEqual(true);
                });
            }
        });
    }
    test.WidgetNodeTests = WidgetNodeTests;
})(test || (test = {}));
;
var test;
(function (test) {
    test.describeQueue("Core Widget tests", function () {
        describe("test.html", function () {
            Object.keys(ic).forEach(function (className) {
                var widgetClass = ic[className];
                if (widgetClass.prototype instanceof ic.Widget) {
                    var selector = widgetClass.selector;
                    if (selector) {
                        var count_2 = $(selector).filter(function () {
                            return !this.closest("header");
                        }).length;
                        (count_2 ? it : xit)(className + " has at least a single element", function () {
                            expect(count_2).toBeGreaterThan(0);
                        });
                    }
                }
            });
        });
        describe("State is being set correctly", function () {
            var $screen = $("#ic-screens-1"), screen = $screen[0], widget = screen.icWidget, oldState = widget.state.clone();
            afterEach(function () {
                if (Object.keys(widget.state).length > 0) {
                    widget.state = {};
                    widget.fixState();
                }
            });
            it("starts with no state", function () {
                expect(oldState).toEqual({ active: true });
            });
            it("adding a state works", function () {
                widget.addState("test");
                expect(widget.state["test"]).toEqual(true);
                widget.removeState("test");
            });
            it("adding a state adds a class", function () {
                widget.addState("test");
                expect($screen.hasClass("ict-state-test")).toEqual(true);
                widget.removeState("test");
            });
            it("removing a state works", function () {
                widget.addState("test");
                widget.removeState("test");
                expect(widget.state["test"]).toEqual(false);
            });
            it("removing a state removes a class", function () {
                widget.addState("test");
                widget.removeState("test");
                expect($screen.hasClass("ict-state-test")).toEqual(false);
            });
            it("toggling a state works", function () {
                widget.addState("test");
                widget.toggleState("test");
                expect(widget.state["test"]).toEqual(false);
            });
            it("toggling a state toggles a class", function () {
                widget.addState("test");
                widget.toggleState("test");
                expect($screen.hasClass("ict-state-test")).toEqual(false);
            });
            it("not having an attribute doesn't have a class", function () {
                expect($screen.hasClass("ict-test")).toEqual(false);
            });
            it("having an normal attribute doesn't add a class", function () {
                var oldClass = $screen.attr("class");
                screen.setAttribute("test", "");
                widget.fixState();
                expect($screen.attr("class")).toEqual(oldClass);
                screen.removeAttribute("test");
                widget.fixState();
            });
            it("having an ic-prefixed attribute adds a class", function () {
                screen.setAttribute("ic-test", "");
                widget.fixState();
                expect($screen.hasClass("ict-test")).toEqual(true);
                screen.removeAttribute("ic-test");
                widget.fixState();
            });
            it("having an ic-prefixed attribute value adds a class", function () {
                screen.setAttribute("ic-test", "12345");
                widget.fixState();
                expect($screen.hasClass("ict-test-12345")).toEqual(true);
                screen.removeAttribute("ic-test");
                widget.fixState();
            });
        });
    });
    function WidgetTests($widget, widget) {
        function cleanup() {
            ic.Widget.forEach(function (widget) {
                if (Object.keys(widget.state).length > 0 && widget.element.closest("ic-activity")) {
                    widget.state = {};
                    widget.fixState();
                }
            });
            if ($widget.length > 0) {
                test.screensWidget["screenIndex"] = -1;
                test.screensWidget.screen = $widget.closest("ic-screen")[0].icWidget.index - 1;
            }
        }
        beforeEach(cleanup);
        afterAll(cleanup);
        test.WidgetNodeTests($widget, widget);
        describe("Widget tests", function () {
            it("Widget test requires one or more widgets", function () {
                expect($widget.length).toBeGreaterThan(0);
            });
            it("has the correct classname", function () {
                expect($widget.hasClass(widget[0].selector.replace(/^ic-([^\[,]+).*$/, "ict-$1"))).toEqual(true);
            });
            it("mouse move changes hover state", function () {
                $widget.eq(0).simulate("mouseenter");
                expect(widget[0].hasState("hover")).toEqual(true);
                $widget.eq(0).simulate("mouseleave");
                expect(widget[0].hasState("hover")).toEqual(false);
            });
        });
    }
    test.WidgetTests = WidgetTests;
})(test || (test = {}));
;
var test;
(function (test) {
    var selector = ic.AudioWidget.prototype.selector;
    var $test = $("<ic-screen>\n\t\t<ic-activities>\n\t\t\t<ic-anchor></ic-anchor>\n\n\t\t\t<p>AudioWidget</p>\n\n\t\t\t<ic-activity>\n\t\t\t\t<ic-audio>Audio 0</ic-audio>\n\n\t\t\t\t<ic-audio>Audio 1</ic-audio>\n\t\t\t</ic-activity>\n\t\t</ic-activities>\n\t</ic-screen>").appendTo("ic-screens");
})(test || (test = {}));
;
var test;
(function (test) {
    var selector = ic.BoxWidget.prototype.selector;
    var $test = $("<ic-screen>\n\t\t<ic-activities>\n\t\t\t<ic-anchor></ic-anchor>\n\n\t\t\t<p>BoxWidget</p>\n\n\t\t\t<ic-activity>\n\t\t\t\t<ic-box>Box 0</ic-box>\n\n\t\t\t\t<ic-box>Box 1</ic-box>\n\t\t\t</ic-activity>\n\t\t</ic-activities>\n\t</ic-screen>").appendTo("ic-screens");
    test.describeQueue("BoxWidget", function () {
        var widget = [];
        var $widget = $test.find(selector).each(function (i, element) {
            widget[i] = element.icWidget;
        });
        it("selector is \"ic-box\"", function () {
            expect(selector).toEqual("ic-box");
        });
        test.WidgetTests($widget, widget);
    });
})(test || (test = {}));
;
var test;
(function (test) {
    function InputTests($widget, widget) {
        test.WidgetTests($widget, widget);
        describe("InputWidget tests", function () {
            it("InputWidget test requires one or more widgets", function () {
                expect($widget.length).toBeGreaterThan(0);
            });
            it("has a numeric index", function () {
                expect(widget[0].index).toBeGreaterThan(-1);
            });
        });
    }
    test.InputTests = InputTests;
})(test || (test = {}));
;
var test;
(function (test) {
    var selector = ic.InputButtonWidget.prototype.selector;
    var $test = $("<ic-screen>\n\t\t<ic-activities>\n\t\t\t<ic-anchor></ic-anchor>\n\n\t\t\t<p>InputButtonWidget</p>\n\n\t\t\t<ic-activity>\n\t\t\t\t<ic-button>Button 0</ic-button>\n\n\t\t\t\t<ic-button>Button 1</ic-button>\n\t\t</ic-activities>\n\t</ic-screen>").appendTo("ic-screens");
    function ButtonTests($widget, widget) {
        test.InputTests($widget, widget);
        describe("InputButtonWidget tests", function () {
            it("InputButtonWidget test requires two or more widgets", function () {
                expect($widget.length).toBeGreaterThan(1);
            });
            it("mouse click sets click state", function () {
                $widget.eq(0).simulate("mousedown");
                expect(widget[0].hasState("click")).toEqual(true);
                $widget.eq(0).simulate("mouseup");
                expect(widget[0].hasState("click")).toEqual(false);
            });
            it("mouse click when disabled does nothing", function () {
                widget[0].addState("disabled");
                $widget.eq(0).simulate("mousedown");
                expect(widget[0].hasState("click")).toEqual(false);
                $widget.eq(0).simulate("mouseup");
                expect(widget[0].hasState("click")).toEqual(false);
            });
            it("mouse move after click changes click & hover state", function () {
                $widget.eq(0).simulate("mousedown").simulate("mouseenter");
                expect(widget[0].hasState("hover")).toEqual(true);
                expect(widget[0].hasState("click")).toEqual(true);
                $widget.eq(0).simulate("mouseleave");
                expect(widget[0].hasState("hover")).toEqual(false);
                expect(widget[0].hasState("click")).toEqual(false);
                $widget.eq(0).simulate("mouseenter");
                expect(widget[0].hasState("hover")).toEqual(true);
                expect(widget[0].hasState("click")).toEqual(true);
                $widget.eq(0).simulate("mouseup");
            });
            it("mouse move after click when disabled does nothing", function () {
                widget[0].addState("disabled");
                $widget.eq(0).simulate("mousedown").simulate("mouseenter");
                expect(widget[0].hasState("hover")).toEqual(false);
                expect(widget[0].hasState("click")).toEqual(false);
                $widget.eq(0).simulate("mouseleave");
                expect(widget[0].hasState("hover")).toEqual(false);
                expect(widget[0].hasState("click")).toEqual(false);
                $widget.eq(0).simulate("mouseenter");
                expect(widget[0].hasState("hover")).toEqual(false);
                expect(widget[0].hasState("click")).toEqual(false);
                $widget.eq(0).simulate("mouseup");
            });
            it("mouse move after click doesn't change sibling click & hover state", function () {
                $widget.eq(1).simulate("mousedown");
                $widget.eq(0).simulate("mouseenter");
                expect(widget[0].hasState("hover")).toEqual(false);
                expect(widget[0].hasState("click")).toEqual(false);
                $widget.eq(0).simulate("mouseleave");
                expect(widget[0].hasState("hover")).toEqual(false);
                expect(widget[0].hasState("click")).toEqual(false);
                $widget.eq(0).simulate("mouseenter");
                expect(widget[0].hasState("hover")).toEqual(false);
                expect(widget[0].hasState("click")).toEqual(false);
                $widget.eq(1).simulate("mouseup");
            });
        });
    }
    test.ButtonTests = ButtonTests;
    test.describeQueue("InputButtonWidget", function () {
        var widget = [];
        var $widget = $test.find(selector).each(function (i, element) {
            widget[i] = element.icWidget;
        });
        it("selector is \"ic-button\"", function () {
            expect(selector).toEqual("ic-button");
        });
        ButtonTests($widget, widget);
    });
})(test || (test = {}));
;
var test;
(function (test) {
    var selector = ic.FirstButtonWidget.prototype.selector;
    var $test = $("<ic-screen>\n\t\t<ic-activities>\n\t\t\t<ic-anchor></ic-anchor>\n\n\t\t\t<p>FirstButtonWidget</p>\n\n\t\t\t<ic-activity>\n\t\t\t\t<ic-button ic-button=\"first\">Button 0</ic-button>\n\n\t\t\t\t<ic-button ic-button=\"first\">Button 1</ic-button>\n\n\t\t\t\t<ic-button>Button</ic-button>\n\t\t\t</ic-activity>\n\t\t</ic-activities>\n\t</ic-screen>").appendTo("ic-screens");
    function FirstButtonTests($widget, widget) {
        describe("FirstButtonWidget tests", function () {
            it("moves us to the first activity", function () {
                var anchors = widget[0].get(ic.AnchorWidget);
                anchors.last().addState("active");
                expect(anchors.first().hasState("active")).toEqual(false);
                expect(anchors.last().hasState("active")).toEqual(true);
                $widget.eq(0).simulate("mousedown").simulate("mouseup");
                expect(anchors.first().hasState("active")).toEqual(true);
                expect(anchors.last().hasState("active")).toEqual(false);
            });
        });
    }
    test.FirstButtonTests = FirstButtonTests;
    test.describeQueue("FirstButtonWidget", function () {
        var widget = [];
        var $widget = $test.find(selector).each(function (i, element) {
            widget[i] = element.icWidget;
        });
        it("selector is \"ic-button[ic-button=first]\"", function () {
            expect(selector).toEqual("ic-button[ic-button=first]");
        });
        FirstButtonTests($widget, widget);
    });
})(test || (test = {}));
;
var test;
(function (test) {
    var selector = ic.FirstScreenButtonWidget.prototype.selector;
    var $test = $("<ic-screen>\n\t\t<ic-activities>\n\t\t\t<ic-anchor></ic-anchor>\n\n\t\t\t<p>FirstScreenButtonWidget</p>\n\n\t\t\t<ic-activity>\n\t\t\t\t<ic-button ic-button=\"firstscreen\">Button 0</ic-button>\n\n\t\t\t\t<ic-button ic-button=\"firstscreen\">Button 1</ic-button>\n\n\t\t\t\t<ic-button>Button</ic-button>\n\t\t\t</ic-activity>\n\t\t</ic-activities>\n\t</ic-screen>").appendTo("ic-screens");
    function FirstScreenButtonTests($widget, widget) {
        describe("FirstScreenButtonWidget tests", function () {
            it("moves us to the first screen", function () {
                test.screensWidget.screen = test.screensWidget.length - 1;
                expect(test.screensWidget.screen).toEqual(test.screensWidget.length - 1);
                $widget.eq(0).simulate("mousedown").simulate("mouseup");
                expect(test.screensWidget.screen).toEqual(0);
            });
        });
    }
    test.FirstScreenButtonTests = FirstScreenButtonTests;
    test.describeQueue("FirstScreenButtonWidget", function () {
        var widget = [];
        var $widget = $test.find(selector).each(function (i, element) {
            widget[i] = element.icWidget;
        });
        it("selector is \"ic-button[ic-button=firstscreen]\"", function () {
            expect(selector).toEqual("ic-button[ic-button=firstscreen]");
        });
        FirstScreenButtonTests($widget, widget);
    });
})(test || (test = {}));
;
var test;
(function (test) {
    var selector = ic.LastButtonWidget.prototype.selector;
    var $test = $("<ic-screen>\n\t\t<ic-activities>\n\t\t\t<ic-anchor></ic-anchor>\n\n\t\t\t<p>LastButtonWidget</p>\n\n\t\t\t<ic-activity>\n\t\t\t\t<ic-button ic-button=\"last\">Button 0</ic-button>\n\n\t\t\t\t<ic-button ic-button=\"last\">Button 1</ic-button>\n\n\t\t\t\t<ic-button>Button</ic-button>\n\t\t\t</ic-activity>\n\t\t</ic-activities>\n\t</ic-screen>").appendTo("ic-screens");
    function LastButtonTests($widget, widget) {
        describe("LastButtonWidget tests", function () {
            it("moves us to the last activity", function () {
                var anchors = widget[0].get(ic.AnchorWidget);
                anchors.first().addState("active");
                expect(anchors.first().hasState("active")).toEqual(true);
                expect(anchors.last().hasState("active")).toEqual(false);
                $widget.eq(0).simulate("mousedown").simulate("mouseup");
                expect(anchors.first().hasState("active")).toEqual(false);
                expect(anchors.last().hasState("active")).toEqual(true);
            });
        });
    }
    test.LastButtonTests = LastButtonTests;
    test.describeQueue("LastButtonWidget", function () {
        var widget = [];
        var $widget = $test.find(selector).each(function (i, element) {
            widget[i] = element.icWidget;
        });
        it("selector is \"ic-button[ic-button=last]\"", function () {
            expect(selector).toEqual("ic-button[ic-button=last]");
        });
        LastButtonTests($widget, widget);
    });
})(test || (test = {}));
;
var test;
(function (test) {
    var selector = ic.LastScreenButtonWidget.prototype.selector;
    var $test = $("<ic-screen>\n\t\t<ic-activities>\n\t\t\t<ic-anchor></ic-anchor>\n\n\t\t\t<p>LastScreenButtonWidget</p>\n\n\t\t\t<ic-activity>\n\t\t\t\t<ic-button ic-button=\"lastscreen\">Button 0</ic-button>\n\n\t\t\t\t<ic-button ic-button=\"lastscreen\">Button 1</ic-button>\n\n\t\t\t\t<ic-button>Button</ic-button>\n\t\t\t</ic-activity>\n\t\t</ic-activities>\n\t</ic-screen>").appendTo("ic-screens");
    function LastScreenButtonTests($widget, widget) {
        describe("LastScreenButtonWidget tests", function () {
            it("moves us to the last screen", function () {
                test.screensWidget.screen = 0;
                expect(test.screensWidget.screen).toEqual(0);
                $widget.eq(0).simulate("mousedown").simulate("mouseup");
                expect(test.screensWidget.screen).toEqual(test.screensWidget.length - 1);
            });
        });
    }
    test.LastScreenButtonTests = LastScreenButtonTests;
    test.describeQueue("LastScreenButtonWidget", function () {
        var widget = [];
        var $widget = $test.find(selector).each(function (i, element) {
            widget[i] = element.icWidget;
        });
        it("selector is \"ic-button[ic-button=lastscreen]\"", function () {
            expect(selector).toEqual("ic-button[ic-button=lastscreen]");
        });
        LastScreenButtonTests($widget, widget);
    });
})(test || (test = {}));
;
var test;
(function (test) {
    var selector = ic.MarkButtonWidget.prototype.selector;
    var $test = $("<ic-screen>\n\t\t<ic-activities>\n\t\t\t<ic-anchor></ic-anchor>\n\n\t\t\t<p>MarkButtonWidget</p>\n\n\t\t\t<ic-activity>\n\t\t\t\t<ic-button ic-button=\"mark\">Mark Button 0</ic-button>\n\n\t\t\t\t<ic-button ic-button=\"mark\">Mark Button 1</ic-button>\n\t\t\t</ic-activity>\n\n\t\t\t<ic-activity>\n\t\t\t\t<ic-button>Button</ic-button>\n\t\t\t</ic-activity>\n\t\t</ic-activities>\n\t</ic-screen>").appendTo("ic-screens");
    function MarkButtonTests($widget, widget) {
        describe("MarkButtonWidget tests", function () {
            it("marks our activity", function () {
                widget[0].screenWidget.removeState("marked");
                expect(widget[0].screenWidget.hasState("marked")).toEqual(false);
                $widget.eq(0).simulate("mousedown").simulate("mouseup");
                expect(widget[0].screenWidget.hasState("marked")).toEqual(true);
            });
            xit("doesn't mark sibling activities", function () {
                widget[0].screenWidget.removeState("marked");
                $widget.eq(0).simulate("mousedown").simulate("mouseup");
            });
            it("doesn't mark root", function () {
                widget[0].screenWidget.removeState("marked");
                widget[0].rootWidget.removeState("marked");
                expect(widget[0].rootWidget.hasState("marked")).toEqual(false);
                $widget.eq(0).simulate("mousedown").simulate("mouseup");
                expect(widget[0].rootWidget.hasState("marked")).toEqual(false);
            });
        });
    }
    test.MarkButtonTests = MarkButtonTests;
    test.describeQueue("MarkButtonWidget", function () {
        var widget = [];
        var $widget = $test.find(selector).each(function (i, element) {
            widget[i] = element.icWidget;
        });
        it("selector is \"ic-button[ic-button=mark]\"", function () {
            expect(selector).toEqual("ic-button[ic-button=mark]");
        });
        MarkButtonTests($widget, widget);
    });
})(test || (test = {}));
;
var test;
(function (test) {
    var selector = ic.NextButtonWidget.prototype.selector;
    var $test = $("<ic-screen>\n\t\t<ic-activities>\n\t\t\t<ic-anchor></ic-anchor>\n\n\t\t\t<p>NextButtonWidget</p>\n\n\t\t\t<ic-activity>\n\t\t\t\t<ic-button ic-button=\"next\">Next Button 0</ic-button>\n\n\t\t\t\t<ic-button ic-button=\"next\">Next Button 1</ic-button>\n\n\t\t\t\t<ic-button>Button</ic-button>\n\t\t\t</ic-activity>\n\t\t</ic-activities>\n\t</ic-screen>").appendTo("ic-screens");
    function NextButtonTests($widget, widget) {
        describe("NextButtonWidget tests", function () {
            it("moves us to the next activity", function () {
                var anchors = widget[0].get(ic.AnchorWidget);
                anchors[0].addState("active");
                expect(anchors[0].hasState("active")).toEqual(true);
                expect(anchors[1].hasState("active")).toEqual(false);
                $widget.eq(0).simulate("mousedown").simulate("mouseup");
                expect(anchors[0].hasState("active")).toEqual(false);
                expect(anchors[1].hasState("active")).toEqual(true);
            });
        });
    }
    test.NextButtonTests = NextButtonTests;
    test.describeQueue("NextButtonWidget", function () {
        var widget = [];
        var $widget = $test.find(selector).each(function (i, element) {
            widget[i] = element.icWidget;
        });
        it("selector is \"ic-button[ic-button=next]\"", function () {
            expect(selector).toEqual("ic-button[ic-button=next]");
        });
        NextButtonTests($widget, widget);
    });
})(test || (test = {}));
;
var test;
(function (test) {
    var selector = ic.PrevButtonWidget.prototype.selector;
    var $test = $("<ic-screen>\n\t\t<ic-activities>\n\t\t\t<ic-anchor></ic-anchor>\n\n\t\t\t<p>PrevButtonWidget</p>\n\n\t\t\t<ic-activity>\n\t\t\t\t<ic-button ic-button=\"prev\">Prev Button 0</ic-button>\n\n\t\t\t\t<ic-button ic-button=\"prev\">Prev Button 1</ic-button>\n\n\t\t\t\t<ic-button>Button</ic-button>\n\t\t\t</ic-activity>\n\t\t</ic-activities>\n\t</ic-screen>").appendTo("ic-screens");
    function PrevButtonTests($widget, widget) {
        describe("PrevButtonWidget tests", function () {
            it("moves us to the prev activity", function () {
                var anchors = widget[0].get(ic.AnchorWidget);
                anchors[1].addState("active");
                expect(anchors[0].hasState("active")).toEqual(false);
                expect(anchors[1].hasState("active")).toEqual(true);
                $widget.eq(0).simulate("mousedown").simulate("mouseup");
                expect(anchors[0].hasState("active")).toEqual(true);
                expect(anchors[1].hasState("active")).toEqual(false);
            });
        });
    }
    test.PrevButtonTests = PrevButtonTests;
    test.describeQueue("PrevButtonWidget", function () {
        var widget = [];
        var $widget = $test.find(selector).each(function (i, element) {
            widget[i] = element.icWidget;
        });
        it("selector is \"ic-button[ic-button=prev]\"", function () {
            expect(selector).toEqual("ic-button[ic-button=prev]");
        });
        PrevButtonTests($widget, widget);
    });
})(test || (test = {}));
;
var test;
(function (test) {
    var selector = ic.SelectWidget.prototype.selector;
    var $test = $("<ic-screen>\n\t\t<ic-activities>\n\t\t\t<ic-anchor></ic-anchor>\n\n\t\t\t<p>SelectWidget</p>\n\n\t\t\t<ic-activity>\n\t\t\t\t<ic-select>Select 0</ic-select>\n\n\t\t\t\t<ic-select>Select 1</ic-select>\n\t\t\t</ic-activity>\n\t\t</ic-activities>\n\t</ic-screen>").appendTo("ic-screens");
})(test || (test = {}));
;
var test;
(function (test) {
    var selector = ic.ToggleWidget.prototype.selector;
    var $test = $("<ic-screen>\n\t\t<ic-activities>\n\t\t\t<ic-anchor></ic-anchor>\n\n\t\t\t<p>ToggleWidget</p>\n\n\t\t\t<ic-activity>\n\t\t\t\t<ic-toggle>Toggle 0</ic-toggle>\n\n\t\t\t\t<ic-toggle>Toggle 1</ic-toggle>\n\n\t\t\t\t<ic-toggleic-group=\"1\">Toggle 2</ic-toggle>\n\n\t\t\t\t<ic-toggle ic-group=\"1\">Toggle 3</ic-toggle>\n\n\t\t\t\t<ic-toggle ic-toggle=\"multiple\">Toggle 4</ic-toggle>\n\n\t\t\t\t<ic-toggle ic-toggle=\"multiple\">Toggle 5</ic-toggle>\n\n\t\t\t\t<ic-toggle ic-group=\"1\" ic-toggle=\"multiple\">Toggle 6</ic-toggle>\n\n\t\t\t\t<ic-toggle ic-group=\"1\" ic-toggle=\"multiple\">Toggle 7</ic-toggle>\n\t\t\t</ic-activity>\n\n\t\t\t<ic-activity>\n\t\t\t\t<ic-toggle id=\"ic-toggle-8\">Toggle 8</ic-toggle>\n\t\t\t</ic-activity>\n\t\t</ic-activities>\n\t</ic-screen>").appendTo("ic-screens");
    function ToggleTests($widget, widget) {
        test.ButtonTests($widget, widget);
        describe("ToggleWidget tests", function () {
            it("ToggleWidget test requires eight or more widgets", function () {
                expect($widget.length).toBeGreaterThan(7);
            });
            it("mouse click toggles checked state", function () {
                $widget.eq(0).simulate("mousedown").simulate("mouseup");
                expect(widget[0].hasState("checked")).toEqual(true);
                $widget.eq(0).simulate("mousedown").simulate("mouseup");
                expect(widget[0].hasState("checked")).toEqual(false);
            });
            it("mouse click elsewhere doesn't change checked state", function () {
                $widget.eq(1).simulate("mousedown");
                $widget.eq(0).simulate("mouseup").simulate("click");
                expect(widget[0].hasState("checked")).toEqual(false);
                expect(widget[1].hasState("checked")).toEqual(false);
                $widget.eq(0).simulate("mousedown");
                $widget.eq(1).simulate("mouseup").simulate("click");
                expect(widget[0].hasState("checked")).toEqual(false);
                expect(widget[1].hasState("checked")).toEqual(false);
            });
            describe("ic-toggle=single", function () {
                it("clicking doesn't check sibling", function () {
                    $widget.eq(0).simulate("mousedown").simulate("mouseup");
                    expect(widget[0].hasState("checked")).toEqual(true);
                    expect(widget[1].hasState("checked")).toEqual(false);
                });
                it("clicking sibling unchecks self", function () {
                    $widget.eq(0).simulate("mousedown").simulate("mouseup");
                    $widget.eq(1).simulate("mousedown").simulate("mouseup");
                    expect(widget[0].hasState("checked")).toEqual(false);
                    expect(widget[1].hasState("checked")).toEqual(true);
                });
                it("clicking group 0 doesn't affect group 1", function () {
                    $widget.eq(3).simulate("mousedown").simulate("mouseup");
                    $widget.eq(0).simulate("mousedown").simulate("mouseup");
                    expect(widget[0].hasState("checked")).toEqual(true);
                    expect(widget[3].hasState("checked")).toEqual(true);
                });
                it("clicking group 1 doesn't affect group 0", function () {
                    $widget.eq(0).simulate("mousedown").simulate("mouseup");
                    $widget.eq(3).simulate("mousedown").simulate("mouseup");
                    expect(widget[0].hasState("checked")).toEqual(true);
                    expect(widget[3].hasState("checked")).toEqual(true);
                });
                it("clicking single doesn't affect multiple", function () {
                    $widget.eq(4).simulate("mousedown").simulate("mouseup");
                    $widget.eq(0).simulate("mousedown").simulate("mouseup");
                    expect(widget[0].hasState("checked")).toEqual(true);
                    expect(widget[4].hasState("checked")).toEqual(true);
                });
                it("clicking doesn't affect other activities", function () {
                    $widget.eq(0).simulate("mousedown").simulate("mouseup");
                    $widget.eq(7).simulate("mousedown").simulate("mouseup");
                    expect(widget[0].hasState("checked")).toEqual(true);
                    expect(widget[7].hasState("checked")).toEqual(true);
                });
            });
            describe("ic-toggle=multiple", function () {
                it("clicking doesn't check sibling", function () {
                    $widget.eq(4).simulate("mousedown").simulate("mouseup");
                    expect(widget[4].hasState("checked")).toEqual(true);
                    expect(widget[5].hasState("checked")).toEqual(false);
                });
                it("clicking sibling doesn't uncheck self", function () {
                    $widget.eq(4).simulate("mousedown").simulate("mouseup");
                    $widget.eq(5).simulate("mousedown").simulate("mouseup");
                    expect(widget[4].hasState("checked")).toEqual(true);
                    expect(widget[5].hasState("checked")).toEqual(true);
                });
                it("clicking group 0 doesn't affect group 1", function () {
                    $widget.eq(6).simulate("mousedown").simulate("mouseup");
                    $widget.eq(4).simulate("mousedown").simulate("mouseup");
                    expect(widget[4].hasState("checked")).toEqual(true);
                    expect(widget[6].hasState("checked")).toEqual(true);
                });
                it("clicking group 1 doesn't affect group 0", function () {
                    $widget.eq(4).simulate("mousedown").simulate("mouseup");
                    $widget.eq(6).simulate("mousedown").simulate("mouseup");
                    expect(widget[4].hasState("checked")).toEqual(true);
                    expect(widget[6].hasState("checked")).toEqual(true);
                });
                it("clicking multiple doesn't affect single", function () {
                    $widget.eq(0).simulate("mousedown").simulate("mouseup");
                    $widget.eq(4).simulate("mousedown").simulate("mouseup");
                    expect(widget[0].hasState("checked")).toEqual(true);
                    expect(widget[4].hasState("checked")).toEqual(true);
                });
            });
        });
    }
    test.ToggleTests = ToggleTests;
    test.describeQueue("ToggleWidget", function () {
        var widget = [];
        var $widget = $test.find(selector).each(function (i, element) {
            widget[i] = element.icWidget;
        });
        it("selector is \"ic-toggle\"", function () {
            expect(selector).toEqual("ic-toggle,ic-button[ic-button=toggle]");
        });
        ToggleTests($widget, widget);
    });
})(test || (test = {}));
;
var test;
(function (test) {
    var selector = ic.NavWidget.prototype.selector;
    var $test = $("<ic-screen>\n\t\t<ic-activities>\n\t\t\t<ic-anchor></ic-anchor>\n\n\t\t\t<p>NavWidget</p>\n\n\t\t\t<ic-activity>\n\t\t\t\t<ic-nav></ic-nav>\n\t\t\t</ic-activity>\n\t\t</ic-activities>\n\t</ic-screen>").appendTo("ic-screens");
    function NavTests($widget, widget) {
        test.WidgetTests($widget, widget);
        describe("NavWidget tests", function () {
            it("has the correct number of children", function () {
                var anchors = widget[0].get(ic.AnchorWidget);
                expect(widget[0].anchors.length - 1).toEqual(anchors.length);
            });
            xit("moves us to another activity", function () {
                var anchors = widget[0].get(ic.AnchorWidget);
                widget[0].anchors[1].icWidget.addState("active");
                console.log(anchors[0].hasState("active"), anchors[1].hasState("active"));
                expect(anchors[0].hasState("active")).toEqual(true);
                expect(anchors[1].hasState("active")).toEqual(false);
                $widget.eq(0).simulate("mousedown", { target: widget[0].anchors[2] }).simulate("mouseup", { target: widget[0].anchors[2] });
                console.log(anchors[0].hasState("active"), anchors[1].hasState("active"));
                expect(anchors[0].hasState("active")).toEqual(false);
                expect(anchors[1].hasState("active")).toEqual(true);
            });
        });
    }
    test.NavTests = NavTests;
    test.describeQueue("NavWidget", function () {
        var widget = [];
        var $widget = $test.find(selector).each(function (i, element) {
            widget[i] = element.icWidget;
        });
        it("selector is \"ic-nav\"", function () {
            expect(selector).toEqual("ic-nav");
        });
        NavTests($widget, widget);
    });
})(test || (test = {}));
;
var test;
(function (test) {
    var selector = ic.PinWidget.prototype.selector;
    var $test = $("<ic-screen>\n\t\t<ic-activities>\n\t\t\t<ic-anchor></ic-anchor>\n\n\t\t\t<p>PinWidget</p>\n\n\t\t\t<ic-activity>\n\t\t\t\t<ic-pin>Pin 0</ic-pin>\n\n\t\t\t\t<ic-pin>Pin 1</ic-pin>\n\t\t\t</ic-activity>\n\t\t</ic-activities>\n\t</ic-screen>").appendTo("ic-screens");
})(test || (test = {}));
;
var test;
(function (test) {
    var selector = ic.PinsWidget.prototype.selector;
    var $test = $("<ic-screen>\n\t\t<ic-activities>\n\t\t\t<ic-anchor></ic-anchor>\n\n\t\t\t<p>PinsWidget</p>\n\n\t\t\t<ic-activity>\n\t\t\t\t<ic-pins>Pins 0</ic-pins>\n\n\t\t\t\t<ic-pins>Pins 1</ic-pins>\n\t\t\t</ic-activity>\n\t\t</ic-activities>\n\t</ic-screen>").appendTo("ic-screens");
})(test || (test = {}));
;
var test;
(function (test) {
    var selector = ic.QuestionWidget.prototype.selector;
    var $test = $("<ic-screen>\n\t\t<ic-activities>\n\t\t\t<ic-anchor></ic-anchor>\n\n\t\t\t<p>QuestionWidget</p>\n\n\t\t\t<ic-activity>\n\t\t\t\t<ic-question>Question 0</ic-question>\n\n\t\t\t\t<ic-question>Question 1</ic-question>\n\t\t\t</ic-activity>\n\t\t</ic-activities>\n\t</ic-screen>").appendTo("ic-screens");
    test.describeQueue("QuestionWidget", function () {
        var widget = [];
        var $widget = $test.find(selector).each(function (i, element) {
            widget[i] = element.icWidget;
        });
        it("selector is \"ic-question\"", function () {
            expect(selector).toEqual("ic-question");
        });
        test.WidgetTests($widget, widget);
    });
})(test || (test = {}));
;
var test;
(function (test) {
    var selector = ic.TimerWidget.prototype.selector;
    var $test = $("<ic-screen>\n\t\t<ic-activities>\n\t\t\t<ic-anchor></ic-anchor>\n\n\t\t\t<p>TimerWidget</p>\n\n\t\t\t<ic-activity>\n\t\t\t\t<ic-timer>Timer 0</ic-timer>\n\n\t\t\t\t<ic-timer>Timer 1</ic-timer>\n\t\t\t</ic-activity>\n\t\t</ic-activities>\n\t</ic-screen>").appendTo("ic-screens");
})(test || (test = {}));
;
var test;
(function (test) {
    if (/complete|loaded|interactive/.test(document.readyState) && document.body) {
        test.startup(document);
    }
    else {
        document.addEventListener("DOMContentLoaded", test.startup.bind(test, document), false);
    }
})(test || (test = {}));
;
//# sourceMappingURL=activity.test.js.map