<?php
/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

require_once "include/config.default.php";
if (file_exists("include/config.inc.php")) {
	require_once "include/config.inc.php";
} else {
	require_once "install/install.php";
}

require_once "include/common.php";
require_once "include/version.php";

do_login(true); // Temporarily make sure we're logged in

header("Location: editor/index.html");
