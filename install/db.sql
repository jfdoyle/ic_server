-- Infuze Creator database structure

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

-- --------------------------------------------------------

--
-- Table structure for table `ic_group`
--

CREATE TABLE IF NOT EXISTS `ic_group` (
`group_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `group_parent` bigint(20) unsigned NOT NULL,
  `group_node` bigint(20) unsigned NOT NULL,
  `group_name` varchar(32) NOT NULL,
  PRIMARY KEY (`group_id`),
  KEY (`group_parent`),
  KEY (`group_node`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `ic_log`
--

CREATE TABLE IF NOT EXISTS `ic_log` (
`log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `log_group` bigint(20) unsigned NOT NULL,
  `log_user` bigint(20) unsigned NOT NULL,
  `log_node` bigint(20) unsigned NOT NULL,
  `log_time` datetime NOT NULL,
  `log_action` enum('add','delete','edit','rename','flag','comment','move','build') NOT NULL,
  `log_file` varchar(64) NOT NULL,
  PRIMARY KEY (`log_id`),
  KEY `log_organisation` (`log_group`,`log_user`,`log_node`,`log_time`,`log_file`),
  KEY `log_node` (`log_node`,`log_action`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `ic_node`
--

CREATE TABLE IF NOT EXISTS `ic_node` (
`node_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `node_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `node_creator` bigint(20) unsigned NOT NULL,
  `node_created` datetime NOT NULL,
  `node_editor` bigint(20) unsigned NOT NULL,
  `node_time` datetime NOT NULL,
  `node_flags` bigint(20) unsigned NOT NULL DEFAULT '0',
  `node_type` int(5) unsigned NOT NULL DEFAULT '10',
  `node_name` varchar(128) NOT NULL,
  `node_html` text NOT NULL,
  `node_children` text NOT NULL,
  `node_data` text NOT NULL,
  `node_tags` text NOT NULL,
  `node_build` text NOT NULL,
  PRIMARY KEY (`node_id`),
  KEY (`node_parent`),
  KEY (`node_time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

-- --------------------------------------------------------

--
-- Table structure for table `ic_user`
--

CREATE TABLE IF NOT EXISTS `ic_user` (
`user_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_current` bigint(20) unsigned NOT NULL,
  `user_permission` int(10) unsigned NOT NULL,
  `user_email` varchar(128) NOT NULL,
  `user_password` varchar(128) NOT NULL,
  `user_group` varchar(128) NOT NULL,
  `user_salt` varchar(128) NOT NULL,
  `user_seen` datetime NOT NULL,
  `user_update` datetime NOT NULL,
  PRIMARY KEY (`user_id`),
  KEY (`user_current`),
  KEY (`user_email`),
  KEY (`user_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
