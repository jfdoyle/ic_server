<?php
/**
 * Called when we can't find the config.inc.php file - if it's got the right
 * data from our own form then setup initial database and return, otherwise
 * output a form to fill in the initial values and quit
 */
error_reporting(0);
ini_set('display_errors', 0);

function get_default($name, $default) {
	$result = filter_input(INPUT_POST, $name);
	if (is_null($result)) {
		$result = $default;
	}
	return $result;
}

function is_error() {
	global $mysqli_errno;

	$numbers = func_get_args();
	foreach ($numbers as $num) {
		if ($mysqli_errno == $num) {
			echo " class=\"warn\"";
			break;
		}
	}
}

function is_root_error() {
	global $mysqli_root_errno;

	$numbers = func_get_args();
	foreach ($numbers as $num) {
		if ($mysqli_root_errno == $num) {
			echo " class=\"warn\"";
			break;
		}
	}
}

function is_valid_email() {
	global $user_email;

	if (!filter_var($user_email, FILTER_VALIDATE_EMAIL) !== false) {
		echo " class=\"warn\"";
	}
}

function is_valid_password() {
	global $user_password, $user_password_repeat;

	if (empty($user_password) || $user_password !== $user_password_repeat) {
		echo " class=\"warn\"";
	}
}

// TODO: Check permissions
function check_should_create() {
	global $mysqli_errno, $user_email, $user_password, $user_password_repeat, //
	$db_host, $db_port, $db_root, $db_root_pass, //
	$mysqli_root, $mysqli_root_error, $mysqli_root_errno;

	if ($mysqli_errno // check we have some connectivity
			&& !filter_var($user_email, FILTER_VALIDATE_EMAIL) === false // check the email is real
			&& !empty($user_password) && $user_password === $user_password_repeat) { // check we've got a valid password
		$mysqli_root = new mysqli($db_host, $db_root, $db_root_pass, "", $db_port);
		$mysqli_root_error = "Connected " . mysqli_connect_error();
		$mysqli_root_errno = mysqli_connect_errno();
		if (!$mysqli_root_errno) { // let's create things
			return true;
		}
	}
	return false;
}

// TODO: Error handling
function create_database() {
	global $mysqli_root, $db_host, $db_port, $db_user, $db_pass, $db_login, $db_database, $group_name, $user_email, $user_password;

	if ($mysqli_root->query("CREATE DATABASE IF NOT EXISTS $db_database") !== TRUE) {
		echo "Error creating database: " . $mysqli_root->error;
	} elseif (!$mysqli_root->query("CREATE USER '$db_user'@'$db_login' IDENTIFIED BY '$db_pass'")) {
		echo "Error creating user: " . $mysqli_root->error;
	} elseif (!$mysqli_root->query("GRANT ALL ON `$db_database`.* TO '$db_user'@'$db_login'")) {
		echo "Error giving permission: " . $mysqli_root->error;
	} else {
		// Make sure we're using the correct default DB
		$mysqli_root->select_db($db_database);
		// Create basic layout
		$mysqli_root->multi_query(file_get_contents("install/db.sql"));
		do { // flush multi_queries
			$mysqli_root->next_result();
			if ($mysqli_root->errno) {
				echo "Error " . $mysqli_root->errno . ": " . $mysqli_root->error;
			}
		} while ($mysqli_root->more_results());
		// Create Template Node
		$mysqli_root->query("INSERT INTO `ic_node` (`node_time`, `node_created`, `node_name`, `node_type`) SELECT UTC_TIMESTAMP(), UTC_TIMESTAMP(), '$group_name', 1");
		$template_id = $mysqli_root->insert_id;
		// Create Node
		$mysqli_root->query("INSERT INTO `ic_node` (`node_time`, `node_created`, `node_name`, `node_type`) SELECT UTC_TIMESTAMP(), UTC_TIMESTAMP(), '$group_name', 0");
		$node_id = $mysqli_root->insert_id;
		// Create Default Theme Node
		$mysqli_root->query("INSERT INTO `ic_node` (`node_time`, `node_created`, `node_name`, `node_type`, `node_parent`) SELECT UTC_TIMESTAMP(), UTC_TIMESTAMP(), 'default', 11, '$template_id'");
		$theme_id = $mysqli_root->insert_id;
		$mysqli_root->query("UPDATE `ic_node` SET `node_children` = '$theme_id' WHERE `node_id` = '$template_id' LIMIT 1");
		// Create group
		$mysqli_root->query("INSERT INTO `ic_group` (`group_node`, `group_name`) VALUES ('$node_id','$group_name')");
		$group_id = $mysqli_root->insert_id;
		// Create user
		$salt = hash("sha512", uniqid(mt_rand(1, mt_getrandmax()), true));
		$password = hash("sha512", $user_password . $salt);
		$permissions = 0xffff;
		$mysqli_root->query("INSERT INTO `ic_user` (`user_group`, `user_email`, `user_password`, `user_salt`, `user_permission`, `user_update`) "
				. "VALUES ('$group_id', '$user_email', '$password', '$salt', '$permissions', UTC_TIMESTAMP())");
		if ($mysqli_root->errno) {
			echo "Error " . $mysqli_root->errno . ": " . $mysqli_root->error;
		}
		$user_id = $mysqli_root->insert_id;
		// Make sure we mark who created the root node
		$mysqli_root->query("UPDATE `ic_node` SET `node_creator` = '$user_id' WHERE `node_id` = '$node_id'");

		file_put_contents("include/config.inc.php", "<?php\n" //
				. "\$cfg[\"mysql_host\"] = \"$db_host\";\n" //
				. "\$cfg[\"mysql_port\"] = $db_port;\n" //
				. "\$cfg[\"mysql_user\"] = \"$db_user\";\n" //
				. "\$cfg[\"mysql_password\"] = \"$db_pass\";\n" //
				. "\$cfg[\"mysql_database\"] = \"$db_database\";\n" //
		);
		return true;
	}
	return false;
}

$db_host = get_default("db_host", "127.0.0.1");
$db_port = get_default("db_port", "3306");
$db_database = get_default("db_database", "ic");
$db_user = get_default("db_user", "ic_user");
$db_pass = get_default("db_pass", "ic_password");
$db_login = get_default("db_login", "localhost");

$db_root = get_default("db_root", "root");
$db_root_pass = get_default("db_root_pass", "");

$group_name = get_default("group_name", "Admin");

$user_email = get_default("user_email", "");
$user_password = get_default("user_password", "");
$user_password_repeat = get_default("user_password_repeat", "");

$mysqli = new mysqli($db_host, $db_user, $db_pass, $db_database, $db_port);
$mysqli_error = mysqli_connect_error();
$mysqli_errno = mysqli_connect_errno();

if (check_should_create()) {
	if (create_database()) {
		include "include/config.inc.php";
		return;
	}
}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Infuze Creator Install Wizard</title>
		<style>
			body {
				text-align: left;
				background: #282828;
			}
			table {
				margin: 0 auto;
				max-width: 800px;
				background: white;
				border-radius: 0.5em;
			}
			p {
				margin: 0;
				padding: 0.25em 0.5em;
			}
			th {
				font-weight: bold;
				text-align: center;
			}
			th:only-child {
			}
			td:first-child {
				padding: 1px 0.5em;
				text-align: right;
			}
			td:last-child {
				padding: 1px 0.5em;
				font-style: italic;
				background: #bbb;
			}
			td:only-child {
				text-align: left;
				padding: 0.25em 0.5em;
			}
			tbody:first-child th {
				padding: 0.25em 0.5em;
				font-size: 3em;
			}
			tbody > tr > td,
			tbody > tr > th {
				background: lightgrey;
			}
			tbody > tr.warn > td {
				font-weight: bold;
				color: red;
			}
			tbody > tr:first-child > th {
				background: lightblue;
				border-top-left-radius: 0.25em;
				border-top-right-radius: 0.25em;
				font-size: 2em;
			}
			tbody > tr:only-child > th {
				border-bottom-left-radius: 0.25em;
				border-bottom-right-radius: 0.25em;
			}
			tbody > tr:last-child > td:first-child {
				border-bottom-left-radius: 0.5em;
			}
			tbody > tr:last-child > td:last-child {
				border-bottom-right-radius: 0.5em;
			}
			input[type="text"],
			input[type="password"] {
				box-sizing: border-box;
				padding: 1px 0.25em;
				width: 100%;
				color: black;
			}
			input[type="submit"] {
				font-size: inherit;
				margin: 0.25em;
			}
		</style>
	</head>
	<body>
		<form method="post">
			<table>
				<tbody>
					<tr>
						<th colspan="3">Infuze Creator Install Wizard</th>
					</tr><tr>
						<td colspan="3">
							<p>This page is showing as you have not setup the Infuze Creator configuration.</p>
							<p>Please fill out the following fields and click Submit</p>
							<?php print "$mysqli_errno<br>$mysqli_error<br><br>$mysqli_root_errno<br>$mysqli_root_error"; ?>
						</td>
					</tr>
				</tbody><tbody>
					<tr>
						<th colspan="3">Database</th>
					</tr><tr <?php is_error(2002); ?>>
						<td>Host</td>
						<td><input type="text" autocomplete="false" name="db_host" value="<?php echo $db_host; ?>"></td>
						<td>Hostname of MySQL server (127.0.0.1)</td>
					</tr><tr <?php is_error(2002); ?>>
						<td>Port</td>
						<td><input type="text" autocomplete="false" name="db_port" value="<?php echo $db_port; ?>"></td>
						<td>Port number of MySQL server (3306)</td>
					</tr><tr <?php is_error(1045, 1049); ?>>
						<td>Database</td>
						<td><input type="text" autocomplete="false" name="db_database" value="<?php echo $db_database; ?>"></td>
						<td>Database to use (ic)</td>
					</tr><tr <?php is_error(1045); ?>>
						<td>Username</td>
						<td><input type="text" autocomplete="false" name="db_user" value="<?php echo $db_user; ?>"></td>
						<td>User to use / create (ic_user)</td>
					</tr><tr <?php is_error(1045); ?>>
						<td>Password</td>
						<td><input type="text" autocomplete="false" name="db_pass" value="<?php echo $db_pass; ?>"></td>
						<td>Password for user (ic_password)</td>
					</tr><tr <?php is_root_error(1045); ?>>
						<th colspan="2">To Create User and Database</th>
						<td>&nbsp;</td>
					</tr><tr <?php is_error(2002); ?>>
						<td>Admin User</td>
						<td><input type="text" autocomplete="false" name="db_root" value="<?php echo $db_root; ?>"></td>
						<td>Required to create the database and login (root)</td>
					</tr><tr <?php is_root_error(1045); ?>>
						<td>Admin Password</td>
						<td><input type="text" autocomplete="false" name="db_root_pass" value="<?php echo $db_root_pass; ?>"></td>
						<td>Required to create the database and login (-)</td>
					</tr><tr>
						<td>Login</td>
						<td><input type="text" autocomplete="false" name="db_login" value="<?php echo $db_login; ?>"></td>
						<td>If creating user, limit to only this hostname (localhost)</td>
					</tr><tr>
						<td colspan="3">
							<p>In order to create the database and login you need to supply the root user account, if supplying a pre-existing login then you do not need that.</p>
							<p><b>NOTE:</b> Once these values are entered they cannot be changed without server / database access.</p>
						</td>
					</tr>
				</tbody><tbody>
					<tr>
						<th colspan="3">Admin Group</th>
					</tr><tr>
						<td>Name</td>
						<td><input type="text" autocomplete="false" name="group_name" value="<?php echo $group_name; ?>"></td>
						<td>Name of the admin group (Admin)</td>
					</tr><tr>
						<td colspan="3">
							<p>This is used to inherit templates and themes by all other Groups and Projects.</p>
						</td>
					</tr>
				</tbody><tbody>
					<tr>
						<th colspan="3">Admin User</th>
					</tr><tr <?php is_valid_email(); ?>>
						<td>Email</td>
						<td><input type="text" name="user_email" value="<?php echo $user_email; ?>"></td>
						<td>Email of the admin user (no default)</td>
					</tr><tr <?php is_valid_password(); ?>>
						<td>Password</td>
						<td><input type="password" name="user_password" value="<?php echo $user_password; ?>"></td>
						<td>Password for the admin user (no default)</td>
					</tr><tr <?php is_valid_password(); ?>>
						<td>Repeat Password</td>
						<td><input type="password" autocomplete="false" name="user_password_repeat" value="<?php echo $user_password_repeat; ?>"></td>
						<td>&nbsp;</td>
					</tr><tr>
						<td colspan="3">
							<p>Once created you can use this user to create other users and groups.</p>
						</td>
					</tr>
				</tbody><tbody>
					<tr>
						<th colspan="3">
							<input type="submit" value="Submit">
						</th>
					</tr>
				</tbody>
			</table>
		</form>
	</body>
</html>
<?php
die;
