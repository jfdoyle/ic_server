<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 */

// Backup the database
require_once "include/common.php";
require_once "include/twitter.php";

if (php_sapi_name() === "cli" || login_check() && is_root()) {
	require_once "include/backup.php";

	$result = backup();
	tweet("Backup " . (empty($result["success"]) ? "FAILED" : "success"));
	die($result[empty($result["success"]) ? "error" : "success"]);
}
die("NO PERMISSION");
