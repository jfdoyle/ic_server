<?php

/*
 * Copyright © 2013-2016 Infuze Ltd <enq@infuze.co.uk>, All Rights Reserved.
 * 
 * Post a list of everything that has changed since a certain date (passed in url)
 */

require_once "include/db.php";

if (!$cfg["replicate"]) {
	die("REPLICATION DISABLED");
}

$time_limit = 300;

ini_set("memory_limit", "1024M");
ini_set('max_execution_time', "3000");
set_time_limit($time_limit);

header("Content-type: text/plain");

set_error_handler(function($errno, $message) {
	if ($errno === E_WARNING && (strpos($message, "filemtime") === 0 || strpos($message, "fopen") === 0)) {
		return true; // using filemtime for file_exists too
	}
	return false; // default handler
});

$ctx = hash_init("sha512");
$salt = "Infuze Creator SaLt v4n6yo7wyd78kcy4wo8ivtyu83v"; // Random string, don't change (normally)

function hash_print_end() {
	global $hash_output, $output, $ctx, $salt;

	if ($hash_output) {
		$output .= "HASH " . hash("sha512", hash_final($ctx) . $salt) . "\n";
	}
}

function verbose_print($str) {
	global $cfg;
	if ($cfg["verbose"]) {
		print($str . "\n");
	}
}

if (empty($_GET)) {

	if (empty($cfg["replicate_host"])) {
		die("REPLICATION SERVER ONLY");
	}

	// Decode data

	$fields = array();
	$result = $mysqli->query("SELECT"
			. "(SELECT MAX(`node_time`) FROM `ic_node`),"
			. "(SELECT MAX(`user_update`) FROM `ic_user`),"
			. "(SELECT MAX(`group_id`) FROM `ic_group`),"
			. "(SELECT MAX(`log_id`) FROM `ic_log`)");
	$row = $result->fetch_row();
	$result->free();
	$ic_node = str_replace(" ", "T", empty($row[0]) ? "0000-00-00 00:00:00" : $row[0]) . "Z";
	$ic_user = str_replace(" ", "T", empty($row[1]) ? "0000-00-00 00:00:00" : $row[1]) . "Z";
	$ic_group = $row[2] ?: 0;
	$ic_log = $row[3] ?: 0;

	$file = file_get_contents("$cfg[replicate_host]replicate.php?ic_node=$ic_node&ic_user=$ic_user&ic_group=$ic_group&ic_log=$ic_log");

	$lines = explode("\n", $file);
	//error_log("Start");
	$mysqli->begin_transaction();
	foreach ($lines as $line) {
		$str = trim($line);
		if (!empty($str)) {
			$words = explode(" ", $line, 3);
			switch ($words[0]) {
				case "FIELDS":
					hash_update($ctx, $str);
					$fields[$words[1]] = $words[2];
					//$matches = str_getcsv(trim($words[2], "()"), ",", "`");
					//error_log(count($matches) . " = " . print_r($matches, true));
					//preg_match_all('/`((?:\\\\.|[^\\\\`])*)`,?/', trim($words[2], "()"), $matches);
					//error_log(count($matches[1]) . " = " . print_r($matches[1], true));
					break;

				case "REPLACE":
					hash_update($ctx, $str);
					if ($fields[$words[1]]) {
						//$matches = str_getcsv(trim($words[2], "()"));
						//error_log(count($matches) . " = " . print_r($matches, true));
						//preg_match_all('/"((?:\\\\.|[^\\\\"])*)",?/', trim($words[2], "()"), $matches);
						//error_log(count($matches[1]) . " = " . print_r($matches[1], true));
						$query = "REPLACE INTO $words[1] " . $fields[$words[1]] . " VALUES $words[2]";
						verbose_print($query);
						if ($mysqli->query($query)) {
							verbose_print("...UPDATED");
						}
					}
					break;

				case "FILE":
					hash_update($ctx, $str);
					$words[2] = urldecode($words[2]);
					preg_match("@/(assets/\d+/.*)$@", $words[2], $matches);
					$filename = $root . "/" . $matches[1];
					$time = filemtime($filename);
					verbose_print("FILE ..." . $words[2]);
					if ($time === false || strtotime($words[1]) > intval(gmdate("U", $time))) {
						if (!is_dir(pathinfo($filename, PATHINFO_DIRNAME))) {
							mkdir(pathinfo($filename, PATHINFO_DIRNAME));
						}
						file_put_contents($filename, file_get_contents(str_replace(" ", "%20", $words[2])));
						verbose_print("...COPIED (" . $words[1] . ") ");
					}
					break;

				case "HASH":
					// As soon as we hit the HASH line we stop dealing with anything else, it marks the last line and enforces data validity
					$newHash = hash("sha512", hash_final($ctx) . $salt);
					if ($words[1] === $newHash) {
						$mysqli->commit();
						verbose_print("... COMPLETE");
						//error_log("End - success");
					} else {
						$mysqli->rollback();
						verbose_print("... BAD HASH!");
						//error_log("End - error");
					}
					die;
			}
		}
	}
	$mysqli->rollback();
	verbose_print("... NO HASH FOUND");
} else {

	// Send data

	$output = "";

	function hash_print($str) {
		global $hash_output, $output, $ctx;

		$hash_output = true;
		hash_update($ctx, $str);
		$output .= $str . "\n";
	}

	function send_table_changes($table, $key, $assets = false) {
		global $mysqli;

		$since = filter_input(INPUT_GET, $table, FILTER_SANITIZE_STRING);
		if (preg_match("/^\d+$/", $since) || preg_match("/^\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\dZ$/", $since)) {
			$url = "http://" . filter_input(INPUT_SERVER, "HTTP_HOST", FILTER_SANITIZE_STRING) . "/";
			$fields = false;
			$result = $mysqli->query("SELECT * FROM `$table` WHERE `$key` > '$since' ORDER BY `$key`", MYSQLI_STORE_RESULT);
			while (($row = $result->fetch_assoc())) {
				if (!$fields) {
					$fields = true;
					hash_print("FIELDS `$table` (`" . implode("`,`", array_keys($row)) . "`)");
				}
				hash_print("REPLACE `$table` (" . preg_replace("/^\[|\]$/", "", json_encode(array_values($row))) . ")");
				if ($assets) {
					foreach (glob("assets/$row[node_id]/*", GLOB_NOSORT) as $filename) {
						if (is_file($filename)) {
							hash_print("FILE " . str_replace("+0000", "Z", gmdate(DATE_ISO8601, filemtime($filename))) . " " . $url . "assets/$row[node_id]/" . urlencode(pathinfo($filename, PATHINFO_BASENAME)));
						}
					}
				}
			}
			$result->free();
		}
	}

	send_table_changes("ic_node", "node_time", true);
	send_table_changes("ic_user", "user_update");
	send_table_changes("ic_group", "group_id");
	send_table_changes("ic_log", "log_id");

	hash_print_end();

	print($output);

	// Finally make sure we backup this data
	require_once "include/db.backup.php";
}
