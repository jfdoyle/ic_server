module.exports = {
	apis: {
		files: ["api_handlers/*.ts"],
		tasks: ["changed:ts", "changed:uglify"]
	}
};
