module.exports = {
	main: {
		"tsconfig": true,
		"options": {
			"comments": true
		},
		"src": ["api_handlers/scorm.ts", "api_handlers/rsa.ts", "api_handlers/results.ts"],
		"outDir": "api_handlers/"
	}
};
