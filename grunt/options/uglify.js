module.exports = {
	main: {
		options: {
			screwIE8: true,
			preserveComments: "some",
			toplevel: true,
			mangle: {
				toplevel: true
			},
			compress: {
//				drop_console: true,
				drop_debugger: true,
				pure_getters: true,
				unsafe: true
			}
		},
		files: {
			"api_handlers/rsa.min.js": ["api_handlers/rsa.js"],
			"api_handlers/results.min.js": ["api_handlers/results.js"],
			"api_handlers/scorm.min.js": ["api_handlers/scorm.js"]
		}
	}
};
